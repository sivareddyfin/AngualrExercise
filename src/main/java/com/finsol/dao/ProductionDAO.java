package com.finsol.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.model.PackageDetails;
import com.finsol.model.ProductionScheduler;
import com.finsol.model.ProductionSchedulerEvents;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

@Repository("ProductionDAO")
@Transactional
public class ProductionDAO 
{
	private static final Logger logger = Logger.getLogger(ProductionDAO.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public List<ProductionScheduler> loadProductionSchedulerData(String changeItem)
    {
		String sql =null;
		if(changeItem.indexOf("-")!=-1)
		{
			sql = "select * from ProductionScheduler where  assemblydate='"+DateUtils.getSqlDateFromString(changeItem,Constants.GenericDateFormat.DATE_FORMAT)+"' and lineitemstatus='valid' and prodstatus='Scheduled' and modelno!='NA'";
		}
		else
		{
			if(changeItem.equalsIgnoreCase("change"))
			{
				sql = "select * from ProductionScheduler where prodstatus in ('Modified','To be cancelled')  and lineitemstatus='valid' and modelno!='NA'";
			}
			else
				sql = "select * from ProductionScheduler where prodstatus='New' and assemblydate is null and modelno!='NA'";
		
		}
        
        return hibernateDao.findByNativeSql(sql, ProductionScheduler.class);
    }
	
	
	public List<ProductionScheduler> loadProductionSchedulerSearch(String name,String value)
    {
		String sql = "select * from ProductionScheduler where "+name+"='"+value+"' and modelno!='NA' and lineitemstatus='valid'";
        
        return hibernateDao.findByNativeSql(sql, ProductionScheduler.class);
    }	
	
	
	@SuppressWarnings("unchecked")
	public List<ProductionSchedulerEvents> listEvents() {
		return (List<ProductionSchedulerEvents>) hibernateDao.getSessionFactory().openSession().createCriteria(ProductionSchedulerEvents.class).list();
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
	public boolean saveMultipleEntities(List entitiesList) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.saveOrUpdate(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public void deletePackageDtls(PackageDetails packageDetails) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM PackageDetails WHERE modelno = '"+packageDetails.getModelno()+"' and salesorder='"+packageDetails.getSalesorder()+"'").executeUpdate();
	}
	
	public List<PackageDetails> loadPackageDtls(List one,List two)
    {        
        Query query =hibernateDao.getSessionFactory().openSession().createQuery("from PackageDetails where salesorder in (:sales) and modelno in (:models)");
        query.setParameterList("sales", one);
        query.setParameterList("models", two);
        List<PackageDetails> list = query.list();
        return list;
       // return hibernateDao.findByNativeSql(sql, PackageDetails.class);
    }
}
