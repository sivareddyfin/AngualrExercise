package com.finsol.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.model.HyphonicGreeseAmount;
import com.finsol.model.MotorHorsePower;
import com.finsol.model.PrestNeoGreeseAmount;
import com.finsol.model.QAIssue;
import com.finsol.model.QAIssueDetails;
import com.finsol.model.ServiceInquiry;
import com.finsol.model.ServiceInquiryDtls;
import com.finsol.model.TestReport;
import com.finsol.model.TestReportDtls;
import com.finsol.service.ProductSelectionService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;
@Repository("ProductSelectionDao")
@Transactional
public class ProductSelectionDao {
private static final Logger logger = Logger.getLogger(ProductSelectionDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	@Autowired
	private ProductSelectionService productSelectionService;

	public List<String> getVoltagesOnfrequency(Integer frequencey) {
	
		Session session=hibernateDao.getSessionFactory().openSession();
		/*SQLQuery query=session.createSQLQuery("select Voltage from Voltage where  status=1 and  frequencey="+frequencey+"");*/
		SQLQuery query=session.createSQLQuery("select Voltage from Voltage where  status=1 and  frequencey="+frequencey+"");
		List<String> result=query.list();
		return result;
	}

	public List<String> getAllCouplingTypes() {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select couplingType from CouplingType where  status=1");
		List<String> result=query.list();
		return result;
	}

	public List<Object[]> getLocationFactor() {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select loadLocation,lf  from  LocationFactor   where  status=1");
		List<Object[]> result=query.list();
		return result;
	}

	public List<Object[]> getCouplingFactor() {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select couplingMethod,cf  from  CouplingFactor   where  status=1");
		List<Object[]> result=query.list();
		return result;
	}

	public List<Object[]> getShockFactor() {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select degreeOfShock,fsform  from  shockFactor   where  status=1");
		List<Object[]> result=query.list();
		return result;
	}

	public List<String> getAllLoadTypes() {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select loadType  from  loadTypes   where  status=1");
		List<String> result=query.list();
		return result;
	}

	public List<Object[]> getAllLoadfactor(String loadtype, int workinghours) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select loadFactor,operatingHrsEnd   from  LoadFactor    where  loadType='"+loadtype+"'  and status=1");
		List<Object[]> result=query.list();
		return result;
	}

	public List<Object[]> getAllOutputSpeedsRadialNodes(int frequency) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select Top 1 RadialLoadN,ReductionRatio,oPSpeed,oPTorqueKgFM from FrameSizes where frequency="+frequency+"");
		List<Object[]> result=query.list();
		return result;
	}

	public List<Object[]> gtmomentOfInertia(String motortype, Double powerkw) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select Top 1 mi,gdsq from MomentOfInertias  where motorType='"+motortype+"' and wattage="+powerkw+"");
		List<Object[]> result=query.list();
		return result;
	}

	public List<Object[]> getCalculatedPower() 
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select startingpower,endingpower  from SelectPower ");
		List<Object[]> result=query.list();
		return result;
	 
	}

	public List<Integer> getReductionRation(int frequency, Double rpm) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select reductionvalue from outputrpm where frquency="+frequency+" and outputrpm="+rpm+"");
		List<Integer> result=query.list();
		return result;
	}

	public List<Double> getKws()
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select distinct  wattage  from MomentOfInertias order by wattage");
		List<Double> result=query.list();
		return result;
	}

	public List<Object[]> getFrameSize(int frequency, int reductionvalue, Double kws) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select FrameSize,IPCapacitySymbol,ReductionRatio,RadialLoadN,wattage,oPTorqueNM  from FrameSizes where frequency="+frequency+" and ReductionRatio="+reductionvalue+" and wattage="+kws+"");
		List<Object[]> result=query.list();
		return result;
	}

	public Double getMaxRpm(int frequency, Double rpm) {
Double maxrpm=0.0;
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select top 1 coalesce(outputrpm,0) from outputrpm where outputrpm>="+rpm+" and frquency="+frequency+" order by outputrpm;");
		List<Double> result=query.list();
		for(Double rpm1:result)
		{
			maxrpm=rpm1;
		}
		
		return maxrpm;
	}

	public Double getMinRpm(int frequency, Double rpm) 
	{
		Double minrpm=0.0;
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=session.createSQLQuery("select top 1 coalesce(outputrpm,0) from outputrpm where outputrpm<="+rpm+" and frquency="+frequency+" order by outputrpm desc;");
		List<Double> result=query.list();
		for(Double rpm1:result)
		{
			minrpm=rpm1;
		}
		
		 
		
		return minrpm;
		 
	}

	public List<String> getModelNUmberOnFootMountWihoutBrake(int frequency, Double kws, int capacity,
			int framesizevalue, int reduction, int shaftraidoloadn, Double rpm) {
		Session session=hibernateDao.getSessionFactory().openSession();
		
		if(frequency==50)
		{
			 session=hibernateDao.getSessionFactory().openSession();
			/*SQLQuery query=session.createSQLQuery("select  footMountNumberStandard from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad50hz="+shaftraidoloadn+"and outputSpeed50hz="+rpm+"");
*/		SQLQuery query=session.createSQLQuery("select  footMountNumberStandard from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad50hz="+shaftraidoloadn+"");
	
			 List<String> result=query.list( );
			return result;
			
		}
		
		else
		{
			 session=hibernateDao.getSessionFactory().openSession();
				SQLQuery query=session.createSQLQuery("select  footMountNumberStandard from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad60hz="+shaftraidoloadn+"");
				List<String> result=query.list();
				return result;
		}
	}

	public List<String> getModelNUmberOnFootMountBrake(int frequency, Double kws, int capacity, int framesizevalue,
			int reduction, Double shaftraidoloadn, Double rpm) 
	{
	Session session=hibernateDao.getSessionFactory().openSession();
		
		if(frequency==50)
		{
			 session=hibernateDao.getSessionFactory().openSession();
			SQLQuery query=session.createSQLQuery("select footMountModelNumberWithBrake  from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad50hz="+shaftraidoloadn+"");
			List<String> result=query.list( );
			return result;
			
		}
		
		else
		{
			 session=hibernateDao.getSessionFactory().openSession();
				SQLQuery query=session.createSQLQuery("select footMountModelNumberWithBrake   from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad60hz="+shaftraidoloadn+"");
				List<String> result=query.list();
				return result;
		}
		 
	}

	public List<String> getModelNUmberOnFlangeMountWihoutBrake(int frequency, Double kws, int capacity,
			int framesizevalue, int reduction, Double shaftraidoloadn, Double rpm) {
		
Session session=hibernateDao.getSessionFactory().openSession();
		
		if(frequency==50)
		{
			 session=hibernateDao.getSessionFactory().openSession();
			SQLQuery query=session.createSQLQuery("select  flangeMountModelNumberStandard  from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad50hz="+shaftraidoloadn+"");
			List<String> result=query.list( );
			return result;
			
		}
		
		else
		{
			 session=hibernateDao.getSessionFactory().openSession();
				SQLQuery query=session.createSQLQuery("select  flangeMountModelNumberStandard   from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad60hz="+shaftraidoloadn+"");
				List<String> result=query.list();
				return result;
		}
		 
	}

	public List<String> getModelNUmberOnFlangeMountWithBrake(int frequency, Double kws, int capacity,
			int framesizevalue, int reduction, Double shaftraidoloadn, Double rpm) 
	{
Session session=hibernateDao.getSessionFactory().openSession();
		
		if(frequency==50)
		{
			 session=hibernateDao.getSessionFactory().openSession();
			SQLQuery query=session.createSQLQuery("select  flangeMountModelNumberWithBrake  from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad50hz="+shaftraidoloadn+"");
			List<String> result=query.list( );
			return result;
			
		}
		
		else
		{
			 session=hibernateDao.getSessionFactory().openSession();
				SQLQuery query=session.createSQLQuery("select  flangeMountModelNumberWithBrake   from  ModelNumber where power="+kws+" and capacity="+capacity+" and frameSize="+framesizevalue+" and ratio="+reduction+" and alowablRadialLoad60hz="+shaftraidoloadn+"");
				List<String> result=query.list();
				return result;
		}
		 
	}

	public Integer getAllowableFrequency(Double ratio, String string) 
 {
	try
	{
		Integer allwablefrequency = 0;
	
		Session session = hibernateDao.getSessionFactory().openSession();
		if (string.equals("direct")) {
			SQLQuery query = session.createSQLQuery(
					" select  frequencey from AFrequency where ratio =(select top 1 ratio from AFrequency where ratio >= "
							+ ratio + " and motortype='Direct Shaft' order by ratio) and motortype ='Direct Shaft' ");
			List<Double> result = query.list();
			for (Double val : result) {
				allwablefrequency = val.intValue();
			}

			if (allwablefrequency == 0) {
				allwablefrequency = 5;
			}

		} else {

			SQLQuery query = session.createSQLQuery(
					" select  frequencey from AFrequency where ratio =(select top 1 ratio from AFrequency where ratio >= "
							+ ratio + " and motortype <> 'Direct Shaft' order by ratio) and motortype <> 'Direct Shaft' ");
			List<Double> result = query.list();
			for (Double val : result) {
				allwablefrequency = val.intValue();
			}

			if (allwablefrequency == 0) {
				allwablefrequency = 5;
			}

			// TODO Auto-generated method stub

		}

		return allwablefrequency;
		
	}catch(Exception e)
	{
		logger.error("the error occure at"+e.getMessage());
		return 0;
		
	}

	}

	public List<Double> getAllowableFrequencies(String coupilingtype) 
	{
		Session session = hibernateDao.getSessionFactory().openSession();
		List<Double> result=null;
		if(coupilingtype.equalsIgnoreCase("Direct Shaft"))
		{
		SQLQuery s	=session.createSQLQuery(" select  frequencey from AFrequency where motortype = 'Direct Shaft' ");
			 result = s.list();

		}
		else
		{
			SQLQuery s	=session.createSQLQuery(" select  frequencey from AFrequency where motortype != 'Direct Shaft' ");
			 result = s.list();

		}
		
		return result;
	 	}
	 

	public boolean saveEntities(List entitiesList) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.saveOrUpdate(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	
	public boolean saveEntitiesTestReport(List entitiesList,TestReport testReport) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        productSelectionService.deleteTestReportDtls(testReport);
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.saveOrUpdate(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public Object getEntityByIdVarchar(String id,Class className) 
	{		
		return (Object) hibernateDao.getSessionFactory().openSession().get(className, id);
	}
	
	
	public List<TestReportDtls> getTestReportDtlsByPONumber(String custPONum)
    {
        String sql = "select * from TestReportDtls where customerpono ='" + custPONum+"'";
        
        return hibernateDao.findByNativeSql(sql, TestReportDtls.class);
    }

	public void deleteTestReportDtls(TestReport testReport) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM TestReportDtls WHERE customerpono = '"+testReport.getCustomerpono()+"'").executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<TestReport> listTestReports() {
		return (List<TestReport>) hibernateDao.getSessionFactory().openSession().createCriteria(TestReport.class).list();
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
	
	public List<TestReport> loadTestReportsByCurrentDate()
    {
        String sql = "select * from TestReport where assembleddate = CAST(CURRENT_TIMESTAMP AS DATE)";
        
        return hibernateDao.findByNativeSql(sql, TestReport.class);
    }
	
	public List<TestReport> loadTestReportsByCustomerName(String fromdate,String todate)
    {
        String sql = "select * from TestReport where datereceived between '" + DateUtils.getSqlDateFromString(fromdate,Constants.GenericDateFormat.DATE_FORMAT) + "' and '" + DateUtils.getSqlDateFromString(todate,Constants.GenericDateFormat.DATE_FORMAT) + "'";
        
        return hibernateDao.findByNativeSql(sql, TestReport.class);
    }
	
	
	
	public List<QAIssue> loadQAIssuesByCustomerName(String fromdate,String todate)
    {
        String sql = "select * from QAIssue where scadate between '" + DateUtils.getSqlDateFromString(fromdate,Constants.GenericDateFormat.DATE_FORMAT) + "' and '" + DateUtils.getSqlDateFromString(todate,Constants.GenericDateFormat.DATE_FORMAT) + "' ";
        return hibernateDao.findByNativeSql(sql, QAIssue.class);
    }
	
	
	public List<ServiceInquiry> loadServiceInquiryByCustomerName(String cusName)
    {
        String sql = "select * from ServiceInquiry where customer='"+cusName+"'";
        return hibernateDao.findByNativeSql(sql, ServiceInquiry.class);
    }
	
	public List loadPrestNeoGreeseAmount(Integer framesize,Integer ratio,String flangetype)
    {
		PrestNeoGreeseAmount prestNeoGreeseAmount =null;
        String sql = "select * from PrestNeoGreeseAmount where framesize ="+framesize+" and ratio="+ratio+" and flangetype='"+flangetype+"'";
        List ls=hibernateDao.findByNativeSql(sql, PrestNeoGreeseAmount.class);
        	 
        return ls;
    }
	public List loadHyphonicGreeseAmount(Integer framsize)
    {
        String sql = "select * from HyphonicGreeseAmount where framesize ="+framsize+"";
        List ls=hibernateDao.findByNativeSql(sql, HyphonicGreeseAmount.class);
        return ls;
    }
	public List<MotorHorsePower> getHorsePower(Integer frequency,Integer voltage,Double power)
    {
        String sql = "select * from MotorHorsePower where frequency ="+frequency+" and voltage ="+voltage+" and horsepower="+power+" ";        
        return hibernateDao.findByNativeSql(sql, MotorHorsePower.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<ServiceInquiryDtls> listServiceInquiryDtls(String crefno) {
		String sql = "select * from ServiceInquiryDtls where casereferenceno ='"+crefno+"' "; 
		  return hibernateDao.findByNativeSql(sql, ServiceInquiryDtls.class);
	}
	@SuppressWarnings("unchecked")
	public List<QAIssueDetails> listQAIssueDetails(String crefno) {
		String sql = "select * from QAIssueDetails where casereferenceno ='"+crefno+"' "; 
		  return hibernateDao.findByNativeSql(sql, QAIssueDetails.class);
	}
	
}
