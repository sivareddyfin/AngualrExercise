package com.finsol.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.model.Users;

@Repository("UserManagementDao")
@Transactional
public class UserManagementDao {
	
	private static final Logger logger = Logger.getLogger(UserManagementDao.class);

	@Autowired	
	private HibernateDao hibernateDao;
	
	public boolean saveEntities(List entitiesList) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.saveOrUpdate(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	@SuppressWarnings("unchecked")
	public List<Users> listUsers() {
		return (List<Users>) hibernateDao.getSessionFactory().openSession().createCriteria(Users.class).list();
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
	
}
