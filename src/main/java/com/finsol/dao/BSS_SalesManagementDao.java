package com.finsol.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.model.Currency;
import com.finsol.model.Region;
import com.finsol.model.YearlyBudget;


/**
 * @author Rama Krishna
 * 
 */
@Repository("BSS_SalesManagementDao")
@Transactional
public class BSS_SalesManagementDao {
	

	private static final Logger logger = Logger.getLogger(BSS_SalesManagementDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	public boolean saveMultipleEntities(List entitiesList) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.saveOrUpdate(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public boolean saveEntities(List entitiesList) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.save(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public List getProductCategory(String countrycode,Integer year,Boolean isMaster)
    {
		
		String sql =null;
		if(isMaster)
		{
			sql = "select productcategorycode,productcategory from ProductCategory";
		}
		else
		{
			sql = "select pccode,pcname from YearlyHistory  where countrycode='" + countrycode+"' and year=" + year+"";
		}
		
        List productCategoryList=hibernateDao.findBySqlCriteria(sql);
        if(productCategoryList.size()>0 && productCategoryList!=null)
        	return productCategoryList;
        else
        	return null;         
    }
	
	public List getRegionSalesMenByRegionCode(String regionCode)
    {
		String sql = "select salesmancode from Regionsalesmen  where regioncode='"+regionCode+"'";
		
        List regionList=hibernateDao.findBySqlCriteria(sql);
        if(regionList.size()>0 && regionList!=null)
        	return regionList;
        else
        	return null;         
    }
	
	
	public List getProductsByCategory(String categorycode,Boolean isMaster)
    {
		String sql =null;
		if(isMaster)
		{
			sql = "select productcode,product from Product  where pccode='" + categorycode+"'";
		}
		else
		{
			sql = "select productname,bookingvalue from YearlyHistory  where pccode='" + categorycode+"'";
		}
		
        List productsList=hibernateDao.findBySqlCriteria(sql);
        if(productsList.size()>0 && productsList!=null)
        	return productsList;
        else
        	return null;         
    }
	
	public List getYearlyBudget(String countrycode,Integer year,String pccode,Boolean isMaster)
    {
		String sql =null;
		List productsList=null;
		if(isMaster)
		{
			sql = "select productcode,product from Product  where pccode='" + pccode+"'";
			productsList=hibernateDao.findBySqlCriteria(sql);
		}
		else
		{
			sql = "select * from YearlyBudget  where countrycode='" + countrycode+"' and pccode='" + pccode+"' and  year=" + year+"";
			productsList=hibernateDao.findByNativeSql(sql,YearlyBudget.class);
		}
		
        if(productsList.size()>0 && productsList!=null)
        	return productsList;
        else
        	return null;         
    }
	
	
	public List getCurrencyByCountryCode(String countrycode)
    {
			String sql = "select * from Currency  where countrycode='" + countrycode+"'";
			List currencyList=hibernateDao.findByNativeSql(sql,Currency.class);
		
        if(currencyList.size()>0 && currencyList!=null)
        	return currencyList;
        else
        	return null;         
    }
	
	public List getRegionByCountryCode(String countrycode)
    {
		String sql = "select * from Region  where countrycode='" + countrycode+"' order by id";
		List currencyList=hibernateDao.findByNativeSql(sql,Region.class);
	
        if(currencyList.size()>0 && currencyList!=null)
        	return currencyList;
        else
        	return null;         
    }
	
	
	
	
	
	
	public void deleteRegionSales(String regionCode) {
		//hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Shift WHERE shiftid = "+shift.getShiftid()).executeUpdate();
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Regionsalesmen where regioncode='"+regionCode+"'").executeUpdate();
	}
	// created by siva reddy
	public List<Integer> getDistinctMonthNumbers() {
		
		List<Integer> values=null;
		try
		{
		
		Session s=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= s.createSQLQuery("select distinct month from ytdbookingregion where year=2016 ORDER BY month");
		 values=query.list();	
		
		}catch(Exception e)
		{
			logger.error("getDistinctMonthNumbers() getDistinctMonthNumbers()  getDistinctMonthNumbers()   getDistinctMonthNumbers() "+e.getMessage());
			
			
		}
		return values;
	}

	public Double getBudgetvaluesforMonthleyYtd(int i, String countryname) 
	{
		
		List<Double> values=null;
		Double f=0.0;
		try
		{
		Session s=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= s.createSQLQuery("select sum(month"+i+") from yearlybudget where year=2016 and country='"+countryname+"'");
		 values=query.list();	
		 if(values.contains(null))
		 {
			 
			 f=0.0;
		 }
		 
		 else
		 {
		 for(Double d:values)
		 {
			 f=d;
			 
		 }
		 }
		}catch(Exception e)
		{
			logger.error(" getBudgetvaluesforMonthleyYtd() getBudgetvaluesforMonthleyYtd() getBudgetvaluesforMonthleyYtd() getBudgetvaluesforMonthleyYtd() "+e.getMessage());
		}
		return f;
	}

	public Double getBookingvaluesforMonthleyYtd(int i, String countryname) {

		List<Double> values=null;
		Double f=0.0;
		try{
			Session s=hibernateDao.getSessionFactory().openSession();
			SQLQuery query= s.createSQLQuery("select sum(bookingamount) from ytdbookingregion where year=2016 and country='"+countryname+"' and month="+i+"");
			 values=query.list();	
			 if(values.contains(null))
			 {
				 
				 f=0.0;
			 }
			 else
			 {
			 
			 for(Double d:values)
			 {
				 f=d;
				 
			 }
			 }
		}catch(Exception e)
		{
			
			logger.error("getBookingvaluesforMonthleyYtd()  getBookingvaluesforMonthleyYtd()  getBookingvaluesforMonthleyYtd() getBookingvaluesforMonthleyYtd() "+e.getMessage());
						
		}
	 
		return f;
	}
	

	public List getProductsByCategory(String countrycode,Integer year,String isMaster)
    {
		String sql =null;
		List productsList=null;
		if(isMaster.equalsIgnoreCase("false"))
		{
			sql = "select productname,bookingvalue,slno from YearlyHistory  where countrycode='"+countrycode+"' and year="+year;
			try{
				productsList=hibernateDao.findBySqlCriteria(sql);
			}
			catch(Exception e)
			{
				System.out.println("the exception is"+e);
			}
		}
		else
		{
			sql = "select productname,bookingvalue,slno from YearlyHistory  where pccode='" +isMaster+"' and countrycode='"+countrycode+"' and year="+year+"";
			try{
				productsList=hibernateDao.findBySqlCriteria(sql);
			}
			catch(Exception e)
			{
				System.out.println("the exception is"+e);
			}
		}
		
        //List productsList=hibernateDao.findBySqlCriteria(sql);
		
        if(productsList.size()>0 && productsList!=null)
        	return productsList;
        else
        	return null;         
    }
	
	public Object getRowByColumn(Class className,String fName,String value)
	{
		return hibernateDao.getRowByColumn(className,fName,value);
	}
	
	
	public List getTag(String pcode,String framesize)
    {
		String sql =null;
		List productsList=null;

			sql = "select tag from FrameSizeAndTag  where productcode='"+pcode+"' and framesize="+framesize;
			try{
				productsList=hibernateDao.findBySqlCriteria(sql);
			}
			catch(Exception e)
			{
				System.out.println("the exception is"+e);
			}

		
        //List productsList=hibernateDao.findBySqlCriteria(sql);
		
        if(productsList.size()>0 && productsList!=null)
        	return productsList;
        else
        	return null;         
    }

	public List<Currency> getCurrencyByCountryCode1(String countrycode) 
	{
		
		Session session = (Session)hibernateDao.getSessionFactory().openSession();	
		String strQry="From Currency where  countrycode='"+countrycode+"'";
		Query sqlQuery=session.createQuery(strQry);
		List<Currency> curr=sqlQuery.list();
		 
		return curr;
		
		 
	}
}
