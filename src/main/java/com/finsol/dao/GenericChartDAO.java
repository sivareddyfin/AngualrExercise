package com.finsol.dao;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Rama Krishna
 * 
 */
@Repository("GenericChartDAO")
@Transactional
public class GenericChartDAO {
	
	private static final Logger logger = Logger.getLogger(GenericChartDAO.class);
	
	@Autowired	
	private HibernateDao hibernateDao;

	public List<Integer> getDistinctMonthNumbers(Integer curYear) {
		
		List<Integer> values=null;
		try
		{		
			Session session=hibernateDao.getSessionFactory().openSession();
			SQLQuery query= session.createSQLQuery("select distinct month from ytdbookingregion where year="+curYear+" ORDER BY month");
			values=query.list();		
		}
		catch(Exception e)
		{
			logger.error("getDistinctMonthNumbers()---"+e.getMessage());
			
			
		}
		return values;
	}
	
	public List<Object[]> getCountries() 
	{		
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select country,budgclr,bokclr from country where countrycode!='HK' ORDER BY country_order desc");
		List<Object[]> country=query.list();
		return country;
	}
	
	public List<Object[]> getCountriesNameColor(String countryCode) 
	{		
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select country,budgclr,bokclr from country where countrycode='"+countryCode+"' ORDER BY country_order desc");
		List<Object[]> country=query.list();
		return country;
	}
	public List<Object[]> getBudgetofallmonths(Integer curYear,String countryname) {
		
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query=null;
		if("NA".equalsIgnoreCase(countryname))
		 query= session.createSQLQuery("select coalesce(sum(month1),0) as a,coalesce(sum(month2),0) as b,coalesce(sum(month3),0) as c,coalesce(sum(month4),0) as d,coalesce(sum(month5),0) as e,coalesce(sum(month6),0) as f,coalesce(sum(month7),0) as g,coalesce(sum(month8),0) as h,coalesce(sum(month9),0) as i,coalesce(sum(month10),0) as j,coalesce(sum(month11),0) as k,coalesce(sum(month12),0) as l from YearlyBudget where year="+curYear+"");
		else
		 query= session.createSQLQuery("select country as ctry,sum(month1) as a,sum(month2) as b,sum(month3) as c,sum(month4) as d,sum(month5) as e,sum(month6) as z,sum(month7) as f,sum(month8) as g,sum(month9) as h,sum(month10) as i,sum(month11) as j,sum(month12) as k from YearlyBudget where country in(select country from country where country='"+countryname+"') and year="+curYear+" group by country");
			
			//query= session.createSQLQuery("select country as ctry,sum(month1) as a,sum(month2) as b,sum(month3) as c,sum(month4) as d,sum(month5) as e,sum(month6) as z,sum(month7) as f,sum(month8) as g,sum(month9) as h,sum(month10) as i,sum(month11) as j,sum(month12) as k from YearlyBudget where LOWER(country) in(select country from country where UPPER(country)= UPPER('"+countryname+"')) and year="+curYear+" group by country");
		List<Object[]> values=query.list();
		// TODO Auto-generated method stub
		return values;		 
	}
	
	public List<Object[]> getBookingsOfMonthley(String countryname,Integer curYear) {
		// TODO Auto-generated method stub
		SQLQuery query=null;
		Session session=hibernateDao.getSessionFactory().openSession();
		if("NA".equalsIgnoreCase(countryname))
			query= session.createSQLQuery("select sum(amountusd),month from ytdbookingregion where year="+curYear+"   group by month order by month");
		else
			query= session.createSQLQuery("select sum(bookingamount),month from ytdbookingregion where year="+curYear+" and country = '"+countryname+"'   group by month order by month");
		List<Object[]> values=query.list();
		// TODO Auto-generated method stub
		return values;
		 
	}
	public Double getQuarterlyBooking(Integer year,String mString,String countryName)
    {
		String sql = null;
		if("NA".equalsIgnoreCase(countryName))
			 sql = "select coalesce(sum(amountusd),0)/3 as quarter from YtdBookingProduct where year="+year+" and month in("+mString+") ;";
		else
			sql = "select coalesce(sum(bookingamount),0)/3 as quarter from YtdBookingProduct where year="+year+" and country='"+countryName+"' and month in("+mString+") ;";
        List reqDateList=hibernateDao.findBySqlCriteria(sql);
        Map mp=(Map)reqDateList.get(0);
		Double value=(Double)mp.get("quarter");
        if(reqDateList.size()>0 && reqDateList!=null)
        	return value;
        else
        	return 0.00; 
    }
	
	public Double getBookingValueByYear(Integer year,String countryCode) 
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		Double bVal=0.00;
		Query query=null;
		try
		{
			if(countryCode.equals("NA"))
				query= session.createQuery("select sum(bookingvalue) from YearlyHistory where year="+year+"");
			else
				query= session.createQuery("select sum(bookingvalue) from YearlyHistory where year="+year+" and countrycode='"+countryCode+"'");
			
			List values=query.list();			
			bVal=(Double)values.get(0);

			if(bVal!=null)
				return bVal;
			else
				return 0.00;
		}catch(Exception e)
		{
			System.out.println("this is the error message"+e.getMessage());
			return 0.00;
		}
		finally
		{
			session.close();
		}
	}
	
	public Double getBudgetValueByYear(Integer year,String countryCode) 
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		Double bVal=0.00;
		Query query=null;
		try
		{
			if("NA".equalsIgnoreCase(countryCode))
				query= session.createQuery("select coalesce(sum(month1+month2+month3+month4+month5+month6+month7+month8+month9+month10+month11+month12),0) from YearlyBudget where year="+year+"");
			else
				query= session.createQuery("select coalesce(sum(month1+month2+month3+month4+month5+month6+month7+month8+month9+month10+month11+month12),0) from YearlyBudget where year="+year+" and countrycode='"+countryCode+"'");
				
			List values=query.list();			
			bVal=(Double)values.get(0);

			if(bVal!=null)
				return bVal;
			else
				return 0.00;
		}catch(Exception e)
		{
			System.out.println("this is the error message"+e.getMessage());
			return 0.00;
		}	
		finally
		{
			session.close();
		}
	}
	
	public List<Integer> getLastYears(int year) 
	{		 
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select distinct  year from YtdBookingProduct  where year in (select distinct top 7 year from YtdBookingProduct where year <= "+year+" order by year  desc)  order by year  ");
		List<Integer> values=query.list();	
		return values;		
	}

	public List<Object[]> getcatagaoysbookingamountoveryears(String catageoryname,int year) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <="+year+" order by year desc) and apc ='"+catageoryname+"' group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> getProducts(int year, String category) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select product,sum(bookingamount) from  YtdBookingProduct where year="+year+"  and productcategory ='"+category+"'group  by product");
		List<Object[]> values=query.list();	
		return values;
	}

	public List getBookingValueForIndustrySectorShareOptBasedoncountry(int i, String country,String group) 
	{
		String sql =null;
		 
		if("sca".equalsIgnoreCase(group))
			sql = "select sum(bookingamount)  as value,industrysectorcode as code,industrysector as name,'#/app/SCA_bookingByIndustry' AS link,year  from YtdBookingIndustry where year="+i+" and country='"+country+"' group by  year,industrysector,industrysectorcode";
		else
			sql = "select sum(bookingamount)  as value,industrysectorcode as code,industrysector as name,'#/app/"+country+"_bookingByIndustry' AS link,year  from YtdBookingIndustry where year="+i+" and country='"+country+"' group by  year,industrysector,industrysectorcode";
		
		List reqDateList=hibernateDao.findBySqlCriteria(sql);
        if(reqDateList.size()>0 && reqDateList!=null)
        	return reqDateList;
        else
        	return null;  
		 
	}

	public List<Object[]> getIndustriesByIndustrySectorbasedoncountry(String parameter, String countrycode) 
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select distinct industry,industrycode from YtdBookingIndustry where industrysectorcode='"+parameter+"' and country=(select country from  country where countrycode='"+countrycode+"')");
		List<Object[]> productList=query.list();
		return productList;
	}

	public Double getBookingValueForIndustrySharebasedoncountry(String icode, int parseInt, String countrycode) {
		
		Session session=hibernateDao.getSessionFactory().openSession();
		Double bVal=0.00;
		try
		{
			SQLQuery query= session.createSQLQuery("select sum(bookingamount) from YtdBookingIndustry where industrycode='"+icode+"' and year="+parseInt+" and country=(select country from country where countrycode='"+countrycode+"')");
			List values=query.list();			
			bVal=(Double)values.get(0);

			if(bVal!=null)
				return bVal;
			else
				return 0.00;
		}catch(Exception e)
		{
			System.out.println("this is the error message"+e.getMessage());
			return 0.00;
		}
		
		 
	}

	public List<Object[]> getAllDetailsAboutProducts(String category) 
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select product,bokclr from product where pccode='"+category+"'");
		List<Object[]> productList=query.list();
		return productList;
	}

	public List<Object[]> catagaoysbookingamountforproductoveryears(String catageoryname, int year) 
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <=2016 order by year desc) and product ='"+catageoryname+"'group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}
	
	public List getBookingValueForProductShareOptBasingonCountry(int i, String countrycode) 
	{
		String sql = "select sum(bookingamount) as value,product as name from YtdBookingProduct where year="+i+" and country=(select distinct country from Country where countrycode='"+countrycode+"') group by product";
	    List reqDateList=hibernateDao.findBySqlCriteria(sql);
	    if(reqDateList.size()>0 && reqDateList!=null)
	    	return reqDateList;
	    else
	    	return null;  
	}
	public List<Object[]> getTabledataforCatagories(Integer tableyears, List<Object[]> aboutcatgories) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select productcategory,sum(bookingamount) from  YtdBookingProduct where year = "+tableyears+"  and productcategory  in(select productcategory  from productcategory)   group by productcategory");
		 
		List<Object[]> values=query.list();	
		return values;
		}
	
	public List<Object[]> getTabledataforproducts(Integer tableyears, String category) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select product,sum(amountusd) from  YtdBookingProduct where year = "+tableyears+" and apc='"+category+"' and product  in(select product  from product  where pccode='"+category+"' )  group by product");
		 
		List<Object[]> values=query.list();	
		return values;
	}
	
	public List<Object[]> getcatagaoysbookingamountoveryearsforindividualcountry(String catageoryname, int year, String countrycode) 	
	{
		
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(bookingamount) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <="+year+" order by year desc) and apc ='"+catageoryname+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> getTabledataforCatagoriesonparticularcountry(Integer tableyears, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select productcategory,sum(bookingamount) from  YtdBookingProduct where year = "+tableyears+"  and productcategory  in(select productcategory  from productcategory)  and country =(select country from country where countrycode='"+countrycode+"')  group by productcategory");
		 
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> getTabledataforproducts(Integer tableyears, String category, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select product,sum(bookingamount) from  YtdBookingProduct where year = "+tableyears+"  and product  in(select product  from product  where pccode='"+category+"' )  and country=(select country from country where countrycode='"+countrycode+"') group by product");
		 
		List<Object[]> values=query.list();	
		return values;
	}
	
	public List<Object[]> getcatagaoysbookingamountforproductoveryearsoverIndividualCountry(String catageoryname,
			int year, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(bookingamount) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}
	
	public List<Object[]> productbookingamountoveryearsforCountry(String catageoryname, int year, String countrycode) {

		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(bookingamount) from  YtdBookingProduct where year in(select distinct top 3 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}
	
	public List<Object[]> getproductbookingamountoveryears(String catageoryname, int year) 
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 3 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"'group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	 
	}
	
	public List<Object[]> getCountriesDtails(String countrycode) {
		// TODO Auto-generated method stub
		
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select country,budgclr,bokclr from country where countrycode='"+countrycode+"'");
		List<Object[]> country=query.list();
		return country;
	}

	public List<Object[]> getBookingsOfMonthleyInUsd(String countryname, Integer curYear) {
		SQLQuery query=null;
		Session session=hibernateDao.getSessionFactory().openSession();
		if("NA".equalsIgnoreCase(countryname))
			query= session.createSQLQuery("select sum(amountusd),month from ytdbookingregion where year="+curYear+"   group by month order by month");
		else
			query= session.createSQLQuery("select sum(amountusd),month from ytdbookingregion where year="+curYear+" and country = '"+countryname+"'   group by month order by month");
		List<Object[]> values=query.list();
		// TODO Auto-generated method stub
		return values;
	}

	public Double getQuarterlyBookingInUsd(int year, String monthString, String countryName) {
		String sql = null;
		if("NA".equalsIgnoreCase(countryName))
			 sql = "select coalesce(sum(amountusd),0)/3 as quarter from YtdBookingProduct where year="+year+" and month in("+monthString+") ;";
		else
			sql = "select coalesce(sum(amountusd),0)/3 as quarter from YtdBookingProduct where year="+year+" and country='"+countryName+"' and month in("+monthString+") ;";
        List reqDateList=hibernateDao.findBySqlCriteria(sql);
        Map mp=(Map)reqDateList.get(0);
		Double value=(Double)mp.get("quarter");
        if(reqDateList.size()>0 && reqDateList!=null)
        	return value;
        else
        	return 0.00; 
		 
	}

	public Double getCWQuarterlyBookingValue(String quarterly, int i, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		Double bVal=0.00;
	 
			SQLQuery query= session.createSQLQuery("select sum(bookingamount) from YtdBookingRegion where month in("+quarterly+") and year="+i+" and country=(select country from country where countrycode='"+countrycode+"')");
			List values=query.list();			
			bVal=(Double)values.get(0);

			if(bVal!=null)
				return bVal;
			else
				return 0.0;
	}

	public List getBookingValueForIndustrySectorShareOptBasedoncountryOnDollar(int i, String country, String group) 
	{

		String sql =null;
		 
		if("sca".equalsIgnoreCase(group))
			sql = "select sum(amountusd)  as value,industrysectorcode as code,industrysector as name,'#/app/SCA_bookingByIndustry' AS link,year  from YtdBookingIndustry where year="+i+" and country='"+country+"' group by  year,industrysector,industrysectorcode";
		else
			sql = "select sum(amountusd)  as value,industrysectorcode as code,industrysector as name,'#/app/"+country+"_bookingByIndustry' AS link,year  from YtdBookingIndustry where year="+i+" and country='"+country+"' group by  year,industrysector,industrysectorcode";
		
		List reqDateList=hibernateDao.findBySqlCriteria(sql);
        if(reqDateList.size()>0 && reqDateList!=null)
        	return reqDateList;
        else
        	return null;  
		
		
		
		
	}

	public List<Object[]> catagaoysbookingamountoveryearsforindividualcountrydollar(String catageoryname, int year,
			String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <="+year+" order by year desc) and apc ='"+catageoryname+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> getTabledataforCatagoriesonparticularcountrydollar(Integer tableyears, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select productcategory,sum(amountusd) from  YtdBookingProduct where year = "+tableyears+"  and productcategory  in(select productcategory  from productcategory)  and country =(select country from country where countrycode='"+countrycode+"')  group by productcategory");
		 
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> getTabledataforproductsdollar(Integer tableyears, String category, String countrycode) {
		// TODO Auto-generated method stub

		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select product,sum(amountusd) from  YtdBookingProduct where year = "+tableyears+"  and product  in(select product  from product  where pccode='"+category+"' )  and country=(select country from country where countrycode='"+countrycode+"') group by product");
		 
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> catagaoysbookingamountforproductoveryearsoverIndividualCountrydollar(String catageoryname,
			int year, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> productbookingamountoveryearsforCountrydolllar(String catageoryname, int year,
			String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 3 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public Double getCWQuarterlyBookingValuedollar(String quarterly, int i, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		Double bVal=0.00;
		SQLQuery query=null;
	 if(countrycode!=null)
	 {
			 query= session.createSQLQuery("select sum(amountusd) from YtdBookingRegion where month in("+quarterly+") and year="+i+" and country=(select country from country where countrycode='"+countrycode+"')");
	 }
	 else
	 {
		 query= session.createSQLQuery("select sum(amountusd) from YtdBookingRegion where month in("+quarterly+") and year="+i+"");
	 }
			List values=query.list();			
			bVal=(Double)values.get(0);

			if(bVal!=null)
				return bVal;
			else
				return 0.0;
	}

	public List<Object[]> catagaoysbookingamountforproductoveryears(String catageoryname, int year, String category) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and apc='"+category+"'group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> catagaoysbookingamountforproductoveryearsoverIndividualCountry(String catageoryname, int year,
			String countrycode, String category) {
		 Session session=hibernateDao.getSessionFactory().openSession();
			SQLQuery query= session.createSQLQuery("select year,sum(bookingamount) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and apc='"+category+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
			List<Object[]> values=query.list();	
			return values;
	}

	public List<Object[]> catagaoysbookingamountforproductoveryearsoverIndividualCountrydollar(String catageoryname,
			int year, String countrycode, String category) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 7 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and apc='"+category+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> productbookingamountoveryearsforCountry(String catageoryname, int year, String countrycode,
			String catageorey) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(bookingamount) from  YtdBookingProduct where year in(select distinct top 3 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and apc='"+catageorey+"' and  country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public List<Object[]> productbookingamountoveryearsforCountrydolllar(String catageoryname, int year,
			String countrycode, String catageorey) {
		Session session=hibernateDao.getSessionFactory().openSession();
		SQLQuery query= session.createSQLQuery("select year,sum(amountusd) from  YtdBookingProduct where year in(select distinct top 3 year from YtdBookingProduct where year <="+year+" order by year desc) and product ='"+catageoryname+"' and  apc='"+catageorey+"' and country=(select country from country where countrycode='"+countrycode+"')group by year order by year");
		List<Object[]> values=query.list();	
		return values;
	}

	public Double getBookingValueForIndustrySharebasedoncountrydollr(String icode, int parseInt, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		Double bVal=0.00;
		try
		{
			SQLQuery query= session.createSQLQuery("select sum(amountusd) from YtdBookingIndustry where industrycode='"+icode+"' and year="+parseInt+" and country=(select country from country where countrycode='"+countrycode+"')");
			List values=query.list();			
			bVal=(Double)values.get(0);

			if(bVal!=null)
				return bVal;
			else
				return 0.00;
		}catch(Exception e)
		{
			System.out.println("this is the error message"+e.getMessage());
			return 0.00;
		}
	}

	public Double getBookingValueForIndustrySharebasedoncountrydollar(String icode, int parseInt, String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		Double bVal=0.00;
		try
		{
			SQLQuery query= session.createSQLQuery("select sum(amountusd) from YtdBookingIndustry where industrycode='"+icode+"' and year="+parseInt+" and country=(select country from country where countrycode='"+countrycode+"')");
			List values=query.list();			
			bVal=(Double)values.get(0);

			if(bVal!=null)
				return bVal;
			else
				return 0.00;
		}catch(Exception e)
		{
			System.out.println("this is the error message"+e.getMessage());
			return 0.00;
		}
	}

	 
	

	 

	 
}
