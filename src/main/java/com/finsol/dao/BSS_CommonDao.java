package com.finsol.dao;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/*import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;*/
/**
 * @author naidu
 *
 */
@Repository("BSS_CommonDao")
@Transactional
public class BSS_CommonDao {
	
private static final Logger logger = Logger.getLogger(BSS_CommonDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }	
	
	public List getDropdownNames(String sqlQuery)
    {
        List dropDownList=hibernateDao.findBySqlCriteria(sqlQuery);
        if(dropDownList.size()>0 && dropDownList!=null)
        	return dropDownList;
        else
        	return null;         
    }
	
	 public Object getEntityByIdVarchar(String id,Class className) 
	 {	
	 return (Object) hibernateDao.getSessionFactory().openSession().get(className, id);
	 }

	 
	 public Object getEntityByDate(Date id,Class className) 
		{		
			return (Object) hibernateDao.getSessionFactory().openSession().get(className, id);
		}
	 public Object getEntityById(Integer id,Class className) 
		{		
			return (Object) hibernateDao.getSessionFactory().openSession().get(className, id);
		}
}
