package com.finsol.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.model.DeliveryUpdate;
import com.finsol.model.DeliveryUpdateTemp;
import com.finsol.model.ProbabilityMaster;
import com.finsol.model.ProgressUpdateDtlsTemp;
import com.finsol.model.ProgressUpdateSmry;
import com.finsol.model.ProgressUpdateSmryTemp;
import com.finsol.model.Quotation;
import com.finsol.model.SalesProgressIndicators;
import com.finsol.model.ViewNotes;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

@Repository("SalesReportDAO")
@Transactional
public class SalesReportDAO
{

	private static final Logger logger = Logger.getLogger(SalesReportDAO.class);

	@Autowired
	private HibernateDao hibernateDao;

	public boolean saveMultipleEntities(List entitiesList)
	{
		Session session = null;
		Transaction transaction = null;
		boolean updateFlag = false;
		try
		{
			session = (Session) hibernateDao.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			for (int i = 0; i < entitiesList.size(); i++)
			{
				session.saveOrUpdate(entitiesList.get(i));
			}
			session.flush();
			transaction.commit();
			updateFlag = true;
			return updateFlag;
		}
		catch (JDBCException jde)
		{
			logger.fatal("Error occured in database communication", jde);
			transaction.rollback();
			return updateFlag;
		}
		finally
		{
			if (session.isOpen())
			{
				session.close();
			}
		}
	}

	public void deleteSalesProgressIndicators()
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SalesProgressIndicators").executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<SalesProgressIndicators> listSalesProgressIndicators()
	{
		return (List<SalesProgressIndicators>) hibernateDao.getSessionFactory().openSession()
		        .createCriteria(SalesProgressIndicators.class).list();
	}

	@SuppressWarnings("unchecked")
	public List<ProbabilityMaster> listProbabilityMaster()
	{
		return (List<ProbabilityMaster>) hibernateDao.getSessionFactory().openSession().createCriteria(ProbabilityMaster.class)
		        .list();
	}

	public void deleteProbabilityMaster(ProbabilityMaster probabilityMaster)
	{
		hibernateDao
		        .getSessionFactory()
		        .openSession()
		        .createQuery("DELETE FROM ProbabilityMaster WHERE customergroup = '" + probabilityMaster.getCustomergroup() + "'")
		        .executeUpdate();
	}

	public List<ProbabilityMaster> loadProbabilityMasterByCustomerGroup(String customerGroup)
	{
		String sql = "select * from ProbabilityMaster where customergroup ='" + customerGroup + "'";

		return hibernateDao.findByNativeSql(sql, ProbabilityMaster.class);
	}

	public List loadDeliveryUpdate(String customerName)
	{
		String sql = "select * from DeliveryUpdateTemp where customername ='" + customerName + "'";
		List ls=hibernateDao.findByNativeSql(sql, DeliveryUpdateTemp.class);
		if(ls==null || ls.isEmpty())
		{
			sql = "select * from DeliveryUpdate where customername ='" + customerName + "'";
			ls=hibernateDao.findByNativeSql(sql, DeliveryUpdate.class);
		}
		return ls;
	}

	public List<Quotation> loadQutationUpdate(String customerName)
	{
		String sql = "select * from Quotation where custName ='" + customerName + "'";

		return hibernateDao.findByNativeSql(sql, Quotation.class);
	}

	public List<DeliveryUpdateTemp> loadDeliveryUpdateTemp(String salesrprtrefno)
	{
		String sql = "select * from DeliveryUpdateTemp where salesrprtrefno ='" + salesrprtrefno + "'";
		return hibernateDao.findByNativeSql(sql, DeliveryUpdateTemp.class);
	}

	public List<ProgressUpdateSmryTemp> loadProgressUpdateSmryTemp(String salesrprtrefno)
	{
		String sql = "select * from ProgressUpdateSmryTemp where salesrprtrefno ='" + salesrprtrefno + "'";
		return hibernateDao.findByNativeSql(sql, ProgressUpdateSmryTemp.class);
	}

	public List<ProgressUpdateDtlsTemp> loadProgressUpdateDtlsTemp(String salesrprtrefno)
	{
		String sql = "select * from ProgressUpdateDtlsTemp where salesrprtrefno ='" + salesrprtrefno + "'";
		return hibernateDao.findByNativeSql(sql, ProgressUpdateDtlsTemp.class);
	}

	public List loadProgressUpdateDtls(String quotno)
	{
		String sql = "select * from ProgressUpdateSmryTemp where quotationno ='" + quotno + "'";
		List ls= hibernateDao.findByNativeSql(sql, ProgressUpdateSmryTemp.class);
		
		if(ls==null || ls.isEmpty())
		{
			sql = "select * from ProgressUpdateSmry where quotationno ='" + quotno + "'";
			ls= hibernateDao.findByNativeSql(sql, ProgressUpdateSmry.class);
		}
		
		return ls;
		
	}
	
	public void deleteDeliverUpdatesTemp(String qtnNo)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM DeliveryUpdateTemp where quotationno='"+qtnNo+"'").executeUpdate();
	}
	public void deleteDeliverUpdates(String qtnNo)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM DeliveryUpdate where quotationno='"+qtnNo+"'").executeUpdate();
	}
	
	public void deleteProgressUpdateSmry(String qtnNo)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM DeliveryUpdate where quotationno='"+qtnNo+"'").executeUpdate();
	}
	
	public void deleteProgressUpdateSmryTemp(String qtnNo)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM ProgressUpdateSmryTemp where quotationno='"+qtnNo+"'").executeUpdate();
	}
	
	
	
	public void deleteProgressUpdateDtls(String qtnNo)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM DeliveryUpdate where quotationno='"+qtnNo+"'").executeUpdate();
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM ProgressUpdateDtlsTemp where quotationno='"+qtnNo+"'").executeUpdate();
	}
	
	public List loadViewNotes(String quotno,String customer,String salesman,String srdate)
	{
		String sql = null;
		
		if("NA".equalsIgnoreCase(quotno) && "NA".equalsIgnoreCase(customer))
		{
			sql = "select * from ViewNotes where notesdate= '" + DateUtils.getSqlDateFromString(srdate,Constants.GenericDateFormat.DATE_FORMAT) + "' and salesman='" + salesman + "'";
		}
		else if("NA".equalsIgnoreCase(quotno) && !("NA".equalsIgnoreCase(customer)))
		{
			sql = "select * from ViewNotes where notesdate= '" + DateUtils.getSqlDateFromString(srdate,Constants.GenericDateFormat.DATE_FORMAT) + "' and customer= '" + customer + "' and salesman='" + salesman + "'";
		}
		else if(!("NA".equalsIgnoreCase(quotno)) && "NA".equalsIgnoreCase(customer))
		{
			sql = "select * from ViewNotes where quotationno ='" + quotno + "' and notesdate= '" + DateUtils.getSqlDateFromString(srdate,Constants.GenericDateFormat.DATE_FORMAT) + "' and salesman='" + salesman + "'";
		}
		else
		{
			sql = "select * from ViewNotes where notesdate= '" + DateUtils.getSqlDateFromString(srdate,Constants.GenericDateFormat.DATE_FORMAT) + "' and quotationno ='" + quotno + "' and customer= '" + customer + "' and salesman='" + salesman + "'";
		}
		List ls= hibernateDao.findByNativeSql(sql, ViewNotes.class);
		
		return ls;
		
	}
	
	public List loadViewNotesByDate(String fromdate,String todate)
	{
		String sql = "select * from ViewNotes where notesdate between '" + DateUtils.getSqlDateFromString(fromdate,Constants.GenericDateFormat.DATE_FORMAT) + "' and '" + DateUtils.getSqlDateFromString(todate,Constants.GenericDateFormat.DATE_FORMAT) + "'";
		List ls= hibernateDao.findByNativeSql(sql, ViewNotes.class);
		
		return ls;
		
	}
	
	
}
