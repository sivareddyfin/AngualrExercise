package com.finsol.dao;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.bean.ApplicationBean;
import com.finsol.bean.IndustryBean;
import com.finsol.bean.TaxMasterBean;
import com.finsol.model.Application;
import com.finsol.model.Country;
import com.finsol.model.Industry;
import com.finsol.model.IndustrySector;
import com.finsol.model.Salesman;
import com.finsol.model.TaxMaster;

/*import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;*/
/**
 * @author naidu
 *
 */
@Repository("MasterScreen_TwoDao")
@Transactional
public class MasterScreen_TwoDao {
	
private static final Logger logger = Logger.getLogger(MasterScreen_TwoDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;

	public List<Country> getCountriesandCountryCodes() {
		Session session=hibernateDao.getSessionFactory().openSession();
		String hql = "FROM Country";
		Query query = session.createQuery(hql);
		List<Country> results = query.list();
		return results;
	}

	public void saveCountry(ArrayList<Country> al) {
		
		Session session=hibernateDao.getSessionFactory().openSession();
		session.beginTransaction();
		Query q=session.createQuery("delete from Country");
		q.executeUpdate();
		for(Country c:al)
		{
			Country v1=new 	Country();
			v1.setBokclr(c.getBokclr());
			v1.setBudgclr(c.getBudgclr());
			v1.setCountry(c.getCountry());
			v1.setCurrencycode(c.getCurrencycode());
			v1.setCountry_order(c.getCountry_order());
			v1.setDescription(c.getDescription());
			v1.setStatus(c.getStatus());
			v1.setCountrycode(c.getCountrycode());
			
			session.save(v1);
		}
		
		session.getTransaction().commit();
		
		session.close();
	}

	public List<Salesman> getAllSalesman(String countrycode) {
		Session session=hibernateDao.getSessionFactory().openSession();
		String hql = "FROM Salesman where countrycode='"+countrycode+"'";
		Query query = session.createQuery(hql);
		List<Salesman> results = query.list();
		return results;
	}

	public void saveSalesman(List<Salesman> m, String countrycode) 
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		session.beginTransaction();
		Query q=session.createQuery("delete from Salesman where countrycode='"+countrycode+"'");
		q.executeUpdate();
		for(Salesman c:m)
		{
			Salesman v1=new Salesman();
			v1.setCountrycode(c.getCountrycode());
			v1.setEmployeecode("staticdata");
			v1.setIni(c.getIni());
			v1.setRegioncode(c.getRegioncode());
			v1.setSalesman(c.getSalesman());
			v1.setSalesmancode(c.getSalesmancode());
			v1.setStatus(c.getStatus());
			v1.setName("static");
			v1.setOffice("static");
			v1.setTitleoffice("static");
			v1.setVm("static");
			v1.setVm2("static");
			session.save(v1);
			//UpdatedCode
			String updateregioninytdbookingregion="update YtdBookingRegion set region='"+c.getRegioncode()+"' where salesmancode='"+c.getSalesmancode()+"'";
			SQLQuery updateregion  =session.createSQLQuery(updateregioninytdbookingregion);
			updateregion.executeUpdate();
			
			String updateregioninytdbooking="update YtdBooking set regioncode='"+c.getRegioncode()+"' where salesmancode='"+c.getSalesmancode()+"'";
			SQLQuery updateregioncodeinytdbooking  =session.createSQLQuery(updateregioninytdbooking);
			updateregioncodeinytdbooking.executeUpdate();
		}
		
		session.getTransaction().commit();
		
		session.close();
	}

	public void saveTaxMaster(String countrycode, ArrayList<TaxMasterBean> al)
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		session.beginTransaction();
		Query q=session.createQuery("delete from TaxMaster where countrycode='"+countrycode+"'");
		q.executeUpdate();
		for(TaxMasterBean c:al)
		{
			TaxMaster v1=new 	TaxMaster();
			v1.setCountrycode(countrycode);
			v1.setPercentage(c.getPercentage());
			v1.setTaxname(c.getTax());
			 
			
			session.save(v1);
		}
		
		session.getTransaction().commit();
		
		session.close();

		
	}

	public void saveIndustryMaster(ArrayList<IndustryBean> al)
	{
	
		
		Session session=hibernateDao.getSessionFactory().openSession();
		session.beginTransaction();
		Query q=session.createQuery("delete from Industry ");
		q.executeUpdate();
		for(IndustryBean c:al)
		{
			Industry v1=new 	Industry();
			 v1.setTotalIndustryname(c.getIndustryName());
			 v1.setIndustrycode(c.getIndustryCode());
			 
			
			session.save(v1);
		}
		
		session.getTransaction().commit();
		
		session.close();
		
		
		
		this.updatebooking();
		
		this.testindustry();
		
	 	 

		
	}

	public void saveApplication(ArrayList<ApplicationBean> al)
	{
		Session session=hibernateDao.getSessionFactory().openSession();
		session.beginTransaction();
		Query q=session.createQuery("delete from Application ");
		q.executeUpdate();
		for(ApplicationBean c:al)
		{
			Application v1=new 	Application();
			 v1.setApplication(c.getApplicationName());
			 v1.setApplicationcode(c.getApplicationCode());
			 
			
			session.save(v1);
		}
		
		session.getTransaction().commit();
		
		session.close();
		
	this.updatebooking();
		
	this.testindustry();
	 	
}
	
	
	
	public void  updatebooking()
	{
		
		Session session2=hibernateDao.getSessionFactory().openSession();
		
		//for Application------------------------------------>
		
		String  applicationquery="from Application";
		Query applicationquery1 =session2.createQuery(applicationquery);
		List<Application> applicationdata=applicationquery1.list();	
		TreeMap<String,String> applicationcodeandapplicationname= new TreeMap<String,String>();
		for(Application a:applicationdata)
		{
			applicationcodeandapplicationname.put(a.getApplicationcode(),a.getApplication());
			
		}
		
		
		//for Industry------------------------------------------------>
		String IndustryQuery="from Industry";
		Query industry1=session2.createQuery(IndustryQuery);
		List<Industry> industry2=industry1.list();
		TreeMap<String,String> industrycodeindustryname=new TreeMap<String,String>();
		for(Industry i1:industry2)
		{
			industrycodeindustryname.put(i1.getIndustrycode(),i1.getTotalIndustryname());
			
		}
		
		
		
		//for Industry Sector-------------------------------------------------->
		
		String Industrysector="from IndustrySector";
		Query industrysectorquery=session2.createQuery(Industrysector);
		List<IndustrySector> industrysector1=industrysectorquery.list();
		TreeMap<String,String> industrysectornameandindustrysectorcode=new TreeMap<String,String>();
		
		
		for(IndustrySector is1:industrysector1)
		{
			industrysectornameandindustrysectorcode.put(is1.getIndustrysectorcode(),is1.getIndustrysector());
		}
		
		
		
		
		
		
		
		String appcodequery="select distinct appcode from YtdBooking";
		Query appcodequerydata =session2.createQuery(appcodequery);
		
		List<String> onlyappcodequerydata=appcodequerydata.list();
		
		for(String a:onlyappcodequerydata)
		{
			
			if(a.equalsIgnoreCase("NA"))
			{
				String  industrycode="NoAppcode";
				String industryname="NoAppcode";
				String appcode="NoAppcode";
				String applicationname ="NoAppcode";
				String industrysector="NoAppcode";
				String industrysectorcode="NoAppcode";
				
				String updatingindustryapplicationinytdbooking="update YtdBooking  set applicationcode='"+appcode+"',application='"+applicationname+"',industry='"+industryname+"',industrycode='"+industrycode+"',industrysectorcode='"+industrysectorcode+"',industrysector ='"+industrysector+"' where  appcode='"+a+"'";
				Query queryupdatingindustryapplicationinytdbooking =session2.createQuery(updatingindustryapplicationinytdbooking);
				queryupdatingindustryapplicationinytdbooking.executeUpdate();
				
			}
			
			else
			{
				
		 
			String appcode="No Application Code";
			if(a.length()>=3)
			{
				appcode=a.substring(a.length() - 1);
			}
			String industrycode="No Industry Code";
			if(a.length()>=2)
			{
		   industrycode=a.substring (0,2);
		    }
		  String industrysectorcode=a.substring (0,1);
		  
			 
			 
				String applicationname1=(String)applicationcodeandapplicationname.get(appcode);
				String applicationname =null;
	            String industryname1=(String)industrycodeindustryname.get(industrycode);
	            String industryname=null;
	            String industrysectorname1=(String)industrysectornameandindustrysectorcode.get(industrysectorcode);
	            String industrysectorname=null;
	            if(applicationname1==null)
	            {
	            	applicationname="No Application Name";
	            }
	            
	            else
	            {
	             applicationname = applicationname1.replace("'","''");
	            	
	            }
	            
	            if(industryname1==null)
	            {
	            	industryname="NO Industry Name";
	            }
	            else
	            {
	            	industryname = industryname1.replace("'","''");
	            	
	            }
	            
	            if(industrysectorname1==null)
	            {
	            	industrysectorname="No Industry Sector Name";
	            }
	            else
	            {
	            	industrysectorname = industrysectorname1.replace("'","''");
	            }
	          
			
				 String updatingindustryapplicationinytdbooking="update YtdBooking  set applicationcode='"+appcode+"',application='"+applicationname+"',industry='"+industryname+"',industrycode='"+industrycode+"',industrysectorcode='"+industrysectorcode+"',industrysector ='"+industrysectorname+"' where  appcode='"+a+"'";
	             Query queryupdatingindustryapplicationinytdbooking =session2.createQuery(updatingindustryapplicationinytdbooking);
				  queryupdatingindustryapplicationinytdbooking.executeUpdate();
				
				
		 
			
			
		}
		
		

		}
		 
		
		

		
	}
	
	
	 
	
	
public  void testindustry() {
	
	Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	String sql="Truncate table ytdbookingIndustry";
	 SQLQuery q =session1.createSQLQuery(sql);
	 q.executeUpdate();
	 
	 Session session2=(Session)hibernateDao.getSessionFactory().openSession();
	 String sql2="INSERT INTO [dbo].[YtdBookingIndustry]([amountusd],[bookingamount],[country],[industry],[industrycode],[industrysector],[industrysectorcode],[month],[year],[migrationtime],[applicationcode],[application]) select [amountusd],[amount],[country],[industry],[industrycode],[industrysector],[industrysectorcode],[bookingmonth],[bookingyear],[migrationtime],[applicationcode],[application] from ytdbooking";
	
	
	 SQLQuery q2 =session1.createSQLQuery(sql2);
	 q2.executeUpdate();
	 
	 
	 String sql3="update YtdBookingIndustry set industrysectorcode='NA' where industrysector='NA'";
	 SQLQuery q3 =session1.createSQLQuery(sql3);
	 q3.executeUpdate();
	 
	 
	 
	 System.out.println("updtaeddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd industry");
		// TODO Auto-generated m
		// TODO Auto-generated method stub
		
	}



	
	
	
}