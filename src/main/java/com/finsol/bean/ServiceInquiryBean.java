package com.finsol.bean;



public class ServiceInquiryBean {

	private  String caseReferenceno;   	
	private  String customer;	
	private  String customerCode;	
	private  String customerStatus;	
	private  String contactno;	
	private  String telephoneno;	
	private  String address;	
	private  String description;
	private  Byte serviceLocation;
	private  String siteaddress;
	private  String personContactno;
	private  Byte attachDrawing;
	private  String  photoLocation;
	private  String customerRemarks;
	private  String inquiryDate;
	private  String imageData;
	private Boolean modifyFlag;

	public String getCaseReferenceno()
	{
		return caseReferenceno;
	}
	public void setCaseReferenceno(String caseReferenceno)
	{
		this.caseReferenceno = caseReferenceno;
	}
	public String getCustomer()
	{
		return customer;
	}
	public void setCustomer(String customer)
	{
		this.customer = customer;
	}
	public String getCustomerCode()
	{
		return customerCode;
	}
	public void setCustomerCode(String customerCode)
	{
		this.customerCode = customerCode;
	}
	public String getCustomerStatus()
	{
		return customerStatus;
	}
	public void setCustomerStatus(String customerStatus)
	{
		this.customerStatus = customerStatus;
	}
	public String getContactno()
	{
		return contactno;
	}
	public void setContactno(String contactno)
	{
		this.contactno = contactno;
	}
	public String getTelephoneno()
	{
		return telephoneno;
	}
	public void setTelephoneno(String telephoneno)
	{
		this.telephoneno = telephoneno;
	}
	public String getAddress()
	{
		return address;
	}
	public void setAddress(String address)
	{
		this.address = address;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Byte getServiceLocation()
	{
		return serviceLocation;
	}
	public void setServiceLocation(Byte serviceLocation)
	{
		this.serviceLocation = serviceLocation;
	}
	public String getSiteaddress()
	{
		return siteaddress;
	}
	public void setSiteaddress(String siteaddress)
	{
		this.siteaddress = siteaddress;
	}
	public String getPersonContactno()
	{
		return personContactno;
	}
	public void setPersonContactno(String personContactno)
	{
		this.personContactno = personContactno;
	}
	public Byte getAttachDrawing()
	{
		return attachDrawing;
	}
	public void setAttachDrawing(Byte attachDrawing)
	{
		this.attachDrawing = attachDrawing;
	}
	public String getPhotoLocation()
	{
		return photoLocation;
	}
	public void setPhotoLocation(String photoLocation)
	{
		this.photoLocation = photoLocation;
	}
	public String getCustomerRemarks()
	{
		return customerRemarks;
	}
	public void setCustomerRemarks(String customerRemarks)
	{
		this.customerRemarks = customerRemarks;
	}
	public String getImageData()
	{
		return imageData;
	}
	public void setImageData(String imageData)
	{
		this.imageData = imageData;
	}
	public String getInquiryDate()
	{
		return inquiryDate;
	}
	public void setInquiryDate(String inquiryDate)
	{
		this.inquiryDate = inquiryDate;
	}
	public Boolean getModifyFlag()
	{
		return modifyFlag;
	}
	public void setModifyFlag(Boolean modifyFlag)
	{
		this.modifyFlag = modifyFlag;
	}
	
	
}
