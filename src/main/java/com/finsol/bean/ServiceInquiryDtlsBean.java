package com.finsol.bean;

public class ServiceInquiryDtlsBean {	
	
	private  String caseReferenceno; 
	private  Integer id;
	private  String gearHeadslno;	
	private  String motorslno;	
	private  String shimfgno;	
	private  Byte serviceInquiryFor;
	private  Double quantity;	
	private  Byte isSelectedModel;	
	private  String selectedModel;	
	private  Byte brake;	
	private  Byte isModelUnkonown;	
	private  String modelUnkonown;		
	private  Byte brakeVoltage;		
	private  String application;	
	private  Byte rotationDirection;	
	private  String accessories;
	private Integer slno;
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getCaseReferenceno()
	{
		return caseReferenceno;
	}
	public void setCaseReferenceno(String caseReferenceno)
	{
		this.caseReferenceno = caseReferenceno;
	}
	public String getGearHeadslno()
	{
		return gearHeadslno;
	}
	public void setGearHeadslno(String gearHeadslno)
	{
		this.gearHeadslno = gearHeadslno;
	}
	public String getMotorslno()
	{
		return motorslno;
	}
	public void setMotorslno(String motorslno)
	{
		this.motorslno = motorslno;
	}
	public String getShimfgno()
	{
		return shimfgno;
	}
	public void setShimfgno(String shimfgno)
	{
		this.shimfgno = shimfgno;
	}
	public Byte getServiceInquiryFor()
	{
		return serviceInquiryFor;
	}
	public void setServiceInquiryFor(Byte serviceInquiryFor)
	{
		this.serviceInquiryFor = serviceInquiryFor;
	}
	public Double getQuantity()
	{
		return quantity;
	}
	public void setQuantity(Double quantity)
	{
		this.quantity = quantity;
	}
	public Byte getIsSelectedModel()
	{
		return isSelectedModel;
	}
	public void setIsSelectedModel(Byte isSelectedModel)
	{
		this.isSelectedModel = isSelectedModel;
	}
	public String getSelectedModel()
	{
		return selectedModel;
	}
	public void setSelectedModel(String selectedModel)
	{
		this.selectedModel = selectedModel;
	}
	public Byte getBrake()
	{
		return brake;
	}
	public void setBrake(Byte brake)
	{
		this.brake = brake;
	}
	public Byte getIsModelUnkonown()
	{
		return isModelUnkonown;
	}
	public void setIsModelUnkonown(Byte isModelUnkonown)
	{
		this.isModelUnkonown = isModelUnkonown;
	}
	public String getModelUnkonown()
	{
		return modelUnkonown;
	}
	public void setModelUnkonown(String modelUnkonown)
	{
		this.modelUnkonown = modelUnkonown;
	}
	public Byte getBrakeVoltage()
	{
		return brakeVoltage;
	}
	public void setBrakeVoltage(Byte brakeVoltage)
	{
		this.brakeVoltage = brakeVoltage;
	}
	public String getApplication()
	{
		return application;
	}
	public void setApplication(String application)
	{
		this.application = application;
	}
	public Byte getRotationDirection()
	{
		return rotationDirection;
	}
	public void setRotationDirection(Byte rotationDirection)
	{
		this.rotationDirection = rotationDirection;
	}
	public String getAccessories()
	{
		return accessories;
	}
	public void setAccessories(String accessories)
	{
		this.accessories = accessories;
	}
	public Integer getSlno()
	{
		return slno;
	}
	public void setSlno(Integer slno)
	{
		this.slno = slno;
	}
	
	
	
	

}
