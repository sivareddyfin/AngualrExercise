package com.finsol.bean;

public class TestReportDtlsBean1 
{
	private  String slno1;
	private  String motorslno1;
	private  Double	measuredGearHeadWeight1;
	private  Double	measuredGreaseWeight1;
	private  Double	measuredFinalWeight1;
	private Byte airPressureTest1;
	private  Double measuredIpVoltage1; 
	private  Double measuredAmpere1; 
	private  Double measuredIpPower1;
	private  Double measuredOpPower1;
	private  Double measuredTemperatureInitial1;
	private  Double measuredTemperatureFinal1;
	private  Double measuredSoundLevelcw1;
	private  Double measuredSoundLevelccw1;
	private Byte confirmbbsSize1;
	private Byte measuredThermistorCablePresent1;
	private Byte measuredSpaceHeaterCablePresent1;
	private  Double overallVibration1;
	private Byte  concentricity1;
	private Double  measuredOpSpeed1;
	
	
	public String getSlno1() {
		return slno1;
	}
	public void setSlno1(String slno1) {
		this.slno1 = slno1;
	}
	public String getMotorslno1() {
		return motorslno1;
	}
	public void setMotorslno1(String motorslno1) {
		this.motorslno1 = motorslno1;
	}


	public Double getMeasuredGearHeadWeight1() {
		return measuredGearHeadWeight1;
	}
	public void setMeasuredGearHeadWeight1(Double measuredGearHeadWeight1) {
		this.measuredGearHeadWeight1 = measuredGearHeadWeight1;
	}
	public Double getMeasuredGreaseWeight1() {
		return measuredGreaseWeight1;
	}
	public void setMeasuredGreaseWeight1(Double measuredGreaseWeight1) {
		this.measuredGreaseWeight1 = measuredGreaseWeight1;
	}
	public Double getMeasuredFinalWeight1() {
		return measuredFinalWeight1;
	}
	public void setMeasuredFinalWeight1(Double measuredFinalWeight1) {
		this.measuredFinalWeight1 = measuredFinalWeight1;
	}
	public Byte getAirPressureTest1() {
		return airPressureTest1;
	}
	public void setAirPressureTest1(Byte airPressureTest1) {
		this.airPressureTest1 = airPressureTest1;
	}
	public Double getMeasuredIpVoltage1() {
		return measuredIpVoltage1;
	}
	public void setMeasuredIpVoltage1(Double measuredIpVoltage1) {
		this.measuredIpVoltage1 = measuredIpVoltage1;
	}
	public Double getMeasuredAmpere1() {
		return measuredAmpere1;
	}
	public void setMeasuredAmpere1(Double measuredAmpere1) {
		this.measuredAmpere1 = measuredAmpere1;
	}
	public Double getMeasuredIpPower1() {
		return measuredIpPower1;
	}
	public void setMeasuredIpPower1(Double measuredIpPower1) {
		this.measuredIpPower1 = measuredIpPower1;
	}
	public Double getMeasuredOpPower1() {
		return measuredOpPower1;
	}
	public void setMeasuredOpPower1(Double measuredOpPower1) {
		this.measuredOpPower1 = measuredOpPower1;
	}
	public Double getMeasuredTemperatureInitial1() {
		return measuredTemperatureInitial1;
	}
	public void setMeasuredTemperatureInitial1(Double measuredTemperatureInitial1) {
		this.measuredTemperatureInitial1 = measuredTemperatureInitial1;
	}
	public Double getMeasuredTemperatureFinal1() {
		return measuredTemperatureFinal1;
	}
	public void setMeasuredTemperatureFinal1(Double measuredTemperatureFinal1) {
		this.measuredTemperatureFinal1 = measuredTemperatureFinal1;
	}
	public Double getMeasuredSoundLevelcw1() {
		return measuredSoundLevelcw1;
	}
	public void setMeasuredSoundLevelcw1(Double measuredSoundLevelcw1) {
		this.measuredSoundLevelcw1 = measuredSoundLevelcw1;
	}
	public Double getMeasuredSoundLevelccw1() {
		return measuredSoundLevelccw1;
	}
	public void setMeasuredSoundLevelccw1(Double measuredSoundLevelccw1) {
		this.measuredSoundLevelccw1 = measuredSoundLevelccw1;
	}
	public Byte getConfirmbbsSize1() {
		return confirmbbsSize1;
	}
	public void setConfirmbbsSize1(Byte confirmbbsSize1) {
		this.confirmbbsSize1 = confirmbbsSize1;
	}
	public Byte getMeasuredThermistorCablePresent1() {
		return measuredThermistorCablePresent1;
	}
	public void setMeasuredThermistorCablePresent1(
			Byte measuredThermistorCablePresent1) {
		this.measuredThermistorCablePresent1 = measuredThermistorCablePresent1;
	}
	public Byte getMeasuredSpaceHeaterCablePresent1() {
		return measuredSpaceHeaterCablePresent1;
	}
	public void setMeasuredSpaceHeaterCablePresent1(
			Byte measuredSpaceHeaterCablePresent1) {
		this.measuredSpaceHeaterCablePresent1 = measuredSpaceHeaterCablePresent1;
	}
	public Double getOverallVibration1() {
		return overallVibration1;
	}
	public void setOverallVibration1(Double overallVibration1) {
		this.overallVibration1 = overallVibration1;
	}
	public Byte getConcentricity1() {
		return concentricity1;
	}
	public void setConcentricity1(Byte concentricity1) {
		this.concentricity1 = concentricity1;
	}
	public Double getMeasuredOpSpeed1() {
		return measuredOpSpeed1;
	}
	public void setMeasuredOpSpeed1(Double measuredOpSpeed1) {
		this.measuredOpSpeed1 = measuredOpSpeed1;
	}
	
	
	

}
