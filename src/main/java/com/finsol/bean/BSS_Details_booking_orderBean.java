package com.finsol.bean;

public class BSS_Details_booking_orderBean {
	
	 
	 
	private String territory;
	
	private String salesmancode;
	
	private String salesman;
	
	private String customercode;

	private String customer;
	
	private String  startdate;
	
	private String product;

	private String entrydate;
	
	private	String salesorder;
	
	private	String u_p_r;
	
	private String  amount_usd;
	
	private String app_code;

	private Integer year;
	
	private Byte status;

	public String getTerritory() {
		return territory;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public String getSalesmancode() {
		return salesmancode;
	}

	public void setSalesmancode(String salesmancode) {
		this.salesmancode = salesmancode;
	}

	public String getSalesman() {
		return salesman;
	}

	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}

	public String getCustomercode() {
		return customercode;
	}

	public void setCustomercode(String customercode) {
		this.customercode = customercode;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getEntrydate() {
		return entrydate;
	}

	public void setEntrydate(String entrydate) {
		this.entrydate = entrydate;
	}

	public String getSalesorder() {
		return salesorder;
	}

	public void setSalesorder(String salesorder) {
		this.salesorder = salesorder;
	}

	public String getU_p_r() {
		return u_p_r;
	}

	public void setU_p_r(String u_p_r) {
		this.u_p_r = u_p_r;
	}

	public String getAmount_usd() {
		return amount_usd;
	}

	public void setAmount_usd(String amount_usd) {
		this.amount_usd = amount_usd;
	}

	public String getApp_code() {
		return app_code;
	}

	public void setApp_code(String app_code) {
		this.app_code = app_code;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
	 	
}
