package com.finsol.bean;


public class ProductionSchedulerBean {
	
	private Integer id;	
	private String assemblyDate;
	private String dueDate;
	private String completedDate;	
	private String reqDeliveryDate;
	private String applicationCode;
	private String pid;
	private String pline;
	private String upr;
	private String salesOrder;
	private String name;
	private String description;
	private String contractno;
	private String customerCode;
	private Byte country;	
	private String modelno;
	private String serialno;
	private Integer score;	
	private Integer totalScore;	
	private Integer quantity;
	private String testedBy;
	private String prodStatus; //  New/scheduled / Modified / closed / cancelled
	private String lineItemStatus; //  valid/invalid
	private String changeItem; //  New/Change/Cancel
	private String changedItem1;
	private String changedItem2;
	private String changedItem3;
	private String changedItem4;
	private String startTime;
	private String endTime;
	private String checkPacking;
	private Boolean modifyFlag;	
	private Integer completedQuantity;
		
		
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAssemblyDate() {
		return assemblyDate;
	}
	public void setAssemblyDate(String assemblyDate) {
		this.assemblyDate = assemblyDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getCompletedDate() {
		return completedDate;
	}
	public void setCompletedDate(String completedDate) {
		this.completedDate = completedDate;
	}

	public String getReqDeliveryDate() {
		return reqDeliveryDate;
	}
	public void setReqDeliveryDate(String reqDeliveryDate) {
		this.reqDeliveryDate = reqDeliveryDate;
	}
	public String getApplicationCode() {
		return applicationCode;
	}
	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getPline() {
		return pline;
	}
	public void setPline(String pline) {
		this.pline = pline;
	}
	public String getUpr() {
		return upr;
	}
	public void setUpr(String upr) {
		this.upr = upr;
	}
	public String getSalesOrder() {
		return salesOrder;
	}
	public void setSalesOrder(String salesOrder) {
		this.salesOrder = salesOrder;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContractno() {
		return contractno;
	}
	public void setContractno(String contractno) {
		this.contractno = contractno;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public Byte getCountry() {
		return country;
	}
	public void setCountry(Byte country) {
		this.country = country;
	}
	public String getModelno() {
		return modelno;
	}
	public void setModelno(String modelno) {
		this.modelno = modelno;
	}
	public String getSerialno() {
		return serialno;
	}
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getTestedBy() {
		return testedBy;
	}
	public void setTestedBy(String testedBy) {
		this.testedBy = testedBy;
	}
	public String getProdStatus() {
		return prodStatus;
	}
	public void setProdStatus(String prodStatus) {
		this.prodStatus = prodStatus;
	}
	public String getLineItemStatus() {
		return lineItemStatus;
	}
	public void setLineItemStatus(String lineItemStatus) {
		this.lineItemStatus = lineItemStatus;
	}
	public String getChangeItem() {
		return changeItem;
	}
	public void setChangeItem(String changeItem) {
		this.changeItem = changeItem;
	}
	public String getChangedItem1() {
		return changedItem1;
	}
	public void setChangedItem1(String changedItem1) {
		this.changedItem1 = changedItem1;
	}
	public String getChangedItem2() {
		return changedItem2;
	}
	public void setChangedItem2(String changedItem2) {
		this.changedItem2 = changedItem2;
	}
	public String getChangedItem3() {
		return changedItem3;
	}
	public void setChangedItem3(String changedItem3) {
		this.changedItem3 = changedItem3;
	}
	public String getChangedItem4() {
		return changedItem4;
	}
	public void setChangedItem4(String changedItem4) {
		this.changedItem4 = changedItem4;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getCheckPacking() {
		return checkPacking;
	}
	public void setCheckPacking(String checkPacking) {
		this.checkPacking = checkPacking;
	}
	public Boolean getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(Boolean modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getCompletedQuantity() {
		return completedQuantity;
	}
	public void setCompletedQuantity(Integer completedQuantity) {
		this.completedQuantity = completedQuantity;
	}

	

}
