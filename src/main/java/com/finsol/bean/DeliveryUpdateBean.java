package com.finsol.bean;

public class DeliveryUpdateBean 
{
	
	private String customer; 	
	private String quotationno;
	private String  quotationDate;
	private String  exporderDate;	
	private String  expdelDate;
	private String  projdelDate;
	private String salesMan;

	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getQuotationno() {
		return quotationno;
	}
	public void setQuotationno(String quotationno) {
		this.quotationno = quotationno;
	}
	public String getQuotationDate() {
		return quotationDate;
	}
	public void setQuotationDate(String quotationDate) {
		this.quotationDate = quotationDate;
	}
	public String getExporderDate() {
		return exporderDate;
	}
	public void setExporderDate(String exporderDate) {
		this.exporderDate = exporderDate;
	}
	public String getExpdelDate() {
		return expdelDate;
	}
	public void setExpdelDate(String expdelDate) {
		this.expdelDate = expdelDate;
	}
	public String getProjdelDate() {
		return projdelDate;
	}
	public void setProjdelDate(String projdelDate) {
		this.projdelDate = projdelDate;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	
	
	
}
