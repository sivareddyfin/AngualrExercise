package com.finsol.bean;


public class SalesReportBean {
	
	
	private Integer seqno;		
	private String salesrptrefno;	//Salesmancode+Sequence/Year	
	private String salesMan;	
	private String customer;	
	private String  srdatetime;	
	private Byte activity;	
	private String callnotes;	
	private String remarks;	
	private String assignedto;	
	private String quoteOrRefno;	
	private Byte caseOrQuotaion;
	private String customerStatus;
	private String contactName;
	private String salesManCode;
	
	public Integer getSeqno() {
		return seqno;
	}
	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}
	public String getSalesrptrefno() {
		return salesrptrefno;
	}
	public void setSalesrptrefno(String salesrptrefno) {
		this.salesrptrefno = salesrptrefno;
	}

	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getSrdatetime() {
		return srdatetime;
	}
	public void setSrdatetime(String srdatetime) {
		this.srdatetime = srdatetime;
	}
	public Byte getActivity() {
		return activity;
	}
	public void setActivity(Byte activity) {
		this.activity = activity;
	}
	public String getCallnotes() {
		return callnotes;
	}
	public void setCallnotes(String callnotes) {
		this.callnotes = callnotes;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getAssignedto() {
		return assignedto;
	}
	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}

	public Byte getCaseOrQuotaion() {
		return caseOrQuotaion;
	}
	public void setCaseOrQuotaion(Byte caseOrQuotaion) {
		this.caseOrQuotaion = caseOrQuotaion;
	}
	public String getQuoteOrRefno() {
		return quoteOrRefno;
	}
	public void setQuoteOrRefno(String quoteOrRefno) {
		this.quoteOrRefno = quoteOrRefno;
	}
	public String getSalesMan()
	{
		return salesMan;
	}
	public void setSalesMan(String salesMan)
	{
		this.salesMan = salesMan;
	}
	public String getCustomerStatus() {
		return customerStatus;
	}
	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}
	public String getContactName()
	{
		return contactName;
	}
	public void setContactName(String contactName)
	{
		this.contactName = contactName;
	}
	public String getSalesManCode()
	{
		return salesManCode;
	}
	public void setSalesManCode(String salesManCode)
	{
		this.salesManCode = salesManCode;
	}	

	
	
	
}
