package com.finsol.bean;


public class QuotationFormBean {
	private Integer qid;
	public Integer getQid() {
		return qid;
	}
	public void setQid(Integer qid) {
		this.qid = qid;
	}
	private String  qtnType;
	private String  qtnNo;
	private String  custName;
	private String  dateQuote;
	private String  currency;
	private String  attention;
	private String  custCode;
	private String  dateValid;
	private String  tax;
	private String  copy;
	private String  country;
	private String  salesMan;
	private String  taxPercent;
	private String  telephone;
	private String  custRef;
	private String  salesManCode;
	private String  discount;
	private String  email;
	public Integer getModifiedstatus()
	{
		return modifiedstatus;
	}
	public void setModifiedstatus(Integer modifiedstatus)
	{
		this.modifiedstatus = modifiedstatus;
	}
	private String  payment;
	private String  salesTerm;
	private String  extPercent;
	private String  item;
	private String  showCost;
	private String  subject;
	private String  application;
	private String  industry;
	
	private String  freight;
	private String  freightTotal;
	private String  courier;
	private String  courierTotal;
	private Integer modifiedstatus;
	
	
	
	public String getFreight() {
		return freight;
	}
	public void setFreight(String freight) {
		this.freight = freight;
	}
	public String getFreightTotal() {
		return freightTotal;
	}
	public void setFreightTotal(String freightTotal) {
		this.freightTotal = freightTotal;
	}
	public String getCourier() {
		return courier;
	}
	public void setCourier(String courier) {
		this.courier = courier;
	}
	public String getCourierTotal() {
		return courierTotal;
	}
	public void setCourierTotal(String courierTotal) {
		this.courierTotal = courierTotal;
	}
	public String getQtnType() {
		return qtnType;
	}
	public void setQtnType(String qtnType) {
		this.qtnType = qtnType;
	}
	public String getQtnNo() {
		return qtnNo;
	}
	public void setQtnNo(String qtnNo) {
		this.qtnNo = qtnNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getDateQuote() {
		return dateQuote;
	}
	public void setDateQuote(String dateQuote) {
		this.dateQuote = dateQuote;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAttention() {
		return attention;
	}
	public void setAttention(String attention) {
		this.attention = attention;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getDateValid() {
		return dateValid;
	}
	public void setDateValid(String dateValid) {
		this.dateValid = dateValid;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getCopy() {
		return copy;
	}
	public void setCopy(String copy) {
		this.copy = copy;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	public String getTaxPercent() {
		return taxPercent;
	}
	public void setTaxPercent(String taxPercent) {
		this.taxPercent = taxPercent;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getCustRef() {
		return custRef;
	}
	public void setCustRef(String custRef) {
		this.custRef = custRef;
	}
	public String getSalesManCode() {
		return salesManCode;
	}
	public void setSalesManCode(String salesManCode) {
		this.salesManCode = salesManCode;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getSalesTerm() {
		return salesTerm;
	}
	public void setSalesTerm(String salesTerm) {
		this.salesTerm = salesTerm;
	}
	public String getExtPercent() {
		return extPercent;
	}
	public void setExtPercent(String extPercent) {
		this.extPercent = extPercent;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getShowCost() {
		return showCost;
	}
	public void setShowCost(String showCost) {
		this.showCost = showCost;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}

	
	
	private String  qtnProgress;
	public String getQtnProgress() {
		return qtnProgress;
	}
	public void setQtnProgress(String qtnProgress) {
		this.qtnProgress = qtnProgress;
	}
	public String getSalesPerson() {
		return salesPerson;
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	public String getEmailAddr() {
		return emailAddr;
	}
	public void setEmailAddr(String emailAddr) {
		this.emailAddr = emailAddr;
	}
	public String getPaymentTerm() {
		return paymentTerm;
	}
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	public String getEstPercent() {
		return estPercent;
	}
	public void setEstPercent(String estPercent) {
		this.estPercent = estPercent;
	}
	public String getAssigned() {
		return assigned;
	}
	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}
	public String getCaseRef() {
		return caseRef;
	}
	public void setCaseRef(String caseRef) {
		this.caseRef = caseRef;
	}
	private String  salesPerson;
	private String  emailAddr;
	private String  paymentTerm;
	private String  estPercent;
	private String  assigned;
	private String  caseRef;
	private String  remarks;
	private String  profit;
	private String  modelTerm;
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getProfit() {
		return profit;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	public String getModelTerm() {
		return modelTerm;
	}
	public void setModelTerm(String modelTerm) {
		this.modelTerm = modelTerm;
	}


	 private String fax;
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}

	
	
	private String subTotal;
	
	 private String grandTotal;
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	 
	private String addedTax;
	public String getAddedTax() {
		return addedTax;
	}
	public void setAddedTax(String addedTax) {
		this.addedTax = addedTax;
	}
	
	
	private String salesmanPhNo;
	private String createdBy;
	public String getSalesmanPhNo()
	{
		return salesmanPhNo;
	}
	public void setSalesmanPhNo(String salesmanPhNo)
	{
		this.salesmanPhNo = salesmanPhNo;
	}
	public String getCreatedBy()
	{
		return createdBy;
	}
	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
		
		
	}
	
	private String deliveryTime;
	private String deliveryTerms;
	private String location;
	private String currencyCon;
	private String paymentTermCon;
	private String validityCon;
	public String getDeliveryTime()
	{
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime)
	{
		this.deliveryTime = deliveryTime;
	}
	public String getDeliveryTerms()
	{
		return deliveryTerms;
	}
	public void setDeliveryTerms(String deliveryTerms)
	{
		this.deliveryTerms = deliveryTerms;
	}
	public String getLocation()
	{
		return location;
	}
	public void setLocation(String location)
	{
		this.location = location;
	}
	public String getCurrencyCon()
	{
		return currencyCon;
	}
	public void setCurrencyCon(String currencyCon)
	{
		this.currencyCon = currencyCon;
	}
	public String getPaymentTermCon()
	{
		return paymentTermCon;
	}
	public void setPaymentTermCon(String paymentTermCon)
	{
		this.paymentTermCon = paymentTermCon;
	}
	public String getValidityCon()
	{
		return validityCon;
	}
	public void setValidityCon(String validityCon)
	{
		this.validityCon = validityCon;
	}
	
	
	
	
	
	
	
	
	
	 

}
