package com.finsol.bean;

public class QAIssueBean {
	
	private String caseReferenceNumber; 	
	private  String description;	
	private  String shimfgno;	
	private  String[] qaIssuesFor;
	private  String[] actionRequired;
	private  String scaClaimno;
	private  String scaDate;
	private  String scaOrderno;
	private  String model;
	private  String serialno;
	private  String possibleReasonsRemarks;
	private  Byte attachDrawing;	
	private 	Boolean modifyFlag;
	private  String imageData;
	//private  String gearHeadslno;	
	//private  String motorslno;	
	//private  String customer;	
	//private  String customerCode;	
	//private  String contactno;	
	//private  String telephoneno;	
	//private  String address;	
	//private  String status;
	//private  String title;
	//private  String pageitemno;
	//private  String shipInvoiceno;
	//private  String shipmentDate;
	//private  String recievingDate;	
	//private  String blorawbno;	
	//private  String vesselOrFlightno;	
	//private  Integer qtyDamazed;	
	//private  Integer outOf;	
	//private  String qaIssuesDescription;	
	//private  String shimfgno1;
	//private  Byte qualityAssurance;
	//private  Boolean modifyFlag;
	
	public String getCaseReferenceNumber()
	{
		return caseReferenceNumber;
	}
	public void setCaseReferenceNumber(String caseReferenceNumber)
	{
		this.caseReferenceNumber = caseReferenceNumber;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getShimfgno()
	{
		return shimfgno;
	}
	public void setShimfgno(String shimfgno)
	{
		this.shimfgno = shimfgno;
	}
	public String[] getQaIssuesFor()
	{
		return qaIssuesFor;
	}
	public void setQaIssuesFor(String[] qaIssuesFor)
	{
		this.qaIssuesFor = qaIssuesFor;
	}
	public String[] getActionRequired()
	{
		return actionRequired;
	}
	public void setActionRequired(String[] actionRequired)
	{
		this.actionRequired = actionRequired;
	}
	public String getScaClaimno()
	{
		return scaClaimno;
	}
	public void setScaClaimno(String scaClaimno)
	{
		this.scaClaimno = scaClaimno;
	}
	public String getScaDate()
	{
		return scaDate;
	}
	public void setScaDate(String scaDate)
	{
		this.scaDate = scaDate;
	}
	public String getScaOrderno()
	{
		return scaOrderno;
	}
	public void setScaOrderno(String scaOrderno)
	{
		this.scaOrderno = scaOrderno;
	}
	public String getModel()
	{
		return model;
	}
	public void setModel(String model)
	{
		this.model = model;
	}
	public String getSerialno()
	{
		return serialno;
	}
	public void setSerialno(String serialno)
	{
		this.serialno = serialno;
	}
	public String getPossibleReasonsRemarks()
	{
		return possibleReasonsRemarks;
	}
	public void setPossibleReasonsRemarks(String possibleReasonsRemarks)
	{
		this.possibleReasonsRemarks = possibleReasonsRemarks;
	}
	public Byte getAttachDrawing()
	{
		return attachDrawing;
	}
	public void setAttachDrawing(Byte attachDrawing)
	{
		this.attachDrawing = attachDrawing;
	}
	public Boolean getModifyFlag()
	{
		return modifyFlag;
	}
	public void setModifyFlag(Boolean modifyFlag)
	{
		this.modifyFlag = modifyFlag;
	}
	public String getImageData()
	{
		return imageData;
	}
	public void setImageData(String imageData)
	{
		this.imageData = imageData;
	}

	
	

}
