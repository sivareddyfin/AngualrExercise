package com.finsol.bean;

public class TestReportDtlsBean5
{
	private  String slno5;
	private  String motorslno5;
	private  String model5;
	private  Double	measuredGearHeadWeight5;
	private  Double	measuredGreaseWeight5;
	private  Double	measuredFinalWeight5;
	private Byte airPressureTest5;
	private  Double measuredIpVoltage5; 
	private  Double measuredAmpere5; 
	private  Double measuredIpPower5;
	private  Double measuredOpPower5;
	private  Double measuredTemperatureInitial5;
	private  Double measuredTemperatureFinal5;
	private  Double measuredSoundLevelcw5;
	private  Double measuredSoundLevelccw5;
	private Byte confirmbbsSize5;
	private Byte measuredThermistorCablePresent5;
	private Byte measuredSpaceHeaterCablePresent5;
	private  Double overallVibration5;
	private Byte  concentricity5;
	private Double  measuredOpSpeed5;
	
	
	public String getSlno5() {
		return slno5;
	}
	public void setSlno5(String slno5) {
		this.slno5 = slno5;
	}
	public String getMotorslno5() {
		return motorslno5;
	}
	public void setMotorslno5(String motorslno5) {
		this.motorslno5 = motorslno5;
	}
	public String getModel5() {
		return model5;
	}
	public void setModel5(String model5) {
		this.model5 = model5;
	}

	
	public Double getMeasuredGearHeadWeight5() {
		return measuredGearHeadWeight5;
	}
	public void setMeasuredGearHeadWeight5(Double measuredGearHeadWeight5) {
		this.measuredGearHeadWeight5 = measuredGearHeadWeight5;
	}
	public Double getMeasuredGreaseWeight5() {
		return measuredGreaseWeight5;
	}
	public void setMeasuredGreaseWeight5(Double measuredGreaseWeight5) {
		this.measuredGreaseWeight5 = measuredGreaseWeight5;
	}
	public Double getMeasuredFinalWeight5() {
		return measuredFinalWeight5;
	}
	public void setMeasuredFinalWeight5(Double measuredFinalWeight5) {
		this.measuredFinalWeight5 = measuredFinalWeight5;
	}
	public Byte getAirPressureTest5() {
		return airPressureTest5;
	}
	public void setAirPressureTest5(Byte airPressureTest5) {
		this.airPressureTest5 = airPressureTest5;
	}
	public Double getMeasuredIpVoltage5() {
		return measuredIpVoltage5;
	}
	public void setMeasuredIpVoltage5(Double measuredIpVoltage5) {
		this.measuredIpVoltage5 = measuredIpVoltage5;
	}
	public Double getMeasuredAmpere5() {
		return measuredAmpere5;
	}
	public void setMeasuredAmpere5(Double measuredAmpere5) {
		this.measuredAmpere5 = measuredAmpere5;
	}
	public Double getMeasuredIpPower5() {
		return measuredIpPower5;
	}
	public void setMeasuredIpPower5(Double measuredIpPower5) {
		this.measuredIpPower5 = measuredIpPower5;
	}
	public Double getMeasuredOpPower5() {
		return measuredOpPower5;
	}
	public void setMeasuredOpPower5(Double measuredOpPower5) {
		this.measuredOpPower5 = measuredOpPower5;
	}
	public Double getMeasuredTemperatureInitial5() {
		return measuredTemperatureInitial5;
	}
	public void setMeasuredTemperatureInitial5(Double measuredTemperatureInitial5) {
		this.measuredTemperatureInitial5 = measuredTemperatureInitial5;
	}
	public Double getMeasuredTemperatureFinal5() {
		return measuredTemperatureFinal5;
	}
	public void setMeasuredTemperatureFinal5(Double measuredTemperatureFinal5) {
		this.measuredTemperatureFinal5 = measuredTemperatureFinal5;
	}
	public Double getMeasuredSoundLevelcw5() {
		return measuredSoundLevelcw5;
	}
	public void setMeasuredSoundLevelcw5(Double measuredSoundLevelcw5) {
		this.measuredSoundLevelcw5 = measuredSoundLevelcw5;
	}
	public Double getMeasuredSoundLevelccw5() {
		return measuredSoundLevelccw5;
	}
	public void setMeasuredSoundLevelccw5(Double measuredSoundLevelccw5) {
		this.measuredSoundLevelccw5 = measuredSoundLevelccw5;
	}
	public Byte getConfirmbbsSize5() {
		return confirmbbsSize5;
	}
	public void setConfirmbbsSize5(Byte confirmbbsSize5) {
		this.confirmbbsSize5 = confirmbbsSize5;
	}
	public Byte getMeasuredThermistorCablePresent5() {
		return measuredThermistorCablePresent5;
	}
	public void setMeasuredThermistorCablePresent5(
			Byte measuredThermistorCablePresent5) {
		this.measuredThermistorCablePresent5 = measuredThermistorCablePresent5;
	}
	public Byte getMeasuredSpaceHeaterCablePresent5() {
		return measuredSpaceHeaterCablePresent5;
	}
	public void setMeasuredSpaceHeaterCablePresent5(
			Byte measuredSpaceHeaterCablePresent5) {
		this.measuredSpaceHeaterCablePresent5 = measuredSpaceHeaterCablePresent5;
	}
	public Double getOverallVibration5() {
		return overallVibration5;
	}
	public void setOverallVibration5(Double overallVibration5) {
		this.overallVibration5 = overallVibration5;
	}
	public Byte getConcentricity5() {
		return concentricity5;
	}
	public void setConcentricity5(Byte concentricity5) {
		this.concentricity5 = concentricity5;
	}
	public Double getMeasuredOpSpeed5() {
		return measuredOpSpeed5;
	}
	public void setMeasuredOpSpeed5(Double measuredOpSpeed5) {
		this.measuredOpSpeed5 = measuredOpSpeed5;
	}
	
	
	
	
}
