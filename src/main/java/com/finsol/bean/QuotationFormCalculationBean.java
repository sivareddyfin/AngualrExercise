package com.finsol.bean;

public class QuotationFormCalculationBean 
{
	private String  freight;
	private String  freightTotal;
	private String  courier;
	private String  courierTotal;
	public String getFreight() {
		return freight;
	}
	public void setFreight(String freight) {
		this.freight = freight;
	}
	public String getFreightTotal() {
		return freightTotal;
	}
	public void setFreightTotal(String freightTotal) {
		this.freightTotal = freightTotal;
	}
	public String getCourier() {
		return courier;
	}
	public void setCourier(String courier) {
		this.courier = courier;
	}
	public String getCourierTotal() {
		return courierTotal;
	}
	public void setCourierTotal(String courierTotal) {
		this.courierTotal = courierTotal;
	}



}
