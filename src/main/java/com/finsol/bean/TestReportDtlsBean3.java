package com.finsol.bean;

public class TestReportDtlsBean3 
{
	private  String slno3;
	private  String motorslno3;
	private  String model3;
	private  Double	measuredGearHeadWeight3;
	private  Double	measuredGreaseWeight3;
	private  Double	measuredFinalWeight3;
	private Byte airPressureTest3;
	private  Double measuredIpVoltage3; 
	private  Double measuredAmpere3; 
	private  Double measuredIpPower3;
	private  Double measuredOpPower3;
	private  Double measuredTemperatureInitial3;
	private  Double measuredTemperatureFinal3;
	private  Double measuredSoundLevelcw3;
	private  Double measuredSoundLevelccw3;
	private Byte confirmbbsSize3;
	private Byte measuredThermistorCablePresent3;
	private Byte measuredSpaceHeaterCablePresent3;
	private  Double overallVibration3;
	private Byte  concentricity3;
	private Double  measuredOpSpeed3;
	
	public String getSlno3() {
		return slno3;
	}
	public void setSlno3(String slno3) {
		this.slno3 = slno3;
	}
	public String getMotorslno3() {
		return motorslno3;
	}
	public void setMotorslno3(String motorslno3) {
		this.motorslno3 = motorslno3;
	}
	public String getModel3() {
		return model3;
	}
	public void setModel3(String model3) {
		this.model3 = model3;
	}

	public Double getMeasuredGearHeadWeight3() {
		return measuredGearHeadWeight3;
	}
	public void setMeasuredGearHeadWeight3(Double measuredGearHeadWeight3) {
		this.measuredGearHeadWeight3 = measuredGearHeadWeight3;
	}
	public Double getMeasuredGreaseWeight3() {
		return measuredGreaseWeight3;
	}
	public void setMeasuredGreaseWeight3(Double measuredGreaseWeight3) {
		this.measuredGreaseWeight3 = measuredGreaseWeight3;
	}
	public Double getMeasuredFinalWeight3() {
		return measuredFinalWeight3;
	}
	public void setMeasuredFinalWeight3(Double measuredFinalWeight3) {
		this.measuredFinalWeight3 = measuredFinalWeight3;
	}
	public Byte getAirPressureTest3() {
		return airPressureTest3;
	}
	public void setAirPressureTest3(Byte airPressureTest3) {
		this.airPressureTest3 = airPressureTest3;
	}
	public Double getMeasuredIpVoltage3() {
		return measuredIpVoltage3;
	}
	public void setMeasuredIpVoltage3(Double measuredIpVoltage3) {
		this.measuredIpVoltage3 = measuredIpVoltage3;
	}
	public Double getMeasuredAmpere3() {
		return measuredAmpere3;
	}
	public void setMeasuredAmpere3(Double measuredAmpere3) {
		this.measuredAmpere3 = measuredAmpere3;
	}
	public Double getMeasuredIpPower3() {
		return measuredIpPower3;
	}
	public void setMeasuredIpPower3(Double measuredIpPower3) {
		this.measuredIpPower3 = measuredIpPower3;
	}
	public Double getMeasuredOpPower3() {
		return measuredOpPower3;
	}
	public void setMeasuredOpPower3(Double measuredOpPower3) {
		this.measuredOpPower3 = measuredOpPower3;
	}
	public Double getMeasuredTemperatureInitial3() {
		return measuredTemperatureInitial3;
	}
	public void setMeasuredTemperatureInitial3(Double measuredTemperatureInitial3) {
		this.measuredTemperatureInitial3 = measuredTemperatureInitial3;
	}
	public Double getMeasuredTemperatureFinal3() {
		return measuredTemperatureFinal3;
	}
	public void setMeasuredTemperatureFinal3(Double measuredTemperatureFinal3) {
		this.measuredTemperatureFinal3 = measuredTemperatureFinal3;
	}
	public Double getMeasuredSoundLevelcw3() {
		return measuredSoundLevelcw3;
	}
	public void setMeasuredSoundLevelcw3(Double measuredSoundLevelcw3) {
		this.measuredSoundLevelcw3 = measuredSoundLevelcw3;
	}
	public Double getMeasuredSoundLevelccw3() {
		return measuredSoundLevelccw3;
	}
	public void setMeasuredSoundLevelccw3(Double measuredSoundLevelccw3) {
		this.measuredSoundLevelccw3 = measuredSoundLevelccw3;
	}
	public Byte getConfirmbbsSize3() {
		return confirmbbsSize3;
	}
	public void setConfirmbbsSize3(Byte confirmbbsSize3) {
		this.confirmbbsSize3 = confirmbbsSize3;
	}
	public Byte getMeasuredThermistorCablePresent3() {
		return measuredThermistorCablePresent3;
	}
	public void setMeasuredThermistorCablePresent3(
			Byte measuredThermistorCablePresent3) {
		this.measuredThermistorCablePresent3 = measuredThermistorCablePresent3;
	}
	public Byte getMeasuredSpaceHeaterCablePresent3() {
		return measuredSpaceHeaterCablePresent3;
	}
	public void setMeasuredSpaceHeaterCablePresent3(
			Byte measuredSpaceHeaterCablePresent3) {
		this.measuredSpaceHeaterCablePresent3 = measuredSpaceHeaterCablePresent3;
	}
	public Double getOverallVibration3() {
		return overallVibration3;
	}
	public void setOverallVibration3(Double overallVibration3) {
		this.overallVibration3 = overallVibration3;
	}
	public Byte getConcentricity3() {
		return concentricity3;
	}
	public void setConcentricity3(Byte concentricity3) {
		this.concentricity3 = concentricity3;
	}
	public Double getMeasuredOpSpeed3() {
		return measuredOpSpeed3;
	}
	public void setMeasuredOpSpeed3(Double measuredOpSpeed3) {
		this.measuredOpSpeed3 = measuredOpSpeed3;
	}
	
	
	
}
