package com.finsol.bean;

public class QuotationformGridBean 
{
	private Integer id;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	private String  model;
	private String  productDesp;
	private String  motorPower;
	private String  inputRPM;
	private String  ratio;
	private String  outPutRPM;
	private String  accessories;
	private String  qnty;
	private String  deliveryTime;
	private String  unitPrice;
	private String  subTotal;
	private String  partNumber;
	private String  partDesp;
	private String  serialNumber;
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getProductDesp() {
		return productDesp;
	}
	public void setProductDesp(String productDesp) {
		this.productDesp = productDesp;
	}
	public String getMotorPower() {
		return motorPower;
	}
	public void setMotorPower(String motorPower) {
		this.motorPower = motorPower;
	}
	public String getInputRPM() {
		return inputRPM;
	}
	public void setInputRPM(String inputRPM) {
		this.inputRPM = inputRPM;
	}
	public String getRatio() {
		return ratio;
	}
	public void setRatio(String ratio) {
		this.ratio = ratio;
	}
	public String getOutPutRPM() {
		return outPutRPM;
	}
	public void setOutPutRPM(String outPutRPM) {
		this.outPutRPM = outPutRPM;
	}
	public String getAccessories() {
		return accessories;
	}
	public void setAccessories(String accessories) {
		this.accessories = accessories;
	}
	public String getQnty() {
		return qnty;
	}
	public void setQnty(String qnty) {
		this.qnty = qnty;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartDesp() {
		return partDesp;
	}
	public void setPartDesp(String partDesp) {
		this.partDesp = partDesp;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	 

	
	
	 private String  sfactor;
	 private String  costPrice;
	 private String  motorDetails;
	 private String  specialFeaturs;
	 private String  perSN;
	 private String  quantity;
	 private String  totalPrice;
	public String getSfactor() {
		return sfactor;
	}
	public void setSfactor(String sfactor) {
		this.sfactor = sfactor;
	}
	public String getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}
	public String getMotorDetails() {
		return motorDetails;
	}
	public void setMotorDetails(String motorDetails) {
		this.motorDetails = motorDetails;
	}
	public String getSpecialFeaturs() {
		return specialFeaturs;
	}
	public void setSpecialFeaturs(String specialFeaturs) {
		this.specialFeaturs = specialFeaturs;
	}
	public String getPerSN() {
		return perSN;
	}
	public void setPerSN(String perSN) {
		this.perSN = perSN;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	 
	 
	 
private String region;
private	String pole;
private String phase;
private String  voltage;
private String freq;
public String getPole()
{
	return pole;
}
public void setPole(String pole)
{
	this.pole = pole;
}
 
public String getPhase()
{
	return phase;
}
public void setPhase(String phase)
{
	this.phase = phase;
}
public String getVoltage()
{
	return voltage;
}
public void setVoltage(String voltage)
{
	this.voltage = voltage;
}
public String getFreq()
{
	return freq;
}
public void setFreq(String freq)
{
	this.freq = freq;
}
 
private String appcode;
public String getAppcode()
{
	return appcode;
}
public void setAppcode(String appcode)
{
	this.appcode = appcode;
}

private String equipNo;
public String getEquipNo()
{
	return equipNo;
}
public void setEquipNo(String equipNo)
{
	this.equipNo = equipNo;
}
	




private String inPutRPM;
private String supplierRef;
public String getInPutRPM()
{
	return inPutRPM;
}
public void setInPutRPM(String inPutRPM)
{
	this.inPutRPM = inPutRPM;
}
public String getSupplierRef()
{
	return supplierRef;
}
public void setSupplierRef(String supplierRef)
{
	this.supplierRef = supplierRef;
}





}
