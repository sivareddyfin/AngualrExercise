package com.finsol.bean;


public class QAIssueDetailsBean
{
	private  String itemDescription;	
	private  Integer qty;	
	private  String partno;	
	private  Integer inPackageno;
	private  Integer id;
	private  String orderno;
	private  Boolean toClaim;
	private  Integer slno;
	public String getItemDescription()
	{
		return itemDescription;
	}
	public void setItemDescription(String itemDescription)
	{
		this.itemDescription = itemDescription;
	}
	public Integer getQty()
	{
		return qty;
	}
	public void setQty(Integer qty)
	{
		this.qty = qty;
	}
	public String getPartno()
	{
		return partno;
	}
	public void setPartno(String partno)
	{
		this.partno = partno;
	}
	public Integer getInPackageno()
	{
		return inPackageno;
	}
	public void setInPackageno(Integer inPackageno)
	{
		this.inPackageno = inPackageno;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getOrderno()
	{
		return orderno;
	}
	public void setOrderno(String orderno)
	{
		this.orderno = orderno;
	}

	public Boolean getToClaim()
	{
		return toClaim;
	}
	public void setToClaim(Boolean toClaim)
	{
		this.toClaim = toClaim;
	}
	public Integer getSlno()
	{
		return slno;
	}
	public void setSlno(Integer slno)
	{
		this.slno = slno;
	}
	
	
}
