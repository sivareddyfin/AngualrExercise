package com.finsol.bean;

public class TestReportDtlsBean2 
{
	private  String slno2;
	private  String motorslno2;
	private  String model2;
	private  Double	measuredGearHeadWeight2;
	private  Double	measuredGreaseWeight2;
	private  Double	measuredFinalWeight2;
	private Byte airPressureTest2;
	private  Double measuredIpVoltage2; 
	private  Double measuredAmpere2; 
	private  Double measuredIpPower2;
	private  Double measuredOpPower2;
	private  Double measuredTemperatureInitial2;
	private  Double measuredTemperatureFinal2;
	private  Double measuredSoundLevelcw2;
	private  Double measuredSoundLevelccw2;
	private Byte confirmbbsSize2;
	private Byte measuredThermistorCablePresent2;
	private Byte measuredSpaceHeaterCablePresent2;
	private  Double overallVibration2;
	private Byte  concentricity2;
	private Double  measuredOpSpeed2;
	
	public String getSlno2() {
		return slno2;
	}
	public void setSlno2(String slno2) {
		this.slno2 = slno2;
	}
	public String getMotorslno2() {
		return motorslno2;
	}
	public void setMotorslno2(String motorslno2) {
		this.motorslno2 = motorslno2;
	}
	public String getModel2() {
		return model2;
	}
	public void setModel2(String model2) {
		this.model2 = model2;
	}

	public Double getMeasuredGearHeadWeight2() {
		return measuredGearHeadWeight2;
	}
	public void setMeasuredGearHeadWeight2(Double measuredGearHeadWeight2) {
		this.measuredGearHeadWeight2 = measuredGearHeadWeight2;
	}
	public Double getMeasuredGreaseWeight2() {
		return measuredGreaseWeight2;
	}
	public void setMeasuredGreaseWeight2(Double measuredGreaseWeight2) {
		this.measuredGreaseWeight2 = measuredGreaseWeight2;
	}
	public Double getMeasuredFinalWeight2() {
		return measuredFinalWeight2;
	}
	public void setMeasuredFinalWeight2(Double measuredFinalWeight2) {
		this.measuredFinalWeight2 = measuredFinalWeight2;
	}
	public Byte getAirPressureTest2() {
		return airPressureTest2;
	}
	public void setAirPressureTest2(Byte airPressureTest2) {
		this.airPressureTest2 = airPressureTest2;
	}
	public Double getMeasuredIpVoltage2() {
		return measuredIpVoltage2;
	}
	public void setMeasuredIpVoltage2(Double measuredIpVoltage2) {
		this.measuredIpVoltage2 = measuredIpVoltage2;
	}
	public Double getMeasuredAmpere2() {
		return measuredAmpere2;
	}
	public void setMeasuredAmpere2(Double measuredAmpere2) {
		this.measuredAmpere2 = measuredAmpere2;
	}
	public Double getMeasuredIpPower2() {
		return measuredIpPower2;
	}
	public void setMeasuredIpPower2(Double measuredIpPower2) {
		this.measuredIpPower2 = measuredIpPower2;
	}
	public Double getMeasuredOpPower2() {
		return measuredOpPower2;
	}
	public void setMeasuredOpPower2(Double measuredOpPower2) {
		this.measuredOpPower2 = measuredOpPower2;
	}
	public Double getMeasuredTemperatureInitial2() {
		return measuredTemperatureInitial2;
	}
	public void setMeasuredTemperatureInitial2(Double measuredTemperatureInitial2) {
		this.measuredTemperatureInitial2 = measuredTemperatureInitial2;
	}
	public Double getMeasuredTemperatureFinal2() {
		return measuredTemperatureFinal2;
	}
	public void setMeasuredTemperatureFinal2(Double measuredTemperatureFinal2) {
		this.measuredTemperatureFinal2 = measuredTemperatureFinal2;
	}
	public Double getMeasuredSoundLevelcw2() {
		return measuredSoundLevelcw2;
	}
	public void setMeasuredSoundLevelcw2(Double measuredSoundLevelcw2) {
		this.measuredSoundLevelcw2 = measuredSoundLevelcw2;
	}
	public Double getMeasuredSoundLevelccw2() {
		return measuredSoundLevelccw2;
	}
	public void setMeasuredSoundLevelccw2(Double measuredSoundLevelccw2) {
		this.measuredSoundLevelccw2 = measuredSoundLevelccw2;
	}
	public Byte getConfirmbbsSize2() {
		return confirmbbsSize2;
	}
	public void setConfirmbbsSize2(Byte confirmbbsSize2) {
		this.confirmbbsSize2 = confirmbbsSize2;
	}
	public Byte getMeasuredThermistorCablePresent2() {
		return measuredThermistorCablePresent2;
	}
	public void setMeasuredThermistorCablePresent2(
			Byte measuredThermistorCablePresent2) {
		this.measuredThermistorCablePresent2 = measuredThermistorCablePresent2;
	}
	public Byte getMeasuredSpaceHeaterCablePresent2() {
		return measuredSpaceHeaterCablePresent2;
	}
	public void setMeasuredSpaceHeaterCablePresent2(
			Byte measuredSpaceHeaterCablePresent2) {
		this.measuredSpaceHeaterCablePresent2 = measuredSpaceHeaterCablePresent2;
	}
	public Double getOverallVibration2() {
		return overallVibration2;
	}
	public void setOverallVibration2(Double overallVibration2) {
		this.overallVibration2 = overallVibration2;
	}
	public Byte getConcentricity2() {
		return concentricity2;
	}
	public void setConcentricity2(Byte concentricity2) {
		this.concentricity2 = concentricity2;
	}
	public Double getMeasuredOpSpeed2() {
		return measuredOpSpeed2;
	}
	public void setMeasuredOpSpeed2(Double measuredOpSpeed2) {
		this.measuredOpSpeed2 = measuredOpSpeed2;
	}
	
	
	
	
}
