package com.finsol.bean;


public class MianCaseFormBean 

{
	private Integer  case1Id;
	private Integer tempid;
	private Integer mainid;
	
	private String casePriority;
	private Integer orginReq;

	 
	public String getCasePriority()
	{
		return casePriority;
	}
	public void setCasePriority(String casePriority)
	{
		this.casePriority = casePriority;
	}
	public Integer getOrginReq()
	{
		return orginReq;
	}
	public void setOrginReq(Integer orginReq)
	{
		this.orginReq = orginReq;
	}
	public Integer getTempid() {
		return tempid;
	}
	public void setTempid(Integer tempid) {
		this.tempid = tempid;
	}
	public Integer getMainid() {
		return mainid;
	}
	public void setMainid(Integer mainid) {
		this.mainid = mainid;
	}
	private String  token;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Integer getCase1Id() {
		return case1Id;
	}
	public void setCase1Id(Integer case1Id) {
		this.case1Id = case1Id;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCaseRef() {
		return caseRef;
	}
	public void setCaseRef(String caseRef) {
		this.caseRef = caseRef;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCustomerRef() {
		return customerRef;
	}
	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}
	private String  customer;
	private String  customerCode;
	private String  caseRef;
	private String  contactPerson;
	private String  phone;
	private String  address;
	private String  customerRef;
	private String  dtReport;
	private String  dateRepot;
	//private Date    dateExp;
	//private Date    dateCase;
	private String    dateExp;
	private String    dateCase;
    public String getDateExp() {
		return dateExp;
	}
	public void setDateExp(String dateExp) {
		this.dateExp = dateExp;
	}
	public String getDateCase() {
		return dateCase;
	}
	public void setDateCase(String dateCase) {
		this.dateCase = dateCase;
	}
	private String images; 
	
	private String departmentname;
	 
	 
	 
	public String getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}
	/*public Date getDateCase() {
		return dateCase;
	}
	public void setDateCase(Date dateCase) {
		this.dateCase = dateCase;
	}
	public Date getDateExp() {
		return dateExp;
	}
	public void setDateExp(Date dateExp) {
		this.dateExp = dateExp;
	}*/
	public String getDtReport() {
		return dtReport;
	}
	public void setDtReport(String dtReport) {
		this.dtReport = dtReport;
	}
	public String getDateRepot() {
		return dateRepot;
	}
	public void setDateRepot(String dateRepot) {
		this.dateRepot = dateRepot;
	}
	 
	private String  assigned;
	private String  custStatus;

	public String getAssigned() {
		return assigned;
	}
	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}
	public String getCustStatus() {
		return custStatus;
	}
	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}


}
