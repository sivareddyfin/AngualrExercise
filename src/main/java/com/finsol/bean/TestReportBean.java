package com.finsol.bean;


public class TestReportBean 
{	
	private Integer id;
	private  String customerAssembyNumber;	
	private String startDateTime;	
	private String endDateTime;	
	private String dateRequired;
	private String dateReceived;
	private  String model;
	private  String couplingType;
	private  String[] accessories;
	private  Double inputShaft;
	private  Double outputShaft;
	private  String motorBrand;
	private  String outputrpm;
	private  String motorType;
	private Byte thermister;
	private Byte spaceHeater;
	private  String[] terminalBox;
	private  String paintColor;
	private Byte frequency;
	private  String protection;
	private  String voltage;
	private  String[] cableEntry;
	private  Double brakeVoltage;
	private  String changeBrakeKit;
	private  String lubrication;	
	private Byte motorDirection;	
	private  String assembledBy;	
	private  String testpPerformedBy;	
	private String assembledDate;
	private String bbsimage;
	private String customerName;
	private Byte phaseVoltage;	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCustomerAssembyNumber() {
		return customerAssembyNumber;
	}
	public void setCustomerAssembyNumber(String customerAssembyNumber) {
		this.customerAssembyNumber = customerAssembyNumber;
	}
	public String getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}
	public String getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}
	public String getDateRequired() {
		return dateRequired;
	}
	public void setDateRequired(String dateRequired) {
		this.dateRequired = dateRequired;
	}
	public String getDateReceived() {
		return dateReceived;
	}
	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getCouplingType() {
		return couplingType;
	}
	public void setCouplingType(String couplingType) {
		this.couplingType = couplingType;
	}

	public Double getInputShaft() {
		return inputShaft;
	}
	public void setInputShaft(Double inputShaft) {
		this.inputShaft = inputShaft;
	}
	public Double getOutputShaft() {
		return outputShaft;
	}
	public void setOutputShaft(Double outputShaft) {
		this.outputShaft = outputShaft;
	}
	public String getMotorBrand() {
		return motorBrand;
	}
	public void setMotorBrand(String motorBrand) {
		this.motorBrand = motorBrand;
	}
	public String getOutputrpm() {
		return outputrpm;
	}
	public void setOutputrpm(String outputrpm) {
		this.outputrpm = outputrpm;
	}
	public String getMotorType() {
		return motorType;
	}
	public void setMotorType(String motorType) {
		this.motorType = motorType;
	}
	public Byte getThermister() {
		return thermister;
	}
	public void setThermister(Byte thermister) {
		this.thermister = thermister;
	}
	public Byte getSpaceHeater() {
		return spaceHeater;
	}
	public void setSpaceHeater(Byte spaceHeater) {
		this.spaceHeater = spaceHeater;
	}

	public void setPaintColor(String paintColor) {
		this.paintColor = paintColor;
	}
	public Byte getFrequency() {
		return frequency;
	}
	public void setFrequency(Byte frequency) {
		this.frequency = frequency;
	}
	public String getProtection() {
		return protection;
	}
	public void setProtection(String protection) {
		this.protection = protection;
	}
	public String getVoltage() {
		return voltage;
	}
	public void setVoltage(String voltage) {
		this.voltage = voltage;
	}
	public Double getBrakeVoltage() {
		return brakeVoltage;
	}
	public void setBrakeVoltage(Double brakeVoltage) {
		this.brakeVoltage = brakeVoltage;
	}
	public String getChangeBrakeKit() {
		return changeBrakeKit;
	}
	public void setChangeBrakeKit(String changeBrakeKit) {
		this.changeBrakeKit = changeBrakeKit;
	}
	public String getLubrication() {
		return lubrication;
	}
	public void setLubrication(String lubrication) {
		this.lubrication = lubrication;
	}
	public Byte getMotorDirection() {
		return motorDirection;
	}
	public void setMotorDirection(Byte motorDirection) {
		this.motorDirection = motorDirection;
	}
	public String getAssembledBy() {
		return assembledBy;
	}
	public void setAssembledBy(String assembledBy) {
		this.assembledBy = assembledBy;
	}

	
	public String getTestpPerformedBy() {
		return testpPerformedBy;
	}
	public void setTestpPerformedBy(String testpPerformedBy) {
		this.testpPerformedBy = testpPerformedBy;
	}
	public String getAssembledDate() {
		return assembledDate;
	}
	public void setAssembledDate(String assembledDate) {
		this.assembledDate = assembledDate;
	}
	public String getBbsimage() {
		return bbsimage;
	}
	public void setBbsimage(String bbsimage) {
		this.bbsimage = bbsimage;
	}
	public String[] getAccessories() {
		return accessories;
	}
	public void setAccessories(String[] accessories) {
		this.accessories = accessories;
	}
	public String[] getTerminalBox() {
		return terminalBox;
	}
	public void setTerminalBox(String[] terminalBox) {
		this.terminalBox = terminalBox;
	}
	public String getPaintColor() {
		return paintColor;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	
	
	public String[] getCableEntry()
	{
		return cableEntry;
	}
	public void setCableEntry(String[] cableEntry)
	{
		this.cableEntry = cableEntry;
	}
	public Byte getPhaseVoltage()
	{
		return phaseVoltage;
	}
	public void setPhaseVoltage(Byte phaseVoltage)
	{
		this.phaseVoltage = phaseVoltage;
	}

	
	
	
}
