package com.finsol.bean;


public class ProgressUpdateBean {
	
	private String customer;	
	private String quotationno;	
	private String[] probabilities;
	private Double  totalProbability;
	private String salesMan;	
	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getQuotationno() {
		return quotationno;
	}
	public void setQuotationno(String quotationno) {
		this.quotationno = quotationno;
	}
	public String[] getProbabilities() {
		return probabilities;
	}
	public void setProbabilities(String[] probabilities) {
		this.probabilities = probabilities;
	}
	public Double getTotalProbability() {
		return totalProbability;
	}
	public void setTotalProbability(Double totalProbability) {
		this.totalProbability = totalProbability;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	
	

}
