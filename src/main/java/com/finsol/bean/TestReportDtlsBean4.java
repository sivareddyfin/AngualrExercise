package com.finsol.bean;

public class TestReportDtlsBean4 
{
	private  String slno4;
	private  String motorslno4;
	private  String model4;
	private  Double	measuredGearHeadWeight4;
	private  Double	measuredGreaseWeight4;
	private  Double	measuredFinalWeight4;
	private Byte airPressureTest4;
	private  Double measuredIpVoltage4; 
	private  Double measuredAmpere4; 
	private  Double measuredIpPower4;
	private  Double measuredOpPower4;
	private  Double measuredTemperatureInitial4;
	private  Double measuredTemperatureFinal4;
	private  Double measuredSoundLevelcw4;
	private  Double measuredSoundLevelccw4;
	private Byte confirmbbsSize4;
	private Byte measuredThermistorCablePresent4;
	private Byte measuredSpaceHeaterCablePresent4;
	private  Double overallVibration4;
	private Byte  concentricity4;
	private Double  measuredOpSpeed4;
	
	
	public String getSlno4() {
		return slno4;
	}
	public void setSlno4(String slno4) {
		this.slno4 = slno4;
	}
	public String getMotorslno4() {
		return motorslno4;
	}
	public void setMotorslno4(String motorslno4) {
		this.motorslno4 = motorslno4;
	}
	public String getModel4() {
		return model4;
	}
	public void setModel4(String model4) {
		this.model4 = model4;
	}

	
	public Double getMeasuredGearHeadWeight4() {
		return measuredGearHeadWeight4;
	}
	public void setMeasuredGearHeadWeight4(Double measuredGearHeadWeight4) {
		this.measuredGearHeadWeight4 = measuredGearHeadWeight4;
	}
	public Double getMeasuredGreaseWeight4() {
		return measuredGreaseWeight4;
	}
	public void setMeasuredGreaseWeight4(Double measuredGreaseWeight4) {
		this.measuredGreaseWeight4 = measuredGreaseWeight4;
	}
	public Double getMeasuredFinalWeight4() {
		return measuredFinalWeight4;
	}
	public void setMeasuredFinalWeight4(Double measuredFinalWeight4) {
		this.measuredFinalWeight4 = measuredFinalWeight4;
	}
	public Byte getAirPressureTest4() {
		return airPressureTest4;
	}
	public void setAirPressureTest4(Byte airPressureTest4) {
		this.airPressureTest4 = airPressureTest4;
	}
	public Double getMeasuredIpVoltage4() {
		return measuredIpVoltage4;
	}
	public void setMeasuredIpVoltage4(Double measuredIpVoltage4) {
		this.measuredIpVoltage4 = measuredIpVoltage4;
	}
	public Double getMeasuredAmpere4() {
		return measuredAmpere4;
	}
	public void setMeasuredAmpere4(Double measuredAmpere4) {
		this.measuredAmpere4 = measuredAmpere4;
	}
	public Double getMeasuredIpPower4() {
		return measuredIpPower4;
	}
	public void setMeasuredIpPower4(Double measuredIpPower4) {
		this.measuredIpPower4 = measuredIpPower4;
	}
	public Double getMeasuredOpPower4() {
		return measuredOpPower4;
	}
	public void setMeasuredOpPower4(Double measuredOpPower4) {
		this.measuredOpPower4 = measuredOpPower4;
	}
	public Double getMeasuredTemperatureInitial4() {
		return measuredTemperatureInitial4;
	}
	public void setMeasuredTemperatureInitial4(Double measuredTemperatureInitial4) {
		this.measuredTemperatureInitial4 = measuredTemperatureInitial4;
	}
	public Double getMeasuredTemperatureFinal4() {
		return measuredTemperatureFinal4;
	}
	public void setMeasuredTemperatureFinal4(Double measuredTemperatureFinal4) {
		this.measuredTemperatureFinal4 = measuredTemperatureFinal4;
	}
	public Double getMeasuredSoundLevelcw4() {
		return measuredSoundLevelcw4;
	}
	public void setMeasuredSoundLevelcw4(Double measuredSoundLevelcw4) {
		this.measuredSoundLevelcw4 = measuredSoundLevelcw4;
	}
	public Double getMeasuredSoundLevelccw4() {
		return measuredSoundLevelccw4;
	}
	public void setMeasuredSoundLevelccw4(Double measuredSoundLevelccw4) {
		this.measuredSoundLevelccw4 = measuredSoundLevelccw4;
	}
	public Byte getConfirmbbsSize4() {
		return confirmbbsSize4;
	}
	public void setConfirmbbsSize4(Byte confirmbbsSize4) {
		this.confirmbbsSize4 = confirmbbsSize4;
	}
	public Byte getMeasuredThermistorCablePresent4() {
		return measuredThermistorCablePresent4;
	}
	public void setMeasuredThermistorCablePresent4(
			Byte measuredThermistorCablePresent4) {
		this.measuredThermistorCablePresent4 = measuredThermistorCablePresent4;
	}
	public Byte getMeasuredSpaceHeaterCablePresent4() {
		return measuredSpaceHeaterCablePresent4;
	}
	public void setMeasuredSpaceHeaterCablePresent4(
			Byte measuredSpaceHeaterCablePresent4) {
		this.measuredSpaceHeaterCablePresent4 = measuredSpaceHeaterCablePresent4;
	}
	public Double getOverallVibration4() {
		return overallVibration4;
	}
	public void setOverallVibration4(Double overallVibration4) {
		this.overallVibration4 = overallVibration4;
	}
	public Byte getConcentricity4() {
		return concentricity4;
	}
	public void setConcentricity4(Byte concentricity4) {
		this.concentricity4 = concentricity4;
	}
	public Double getMeasuredOpSpeed4() {
		return measuredOpSpeed4;
	}
	public void setMeasuredOpSpeed4(Double measuredOpSpeed4) {
		this.measuredOpSpeed4 = measuredOpSpeed4;
	}
	
	
	
	
	
}
