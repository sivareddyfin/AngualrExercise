package com.finsol.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.finsol.bean.ApplicationBean;
import com.finsol.bean.CountryBean;
import com.finsol.bean.CountryLogoBean;
import com.finsol.bean.IndustryBean;
import com.finsol.bean.SalesmanBean;
import com.finsol.bean.TaxMasterBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.Application;
import com.finsol.model.Country;
import com.finsol.model.CountryLogo;
import com.finsol.model.Industry;
import com.finsol.model.MigrateAppcode;
import com.finsol.model.Salesman;
import com.finsol.model.TaxMaster;
import com.finsol.service.MasterScreen_ServiceTwo;
/**
 * @author Rama Krishna
 * 
 */
@Controller
public class MasterScreenControllerTwo {
	
	private static final Logger logger = Logger.getLogger(MasterScreenControllerTwo.class);
	
	@Autowired
	private MasterScreen_ServiceTwo masterservicetwo;
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/getCoutries", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public  @ResponseBody JSONObject  productCatageorySave(@RequestBody String jsonData) throws Exception
	{
		
		JSONObject countries=new JSONObject();
		JSONArray productcatgoriesaraay = new JSONArray();
	List<Country>  country=	masterservicetwo.getCountriesandCountryCodes();
	int i=1;
	for(Country c:country){
		JSONObject productcatgoriesOne=new JSONObject();
		productcatgoriesOne.put("id",i);
		productcatgoriesOne.put("countrycode",c.getCountrycode());
		productcatgoriesOne.put("bokclr",c.getBokclr());
        productcatgoriesOne.put("budgclr",c.getBudgclr());
        productcatgoriesOne.put("country",c.getCountry());
        productcatgoriesOne.put("country_order",c.getCountry_order());
        productcatgoriesOne.put("description",c.getDescription());
        productcatgoriesOne.put("country_Currencycode",c.getCurrencycode());
        productcatgoriesOne.put("status",c.getStatus());
        productcatgoriesaraay.add(productcatgoriesOne);
         i++;

	}
	
	
	
	if(productcatgoriesaraay.size()==0)
	{
		JSONObject productcatgoriesOne=new JSONObject();
		productcatgoriesOne.put("id",i);
		  productcatgoriesaraay.add(productcatgoriesOne);
		
		
	}
	countries.put("countries",productcatgoriesaraay);
		
		 
		return countries;
	
	}
	
	
	@RequestMapping(value = "/saveCountries", method = RequestMethod.POST)
	public @ResponseBody boolean NewselctionCase(MultipartHttpServletRequest req)
			throws Exception {
		
		
		String  griddata=req.getParameter("gridData");
		org.json.JSONArray branchData=new org.json.JSONArray(griddata);
		
		//JSONArray branchData=(JSONArray) jsonData.get("gridData");
		
	//	org.json.JSONArray gData=new org.json.JSONArray(ss1);
		ArrayList<CountryBean> al=new ArrayList<CountryBean>();
		
		for (int i = 0; i <branchData.length(); i++) {

			CountryBean newcasegrid = new ObjectMapper().readValue(branchData.get(i).toString(),CountryBean.class);
			al.add(newcasegrid);
			

		}
		
		
		ArrayList<Country> al1=this.getCountriedData(al);
		masterservicetwo.saveCountry(al1);
		return true;
		
		
		
	}
	
	
	

	@RequestMapping(value = "/saveCountriesLogo", method = RequestMethod.POST)
	public @ResponseBody boolean saveCountry(MultipartHttpServletRequest req)
			throws Exception {
		ServletContext servletContext	= req.getSession().getServletContext();
		/*ServletContext servletContext	= req.getServletContext();*/
		 
	    List<MultipartFile> mpf=req.getFiles("usersign");
		
	 String imapath=this.getImagepathOfCustmerTemp(mpf,servletContext);
	 
		
		String  countrycode1=req.getParameter("formdata");
		
		CountryLogoBean cbean = new ObjectMapper().readValue(countrycode1,CountryLogoBean.class);
		
		String countrycode=cbean.getCountry();
		//String  countrycode=req.getParameter("countrycode");
		 Session session=hibernateDao.getSessionFactory().openSession();
		 
		 
		 session.beginTransaction();
			 
			
			 
			
		 
		 
		 String delete="delete from  CountryLogo where countrycode='"+countrycode+"'";
		 SQLQuery a=session.createSQLQuery(delete);
		 a.executeUpdate();
		 
		 CountryLogo cl=new CountryLogo();
		 cl.setCountryflag(imapath);
		 cl.setCountrycode(countrycode);
		 session.save(cl);
		 
		//org.json.JSONArray branchData=new org.json.JSONArray(griddata);
		
		//JSONArray branchData=(JSONArray) jsonData.get("gridData");
		
	//	org.json.JSONArray gData=new org.json.JSONArray(ss1);
		ArrayList<CountryBean> al=new ArrayList<CountryBean>();
	 
		
		session.getTransaction().commit();
		
		session.close();
		 
		return true;
		
		
		
	}
	
	
	
	
	
	
	private String getImagepathOfCustmerTemp(List<MultipartFile> mpf,ServletContext servletContext) throws Exception
	{
		String photoName=null;
		String photoPath=null;
	 int i=0;
		for(MultipartFile mf:mpf)
		{
			
			String fname=mf.getOriginalFilename();
			//logger.info("--------------"+mf.getOriginalFilename());
			
			
			if (mf.getSize() != 0) 
			{
				Random t = new Random();
				int num = t.nextInt(10000);
				
				String exts = getFileExtension(mf.getOriginalFilename());
				photoName = i + num + "_photo" + "." + exts;
		
				photoPath = "logo/" + photoName;
				
				 

				File dir1 = new File(servletContext.getRealPath("/") + "logo/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (mf.getSize() != 0) 
				{
					byte[] bytes1 = mf.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}
			i++;
		}
	
		return photoPath;

	}


	
	public  String getFileExtension(String fileName) 
	{
	    if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	    return fileName.substring(fileName.lastIndexOf(".")+1);
	    else return "";
	}
		 	

	
	
	@RequestMapping(value = "/getCountriesLogo",method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody JSONObject getCountryLogo(HttpServletRequest req)
			throws Exception {
		
		
		//String  countrylogo=req.getParameter("countrylogo");
		String  countrycode=req.getParameter("countrycode");
		
		 Session session=hibernateDao.getSessionFactory().openSession();
			/*String s= req.getParameter("countrycode");
			*/
			String q="from  CountryLogo where  countrycode='"+countrycode+"'";
			Query a=session.createQuery(q);
			List<CountryLogo> l=a.list();
			JSONObject mainobject=new JSONObject();
			 
			 
			for(CountryLogo t:l)
			{
				mainobject.put("countrylogo",t.getCountryflag());
				
			}
		 
		 
		return mainobject;
		
		
		
	}
	
	
	
	
 
	

	private ArrayList<Country> getCountriedData(ArrayList<CountryBean> al) 
	{
		ArrayList<Country> a2=new ArrayList<Country>();
		for(CountryBean v:al)
		{
			Country c1=new Country();
			
			c1.setBokclr(v.getBokclr());
			c1.setBudgclr(v.getBudgclr());
			c1.setCountry(v.getCountry());
			c1.setCurrencycode(v.getCountry_Currencycode());
			c1.setCountry_order(v.getCountry_order());
			c1.setCountrycode(v.getCountrycode());
			c1.setDescription(v.getDescription());
			c1.setStatus(v.getStatus());
			
			a2.add(c1);
		}
		// TODO Auto-generated method stub
		return a2;
	}
	

	/*@RequestMapping(value = "/getSalesManfromMaster", method = RequestMethod.GET)
    public  @ResponseBody JSONObject  getSaleamanFromMaster(HttpServletRequest req,HttpServletResponse res) throws Exception
	{
		
		JSONObject saleman=new JSONObject();
		JSONArray productcatgoriesaraay = new JSONArray();
		String countrycode=req.getParameter("countrycode");
		List<Salesman> s=masterservicetwo.getAllSalesman(countrycode);
		int i=1;
		for(Salesman s1:s)
		{
			JSONObject subobject=new JSONObject();
			subobject.put("id",1);
			subobject.put("salesman",s1.getSalesman());
			subobject.put("regioncode",s1.getRegioncode());
			subobject.put("salesmancode",s1.getSalesmancode());
			subobject.put("ini",s1.getIni());
			subobject.put("regioncode",s1.getRegioncode());
			subobject.put("status",s1.getStatus());
			productcatgoriesaraay.add(subobject);
			i++;
			
		}
	 
		saleman.put("saleman", productcatgoriesaraay);
	
	
	 if(productcatgoriesaraay.size()==0)
	 {
		 
		 JSONObject subobject=new JSONObject();
			subobject.put("id",1);
			productcatgoriesaraay.add(subobject);
			 
	 }
	 
	 saleman.put("saleman", productcatgoriesaraay);
		 
		return saleman;
	
	}
	*/
	
	
	
	
	@RequestMapping(value = "/getSalesManfromMaster", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONObject
	getSaleamanFromMaster(@RequestBody String jsonData) throws Exception 
	{
		JSONObject saleman=new JSONObject();
		JSONArray productcatgoriesaraay = new JSONArray();
		//String countrycode=req.getParameter("countrycode");
		List<Salesman> s=masterservicetwo.getAllSalesman(jsonData);
		int i=1;
		for(Salesman s1:s)
		{
			JSONObject subobject=new JSONObject();
			subobject.put("id",i);
			subobject.put("salesman",s1.getSalesman());
			subobject.put("regionid",Integer.parseInt(s1.getRegioncode()));
			subobject.put("salesmancode",s1.getSalesmancode());
			subobject.put("ini",s1.getIni());
			//subobject.put("regioncode",s1.getRegioncode());
			subobject.put("status",s1.getStatus());
			productcatgoriesaraay.add(subobject);
			i++;
			
		}
	 
		saleman.put("saleman", productcatgoriesaraay);
	
		/*if(productcatgoriesaraay.size()==0)
		 {
			 
			 JSONObject subobject=new JSONObject();
				subobject.put("id",1);
				productcatgoriesaraay.add(subobject);
				 
		 }
		 */
		saleman.put("saleman", productcatgoriesaraay);		 
		return saleman;
	

		 
	}
	
	
	
	
	@RequestMapping(value = "/Salesmanformsubmit", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public  @ResponseBody boolean  productSave(@RequestBody   JSONObject jsonData) throws Exception
	{
		JSONObject json_data=null;
		JSONObject countrycode1=(JSONObject) jsonData.get("formData");
		String countrycode=(String)countrycode1.get("countryCode");
		JSONArray branchData=(JSONArray) jsonData.get("gridData");
		ArrayList<SalesmanBean> al=new ArrayList<SalesmanBean>();
		for(int i=0;i<branchData.size();i++)
		{
		SalesmanBean newcasegrid = new ObjectMapper().readValue(branchData.get(i).toString(),SalesmanBean.class);
		al.add(newcasegrid);

			
		}
		
		List<Salesman> m=this.getOfsalesmandata(al,countrycode);
		 
		masterservicetwo.saveSalesman(m,countrycode);
		return true;
	
	}


	private List<Salesman> getOfsalesmandata(ArrayList<SalesmanBean> al, String countrycode) 
	{
		ArrayList<Salesman> al2=new ArrayList();
		for(SalesmanBean ss:al )
		{
			Salesman s=new Salesman();
			s.setCountrycode(countrycode);
			s.setEmployeecode(ss.getEmployeecode());
			s.setIni(ss.getIni());	
			s.setRegioncode(ss.getRegionid());
			s.setSalesman(ss.getSalesman());
			s.setSalesmancode(ss.getSalesmancode());
			s.setStatus(ss.getStatus());
			al2.add(s);
			
		}
		// TODO Auto-generated method stub
		return al2;
	}
	


	/*
	@RequestMapping(value = "/getIndustries", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public  @ResponseBody JSONObject  getIndustries(@RequestBody String jsonData) throws Exception
	{
		
		JSONObject industries=new JSONObject();
		JSONArray industriesarray = new JSONArray();
	List<Country>  country=	masterservicetwo.getCountriesandCountryCodes();
	int i=1;
	for(Country c:country){
		JSONObject productcatgoriesOne=new JSONObject();
		productcatgoriesOne.put("id",i);
		productcatgoriesOne.put("countrycode",c.getCountrycode());
		productcatgoriesOne.put("bokclr",c.getBokclr());
        productcatgoriesOne.put("budgclr",c.getBudgclr());
        productcatgoriesOne.put("country",c.getCountry());
        productcatgoriesOne.put("country_order",c.getCountry_order());
        productcatgoriesOne.put("description",c.getDescription());
        productcatgoriesOne.put("country_Currencycode",c.getCountry_Currencycode());
        productcatgoriesOne.put("status",c.getStatus());
        productcatgoriesaraay.add(productcatgoriesOne);
         i++;

	}
	
	
	
	if(productcatgoriesaraay.size()==0)
	{
		JSONObject productcatgoriesOne=new JSONObject();
		productcatgoriesOne.put("id",i);
		  productcatgoriesaraay.add(productcatgoriesOne);
		
		
	}
	countries.put("countries",productcatgoriesaraay);
		
		 
		return countries;
	
	}
	
*/
	
	
	 @RequestMapping(value = "/getcountrycheck", method = RequestMethod.GET)
	    public  @ResponseBody  boolean  getCountryCodeCheck(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 int p=0;
		 Session session=hibernateDao.getSessionFactory().openSession();
			String s= req.getParameter("country");
			
			String q="select count(country) from Country where country='"+s+"'";
			Query a=session.createQuery(q);
			List<Long> l=a.list();
			if(l.size()!=0)
			{
			 for(Long k:l)
			 {
				p= k.intValue();
			 }
			
			}
		if(p==0)
		{
			return true;
		}
			
		else
		{
			return false;
		}
			
		}
	 
	 @RequestMapping(value = "/getcountryordercheck", method = RequestMethod.GET)
	    public  @ResponseBody  boolean  getCountryorderCheck(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 int p=0;
		 Session session=hibernateDao.getSessionFactory().openSession();
			String s= req.getParameter("Order");
			Byte b=Byte.parseByte(s);
			
			String q="select count(country_order) from Country where country_order="+b+"";
			
			
			Query a=session.createQuery(q);
			List<Long> l=a.list();
			if(l.size()!=0)
			{
			 for(Long k:l)
			 {
				p= k.intValue();
			 }
			
			}
		if(p==0)
		{
			return true;
		}
			
		else
		{
			return false;
		}
		
		}
		
	

	 @RequestMapping(value = "/getcountrycodecheck", method = RequestMethod.GET)
	    public  @ResponseBody  boolean  getCountryCodeCheck222(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 int p=0;
		 Session session=hibernateDao.getSessionFactory().openSession();
			String s= req.getParameter("countrycode");
			
			String q="select count(countrycode) from Country where countrycode='"+s+"'";
			Query a=session.createQuery(q);
			List<Long> l=a.list();
			if(l.size()!=0)
			{
			 for(Long k:l)
			 {
				p= k.intValue();
			 }
			
			}
		if(p==0)
		{
			return true;
		}
			
		else
		{
			return false;
		}
			
		}
	
	 
	 
	 
	 @RequestMapping(value = "/getTaxnameoncountrycode", method = RequestMethod.POST)
	    public  @ResponseBody  JSONObject  getaxNameOnCountrycode(@RequestBody String jsonData) throws Exception
	    
		{
		 int p=0;
		 Session session=hibernateDao.getSessionFactory().openSession();
			/*String s= req.getParameter("countrycode");
			*/
			String q="from TaxMaster where countrycode='"+jsonData+"'";
			Query a=session.createQuery(q);
			List<TaxMaster> l=a.list();
			JSONObject mainobject=new JSONObject();
			JSONArray mainarray=new  JSONArray();
			int i=1;
			for(TaxMaster t:l)
			{
				
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("tax",t.getTaxname());
				subobject.put("percentage",t.getPercentage());
				i++;
				mainarray.add(subobject);
			}
			
			
			
			
			
			if(l.size()==0)
			{
				JSONObject subobject=new JSONObject();
				subobject.put("id",1);
				 
				mainarray.add(subobject);
				
			}
			mainobject.put("countrytax",mainarray);
	
			return mainobject;
	 
	 
		}
	 
	 
	 @RequestMapping(value = "/SaveTaxCountry", method = RequestMethod.POST)
	    public  @ResponseBody  boolean  saveCountryTaxMaster(MultipartHttpServletRequest req) throws Exception
	    
		{
		 String  countrycode=req.getParameter("formData");
		   
		 
		 Country country = new ObjectMapper().readValue(countrycode, Country.class);
		String countrycode1 =country.getCountrycode();
		    
		   
			//String countrycode=(String)countrycode1.get("countrycode");
			//JSONArray branchData=(JSONArray) jsonData.get("gridData");
			String  griddata=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(griddata);
			ArrayList<TaxMasterBean> al=new ArrayList<TaxMasterBean>();
			for(int i=0;i<gData.length();i++)
			{
				TaxMasterBean taxmaster = new ObjectMapper().readValue(gData.get(i).toString(),TaxMasterBean.class);
			   al.add(taxmaster);
			}
			masterservicetwo.saveTaxMaster(countrycode1,al);
			
	
			return true;
	 
	 
		}
	 
	 
	 
	 
	 
	 @RequestMapping(value = "/getIndustryFromMaster", method = RequestMethod.POST)
	    public  @ResponseBody  JSONObject  getIndustryFromMaster(HttpServletRequest req) throws Exception
	    
		{
		 
		 Session session=hibernateDao.getSessionFactory().openSession();
			/*String s= req.getParameter("countrycode");
			*/
			String q="from  Industry ";
			Query a=session.createQuery(q);
			List<Industry> l=a.list();
			JSONObject mainobject=new JSONObject();
			JSONArray mainarray=new  JSONArray();
			int i=1;
			for(Industry t:l)
			{
				
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("industryCode",t.getIndustrycode());
				subobject.put("industryName",t.getTotalIndustryname());
				i++;
				mainarray.add(subobject);
			}
			mainobject.put("countrytax",mainarray);
	
			return mainobject;
	 
	 
	 
		}
	 
	 
	 @RequestMapping(value = "/saveIndustryToMaster", method = RequestMethod.POST)
	    public  @ResponseBody  boolean  saveIndustryToMaster(MultipartHttpServletRequest req) throws Exception
	    
		{
		 
		   //String  countrycode1=req.getParameter("formData");
			//String countrycode=(String)countrycode1.get("countrycode");
			//JSONArray branchData=(JSONArray) jsonData.get("gridData");
			String  griddata=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(griddata);
			ArrayList<IndustryBean> al=new ArrayList<IndustryBean>();
			for(int i=0;i<gData.length();i++)
			{
				IndustryBean indbean = new ObjectMapper().readValue(gData.get(i).toString(),IndustryBean.class);
			   al.add(indbean);
			}
			masterservicetwo.saveIndustryMaster(al);
			
	
			return true;
	 
	 
		}
	 
	 
	 
	 
	 @RequestMapping(value = "/getApplicationFromMaster", method = RequestMethod.POST)
	    public  @ResponseBody  JSONObject  getApplicationFromMaster(HttpServletRequest req) throws Exception
	    
		{
		 
		 Session session=hibernateDao.getSessionFactory().openSession();
			/*String s= req.getParameter("countrycode");
			*/
			String q="from  Application ";
			Query a=session.createQuery(q);
			List<Application> l=a.list();
			JSONObject mainobject=new JSONObject();
			JSONArray mainarray=new  JSONArray();
			int i=1;
			for(Application t:l)
			{
				
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("applicationCode",t.getApplicationcode());
				subobject.put("applicationName",t.getApplication());
				i++;
				mainarray.add(subobject);
			}
			mainobject.put("countrytax",mainarray);
	
			return mainobject;
		   
	 
		}
	 
	 
	 @RequestMapping(value = "/saveApplicationToMaster", method = RequestMethod.POST)
	    public  @ResponseBody  boolean  saveApplicationToMaster(MultipartHttpServletRequest req) throws Exception
	    
		{
		 
		  // String  countrycode1=req.getParameter("formData");
			//String countrycode=(String)countrycode1.get("countrycode");
			//JSONArray branchData=(JSONArray) jsonData.get("gridData");
			String  griddata=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(griddata);
			ArrayList<ApplicationBean> al=new ArrayList<ApplicationBean>();
			for(int i=0;i<gData.length();i++)
			{
				ApplicationBean appbean = new ObjectMapper().readValue(gData.get(i).toString(),ApplicationBean.class);
			   al.add(appbean);
			}
			masterservicetwo.saveApplication(al);
			
	
			return true;
	 
	 
		}
	 
	 

	 
	 @RequestMapping(value = "/updateappcodemigration", method = RequestMethod.GET)
	    public  @ResponseBody  boolean  updateappcodemigration( ) throws Exception
	    
		{
		 Session session=hibernateDao.getSessionFactory().openSession();
		 String queryupdate="from MigrateAppcode";
		 
		 Query q=session.createQuery(queryupdate);
		List<MigrateAppcode> data= q.list();
		for(MigrateAppcode aa:data)
		{
			String updatequery="update YtdBooking  set appcode='"+aa.getApplicationcode()+"' where salesorder='" +aa.getSalesorder()+"'";
			 Query q1=session.createQuery(updatequery);
			 q1.executeUpdate();
		}
			
	System.out.println("executed sucessfullyyyyyyyyyyyy--------------->");
			return true;
	 
	 
		}
	 

	
	
}
	
	
	