package com.finsol.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.finsol.bean.DeliveryUpdateBean;
import com.finsol.bean.ProbabilityMasterBean;
import com.finsol.bean.ProgressUpdateBean;
import com.finsol.bean.SalesProgressIndicatorsBean;
import com.finsol.bean.SalesReportBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.CustmerDetails;
import com.finsol.model.CustmerType;
import com.finsol.model.DeliveryUpdate;
import com.finsol.model.DeliveryUpdateTemp;
import com.finsol.model.Description;
import com.finsol.model.ProbabilityMaster;
import com.finsol.model.ProgressUpdateDtlsTemp;
import com.finsol.model.ProgressUpdateSmry;
import com.finsol.model.ProgressUpdateSmryTemp;
import com.finsol.model.Quotation;
import com.finsol.model.SalesProgressIndicators;
import com.finsol.model.SalesReport;
import com.finsol.model.Salesman;
import com.finsol.model.Users;
import com.finsol.model.ViewNotes;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.SalesReportService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

/**
 * @author Rama Krishna
 *
 */
@Controller
public class SalesReportController
{

	private static final Logger logger = Logger.getLogger(SalesReportController.class);

	@Autowired
	private SalesReportService salesReportService;

	@Autowired
	private BSS_CommonService commonService;

	@Autowired
	private HibernateDao hibernateDao;

	//----------------------------------------------- Sales ProgressIndicators  Start ------------------------------------------------------//

	@RequestMapping(value = "/saveProgressIndicators", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveProgressIndicators(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();

		SalesProgressIndicators salesProgressIndicators = null;
		try
		{
			JSONArray indicatorData = (JSONArray) jsonData.get("gridData");

			for (int i = 0; i < indicatorData.size(); i++)
			{
				SalesProgressIndicatorsBean salesProgressIndicatorsBean = new ObjectMapper().readValue(indicatorData.get(i)
				        .toString(), SalesProgressIndicatorsBean.class);
				salesProgressIndicators = prepareModelForSalesProgressIndicators(salesProgressIndicatorsBean);
				entityList.add(salesProgressIndicators);

				if (salesProgressIndicatorsBean.getIndicatorcode() != null)
				{
					String strQry = "UPDATE ProbabilityMaster set status=" + salesProgressIndicatorsBean.getStatus()
					        + " WHERE indicatorcode =" + salesProgressIndicatorsBean.getIndicatorcode() + "";
					Integer updatedRows = commonService.executeUpdate(strQry);
				}

			}
			//salesReportService.deleteSalesProgressIndicators();
			isInsertSuccess = salesReportService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}

	}

	private SalesProgressIndicators prepareModelForSalesProgressIndicators(SalesProgressIndicatorsBean salesProgressIndicatorsBean)
	{
		SalesProgressIndicators salesProgressIndicators = new SalesProgressIndicators();

		salesProgressIndicators.setId(salesProgressIndicatorsBean.getId());
		salesProgressIndicators.setIndicator(salesProgressIndicatorsBean.getIndicator());
		salesProgressIndicators.setDescription(salesProgressIndicatorsBean.getDescription());
		salesProgressIndicators.setStatus(salesProgressIndicatorsBean.getStatus());
		salesProgressIndicators.setIndicatorcode(salesProgressIndicatorsBean.getIndicatorcode());
		return salesProgressIndicators;
	}

	@RequestMapping(value = "/getAllSalesProgressIndicators", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllSalesProgressIndicators(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();

		List<SalesProgressIndicatorsBean> salesProgressIndicatorsBeans = prepareListofSalesProgressIndicatorsBeans(salesReportService
		        .listSalesProgressIndicators());

		org.json.JSONArray indicatorsArray = new org.json.JSONArray(salesProgressIndicatorsBeans);

		response.put("indicators", indicatorsArray);

		return response.toString();
	}

	private List<SalesProgressIndicatorsBean> prepareListofSalesProgressIndicatorsBeans(
	        List<SalesProgressIndicators> salesProgressIndicators)
	{
		List<SalesProgressIndicatorsBean> beans = null;
		SalesProgressIndicatorsBean bean = null;
		if (salesProgressIndicators != null && !salesProgressIndicators.isEmpty())
		{
			beans = new ArrayList<SalesProgressIndicatorsBean>();

			for (SalesProgressIndicators salesProgressIndicator : salesProgressIndicators)
			{
				bean = new SalesProgressIndicatorsBean();

				bean.setId(salesProgressIndicator.getId());
				bean.setIndicator(salesProgressIndicator.getIndicator());
				bean.setDescription(salesProgressIndicator.getDescription());

				bean.setStatus(salesProgressIndicator.getStatus());
				bean.setIndicatorcode(salesProgressIndicator.getIndicatorcode());
				beans.add(bean);
			}
		}

		return beans;
	}

	//-----------------------------------------------Sales ProgressIndicators  End ------------------------------------------------------//

	//----------------------------------------------- Probability Master  Start ------------------------------------------------------//

	@RequestMapping(value = "/saveProbabilityMaster", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveProbabilityMaster(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		ProbabilityMaster probabilityMaster = null;
		try
		{
			String customerGroup = (String) jsonData.getString("customerGroup");
			JSONArray indicatorData = (JSONArray) jsonData.get("gridData");

			for (int i = 0; i < indicatorData.size(); i++)
			{
				ProbabilityMasterBean probabilityMasterBean = new ObjectMapper().readValue(indicatorData.get(i).toString(),
				        ProbabilityMasterBean.class);
				probabilityMaster = prepareModelForProbabilityMaster(probabilityMasterBean, customerGroup);
				entityList.add(probabilityMaster);
			}
			salesReportService.deleteProbabilityMaster(probabilityMaster);
			isInsertSuccess = salesReportService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private ProbabilityMaster prepareModelForProbabilityMaster(ProbabilityMasterBean probabilityMasterBean, String customerGroup)
	{
		ProbabilityMaster probabilityMaster = new ProbabilityMaster();
		SalesProgressIndicators SalesProgressIndicator = (SalesProgressIndicators) commonService.getEntityById(
		        probabilityMasterBean.getIndicatorcode(), SalesProgressIndicators.class);
		probabilityMaster.setId(probabilityMasterBean.getId());
		probabilityMaster.setIndicatorcode(probabilityMasterBean.getIndicatorcode());
		probabilityMaster.setProbability(probabilityMasterBean.getProbability());
		probabilityMaster.setCustomergroup(customerGroup);
		probabilityMaster.setStatus((byte) 0);
		probabilityMaster.setIndicator(SalesProgressIndicator.getIndicator());
		return probabilityMaster;
	}

	@RequestMapping(value = "/getProbabilityIndicatorsForCustGroup", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllProbabilityIndicators(@RequestBody String jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();

		CustmerDetails custmerDetails = (CustmerDetails) hibernateDao.getRowByColumn(CustmerDetails.class, "accountid", jsonData);
		CustmerType custmerType = (CustmerType) hibernateDao.getRowByColumn(CustmerType.class, "custmertype",
		        custmerDetails.getCustomerType());

		List<ProbabilityMasterBean> probabilityMasterBean = prepareListofProbabilityMasterBeans(salesReportService
		        .loadProbabilityMasterByCustomerGroup(custmerType.getCustmertypename()));

		org.json.JSONArray probabilitiesArray = new org.json.JSONArray(probabilityMasterBean);

		response.put("probabilities", probabilitiesArray);

		return response.toString();
	}

	@RequestMapping(value = "/getAllProbabilityMasters", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllProbabilityMasters(@RequestBody String jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();

		List<ProbabilityMasterBean> probabilityMasterBean = prepareListofProbabilityMasterBeans(salesReportService
		        .loadProbabilityMasterByCustomerGroup(jsonData));

		org.json.JSONArray probabilitiesArray = new org.json.JSONArray(probabilityMasterBean);

		response.put("probabilities", probabilitiesArray);

		return response.toString();
	}

	private List<ProbabilityMasterBean> prepareListofProbabilityMasterBeans(List<ProbabilityMaster> probabilityMasters)
	{
		List<ProbabilityMasterBean> beans = null;
		ProbabilityMasterBean bean = null;
		if (probabilityMasters != null && !probabilityMasters.isEmpty())
		{
			beans = new ArrayList<ProbabilityMasterBean>();

			for (ProbabilityMaster probabilityMaster : probabilityMasters)
			{
				bean = new ProbabilityMasterBean();
				if (probabilityMaster.getStatus() != 1)
				{
					bean.setId(probabilityMaster.getId());
					bean.setIndicatorcode(probabilityMaster.getIndicatorcode());
					bean.setProbability(probabilityMaster.getProbability());
					bean.setIndicator(probabilityMaster.getIndicator());
					beans.add(bean);
				}
			}
		}

		return beans;
	}

	//---------------------------------------------  Probability Master  End ------------------------------------------------------//

	//---------------------------------------------  Sales Report Start ------------------------------------------------------//

	@RequestMapping(value = "/saveSalesReport", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveSalesReport(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		SalesReport salesReport = null;

		//DeliveryUpdate deliveryUpdate=null;
		//ProgressUpdateSmry progressUpdateSmry=null;
		//ProgressUpdateDtls progressUpdateDtls=null;

		try
		{
			JSONObject salesReportData = (JSONObject) jsonData.get("salesreport");
			//Integer it=Integer.parseInt(salesReportData.toString());				
			//JSONObject deliveryUpdateData=(JSONObject) jsonData.get("deliveryupdate");
			//JSONObject progressUpdateData=(JSONObject) jsonData.get("progressupdate");

			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			//int month = now.get(Calendar.MONTH) + 1;
			//int date = now.get(Calendar.DATE);
			SalesReportBean salesReportBean = new ObjectMapper().readValue(salesReportData.toString(), SalesReportBean.class);
			salesReport = prepareModelForSalesReport(salesReportBean);
			
			String salesReportRefNumber=salesReport.getSalesrptrefno();

			List<DeliveryUpdateTemp> deliveryUpdateTmp = salesReportService.loadDeliveryUpdateTemp(salesReportRefNumber);
			List<ProgressUpdateSmryTemp> progressUpdateSmryTmp = salesReportService.loadProgressUpdateSmryTemp(salesReportRefNumber);
		//	List<ProgressUpdateDtlsTemp> progressUpdateDtlsTmp = salesReportService.loadProgressUpdateDtlsTemp(salesReportRefNumber);

			entityList.add(salesReport);
			
			if(deliveryUpdateTmp!=null && deliveryUpdateTmp.size()>0)
			{
				for(DeliveryUpdateTemp deliveryUpdateTemp : deliveryUpdateTmp)
				{
					DeliveryUpdate deliveryUpdate=new DeliveryUpdate();
					org.apache.commons.beanutils.BeanUtils.copyProperties(deliveryUpdate,deliveryUpdateTemp);
					deliveryUpdate.setId(null);
					entityList.add(deliveryUpdate);
					salesReportService.deleteDeliverUpdates(deliveryUpdate.getQuotationno());
					salesReportService.deleteDeliverUpdatesTemp(deliveryUpdate.getQuotationno());
				}
					
			}
			
			if(progressUpdateSmryTmp!=null && progressUpdateSmryTmp.size()>0)
			{
				for(ProgressUpdateSmryTemp progressUpdateSmryTemp : progressUpdateSmryTmp)
				{
					ProgressUpdateSmry progressUpdateSmry=new ProgressUpdateSmry();					
					org.apache.commons.beanutils.BeanUtils.copyProperties(progressUpdateSmry,progressUpdateSmryTemp);
					progressUpdateSmry.setId(null);
					entityList.add(progressUpdateSmry);
					salesReportService.deleteProgressUpdateSmry(progressUpdateSmry.getQuotationno());	
					salesReportService.deleteProgressUpdateSmryTemp(progressUpdateSmry.getQuotationno());	
				
				}
			}
			
/*			if(progressUpdateDtlsTmp!=null && progressUpdateDtlsTmp.size()>0)
			{
				for(ProgressUpdateDtlsTemp progressUpdateDtlsTemp : progressUpdateDtlsTmp)
				{
					ProgressUpdateDtls progressUpdateDtls=new ProgressUpdateDtls();					
					org.apache.commons.beanutils.BeanUtils.copyProperties(progressUpdateDtls,progressUpdateDtlsTemp);		
					progressUpdateDtls.setId(null);
					entityList.add(progressUpdateDtls);					
					salesReportService.deleteProgressUpdateDtls(progressUpdateDtls.getQuotationno());
				
				}
			}*/
			
			//String timeStamp = new SimpleDateFormat("dd-MM-yyyy  HH:mm:ss a").format(new Date());
			Date sysdate = new Date();
			if(salesReportBean.getCaseOrQuotaion()==1)
			{
				ViewNotes viewnotes=new ViewNotes();
				viewnotes.setCustomer(salesReportBean.getCustomer());
				viewnotes.setSalesman(salesReportBean.getSalesMan());
				viewnotes.setQuotationno(salesReportBean.getQuoteOrRefno());
				viewnotes.setNotesdate(DateUtils.getSqlDateFromString(salesReportBean.getSrdatetime(),Constants.GenericDateFormat.DATE_FORMAT));
				viewnotes.setViewnotes(salesReportBean.getCallnotes());				
				entityList.add(viewnotes);		
			}
			
			if(salesReportBean.getCaseOrQuotaion()==0)
			{
		 		int hour = now.get(Calendar.HOUR_OF_DAY);
				int minute = now.get(Calendar.MINUTE);
				String hoursandminutes=hour+":"+minute;
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				String dd = dateFormat.format(date);
				Date dateexp1 = dateFormat.parse(dd);
				
			 	Description des=new Description();
				des.setCaserefernce(salesReportBean.getQuoteOrRefno());
				des.setRemarks(salesReportBean.getRemarks());
				des.setHoursandminutes(hoursandminutes);
				des.setCurrentDate(dateexp1);
				
				entityList.add(des);		
			}
			
			
			isInsertSuccess = salesReportService.saveMultipleEntities(entityList);
			if(isInsertSuccess)
			{
				String strQry = "UPDATE CustmerDetails set customerStatus='"+salesReportBean.getCustomerStatus()+"' WHERE accountid ='"+salesReportBean.getCustomer()+"'";
				Integer updatedRows=commonService.executeUpdate(strQry);
				
				if(salesReportBean.getCaseOrQuotaion()==0)
				{
					strQry = "UPDATE CaseSummary set custStatus='"+salesReportBean.getCustomerStatus()+"' WHERE caseRef ='"+salesReportBean.getQuoteOrRefno()+"'";
					updatedRows=commonService.executeUpdate(strQry);
					 
	
						
				}
			}
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private SalesReport prepareModelForSalesReport(SalesReportBean salesReportBean)
	{
		Integer maxId = commonService.getMaximumID("seqno", "SalesReport");
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		Salesman sm = (Salesman) hibernateDao.getRowByColumn(Salesman.class, "salesmancode", salesReportBean.getSalesManCode());

		//Salesmancode+Sequence/Year
		SalesReport salesReport = new SalesReport();
		salesReport.setActivity(salesReportBean.getActivity());
		salesReport.setAssignedto(salesReportBean.getAssignedto());
		salesReport.setCallnotes(salesReportBean.getCallnotes());
		salesReport.setCustomer(salesReportBean.getCustomer());
		salesReport.setRemarks(salesReportBean.getRemarks());
		salesReport.setSalesmancode(sm.getSalesmancode());
		salesReport.setCustomerstatus(salesReportBean.getCustomerStatus());
		salesReport.setContactname(salesReportBean.getContactName());
		salesReport.setSalesrptrefno(sm.getSalesmancode() + maxId + "/" + year);
		salesReport.setSeqno(maxId);
		salesReport.setSrdatetime(DateUtils.getSqlDateFromString(salesReportBean.getSrdatetime(),Constants.GenericDateFormat.DATE_FORMAT));
		if (salesReportBean.getCaseOrQuotaion() == 0)
		{
			salesReport.setCasereferenceno(salesReportBean.getQuoteOrRefno());
		}
		else if (salesReportBean.getCaseOrQuotaion() == 1)
		{
			salesReport.setQuotationno(salesReportBean.getQuoteOrRefno());
		}
		else
		{

		}

		return salesReport;
	}

	//Save Delivery Update---

	@RequestMapping(value = "/saveDeliveryUpdate", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveDeliveryUpdate(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		DeliveryUpdateTemp deliveryUpdate = null;

		try
		{
			String salesMan = (String) jsonData.get("formData");
			JSONArray deliveryUpdateData = (JSONArray) jsonData.get("gridData");

			for (int i = 0; i < deliveryUpdateData.size(); i++)
			{
				DeliveryUpdateBean deliveryUpdateBean = new ObjectMapper().readValue(deliveryUpdateData.get(i).toString(),
				        DeliveryUpdateBean.class);
				deliveryUpdate = prepareModelForDeliveryUpdate(deliveryUpdateBean, salesMan);
				entityList.add(deliveryUpdate);
				
				salesReportService.deleteDeliverUpdatesTemp(deliveryUpdate.getQuotationno());
			}

			isInsertSuccess = salesReportService.saveMultipleEntities(entityList);

			return isInsertSuccess;

		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}

	}

	private DeliveryUpdateTemp prepareModelForDeliveryUpdate(DeliveryUpdateBean deliveryUpdateBean, String salesMan)
	{
		Integer maxId = commonService.getMaximumID("seqno", "SalesReport");
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		Salesman sm = (Salesman) hibernateDao.getRowByColumn(Salesman.class, "salesman", salesMan);
		String salesrprtrefno = sm.getSalesmancode() + maxId + "/" + year;
		DeliveryUpdateTemp deliveryUpdate = new DeliveryUpdateTemp();
		/*			String newExdelDate=null;
					String newExorderDate=null;
					String newProjdelDate=null;
					if(deliveryUpdateBean.getExpdelDate().indexOf("T")!=-1)
					{
						String[] expdelDate=deliveryUpdateBean.getExpdelDate().split("T");
						String[] expdelDate1=expdelDate[0].split("-");
						newExdelDate=(Integer.parseInt(expdelDate1[2])+1)+"-"+expdelDate1[1]+"-"+expdelDate1[0];
					}
					if(deliveryUpdateBean.getExporderDate().indexOf("T")!=-1)
					{
						String[] exporderDate=deliveryUpdateBean.getExporderDate().split("T");
						String[] exporderDate1=exporderDate[0].split("-");
						newExorderDate=(Integer.parseInt(exporderDate1[2])+1)+"-"+exporderDate1[1]+"-"+exporderDate1[0];
					}
					if(deliveryUpdateBean.getExporderDate().indexOf("T")!=-1)
					{
						String[] projdelDate=deliveryUpdateBean.getExporderDate().split("T");
						String[] projdelDate1=projdelDate[0].split("-");
						newProjdelDate=(Integer.parseInt(projdelDate1[2])+1)+"-"+projdelDate1[1]+"-"+projdelDate1[0];
					}*/

		deliveryUpdate.setCustomername(deliveryUpdateBean.getCustomer());
		deliveryUpdate.setQuotationno(deliveryUpdateBean.getQuotationno());
		deliveryUpdate.setExpdeldate(DateUtils.getSqlDateFromString(deliveryUpdateBean.getExpdelDate(),
		        Constants.GenericDateFormat.DATE_FORMAT));
		deliveryUpdate.setExporderdate(DateUtils.getSqlDateFromString(deliveryUpdateBean.getExporderDate(),
		        Constants.GenericDateFormat.DATE_FORMAT));
		deliveryUpdate.setProjdeldate(DateUtils.getSqlDateFromString(deliveryUpdateBean.getProjdelDate(),
		        Constants.GenericDateFormat.DATE_FORMAT));
		deliveryUpdate.setQuotationdate(DateUtils.getSqlDateFromString(deliveryUpdateBean.getQuotationDate(),
		        Constants.GenericDateFormat.DATE_FORMAT));
		deliveryUpdate.setSalesrprtrefno(salesrprtrefno);
		deliveryUpdate.setSalesmancode(sm.getSalesmancode());
		return deliveryUpdate;
	}

	@RequestMapping(value = "/saveProgressUpdate", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveProgressUpdate(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess = false;
		ProgressUpdateSmryTemp progressUpdateSmry = null;
		ProgressUpdateDtlsTemp progressUpdateDtls = null;
		List entityList = new ArrayList();
		try
		{
			Integer maxId = commonService.getMaximumID("seqno", "SalesReport");
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);

			JSONObject progressUpdateData = (JSONObject) jsonData.get("progressupdate");
			ProgressUpdateBean progressUpdateBean = new ObjectMapper().readValue(progressUpdateData.toString(),
			        ProgressUpdateBean.class);

			Salesman sm = (Salesman) hibernateDao.getRowByColumn(Salesman.class, "salesman", progressUpdateBean.getSalesMan());
			String salesRefNo = sm.getSalesmancode() + maxId + "/" + year;

			progressUpdateSmry = prepareModelForProgreeUpdateSmry(progressUpdateBean, salesRefNo, sm.getSalesmancode());
			entityList.add(progressUpdateSmry);

			salesReportService.deleteProgressUpdateSmryTemp(progressUpdateSmry.getQuotationno());	
			
			
			//progressUpdateDtls = prepareModelForProgreeUpdateDtls(progressUpdateBean, salesRefNo, sm.getSalesmancode());
			//entityList.add(progressUpdateDtls);
			isInsertSuccess = salesReportService.saveMultipleEntities(entityList);
			return isInsertSuccess;

		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private ProgressUpdateSmryTemp prepareModelForProgreeUpdateSmry(ProgressUpdateBean progressUpdateBean, String salesRefNo,
	        String salesManCode)
	{
		ProgressUpdateSmryTemp progressUpdateSmry = new ProgressUpdateSmryTemp();
		progressUpdateSmry.setCustomername(progressUpdateBean.getCustomer());
		String[] pbsArray = progressUpdateBean.getProbabilities();
		String pbs = "";
		for (int i = 0; i < pbsArray.length; i++)
		{
			pbs = pbs + pbsArray[i] + ",";

		}
		pbs=pbs.replace("\"", "");
		progressUpdateSmry.setProbabilities(pbs.replace("null", "false"));
		progressUpdateSmry.setQuotationno(progressUpdateBean.getQuotationno());
		progressUpdateSmry.setTotalprobability(progressUpdateBean.getTotalProbability());
		progressUpdateSmry.setSalesrprtrefno(salesRefNo);
		progressUpdateSmry.setSalesmancode(salesManCode);
		return progressUpdateSmry;
	}

/*	private ProgressUpdateDtlsTemp prepareModelForProgreeUpdateDtls(ProgressUpdateBean progressUpdateBean, String salesRefNo,
	        String salesManCode)
	{
		ProgressUpdateDtlsTemp progressUpdateDtls = new ProgressUpdateDtlsTemp();
		String[] pbsArray = progressUpdateBean.getProbabilities();
		String pbs = "";
		for (int i = 0; i < pbsArray.length; i++)
		{
			pbs = pbs + pbsArray[i] + ",";

		}
		progressUpdateDtls.setCustomername(progressUpdateBean.getCustomer());
		progressUpdateDtls.setProbabilities(pbs.replace("null", "false"));
		progressUpdateDtls.setQuotationno(progressUpdateBean.getQuotationno());
		progressUpdateDtls.setSalesrprtrefno(salesRefNo);
		progressUpdateDtls.setSalesmancode(salesManCode);
		return progressUpdateDtls;
	}*/

	@RequestMapping(value = "/loadDeliveryUpdates", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String loadDeliveryUpdates(@RequestBody String jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		List<DeliveryUpdateBean> deliveryUpdateBeans = null;
		List lst = salesReportService.loadDeliveryUpdate(jsonData);

		List<Quotation> lst1 = salesReportService.loadQutationUpdate(jsonData);

		if (lst != null && lst.size() > 0)
			deliveryUpdateBeans = prepareBeanForDeliveryUpdate(lst);
		else
			deliveryUpdateBeans = prepareBeanForDeliveryUpdate1(lst1);

		org.json.JSONArray testReportArray = new org.json.JSONArray(deliveryUpdateBeans);
		response.put("updateDelivery", testReportArray);
		return testReportArray.toString();
	}

	private List<DeliveryUpdateBean> prepareBeanForDeliveryUpdate1(List<Quotation> quotations)
	{
		List<DeliveryUpdateBean> beans = null;

		if (quotations != null && !quotations.isEmpty())
		{
			beans = new ArrayList<DeliveryUpdateBean>();
			DeliveryUpdateBean bean = null;
			for (Quotation quotation : quotations)
			{
				bean = new DeliveryUpdateBean();
				bean.setCustomer(quotation.getCustName());
				bean.setQuotationDate(DateUtils.formatDate(quotation.getDateQuote(), Constants.GenericDateFormat.DATE_FORMAT));
				bean.setQuotationno(quotation.getQtnNo());

				beans.add(bean);
			}
		}
		return beans;
	}

	private List<DeliveryUpdateBean> prepareBeanForDeliveryUpdate(List deliveryUpdates)
	{
		
		
		List<DeliveryUpdateBean> beans = null;

		if (deliveryUpdates != null && !deliveryUpdates.isEmpty())
		{
			Object obj=deliveryUpdates.get(0);
			beans = new ArrayList<DeliveryUpdateBean>();
			DeliveryUpdateBean bean = null;
			if(obj instanceof  DeliveryUpdate)
			{
				List<DeliveryUpdate> deliveryUpdts=deliveryUpdates;
				for (DeliveryUpdate deliveryUpdate : deliveryUpdts)
				{
					bean = new DeliveryUpdateBean();
					bean.setCustomer(deliveryUpdate.getCustomername());
					bean.setQuotationDate(DateUtils.formatDate(deliveryUpdate.getQuotationdate(),
					        Constants.GenericDateFormat.DATE_FORMAT));
					bean.setQuotationno(deliveryUpdate.getQuotationno());

					if (deliveryUpdate.getExpdeldate() != null)
						bean.setExpdelDate(DateUtils.formatDate(deliveryUpdate.getExpdeldate(),
						        Constants.GenericDateFormat.DATE_FORMAT));
					if (deliveryUpdate.getExporderdate() != null)
						bean.setExporderDate(DateUtils.formatDate(deliveryUpdate.getExporderdate(),
						        Constants.GenericDateFormat.DATE_FORMAT));
					if (deliveryUpdate.getProjdeldate() != null)
						bean.setProjdelDate(DateUtils.formatDate(deliveryUpdate.getProjdeldate(),
						        Constants.GenericDateFormat.DATE_FORMAT));

					beans.add(bean);
				}
			}
			else
			{
				List<DeliveryUpdateTemp> deliveryUpdtstmp=deliveryUpdates;
				for (DeliveryUpdateTemp deliveryUpdate : deliveryUpdtstmp)
				{
					bean = new DeliveryUpdateBean();
					bean.setCustomer(deliveryUpdate.getCustomername());
					bean.setQuotationDate(DateUtils.formatDate(deliveryUpdate.getQuotationdate(),
					        Constants.GenericDateFormat.DATE_FORMAT));
					bean.setQuotationno(deliveryUpdate.getQuotationno());

					if (deliveryUpdate.getExpdeldate() != null)
						bean.setExpdelDate(DateUtils.formatDate(deliveryUpdate.getExpdeldate(),
						        Constants.GenericDateFormat.DATE_FORMAT));
					if (deliveryUpdate.getExporderdate() != null)
						bean.setExporderDate(DateUtils.formatDate(deliveryUpdate.getExporderdate(),
						        Constants.GenericDateFormat.DATE_FORMAT));
					if (deliveryUpdate.getProjdeldate() != null)
						bean.setProjdelDate(DateUtils.formatDate(deliveryUpdate.getProjdeldate(),
						        Constants.GenericDateFormat.DATE_FORMAT));

					beans.add(bean);
				}
			}
			
		
		}
		return beans;
	}
	
	
	@RequestMapping(value = "/loadProgressUpdates", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String loadProgressUpdates(@RequestBody String jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		List<ProgressUpdateBean> progressUpdateBeans = null;
		
		List lst = salesReportService.loadProgressUpdateDtls(jsonData);
		progressUpdateBeans = prepareBeanForProgressUpdate(lst);
		org.json.JSONArray testReportArray = new org.json.JSONArray(progressUpdateBeans);
		response.put("progressUpdates", testReportArray);
		
		return testReportArray.toString();
	}

	
	private List<ProgressUpdateBean> prepareBeanForProgressUpdate(List pudtls)
	{
		List<ProgressUpdateBean> beans = null;

		if (pudtls != null && !pudtls.isEmpty())
		{
			Object obj=pudtls.get(0);
			beans = new ArrayList<ProgressUpdateBean>();
			ProgressUpdateBean bean = null;
			if(obj instanceof  ProgressUpdateSmry)
			{
				List<ProgressUpdateSmry> puds=pudtls;
				for (ProgressUpdateSmry pudtl : puds)
				{
					bean = new ProgressUpdateBean();
					bean.setProbabilities(pudtl.getProbabilities().split(","));
					bean.setCustomer(pudtl.getCustomername());
					bean.setQuotationno(pudtl.getQuotationno());
					bean.setSalesMan(pudtl.getSalesmancode());
					bean.setTotalProbability(pudtl.getTotalprobability());
					beans.add(bean);
					
				}
			}
			else
			{
				List<ProgressUpdateSmryTemp> puds=pudtls;
				for (ProgressUpdateSmryTemp pudtl : puds)
				{
					bean = new ProgressUpdateBean();
					bean.setProbabilities(pudtl.getProbabilities().split(","));
					bean.setCustomer(pudtl.getCustomername());
					bean.setQuotationno(pudtl.getQuotationno());
					bean.setSalesMan(pudtl.getSalesmancode());
					bean.setTotalProbability(pudtl.getTotalprobability());
					beans.add(bean);
					
				}
			}
	
		}
		return beans;
	}
	
	
	@RequestMapping(value = "/loadViewNotes", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String loadViewNotes(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		List<ProgressUpdateBean> progressUpdateBeans = null;
		
		String salesman=(String)jsonData.get("salesMan");
		String srdate=(String)jsonData.get("srdate");
		
		String quotationo=(String)jsonData.get("quotationNo");
		String customer=(String)jsonData.get("customer");
		
		List<ViewNotes> listNotes = salesReportService.loadViewNotes(quotationo,customer,salesman,srdate);
		org.json.JSONArray viewNotesArray = new org.json.JSONArray(listNotes);
		response.put("listNotes", viewNotesArray);
		
		return viewNotesArray.toString();
	}
	
	@RequestMapping(value = "/loadViewNotesByDate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String loadViewNotesByDate(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		List<ProgressUpdateBean> progressUpdateBeans = null;
		
		String fromdate=(String)jsonData.get("fromdate");
		String todate=(String)jsonData.get("todate");
		
		
		List<ViewNotes> listNotes = salesReportService.loadViewNotesByDate(fromdate,todate);
		org.json.JSONArray viewNotesArray = new org.json.JSONArray(listNotes);
		response.put("listNotes", viewNotesArray);
		
		return viewNotesArray.toString();
	}
	
	@RequestMapping(value = "/getSalesManNameCode", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getSalesManNameCode(@RequestBody String jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		List<ProgressUpdateBean> progressUpdateBeans = null;
		
		Users user = (Users) hibernateDao.getRowByColumn(Users.class, "loginid", jsonData);
		
		Salesman sm = (Salesman) hibernateDao.getRowByColumn(Salesman.class, "salesmancode", user.getSalesmancode());
		
		response.put("salesMan", sm.getSalesman());
		response.put("salesManCode", sm.getSalesmancode());
		
		return response.toString();
	}

}