package com.finsol.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.finsol.dao.HibernateDao;
import com.finsol.model.Salesman;
import com.finsol.service.Bss_Migrate_ServiceImpl;

/**
 * @author Rama Krishna
 * 
 */
@Controller
public class ChartController {
	
	private static final Logger logger = Logger.getLogger(ChartController.class);
	
	@Autowired
	private Bss_Migrate_ServiceImpl migratesevice;
	
	@Autowired
	private HibernateDao hibernateDao;
	//Generating Hi Chart for Area/Region YTD
	
	@RequestMapping(value = "/ytdcode", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   ytdcode() 
	{
		
	
		ArrayList<String> al=new ArrayList<String>();
		ArrayList<String> al2=new ArrayList<String>();
		JSONObject keyforyears = null;
		JSONObject keyforyears1 = new JSONObject();
		JSONArray jsonArrayofvaluesbasedonyeras = null;
		int lastyear=0;
		JSONArray seriesobjArray = new JSONArray();
		HashMap<String,String> colorMap=new HashMap<String,String>();
		HashMap<String,String> budcolormap=new HashMap<String,String>();
		DecimalFormat df = new DecimalFormat("#.##");
		 //String budget_valueStr=df.format(budget_value2016);
		JSONObject categoryJson = null;
		JSONArray categoryArray = null;
		
		HashMap monthname=new HashMap();
		monthname.put(1,"Jan");
		monthname.put(2,"Feb");
		monthname.put(3,"Mar");
		monthname.put(4,"Apr");
		monthname.put(5,"May");
		monthname.put(6,"Jun");
		monthname.put(7,"Jul");
		monthname.put(8,"Aug");
		monthname.put(9,"Sep");
		monthname.put(10,"Oct");
		monthname.put(11,"Nov");
		monthname.put(12,"Dec");
		
		Calendar now = Calendar.getInstance();
	    int currMonth=now.get(Calendar.MONTH) + 1;
		String currMonthname=(String)monthname.get(currMonth);
		List<Integer> years=migratesevice.gettingBookingBudgetYears();

		if (!years.isEmpty()) 
		{
			lastyear = years.get(years.size() - 1);
		}
		//List<Object[]> coutries2=migratesevice.getCountries();
		List<Object[]> coutries=migratesevice.getCountries1();
		
		for(Object[] countrydetails:coutries)
		{
			String countryname=(String)countrydetails[0];
			String color=(String)countrydetails[2];
			String budcolor=(String)countrydetails[1];
			al.add(countryname);
			al2.add(color);
			colorMap.put(countryname, color);
			budcolormap.put(countryname,budcolor);
		}	 
		
		ArrayList<Double> budgetvalues=new ArrayList<Double>();
		ArrayList<Double> boookingvalues=new ArrayList<Double>();
		// Collections.reverse(coutries);
		for(Object[] countrydetails:coutries)
		{	
			categoryJson = new JSONObject();
			categoryArray = new JSONArray();
			String countryname=(String)countrydetails[0];
			
			keyforyears = new JSONObject();
			jsonArrayofvaluesbasedonyeras = new JSONArray();
			keyforyears.put("seriesname",countryname);
			keyforyears.put("color",colorMap.get(countryname));
			 
			Double budget_value=0.0;
			for(Integer year:years)		
			{
				JSONObject allvaluesinyears = new JSONObject();
				if(lastyear==year)
				{
					//categoryJson.put("label", year+"\n(Budget till "+currMonthname+")");	    	
					categoryJson.put("label", year+"\n(Budget YTD)");
					categoryArray.add(categoryJson);
					
					allvaluesinyears.put("name","Budget of "+year);
					budget_value=migratesevice.getBudgetValue(countryname,year);
					 //String budget_valueStr=df.format(budget_value);
					
					
					budgetvalues.add(budget_value);
					
					
				 
					 allvaluesinyears.put("value",budget_value);
					 allvaluesinyears.put("color",budcolormap.get(countryname));
					// allvaluesinyears.put("alpha","60");
					 
					 
					 jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);
				}	
				
				allvaluesinyears.remove("alpha");
				categoryJson.put("label", year.toString());	    	
				categoryArray.add(categoryJson);
				
				allvaluesinyears.put("name","Booking of "+year);
				
				Double booking_value=migratesevice.getBookingValue(countryname,year);
				allvaluesinyears.put("value",booking_value);
				allvaluesinyears.put("color",colorMap.get(countryname));
				allvaluesinyears.put("link","#/app/SCA_groupByArea");
				
				if(lastyear==year)
				{
					if(budget_value!=null  && booking_value!=null)
					{
					allvaluesinyears.put("tooltext","$label ,$seriesname,$datavalue <br>"+(df.format((booking_value/budget_value)*100))+"% of Budget");
					}
					 
				
			
					boookingvalues.add(booking_value);
				 
						
						
					 
				}
				jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);

			}
			keyforyears.put("data",jsonArrayofvaluesbasedonyeras);
			seriesobjArray.add(keyforyears);
		}
		
		keyforyears1.put("category",categoryArray);		
		keyforyears1.put("dataset",seriesobjArray);	
		
		ArrayList<Double>  percentages= new ArrayList<Double>();
		for(int i=0;i<coutries.size();i++)
		{
			if(lastyear>0)
			 {
				if((Double)budgetvalues.get(i)>0.0 && (Double)boookingvalues.get(i)> 0.0)
				{
					Double k=boookingvalues.get(i)/budgetvalues.get(i);
					Double v=k*100;
					percentages.add(v);
				}				 
				else
				{
					percentages.add(0.0);					
				}			
			 }
		}
		
		keyforyears1.put("per",percentages);

		return keyforyears1;
	}
	
	
	@RequestMapping(value = "/groupShareYtd", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   groupShareYtd() 
	{	
		try
		{
			
			Calendar now = Calendar.getInstance();
			int currentyear = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			int hour = now.get(Calendar.HOUR_OF_DAY);
			int minute = now.get(Calendar.MINUTE);
			int second = now.get(Calendar.SECOND);
			int millis = now.get(Calendar.MILLISECOND);
		JSONObject keyforyears = new JSONObject();
		JSONObject countryBoking2016 = null;
		JSONArray jsonArrayofCountryBooking2016 = new JSONArray();
		
		
		JSONObject countryBoking2013 = null;
		JSONArray jsonArrayofCountryBooking2013 = new JSONArray();
		JSONObject countryBoking2014 = null;
		JSONArray jsonArrayofCountryBooking2014 = new JSONArray();
		JSONObject countryBoking2015 = null;
		JSONArray jsonArrayofCountryBooking2015 = new JSONArray();
		
		
		
		ArrayList<String> al=new ArrayList<String>();
		ArrayList al2=new ArrayList();
		HashMap<String,String> hmColor=new HashMap<String,String>();

		List<Object[]> coutries=migratesevice.getCountries();
		
		for(Object[] countrydetails:coutries)
		{
			String countryname=(String)countrydetails[0];
			String color=(String)countrydetails[2];
			al2.add(color);
			al.add(countryname);
			hmColor.put(countryname, color);
		}	 
 
		for(String countryname:al)
		{
			 countryBoking2016 = new JSONObject();
			 Double budget_value2016=migratesevice.getBookingValue(countryname,currentyear);
			// DecimalFormat df = new DecimalFormat("#.##");
			 //String budget_valueStr=df.format(budget_value2016);
			 
			 countryBoking2016.put("name", countryname);
			 countryBoking2016.put("value", budget_value2016);	
			 countryBoking2016.put("color", hmColor.get(countryname));	
			 jsonArrayofCountryBooking2016.add(countryBoking2016);
			 
			 
			 countryBoking2013 = new JSONObject();
			 Double budget_value2013=migratesevice.getBookingValue(countryname,currentyear-3);
			 countryBoking2013.put("name", countryname);
			 countryBoking2013.put("value", budget_value2013);	
			 countryBoking2013.put("color", hmColor.get(countryname));	
			 jsonArrayofCountryBooking2013.add(countryBoking2013);
			 
			 countryBoking2014 = new JSONObject();
			 Double budget_value2014=migratesevice.getBookingValue(countryname,currentyear-2);
			 countryBoking2014.put("name", countryname);
			 countryBoking2014.put("value", budget_value2014);	
			 countryBoking2014.put("color", hmColor.get(countryname));	
			 jsonArrayofCountryBooking2014.add(countryBoking2014);
			 
			 countryBoking2015 = new JSONObject();
			 Double budget_value2015=migratesevice.getBookingValue(countryname,currentyear-1);
			 countryBoking2015.put("name", countryname);
			 countryBoking2015.put("value", budget_value2015);	
			 countryBoking2015.put("color", hmColor.get(countryname));	
			 jsonArrayofCountryBooking2015.add(countryBoking2015);
			 
			 
			 
		}
		
		keyforyears.put("data",jsonArrayofCountryBooking2016);	
		keyforyears.put("data1",jsonArrayofCountryBooking2013);	
		keyforyears.put("data2",jsonArrayofCountryBooking2014);	
		keyforyears.put("data3",jsonArrayofCountryBooking2015);	
		//keyforyears.put("colors",al2);		
		
		return keyforyears;	 	
		
		}catch(Exception e)
		{
			logger.info("======CONTROLLER METHOD=groupShareYtd====================================="+e.getCause());
			return null;
			
		}
	}

	@RequestMapping(value = "/productShareYtd", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   productShareYtd() 
	{ 
		try
		{
			
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			int hour = now.get(Calendar.HOUR_OF_DAY);
			int minute = now.get(Calendar.MINUTE);
			int second = now.get(Calendar.SECOND);
			int millis = now.get(Calendar.MILLISECOND);
		/* 
		 * DecimalFormat df = new DecimalFormat("#.##");
		JSONObject keyforyears = new JSONObject();
		
		JSONObject productBooking2016 = null;
		JSONArray jsonArrayofProductBooking2016 = new JSONArray();
		
		JSONObject productBooking2013 = null;
		JSONArray jsonArrayofProductBooking2013 = new JSONArray();
		
		JSONObject productBooking2014 = null;
		JSONArray jsonArrayofProductBooking2014 = new JSONArray();
		
		JSONObject productBooking2015 = null;
		JSONArray jsonArrayofProductBooking2015 = new JSONArray();
		ArrayList<String> al=new ArrayList<String>();
				
		List<Object[]> products=migratesevice.getProducts();
		
		for(Object[] product:products)
		{
			String productname=(String)product[0];
			al.add(productname);			
		}		
 
		 for(String productname:al)
		 {
			 
			 productBooking2016 = new JSONObject();
			 Double budget_value2016=migratesevice.getBookingValueForProductShare(productname,2016);			 
			 //String budget_valueStr=df.format(budget_value);
			 productBooking2016.put("label", productname);
			 productBooking2016.put("value", budget_value2016);			 
			 jsonArrayofProductBooking2016.add(productBooking2016);		 
			 
			 productBooking2013 = new JSONObject();
			 Double budget_value2013=migratesevice.getBookingValueForProductShare(productname,2013);			 
			 productBooking2013.put("label", productname);
			 productBooking2013.put("value", budget_value2013);			 
			 jsonArrayofProductBooking2013.add(productBooking2013);		
			 
			 productBooking2014 = new JSONObject();
			 Double budget_value2014=migratesevice.getBookingValueForProductShare(productname,2014);			 
			 productBooking2014.put("label", productname);
			 productBooking2014.put("value", budget_value2014);			 
			 jsonArrayofProductBooking2014.add(productBooking2014);		
			 
			 productBooking2015 = new JSONObject();
			 Double budget_value2015=migratesevice.getBookingValueForProductShare(productname,2015);			 
			 productBooking2015.put("label", productname);
			 productBooking2015.put("value", budget_value2015);			 
			 jsonArrayofProductBooking2015.add(productBooking2015);				 
		}*/
		
		JSONObject keyforyears = new JSONObject();
		int pastyears=year-3;
		
		for(int i=pastyears;i<=year;i++)
		{			
			List productData=migratesevice.getBookingValueForProductShareOpt(i,"NA");
			
			if(i==year)
				keyforyears.put("data",productData);			
			if(i==year-1)
				keyforyears.put("data1",productData);
			if(i==year-2)
				keyforyears.put("data2",productData);	
			if(i==year-3)
				keyforyears.put("data3",productData);	
		}
		
		return keyforyears;	 
		
		}catch(Exception e)
		{
			
			logger.info("======CONTROLLER METHOD=productShareYtd====================================="+e.getCause());
			return null;
			
		}
	}
	

	
	//-------------------------------------- Industry YTD @ Fusion Charts -------------------------------//
	
	@RequestMapping(value = "/industryShareYtd", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   industryShareYtd() 
	{
		 			
			JSONObject keyforyears = new JSONObject();
			/*JSONObject industrySectorBooking = null;
			JSONArray jsonArrayofIndustrySectorBooking = new JSONArray();
			ArrayList<String> al=new ArrayList<String>();
			ArrayList<String> al2=new ArrayList<String>();
			HashMap<String,String>hm=new HashMap<String,String>();
			
			JSONObject industryBooking = null;
	 		List<Object[]> industries=migratesevice.getIndustrieSectors();
			
			for(Object[] industry:industries)
			{
				String industrysectorname=(String)industry[0];
				String industrysectorcode=(String)industry[1];
				//String color=(String)countrydetails[2];
				//al2.add(color);
				al.add(industrysectorname);	
				al2.add(industrysectorcode);
				hm.put(industrysectorcode, industrysectorname);
			}	 
			 
			 for(String industrysectorcode:al2)
			 {
				 industryBooking = new JSONObject();
				 
				 industrySectorBooking= new JSONObject();
				 Double budget_value=migratesevice.getBookingValueForIndustrySectorShare(industrysectorcode,2016);
				 String ccname=hm.get(industrysectorcode).toString();
				 industryBooking.put("name",ccname);	
				 industryBooking.put("value", budget_value);
				 industryBooking.put("code", industrysectorcode);
				 
				 jsonArrayofIndustrySectorBooking.add(industryBooking);
			 }
			
			keyforyears.put("data",jsonArrayofIndustrySectorBooking);	*/

			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			JSONArray jsonArray3 = new JSONArray();
			JSONArray jsonArray4 = new JSONArray();
			JSONArray jsonArray = new JSONArray();
			
			JSONObject jsonObj = null;
			
			for(int i=2014;i<=2017;i++)
			{			
				List industrySectorData=migratesevice.getBookingValueForIndustrySectorShareOpt(i);
				
				if(i==2017)
					keyforyears.put("data",industrySectorData);					
				if(i==2014)
					keyforyears.put("data1",industrySectorData);
				if(i==2015)
					keyforyears.put("data2",industrySectorData);	
				if(i==2016)
					keyforyears.put("data3",industrySectorData);	
			}
			
			List industrySectorTableData=migratesevice.getBookingValueForIndustrySectorShareTableData("All");
			for(int i=0;i<industrySectorTableData.size();i++)
			{
				jsonObj = new JSONObject();
				Map mp=(Map)industrySectorTableData.get(i);
				Integer year=(Integer)mp.get("year");
				String name=(String)mp.get("name");
				Double val=(Double)mp.get("value");				
				
				if(year==2017)
				{
					jsonObj.put("year", year);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray1.add(jsonObj);
				}
				if(year==2016)
				{
					jsonObj.put("year", year);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray2.add(jsonObj);
				}
				if(year==2015)
				{
					jsonObj.put("year", year);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray3.add(jsonObj);
				}
				if(year==2014)
				{
					jsonObj.put("year", year);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray4.add(jsonObj);
				}
			}

			jsonArray.add(jsonArray1);
			jsonArray.add(jsonArray2);
			jsonArray.add(jsonArray3);
			jsonArray.add(jsonArray4);
			
			keyforyears.put("tableData",jsonArray);	
			
			return keyforyears;	 	
	 
	}
	
	
	@RequestMapping(value = "/industryShareYtdInner", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   industryShareYtdInner(HttpServletRequest req,HttpServletResponse resp) 
	{
		try
		{	
			JSONObject keyforyears = new JSONObject();
			JSONObject dataBooking = null;
			JSONArray jsonArrayofdataBooking = new JSONArray();

			List<Object[]> industrycodes=migratesevice.getIndustriesByIndustrySector(req.getParameter("industrysectorcode"));
		 		
	 		for(Object[] industrycode:industrycodes)
			{
				dataBooking = new JSONObject();
				String iname=(String)industrycode[0];
				String icode=(String)industrycode[1];
				Double budget_value_industry=migratesevice.getBookingValueForIndustryShare(icode,Integer.parseInt(req.getParameter("year")));					 		

				dataBooking.put("name", iname);
				dataBooking.put("value", budget_value_industry);
				
				jsonArrayofdataBooking.add(dataBooking);
			}
	 		keyforyears.put("data",jsonArrayofdataBooking);	
	 		
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			JSONArray jsonArray3 = new JSONArray();
			JSONArray jsonArray4 = new JSONArray();
			JSONArray jsonArray = new JSONArray();
			
			JSONObject jsonObj = null;
	 		
	 		List industrySectorTableData=migratesevice.getBookingValueForIndustrySectorShareTableData(req.getParameter("industrysectorcode"));
			for(int i=0;i<industrySectorTableData.size();i++)
			{
				jsonObj = new JSONObject();
				Map mp=(Map)industrySectorTableData.get(i);
				Integer year=(Integer)mp.get("year");
				String name=(String)mp.get("name");
				Double val=(Double)mp.get("value");				
				
				if(year==2017)
				{
					jsonObj.put("year", year);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray1.add(jsonObj);
				}
				if(year==2016)
				{
					jsonObj.put("year", year);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray2.add(jsonObj);
				}
				if(year==2015)
				{
					jsonObj.put("year", year);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray3.add(jsonObj);
				}
				if(year==2014)
				{
					jsonObj.put("year", year);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray4.add(jsonObj);
				}
			}

			jsonArray.add(jsonArray1);
			jsonArray.add(jsonArray2);
			jsonArray.add(jsonArray3);
			jsonArray.add(jsonArray4);
			
			keyforyears.put("tableData",jsonArray);	
			
			return keyforyears;	 	
		}
		catch(Exception e){
			logger.info("======CONTROLLER METHOD=industryShareYtdInner============="+e.getMessage());
			return null;
		}
	}
	
	//-------------------------------------- End Industry YTD @ Fusion Charts -------------------------------//
	
	@RequestMapping(value = "/groupByProduct", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject productvategoreryytd(HttpServletResponse res )  
	 {
		
		try
		{
	    JSONObject countryJson =null;
	    JSONArray categoryArray = new JSONArray();
	    JSONObject categoryObject = new JSONObject();
	    JSONObject seriesObjectBudjet = null;	    
	    JSONObject seriesObjectBooking =null;
	    JSONObject seriesObjectBudjet1 = null;	    
	    JSONObject seriesObjectBooking1 = null;
	    JSONArray seriesArrayBudjet = null;
	    JSONArray seriesArrayBooking = null;
	    JSONArray finalArray = new JSONArray();
	    JSONArray finalArray1 = new JSONArray();
	    JSONArray finalArray2 = new JSONArray();
	    JSONObject finalObject = new JSONObject();
	    JSONObject finalObject1 = new JSONObject();
	    JSONObject finalObject2 = new JSONObject();	    
	    HashMap<String,String> colorMap=new HashMap<String,String>();
	    
	    String[] stArray={"078f8f","d44a4a","8f4a8f","5c872a"};
	    
	    List<Object[]> coutries=migratesevice.getCountriesProductYTD();
		for(Object[] countrydetails:coutries)
	    {
			String countryname=(String)countrydetails[0];
			countryJson = new JSONObject();
			countryJson.put("label", countryname);	    	
			categoryArray.add(countryJson);
	    }
	    	
		int i=0;
   		List<Object[]> catageroies=migratesevice.getAllCatagories();
   		for(Object[] catagory:catageroies)
  	    {
   			
   			String categoreycode=(String)catagory[0];
   			colorMap.put(categoreycode, stArray[i]);
   			i++;
  	    }
   		
    	for(Object[] catagoery:catageroies)
  	    {
    		seriesArrayBudjet = new JSONArray();
    	    seriesArrayBooking = new JSONArray();
    	    seriesObjectBudjet = new JSONObject();	    
    	    seriesObjectBooking = new JSONObject();
    	    
    		String categoreycode=(String)catagoery[0];
    		String catagoreyname=(String)catagoery[1];
    		
    		seriesObjectBudjet.put("seriesname", "Budget "+catagoreyname);
    		seriesObjectBooking.put("seriesname", "Booking "+catagoreyname);
    		
    		for(Object[] countrydetails:coutries)
    	    {
    			seriesObjectBudjet1 = new JSONObject();	    
    		    seriesObjectBooking1 = new JSONObject();
    		    
    			String countryname=(String)countrydetails[0];  
    			float budgetgvalue=migratesevice.getBudgetValue(countryname,catagoreyname);
    			seriesObjectBudjet1.put("value", budgetgvalue);
    			seriesObjectBudjet1.put("alpha", "60");
    			seriesObjectBudjet1.put("code", categoreycode);
    			seriesObjectBudjet1.put("color", colorMap.get(categoreycode));
    			
    			seriesArrayBudjet.add(seriesObjectBudjet1);
    			float bookingvalue=migratesevice.getBookingValue(countryname,catagoreyname);
    			seriesObjectBooking1.put("value", bookingvalue);
    			seriesObjectBooking1.put("code", categoreycode);
    			seriesObjectBooking1.put("color", colorMap.get(categoreycode));
    			seriesObjectBooking1.put("link", "#/app/groupByProduct");

    			seriesArrayBooking.add(seriesObjectBooking1);			
    	    }
    		seriesObjectBudjet.put("data", seriesArrayBudjet);
    		seriesObjectBooking.put("data", seriesArrayBooking);
    		
    		finalArray.add(seriesObjectBudjet);
    		finalArray1.add(seriesObjectBooking);
    		
  	    }
    	finalObject1.put("dataset", finalArray);
    	finalObject2.put("dataset", finalArray1);
    	finalArray2.add(finalObject1);
    	finalArray2.add(finalObject2);
    	finalObject.put("category", categoryArray);
    	finalObject.put("data", finalArray2);
    	
    	logger.info("-----------OBJECT-------------"+finalObject.toString());
    	
    	
    	return finalObject;	 	
		}catch(Exception e)
		{
			logger.info("=========Controller Method=industryShareYtdInner ========"+e.getCause());
			return null;
		}
	 }
	
	@RequestMapping(value = "/groupByProductInner", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   groupByProductInner(HttpServletRequest req,HttpServletResponse resp) 
	{
		try
		{	
			logger.info("-----------REQUEST-------------"+req.getParameter("categorycode")+"-----------"+req.getParameter("countryname"));
			JSONObject productJson = null;
			JSONObject keyObj = new JSONObject();
			
			JSONArray jsonArrayofproductBooking = new JSONArray();;
			JSONArray categoryArray = new JSONArray();
			

			List<String> productss=migratesevice.getAllProductsOnCatgeorycode(req.getParameter("categorycode"));
			JSONObject productCatJson = new JSONObject();
			
			
			for(String product:productss)
	    	{
				productJson = new JSONObject();
				
				productCatJson = new JSONObject();
				productCatJson.put("label", product);
				categoryArray.add(productCatJson);				
	    		
	    		float Bookingvalueforproduct=migratesevice.getBookingsForProduct(product,req.getParameter("countryname"),req.getParameter("categorycode"));
	    		
	    		productJson.put("label", product);
	    		productJson.put("value", Bookingvalueforproduct);
	    		
	    		jsonArrayofproductBooking.add(productJson);

	    	}
			
			keyObj.put("data", jsonArrayofproductBooking);
			keyObj.put("category",categoryArray);	
			logger.info("-----------OBJECT-------------"+keyObj.toString());
			return keyObj;	 	
		}
		catch(Exception e){
			logger.info("=============CONTROLLER METHOD=groupByProductInner==============="+e.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value = "/bookingVsSales", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject bookingVsSales(HttpServletResponse res )  
	 {	
		
		try{
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();		
		JSONObject subobject=null;
		DecimalFormat df = new DecimalFormat("#.##");
		
		JSONObject catbOject = new JSONObject();
		JSONArray catArray = new JSONArray();	
		
		JSONObject series1 = new JSONObject();
		JSONObject series2 = new JSONObject();
		
		
		JSONArray sArray1 = new JSONArray();		
		
		JSONArray sArray2 = new JSONArray();	
		
		series1.put("seriesname","BOOKINGS");
		series2.put("seriesname","SALES");
		
		HashMap<Integer,String> monthMap=new HashMap<Integer,String>();
		monthMap.put(1, "Jan");
		monthMap.put(2, "Feb");
		monthMap.put(3, "Mar");
		monthMap.put(4, "Apr");
		monthMap.put(5, "May");
		monthMap.put(6, "Jun");
		monthMap.put(7, "Jul");
		monthMap.put(8, "Aug");
		monthMap.put(9, "Sep");
		monthMap.put(10, "Oct");
		monthMap.put(11, "Nov");
		monthMap.put(12, "Dec");
		
		List<Integer> months=migratesevice.getMonths();
		
		for(Integer monthid:months)
	    {
			subobject=new JSONObject();
			catbOject = new JSONObject();
			
			catbOject.put("label", monthMap.get(monthid));
			catArray.add(catbOject);
			
	    	Double bookingValue=migratesevice.getBookingValueByMonth(monthid,2016);
	    	subobject.put("value", bookingValue);
	    	sArray1.add(subobject);
	    	
	    	Double SalesValue=migratesevice.getSalesValueByMonth(monthid,2016);
	    	subobject.put("value", SalesValue);
	    	sArray2.add(subobject);
	    		    	
	    	//String booking_valueStr=df.format(bookingValue);
	    	// String sales_valueStr=df.format(SalesValue);

	    	
	    }
	   
		series1.put("data", sArray1);
		series2.put("data", sArray2);
		jsonArray.add(series1);
		jsonArray.add(series2);
		
		mainobject.put("dataset",jsonArray);
		mainobject.put("category",catArray);
		return mainobject;
		
		}catch(Exception e)
		{
			logger.info("========CONTROLLER METHOD=bookingVsSales==========="+e.getCause());
			return null;
		}
	}


	@RequestMapping(value = "/cwQwarterlyBooking", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject cwQwarterlyBooking(HttpServletResponse res )  
	 {	
		try
		{
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();		
		DecimalFormat df = new DecimalFormat("#.##");
		String quarterly= null;
		JSONObject series1 = new JSONObject();
		JSONObject series2 = new JSONObject();
		JSONObject series3 = new JSONObject();
		JSONObject series4 = new JSONObject();
		
		JSONObject sdata =null;
		JSONObject sdata1 = new JSONObject();
		JSONObject sdata2 = new JSONObject();
		JSONObject sdata3 = new JSONObject();
		JSONObject sdata4 = new JSONObject();
		
		
		
		JSONArray sArray1 = new JSONArray();	
		JSONArray sArray2 = new JSONArray();
		JSONArray sArray3 = new JSONArray();
		JSONArray sArray4 = new JSONArray();
		
		series1.put("seriesname", "2013");
		series1.put("color", "008ee4");
		
		series2.put("seriesname", "2014");
		series2.put("color", "f8bd19");
		
		series3.put("seriesname", "2015");
		series3.put("color", "6baa01");
		
		series4.put("seriesname", "2016");
		series4.put("color", "e44a00");
		
		
		for(int i=2013;i<2017;i++)
		{
			sdata = new JSONObject();
			if(i==2013)
			{
					quarterly="1,2,3";
					Double bookingValue1=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
					sdata.put("value", bookingValue1);
					sArray1.add(sdata);
					
					quarterly="4,5,6";
					Double bookingValue2=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
					sdata.put("value", bookingValue2);
					sArray1.add(sdata);
					
					quarterly="7,8,9";
					Double bookingValue3=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
					sdata.put("value", bookingValue3);
					sArray1.add(sdata);
					
					quarterly="10,11,12";
					Double bookingValue4=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
					sdata.put("value", bookingValue4);
					sArray1.add(sdata);					
					
			}
			else if(i==2014)
			{					
				quarterly="1,2,3";
				Double bookingValue1=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				sdata.put("value", bookingValue1);
				sArray2.add(sdata);
				
				quarterly="4,5,6";
				Double bookingValue2=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				sdata.put("value", bookingValue2);
				sArray2.add(sdata);
				
				quarterly="7,8,9";
				Double bookingValue3=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				sdata.put("value", bookingValue3);
				sArray2.add(sdata);
				
				quarterly="10,11,12";
				Double bookingValue4=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				sdata.put("value", bookingValue4);
				sArray2.add(sdata);
			}				
			else if(i==2015)
			{
				quarterly="1,2,3";
				Double bookingValue1=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				sdata.put("value", bookingValue1);
				sArray3.add(sdata);
				
				quarterly="4,5,6";
				Double bookingValue2=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				sdata3.put("value", bookingValue2);
				sArray3.add(sdata);
				
				quarterly="7,8,9";
				Double bookingValue3=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				sdata.put("value", bookingValue3);
				sArray3.add(sdata);
				
				quarterly="10,11,12";
				Double bookingValue4=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				sdata.put("value", bookingValue4);
				sArray3.add(sdata);
			}
			else 
			{
				quarterly="1,2,3";
				Double bookingValue1=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				/*BigDecimal b1=migratesevice.getCWQuarterlyBookingValue1(quarterly,i);*/
				sdata.put("value",bookingValue1);
				sArray4.add(sdata);
				
				quarterly="4,5,6";
				Double bookingValue2=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				/*BigDecimal b2=migratesevice.getCWQuarterlyBookingValue1(quarterly,i);*/
				sdata.put("value",bookingValue2);
				sArray4.add(sdata);
				
				quarterly="7,8,9";
			Double bookingValue3=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				/*BigDecimal b3=migratesevice.getCWQuarterlyBookingValue1(quarterly,i);*/
				sdata.put("value",bookingValue3);
				sArray4.add(sdata);
				
				quarterly="10,11,12";
				Double bookingValue4=migratesevice.getCWQuarterlyBookingValue(quarterly,i);
				/*BigDecimal b4=migratesevice.getCWQuarterlyBookingValue1(quarterly,i);*/
				sdata.put("value",bookingValue4);
				sArray4.add(sdata);
			}
		}
	   
		series1.put("data",sArray1);
		series2.put("data",sArray2);
		series3.put("data",sArray3);
		series4.put("data",sArray4);
		jsonArray.add(series1);
		jsonArray.add(series2);
		jsonArray.add(series3);
		jsonArray.add(series4);
		
		mainobject.put("dataset",jsonArray);
		return mainobject;
		
		}catch(Exception e)
		{
			logger.info("===========CONTROLLER METHOD=cwQwarterlyBooking==========="+e.getCause());
			return null;
		}
	}
	
	
	
	@RequestMapping(value = "/ytdcodeDrilldownLevelOne", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject ytdcodeDrilldownLevelOne(HttpServletRequest req,HttpServletResponse res )  
	 {	
		try
		{
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		DecimalFormat df = new DecimalFormat("#.##");	
		JSONObject subobject=new JSONObject();
		
		String countryCode=req.getParameter("countryCode");
		Integer year = Integer.parseInt(req.getParameter("year"));
		HttpSession session=req.getSession(false);
		session.setAttribute("curyear", year);
		//List<String> regions=migratesevice.getRegions(countryCode);
		
		List<Object[]> regionandregionids=migratesevice.getRegionsandregionids(countryCode);
		
		if(regionandregionids.size()==0)
		{
			Double bookingValue = migratesevice.getBookingValueForRegion("NOREGION", countryCode,year);
			subobject.put("value", df.format(bookingValue));
			subobject.put("label", "NA");
			subobject.put("link", "#/app/SCA_groupByArea");
			jsonArray.add(subobject);
		}
		for(Object[] region:regionandregionids)
		{			
			//subobject=new JSONObject();
			
			Integer reg=(Integer)region[1];
			String regioncode=reg.toString();
			
	    	Double bookingValue=migratesevice.getBookingValueForRegion(regioncode,countryCode,year);
	    	subobject.put("value",df.format(bookingValue));
	    	subobject.put("label",(String)region[0]);
	    	subobject.put("link","#/app/SCA_groupByArea");
	    	jsonArray.add(subobject);
	    	
	    	//jsonArray.add(subobject1);
		}
		

		/*subobject2.put("seriesname","Region");
		subobject2.put("data",jsonArray);
		jsonArray2.add(subobject2);			
		subobject4.put("category", jsonArray1);
		jsonArray3.add(subobject4);	
		
		//subobject3.put("xaxis",jsonArray3);		
		mainobject.put("xaxis",jsonArray3);*/		
		
		mainobject.put("data",jsonArray);
		ArrayList<Double>  percentages= new ArrayList<Double>();
		for(int percentage=0;percentage<=jsonArray.size();percentage++)
		{
			percentages.add(0.0);
		}
		
		mainobject.put("per",percentages);
		
		return mainobject;
		
		}catch(Exception e)
		{
			logger.info("===========CONTROLLER METHOD=ytdcodeDrilldownLevelOne========"+e.getCause());;
			return null;
		}
	}
	
	@RequestMapping(value = "/ytdcodeDrilldownLevelTwo", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject ytdcodeDrilldownLevelTwo(HttpServletRequest req,HttpServletResponse res )  
	 {	
		try
		{
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		DecimalFormat df = new DecimalFormat("#.##");	
		JSONObject subobject=new JSONObject();
		
		String regionCode=req.getParameter("regionCode");
		String countryname=req.getParameter("countryCode");
		
		HttpSession session=req.getSession(false);
		Integer year=(Integer)session.getAttribute("curyear");
		
		String countrycode=migratesevice.getCountryName(countryname);
		String regioncode=migratesevice.getRegionsandregionids1(regionCode,countrycode);
		
		//List<String> salesmen=migratesevice.getSalesMen(countryCode,regionCode);
		List<String> salesman1=migratesevice.getSalesman1(countryname,regionCode);
		
		for(String salesman:salesman1)
		{			
			//subobject=new JSONObject();
			
	    	Double bookingValue=migratesevice.getBookingValueForSalesMan(salesman,regioncode,countryname,year);
	    	if(bookingValue!=0.0)
	    	{
	    		Salesman sman=(Salesman)hibernateDao.getRowByColumn(Salesman.class, "salesmancode", salesman);
		    	subobject.put("value",df.format(bookingValue));
		    	subobject.put("label",salesman+"-"+sman.getIni());
		    	jsonArray.add(subobject);
	    	}
	    	
	    	//jsonArray.add(subobject1);
		}
		

		/*subobject2.put("seriesname","Region");
		subobject2.put("data",jsonArray);
		jsonArray2.add(subobject2);			
		subobject4.put("category", jsonArray1);
		jsonArray3.add(subobject4);	
		
		//subobject3.put("xaxis",jsonArray3);		
		mainobject.put("xaxis",jsonArray3);*/		
		
		mainobject.put("data",jsonArray);
		
		ArrayList<Double>  percentages= new ArrayList<Double>();
		for(int percentage=0;percentage<=jsonArray.size();percentage++)
		{
			percentages.add(0.0);
		}
		
		mainobject.put("per",percentages);
		
		
		
		
		return mainobject;
		
		}catch(Exception e)
		{
			logger.info("===========CONTROLLER METHOD=ytdcodeDrilldownLevelOne========"+e.getCause());;
			return null;
		}
	}
	
	@RequestMapping(value = "/historyBookingByYear", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject historyBooking(HttpServletResponse res )  
	 {	
		try
		{
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();		
		JSONObject subobject=null;
		DecimalFormat df = new DecimalFormat("#.##");
		
		for(int i=2003;i<2017;i++)
		{			
			subobject=new JSONObject();
			
	    	Double bookingValue=migratesevice.getBookingValueByYear(i);
	    	//String booking_valueStr=df.format(bookingValue);
	    	 
			subobject.put("label",String.valueOf(i));	    	
			subobject.put("value", bookingValue);
			subobject.put("color", "6495ED");
			
			if(i==2016)
				subobject.put("color", "fdd400");
			
			jsonArray.add(subobject);
		}
	   
		mainobject.put("data",jsonArray);
		return mainobject;
		
		}catch(Exception e)
		{
			logger.info("===========CONTROLLER METHOD=historyBooking========"+e.getCause());;
			return null;
		}
	}

	
	
	

	@RequestMapping(value = "/monthleyytdtoarea", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject monthleYtdToArea(HttpServletRequest req,HttpServletResponse res)  
	 {	
		try
		{
		String link=req.getParameter("link");
		
		HashMap monthname=new HashMap();
		monthname.put(1,"Jan");
		monthname.put(2,"Feb");
		monthname.put(3,"Mar");
		monthname.put(4,"Apr");
		monthname.put(5,"May");
		monthname.put(6,"Jun");
		monthname.put(7,"Jul");
		monthname.put(8,"Aug");
		monthname.put(9,"Sep");
		monthname.put(10,"Oct");
		monthname.put(11,"Nov");
		monthname.put(12,"Dec");
		
	 
		
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		JSONArray mainjsonArray = new JSONArray();
		JSONObject months=null;
		
		List<Integer> monthnumbers =migratesevice.getDistinctMonthNumbers();
		
		for(int i=1;i<=12;i++)
		{
			months=new JSONObject();
			 months.put("label",(String)monthname.get(i));
			 jsonArray.add(months);
			
		}
		
		JSONObject subobject=new JSONObject();
		subobject.put("category",jsonArray);
		mainjsonArray.add(subobject);
		
		 
		mainobject.put("xaxis",mainjsonArray);
		
		
		
		
		
		
		List<Object[]> coutries=migratesevice.getCountries();
		ArrayList<String> countryarrayList=new ArrayList<String>();
	    List <Object[]> budgetofallmonths=migratesevice.getBudgetofallmonths();
		HashMap budgetamountformonths=new HashMap(); 
		for(Object [] budmonth:budgetofallmonths)
		{
			ArrayList amd=new ArrayList();
			amd.add((String)budmonth[0]);
			amd.add((Double)budmonth[1]);
			amd.add((Double)budmonth[2]);
			amd.add((Double)budmonth[3]);
			amd.add((Double)budmonth[4]);
			amd.add((Double)budmonth[5]);
			amd.add((Double)budmonth[6]);
			amd.add((Double)budmonth[7]);
			amd.add((Double)budmonth[8]);
			amd.add((Double)budmonth[9]);
			amd.add((Double)budmonth[10]);
			amd.add((Double)budmonth[11]);
			amd.add((Double)budmonth[12]);
			budgetamountformonths.put((String)budmonth[0],amd);			 
			
			
		}
		
		
		
		
		JSONObject aboutallcountriesbudget=new JSONObject();
		JSONArray countryjsonbudgetArray = new JSONArray();
		
		for(Object[] countrydetails:coutries)
			
		{
			JSONObject aboutcountry=new JSONObject();
			JSONArray countryjsonArray = new JSONArray();
			
			String countryname=(String)countrydetails[0];
			String color=(String)countrydetails[1];
			
			ArrayList abb=(ArrayList)budgetamountformonths.get(countryname);
			for(int i=1;i<=12;i++)
			{
				JSONObject aboutcountrybudgetvalue=new JSONObject();
				Double d=(Double)abb.get(i);
				aboutcountrybudgetvalue.put("value",d);
				countryjsonArray.add(aboutcountrybudgetvalue);
				
		
				
			}
			
			aboutcountry.put("seriesname","Budget "+countryname);
			aboutcountry.put("color",color);
			aboutcountry.put("data",countryjsonArray);
			
			countryjsonbudgetArray.add(aboutcountry);
			
			
		}
		
	 aboutallcountriesbudget.put("dataset",countryjsonbudgetArray);
		
		
//*****************************************************************************//*
		
		
		JSONObject aboutallcountriesbooking=new JSONObject();
		JSONArray countryjsonbookingArray = new JSONArray();
		
		
		for(Object[] countrydetails:coutries)
		{
			JSONObject aboutcountry=new JSONObject();
			JSONArray countryjsonArray = new JSONArray();
			
			String countryname=(String)countrydetails[0];
			String color=(String)countrydetails[2];
			
			List<Object[]> boookingsofmonth=migratesevice.getBookingsOfMonthley(countryname);
			for(Object[] bookingvalues:boookingsofmonth)
			{
				JSONObject aboutcountrybookingvalue=new JSONObject();
				/*Double d=migratesevice.getBookingvaluesforMonthleyYtd(i,countryname);*/
				Double d=(Double)bookingvalues[0];
				aboutcountrybookingvalue.put("value",d);
				if(link.equalsIgnoreCase("product"))					
					aboutcountrybookingvalue.put("link","#/app/countryBookingYTD");
				//aboutcountrybookingvalue.put("alpha","0");
				countryjsonArray.add(aboutcountrybookingvalue);
			}
			
			
			aboutcountry.put("seriesname","Booking "+countryname);
			aboutcountry.put("color",color);
			aboutcountry.put("data",countryjsonArray);
			
			countryjsonbookingArray.add(aboutcountry);
		}
		aboutallcountriesbooking.put("dataset",countryjsonbookingArray);
		
		
		
		
		 
		
		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		
		
		budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
		budgetandbookingarrayforallcountries.add(aboutallcountriesbooking);
		 
		 
		String monthString=null;
		JSONObject tableObject=new JSONObject();
		JSONArray tabletArray = new JSONArray();
		
		for(int i=2013;i<=2015;i++)
		{
			tableObject=new JSONObject();
			tableObject.put("year", i);
			
			for(int j=1;j<=4;j++)
			{
				
				if(j==1)
				{
					monthString="1,2,3";
					Double value=migratesevice.getQuarterlyBooking(i,monthString);					
					tableObject.put("Q1", value);
				}
				if(j==2)
				{
					monthString="4,5,6";
					Double value=migratesevice.getQuarterlyBooking(i,monthString);
					tableObject.put("Q2", value);
				}
				if(j==3)
				{
					monthString="7,8,9";
					Double value=migratesevice.getQuarterlyBooking(i,monthString);
					tableObject.put("Q3", value);
				}
				if(j==4)
				{
					monthString="10,11,12";
					Double value=migratesevice.getQuarterlyBooking(i,monthString);
					tableObject.put("Q4", value);
				}
				
				
				
			}
			tabletArray.add(tableObject);
			
			
		}
		
		
		
		
		
		
		
		mainobject.put("data",budgetandbookingarrayforallcountries);
		mainobject.put("tabledata",tabletArray);
		
	/*	logger.info("ttttttttttttttttttttttttttttt------>"+mainobject.get("data"));
		
		logger.info("xaissssssssssssssssssssssssssssssssss---------->"+mainobject.get("xaxis"));
		
		
		*/
		logger.info("ttttttttttttttttttttttttttttt------>"+budgetamountformonths.keySet());
		
		
		
		 
		return mainobject;
		
		}catch(Exception e)
		{
			logger.info("==========CONTROLLER METHOD"
					+ "=monthleYtdToArea=============="+e.getCause());
			return null;
		}
	}

	
	

	@RequestMapping(value = "/monthleyytdtocatageory", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject monthleYtTocatageory(HttpServletResponse res,HttpServletRequest request )  
	 {	
		
		try{
		String country=request.getParameter("country");
		String country2=country.substring(country.lastIndexOf(" ") + 1);
		String country1=country2.trim();
		String mo=request.getParameter("month");
		
		
		/*For getting months names we are using hashmap for stroing month name in string*/
		
		HashMap monthname=new HashMap();
		monthname.put(1,"Jan");
		monthname.put(2,"Feb");
		monthname.put(3,"Mar");
		monthname.put(4,"Apr");
		monthname.put(5,"May");
		monthname.put(6,"Jun");
		monthname.put(7,"Jul");
		monthname.put(8,"Aug");
		monthname.put(9,"Sept");
		monthname.put(10,"Oct");
		monthname.put(11,"Nov");
		monthname.put(12,"Dec");
		
		
		List<Object[]> aboutcatgories=migratesevice.getAllDetailsAboutCatageory();
		
		 
		
	/*Getting month numbers from data base*/	 
		
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		JSONArray mainjsonArray = new JSONArray();
		JSONObject months=null;
		
		/*List<Integer> monthnumbers =migratesevice.getDistinctMonthNumbers();*/
		
		for(int i=1;i<=12;i++)
		{
			months=new JSONObject();
			 months.put("label",(String)monthname.get(i));
			 jsonArray.add(months);
			
		}
		
		JSONObject subobject=new JSONObject();
		subobject.put("category",jsonArray);
		mainjsonArray.add(subobject);
		
		/*mainobject is used for keeping months on xaxis*/
		mainobject.put("xaxis",mainjsonArray);
		
		
		
		
		 List<Object[]> budgetofcatageories=migratesevice.getAllcatagoriesBasedOncountry(country1);
		 HashMap budgetofcatageoriesvalues=new HashMap();
		 for(Object[] boc:budgetofcatageories)
		 {
			 ArrayList al=new ArrayList();
			 al.add((String)boc[0]);
			 al.add((Double)boc[1]);
			 al.add((Double)boc[2]);
			 al.add((Double)boc[3]);
			 al.add((Double)boc[4]);
			 al.add((Double)boc[5]);
			 al.add((Double)boc[6]);
			 al.add((Double)boc[7]);
			 al.add((Double)boc[8]);
			 al.add((Double)boc[9]);
			 al.add((Double)boc[10]);
			 al.add((Double)boc[11]);
			 al.add((Double)boc[12]);
			 budgetofcatageoriesvalues.put((String)boc[0],al);
			 
		 }
		 
		 logger.info("the key values are --------------------------------->"+budgetofcatageoriesvalues.keySet());	
		
		
		/*Logic for all countries budget*/
		
		JSONObject aboutallcountriesbudget=new JSONObject();
		JSONArray countryjsonbudgetArray = new JSONArray();
		
		for(Object[] catageoreydetails:aboutcatgories)
		{
			JSONObject aboutcountry=new JSONObject();
			JSONArray countryjsonArray = new JSONArray();
			
			String catageoryname=(String)catageoreydetails[0];
			String color=(String)catageoreydetails[2];
			
			Set productcatagories=budgetofcatageoriesvalues.keySet();
			
			if(productcatagories.contains(catageoryname))
			{
				ArrayList abb=(ArrayList)budgetofcatageoriesvalues.get(catageoryname);
				for(int i=1;i<=12;i++)
				{
					
				JSONObject aboutcountrybudgetvalue=new JSONObject();
				Double d= (Double)abb.get(i);
		        aboutcountrybudgetvalue.put("value",d);
		        countryjsonArray.add(aboutcountrybudgetvalue);
				}
			 
			}
			else
			{
				
				
				for(int i=1;i<=12;i++)
				{
					
				JSONObject aboutcountrybudgetvalue=new JSONObject();
				aboutcountrybudgetvalue.put("value",null);
		        countryjsonArray.add(aboutcountrybudgetvalue);
				}
				
			}
			aboutcountry.put("seriesname","Budget "+catageoryname);
			aboutcountry.put("color",color);
			aboutcountry.put("data",countryjsonArray);
			
			countryjsonbudgetArray.add(aboutcountry);
		}	 
		
		
		
		
		aboutallcountriesbudget.put("dataset",countryjsonbudgetArray);
		
		
/*****************************************************************************/
		
		/*logic for all countries booking*/
		JSONObject aboutallcountriesbooking=new JSONObject();
		JSONArray countryjsonbookingArray = new JSONArray();
		
		
		for(Object[] catageoreydetails:aboutcatgories)
		{
			JSONObject aboutcountry=new JSONObject();
			JSONArray countryjsonArray = new JSONArray();
			
			String catageoryname=(String)catageoreydetails[0];
			String color=(String)catageoreydetails[1];
			List<Object[]> bookingofproductcatagories=migratesevice.getbookingofproductcatagories(country1,catageoryname);
			for(Object[] bopc:bookingofproductcatagories)
			{
				JSONObject aboutcountrybookingvalue=new JSONObject();
				/*Double d=migratesevice.getBookingvaluesforMonthleyYtdToCatageorey(i,catageoryname,country1);*/
				Double d=(Double)bopc[0];
				aboutcountrybookingvalue.put("value",d);
				aboutcountrybookingvalue.put("link","#/app/countryBookingYTD");
				
				countryjsonArray.add(aboutcountrybookingvalue);
			}
			
			
			aboutcountry.put("seriesname","Booking "+catageoryname);
			aboutcountry.put("color",color);
			aboutcountry.put("data",countryjsonArray);
			
			countryjsonbookingArray.add(aboutcountry);
		}
		aboutallcountriesbooking.put("dataset",countryjsonbookingArray);
		
		/*****************************************************************************/
		
		
		/*Adding both the json objects in common array*/
		
		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		
		
		budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
		budgetandbookingarrayforallcountries.add(aboutallcountriesbooking);
		 
		/* kepping the aray values in the main json object*/
		
		mainobject.put("data",budgetandbookingarrayforallcountries);
		
		/*logger.info("ttttttttttttttttttttttttttttt------>"+mainobject.get("data"));
		
		logger.info("xaissssssssssssssssssssssssssssssssss---------->"+mainobject.get("xaxis"));
		*/
		
		 
		 
		
		return mainobject;
		
		}catch(Exception e)
		{
			logger.info("===========CONTROLLER METHOD=monthleYtTocatageory============="+e.getCause());
			return null;
		}
	}

	@RequestMapping(value = "/monthleyytdtoproduct", method = RequestMethod.GET)	
	public @ResponseBody JSONObject monthleYtToProduct(HttpServletResponse res,HttpServletRequest request )  
	 {	
		try
		{
		
		 
		String country=request.getParameter("country");
		String country2=country.substring(country.lastIndexOf(" ") + 1);
		String country1=country2.trim();
		
		
		String catageorey=request.getParameter("catageorey");
		String catageorey2=catageorey.substring(country.lastIndexOf(" ") + 1);
		String catageorey1=catageorey2.trim();
		
		String mo=request.getParameter("month");
		
		 
		
		
		/*For getting months names we are using hashmap for stroing month name in string*/
		
		HashMap monthname=new HashMap();
		monthname.put(1,"January");
		monthname.put(2,"February");
		monthname.put(3,"March");
		monthname.put(4,"April");
		monthname.put(5,"May");
		monthname.put(6,"June");
		monthname.put(7,"July");
		monthname.put(8,"August");
		monthname.put(9,"September");
		monthname.put(10,"Octomber");
		monthname.put(11,"November");
		monthname.put(12,"December");
		
		HashMap monthnumber=new HashMap();
		monthnumber.put("Jan",1);
		monthnumber.put("Feb",2);
		monthnumber.put("Mar",3);
		monthnumber.put("Apr",4);
		monthnumber.put("May",5);
		monthnumber.put("Jun",6);
		monthnumber.put("Jul",7);
		monthnumber.put("Aug",8);
		monthnumber.put("Sep",9);
		monthnumber.put("Oct",10);
		monthnumber.put("Nov",11);
		monthnumber.put("Dec",12);
		
		
		
	 /*Getting month numbers from data base*/	 
		
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		JSONArray mainjsonArray = new JSONArray();
		
		List<Object[]> products=migratesevice.getAllProductsBasedOnCatageoreyName(catageorey1);
		
		for(Object[] p:products)
		{
			JSONObject subobject = new JSONObject();
			String productnmae=(String)p[0];
			String productcolor=(String)p[1];
			Double d=migratesevice.getproductValue((Integer)monthnumber.get(mo),country1,productnmae);
			subobject.put("value",d);
			subobject.put("label",productnmae);
			jsonArray.add(subobject);
			
			
		}
		
		mainobject.put("data",jsonArray);
		
	 		
		return mainobject;
		}catch(Exception e)
		{
			logger.info("======================CONTROLLER METHOD=monthleYtToProduct=============="+e.getCause());
			return null;
		}
	}
	
	
	
	@RequestMapping(value = "/otdPieChart", method = RequestMethod.GET)
	public  @ResponseBody JSONObject  otdPieChart() 
	{
		JSONObject keyforyears = new JSONObject();
		JSONArray jsonArray = new JSONArray();			
		JSONObject otdCount = null;
		HashMap<Integer,String> labelMap=new HashMap<Integer,String>();
		
		try
		{			
			
			labelMap.put(0,"On Time");
			labelMap.put(1,"1 Week");
			labelMap.put(2,"2 Weeks");
			labelMap.put(3,"3 weaks And Above Late");
			 
			 Map<Integer,Long> hashMap=migratesevice.getOTDPie();
			 
			 for(int i=0;i<hashMap.size();i++)
			 {
				 otdCount = new JSONObject();

				 otdCount.put("label",labelMap.get(i));	
				 otdCount.put("value", hashMap.get(i));
				 
				 jsonArray.add(otdCount);
			 }
				 
			
			keyforyears.put("data",jsonArray);	
			
			return keyforyears;	 	
		}
		catch(Exception e){
			logger.info("==============CONTROLLER METHOD=otdPieChart========"+e.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value = "/otdRequestDate", method = RequestMethod.GET)
	public  @ResponseBody JSONObject  otdRequestDate() 
	{
		JSONObject keyforyears = new JSONObject();
		
		try
		{			
			 
			 List reqDate=migratesevice.getRequestDate();
			
			 keyforyears.put("data",reqDate);	
			
			return keyforyears;	 	
		}
		catch(Exception e){
			logger.info("=================CONTROLLER METHOD=otdRequestDate"+e.getMessage());
			return null;
		}
	}
	
	
	/*@RequestMapping(value = "/volumeByQTY", method = RequestMethod.GET)	
	public @ResponseBody JSONObject monthleYtToProdut(HttpServletResponse res,HttpServletRequest request )  
	 {	
	 
		String color[]={"#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222","#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222"};
		
		JSONObject mainobject = new JSONObject();
		
		List<String> vendornames=migratesevice.getVendorNames();
		
		JSONArray vendorarray= new JSONArray();
		for(String vendors:vendornames)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("label", vendors);
			vendorarray.add(vendorobject);
			
		}
		JSONObject vendorcatgories = new JSONObject();
		vendorcatgories.put("category", vendorarray);
		JSONArray vendorvatageoreyarray= new JSONArray();
		vendorvatageoreyarray.add(vendorcatgories);
		mainobject.put("vendors",vendorvatageoreyarray);
		
		
		
		List<String> productnames=migratesevice.getproductlines();
		JSONArray productdataarrayandvendorarray= new JSONArray();
		int i=0;
		
		for(String product:productnames)
		{
			JSONObject productlineandvendor = new JSONObject();
			JSONArray productdataarray= new JSONArray();
			List<Object[]> vendorsbookingwithproduct= migratesevice.vendorsbookingwithproduc(product);
			HashMap vendorsinqty=new HashMap();
			
			for(Object[] vendor:vendorsbookingwithproduct)
				
			{
				
				vendorsinqty.put((String)vendor[0],(Double)vendor[1]);
				
			}
			
			Set vendorss=vendorsinqty.keySet();
			
			for(String vendors:vendornames)
			{
				
				if(vendorss.contains(vendors))
				{
				JSONObject productline = new JSONObject();
				
				Double d=migratesevice.getinqty(product,vendors);
				
				productline.put("value",(Double)vendorsinqty.get(vendors));
				productdataarray.add(productline);
				
				}
				
				else
				{
					JSONObject productline = new JSONObject();
					
					productline.put("value",null);
					productdataarray.add(productline);
				}
			}
			
			productlineandvendor.put("seriesname",product);
			productlineandvendor.put("color",color[i]);
			productlineandvendor.put("data",productdataarray);
			productdataarrayandvendorarray.add(productlineandvendor);
			i++;
			
		}
		
		mainobject.put("data",productdataarrayandvendorarray);
		
		
		return mainobject;
		 
		 
		 }
	
	*/
	
	
	
	
	
	
	@RequestMapping(value = "/volumeByQTY", method = RequestMethod.GET)	
	public @ResponseBody JSONObject monthleYtToProdut(HttpServletResponse res,HttpServletRequest request )  
	 {	
	
		
		try
		{
	 
		String color[]={"#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222","#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222"};
		
		JSONObject mainobject = new JSONObject();
		
		List<String> vendornames=migratesevice.getVendorNamesYtdSales2();
		
		JSONArray vendorarray= new JSONArray();
		for(String vendors:vendornames)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("label", vendors);
			vendorarray.add(vendorobject);
			
		}
		JSONObject vendorcatgories = new JSONObject();
		vendorcatgories.put("category", vendorarray);
		JSONArray vendorvatageoreyarray= new JSONArray();
		vendorvatageoreyarray.add(vendorcatgories);
		mainobject.put("vendors",vendorvatageoreyarray);
		
		
		
		List<String> productnames=migratesevice.getproductlines();
		JSONArray productdataarrayandvendorarray= new JSONArray();
		int i=0;
		
		for(String product:productnames)
		{
			JSONObject productlineandvendor = new JSONObject();
			JSONArray productdataarray= new JSONArray();
			List<Object[]> vendorsbookingwithproduct= migratesevice.vendorsbookingwithproducwithYtdSales2(product);
			HashMap vendorsinqty=new HashMap();
			
			for(Object[] vendor:vendorsbookingwithproduct)
				
			{
				
				vendorsinqty.put((String)vendor[0],(Double)vendor[1]);
				
			}
			
			Set vendorss=vendorsinqty.keySet();
			
			for(String vendors:vendornames)
			{
				
				if(vendorss.contains(vendors))
				{
				JSONObject productline = new JSONObject();
				
				Double d=migratesevice.getinqty(product,vendors);
				
				productline.put("value",(Double)vendorsinqty.get(vendors));
				productdataarray.add(productline);
				
				}
				
				else
				{
					JSONObject productline = new JSONObject();
					
					productline.put("value",null);
					productdataarray.add(productline);
				}
			}
			
			productlineandvendor.put("seriesname",product);
			productlineandvendor.put("color",color[i]);
			productlineandvendor.put("data",productdataarray);
			productdataarrayandvendorarray.add(productlineandvendor);
			i++;
			
		}
		
		mainobject.put("data",productdataarrayandvendorarray);
		
		
		return mainobject;
		
		}catch(Exception e)
		{
			logger.info("=====================CONTROLLER METHOD=monthleYtToProdut=================="+e.getCause());
			return null;
		}
		 
		 
		 }
	
	
	
	
	/*
	
	@RequestMapping(value = "/otdBusinessVolume", method = RequestMethod.GET)	
	public @ResponseBody JSONObject otdBusinessVolume(HttpServletResponse res,HttpServletRequest request )  
	 {	
	 Getting distinct vendor names from  YTDSales 
		String color[]={"#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222","#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222"};
		
		JSONObject mainobject = new JSONObject();
		
		List<String> vendornames=migratesevice.getVendorNames();
		
		JSONArray vendorarray= new JSONArray();
		for(String vendors:vendornames)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("label", vendors);
			vendorarray.add(vendorobject);
			
		}
		JSONObject vendorcatgories = new JSONObject();
		vendorcatgories.put("category", vendorarray);
		JSONArray vendorvatageoreyarray= new JSONArray();
		vendorvatageoreyarray.add(vendorcatgories);
		mainobject.put("vendors",vendorvatageoreyarray);
		
		
		
		List<String> productnames=migratesevice.getproductlines();
		JSONArray productdataarrayandvendorarray= new JSONArray();
		int i=0;
		
		for(String product:productnames)
		{
			JSONObject productlineandvendor = new JSONObject();
			JSONArray productdataarray= new JSONArray();
			List<Object[]> vendorsboookingonusd=migratesevice.getsunofvendorsusd(product);
			HashMap vendorsusd=new HashMap();
			for(Object[] vud:vendorsboookingonusd)
			{
				vendorsusd.put((String)vud[0],(Double)vud[1]);
				
			}
			Set vendorsusdset=vendorsusd.keySet();
			
			for(String vendors:vendornames)
			{
				
				if(vendorsusdset.contains(vendors))
				{
				JSONObject productline = new JSONObject();
				Double d=migratesevice.getamountusd(product,vendors);
				double roundOff = Math.round(d * 100.0) / 100.0;
				 productline.put("value",(Double)vendorsusd.get(vendors));
				productdataarray.add(productline);
				
				}
				
				else
				{
					JSONObject productline = new JSONObject();
					productline.put("value",null);
					productdataarray.add(productline);
					
				}
			}
			
			productlineandvendor.put("seriesname",product);
			productlineandvendor.put("color",color[i]);
			productlineandvendor.put("data",productdataarray);
			productdataarrayandvendorarray.add(productlineandvendor);
			i++;
			
		}
		
		mainobject.put("data",productdataarrayandvendorarray);
		
		
		return mainobject;
		 
		 
		 }
	*/

	
	@RequestMapping(value = "/otdBusinessVolume", method = RequestMethod.GET)	
	public @ResponseBody JSONObject otdBusinessVolume(HttpServletResponse res,HttpServletRequest request )  
	 {	
		try{
	 
		String color[]={"#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222","#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222"};
		
		JSONObject mainobject = new JSONObject();
		
		List<String> vendornames=migratesevice.getVendorNamesYtdSales2();
		
		JSONArray vendorarray= new JSONArray();
		for(String vendors:vendornames)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("label", vendors);
			vendorarray.add(vendorobject);
			
		}
		JSONObject vendorcatgories = new JSONObject();
		vendorcatgories.put("category", vendorarray);
		JSONArray vendorvatageoreyarray= new JSONArray();
		vendorvatageoreyarray.add(vendorcatgories);
		mainobject.put("vendors",vendorvatageoreyarray);
		
		
		
		List<String> productnames=migratesevice.getproductlines();
		JSONArray productdataarrayandvendorarray= new JSONArray();
		int i=0;
		
		for(String product:productnames)
		{
			JSONObject productlineandvendor = new JSONObject();
			JSONArray productdataarray= new JSONArray();
			List<Object[]> vendorsboookingonusd=migratesevice.getsunofvendorsusdFromYTDSales2(product);
			HashMap vendorsusd=new HashMap();
			for(Object[] vud:vendorsboookingonusd)
			{
				vendorsusd.put((String)vud[0],(Double)vud[1]);
				
			}
			Set vendorsusdset=vendorsusd.keySet();
			
			for(String vendors:vendornames)
			{
				
				if(vendorsusdset.contains(vendors))
				{
				JSONObject productline = new JSONObject();
				Double d=migratesevice.getamountusd(product,vendors);
				double roundOff = Math.round(d * 100.0) / 100.0;
				 productline.put("value",(Double)vendorsusd.get(vendors));
				productdataarray.add(productline);
				
				}
				
				else
				{
					JSONObject productline = new JSONObject();
					productline.put("value",null);
					productdataarray.add(productline);
					
				}
			}
			
			productlineandvendor.put("seriesname",product);
			productlineandvendor.put("color",color[i]);
			productlineandvendor.put("data",productdataarray);
			productdataarrayandvendorarray.add(productlineandvendor);
			i++;
			
		}
		
		mainobject.put("data",productdataarrayandvendorarray);
		
		
		return mainobject;
		}catch(Exception e)
		{
			logger.info("===========CONTROLLER METHOD=otdBusinessVolume============="+e.getCause());
			return null;
		}
		 
		 
		 }

	
	
	
	
	
	
	@RequestMapping(value = "/averageLeadProductType", method = RequestMethod.GET)
	public  @ResponseBody JSONObject  averageLeadProductType() 
	{
		JSONObject keyforyears = new JSONObject();
		//JSONArray jsonArray = new JSONArray();			
		//JSONObject otdCount = null;
		//HashMap<Integer,String> labelMap=new HashMap<Integer,String>();
		
		try
		{			

			 
			 List list=migratesevice.getAverageLeadProdType();
			 
/*			 for(int i=0;i<hashMap.size();i++)
			 {
				 otdCount = new JSONObject();

				 otdCount.put("label",labelMap.get(i));	
				 otdCount.put("value", hashMap.get(i));
				 
				 jsonArray.add(otdCount);
			 }*/
				 
			
			keyforyears.put("data",list);	
			
			return keyforyears;	 	
		}
		catch(Exception e){
			logger.info("==============CONTROLLER METHOD=averageLeadProductType===="+e.getMessage());
			return null;
		}
	}
	
	
	
	/*@RequestMapping(value = "/productType", method = RequestMethod.GET)	
	public @ResponseBody JSONObject productType(HttpServletResponse res,HttpServletRequest request )  
	 {	
		 Getting distinct vendor names from  YTDSales 
		String color[]={"#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222","#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222"};
		
		JSONObject mainobject = new JSONObject();
		JSONArray productdataarray= new JSONArray();
		
		List<Object[]> productvalues=migratesevice.getProductTypeValues();
		int i=1;
		for(Object[] values:productvalues)
		{
			 
			String productline=(String)values[0];
			String submodel=(String)values[1];
			Double amount=(Double)values[2];
			double roundOff = Math.round(amount * 100.0) / 100.0;
			
			JSONObject subobject = new JSONObject();
			subobject.put("label",productline+"/"+submodel);
			subobject.put("value",roundOff);
			
			Random rand = new Random();
			int r = rand.nextInt(1000);
			int g = rand.nextInt(1000);
			int b = rand.nextInt(1000);
			String rdb=r+""+g+""+b;
			int number=Integer.parseInt(rdb);
			String hex = Integer.toHexString(number);
			 
			
			subobject.put("color","#"+hex);
			
			productdataarray.add(subobject);
			
			 i++;
		}
		
		 		
		mainobject.put("data",productdataarray);
		return mainobject;
		 
		 
		 }
*/
	
	
	
	
	@RequestMapping(value = "/productType", method = RequestMethod.GET)	
	public @ResponseBody JSONObject productType(HttpServletResponse res,HttpServletRequest request )  
	 {	
		try
		{
		 
		String color[]={"#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222","#00FFFF","	#7FFFD4","#8A2BE2","#A52A2A","#D2691E","#DC143C","#00FFFF","#008B8B","#B8860B","#006400","#B22222"};
		
		JSONObject mainobject = new JSONObject();
		JSONArray productdataarray= new JSONArray();
		
		List<Object[]> productvalues=migratesevice.getProductTypeValuesYtdSales3();
	
		for(Object[] values:productvalues)
		{
			 
			String productline=(String)values[0];
			String submodel=(String)values[1];
			Double amount=(Double)values[2];
			double roundOff = Math.round(amount * 100.0) / 100.0;
			
			JSONObject subobject = new JSONObject();
			subobject.put("label",productline+"/"+submodel);
			subobject.put("value",roundOff);
			
			Random rand = new Random();
			int r = rand.nextInt(1000);
			int g = rand.nextInt(1000);
			int b = rand.nextInt(1000);
			String rdb=r+""+g+""+b;
			int number=Integer.parseInt(rdb);
			String hex = Integer.toHexString(number);
			 
			
			subobject.put("color","#"+hex);
			
			productdataarray.add(subobject);
			
			 
		}
		
		
		
		
		List<Object[]> productvalues1=migratesevice.getProductTypeValuesYtdSales4();
		
		for(Object[] values:productvalues1)
		{
			 
			String productline=(String)values[0];
			String submodel=(String)values[1];
			Double amount=(Double)values[2];
			double roundOff = Math.round(amount * 100.0) / 100.0;
			
			JSONObject subobject = new JSONObject();
			subobject.put("label",productline+"/"+submodel);
			subobject.put("value",roundOff);
			
			Random rand = new Random();
			int r = rand.nextInt(1000);
			int g = rand.nextInt(1000);
			int b = rand.nextInt(1000);
			String rdb=r+""+g+""+b;
			int number=Integer.parseInt(rdb);
			String hex = Integer.toHexString(number);
			 
			
			subobject.put("color","#"+hex);
			
			productdataarray.add(subobject);
			
			 
		}
		
		
		 		
		mainobject.put("data",productdataarray);
		return mainobject;
		}catch(Exception e)
		{
			logger.info("============CONTROLLER METHOD=productType============"+e.getCause());;
			return null;
		}
		 
		 
		 }

	
	
	
	@RequestMapping(value = "/profitProducts", method = RequestMethod.GET)
	public  @ResponseBody JSONObject  profitProducts() 
	{
		try
		{
			JSONObject keyforyears = new JSONObject();
			 
			List<Object[]> coutries=migratesevice.getCountries();
			
			JSONArray vendorarray= new JSONArray();
			
			for(Object[] country:coutries)
			{
				JSONObject vendorobject = new JSONObject();
				vendorobject.put("label",(String)country[0]);
				vendorarray.add(vendorobject);
				
			}
			JSONObject vendorcatgories = new JSONObject();
			vendorcatgories.put("category", vendorarray);
			JSONArray vendorvatageoreyarray= new JSONArray();
			vendorvatageoreyarray.add(vendorcatgories);
			keyforyears.put("countries",vendorvatageoreyarray);
			
			JSONArray countryjsonArraywithcatageroey = new JSONArray();
			List<Object[]> aboutcatgories=migratesevice.getAllDetailsAboutCatageory();
			for(Object[] catageoreydetails:aboutcatgories)
			{
				JSONObject aboutcountry=new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
				
				String catageoryname=(String)catageoreydetails[0];
				String color=(String)catageoreydetails[1];
				
				List<Object[]> countriesbookingsoncatagorey=migratesevice.getcountriesbookingsoncatagorey(catageoryname);
				HashMap<String,Double> a=null;
			 
				
			 
					
				 a=new HashMap<String,Double>();
					
				for(Object[] countriesbookings:countriesbookingsoncatagorey)
				{
				 
				a.put((String)countriesbookings[0],(Double)countriesbookings[1]);
				}
				
				if(countriesbookingsoncatagorey.size()!=0)
				{
					Set sbookingcountries=a.keySet();
					
					logger.info("the values in the key set are"+sbookingcountries);
					
					for(Object[] country:coutries)
					{
						
						logger.info("the country is follwing ------------------->"+(String)country[0]);
						
						if(sbookingcountries.contains(((String)country[0])))
						{
							
							 
							JSONObject ebc=new JSONObject();
							Double d=(Double)a.get(((String)country[0]));
							logger.info(d+"-------------------------------------------->"+((String)country[0]).toUpperCase());
							ebc.put("value",d);
							ebc.put("link","#/app/SCA_profitProducts");
							countryjsonArray.add(ebc);
						}
						
						
						
						
						else
						{
							
							 
							JSONObject ebc=new JSONObject();
							Double d=(Double)a.get(((String)country[0]).toUpperCase());
							logger.info(d+"-------------------------------------------->"+((String)country[0]).toUpperCase());
							ebc.put("value",null);
							ebc.put("link","#/app/SCA_profitProducts");
							countryjsonArray.add(ebc);
						}
		            }
					
				}
				 
				
				 
				
			
				
			 
				
				aboutcountry.put("seriesname",catageoryname );
				aboutcountry.put("color",color);
				aboutcountry.put("data",countryjsonArray);
				countryjsonArraywithcatageroey.add(aboutcountry);
			}
			
			
			keyforyears.put("data",countryjsonArraywithcatageroey);
		
			return keyforyears;	 
			
		}catch(Exception e)
		{
			logger.info("=====================CONTROLLER METHOD==profitProducts======="+e.getCause());
			return null;
		}
	}
	
	
	
	/*@RequestMapping(value = "/profitProductsDrilldown", method = RequestMethod.GET)
	public  @ResponseBody JSONObject  profitProductsDrilldown(HttpServletRequest request,HttpServletResponse res) 
	{
		 
			String country=request.getParameter("country");
            String country1=country.trim();
			
			String catageorey=request.getParameter("catageorey");
			String catageorey1=catageorey.trim();
			
			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			
			List<Object[]> products=migratesevice.getAllProductsBasedOnCatageoreyNameforcastdrilldown(catageorey1,country1);
			
			for(Object[] p:products)
			{
				JSONObject subobject = new JSONObject();
				String productnmae=(String)p[0];
				Double productvalue=(Double)p[1];
				 
				subobject.put("value",productvalue);
				subobject.put("label",productnmae);
				jsonArray.add(subobject);
				
			}
			mainobject.put("data",jsonArray);
			return mainobject;
		
		}
*/
	

	
	@RequestMapping(value = "/profitProductsDrilldown", method = RequestMethod.GET)
	public  @ResponseBody JSONObject  forcastproductdrilldown(HttpServletRequest request,HttpServletResponse res) 
	{
		try
		{
		
		    String id=request.getParameter("id");
			String country=request.getParameter("categoryLabel");
            String country1=country.trim();
		    String catageorey=request.getParameter("catageory");
			String catageorey1=catageorey.trim();
		
			if(id.equalsIgnoreCase("local"))
			{
			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray jsonArray1 = new JSONArray();
			 
			
			List<Object[]> products=migratesevice.getAllProductsBasedOnCatageoreyNameforcastdrilldown(catageorey1,country1);
			
			for(Object[] p:products)
			{
				JSONObject subobject = new JSONObject();
				JSONObject subobject1 = new JSONObject();
				String productnmae=(String)p[0];
				Double productvalue=(Double)p[1];
				 
				subobject.put("value",productvalue);
				subobject1.put("label",productnmae);
				jsonArray.add(subobject);
				jsonArray1.add(subobject1);
				
				
			}
			mainobject.put("xaxis",jsonArray1);
			mainobject.put("data",jsonArray);
			 
			return mainobject;
			
			}
			else
				{
				
				JSONObject mainobject = new JSONObject();
				JSONArray jsonArray = new JSONArray();
				JSONArray jsonArray1 = new JSONArray();
				 
				
				List<Object[]> products=migratesevice.getAllProductsBasedOnCatageoreyNameforcastdrilldowndollar(catageorey1,country1);
				
				for(Object[] p:products)
				{
					JSONObject subobject = new JSONObject();
					JSONObject subobject1 = new JSONObject();
					String productnmae=(String)p[0];
					Double productvalue=(Double)p[1];
					 
					subobject.put("value",productvalue);
					subobject1.put("label",productnmae);
					jsonArray.add(subobject);
					jsonArray1.add(subobject1);
					
					
				}
				mainobject.put("xaxis",jsonArray1);
				mainobject.put("data",jsonArray);
				 
				return mainobject;
				
				
				
				
			}
			
		}catch(Exception e)
		{
			
			logger.info("================CONTROLLER METHOD=forcastproductdrilldown================="+e.getCause());
			return null;
		}
		
		 	}
	
	




	
	@RequestMapping(value = "/forecastbacklog", method = RequestMethod.GET)
	public  @ResponseBody JSONObject  forcastbacklog(HttpServletRequest request,HttpServletResponse res) 
	{
		try
		{
		 Calendar now = Calendar.getInstance();
		 int year=now.get(Calendar.YEAR);
		 int month=now.get(Calendar.MONTH)+1;
		 int date=now.get(Calendar.DATE);
		 
		 
			 
			 
			
		 HashMap monthname=new HashMap();
			monthname.put(1,"Jan");
			monthname.put(2,"Feb");
			monthname.put(3,"Mar");
			monthname.put(4,"Apr");
			monthname.put(5,"May");
			monthname.put(6,"Jun");
			monthname.put(7,"Jul");
			monthname.put(8,"Aug");
			monthname.put(9,"Sep");
			monthname.put(10,"Oct");
			monthname.put(11,"Nov");
			monthname.put(12,"Dec");
			
		 
			
			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			JSONObject months=null;
			
			 
			
			for(int i=1;i<=12;i++)
			{
				months=new JSONObject();
				 months.put("label",(String)monthname.get(i));
				 jsonArray.add(months);
				
			}
			
			JSONObject subobject=new JSONObject();
			subobject.put("category",jsonArray);
			mainjsonArray.add(subobject);
			
			 
			mainobject.put("xaxis",mainjsonArray);
			
			
			 
			
			JSONArray chartdata=new JSONArray();
			
			
			/*kepping backlog as of last year data in the month of jan*/
			
			
			JSONObject backlog=new JSONObject();
			JSONArray backlogarray = new JSONArray();
			
			
			for(int i=1;i<=12;i++)
			{
				JSONObject backlogdata=new JSONObject();
				
				if(i==1)
				{
				Double d=migratesevice.getbacklogofpreviousyear(year-1);
					backlogdata.put("value",d);
					backlogdata.put("link","#/app/forecastBacklog");
					backlogarray.add(backlogdata);
					
				}
				
				else
				{
					backlogdata.put("value",null);
					backlogarray.add(backlogdata);
					
					
				}
				
			 
				
				
			}
			
			backlog.put("seriesname","Total Accumulate Backlog");
			 
			backlog.put("canvasPadding",0);
			backlog.put("data",backlogarray);
		
			backlog.put("color","#DC143C");
			chartdata.add(backlog);
			 
			
			
			/***********************************************************************/
			
			
			
			
			
			
			
		JSONObject backlog6 = new JSONObject();
		JSONArray backlogarray6 = new JSONArray();
		for (int i = 1; i <= 12; i++) {
			JSONObject backlogdata6 = new JSONObject();
			backlogdata6.put("value", null);
			backlogarray6.add(backlogdata6);

		}
		backlog6.put("data", backlogarray6);
		chartdata.add(backlog6);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		
	/*code for Backlog of the  current year months up to lesstan the current month on wich your staying****************/
			
			
			
			JSONObject backlog1=new JSONObject();
			JSONArray backlogarray1 = new JSONArray();
			
			
			for(int i=1;i<=12;i++)
			{
				JSONObject backlogdata1=new JSONObject();
				if(i<month)
				{
				Double d=migratesevice.getamountfromcurrentyear(i,year);
					
					backlogdata1.put("value",d);
					backlogarray1.add(backlogdata1);
				}
				
				else
				{
					backlogdata1.put("value",null);
					backlogarray1.add(backlogdata1);
					
			     }
				
				
				
			}
			
			backlog1.put("seriesname","Back log of the month");
			 
			backlog1.put("color","008ee4");
			backlog1.put("canvasPadding",0);
			backlog1.put("data",backlogarray1);
		
			
			chartdata.add(backlog1);
			
			
		/******************************************************************************/	
			
			
		/*code for monthly invoiced***********/
			
			
			
			JSONObject backlog2=new JSONObject();
			JSONArray backlogarray2 = new JSONArray();
			
			
			for(int i=1;i<=12;i++)
			{
				JSONObject backlogdata2=new JSONObject();
				if(i<month)
				{
					Double d=migratesevice.getamountfromcurrentyearinytdsales(i,year);
					backlogdata2.put("value",d);
					backlogarray2.add(backlogdata2);
					
				}
				
				else
				{
					backlogdata2.put("value",null);
					backlogarray2.add(backlogdata2);
					
			     }
				
				
				
			}
			
			backlog2.put("seriesname","Monthly invoiced");
			 
			backlog2.put("color","#55FF33");
			backlog2.put("canvasPadding",0);
			backlog2.put("data",backlogarray2);
		
			
			chartdata.add(backlog2);
			
		
	/*************************************************************/		
			
			
			
			
			JSONObject backlog3=new JSONObject();
			JSONArray backlogarray3 = new JSONArray();
			
			
			for(int i=1;i<=12;i++)
			{
				JSONObject backlogdata3=new JSONObject();
				if(i==month)
				{
					Double d=migratesevice.getamountfromcurrentyearinytdsales(i,year);
					backlogdata3.put("value",d);
					backlogarray3.add(backlogdata3);
					
				}
				
				else
				{
					backlogdata3.put("value",null);
					backlogarray3.add(backlogdata3);
					
			     }
				
				
				
			}
			
			backlog3.put("seriesname","Sales invoiced on current month");
			 
			backlog3.put("color","#060606");
			backlog3.put("canvasPadding",0);
			backlog3.put("data",backlogarray3);
		
			
			chartdata.add(backlog3);
			
		/***************************************************************/

			
			JSONObject backlog4=new JSONObject();
			JSONArray backlogarray4 = new JSONArray();
			
			
			for(int i=1;i<=12;i++)
			{
				JSONObject backlogdata4=new JSONObject();
				if(i>=month)
				{
					Double d=migratesevice.getamountfromcurrentyearinytdsalesforrequired(i,year);
					
					backlogdata4.put("value",d);
					backlogarray4.add(backlogdata4);
					
				}
				
				else
				{
					backlogdata4.put("value",null);
					backlogarray4.add(backlogdata4);
					
			     }
				
				
				
			}
			
			backlog4.put("seriesname","Sales invoiced on current month");
			 
			backlog4.put("color","#CBC0C0");
			backlog4.put("canvasPadding",0);
			backlog4.put("data",backlogarray4);
		
			
			chartdata.add(backlog4);
			
		mainobject.put("data",chartdata);
			 
			 		return mainobject;
		}catch(Exception e)
		{
			logger.info("================================CONTROLER METHOD=forcastbacklog============="+e.getCause());
			return null;
		}
		 	}
	
	
	
	
	
	
	@RequestMapping(value = "/forecastbacklogdrilldown", method = RequestMethod.GET)
	public  @ResponseBody JSONObject  forcastbacklogdrilldown() 
	{
 try
 {
		 
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1;
		int date = now.get(Calendar.DATE);
		HashMap monthname = new HashMap();
		monthname.put(1, "Jan");
		monthname.put(2, "Feb");
		monthname.put(3, "Mar");
		monthname.put(4, "Apr");
		monthname.put(5, "May");
		monthname.put(6, "Jun");
		monthname.put(7, "Jul");
		monthname.put(8, "Aug");
		monthname.put(9, "Sep");
		monthname.put(10, "Oct");
		monthname.put(11, "Nov");
		monthname.put(12, "Dec");

		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		JSONArray mainjsonArray = new JSONArray();
		JSONObject months = null;

		ArrayList<Integer> monthnumberinxaxisorder = new ArrayList<Integer>();
		for (int k = month + 1; k <= 12; k++) {
			monthnumberinxaxisorder.add(k);
			months = new JSONObject();
			months.put("label", (String) monthname.get(k));
			jsonArray.add(months);
		}

		for (int i = 1; i <= month; i++) {
			monthnumberinxaxisorder.add(i);
			months = new JSONObject();
			months.put("label", (String) monthname.get(i));
			jsonArray.add(months);
		}

		JSONObject subobject = new JSONObject();
		subobject.put("category", jsonArray);
		mainjsonArray.add(subobject);

		mainobject.put("xaxis", mainjsonArray);

		/**************************************** getting  baclogs from previous years*************************************/
		JSONArray chartdata = new JSONArray();
		JSONObject backlog1 = new JSONObject();
		JSONArray backlogarray1 = new JSONArray();

		for (int i = 1; i <= 12; i++) {
			JSONObject backlogdata1 = new JSONObject();

			if (i == 1) {
				Double d = migratesevice.getamountfromcurrentyearinytdsalesforrequired(year);
				backlogdata1.put("value", d);
				backlogarray1.add(backlogdata1);

			}

			else {
				backlogdata1.put("value", null);
				backlogarray1.add(backlogdata1);

			}

		}

		backlog1.put("seriesname", "Total Acculmate backlog of previous years");
		backlog1.put("color", "#DC143C");
		backlog1.put("canvasPadding", 0);
		backlog1.put("data", backlogarray1);
		chartdata.add(backlog1);
			
		/***************************Sales Invoiced in current month ***********************************************/	
		int s = monthnumberinxaxisorder.size();
		int lastvalue = monthnumberinxaxisorder.get(s - 1);
		JSONObject backlog2 = new JSONObject();
		JSONArray backlogarray2 = new JSONArray();
		for (Integer mnumber : monthnumberinxaxisorder)

		{

			JSONObject backlogdata2 = new JSONObject();

			if (mnumber == lastvalue) {
				Double d = migratesevice.getamountfromcurrentyearinytdsales(lastvalue, year);
				backlogdata2.put("value", d);
				backlogarray2.add(backlogdata2);
			} else {
				backlogdata2.put("value", null);
				backlogarray2.add(backlogdata2);
			}
		}

		backlog2.put("seriesname", "Sales Invoiced in current month");
		backlog2.put("color", "#060606");
		backlog2.put("canvasPadding", 0);
		backlog2.put("data", backlogarray2);
		chartdata.add(backlog2);
		/******************************** Sales Invoiced in current month**********************************/
		JSONObject backlog3 = new JSONObject();
		JSONArray backlogarray3 = new JSONArray();
		for (Integer mnumber : monthnumberinxaxisorder)

		{
			JSONObject backlogdata3 = new JSONObject();
			if (mnumber == lastvalue) {
				Double d = migratesevice.getamountfromcurrentyearinytdsalesforrequired(lastvalue, year);
				backlogdata3.put("value", d);
				backlogarray3.add(backlogdata3);
			} else {
				backlogdata3.put("value", null);
				backlogarray3.add(backlogdata3);

			}
		}
		backlog3.put("seriesname", "Sales Invoiced in current month");
		backlog3.put("color", "#CBC0C0");
		backlog3.put("canvasPadding", 0);
		backlog3.put("data", backlogarray3);
		chartdata.add(backlog3);

			
			/********************************************  Back log of month  ************************************************/
			
			
			
		JSONObject backlog4 = new JSONObject();
		JSONArray backlogarray4 = new JSONArray();
		for (int k = month + 1; k <= 12; k++) {
			JSONObject backlogdata4 = new JSONObject();
			Double d = migratesevice.getamountfromcurrentyear(k, year - 1);
			backlogdata4.put("value", d);
			backlogarray4.add(backlogdata4);
		}
		for (int i = 1; i <= month; i++) {
			JSONObject backlogdata4 = new JSONObject();

			if (i == month) {
				backlogdata4.put("value", null);
				backlogarray4.add(backlogdata4);
			}

			else {
				Double d = migratesevice.getamountfromcurrentyear(i, year);
				backlogdata4.put("value", d);
				backlogarray4.add(backlogdata4);
			}
		}
		backlog4.put("seriesname", "Back log of month");
		backlog4.put("color", "008ee4");
		backlog4.put("canvasPadding", 0);
		backlog4.put("data", backlogarray4);
		chartdata.add(backlog4);
		mainobject.put("data", chartdata);

		return mainobject;
		
 }catch(Exception e){
	 logger.info("=======================CONTROLLER METHOD=forcastbacklogdrilldown=========="+e.getCause());
	 return null;
	 
 }
		
		 	}
	




}