package com.finsol.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.finsol.bean.QuotationFormBean;
import com.finsol.bean.QuotationformGridBean;
import com.finsol.bean.TermsandConditionsGridBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.ApplicationIndustryCode;
import com.finsol.model.CaseSummary;
import com.finsol.model.CompititorGrid;
import com.finsol.model.CustmerContactDetails;
import com.finsol.model.CustmerDetails;
import com.finsol.model.NewSelectionGrid;
import com.finsol.model.Quotation;
import com.finsol.model.QuotationGrid;
import com.finsol.model.SequenceNumber;
import com.finsol.model.SparePartGrid;
import com.finsol.model.SumitomoReplaceGrid;
import com.finsol.model.TechGrid;
import com.finsol.model.TermsAndConditionGrid;
import com.finsol.model.TermsandConditions;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.BSS_SalesManagementService;
import com.finsol.service.Bss_Migrate_ServiceImpl;
import com.finsol.service.MasterScreen_serviceImpl;
import com.finsol.service.ProductSelectionService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

@Controller
public class QuatationManagmentController {

	private static final Logger logger = Logger.getLogger(QuatationManagmentController.class);

	@Autowired
	private BSS_CommonService bsscommonService;

	@Autowired
	private MasterScreen_serviceImpl masterservice;
	
	 
	@Autowired
	private Bss_Migrate_ServiceImpl migratesevice;
	

	@Autowired
	private BSS_SalesManagementService salesManagementService;

	@Autowired
	private HibernateDao hibernateDao;
	
	@Autowired
	private ProductSelectionService productSelectionService;
	
	

	/*@Autowired
	private ServletContext servletContext;*/

	/*-------------------------------------- Common Methods ----------------------------------------------*/

	@RequestMapping(value = "/quatationformsubmitforFullunit", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody boolean quatationformsubmitforfullunit(MultipartHttpServletRequest req)
			throws Exception {
		
		int status=0;
		
		String ss=req.getParameter("formData");
		
		String ss1=req.getParameter("gridData");
		
		String ss2=req.getParameter("termConData");
		
		
		org.json.JSONArray gData=new org.json.JSONArray(ss1);
		org.json.JSONArray tgData=new org.json.JSONArray(ss2);
		
		QuotationFormBean maincase = new ObjectMapper().readValue(ss,QuotationFormBean.class);
	 
		if(maincase.getModifiedstatus()==1)
		{
			String qtno=maincase.getQtnNo();
			String qtno1[]=qtno.split("-");
			String qtnotocompare=null;
			if(qtno1.length==4)
			{
				qtnotocompare=qtno1[0]+"-"+qtno1[1]+"-"+qtno1[2]+"-";
			}
			
			if(qtno1.length==5)
			{
				qtnotocompare=qtno1[0]+"-"+qtno1[1]+"-"+qtno1[2]+"-"+qtno1[3]+"-";
				
			}
			String custmername=maincase.getCustName();
			String custmercode=maincase.getCustCode();
			
			Session session=hibernateDao.getSessionFactory().openSession();
			String query1="update Quotation set flag=0 where custCode='"+custmercode+"' and custName = '"+custmername+"' and qtnNo like '"+qtnotocompare+"%'";
			SQLQuery query=session.createSQLQuery(query1 );
			query.executeUpdate();
			//List<String> result=query.list();
			
		}
				
		
		
		ArrayList<QuotationformGridBean> newselctiongrid = new ArrayList<QuotationformGridBean>();
		for (int i = 0; i < gData.length(); i++) 
		{
QuotationformGridBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
					QuotationformGridBean.class);
newselctiongrid.add(newcasegrid);
		}
		
		
		
		
		ArrayList<TermsandConditionsGridBean> termsandcondistionsgrid = new ArrayList<TermsandConditionsGridBean>();
		for (int i = 0; i < tgData.length(); i++) 
		{
			TermsandConditionsGridBean termsgrid = new ObjectMapper().readValue(tgData.get(i).toString(),
		TermsandConditionsGridBean.class);
			termsandcondistionsgrid.add(termsgrid);
		}
		
		
		
	HttpSession hs=	req.getSession();
	String user=(String)hs.getAttribute("user");

		
		
	Quotation quatation=this.getQuotationData(maincase,user);
	
		
	masterservice.saveQuotation(quatation);
	int quotationid=masterservice.getMaxQuationid();
	
	
	List<QuotationGrid> qgrid=this.getquotationtiongrid(quotationid,newselctiongrid);
	masterservice.savequotationGrid(qgrid);
	
	List<TermsAndConditionGrid> qtgrid=this.gettermanadconditionsofquationgrid(quotationid,termsandcondistionsgrid);	
	masterservice.savequationtermsandconditiongrid(qtgrid);	
	
	
	String version = quatation.getQtnNo().substring(quatation.getQtnNo().lastIndexOf('-') + 1);
	String[] spl=quatation.getQtnNo().split("-");	
	String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");
	
	
	
	String countrycode=(String) st[0].subSequence(0,1);
String intial=	(String)st[0].substring(st[0].length() - 2);
	
	ArrayList<SequenceNumber> sequencenumberlist=new ArrayList<SequenceNumber>();


	SequenceNumber sequenceNumber=new SequenceNumber();

	sequenceNumber.setProcessname(spl[0]);
	sequenceNumber.setCountrycode(countrycode);
	sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
	sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
	sequenceNumber.setTotalsequenceno(quatation.getQtnNo());
	sequenceNumber.setVersion(version);
	sequenceNumber.setStaffinitial(intial);
	

	
	sequencenumberlist.add(sequenceNumber);
	
	productSelectionService.saveEntities(sequencenumberlist);
		return true;

	}

	 






private List<TermsAndConditionGrid> gettermanadconditionsofquationgrid(int quotationid,
			ArrayList<TermsandConditionsGridBean> termsandcondistionsgrid) {
	ArrayList<TermsAndConditionGrid> al=new ArrayList<TermsAndConditionGrid>();
		
	for(TermsandConditionsGridBean t:termsandcondistionsgrid)
	{
		TermsAndConditionGrid tacg=new TermsAndConditionGrid();
		tacg.setModelCon(t.getModelCon());
		tacg.setModelTerm(t.getModelTerm());
		tacg.setQid(quotationid);
		al.add(tacg);
	}
		return al;
	}








private List<QuotationGrid> getquotationtiongrid(int quotationid,
			ArrayList<QuotationformGridBean> newselctiongrid) 
{
	ArrayList<QuotationGrid> qgl=new ArrayList<QuotationGrid>();
	for(QuotationformGridBean k:newselctiongrid)
	{
		QuotationGrid qg=new QuotationGrid();
	
	

	
		
	
			qg.setModel(k.getModel());
			
		
		qg.setProductDesp(k.getProductDesp());
        qg.setMotorPower(k.getMotorPower());
		qg.setInputRPM(k.getInputRPM());
		qg.setRatio(k.getRatio());
		qg.setOutPutRPM(k.getOutPutRPM());
		qg.setAccessories(k.getAccessories());
		qg.setQnty(k.getQnty());
		qg.setDeliveryTime(k.getDeliveryTime());
	    qg.setUnitPrice(k.getUnitPrice());
	    qg.setPartNumber(k.getPartNumber());
	    qg.setSubTotal(k.getSubTotal());
		qg.setPartDesp(k.getPartDesp());
		
		
			
			
		
		
		
			qg.setSerialNumber(k.getSerialNumber());
			
		
		 
	    qg.setQid(quotationid);
	    qg.setSfactor(k.getSfactor());
	    qg.setCostPrice(k.getCostPrice());
		qg.setMotorDetails(k.getMotorDetails());
		qg.setSpecialFeaturs(k.getSpecialFeaturs());
		qg.setPerSN(k.getPerSN());
		qg.setQuantity(k.getQuantity());
		qg.setTotalPrice(k.getTotalPrice());
		qg.setRegion(k.getRegion());
		qg.setPole(k.getPole());
		qg.setPhase(k.getPhase());
		qg.setVoltage(k.getVoltage());
		qg.setFreq(k.getFreq());
		qg.setAppcode(k.getAppcode());
		qg.setEquipNo(k.getEquipNo());
		
		
		

		 
		 
		 
		qgl.add(qg);
	}
		// TODO Auto-generated method stub
		return qgl;
	}








private Quotation getQuotationData(QuotationFormBean maincase,String user) 
 {
		Quotation q = new Quotation();
		q.setCustCode(maincase.getCustCode());
		q.setQtnType(maincase.getQtnType());
		q.setQtnNo(maincase.getQtnNo());
		q.setCustName(maincase.getCustName());
		q.setCurrency(maincase.getCurrency());
		q.setAttention(maincase.getAttention());
		q.setCustCode(maincase.getCustCode());
		q.setTax(maincase.getTax());
		q.setCopy(maincase.getCopy());
		q.setCountry(maincase.getCountry());
		q.setSalesMan(maincase.getSalesMan());
		q.setTaxPercent(maincase.getTaxPercent());
		q.setTelephone(maincase.getTelephone());
		q.setCustRef(maincase.getCustRef());
		q.setDiscount(maincase.getDiscount());
		q.setEmail(maincase.getEmail());
		q.setSalesManCode(maincase.getSalesManCode());
		q.setPayment(maincase.getPayment());
		q.setSalesTerm(maincase.getSalesTerm());
		q.setExtPercent(maincase.getExtPercent());
		q.setItem(maincase.getItem());
		q.setShowCost(maincase.getShowCost());
		q.setSubject(maincase.getSubject());
		q.setApplication(maincase.getApplication());
		q.setIndustry(maincase.getIndustry());
		q.setFreight(maincase.getFreight());
		q.setFreightTotal(maincase.getFreightTotal());
		q.setCourier(maincase.getCourier());
		q.setCourierTotal(maincase.getCourierTotal());
		q.setSalesPerson(maincase.getSalesPerson());
		q.setEmailAddr(maincase.getEmailAddr());
		q.setPaymentTerm(maincase.getPaymentTerm());
		q.setEstPercent(maincase.getEstPercent());
		q.setAssigned(maincase.getAssigned());
		q.setCaseRef(maincase.getCaseRef());
		q.setRemarks(maincase.getRemarks());
		q.setProfit(maincase.getProfit());
		q.setModelTerm(maincase.getModelTerm());
		q.setFax(maincase.getFax());
		q.setQuser(user);
		q.setSubTotal(maincase.getSubTotal());
		q.setGrandTotal(maincase.getGrandTotal());
		q.setAddedTax(maincase.getAddedTax());
		q.setSalesmanPhNo(maincase.getSalesmanPhNo());
		q.setCreatedBy(maincase.getCreatedBy());
		q.setProfit(maincase.getProfit());
		q.setQtnProgress(maincase.getQtnProgress());

		
		if (maincase.getDateQuote() != null) {
			Date dateexp1 = DateUtils.getSqlDateFromString(maincase.getDateQuote(),
					Constants.GenericDateFormat.DATE_FORMAT);
			q.setDateQuote(dateexp1);

		}

		if (maincase.getDateValid() != null)

		{

			Date datecase = DateUtils.getSqlDateFromString(maincase.getDateValid(),
					Constants.GenericDateFormat.DATE_FORMAT);
			q.setDateValid(datecase);

		}
		
		

		return q;
	}








@RequestMapping(value = "/quatationformsubmitforproject",method = { RequestMethod.GET, RequestMethod.POST })
public @ResponseBody boolean quatationformsubmitforproject(MultipartHttpServletRequest req)
 throws Exception {

		String ss = req.getParameter("formData");
		String ss1 = req.getParameter("gridData");
		String ss2=req.getParameter("termConData");
		
		
		org.json.JSONArray gData = new org.json.JSONArray(ss1);
		org.json.JSONArray tgData=new org.json.JSONArray(ss2);

		QuotationFormBean maincase = new ObjectMapper().readValue(ss, QuotationFormBean.class);

		ArrayList<QuotationformGridBean> newselctiongrid = new ArrayList<QuotationformGridBean>();
		for (int i = 0; i < gData.length(); i++) {

			QuotationformGridBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
					QuotationformGridBean.class);

			newselctiongrid.add(newcasegrid);
		}

		
		

		ArrayList<TermsandConditionsGridBean> termsandcondistionsgrid = new ArrayList<TermsandConditionsGridBean>();
		for (int i = 0; i < tgData.length(); i++) 
		{
			TermsandConditionsGridBean termsgrid = new ObjectMapper().readValue(tgData.get(i).toString(),
		TermsandConditionsGridBean.class);
			termsandcondistionsgrid.add(termsgrid);
		}
		
		
		
		
		HttpSession hs=	req.getSession();
		String user=(String)hs.getAttribute("user");

			
		Quotation quatation = this.getQuotationData(maincase,user);

		masterservice.saveQuotation(quatation);
		int quotationid = masterservice.getMaxQuationid();

		List<QuotationGrid> qgrid = this.getquotationtiongrid(quotationid, newselctiongrid);
		masterservice.savequotationGrid(qgrid);

		 List<TermsAndConditionGrid> qtgrid=this.gettermanadconditionsofquationgrid(quotationid,termsandcondistionsgrid);	
			masterservice.savequationtermsandconditiongrid(qtgrid);
				

		return true;

	}



@RequestMapping(value = "/quatationformsubmitforspareParts",method = { RequestMethod.GET, RequestMethod.POST })
public @ResponseBody boolean quatationformsubmitforspareParts(MultipartHttpServletRequest req)
 throws Exception {

		String ss = req.getParameter("formData");

		String ss1 = req.getParameter("gridData");
		
		String ss2=req.getParameter("termConData");
		org.json.JSONArray tgData=new org.json.JSONArray(ss2);


		
		org.json.JSONArray gData = new org.json.JSONArray(ss1);

		QuotationFormBean maincase = new ObjectMapper().readValue(ss, QuotationFormBean.class);

		ArrayList<QuotationformGridBean> newselctiongrid = new ArrayList<QuotationformGridBean>();
		for (int i = 0; i < gData.length(); i++) {

			QuotationformGridBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
					QuotationformGridBean.class);

			newselctiongrid.add(newcasegrid);
		}
		
		
		ArrayList<TermsandConditionsGridBean> termsandcondistionsgrid = new ArrayList<TermsandConditionsGridBean>();
		for (int i = 0; i < tgData.length(); i++) 
		{
			TermsandConditionsGridBean termsgrid = new ObjectMapper().readValue(tgData.get(i).toString(),
		TermsandConditionsGridBean.class);
			termsandcondistionsgrid.add(termsgrid);
		}
		
		
		
		HttpSession hs=	req.getSession();
		String user=(String)hs.getAttribute("user");

		

		Quotation quatation = this.getQuotationData(maincase,user);

		masterservice.saveQuotation(quatation);
		int quotationid = masterservice.getMaxQuationid();

		List<QuotationGrid> qgrid = this.getquotationtiongrid(quotationid, newselctiongrid);
		masterservice.savequotationGrid(qgrid);

		

		 List<TermsAndConditionGrid> qtgrid=this.gettermanadconditionsofquationgrid(quotationid,termsandcondistionsgrid);	
		masterservice.savequationtermsandconditiongrid(qtgrid);
			

		return true;

	}




@RequestMapping(value = "/quatationformsubmitforwithoutspareParts",method = { RequestMethod.GET, RequestMethod.POST })
public @ResponseBody boolean quatationformsubmitforwithoutspareParts(MultipartHttpServletRequest req)
 throws Exception {

		String ss = req.getParameter("formData");

		String ss1 = req.getParameter("gridData");
		
		String ss2=req.getParameter("termConData");
		org.json.JSONArray tgData=new org.json.JSONArray(ss2);


		
		org.json.JSONArray gData = new org.json.JSONArray(ss1);

		QuotationFormBean maincase = new ObjectMapper().readValue(ss, QuotationFormBean.class);

		ArrayList<QuotationformGridBean> newselctiongrid = new ArrayList<QuotationformGridBean>();
		for (int i = 0; i < gData.length(); i++) {

			QuotationformGridBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
					QuotationformGridBean.class);

			newselctiongrid.add(newcasegrid);
		}
		
		
		ArrayList<TermsandConditionsGridBean> termsandcondistionsgrid = new ArrayList<TermsandConditionsGridBean>();
		for (int i = 0; i < tgData.length(); i++) 
		{
			TermsandConditionsGridBean termsgrid = new ObjectMapper().readValue(tgData.get(i).toString(),
		TermsandConditionsGridBean.class);
			termsandcondistionsgrid.add(termsgrid);
		}
		
		
		
		HttpSession hs=	req.getSession();
		String user=(String)hs.getAttribute("user");

		

		Quotation quatation = this.getQuotationData(maincase,user);

		masterservice.saveQuotation(quatation);
		int quotationid = masterservice.getMaxQuationid();

		List<QuotationGrid> qgrid = this.getquotationtiongrid(quotationid, newselctiongrid);
		masterservice.savequotationGrid(qgrid);

		

		 List<TermsAndConditionGrid> qtgrid=this.gettermanadconditionsofquationgrid(quotationid,termsandcondistionsgrid);	
		masterservice.savequationtermsandconditiongrid(qtgrid);
			

		return true;

	}







@RequestMapping(value = "/quatationsequencenumber",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject departmentView(HttpServletRequest req)
{
	JSONObject quatationsequencenumber=new JSONObject();
	JSONObject mainobject=new JSONObject();
	JSONArray quatationsequencenumberarray=new JSONArray();
	HttpSession session=req.getSession();
	String countryCode=(String)session.getAttribute("countrycode");
	String usernasme=(String)session.getAttribute("user");
	  
	String quatationimber=bsscommonService.getSequenceNumber("Q",countryCode,usernasme);
	quatationsequencenumber.put("qot",quatationimber);
	quatationsequencenumberarray.add(quatationsequencenumber);
	mainobject.put("data",quatationsequencenumberarray);
	
/*	quatationsequencenumber.put("assigned","Sales");
	quatationsequencenumber.put("billingCountries","AUSTRALIA");*/
	 
	return mainobject;

}



@RequestMapping(value = "/quatationDefaultCountry",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject quatationDefaultCountry()
{
	JSONObject quatationsequencenumber=new JSONObject();
	JSONArray quatationsequencenumberarray=new JSONArray();
	JSONObject subobject=new JSONObject();
	subobject.put("id","AUSTRALIA");
	  
	//String quatationimber=bsscommonService.getSequenceNumber("Q");
	subobject.put("billingCountries","AUSTRALIA");
	quatationsequencenumberarray.add(subobject);
	quatationsequencenumber.put("data",quatationsequencenumberarray);
	
	 
	return quatationsequencenumber;

}






@RequestMapping(value = "/quatationDefaultStatus",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject quatationDefaultStatus()
{
	JSONObject quatationsequencenumber=new JSONObject();
	JSONArray quatationsequencenumberarray=new JSONArray();
	JSONObject subobject=new JSONObject();
	subobject.put("id","Sales");
	  
	//String quatationimber=bsscommonService.getSequenceNumber("Q");
	subobject.put("assigned","Sales");
	quatationsequencenumberarray.add(subobject);
	quatationsequencenumber.put("data",quatationsequencenumberarray);
	
	 
	return quatationsequencenumber;
	 
}



@RequestMapping(value = "/getAddedQuatationscustmerNames",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getAddedQuatationscustmerNames(HttpServletRequest req)
{
	JSONObject quatationsequencenumber=new JSONObject();
	JSONArray quatationsequencenumberarray=new JSONArray();
	//JSONObject subquatationsequencenumber=new JSONObject();
	HttpSession hs=req.getSession();
	String user=(String)hs.getAttribute("user");
	  
	List<Quotation> addedquatattioncustmers=masterservice.getAddedQuatationCustmerNames(user);
	for(Quotation q:addedquatattioncustmers)
	{
		JSONObject subquatationsequencenumber=new JSONObject();
		subquatationsequencenumber.put("custmername",q.getCustName());
		quatationsequencenumberarray.add(subquatationsequencenumber);
		
		
	}
	quatationsequencenumber.put("data",quatationsequencenumberarray);
	//quatationsequencenumber.put("qot",quatationimber);
	
	return quatationsequencenumber;

}



@RequestMapping(value = "/getingSalesmanCode",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getingSalesmanCode(HttpServletRequest req)
{
	JSONObject quatationsequencenumber=new JSONObject();
	JSONArray quatationsequencenumberarray=new JSONArray();
	//JSONObject subquatationsequencenumber=new JSONObject();
	HttpSession hs=req.getSession();
	String user=(String)hs.getAttribute("user");
	  
	List<String> addedquatattioncustmers=masterservice.getSalesMancode();
	for(String q:addedquatattioncustmers)
	{
		JSONObject subquatationsequencenumber=new JSONObject();
		subquatationsequencenumber.put("salesManCode",q);
		quatationsequencenumberarray.add(subquatationsequencenumber);
		
		
	}
	quatationsequencenumber.put("data",quatationsequencenumberarray);
	//quatationsequencenumber.put("qot",quatationimber);
	
	return quatationsequencenumber;

}



@RequestMapping(value = "/getingSalesman",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getingSalesman(HttpServletRequest req)
{
	JSONObject mainobject=new JSONObject();
	JSONArray quatationsequencenumberarray=new JSONArray();
	//JSONObject subquatationsequencenumber=new JSONObject();
	
String salesmancode=req.getParameter("salesmancode");
	  
	List<String> addedquatattioncustmers=masterservice.getSalesManName(salesmancode);
	for(String q:addedquatattioncustmers)
	{
		 
		mainobject.put("salesManCode",q);
		 
		
		
	}
	 
	
	return mainobject;

}





@RequestMapping(value = "/getingTaxPercentages",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getingTaxPercentages(HttpServletRequest req)
{
	JSONObject mainobject=new JSONObject();
	JSONArray quatationsequencenumberarray=new JSONArray();
	//JSONObject subquatationsequencenumber=new JSONObject();
	
String salesmancode=req.getParameter("salesmancode");
	  
	List<String> addedquatattioncustmers=masterservice.getTaxPercentages();
	for(String q:addedquatattioncustmers)
	{
		JSONObject subobject=new JSONObject();
		 
		subobject.put("id",q);
		subobject.put("taxPercent",q);
		quatationsequencenumberarray.add(subobject);
		 
		
		
	}
	 mainobject.put("data",quatationsequencenumberarray);
	
	return mainobject;

}


@RequestMapping(value = "/getingCurrencies",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getingCurriences(HttpServletRequest req)
{
	JSONObject mainobject=new JSONObject();
	JSONArray currencies=new JSONArray();
	//JSONObject subquatationsequencenumber=new JSONObject();
	
 
	  
	List<String> addedquatattioncustmers=masterservice.getCurrencies();
	for(String q:addedquatattioncustmers)
	{
		JSONObject subobject=new JSONObject();
		 
		subobject.put("id",q);
		subobject.put("currency",q);
		currencies.add(subobject);
		 
		
		
	}
	 mainobject.put("data",currencies);
	
	return mainobject;

}




@RequestMapping(value = "/getingTerms",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getingTerms(HttpServletRequest req)
{
	JSONObject mainobject=new JSONObject();
	JSONArray currencies=new JSONArray();
	//JSONObject subquatationsequencenumber=new JSONObject();
	
	Session session=hibernateDao.getSessionFactory().openSession();
	String hql = "from TermsandConditions";
	Query query = session.createQuery(hql);
	List<TermsandConditions> results = query.list();
	
	  
	 
	for(TermsandConditions q:results)
	{
		JSONObject subobject=new JSONObject();
		 
		subobject.put("id",q.getTerms());
		subobject.put("modelTerm",q.getTerms());
		currencies.add(subobject);
		 
		
		
	}
	 mainobject.put("data",currencies);
	
	return mainobject;

}

@RequestMapping(value = "/getingCondition",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getingTermsandconditions(HttpServletRequest req)
{
	JSONObject mainobject=new JSONObject();
	JSONArray currencies=new JSONArray();
	//JSONObject subquatationsequencenumber=new JSONObject();
	String term=req.getParameter("modelterm");
	Session session=hibernateDao.getSessionFactory().openSession();
	String hql = "from TermsandConditions where terms='"+term+"'";
	Query query = session.createQuery(hql);
	List<TermsandConditions> results = query.list();
	
	  
	 
	for(TermsandConditions q:results)
	{
		JSONObject subobject=new JSONObject();
		 
		subobject.put("id",q.getConditions());
		subobject.put("modelCon",q.getConditions());
		currencies.add(subobject);
		 
		
		
	}
	 mainobject.put("data",currencies);
	
	return mainobject;

}




@RequestMapping(value = "/getingQuatationOnCaseRefrence",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getingQuatationOnCaseRefrence(HttpServletRequest req)
{
	
	String custmercode=null;
	HttpSession hs=req.getSession();
	JSONObject mainobject=new JSONObject();
	String caserefrence=(String)hs.getAttribute("caseref");
	int tab=0;
	 
	 String countryCode=(String)hs.getAttribute("countrycode");
	 String usernasme=(String)hs.getAttribute("user");

	
	
	Session session=hibernateDao.getSessionFactory().openSession();
	String hql = "from CaseSummary where caseRef='"+caserefrence+"'";
	Query query = session.createQuery(hql);
	List<CaseSummary> results = query.list();
	for(CaseSummary f:results)
	{
		
		custmercode=f.getCustomerCode();
		tab=f.getTab();
	}
	
	
	
	JSONObject custmerdetailsobect= this.custmerdatabybasingoncustmercode(custmercode,caserefrence,countryCode,usernasme);
	 
	JSONObject gridobject= this.getQuatationData(caserefrence,tab);
	String quatationimber=bsscommonService.getSequenceNumber("Q",countryCode,usernasme);
	 
	mainobject.put("custobject",custmerdetailsobect); 
	mainobject.put("custgrid",gridobject);
	mainobject.put("sequencenumber",quatationimber);
	 
	return mainobject;

}











public JSONObject getQuatationData(String caseref,int tab )
{
	

	

	JSONObject mainobject = new JSONObject();
	JSONArray gridarray = null;
	String token = null;

	 		

		if(tab== 1)
		{
		List<NewSelectionGrid> newselectiongrid = masterservice.getDetailsFromNewSelectionGrid(caseref);
		gridarray = new JSONArray();
		int i = 1;
		for (NewSelectionGrid nst : newselectiongrid) {

			JSONObject gridobject = new JSONObject();
			gridobject.put("gm", nst.getGm());
			gridobject.put("id", i);
			gridobject.put("gearMotor", nst.getGearMotor());
			gridobject.put("quantity", nst.getQuantity());
			gridobject.put("application", nst.getApplication());
			gridobject.put("ambientTemp", nst.getAmbientTemp());
			gridobject.put("gearProduct", nst.getGearProduct());
			gridobject.put("model", nst.getModel());
			gridobject.put("selectModel", nst.getSelectModel());
			gridobject.put("servFactor", nst.getServFactor());
			gridobject.put("motorType", nst.getMotorType());
			gridobject.put("motorKW", nst.getMotorKW().toString());
			gridobject.put("pole", nst.getPole());
			gridobject.put("phase", nst.getPhase());
			gridobject.put("voltage", nst.getVoltage());
			gridobject.put("hertz", nst.getHertz());
			gridobject.put("rpm", nst.getRpm());
			gridobject.put("protectionGrade", nst.getProtectionGrade());
			gridobject.put("internationalStd", nst.getInternationalStd());
			gridobject.put("motorBrake", nst.getMotorBrake());
			gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
			gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
			gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
			gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
			gridobject.put("dirAxial", nst.getDirAxial());
			gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
			gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
			gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
			gridobject.put("dirSssAxial", nst.getDirSssAxial());
			gridobject.put("ratioRequired", nst.getRatioRequired());
			gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
			gridobject.put("presetTorque", nst.getPresetTorque());
			gridobject.put("conf", nst.getConf());
			gridobject.put("mount", nst.getMount());
			 
			
		 
			gridobject.put("mount2",nst.getMount2());
		
			 
			if(nst.getOshaftInc2().equalsIgnoreCase("true"))
			{
				gridobject.put("oshaftInc2",1);
				
			}
			
			else
			{
				gridobject.put("oshaftInc2",0);
				
			}
			
			if(nst.getOshaftInc1().equalsIgnoreCase("true"))
			{
				 
				 gridobject.put("oshaftInc1",1);
				 
				}
			
			else
			{
				
				 gridobject.put("oshaftInc1",0);
			}
			
			gridobject.put("ishaft", nst.getIshaft());
			gridobject.put("oshaft", nst.getOshaft());
			gridobject.put("oshaftInc", nst.getOshaftInc());
			gridobject.put("deg", nst.getDeg());
			gridobject.put("towards", nst.getTowards());
			gridobject.put("oshafty", nst.getOshafty());
			gridobject.put("oshafty2", nst.getOshafty2());
			gridobject.put("backStop", nst.getBackStop());
			gridobject.put("rot", nst.getRot());
			gridobject.put("dirHssAxial", nst.getDirHssAxial());
			gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
			gridobject.put("inverter",nst.getInverter());
			 
			gridobject.put("other",nst.getOther());
			gridobject.put("othersPlz",nst.getOthersPlz());
			
			
			String ssccodes = nst.getSscCode();
			if (ssccodes != null) {
				String sscode[] = ssccodes.split(",");
				gridobject.put("sscCode", sscode);
			}

			gridobject.put("docFile", nst.getImagepath());
			gridobject.put("gridid", i);
			gridarray.add(gridobject);

			i++;

		}

		if (gridarray.size() == 0) {
			JSONObject gridobject = new JSONObject();
			gridobject.put("id", 1);
			gridarray.add(gridobject);
		}

		mainobject.put("griddata", gridarray);
		//mainobject.put("maincaseform", manincaseobjectarray);
		//mainobject.put("description", descriptionobjectarray);
		mainobject.put("tab", 1);

		return mainobject;

	}

		else if (tab == 2) {
		

		List<CompititorGrid> newselectiongrid = masterservice.getDetailsFromCompititorGrid(caseref);
		gridarray = new JSONArray();
		int i = 1;
		for (CompititorGrid nst : newselectiongrid) {

			JSONObject gridobject = new JSONObject();
			gridobject.put("gm", nst.getGm());
			gridobject.put("id", i);
			gridobject.put("gearMotor", nst.getGearMotor());
			gridobject.put("quantity", nst.getQuantity());
			gridobject.put("application", nst.getApplication());
			gridobject.put("ambientTemp", nst.getAmbientTemp());
			gridobject.put("gearProduct", nst.getGearProduct());
			gridobject.put("model", nst.getModel());
			gridobject.put("selectModel", nst.getSelectModel());
			gridobject.put("servFactor", nst.getRequiredSreviceFactor());
			gridobject.put("motorType", nst.getMotorType());
			gridobject.put("motorKW", nst.getMotorKW().toString());
			gridobject.put("pole", nst.getPole());
			gridobject.put("phase", nst.getPhase());
			gridobject.put("voltage", nst.getVoltage());
			gridobject.put("hertz", nst.getHertz());
			gridobject.put("rpm", nst.getRpm());
			gridobject.put("protectionGrade", nst.getProtectionGrade());
			gridobject.put("internationalStd", nst.getInternationalStd());
			gridobject.put("motorBrake", nst.getMotorBrake());
			gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
			gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
			gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
			gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
			/*gridobject.put("dirAxial", nst.getDirAxial());*/
			gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
			gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
			gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
			gridobject.put("dirSssAxial", nst.getDirSssAxial());
			gridobject.put("ratioRequired", nst.getRatioRequired());
			gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
			gridobject.put("presetTorque", nst.getPresetTorque());
			gridobject.put("conf", nst.getConf());
			gridobject.put("mount", nst.getMount());
			//gridobject.put("mount2", nst.getMount2());
			//gridobject.put("mount3", nst.getMount3());
			gridobject.put("ishaft", nst.getIshaft());
			gridobject.put("oshaft", nst.getOshaft());
			gridobject.put("oshaftInc", nst.getOshaftInc());
			gridobject.put("deg", nst.getDeg());
			gridobject.put("towards", nst.getTowards());
			gridobject.put("oshafty", nst.getOshafty());
			gridobject.put("oshafty2", nst.getOshafty2());
			gridobject.put("backStop", nst.getBackStop());
			gridobject.put("rot", nst.getRot());
			gridobject.put("dirHssAxial", nst.getDirHssAxial());
			gridobject.put("requiredSreviceFactor",nst.getRequiredSreviceFactor());
			gridobject.put("competitorBrand", nst.getCompetitorBrand());
			gridobject.put("competitorModel",nst.getCompetitorModel());
			gridobject.put("targetPrice",nst.getTargetPrice());
			gridobject.put("dimension",nst.getDimension());
			
			gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
			gridobject.put("inverter",nst.getInverter());
			
 
			gridobject.put("mount2",nst.getMount2());
			 
			 
			if(nst.getOshaftInc2().equalsIgnoreCase("true"))
			{
				gridobject.put("oshaftInc2",1);
				
			}
			
			else
			{
				gridobject.put("oshaftInc2",0);
				
			}
			
			if(nst.getOshaftInc1().equalsIgnoreCase("true"))
			{
				 
				 gridobject.put("oshaftInc1",1);
				 
				}
			
			else
			{
				
				 gridobject.put("oshaftInc1",0);
			}
			
							
			//gridobject.put("oshaftInc2",nst.getOshaftInc2());
			//gridobject.put("oshaftInc1",nst.getOshaftInc1());
			gridobject.put("other",nst.getOther());
			gridobject.put("othersPlz",nst.getOthersPlz());
			
			
		
		
			
		 
			
			String ssccodes = nst.getCompititorcode();
			if (ssccodes != null) {
				String sscode[] = ssccodes.split(",");
				gridobject.put("compSSCCode", sscode);
			}
			

			gridobject.put("docFile", nst.getImagepath());
			gridobject.put("gridid", i);
			gridarray.add(gridobject);

			i++;

		}

		if (gridarray.size() == 0) {
			JSONObject gridobject = new JSONObject();
			gridobject.put("id", 1);
			gridarray.add(gridobject);
		}

		mainobject.put("griddata", gridarray);
		//mainobject.put("maincaseform", manincaseobjectarray);
		//mainobject.put("description", descriptionobjectarray);
		mainobject.put("tab", 2);

		 

	
		
		return mainobject;
	}
	
	else if(tab == 3)
	{
		

		List<SumitomoReplaceGrid> newselectiongrid = masterservice.getDetailsFromSumitimGrid(caseref);
		gridarray = new JSONArray();
		int i = 1;
		for (SumitomoReplaceGrid nst : newselectiongrid) {

			JSONObject gridobject = new JSONObject();
			gridobject.put("gm", nst.getGm());
			gridobject.put("id", i);
			gridobject.put("gearMotor", nst.getGearMotor());
			gridobject.put("quantity", nst.getQuantity());
			gridobject.put("application", nst.getApplication());
			gridobject.put("ambientTemp", nst.getAmbientTemp());
			gridobject.put("gearProduct", nst.getGearProduct());
			gridobject.put("model", nst.getModel());
			gridobject.put("selectModel", nst.getSelectModel());
			gridobject.put("servFactor", nst.getServFactor());
			gridobject.put("motorType", nst.getMotorType());
			gridobject.put("motorKW", nst.getMotorKW().toString());
			gridobject.put("pole", nst.getPole());
			gridobject.put("phase", nst.getPhase());
			gridobject.put("voltage", nst.getVoltage());
			gridobject.put("hertz", nst.getHertz());
			gridobject.put("rpm", nst.getRpm());
			gridobject.put("protectionGrade", nst.getProtectionGrade());
			gridobject.put("internationalStd", nst.getInternationalStd());
			gridobject.put("motorBrake", nst.getMotorBrake());
			gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
			gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
			gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
			gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
			gridobject.put("dirAxial", nst.getDirAxial());
			gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
			gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
			gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
			gridobject.put("dirSssAxial", nst.getDirSssAxial());
			gridobject.put("ratioRequired", nst.getRatioRequired());
			gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
			gridobject.put("presetTorque", nst.getPresetTorque());
			gridobject.put("conf", nst.getConf());
			gridobject.put("mount", nst.getMount());
			//gridobject.put("mount2", nst.getMount2());
			//gridobject.put("mount3", nst.getMount3());
			gridobject.put("ishaft", nst.getIshaft());
			gridobject.put("oshaft", nst.getOshaft());
			gridobject.put("oshaftInc", nst.getOshaftInc());
			gridobject.put("deg", nst.getDeg());
			gridobject.put("towards", nst.getTowards());
			gridobject.put("oshafty", nst.getOshafty());
			gridobject.put("oshafty2", nst.getOshafty2());
			gridobject.put("backStop", nst.getBackStop());
			gridobject.put("rot", nst.getRot());
			gridobject.put("dirHssAxial", nst.getDirHssAxial());
			
			gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
			gridobject.put("inverter",nst.getInverter());
			

			 
			gridobject.put("mount2",nst.getMount2());
		 
			if(nst.getOshaftInc2().equalsIgnoreCase("true"))
			{
				gridobject.put("oshaftInc2",1);
				
			}
			
			else
			{
				gridobject.put("oshaftInc2",0);
				
			}
			
			if(nst.getOshaftInc1().equalsIgnoreCase("true"))
			{
				 
				 gridobject.put("oshaftInc1",1);
				 
				}
			
			else
			{
				
				 gridobject.put("oshaftInc1",0);
			}
			
			//gridobject.put("oshaftInc2",nst.getOshaftInc2());
			//gridobject.put("oshaftInc1",nst.getOshaftInc1());
			gridobject.put("other",nst.getOther());
			gridobject.put("othersPlz",nst.getOthersPlz());
			
			
			
			String ssccodes = nst.getSscCode();
			if (ssccodes != null) {
				String sscode[] = ssccodes.split(",");
				gridobject.put("sscCode", sscode);
			}

			gridobject.put("docFile", nst.getImagepath());
			gridobject.put("gridid", i);
			
			 
			
			gridobject.put("gearhead",nst.getGearhead());
			gridobject.put("motorSerial",nst.getMotorSerial());
			gridobject.put("shiMfg",nst.getShiMfg());
			gridarray.add(gridobject);

			i++;

		}

		if (gridarray.size() == 0) {
			JSONObject gridobject = new JSONObject();
			gridobject.put("id", 1);
			gridarray.add(gridobject);
		}

		mainobject.put("griddata", gridarray);
		//mainobject.put("maincaseform", manincaseobjectarray);
		//mainobject.put("description", descriptionobjectarray);
		mainobject.put("tab", 3);

		return mainobject;

	
		
		
	}
	
	else if(tab==4)
	{

		List<SparePartGrid> newselectiongrid = masterservice.getDetailsFromSparePartGrid(caseref);
		gridarray = new JSONArray();
		int i = 1;
		for (SparePartGrid nst : newselectiongrid) {

			JSONObject gridobject = new JSONObject();
			gridobject.put("gm", nst.getGm());
			gridobject.put("id", i);
			gridobject.put("gearMotor", nst.getGearMotor());
			gridobject.put("quantity", nst.getQuantity());
			gridobject.put("application", nst.getApplication());
			gridobject.put("ambientTemp", nst.getAmbientTemp());
			gridobject.put("gearProduct", nst.getGearProduct());
			gridobject.put("model", nst.getModel());
			gridobject.put("selectModel", nst.getSelectModel());
			gridobject.put("servFactor", nst.getServFactor());
			gridobject.put("motorType", nst.getMotorType());
			gridobject.put("motorKW", nst.getMotorKW().toString());
			gridobject.put("pole", nst.getPole());
			gridobject.put("phase", nst.getPhase());
			gridobject.put("voltage", nst.getVoltage());
			gridobject.put("hertz", nst.getHertz());
			gridobject.put("rpm", nst.getRpm());
			gridobject.put("protectionGrade", nst.getProtectionGrade());
			gridobject.put("internationalStd", nst.getInternationalStd());
			gridobject.put("motorBrake", nst.getMotorBrake());
			gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
			gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
			gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
			gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
			gridobject.put("dirAxial", nst.getDirAxial());
			gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
			gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
			gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
			gridobject.put("dirSssAxial", nst.getDirSssAxial());
			gridobject.put("ratioRequired", nst.getRatioRequired());
			gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
			gridobject.put("presetTorque", nst.getPresetTorque());
			gridobject.put("conf", nst.getConf());
			gridobject.put("mount", nst.getMount());
			//gridobject.put("mount2", nst.getMount2());
			//gridobject.put("mount3", nst.getMount3());
			gridobject.put("ishaft", nst.getIshaft());
			gridobject.put("oshaft", nst.getOshaft());
			gridobject.put("oshaftInc", nst.getOshaftInc());
			gridobject.put("deg", nst.getDeg());
			gridobject.put("towards", nst.getTowards());
			gridobject.put("oshafty", nst.getOshafty());
			gridobject.put("oshafty2", nst.getOshafty2());
			gridobject.put("backStop", nst.getBackStop());
			gridobject.put("rot", nst.getRot());
			gridobject.put("dirHssAxial", nst.getDirHssAxial());
			String ssccodes = nst.getSscCode();
			if (ssccodes != null) {
				String sscode[] = ssccodes.split(",");
				gridobject.put("sscCode", sscode);
			}

			gridobject.put("docFile", nst.getImagepath());
			gridobject.put("gridid", i);
			
			gridobject.put("spare",nst.getSpare());
			gridobject.put("descript",nst.getDescript());
			gridobject.put("qty",nst.getQty());
			gridobject.put("spare1",nst.getSpare1());
			gridobject.put("descript1",nst.getDescript1());
			gridobject.put("qty1",nst.getQty1());
			gridobject.put("spare2",nst.getSpare2());
			gridobject.put("descript2",nst.getDescript2());
			gridobject.put("qty2",nst.getQty2());
			
			
			
			
			gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
			gridobject.put("inverter",nst.getInverter());
			

			/*if(nst.getMount2().equalsIgnoreCase("true"))
			{
			gridobject.put("mount2",1);
			}
			else
			{
			gridobject.put("mount2", 0);
			}
			if(nst.getMount3().equalsIgnoreCase("true"))
			{
				gridobject.put("mount3",1);
				
			}
			else
			{
				gridobject.put("mount3",0);
			}
			
			if(nst.getOshaftInc2().equalsIgnoreCase("true"))
			{
				gridobject.put("oshaftInc2",1);
				
			}
			
			else
			{
				gridobject.put("oshaftInc2",0);
				
			}
			
			if(nst.getOshaftInc1().equalsIgnoreCase("true"))
			{
				 
				 gridobject.put("oshaftInc1",1);
				 
				}
			
			else
			{
				
				 gridobject.put("oshaftInc1",0);
			}
			
		*/	//gridobject.put("oshaftInc2",nst.getOshaftInc2());
			//gridobject.put("oshaftInc1",nst.getOshaftInc1());
			gridobject.put("other",nst.getOther());
			gridobject.put("othersPlz",nst.getOthersPlz());
			
			
			gridobject.put("gearhead",nst.getGearhead());
			gridobject.put("motorSerial",nst.getMotorSerial());
			gridobject.put("shiMfg",nst.getShiMfg());
			gridobject.put("accessMotor",nst.getAccessMotor());
		
			gridarray.add(gridobject);

			i++;

		}

		if (gridarray.size() == 0) {
			JSONObject gridobject = new JSONObject();
			gridobject.put("id", 1);
			gridarray.add(gridobject);
		}

		mainobject.put("griddata", gridarray);
		//mainobject.put("maincaseform", manincaseobjectarray);
		//mainobject.put("description", descriptionobjectarray);
		
		
		
		
		mainobject.put("tab", 4);

		return mainobject;

	
	}
		
		if(tab==5)
			
		{
			

			List<TechGrid> newselectiongrid = masterservice.getDetailsFromTechGrid(caseref);
			gridarray = new JSONArray();
			int i = 1;
			for (TechGrid nst : newselectiongrid) {

				JSONObject gridobject = new JSONObject();
				gridobject.put("gm", nst.getGm());
				gridobject.put("id", i);
				gridobject.put("gearMotor", nst.getGearMotor());
				gridobject.put("quantity", nst.getQuantity());
				gridobject.put("application", nst.getApplication());
				gridobject.put("ambientTemp", nst.getAmbientTemp());
				gridobject.put("gearProduct", nst.getGearProduct());
				gridobject.put("model", nst.getModel());
				gridobject.put("selectModel", nst.getSelectModel());
				gridobject.put("servFactor", nst.getServFactor());
				gridobject.put("motorType", nst.getMotorType());
				gridobject.put("motorKW", nst.getMotorKW().toString());
				gridobject.put("pole", nst.getPole());
				gridobject.put("phase", nst.getPhase());
				gridobject.put("voltage", nst.getVoltage());
				gridobject.put("hertz", nst.getHertz());
				gridobject.put("rpm", nst.getRpm());
				gridobject.put("protectionGrade", nst.getProtectionGrade());
				gridobject.put("internationalStd", nst.getInternationalStd());
				gridobject.put("motorBrake", nst.getMotorBrake());
				gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
				gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
				gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
				gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
				gridobject.put("dirAxial", nst.getDirAxial());
				gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
				gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
				gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
				gridobject.put("dirSssAxial", nst.getDirSssAxial());
				gridobject.put("ratioRequired", nst.getRatioRequired());
				gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
				gridobject.put("presetTorque", nst.getPresetTorque());
				gridobject.put("conf", nst.getConf());
				gridobject.put("mount", nst.getMount());
				//gridobject.put("mount2", nst.getMount2());
				//gridobject.put("mount3", nst.getMount3());
				gridobject.put("ishaft", nst.getIshaft());
				gridobject.put("oshaft", nst.getOshaft());
				gridobject.put("oshaftInc", nst.getOshaftInc());
				gridobject.put("deg", nst.getDeg());
				gridobject.put("towards", nst.getTowards());
				gridobject.put("oshafty", nst.getOshafty());
				gridobject.put("oshafty2", nst.getOshafty2());
				gridobject.put("backStop", nst.getBackStop());
				gridobject.put("rot", nst.getRot());
				gridobject.put("dirHssAxial", nst.getDirHssAxial());
				gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
				gridobject.put("inverter",nst.getInverter());
				
				String ssccodes = nst.getSscCode();
				if (ssccodes != null) {
					String sscode[] = ssccodes.split(",");
					gridobject.put("sscCode", sscode);
				}

				gridobject.put("docFile", nst.getImagepath());
				gridobject.put("gridid", i);
				gridobject.put("requiredSreviceFactor",nst.getRequiredSreviceFactor());
				gridobject.put("gearhead",nst.getGearhead());
				gridobject.put("motorSerial",nst.getMotorSerial());
				gridobject.put("shiMfg",nst.getShiMfg());

			 
				gridobject.put("mount2",nst.getMount2());
			 
			 
				if(nst.getOshaftInc2().equalsIgnoreCase("true"))
				{
					gridobject.put("oshaftInc2",1);
					
				}
				
				else
				{
					gridobject.put("oshaftInc2",0);
					
				}
				
				if(nst.getOshaftInc1().equalsIgnoreCase("true"))
				{
					 
					 gridobject.put("oshaftInc1",1);
					 
					}
				
				else
				{
					
					 gridobject.put("oshaftInc1",0);
				}
				
				//gridobject.put("oshaftInc2",nst.getOshaftInc2());
				//gridobject.put("oshaftInc1",nst.getOshaftInc1());
				gridobject.put("other",nst.getOther());
				gridobject.put("othersPlz",nst.getOthersPlz());
				
				
				gridarray.add(gridobject);

				i++;

			}

			if (gridarray.size() == 0) {
				JSONObject gridobject = new JSONObject();
				gridobject.put("id", 1);
				gridarray.add(gridobject);
			}

			mainobject.put("griddata", gridarray);
			//mainobject.put("maincaseform", manincaseobjectarray);
			//
			//mainobject.put("description", descriptionobjectarray);
			mainobject.put("tab", 5);

			return mainobject;

		
			
		}
	
	
		return mainobject;

	
 

	
	
}


































public  JSONObject custmerdatabybasingoncustmercode(String custmername1,String caserefrence, String countryCode,String username) 
{
 	 
	   try{
	   
		 
			/*String custmername1=null;*/
			String applicationname=null;
			 /*
			 String custmername=req.getParameter("account");
			 if(custmername!=null)
			 {
			   custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
			 }*/
			 JSONObject mainobject=new JSONObject();
			 JSONArray mainarray=new  JSONArray();
			// JSONArray custmerarray=new  JSONArray();
			 
					 
		List<CustmerDetails>	cutmerdetails= masterservice.getAllCustmersDeatails(custmername1);
		
		mainobject.put("check",0);
		for(CustmerDetails a:cutmerdetails)
			
		{
			JSONObject submainobject=new JSONObject();
			JSONArray custmerarray=new  JSONArray();
			submainobject.put("custid",a.getCustid());
			
			submainobject.put("customerStatus",a.getCustomerStatus());
			submainobject.put("account",a.getAccount());
			submainobject.put("accountid",a.getAccountid());
			submainobject.put("clientwebsite",a.getClientwebsite());
			submainobject.put("mailingadress",a.getMailingadress());
			submainobject.put("shippingadress",a.getShippingadress());
			submainobject.put("billingCountries",a.getBillingCountries());
			submainobject.put("language",a.getLanguage());
			submainobject.put("customerGroup",a.getCustomerType());
			submainobject.put("industries",a.getIndustries());
			submainobject.put("application",a.getApplication());
			submainobject.put("territory",a.getTerritory());
			submainobject.put("caseRef",caserefrence);
			submainobject.put("corporate",a.getCorporate());
			submainobject.put("contName", a.getContName());
			submainobject.put("title", a.getTitle());
			submainobject.put("phnumber",a.getPhnumber());
			submainobject.put("mbnumber",a.getMbnumber());
			submainobject.put("ophnumber",a.getOphnumber());
			submainobject.put("creditStatus",a.getCreditstatus());
			submainobject.put("email",a.getEmail());
			submainobject.put("fax", a.getFax());
			if(a.getApplicanindustryname()==null && a.getApplication()!=null)
			{
				//String applicationcode=req.getParameter( "applicanIndusName");
				
				List<ApplicationIndustryCode> coutries=migratesevice.getAllApplicationCodeName(a.getApplication());
				 
				for(ApplicationIndustryCode a2:coutries)
				{
					
					applicationname=a2.getTotalname();
					
				}
				 
				submainobject.put("applicanIndustry",applicationname);
				 
			}
			
			else
			{
				submainobject.put("applicanIndustry",a.getApplicanindustryname());
			}
			 
			submainobject.put("departmentname",a.getDepartmentname());
			submainobject.put("reports",a.getReports());
			if(a.getSalesPerson()!=null)
			{
				submainobject.put("salesPerson",a.getSalesPerson());
				
			}
			
			else
			{
				String salesman=masterservice.getSalesmanOnSalesCode(a.getSalesPersonCode());
				submainobject.put("salesPerson",salesman);
				
				
			}
			 
			submainobject.put("discountmultiplier",a.getDiscountmultiplier());
			submainobject.put("salesPersonCode",a.getSalesPersonCode());
			submainobject.put("leadsource",a.getLeadsource());
			submainobject.put("currency", a.getCurrency());
			submainobject.put("taxName",a.getTaxName());
			submainobject.put("paymentTerm",a.getPaymentTerm());
			submainobject.put("opportunityAmount", a.getOpportunityAmount());
			if(a.getBddate()!=null)
			{
				String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
				submainobject.put("bddate",k1 );
			}
			
			
			submainobject.put("createdby",a.getCreatedby());
			submainobject.put("lastmodifiedby",a.getLastmodifiedby());
			submainobject.put("imagename", a.getImagename());
			 
			custmerarray.add(submainobject);
			mainobject.put("custmer",custmerarray);
		    
			
			 
			 /*custmerobject.put("account",a);
			custmerarray.add(custmerobject);*/
	/*	String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
		String k2 = k1.replace("-","/");
			 
		String[] k2=k1.split("-");
		 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
		*/
		
		/*DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");*/
		
	/*	Date frmDate = sdf.parse(k2);*/
		/*	mainobject.put("date",k2);*/
			//mainobject.put("date",k1);
			mainobject.put("check",1);
			//mainobject.put("custmer",a);
			//mainobject.put("custid",a.getCustid());
			List<CustmerContactDetails> contactdetails=a.getRolescreens();
			int i=1;
			
			if(contactdetails.size()==0)
			{
				
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("contactName",null);
				subobject.put("contactNumber",null);
				subobject.put("contactAltNumber",null);
				subobject.put("gdepatment",null);
				subobject.put("email",null);
				subobject.put("gtitle",null);
				mainarray.add(subobject);
				
				 
				
			}
			
			for(CustmerContactDetails b:contactdetails)
			{
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("contactName",b.getContactName());
				subobject.put("contactNumber",b.getContactNumber());
				subobject.put("contactAltNumber",b.getContactAltNumber());
				subobject.put("department",b.getDepartment());
				subobject.put("emailAddr",b.getEmailAddr());
				subobject.put("titleName",b.getTitleName());
				mainarray.add(subobject);
				
				i++;
			}
			
		}
		/*mainobject.put("custmer",custmerarray);	 */
		mainobject.put("grid",mainarray );
		//mainobject.put("custmer",custmerarray);
		
		
		
		JSONObject quatationsequencenumber=new JSONObject();
		//JSONObject mainobject=new JSONObject();
		JSONArray quatationsequencenumberarray=new JSONArray();
		
		  
		String quatationimber=bsscommonService.getSequenceNumber("Q",countryCode,username);
		quatationsequencenumber.put("qot",quatationimber);
		quatationsequencenumberarray.add(quatationsequencenumber);
		mainobject.put("data",quatationsequencenumberarray);

			 
			 return mainobject;
			 
	   }catch(Exception e)
	   {
		   logger.info("=================controllerrrr================"+e.getMessage());
		   return null;
				   
	   }
			 
	}
	 
 

@RequestMapping(value = "/getSavedQuatationData",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getSavedQuatationData(HttpServletRequest req)
{
	

	HttpSession hs=req.getSession();
	String caserefrence=(String)hs.getAttribute("caseref");
	int qid=0;
	String quatationnumber=req.getParameter("quationnumber");
	Session session=hibernateDao.getSessionFactory().openSession();
	String hql = "from Quotation where qtnNo='"+quatationnumber+"'";
	Query query = session.createQuery(hql);
	List<Quotation> results = query.list();
	
int	qtntypeforprintout=0;
	JSONObject quatation=new JSONObject();
	JSONArray quatatinformarray=new JSONArray();
	for(Quotation f:results)
	{
		qid=f.getQid();
		JSONObject quatatinsubobject=new JSONObject();
		quatatinsubobject.put("qtnType",f.getQtnType());
		 qtntypeforprintout=Integer.parseInt(f.getQtnType());
		quatatinsubobject.put("qtnNo",f.getQtnNo());
		quatatinsubobject.put("custName",f.getCustName());
		if (f.getDateQuote() != null) {
			//d1 = data.getDateExp();
			String k1 = DateUtils.formatDate(f.getDateQuote(),Constants.GenericDateFormat.DATE_FORMAT);
			quatatinsubobject.put("dateQuote", k1);
                                   }
		quatatinsubobject.put("currency",f.getCurrency());
		if (f.getDateValid() != null) {
			//d1 = data.getDateExp();
			String k1 = DateUtils.formatDate(f.getDateValid(),Constants.GenericDateFormat.DATE_FORMAT);
			quatatinsubobject.put("dateValid", k1);
}
		
		quatatinsubobject.put("tax",f.getTax());
		quatatinsubobject.put("copy",f.getCopy());
		quatatinsubobject.put("country",f.getCountry());
		quatatinsubobject.put("salesMan",f.getSalesMan());
		quatatinsubobject.put("taxPercent",f.getTaxPercent());
		quatatinsubobject.put("telephone",f.getTelephone());
		quatatinsubobject.put("custRef",f.getCustRef());
		quatatinsubobject.put("salesManCode",f.getSalesManCode());
		quatatinsubobject.put("taxPercent",f.getTaxPercent());
		quatatinsubobject.put("telephone",f.getTelephone());
		quatatinsubobject.put("custRef",f.getCustRef());
        quatatinsubobject.put("discount",f.getDiscount());
		quatatinsubobject.put("email",f.getEmail());
		quatatinsubobject.put("payment",f.getPayment());
		quatatinsubobject.put("salesTerm",f.getSalesTerm());
		quatatinsubobject.put("extPercent",f.getExtPercent());
		quatatinsubobject.put("item",f.getItem());
		quatatinsubobject.put("showCost",f.getShowCost());
		quatatinsubobject.put("subject",f.getSubject());
		quatatinsubobject.put("application",f.getApplication());
		quatatinsubobject.put("industry",f.getIndustry());
		quatatinsubobject.put("freight", f.getFreight());
		quatatinsubobject.put("freightTotal",f.getFreightTotal());
		quatatinsubobject.put("courier",f.getCourier());
		quatatinsubobject.put("courierTotal",f.getCourierTotal());
		quatatinsubobject.put("qid",f.getQid());
		quatatinsubobject.put("salesPerson",f.getSalesPerson());
		quatatinsubobject.put("emailAddr",f.getEmailAddr());
		quatatinsubobject.put("assigned",f.getAssigned());
		quatatinsubobject.put("caseRef", f.getCaseRef());
		quatatinsubobject.put("remarks",f.getRemarks());
		quatatinsubobject.put("modelTerm",f.getModelTerm());
		quatatinsubobject.put("attention",f.getAttention());
		quatatinsubobject.put("subTotal",f.getSubTotal());
		quatatinsubobject.put("grandTotal",f.getGrandTotal());
		quatatinsubobject.put("fax",f.getFax());
		quatatinsubobject.put("addedTax",f.getAddedTax());
		quatatinsubobject.put("custCode",f.getCustCode());
		quatatinsubobject.put("paymentTerm",f.getPaymentTerm());
		quatatinsubobject.put("qtnProgress",f.getQtnProgress());
		quatatinsubobject.put("profit",f.getProfit());
		
		quatatinsubobject.put("salesmanPhNo",f.getSalesmanPhNo());
		quatatinsubobject.put("createdBy",f.getCreatedBy());
		
		quatatinsubobject.put("estPercent",f.getEstPercent());
		quatatinsubobject.put("caseRef",f.getCaseRef());
		

	





		
		
		
		
		quatatinformarray.add(quatatinsubobject);
		   		
		 
		  
		
		
		 
		
	}
	
	quatation.put("quatationform",quatatinformarray);
	
	
	JSONArray quatationgridarray=new JSONArray();
	
 
	
	String hql2="from QuotationGrid where qid="+qid+"";
	Query query2 = session.createQuery(hql2);
	List<QuotationGrid> quotationGridresults = query2.list();
	if(qtntypeforprintout==0 || qtntypeforprintout==1)
	{
	int i=1;
	ArrayList<Double> al=new ArrayList<Double>();
	Double totalsum=0.0;
	for(QuotationGrid f:quotationGridresults)
	{
		JSONObject quatatinsubobjectgrid=new JSONObject();
		quatatinsubobjectgrid.put("id",i);
		quatatinsubobjectgrid.put("model",f.getModel());
		quatatinsubobjectgrid.put("productDesp",f.getPartDesp());
		quatatinsubobjectgrid.put("motorDetails",f.getMotorPower()+","+f.getPole()+","+f.getPhase()+","+f.getVoltage()+","+f.getFreq()+","+f.getOutPutRPM());
		quatatinsubobjectgrid.put("inputRPM",f.getInputRPM());
		quatatinsubobjectgrid.put("ratio", f.getRatio());
		quatatinsubobjectgrid.put("outPutRPM", f.getOutPutRPM());
		quatatinsubobjectgrid.put("accessories",f.getAccessories());
		quatatinsubobjectgrid.put("qnty",f.getQnty());
		quatatinsubobjectgrid.put("equipNo",f.getEquipNo());
		quatatinsubobjectgrid.put("unitPrice",f.getUnitPrice());
		quatatinsubobjectgrid.put("subTotal",f.getSubTotal());
		quatatinsubobjectgrid.put("partNumber",f.getPartNumber());
		quatatinsubobjectgrid.put("partDesp",f.getPartDesp());
		quatatinsubobjectgrid.put("serialNumber",f.getSerialNumber());
		quatatinsubobjectgrid.put("sfactor",f.getSfactor());
		quatatinsubobjectgrid.put("costPrice",f.getCostPrice());
		quatatinsubobjectgrid.put("motorPower",f.getMotorPower());
		 
		
		
		
		
		
		//quatatinsubobjectgrid.put("motorDetails",f.getMotorDetails());
		quatatinsubobjectgrid.put("specialFeaturs",f.getSpecialFeaturs());
		quatatinsubobjectgrid.put("perSN",f.getPerSN());
		quatatinsubobjectgrid.put("quantity",f.getQuantity());
		quatatinsubobjectgrid.put("totalPrice",f.getTotalPrice());
		quatatinsubobjectgrid.put("region",f.getRegion());
		
		
		quatatinsubobjectgrid.put("pole",f.getPole());
		quatatinsubobjectgrid.put("phase",f.getPhase());
		quatatinsubobjectgrid.put("voltage",f.getVoltage());
		quatatinsubobjectgrid.put("freq",f.getFreq());
		quatatinsubobjectgrid.put("appcode",f.getAppcode());
		
		 
		
		 
		
		
		totalsum=totalsum+Double.parseDouble(f.getTotalPrice());
		 
		quatationgridarray.add(quatatinsubobjectgrid);
		
		i++;
		
	}
	 
	quatation.put("quatationgrid",quatationgridarray);
	quatation.put("totalsum",totalsum );
	
	}
	
	
	if(qtntypeforprintout==3)
		{
			String modelname = null;
			String snum = null;

			int i = 1;
			ArrayList<String> motor = new ArrayList<String>();
			ArrayList<String> serialnum = new ArrayList<String>();

			for (QuotationGrid f : quotationGridresults)
			{

				if (i == 1)
				{
					modelname = f.getModel();
					snum = f.getSerialNumber();

					motor.add(f.getModel());
					serialnum.add(f.getSerialNumber());

					i++;

				}

				else
				{
					modelname = f.getModel();
					snum = f.getSerialNumber();
					if (motor.contains(modelname) && serialnum.contains(snum))
					{
						i++;

					}

					else
					{

						motor.add(f.getModel());
						serialnum.add(f.getSerialNumber());

						i++;

					}

				}

			}
			
			
			int modelcount=motor.size();
			int pk=1;
			int jk=1;
			 
			
			for(int j=0;j<motor.size();j++)
			{
				
				JSONObject subquatation=new JSONObject();
				JSONArray subquatatinformarray=new JSONArray();
				String modelnumber1=(String)motor.get(j);
				String eqipmentnumber = null;
				String serialnumber=(String)serialnum.get(j);
				String hql3="from QuotationGrid where qid="+qid+"and model='"+modelnumber1+"' and serialNumber='"+serialnumber+"' and  productDesp is null ";
				
				Query query3 = session.createQuery(hql3);
				List<QuotationGrid> quotationGridresultsforspareparts = query3.list();
				
				String  productdesprction=null;
				
				for(QuotationGrid f:quotationGridresultsforspareparts)
				{
					eqipmentnumber=f.getEquipNo();
					JSONObject quatatinsubobjectgrid=new JSONObject();
					quatatinsubobjectgrid.put("id",jk);
					quatatinsubobjectgrid.put("model",f.getModel());
					//quatatinsubobjectgrid.put("productDesp",f.getPartDesp());
					productdesprction=f.getProductDesp();
					quatatinsubobjectgrid.put("motorDetails",f.getMotorPower()+","+f.getPole()+","+f.getPhase()+","+f.getVoltage()+","+f.getFreq()+","+f.getOutPutRPM());
					quatatinsubobjectgrid.put("inputRPM",f.getInputRPM());
					quatatinsubobjectgrid.put("ratio", f.getRatio());
					quatatinsubobjectgrid.put("outPutRPM", f.getOutPutRPM());
					quatatinsubobjectgrid.put("accessories",f.getAccessories());
					quatatinsubobjectgrid.put("qnty",f.getQnty());
					quatatinsubobjectgrid.put("deliveryTime ",f.getDeliveryTime());
					quatatinsubobjectgrid.put("unitPrice",f.getUnitPrice());
					quatatinsubobjectgrid.put("subTotal",f.getSubTotal());
					quatatinsubobjectgrid.put("partNumber",f.getPartNumber());
					quatatinsubobjectgrid.put("partDesp",f.getPartDesp());
					quatatinsubobjectgrid.put("serialNumber",f.getSerialNumber());
					quatatinsubobjectgrid.put("sfactor",f.getSfactor());
					quatatinsubobjectgrid.put("costPrice",f.getCostPrice());
					//quatatinsubobjectgrid.put("motorDetails",f.getMotorDetails());
					quatatinsubobjectgrid.put("specialFeaturs",f.getSpecialFeaturs());
					quatatinsubobjectgrid.put("perSN",f.getPerSN());
					quatatinsubobjectgrid.put("quantity",f.getQuantity());
					quatatinsubobjectgrid.put("totalPrice",f.getTotalPrice());
					quatatinsubobjectgrid.put("region",f.getRegion());
					//quatatinsubobjectgrid.put("equipNo",f.getEquipNo());
					
					
					
					quatatinsubobjectgrid.put("pole",f.getPole());
					quatatinsubobjectgrid.put("phase",f.getPhase());
					quatatinsubobjectgrid.put("voltage",f.getVoltage());
					quatatinsubobjectgrid.put("freq",f.getFreq());
					quatatinsubobjectgrid.put("appcode",f.getAppcode());
					
					 
					
					 
					
					
				 
					 
					subquatatinformarray.add(quatatinsubobjectgrid);
					
					jk++;
					
				}
				if(quotationGridresultsforspareparts .size() >0)
				{
				subquatation.put("id",pk);
				subquatation.put("motor",modelnumber1);
				subquatation.put("snum",serialnumber);
				subquatation.put("data",subquatatinformarray);
				subquatation.put("productDesp",productdesprction);
				subquatation.put("equipNo", eqipmentnumber);

				
				quatationgridarray.add(subquatation);
				 
				pk++;
				
				 				
				}
				
			}
			
			
			
			
			quatation.put("quatationgrid", quatationgridarray);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		int kl=jk;	
			
			
			
			
			 
			
			
			
			
			JSONArray subquatatinformarrayforproductdesp=new JSONArray();
			for(int j=0;j<motor.size();j++)
			{
				
				JSONObject subquatation=new JSONObject();
				//JSONArray subquatatinformarray=new JSONArray();
				 
				String modelnumber1=(String)motor.get(j);
				String serialnumber=(String)serialnum.get(j);
				String hql3="from QuotationGrid where qid="+qid+"and model='"+modelnumber1+"' and serialNumber='"+serialnumber+"' and  productDesp is not null ";
				
				Query query3 = session.createQuery(hql3);
				List<QuotationGrid> quotationGridresultsforspareparts = query3.list();
				
				String  productdesprction=null;
				//int bk=1;
				for(QuotationGrid f:quotationGridresultsforspareparts)
				{
					 
					JSONObject quatatinsubobjectgrid=new JSONObject();
					quatatinsubobjectgrid.put("id",kl++);
					quatatinsubobjectgrid.put("model",f.getModel());
					//quatatinsubobjectgrid.put("productDesp",f.getPartDesp());
					productdesprction=f.getProductDesp();
					quatatinsubobjectgrid.put("motorDetails",f.getMotorPower()+","+f.getPole()+","+f.getPhase()+","+f.getVoltage()+","+f.getFreq()+","+f.getOutPutRPM());
					quatatinsubobjectgrid.put("inputRPM",f.getInputRPM());
					quatatinsubobjectgrid.put("ratio", f.getRatio());
					quatatinsubobjectgrid.put("outPutRPM", f.getOutPutRPM());
					quatatinsubobjectgrid.put("accessories",f.getAccessories());
					quatatinsubobjectgrid.put("qnty",f.getQnty());
					quatatinsubobjectgrid.put("deliveryTime ",f.getDeliveryTime());
					quatatinsubobjectgrid.put("unitPrice",f.getUnitPrice());
					quatatinsubobjectgrid.put("subTotal",f.getSubTotal());
					quatatinsubobjectgrid.put("partNumber",f.getPartNumber());
					quatatinsubobjectgrid.put("partDesp",f.getPartDesp());
					quatatinsubobjectgrid.put("serialNumber",f.getSerialNumber());
					quatatinsubobjectgrid.put("sfactor",f.getSfactor());
					quatatinsubobjectgrid.put("costPrice",f.getCostPrice());
					//quatatinsubobjectgrid.put("motorDetails",f.getMotorDetails());
					quatatinsubobjectgrid.put("specialFeaturs",f.getSpecialFeaturs());
					quatatinsubobjectgrid.put("perSN",f.getPerSN());
					quatatinsubobjectgrid.put("quantity",f.getQuantity());
					quatatinsubobjectgrid.put("totalPrice",f.getTotalPrice());
					quatatinsubobjectgrid.put("region",f.getRegion());
					quatatinsubobjectgrid.put("equipNo",f.getEquipNo());
					
					
					quatatinsubobjectgrid.put("pole",f.getPole());
					quatatinsubobjectgrid.put("phase",f.getPhase());
					quatatinsubobjectgrid.put("voltage",f.getVoltage());
					quatatinsubobjectgrid.put("freq",f.getFreq());
					quatatinsubobjectgrid.put("appcode",f.getAppcode());
					
					 
					
					quatatinsubobjectgrid.put("productDesp",f.getProductDesp());
					
					
				 
					 
					subquatatinformarrayforproductdesp.add(quatatinsubobjectgrid);
					
					/*bk++;*/
					
					//kl++;
				}
				/*subquatation.put("id",pk);
				subquatation.put("motor",modelnumber1);
				subquatation.put("snum",serialnumber);
				subquatation.put("data",subquatatinformarrayforproductdesp);
				subquatation.put("productDesp",productdesprction);
*/
				
			/*	quatationgridarray.add(subquatation);
				pk++;
				*/
				
				 				
				 	
				
			}
			
			

			
			
			
			
			
			
			
			
			
			
			

			//quatation.put("quatationgrid", quatationgridarray);
			quatation.put("quatationgridprd",subquatatinformarrayforproductdesp);

		}
	
	
	
	
	
	
	
	if(qtntypeforprintout==2)
		{
			String modelname = null;
			String snum = null;

			int i = 1;
			ArrayList<String> motor = new ArrayList<String>();
			ArrayList<String> serialnum = new ArrayList<String>();

			for (QuotationGrid f : quotationGridresults)
			{

				if (i == 1)
				{
					modelname = f.getModel();
					snum = f.getSerialNumber();

					motor.add(f.getModel());
					serialnum.add(f.getSerialNumber());

					i++;

				}

				else
				{
					modelname = f.getModel();
					snum = f.getSerialNumber();
					if (motor.contains(modelname) && serialnum.contains(snum))
					{
						i++;

					}

					else
					{

						motor.add(f.getModel());
						serialnum.add(f.getSerialNumber());

						i++;

					}

				}

			}
			
			
			int modelcount=motor.size();
			int pk=1;
			int jk=1;
			
			for(int j=0;j<motor.size();j++)
			{
				
				JSONObject subquatation=new JSONObject();
				JSONArray subquatatinformarray=new JSONArray();
				String modelnumber1=(String)motor.get(j);
				String eqipno=null;
				String serialnumber=(String)serialnum.get(j);
				String hql3="from QuotationGrid where qid="+qid+"and model='"+modelnumber1+"' and serialNumber='"+serialnumber+"'";
				
				Query query3 = session.createQuery(hql3);
				List<QuotationGrid> quotationGridresultsforspareparts = query3.list();
				
				String  productdesprction=null;
				
				for(QuotationGrid f:quotationGridresultsforspareparts)
				{
					JSONObject quatatinsubobjectgrid=new JSONObject();
					quatatinsubobjectgrid.put("id",jk);
					quatatinsubobjectgrid.put("model",f.getModel());
					//quatatinsubobjectgrid.put("productDesp",f.getPartDesp());
					productdesprction=f.getProductDesp();
					quatatinsubobjectgrid.put("motorDetails",f.getMotorPower()+","+f.getPole()+","+f.getPhase()+","+f.getVoltage()+","+f.getFreq()+","+f.getOutPutRPM());
					quatatinsubobjectgrid.put("inputRPM",f.getInputRPM());
					quatatinsubobjectgrid.put("ratio", f.getRatio());
					quatatinsubobjectgrid.put("outPutRPM", f.getOutPutRPM());
					quatatinsubobjectgrid.put("accessories",f.getAccessories());
					quatatinsubobjectgrid.put("qnty",f.getQnty());
					quatatinsubobjectgrid.put("equipNo",f.getEquipNo());
					eqipno=f.getEquipNo();
					quatatinsubobjectgrid.put("deliveryTime ",f.getDeliveryTime());
					quatatinsubobjectgrid.put("unitPrice",f.getUnitPrice());
					quatatinsubobjectgrid.put("subTotal",f.getSubTotal());
					quatatinsubobjectgrid.put("partNumber",f.getPartNumber());
					quatatinsubobjectgrid.put("partDesp",f.getPartDesp());
					quatatinsubobjectgrid.put("serialNumber",f.getSerialNumber());
					quatatinsubobjectgrid.put("sfactor",f.getSfactor());
					quatatinsubobjectgrid.put("costPrice",f.getCostPrice());
					//quatatinsubobjectgrid.put("motorDetails",f.getMotorDetails());
					quatatinsubobjectgrid.put("specialFeaturs",f.getSpecialFeaturs());
					quatatinsubobjectgrid.put("perSN",f.getPerSN());
					quatatinsubobjectgrid.put("quantity",f.getQuantity());
					quatatinsubobjectgrid.put("totalPrice",f.getTotalPrice());
					quatatinsubobjectgrid.put("region",f.getRegion());
					
					
					quatatinsubobjectgrid.put("pole",f.getPole());
					quatatinsubobjectgrid.put("phase",f.getPhase());
					quatatinsubobjectgrid.put("voltage",f.getVoltage());
					quatatinsubobjectgrid.put("freq",f.getFreq());
					quatatinsubobjectgrid.put("appcode",f.getAppcode());
					
					 
					
					 
					
					
				 
					 
					subquatatinformarray.add(quatatinsubobjectgrid);
					
					jk++;
					
				}
				subquatation.put("id",pk);
				subquatation.put("motor",modelnumber1);
				subquatation.put("snum",serialnumber);
				subquatation.put("data",subquatatinformarray);
				subquatation.put("productDesp",productdesprction);
				subquatation.put("equipNo",eqipno);
				
				quatationgridarray.add(subquatation);
				pk++;
				
				
			}
			
			
			
			
			
			
			
			
			

			quatation.put("quatationgrid", quatationgridarray);

		}
	
	
	
	 		
		
		
		
		
		
		
 
		
		
		
		
		
		
	
	
	
	
	
	
	
	
	
         JSONArray quatationtermsgrid=new JSONArray();
	
	String hql3="from TermsAndConditionGrid where qid="+qid+"";
	Query query3 = session.createQuery(hql3);
	List<TermsAndConditionGrid> termsandconditionsgrid = query3.list();
	int t=1;
	 for(TermsAndConditionGrid tag:termsandconditionsgrid)
	 {
		 JSONObject qua=new JSONObject();
		 qua.put("id",t);
		 qua.put("modelTerm",tag.getModelTerm());
		 qua.put("modelCon",tag.getModelCon());
		 quatationtermsgrid.add(qua);
			t++;
			}
	 
	 quatation.put("quatationtermsgrid",quatationtermsgrid);
	 
	return quatation;

}




@RequestMapping(value = "/getSavedQuatationDataOnCustmerName",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getSavedQuatationDataOnCustmerName(HttpServletRequest req)
{
	

	HttpSession hs=req.getSession();
	//String caserefrence=(String)hs.getAttribute("caseref");
	String custmername=req.getParameter("custmernmae");
	int qid=0;
	
	
	Session session=hibernateDao.getSessionFactory().openSession();
	String hql = "from Quotation where  custName='"+custmername+"'";
	Query query = session.createQuery(hql);
	List<Quotation> results = query.list();
	JSONObject quatation=new JSONObject();
	JSONArray quatatinformarray=new JSONArray();
	int k=1;
	for(Quotation f:results)
	{
		
		 
		qid=f.getQid();
		JSONObject quatatinsubobject=new JSONObject();
		quatatinsubobject.put("qtnType",f.getQtnType());
		quatatinsubobject.put("qtnNo",f.getQtnNo());
		quatatinsubobject.put("custName",f.getCustName());
		quatatinsubobject.put("custCode",f.getCustCode());
		quatatinsubobject.put("paymentTerm",f.getPaymentTerm());
		quatatinsubobject.put("qtnProgress",f.getQtnProgress());
		quatatinsubobject.put("profit",f.getProfit());
		
		quatatinsubobject.put("salesmanPhNo",f.getSalesmanPhNo());
		quatatinsubobject.put("createdBy",f.getCreatedBy());
		 
		
		
		
		if (f.getDateQuote() != null) {
			//d1 = data.getDateExp();
			String k1 = DateUtils.formatDate(f.getDateQuote(),Constants.GenericDateFormat.DATE_FORMAT);
			quatatinsubobject.put("dateQuote", k1);
                                   }
		quatatinsubobject.put("currency",f.getCurrency());
		if (f.getDateValid() != null) {
			//d1 = data.getDateExp();
			String k1 = DateUtils.formatDate(f.getDateValid(),Constants.GenericDateFormat.DATE_FORMAT);
			quatatinsubobject.put("dateValid", k1);
}
		
		quatatinsubobject.put("tax",f.getTax());
		quatatinsubobject.put("copy",f.getCopy());
		quatatinsubobject.put("country",f.getCountry());
		quatatinsubobject.put("salesMan",f.getSalesMan());
		quatatinsubobject.put("taxPercent",f.getTaxPercent());
		quatatinsubobject.put("telephone",f.getTelephone());
		quatatinsubobject.put("custRef",f.getCustRef());
		quatatinsubobject.put("salesManCode",f.getSalesManCode());
		quatatinsubobject.put("taxPercent",f.getTaxPercent());
		quatatinsubobject.put("telephone",f.getTelephone());
		quatatinsubobject.put("custRef",f.getCustRef());
        quatatinsubobject.put("discount",f.getDiscount());
		quatatinsubobject.put("email",f.getEmail());
		quatatinsubobject.put("payment",f.getPayment());
		quatatinsubobject.put("salesTerm",f.getSalesTerm());
		quatatinsubobject.put("extPercent",f.getExtPercent());
		quatatinsubobject.put("item",f.getItem());
		quatatinsubobject.put("showCost",f.getShowCost());
		quatatinsubobject.put("subject",f.getSubject());
		quatatinsubobject.put("application",f.getApplication());
		quatatinsubobject.put("industry",f.getIndustry());
		quatatinsubobject.put("freight", f.getFreight());
		quatatinsubobject.put("freightTotal",f.getFreightTotal());
		quatatinsubobject.put("courier",f.getCourier());
		quatatinsubobject.put("courierTotal",f.getCourierTotal());
		quatatinsubobject.put("qid",f.getQid());
		quatatinsubobject.put("salesPerson",f.getSalesPerson());
		quatatinsubobject.put("emailAddr",f.getEmailAddr());
		quatatinsubobject.put("assigned",f.getAssigned());
		quatatinsubobject.put("caseRef", f.getCaseRef());
		quatatinsubobject.put("remarks",f.getRemarks());
		quatatinsubobject.put("modelTerm",f.getModelTerm());
		quatatinsubobject.put("attention",f.getAttention());
		quatatinsubobject.put("subTotal",f.getSubTotal());
		quatatinsubobject.put("grandTotal",f.getGrandTotal());
		quatatinsubobject.put("fax",f.getFax());
		quatatinsubobject.put("addedTax",f.getAddedTax());
		if(f.getFlag()==1)
		{
			quatatinsubobject.put("action",1);
			
		}
		
		else
		{
			quatatinsubobject.put("action",0);
			
		}
		
		
		
		
		quatatinformarray.add(quatatinsubobject);
		   		
		 
		  k++;
		
		
		 
		
	}
	
	quatation.put("quatationform",quatatinformarray);
	
	
	JSONArray quatationgridarray=new JSONArray();
	
	String hql2="from QuotationGrid where qid="+qid+"";
	Query query2 = session.createQuery(hql2);
	List<QuotationGrid> quotationGridresults = query2.list();
	int i=1;
	ArrayList<Double> al=new ArrayList<Double>();
	Double totalsum=0.0;
	for(QuotationGrid f:quotationGridresults)
	{
		JSONObject quatatinsubobjectgrid=new JSONObject();
		quatatinsubobjectgrid.put("id",i);
		quatatinsubobjectgrid.put("model",f.getModel());
		quatatinsubobjectgrid.put("productDesp",f.getProductDesp());
		quatatinsubobjectgrid.put("motorPower",f.getMotorPower());
		quatatinsubobjectgrid.put("inputRPM",f.getInputRPM());
		quatatinsubobjectgrid.put("ratio", f.getRatio());
		quatatinsubobjectgrid.put("outPutRPM", f.getOutPutRPM());
		quatatinsubobjectgrid.put("accessories",f.getAccessories());
		quatatinsubobjectgrid.put("qnty",f.getQnty());
		quatatinsubobjectgrid.put("deliveryTime ",f.getDeliveryTime());
		quatatinsubobjectgrid.put("unitPrice",f.getUnitPrice());
		quatatinsubobjectgrid.put("subTotal",f.getSubTotal());
		quatatinsubobjectgrid.put("partNumber",f.getPartNumber());
		quatatinsubobjectgrid.put("partDesp",f.getPartDesp());
		quatatinsubobjectgrid.put("serialNumber",f.getSerialNumber());
		quatatinsubobjectgrid.put("sfactor",f.getSfactor());
		quatatinsubobjectgrid.put("costPrice",f.getCostPrice());
		quatatinsubobjectgrid.put("motorDetails",f.getMotorDetails());
		quatatinsubobjectgrid.put("specialFeaturs",f.getSpecialFeaturs());
		quatatinsubobjectgrid.put("perSN",f.getPerSN());
		quatatinsubobjectgrid.put("quantity",f.getQuantity());
		quatatinsubobjectgrid.put("totalPrice",f.getTotalPrice());
		quatatinsubobjectgrid.put("pole",f.getPole());
		quatatinsubobjectgrid.put("phase",f.getPhase());
		quatatinsubobjectgrid.put("voltage",f.getVoltage());
		quatatinsubobjectgrid.put("freq",f.getFreq());
		quatatinsubobjectgrid.put("appcode",f.getAppcode());
		
		 
		
		
	//	totalsum=totalsum+Double.parseDouble(f.getTotalPrice());
		 
		quatationgridarray.add(quatatinsubobjectgrid);
		
		i++;
		
	}
	 
	quatation.put("quatationgrid",quatationgridarray);
	quatation.put("totalsum",totalsum );
	
	
	
         JSONArray quatationtermsgrid=new JSONArray();
	
	String hql3="from TermsAndConditionGrid where qid="+qid+"";
	Query query3 = session.createQuery(hql3);
	List<TermsAndConditionGrid> termsandconditionsgrid = query3.list();
	int t=1;
	 for(TermsAndConditionGrid tag:termsandconditionsgrid)
	 {
		 JSONObject qua=new JSONObject();
		 qua.put("id",t);
		 qua.put("modelTerm",tag.getModelTerm());
		 qua.put("modelCon",tag.getModelCon());
		 quatationtermsgrid.add(qua);
			t++;
			}
	 
	 quatation.put("quatationtermsgrid",quatationtermsgrid);
	 
	return quatation;

}




@RequestMapping(value = "/getSavedQuatationDataOnQuatationNumber",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getSavedQuatationDataOnQuatationNumber(HttpServletRequest req)
{
	

	HttpSession hs=req.getSession();
	String caserefrence=(String)hs.getAttribute("caseref");
	int qid=0;
	String quatationnumber=req.getParameter("quationnumber");
	int action=Integer.parseInt(req.getParameter("action"));
	
	Session session=hibernateDao.getSessionFactory().openSession();
	String hql = "from Quotation where qtnNo='"+quatationnumber+"'";
	Query query = session.createQuery(hql);
	List<Quotation> results = query.list();
	JSONObject quatation=new JSONObject();
	JSONArray quatatinformarray=new JSONArray();

	for(Quotation f:results)
	{
		
		 
		qid=f.getQid();
		JSONObject quatatinsubobject=new JSONObject();
		quatatinsubobject.put("qtnType",f.getQtnType());
		if(action==1)
		{
		String qtno=f.getQtnNo();
		String qtnochasr= qtno.substring(qtno.lastIndexOf('-') + 1);
		int i1 = this.vesrionnumber(qtnochasr);
		String newqtnoversion=this.vesrion(i1);
		String oldqtnoversion=this.vesrion(i1-1);
		StringBuffer buffer = new StringBuffer(qtno);
		qtno = buffer.reverse().toString().replaceFirst(oldqtnoversion,newqtnoversion);
		qtno= new StringBuffer(qtno).reverse().toString();
		quatatinsubobject.put("qtnNo",qtno);
		
		}
		
		else
		{
			quatatinsubobject.put("qtnNo",f.getQtnNo());
			
			
		}
		
		
		quatatinsubobject.put("custName",f.getCustName());
		if (f.getDateQuote() != null) {
			//d1 = data.getDateExp();
			String k1 = DateUtils.formatDate(f.getDateQuote(),Constants.GenericDateFormat.DATE_FORMAT);
			quatatinsubobject.put("dateQuote", k1);
                                   }
		quatatinsubobject.put("currency",f.getCurrency());
		if (f.getDateValid() != null) {
			//d1 = data.getDateExp();
			String k1 = DateUtils.formatDate(f.getDateValid(),Constants.GenericDateFormat.DATE_FORMAT);
			quatatinsubobject.put("dateValid", k1);
}
		
		quatatinsubobject.put("tax",f.getTax());
		quatatinsubobject.put("copy",f.getCopy());
		quatatinsubobject.put("country",f.getCountry());
		quatatinsubobject.put("salesMan",f.getSalesMan());
		quatatinsubobject.put("taxPercent",f.getTaxPercent());
		quatatinsubobject.put("telephone",f.getTelephone());
		quatatinsubobject.put("custRef",f.getCustRef());
		quatatinsubobject.put("salesManCode",f.getSalesManCode());
		quatatinsubobject.put("taxPercent",f.getTaxPercent());
		quatatinsubobject.put("telephone",f.getTelephone());
		quatatinsubobject.put("custRef",f.getCustRef());
        quatatinsubobject.put("discount",f.getDiscount());
		quatatinsubobject.put("email",f.getEmail());
		quatatinsubobject.put("payment",f.getPayment());
		quatatinsubobject.put("salesTerm",f.getSalesTerm());
	//	quatatinsubobject.put("extPercent",f.getExtPercent());
		quatatinsubobject.put("item",f.getItem());
		quatatinsubobject.put("showCost",f.getShowCost());
		quatatinsubobject.put("subject",f.getSubject());
		quatatinsubobject.put("application",f.getApplication());
		quatatinsubobject.put("industry",f.getIndustry());
		quatatinsubobject.put("freight", f.getFreight());
		quatatinsubobject.put("freightTotal",f.getFreightTotal());
		quatatinsubobject.put("courier",f.getCourier());
		quatatinsubobject.put("courierTotal",f.getCourierTotal());
		quatatinsubobject.put("qid",f.getQid());
		quatatinsubobject.put("salesPerson",f.getSalesPerson());
		quatatinsubobject.put("emailAddr",f.getEmailAddr());
		quatatinsubobject.put("assigned",f.getAssigned());
		quatatinsubobject.put("caseRef", f.getCaseRef());
		quatatinsubobject.put("remarks",f.getRemarks());
		quatatinsubobject.put("modelTerm",f.getModelTerm());
		quatatinsubobject.put("attention",f.getAttention());
		quatatinsubobject.put("subTotal",f.getSubTotal());
		quatatinsubobject.put("grandTotal",f.getGrandTotal());
		quatatinsubobject.put("fax",f.getFax());
		quatatinsubobject.put("addedTax",f.getAddedTax());
		
		
		quatatinsubobject.put("custCode",f.getCustCode());
		quatatinsubobject.put("paymentTerm",f.getPaymentTerm());
		quatatinsubobject.put("qtnProgress",f.getQtnProgress());
		quatatinsubobject.put("profit",f.getProfit());
		
		quatatinsubobject.put("salesmanPhNo",f.getSalesmanPhNo());
		quatatinsubobject.put("createdBy",f.getCreatedBy());
		quatatinsubobject.put("estPercent",f.getEstPercent());
		quatatinsubobject.put("caseRef",f.getCaseRef());
		
		 
		 
		   
		
		
		
		quatatinformarray.add(quatatinsubobject);
		   		
		 
		  
		
		
		 
		
	}
	
	quatation.put("quatationform",quatatinformarray);
	
	
	JSONArray quatationgridarray=new JSONArray();
	
	String hql2="from QuotationGrid where qid="+qid+"";
	Query query2 = session.createQuery(hql2);
	List<QuotationGrid> quotationGridresults = query2.list();
	int i=1;
	ArrayList<Double> al=new ArrayList<Double>();
	Double totalsum=0.0;
	for(QuotationGrid f:quotationGridresults)
	{
		JSONObject quatatinsubobjectgrid=new JSONObject();
		quatatinsubobjectgrid.put("id",i);
		quatatinsubobjectgrid.put("model",f.getModel());
		quatatinsubobjectgrid.put("productDesp",f.getProductDesp());
		quatatinsubobjectgrid.put("motorPower",f.getMotorPower());
		quatatinsubobjectgrid.put("inputRPM",f.getInputRPM());
		quatatinsubobjectgrid.put("ratio", f.getRatio());
		quatatinsubobjectgrid.put("outPutRPM", f.getOutPutRPM());
		quatatinsubobjectgrid.put("accessories",f.getAccessories());
		quatatinsubobjectgrid.put("qnty",f.getQnty());
		quatatinsubobjectgrid.put("deliveryTime ",f.getDeliveryTime());
		quatatinsubobjectgrid.put("unitPrice",f.getUnitPrice());
		quatatinsubobjectgrid.put("subTotal",f.getSubTotal());
		quatatinsubobjectgrid.put("partNumber",f.getPartNumber());
		quatatinsubobjectgrid.put("partDesp",f.getPartDesp());
		quatatinsubobjectgrid.put("serialNumber",f.getSerialNumber());
		quatatinsubobjectgrid.put("sfactor",f.getSfactor());
		quatatinsubobjectgrid.put("costPrice",f.getCostPrice());
		quatatinsubobjectgrid.put("motorDetails",f.getMotorDetails());
		quatatinsubobjectgrid.put("specialFeaturs",f.getSpecialFeaturs());
		quatatinsubobjectgrid.put("perSN",f.getPerSN());
		quatatinsubobjectgrid.put("quantity",f.getQuantity());
		quatatinsubobjectgrid.put("totalPrice",f.getTotalPrice());
		quatatinsubobjectgrid.put("region",f.getRegion());
		
		
		quatatinsubobjectgrid.put("pole",f.getPole());
		quatatinsubobjectgrid.put("phase",f.getPhase());
		quatatinsubobjectgrid.put("voltage",f.getVoltage());
		quatatinsubobjectgrid.put("freq",f.getFreq());
		 
		quatatinsubobjectgrid.put("appcode",f.getAppcode());
		quatatinsubobjectgrid.put("equipNo",f.getEquipNo());
		
		//equipNo,productDesp,appcode 
		
		
		totalsum=totalsum+Double.parseDouble(f.getTotalPrice());
		 
		quatationgridarray.add(quatatinsubobjectgrid);
		
		i++;
		
	}
	 
	quatation.put("quatationgrid",quatationgridarray);
	quatation.put("totalsum",totalsum );
	
	
	
         JSONArray quatationtermsgrid=new JSONArray();
	
	String hql3="from TermsAndConditionGrid where qid="+qid+"";
	Query query3 = session.createQuery(hql3);
	List<TermsAndConditionGrid> termsandconditionsgrid = query3.list();
	int t=1;
	 for(TermsAndConditionGrid tag:termsandconditionsgrid)
	 {
		 JSONObject qua=new JSONObject();
		 qua.put("id",t);
		 qua.put("modelTerm",tag.getModelTerm());
		 qua.put("modelCon",tag.getModelCon());
		 quatationtermsgrid.add(qua);
			t++;
			}
	 
	 quatation.put("quatationtermsgrid",quatationtermsgrid);
	 
	return quatation;

}








@RequestMapping(value = "/getSavedQuatationPrintData",method = { RequestMethod.GET, RequestMethod.POST })
public  @ResponseBody  JSONObject getSavedQuatationDataOnQuatationNumber1(HttpServletRequest req)
{
	

	HttpSession hs=req.getSession();
	String caserefrence=(String)hs.getAttribute("caseref");
	int qid=0;
	String quatationnumber=req.getParameter("quationnumber");
	//int action=Integer.parseInt(req.getParameter("action"));
	
	Session session=hibernateDao.getSessionFactory().openSession();
	String hql = "from Quotation where qtnNo='"+quatationnumber+"'";
	Query query = session.createQuery(hql);
	List<Quotation> results = query.list();
	JSONObject quatation=new JSONObject();
	JSONArray quatatinformarray=new JSONArray();

	for(Quotation f:results)
	{
		
		 
		qid=f.getQid();
		JSONObject quatatinsubobject=new JSONObject();
		quatatinsubobject.put("qtnType",f.getQtnType());
		 
		
	 
			quatatinsubobject.put("qtnNo",f.getQtnNo());
			
		 
		
		
		quatatinsubobject.put("custName",f.getCustName());
		if (f.getDateQuote() != null) {
			//d1 = data.getDateExp();
			String k1 = DateUtils.formatDate(f.getDateQuote(),Constants.GenericDateFormat.DATE_FORMAT);
			quatatinsubobject.put("dateQuote", k1);
                                   }
		quatatinsubobject.put("currency",f.getCurrency());
		if (f.getDateValid() != null) {
			//d1 = data.getDateExp();
			String k1 = DateUtils.formatDate(f.getDateValid(),Constants.GenericDateFormat.DATE_FORMAT);
			quatatinsubobject.put("dateValid", k1);
}
		
		quatatinsubobject.put("tax",f.getTax());
		quatatinsubobject.put("copy",f.getCopy());
		quatatinsubobject.put("country",f.getCountry());
		quatatinsubobject.put("salesMan",f.getSalesMan());
		quatatinsubobject.put("taxPercent",f.getTaxPercent());
		quatatinsubobject.put("telephone",f.getTelephone());
		quatatinsubobject.put("custRef",f.getCustRef());
		quatatinsubobject.put("salesManCode",f.getSalesManCode());
		quatatinsubobject.put("taxPercent",f.getTaxPercent());
		quatatinsubobject.put("telephone",f.getTelephone());
		quatatinsubobject.put("custRef",f.getCustRef());
        quatatinsubobject.put("discount",f.getDiscount());
		quatatinsubobject.put("email",f.getEmail());
		quatatinsubobject.put("payment",f.getPayment());
		quatatinsubobject.put("salesTerm",f.getSalesTerm());
		quatatinsubobject.put("extPercent",f.getExtPercent());
		quatatinsubobject.put("item",f.getItem());
		quatatinsubobject.put("showCost",f.getShowCost());
		quatatinsubobject.put("subject",f.getSubject());
		quatatinsubobject.put("application",f.getApplication());
		quatatinsubobject.put("industry",f.getIndustry());
		quatatinsubobject.put("freight", f.getFreight());
		quatatinsubobject.put("freightTotal",f.getFreightTotal());
		quatatinsubobject.put("courier",f.getCourier());
		quatatinsubobject.put("courierTotal",f.getCourierTotal());
		quatatinsubobject.put("qid",f.getQid());
		quatatinsubobject.put("salesPerson",f.getSalesPerson());
		quatatinsubobject.put("emailAddr",f.getEmailAddr());
		quatatinsubobject.put("assigned",f.getAssigned());
		quatatinsubobject.put("caseRef", f.getCaseRef());
		quatatinsubobject.put("remarks",f.getRemarks());
		quatatinsubobject.put("modelTerm",f.getModelTerm());
		quatatinsubobject.put("attention",f.getAttention());
		quatatinsubobject.put("subTotal",f.getSubTotal());
		quatatinsubobject.put("grandTotal",f.getGrandTotal());
		quatatinsubobject.put("fax",f.getFax());
		quatatinsubobject.put("addedTax",f.getAddedTax());
		
		
		quatatinsubobject.put("custCode",f.getCustCode());
		quatatinsubobject.put("paymentTerm",f.getPaymentTerm());
		quatatinsubobject.put("qtnProgress",f.getQtnProgress());
		quatatinsubobject.put("profit",f.getProfit());
		
		quatatinsubobject.put("salesmanPhNo",f.getSalesmanPhNo());
		quatatinsubobject.put("createdBy",f.getCreatedBy());

		
		
		
		
		quatatinformarray.add(quatatinsubobject);
		   		
		 
		  
		
		
		 
		
	}
	
	quatation.put("quatationform",quatatinformarray);
	
	
	JSONArray quatationgridarray=new JSONArray();
	
	String hql2="from QuotationGrid where qid="+qid+"";
	Query query2 = session.createQuery(hql2);
	List<QuotationGrid> quotationGridresults = query2.list();
	int i=1;
	ArrayList<Double> al=new ArrayList<Double>();
	Double totalsum=0.0;
	for(QuotationGrid f:quotationGridresults)
	{
		JSONObject quatatinsubobjectgrid=new JSONObject();
		quatatinsubobjectgrid.put("id",i);
		quatatinsubobjectgrid.put("model",f.getModel());
		quatatinsubobjectgrid.put("productDesp",f.getPartDesp());
		quatatinsubobjectgrid.put("motorPower",f.getMotorPower());
		quatatinsubobjectgrid.put("inputRPM",f.getInputRPM());
		quatatinsubobjectgrid.put("ratio", f.getRatio());
		quatatinsubobjectgrid.put("outPutRPM", f.getOutPutRPM());
		quatatinsubobjectgrid.put("accessories",f.getAccessories());
		quatatinsubobjectgrid.put("qnty",f.getQnty());
		quatatinsubobjectgrid.put("deliveryTime ",f.getDeliveryTime());
		quatatinsubobjectgrid.put("unitPrice",f.getUnitPrice());
		quatatinsubobjectgrid.put("subTotal",f.getSubTotal());
		quatatinsubobjectgrid.put("partNumber",f.getPartNumber());
		quatatinsubobjectgrid.put("partDesp",f.getPartDesp());
		quatatinsubobjectgrid.put("serialNumber",f.getSerialNumber());
		quatatinsubobjectgrid.put("sfactor",f.getSfactor());
		quatatinsubobjectgrid.put("costPrice",f.getCostPrice());
		quatatinsubobjectgrid.put("motorDetails",f.getMotorDetails());
		quatatinsubobjectgrid.put("specialFeaturs",f.getSpecialFeaturs());
		quatatinsubobjectgrid.put("perSN",f.getPerSN());
		quatatinsubobjectgrid.put("quantity",f.getQuantity());
		quatatinsubobjectgrid.put("totalPrice",f.getTotalPrice());
		quatatinsubobjectgrid.put("region",f.getRegion());
		
		
		quatatinsubobjectgrid.put("pole",f.getPole());
		quatatinsubobjectgrid.put("phase",f.getPhase());
		quatatinsubobjectgrid.put("voltage",f.getVoltage());
		quatatinsubobjectgrid.put("freq",f.getFreq());
		 
		quatatinsubobjectgrid.put("appcode",f.getAppcode());
		
		
		totalsum=totalsum+Double.parseDouble(f.getTotalPrice());
		 
		quatationgridarray.add(quatatinsubobjectgrid);
		
		i++;
		
	}
	 
	quatation.put("quatationgrid",quatationgridarray);
	quatation.put("totalsum",totalsum );
	
	
	
         JSONArray quatationtermsgrid=new JSONArray();
	
	String hql3="from TermsAndConditionGrid where qid="+qid+"";
	Query query3 = session.createQuery(hql3);
	List<TermsAndConditionGrid> termsandconditionsgrid = query3.list();
	int t=1;
	 for(TermsAndConditionGrid tag:termsandconditionsgrid)
	 {
		 JSONObject qua=new JSONObject();
		 qua.put("id",t);
		 qua.put("modelTerm",tag.getModelTerm());
		 qua.put("modelCon",tag.getModelCon());
		 quatationtermsgrid.add(qua);
			t++;
			}
	 
	 quatation.put("quatationtermsgrid",quatationtermsgrid);
	 
	return quatation;

}




 	 
private String vesrion(int series)
{
	HashMap<Integer,String> a=new HashMap<Integer,String>();
	a.put(1,"A");
	a.put(2,"B");
	a.put(3,"C");
	a.put(4,"D");
	a.put(5,"E");
	a.put(6,"F");
	a.put(7,"G");
	a.put(8,"H");
	a.put(9,"I");
	a.put(10,"J");
	a.put(11,"K");
	a.put(12,"L");
	a.put(13,"M");
	a.put(14,"N");
	a.put(15,"O");
	a.put(16,"P");
	a.put(17,"Q");
	a.put(18,"R");
	a.put(19,"S");
	a.put(20,"T");
	a.put(21,"U");
	a.put(22,"V");
	a.put(23,"W");
	a.put(24,"X");
	a.put(25,"y");
	a.put(26,"Z");
	String version=(String)a.get(series+1);
	return version;
}
		
private int vesrionnumber(String  alphabet)
{
	HashMap<String,Integer> a=new HashMap<String,Integer>();
	a.put("A",1);
	a.put("B",2);
	a.put("C",3);
	a.put("D",4);
	a.put("E",5);
	a.put("F",6);
	a.put("G",7);
	a.put("H",8);
	a.put("I",9);
	a.put("J",10);
	a.put("K",11);
	a.put("L",12);
	a.put("M",13);
	a.put("N",14);
	a.put("O",15);
	a.put("P",16);
	a.put("Q",17);
	a.put("R",18);
	a.put("S",19);
	a.put("T",20);
	a.put("U",21);
	a.put("V",22);
	a.put("W",23);
	a.put("X",24);
	a.put("y",25);
	a.put("Z",26);
	int version=(Integer)a.get(alphabet);
	return version;
}
		
 

 

@RequestMapping(value = "/getToDaysDateForQuatation", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getToDaysDate(HttpServletRequest req,HttpServletResponse res) throws Exception

{
	JSONObject  todaysdateobject =  new JSONObject();
	
	Calendar now = Calendar.getInstance();
	int year = now.get(Calendar.YEAR);
	int month = now.get(Calendar.MONTH) + 2; // Note: zero based!
	int day = now.get(Calendar.DAY_OF_MONTH);
	
	
	
	Calendar c=new GregorianCalendar();
	c.add(Calendar.DATE, 30);
	Date d=c.getTime();
	System.out.println(d.getDate());
	
	todaysdateobject.put("bddate",d.getDate()+"-"+month+"-"+year);
	
	return  todaysdateobject;
}



@RequestMapping(value = "/getVoltagesforquatation", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getVoltagesforquatation(HttpServletRequest req,HttpServletResponse res) throws Exception

{
		 

	List<String> c = masterservice.getVolatagesforCase();
	JSONObject mainobject=new JSONObject();
	JSONArray mainarray = new JSONArray();
	 

	for (String a : c) {
		JSONObject submainobject = new JSONObject();
		submainobject.put("id", a);
		submainobject.put("voltage", a);

		mainarray.add(submainobject);

	}

	mainobject.put("data", mainarray);
	return mainobject;
 
		 
}






}


 






