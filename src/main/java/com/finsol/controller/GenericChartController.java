package com.finsol.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.finsol.dao.HibernateDao;
import com.finsol.model.Country;
import com.finsol.model.Salesman;
import com.finsol.service.Bss_Migrate_ServiceImpl;
import com.finsol.service.GenericChartService;

/**
 * @author Rama Krishna
 * 
 */
@Controller
public class GenericChartController
{

	private static final Logger logger = Logger.getLogger(GenericChartController.class);

	@Autowired
	private GenericChartService genericChartService;

	@Autowired
	private Bss_Migrate_ServiceImpl migratesevice;
	
	@Autowired
	private HibernateDao hibernateDao;

	@RequestMapping(value = "/monthleyytdtoareaSCA", method = RequestMethod.GET)
	public @ResponseBody JSONObject monthleyytdtoareaSCA(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			String link = req.getParameter("link");
			Integer curYear = Integer.parseInt(req.getParameter("currentyear"));

			HashMap monthname = new HashMap();
			monthname.put(1, "Jan");
			monthname.put(2, "Feb");
			monthname.put(3, "Mar");
			monthname.put(4, "Apr");
			monthname.put(5, "May");
			monthname.put(6, "Jun");
			monthname.put(7, "Jul");
			monthname.put(8, "Aug");
			monthname.put(9, "Sep");
			monthname.put(10, "Oct");
			monthname.put(11, "Nov");
			monthname.put(12, "Dec");

			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			JSONObject months = null;
			JSONArray budgetandbookingarrayforallcountries = new JSONArray();

			//List<Integer> monthnumbers =genericChartService.getDistinctMonthNumbers(curYear);

			for (int i = 1; i <= 12; i++)
			{
				months = new JSONObject();
				months.put("label", (String) monthname.get(i));
				jsonArray.add(months);
			}

			JSONObject subobject = new JSONObject();
			subobject.put("category", jsonArray);
			mainjsonArray.add(subobject);

			mainobject.put("xaxis", mainjsonArray);

			//List<Object[]> coutries=genericChartService.getCountries();
			//ArrayList<String> countryarrayList=new ArrayList<String>();
			List<Object[]> budgetofallmonths = genericChartService.getBudgetofallmonths(curYear, "NA");

			Object[] budjetArray = budgetofallmonths.get(0);

			if (budgetofallmonths != null && !budgetofallmonths.isEmpty())
			{
				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
				for (int i = 0; i < 12; i++)
				{
					JSONObject aboutcountrybudgetvalue = new JSONObject();
					Double d = (Double) budjetArray[i];
					aboutcountrybudgetvalue.put("value", d);
					countryjsonArray.add(aboutcountrybudgetvalue);
				}

				aboutcountry.put("seriesname", "Budget");
				aboutcountry.put("color", "6495ED");
				aboutcountry.put("alpha", "60");
				aboutcountry.put("data", countryjsonArray);

				countryjsonbudgetArray.add(aboutcountry);

				aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
				budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
			}
			else
			{
				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();
				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
				for (int i = 0; i < 12; i++)
				{
					JSONObject aboutcountrybudgetvalue = new JSONObject();
					//Double d=(Double)budjetArray[i];
					aboutcountrybudgetvalue.put("value", 0);
					countryjsonArray.add(aboutcountrybudgetvalue);
				}

				aboutcountry.put("seriesname", "Budget");
				aboutcountry.put("color", "6495ED");
				aboutcountry.put("alpha", "60");
				aboutcountry.put("data", countryjsonArray);

				countryjsonbudgetArray.add(aboutcountry);

				aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
				budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);

			}
			JSONObject aboutallcountriesbooking = new JSONObject();
			JSONArray countryjsonbookingArray = new JSONArray();

			//for(Object[] countrydetails:coutries)
			//{
			JSONObject aboutcountry = new JSONObject();
			JSONArray countryjsonArray = new JSONArray();

			//String countryname=(String)countrydetails[0];
			//String color=(String)countrydetails[2];

			List<Object[]> boookingsofmonth = genericChartService.getBookingsOfMonthley("NA", curYear);

			HashMap monthnumberss1 = new HashMap();
			for (Object[] bookingvalues : boookingsofmonth)
			{
				monthnumberss1.put((Integer) bookingvalues[1], (Double) bookingvalues[0]);

			}
			/*for(Object[] bookingvalues:boookingsofmonth)
			{
				JSONObject aboutcountrybookingvalue=new JSONObject();
				Double d=migratesevice.getBookingvaluesforMonthleyYtd(i,countryname);
				Double d=(Double)bookingvalues[0];
				aboutcountrybookingvalue.put("value",d);
				if(link.equalsIgnoreCase("product"))					
					aboutcountrybookingvalue.put("link","#/app/countryBookingYTD");
				//aboutcountrybookingvalue.put("alpha","0");
				countryjsonArray.add(aboutcountrybookingvalue);
			}		*/

			/*
			for(Object[] bookingvalues:boookingsofmonth)
			{
				JSONObject aboutcountrybookingvalue=new JSONObject();
				
				//Double d=migratesevice.getBookingvaluesforMonthleyYtd(i,countryname);
				Double d=(Double)bookingvalues[0];
				aboutcountrybookingvalue.put("value",d);
				if(link.equalsIgnoreCase("product"))					
					aboutcountrybookingvalue.put("link","#/app/countryBookingYTD");
				//aboutcountrybookingvalue.put("alpha","0");
				countryjsonArray.add(aboutcountrybookingvalue);
			}	
			*/

			if (boookingsofmonth.size() != 0)
			{
				Set monthnumbers3 = monthnumberss1.keySet();
				for (int i = 1; i <= 12; i++)
				{
					JSONObject aboutcountrybookingvalue = new JSONObject();
					//Double d = migratesevice.getBookingvaluesforMonthleyYtd(i, countryname);
					if (monthnumbers3.contains(i))
					{
						// Double d=(Double)bookingvalues[0];
						aboutcountrybookingvalue.put("value", (Double) monthnumberss1.get(i));
						if (link.equalsIgnoreCase("product"))
							aboutcountrybookingvalue.put("link", "#/app/countryBookingYTD");
						countryjsonArray.add(aboutcountrybookingvalue);
					}
					else
					{
						aboutcountrybookingvalue.put("value", null);
						countryjsonArray.add(aboutcountrybookingvalue);
					}
				}
			}

			aboutcountry.put("seriesname", "Booking");
			aboutcountry.put("color", "6495ED");
			aboutcountry.put("data", countryjsonArray);

			countryjsonbookingArray.add(aboutcountry);
			//}
			aboutallcountriesbooking.put("dataset", countryjsonbookingArray);

			budgetandbookingarrayforallcountries.add(aboutallcountriesbooking);

			String monthString = null;
			JSONObject tableObject = new JSONObject();
			JSONArray tabletArray = new JSONArray();

			for (int i = curYear - 2; i <= curYear; i++)
			{
				tableObject = new JSONObject();
				tableObject.put("year", i);

				for (int j = 1; j <= 4; j++)
				{
					if (j == 1)
					{
						monthString = "1,2,3";
						Double value = genericChartService.getQuarterlyBooking(i, monthString, "NA");
						tableObject.put("Q1", value);
					}
					if (j == 2)
					{
						monthString = "4,5,6";
						Double value = genericChartService.getQuarterlyBooking(i, monthString, "NA");
						tableObject.put("Q2", value);
					}
					if (j == 3)
					{
						monthString = "7,8,9";
						Double value = genericChartService.getQuarterlyBooking(i, monthString, "NA");
						tableObject.put("Q3", value);
					}
					if (j == 4)
					{
						monthString = "10,11,12";
						Double value = genericChartService.getQuarterlyBooking(i, monthString, "NA");
						tableObject.put("Q4", value);
					}
				}
				tabletArray.add(tableObject);
			}

			mainobject.put("data", budgetandbookingarrayforallcountries);
			mainobject.put("tabledata", tabletArray);

			return mainobject;

		}
		catch (Exception e)
		{
			logger.info("=============CONTROLLER METHOD=forcastbacklogdrilldown=========" + e.getCause());
			return null;
		}
	}

	/*	@RequestMapping(value = "/monthleyytdtoareaCountryWise", method = RequestMethod.GET)		
		 public @ResponseBody JSONObject monthleyytdtoareaCountryWise(HttpServletRequest req,HttpServletResponse res)  
		 {
			String link=req.getParameter("link");
			Integer curYear=Integer.parseInt(req.getParameter("currentyear"));
			String currencey=req.getParameter("id");
			HttpSession session=req.getSession(false);
			//String countryCode=(String)session.getAttribute("countrycode");
			
			HashMap countrycurrenceyvalue=new HashMap();
			countrycurrenceyvalue.put("AU",1.34);
			countrycurrenceyvalue.put("Hk",6.68 );
			countrycurrenceyvalue.put("IN",67.20 );
			countrycurrenceyvalue.put("ID",13100.00 );
	   countrycurrenceyvalue.put("MY",4.07);
			countrycurrenceyvalue.put("OT",1.00);
			countrycurrenceyvalue.put("PH",47.18);
			countrycurrenceyvalue.put("SG",1.36);
			countrycurrenceyvalue.put("TH",35.01);
			countrycurrenceyvalue.put("VN",22301.52);
		
		
			
			
			String countryCode=req.getParameter("countryCode");
			
			HashMap monthname=new HashMap();		
			monthname.put(1,"Jan");
			monthname.put(2,"Feb");
			monthname.put(3,"Mar");
			monthname.put(4,"Apr");
			monthname.put(5,"May");
			monthname.put(6,"Jun");
			monthname.put(7,"Jul");
			monthname.put(8,"Aug");
			monthname.put(9,"Sep");
			monthname.put(10,"Oct");
			monthname.put(11,"Nov");
			monthname.put(12,"Dec");
			
			JSONObject mainobject = new JSONObject();
			
			
			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			JSONObject months=null;
			JSONArray budgetandbookingarrayforallcountries = new JSONArray();
			
			List<Integer> monthnumbers =genericChartService.getDistinctMonthNumbers(curYear);
			
			for(int i=1;i<=12;i++)
			{
				months=new JSONObject();
				months.put("label",(String)monthname.get(i));
				jsonArray.add(months);			
			}
			
			JSONObject subobject=new JSONObject();
			subobject.put("category",jsonArray);
			mainjsonArray.add(subobject);
			 
			mainobject.put("xaxis",mainjsonArray);
			
			List<Object[]> countries=genericChartService.getCountriesNameColor(countryCode);
			Object[] couArray=countries.get(0);
			
			String countryName=(String)couArray[0];
			String countryColor=(String)couArray[1];
			
			ArrayList<String> countryarrayList=new ArrayList<String>();
		    List <Object[]> budgetofallmonths=genericChartService.getBudgetofallmonths(curYear,countryName);
			HashMap budgetamountformonths=new HashMap(); 
			if(budgetofallmonths!=null && !budgetofallmonths.isEmpty())
			{
				for(Object [] budmonth:budgetofallmonths)
				{
					ArrayList amd=new ArrayList();
					amd.add((String)budmonth[0]);
					amd.add((Double)budmonth[1]);
					amd.add((Double)budmonth[2]);
					amd.add((Double)budmonth[3]);
					amd.add((Double)budmonth[4]);
					amd.add((Double)budmonth[5]);
					amd.add((Double)budmonth[6]);
					amd.add((Double)budmonth[7]);
					amd.add((Double)budmonth[8]);
					amd.add((Double)budmonth[9]);
					amd.add((Double)budmonth[10]);
					amd.add((Double)budmonth[11]);
					amd.add((Double)budmonth[12]);
					budgetamountformonths.put((String)budmonth[0],amd);	
				}
				
				JSONObject aboutallcountriesbudget=new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();
				
				JSONObject aboutcountry=new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
				
				String countryname=(String)couArray[0];
				String color=(String)couArray[1];
				
				ArrayList abb=(ArrayList)budgetamountformonths.get(countryname);
				for(int i=1;i<=12;i++)
				{
					JSONObject aboutcountrybudgetvalue=new JSONObject();
					Double d=(Double)abb.get(i);
					aboutcountrybudgetvalue.put("value",d*(Double)countrycurrenceyvalue.get(countryCode));
					countryjsonArray.add(aboutcountrybudgetvalue);
				}
				
				aboutcountry.put("seriesname","Budget "+countryname);
				aboutcountry.put("color",color);
				aboutcountry.put("data",countryjsonArray);
				
				countryjsonbudgetArray.add(aboutcountry);		
				
				aboutallcountriesbudget.put("dataset",countryjsonbudgetArray);
				budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
			}
			else
			{
				JSONObject aboutallcountriesbudget=new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();
				
				JSONObject aboutcountry=new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
				for(int i=1;i<=12;i++)
				{
					JSONObject aboutcountrybudgetvalue=new JSONObject();
					//Double d=(Double)abb.get(i);
					aboutcountrybudgetvalue.put("value",0);
					countryjsonArray.add(aboutcountrybudgetvalue);
				}
				
				aboutcountry.put("seriesname","Budget "+countryName);
				aboutcountry.put("color",countryColor);
				aboutcountry.put("data",countryjsonArray);
				
				countryjsonbudgetArray.add(aboutcountry);		
				
				aboutallcountriesbudget.put("dataset",countryjsonbudgetArray);
				budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);			
			}
			
			JSONObject aboutallcountriesbooking=new JSONObject();
			JSONArray countryjsonbookingArray = new JSONArray();
		
			JSONObject aboutcountry=new JSONObject();
			JSONArray countryjsonArray = new JSONArray();
			
			String countryname=(String)couArray[0];
			String color=(String)couArray[2];
			
			List<Object[]> boookingsofmonth=genericChartService.getBookingsOfMonthley(countryname,curYear);
			
			HashMap monthnumberss1=new HashMap();
			for(Object[] bookingvalues:boookingsofmonth)
			{
				monthnumberss1.put((Integer)bookingvalues[1],(Double)bookingvalues[0]);
				 
			}
			
			if(boookingsofmonth.size()!=0)
			{
				Set monthnumbers3=monthnumberss1.keySet();
				for(int i=1;i<=12;i++)
				{				
					JSONObject aboutcountrybookingvalue=new JSONObject();
					Double d=migratesevice.getBookingvaluesforMonthleyYtd(i,countryname);
					if(monthnumbers3.contains(i))
					{
						//Double d=(Double)bookingvalues[0];
						aboutcountrybookingvalue.put("value",(Double)monthnumberss1.get(i));
						if(link.equalsIgnoreCase("product"))					
							aboutcountrybookingvalue.put("link","#/app/countryBookingYTD");
						countryjsonArray.add(aboutcountrybookingvalue);
					}
					else
					{
						aboutcountrybookingvalue.put("value",null);
						countryjsonArray.add(aboutcountrybookingvalue);
					}
				}
			}
			
			aboutcountry.put("seriesname","Booking "+countryname);
			aboutcountry.put("color",color);
			aboutcountry.put("data",countryjsonArray);
			
			countryjsonbookingArray.add(aboutcountry);
			aboutallcountriesbooking.put("dataset",countryjsonbookingArray);
			
			budgetandbookingarrayforallcountries.add(aboutallcountriesbooking);
			 
			String monthString=null;
			JSONObject tableObject=new JSONObject();
			JSONArray tabletArray = new JSONArray();
			
			for(int i=curYear-2;i<=curYear;i++)
			{
				tableObject=new JSONObject();
				tableObject.put("year", i);
				
				for(int j=1;j<=4;j++)
				{				
					if(j==1)
					{
						monthString="1,2,3";
						Double value=genericChartService.getQuarterlyBooking(i,monthString,countryname);					
						tableObject.put("Q1", value);
					}
					if(j==2)
					{
						monthString="4,5,6";
						Double value=genericChartService.getQuarterlyBooking(i,monthString,countryname);
						tableObject.put("Q2", value);
					}
					if(j==3)
					{
						monthString="7,8,9";
						Double value=genericChartService.getQuarterlyBooking(i,monthString,countryname);
						tableObject.put("Q3", value);
					}
					if(j==4)
					{
						monthString="10,11,12";
						Double value=genericChartService.getQuarterlyBooking(i,monthString,countryname);
						tableObject.put("Q4", value);
					}
				}
				tabletArray.add(tableObject);			
			}
			
			mainobject.put("data",budgetandbookingarrayforallcountries);
			mainobject.put("tabledata",tabletArray);
			
			
			
			
			
			 
			return mainobject;
		}
		*/

	@RequestMapping(value = "/ytdCodeAreaCountryWise", method = RequestMethod.GET)
	public @ResponseBody JSONObject ytdCodeAreaCountryWise(HttpServletRequest req, HttpServletResponse res)
	{
		JSONObject mainobject = new JSONObject();
		JSONObject xaxismaniobject = new JSONObject();
		JSONObject dataobjec4444 = new JSONObject();
		JSONArray yearsarray = new JSONArray();
		JSONArray dataarray = new JSONArray();

		String countrycode = req.getParameter("countryCode");
		String currencyType = req.getParameter("id");

		int lastyear = 0;

		List<Integer> years = migratesevice.gettingBookingBudgetYears();

		if (!years.isEmpty())
		{
			lastyear = years.get(years.size() - 1);
		}

		int budgetyear = 0;
		for (Integer y : years)
		{
			if (lastyear == y)
			{
				JSONObject submainobject = new JSONObject();
				submainobject.put("label", "BUDGET OF " + y + "");
				yearsarray.add(submainobject);

			}

			JSONObject submainobject = new JSONObject();
			submainobject.put("label", "" + y + "");
			yearsarray.add(submainobject);

		}

		mainobject.put("category", yearsarray);
		//JSONArray dataarray11=new JSONArray();
		//dataarray11.add(xaxismaniobject);		
		//mainobject.put("dataxaxis",xaxismaniobject);		
		//now for  region names//
		ArrayList<String> al = new ArrayList<String>();
		ArrayList<Double> al2percentages = new ArrayList<Double>();
		Double budgetvalue = 0.0;
		String seriesname = null;
		List<String> region = migratesevice.getRegionNmaesFromytdbookingRegion(countrycode);
		int budgetcondition = 1;
		for (String s : region)
		{
			al.add(s);
		}
		al.add("BUDGET");
		if (region.size() != 0)
		{

			for (String regionnanme : al)
			{
				if (regionnanme.equalsIgnoreCase("BUDGET"))
				{
					seriesname = "BUDGET";
				}

				else
				{
					Integer regionid = Integer.parseInt(regionnanme);

					seriesname = migratesevice.getRegionNamefromRegioncode(countrycode, regionid);
				}

				JSONObject dataseriesobject = new JSONObject();
				JSONArray dataserialarray = new JSONArray();

				for (Integer year : years)
				{

					if (lastyear == year)
					{
						if (regionnanme.equalsIgnoreCase("BUDGET"))
						{
							List<Double> usd = migratesevice.getOfamountusdfromytdbookingregionforBudget(year, countrycode);

							//Double usdvalue=(Double)usd;
							JSONObject dataobject = new JSONObject();
							if (usd.size() > 0)
							{
								for (Double d : usd)
								{
									budgetvalue = d;
									if("local".equalsIgnoreCase(currencyType))
									{
										Country ctry = (Country) hibernateDao.getRowByColumn(Country.class, "countrycode",countrycode);
										List<Double> local =migratesevice.getExchangeRate(year,1,"USD",ctry.getCurrencycode());
										Double df=local.get(0);
										d=d/df;
										budgetvalue = d;
									}
									if (d > 0.00)
									{
										dataobject.put("value", d);
									}
									else
									{
										dataobject.put("value", null);
									}

								}
							}

							else
							{
								dataobject.put("value", null);

							}

							dataserialarray.add(dataobject);
						}
						else
						{
							JSONObject dataobject = new JSONObject();
							dataobject.put("value", null);
							dataserialarray.add(dataobject);
						}

					}

					//else
					//{

					List<Double> usd = migratesevice.getOfamountusdfromytdbookingregion(year, regionnanme, countrycode,
					        currencyType);
					for (Double d1 : usd)
					{
						JSONObject dataobject = new JSONObject();
						if (d1 > 0.00)
						{
							dataobject.put("value", d1);
						}
						else
						{
							dataobject.put("value", null);
						}
						dataobject.put("link", "#/app/SCA_groupByArea");

						dataserialarray.add(dataobject);

						if (year == lastyear)
						{

							al2percentages.add(d1);

						}

					}

				}
				dataseriesobject.put("seriesname", seriesname);
				dataseriesobject.put("data", dataserialarray);
				dataarray.add(dataseriesobject);
			}
		}

		ArrayList<Double> percentage = new ArrayList<Double>();
		for (Double bookingvalues : al2percentages)
		{
			if(budgetvalue>0.0)
			{
			Double percentagevalue = (bookingvalues * 100) / budgetvalue;
			percentage.add(percentagevalue);
			}

		}

		mainobject.put("dataset", dataarray);
		mainobject.put("per", percentage);
		System.out.println(al2percentages + "---------------------------------->" + budgetvalue);
		//mainobject.put("per",al2percentages);

		return mainobject;

		/*
		
			String link = req.getParameter("link");
			Integer curYear = Integer.parseInt(req.getParameter("currentyear"));
			String currencey = req.getParameter("id");
			HttpSession session = req.getSession(false);
			String countryCode = req.getParameter("countryCode");
		
			if (currencey.equalsIgnoreCase("local")) {
				// String countryCode=(String)session.getAttribute("countrycode");
		
				HashMap countrycurrenceyvalue = new HashMap();
				countrycurrenceyvalue.put("AU", 1.371);
				countrycurrenceyvalue.put("Hk", 6.68);
				countrycurrenceyvalue.put("IN", 66.4);
				countrycurrenceyvalue.put("ID", 13736.26);
				countrycurrenceyvalue.put("MY", 4.288);
				countrycurrenceyvalue.put("OT", 1.00);
				countrycurrenceyvalue.put("PH", 47.0);
				countrycurrenceyvalue.put("SG", 1.41);
				countrycurrenceyvalue.put("TH", 35.97);
				countrycurrenceyvalue.put("VN", 22522.52);
				countrycurrenceyvalue.put("INT",1.00);
				countrycurrenceyvalue.put("EMA",1.00);	
		
		
				HashMap monthname = new HashMap();
				monthname.put(1, "Jan");
				monthname.put(2, "Feb");
				monthname.put(3, "Mar");
				monthname.put(4, "Apr");
				monthname.put(5, "May");
				monthname.put(6, "Jun");
				monthname.put(7, "Jul");
				monthname.put(8, "Aug");
				monthname.put(9, "Sep");
				monthname.put(10, "Oct");
				monthname.put(11, "Nov");
				monthname.put(12, "Dec");
		
				JSONObject mainobject = new JSONObject();
		
				JSONArray jsonArray = new JSONArray();
				JSONArray mainjsonArray = new JSONArray();
				JSONObject months = null;
				JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		
				List<Integer> monthnumbers = genericChartService.getDistinctMonthNumbers(curYear);
		
				for (int i = 1; i <= 12; i++) {
					months = new JSONObject();
					months.put("label", (String) monthname.get(i));
					jsonArray.add(months);
				}
		
				JSONObject subobject = new JSONObject();
				subobject.put("category", jsonArray);
				mainjsonArray.add(subobject);
		
				mainobject.put("xaxis", mainjsonArray);
		
				List<Object[]> countries = genericChartService.getCountriesNameColor(countryCode);
				Object[] couArray = countries.get(0);
		
				String countryName = (String) couArray[0];
				String countryColor = (String) couArray[1];
		
				ArrayList<String> countryarrayList = new ArrayList<String>();
				List<Object[]> budgetofallmonths = genericChartService.getBudgetofallmonths(curYear, countryName);
				HashMap budgetamountformonths = new HashMap();
				if (budgetofallmonths != null && !budgetofallmonths.isEmpty()) {
					for (Object[] budmonth : budgetofallmonths) {
						ArrayList amd = new ArrayList();
						amd.add((String) budmonth[0]);
						amd.add((Double) budmonth[1]);
						amd.add((Double) budmonth[2]);
						amd.add((Double) budmonth[3]);
						amd.add((Double) budmonth[4]);
						amd.add((Double) budmonth[5]);
						amd.add((Double) budmonth[6]);
						amd.add((Double) budmonth[7]);
						amd.add((Double) budmonth[8]);
						amd.add((Double) budmonth[9]);
						amd.add((Double) budmonth[10]);
						amd.add((Double) budmonth[11]);
						amd.add((Double) budmonth[12]);
						budgetamountformonths.put((String) budmonth[0], amd);
					}
		
					JSONObject aboutallcountriesbudget = new JSONObject();
					JSONArray countryjsonbudgetArray = new JSONArray();
		
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();
		
					String countryname = (String) couArray[0];
					String color = (String) couArray[1];
		
					ArrayList abb = (ArrayList) budgetamountformonths.get(countryname);
					for (int i = 1; i <= 12; i++) {
						JSONObject aboutcountrybudgetvalue = new JSONObject();
						Double d = (Double) abb.get(i);
						aboutcountrybudgetvalue.put("value", d * (Double) countrycurrenceyvalue.get(countryCode));
						aboutcountrybudgetvalue.put("value", d );
						countryjsonArray.add(aboutcountrybudgetvalue);
					}
		
					aboutcountry.put("seriesname", "Budget " + countryname);
					aboutcountry.put("color", color);
					aboutcountry.put("data", countryjsonArray);
		
					countryjsonbudgetArray.add(aboutcountry);
		
					aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
					budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
				} else {
					JSONObject aboutallcountriesbudget = new JSONObject();
					JSONArray countryjsonbudgetArray = new JSONArray();
		
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();
					for (int i = 1; i <= 12; i++) {
						JSONObject aboutcountrybudgetvalue = new JSONObject();
						// Double d=(Double)abb.get(i);
						aboutcountrybudgetvalue.put("value", 0);
						countryjsonArray.add(aboutcountrybudgetvalue);
					}
		
					aboutcountry.put("seriesname", "Budget " + countryName);
					aboutcountry.put("color", countryColor);
					aboutcountry.put("data", countryjsonArray);
		
					countryjsonbudgetArray.add(aboutcountry);
		
					aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
					budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
				}
		
				JSONObject aboutallcountriesbooking = new JSONObject();
				JSONArray countryjsonbookingArray = new JSONArray();
		
				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
		
				String countryname = (String) couArray[0];
				String color = (String) couArray[2];
		
				List<Object[]> boookingsofmonth = genericChartService.getBookingsOfMonthley(countryname, curYear);
		
				HashMap monthnumberss1 = new HashMap();
				for (Object[] bookingvalues : boookingsofmonth) {
					monthnumberss1.put((Integer) bookingvalues[1], (Double) bookingvalues[0]);
		
				}
		
				if (boookingsofmonth.size() != 0) {
					Set monthnumbers3 = monthnumberss1.keySet();
					for (int i = 1; i <= 12; i++) {
						JSONObject aboutcountrybookingvalue = new JSONObject();
						//Double d = migratesevice.getBookingvaluesforMonthleyYtd(i, countryname);
						if (monthnumbers3.contains(i)) {
							// Double d=(Double)bookingvalues[0];
							aboutcountrybookingvalue.put("value", (Double) monthnumberss1.get(i));
							if (link.equalsIgnoreCase("product"))
								aboutcountrybookingvalue.put("link", "#/app/countryBookingYTD");
							countryjsonArray.add(aboutcountrybookingvalue);
						} else {
							aboutcountrybookingvalue.put("value", null);
							countryjsonArray.add(aboutcountrybookingvalue);
						}
					}
				}
		
				aboutcountry.put("seriesname", "Booking " + countryname);
				aboutcountry.put("color", color);
				aboutcountry.put("data", countryjsonArray);
		
				countryjsonbookingArray.add(aboutcountry);
				aboutallcountriesbooking.put("dataset", countryjsonbookingArray);
		
				budgetandbookingarrayforallcountries.add(aboutallcountriesbooking);
		
				String monthString = null;
				JSONObject tableObject = new JSONObject();
				JSONArray tabletArray = new JSONArray();
		
				for (int i = curYear - 2; i <= curYear; i++) {
					tableObject = new JSONObject();
					tableObject.put("year", i);
		
					for (int j = 1; j <= 4; j++) {
						if (j == 1) {
							monthString = "1,2,3";
							Double value = genericChartService.getQuarterlyBooking(i, monthString, countryname);
							tableObject.put("Q1", value);
						}
						if (j == 2) {
							monthString = "4,5,6";
							Double value = genericChartService.getQuarterlyBooking(i, monthString, countryname);
							tableObject.put("Q2", value);
						}
						if (j == 3) {
							monthString = "7,8,9";
							Double value = genericChartService.getQuarterlyBooking(i, monthString, countryname);
							tableObject.put("Q3", value);
						}
						if (j == 4) {
							monthString = "10,11,12";
							Double value = genericChartService.getQuarterlyBooking(i, monthString, countryname);
							tableObject.put("Q4", value);
						}
					}
					tabletArray.add(tableObject);
				}
		
				mainobject.put("data", budgetandbookingarrayforallcountries);
				mainobject.put("tabledata", tabletArray);
		
				return mainobject;
		
			}
		
			else {
				HashMap countrycurrenceyvalue = new HashMap();
				countrycurrenceyvalue.put("AU", 1.34);
				countrycurrenceyvalue.put("Hk", 6.68);
				countrycurrenceyvalue.put("IN", 67.20);
				countrycurrenceyvalue.put("ID", 13100.00);
				countrycurrenceyvalue.put("MY", 4.07);
				countrycurrenceyvalue.put("OT", 1.00);
				countrycurrenceyvalue.put("PH", 47.18);
				countrycurrenceyvalue.put("SG", 1.36);
				countrycurrenceyvalue.put("TH", 35.01);
				countrycurrenceyvalue.put("VN", 22301.52);
		
				HashMap monthname = new HashMap();
				monthname.put(1, "Jan");
				monthname.put(2, "Feb");
				monthname.put(3, "Mar");
				monthname.put(4, "Apr");
				monthname.put(5, "May");
				monthname.put(6, "Jun");
				monthname.put(7, "Jul");
				monthname.put(8, "Aug");
				monthname.put(9, "Sep");
				monthname.put(10, "Oct");
				monthname.put(11, "Nov");
				monthname.put(12, "Dec");
		
				JSONObject mainobject = new JSONObject();
		
				JSONArray jsonArray = new JSONArray();
				JSONArray mainjsonArray = new JSONArray();
				JSONObject months = null;
				JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		
				List<Integer> monthnumbers = genericChartService.getDistinctMonthNumbers(curYear);
		
				for (int i = 1; i <= 12; i++) {
					months = new JSONObject();
					months.put("label", (String) monthname.get(i));
					jsonArray.add(months);
				}
		
				JSONObject subobject = new JSONObject();
				subobject.put("category", jsonArray);
				mainjsonArray.add(subobject);
		
				mainobject.put("xaxis", mainjsonArray);
		
				List<Object[]> countries = genericChartService.getCountriesNameColor(countryCode);
				Object[] couArray = countries.get(0);
		
				String countryName = (String) couArray[0];
				String countryColor = (String) couArray[1];
		
				ArrayList<String> countryarrayList = new ArrayList<String>();
				List<Object[]> budgetofallmonths = genericChartService.getBudgetofallmonths(curYear, countryName);
				HashMap budgetamountformonths = new HashMap();
				if (budgetofallmonths != null && !budgetofallmonths.isEmpty()) {
					for (Object[] budmonth : budgetofallmonths) {
						ArrayList amd = new ArrayList();
						amd.add((String) budmonth[0]);
						amd.add((Double) budmonth[1]);
						amd.add((Double) budmonth[2]);
						amd.add((Double) budmonth[3]);
						amd.add((Double) budmonth[4]);
						amd.add((Double) budmonth[5]);
						amd.add((Double) budmonth[6]);
						amd.add((Double) budmonth[7]);
						amd.add((Double) budmonth[8]);
						amd.add((Double) budmonth[9]);
						amd.add((Double) budmonth[10]);
						amd.add((Double) budmonth[11]);
						amd.add((Double) budmonth[12]);
						budgetamountformonths.put((String) budmonth[0], amd);
					}
		
					JSONObject aboutallcountriesbudget = new JSONObject();
					JSONArray countryjsonbudgetArray = new JSONArray();
		
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();
		
					String countryname = (String) couArray[0];
					String color = (String) couArray[1];
		
					ArrayList abb = (ArrayList) budgetamountformonths.get(countryname);
					for (int i = 1; i <= 12; i++) {
						JSONObject aboutcountrybudgetvalue = new JSONObject();
						Double d = (Double) abb.get(i);
						aboutcountrybudgetvalue.put("value", d);
						countryjsonArray.add(aboutcountrybudgetvalue);
					}
		
					aboutcountry.put("seriesname", "Budget " + countryname);
					aboutcountry.put("color", color);
					aboutcountry.put("data", countryjsonArray);
		
					countryjsonbudgetArray.add(aboutcountry);
		
					aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
					budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
				} else {
					JSONObject aboutallcountriesbudget = new JSONObject();
					JSONArray countryjsonbudgetArray = new JSONArray();
		
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();
					for (int i = 1; i <= 12; i++) {
						JSONObject aboutcountrybudgetvalue = new JSONObject();
						// Double d=(Double)abb.get(i);
						aboutcountrybudgetvalue.put("value", 0);
						countryjsonArray.add(aboutcountrybudgetvalue);
					}
		
					aboutcountry.put("seriesname", "Budget " + countryName);
					aboutcountry.put("color", countryColor);
					aboutcountry.put("data", countryjsonArray);
		
					countryjsonbudgetArray.add(aboutcountry);
		
					aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
					budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
				}
		
				JSONObject aboutallcountriesbooking = new JSONObject();
				JSONArray countryjsonbookingArray = new JSONArray();
		
				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
		
				String countryname = (String) couArray[0];
				String color = (String) couArray[2];
		
				List<Object[]> boookingsofmonth = genericChartService.getBookingsOfMonthleyInUsd(countryname, curYear);
		
				HashMap monthnumberss1 = new HashMap();
				for (Object[] bookingvalues : boookingsofmonth) {
					monthnumberss1.put((Integer) bookingvalues[1], (Double) bookingvalues[0]);
		
				}
		
				if (boookingsofmonth.size() != 0) {
					Set monthnumbers3 = monthnumberss1.keySet();
					for (int i = 1; i <= 12; i++) {
						JSONObject aboutcountrybookingvalue = new JSONObject();
						
						 * Double d=migratesevice.getBookingvaluesforMonthleyYtd(i,
						 * countryname);
						 
						if (monthnumbers3.contains(i)) {
							// Double d=(Double)bookingvalues[0];
							aboutcountrybookingvalue.put("value", (Double) monthnumberss1.get(i));
							if (link.equalsIgnoreCase("product"))
								aboutcountrybookingvalue.put("link", "#/app/countryBookingYTD");
							countryjsonArray.add(aboutcountrybookingvalue);
						} else {
							aboutcountrybookingvalue.put("value", null);
							countryjsonArray.add(aboutcountrybookingvalue);
						}
					}
				}
		
				aboutcountry.put("seriesname", "Booking " + countryname);
				aboutcountry.put("color", color);
				aboutcountry.put("data", countryjsonArray);
		
				countryjsonbookingArray.add(aboutcountry);
				aboutallcountriesbooking.put("dataset", countryjsonbookingArray);
		
				budgetandbookingarrayforallcountries.add(aboutallcountriesbooking);
		
				String monthString = null;
				JSONObject tableObject = new JSONObject();
				JSONArray tabletArray = new JSONArray();
		
				for (int i = curYear - 2; i <= curYear; i++) {
					tableObject = new JSONObject();
					tableObject.put("year", i);
		
					for (int j = 1; j <= 4; j++) {
						if (j == 1) {
							monthString = "1,2,3";
							Double value = genericChartService.getQuarterlyBookingInUsd(i, monthString, countryname);
							tableObject.put("Q1", value);
						}
						if (j == 2) {
							monthString = "4,5,6";
							Double value = genericChartService.getQuarterlyBookingInUsd(i, monthString, countryname);
							tableObject.put("Q2", value);
						}
						if (j == 3) {
							monthString = "7,8,9";
							Double value = genericChartService.getQuarterlyBookingInUsd(i, monthString, countryname);
							tableObject.put("Q3", value);
						}
						if (j == 4) {
							monthString = "10,11,12";
							Double value = genericChartService.getQuarterlyBookingInUsd(i, monthString, countryname);
							tableObject.put("Q4", value);
						}
					}
					tabletArray.add(tableObject);
				}
		
				mainobject.put("data", budgetandbookingarrayforallcountries);
				mainobject.put("tabledata", tabletArray);
		
				return mainobject;
			}
		
		
		*/
	}

	@RequestMapping(value = "/ytdCodeAreaCountryWiseDrilldownLevelOne", method = RequestMethod.GET)
	public @ResponseBody JSONObject ytdCodeAreaCountryWiseDrilldownLevelOne(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			DecimalFormat df = new DecimalFormat("#.##");
			JSONObject subobject = new JSONObject();

			String regionCode = req.getParameter("regionCode");
			String countryCode = req.getParameter("countryCode");
			String regioncodeinytdbooking = migratesevice.getregionfromRegion(regionCode, countryCode);
			String year = req.getParameter("year");
			String currencyType = req.getParameter("id");

			List<String> salesmen = migratesevice.getSalesMenCountryWise(countryCode, regioncodeinytdbooking);

			for (String salesman : salesmen)
			{
				//subobject=new JSONObject();				
				Double bookingValue = migratesevice.getBookingValueForSalesManCountryWise(salesman, regioncodeinytdbooking,
				        countryCode, Integer.parseInt(year), currencyType);
				if(bookingValue!=0.0)
				{
					Salesman sman=(Salesman)hibernateDao.getRowByColumn(Salesman.class, "salesmancode", salesman);
					subobject.put("value", df.format(bookingValue));
					subobject.put("label", salesman+"-"+sman.getIni());
					//subobject.put("link","#/app/SCA_groupByArea");
					jsonArray.add(subobject);
				}
				//jsonArray.add(subobject1);
			}

			/*subobject2.put("seriesname","Region");
			subobject2.put("data",jsonArray);
			jsonArray2.add(subobject2);			
			subobject4.put("category", jsonArray1);
			jsonArray3.add(subobject4);				
			//subobject3.put("xaxis",jsonArray3);		
			mainobject.put("xaxis",jsonArray3);*/

			mainobject.put("data", jsonArray);
			
			ArrayList<Double>  percentages= new ArrayList<Double>();
			for(int percentage=0;percentage<=jsonArray.size();percentage++)
			{
				percentages.add(0.0);
			}
			
			mainobject.put("per",percentages);

			return mainobject;

		}
		catch (Exception e)
		{
			logger.info("===========CONTROLLER METHOD=ytdcodeDrilldownLevelOne========" + e.getCause());;
			return null;
		}
	}

	@RequestMapping(value = "/monthleyytdtoareaCountryWise", method = RequestMethod.GET)
	public @ResponseBody JSONObject monthleyytdtoareaCountryWise(HttpServletRequest req, HttpServletResponse res)
	{

		String link = req.getParameter("link");
		Integer curYear = Integer.parseInt(req.getParameter("currentyear"));
		String currencey = req.getParameter("id");
		HttpSession session = req.getSession(false);
		String countryCode = req.getParameter("countryCode");

		if (currencey.equalsIgnoreCase("local"))
		{
			// String countryCode=(String)session.getAttribute("countrycode");

			HashMap countrycurrenceyvalue = new HashMap();
			countrycurrenceyvalue.put("AU", 1.371);
			countrycurrenceyvalue.put("Hk", 6.68);
			countrycurrenceyvalue.put("IN", 66.4);
			countrycurrenceyvalue.put("ID", 13736.26);
			countrycurrenceyvalue.put("MY", 4.288);
			countrycurrenceyvalue.put("OT", 1.00);
			countrycurrenceyvalue.put("PH", 47.0);
			countrycurrenceyvalue.put("SG", 1.41);
			countrycurrenceyvalue.put("TH", 35.97);
			countrycurrenceyvalue.put("VN", 22522.52);
			countrycurrenceyvalue.put("INT", 1.00);
			countrycurrenceyvalue.put("EMA", 1.00);

			HashMap monthname = new HashMap();
			monthname.put(1, "Jan");
			monthname.put(2, "Feb");
			monthname.put(3, "Mar");
			monthname.put(4, "Apr");
			monthname.put(5, "May");
			monthname.put(6, "Jun");
			monthname.put(7, "Jul");
			monthname.put(8, "Aug");
			monthname.put(9, "Sep");
			monthname.put(10, "Oct");
			monthname.put(11, "Nov");
			monthname.put(12, "Dec");

			JSONObject mainobject = new JSONObject();

			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			JSONObject months = null;
			JSONArray budgetandbookingarrayforallcountries = new JSONArray();

			List<Integer> monthnumbers = genericChartService.getDistinctMonthNumbers(curYear);

			for (int i = 1; i <= 12; i++)
			{
				months = new JSONObject();
				months.put("label", (String) monthname.get(i));
				jsonArray.add(months);
			}

			JSONObject subobject = new JSONObject();
			subobject.put("category", jsonArray);
			mainjsonArray.add(subobject);

			mainobject.put("xaxis", mainjsonArray);

			List<Object[]> countries = genericChartService.getCountriesNameColor(countryCode);
			Object[] couArray = countries.get(0);

			String countryName = (String) couArray[0];
			String countryColor = (String) couArray[1];

			ArrayList<String> countryarrayList = new ArrayList<String>();
			List<Object[]> budgetofallmonths = genericChartService.getBudgetofallmonths(curYear, countryName);
			HashMap budgetamountformonths = new HashMap();
			if (budgetofallmonths != null && !budgetofallmonths.isEmpty())
			{
				for (Object[] budmonth : budgetofallmonths)
				{
					ArrayList amd = new ArrayList();
					amd.add((String) budmonth[0]);
					amd.add((Double) budmonth[1]);
					amd.add((Double) budmonth[2]);
					amd.add((Double) budmonth[3]);
					amd.add((Double) budmonth[4]);
					amd.add((Double) budmonth[5]);
					amd.add((Double) budmonth[6]);
					amd.add((Double) budmonth[7]);
					amd.add((Double) budmonth[8]);
					amd.add((Double) budmonth[9]);
					amd.add((Double) budmonth[10]);
					amd.add((Double) budmonth[11]);
					amd.add((Double) budmonth[12]);
					budgetamountformonths.put((String) budmonth[0], amd);
				}

				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();

				String countryname = (String) couArray[0];
				String color = (String) couArray[1];

				ArrayList abb = (ArrayList) budgetamountformonths.get(countryname);
				for (int i = 1; i <= 12; i++)
				{
					JSONObject aboutcountrybudgetvalue = new JSONObject();
					Double d = (Double) abb.get(i);
					if (d != null)
					{
						aboutcountrybudgetvalue.put("value", d * (Double) countrycurrenceyvalue.get(countryCode));
					}
					/*aboutcountrybudgetvalue.put("value", d );*/
					countryjsonArray.add(aboutcountrybudgetvalue);
				}

				aboutcountry.put("seriesname", "Budget " + countryname);
				aboutcountry.put("color", color);
				aboutcountry.put("data", countryjsonArray);

				countryjsonbudgetArray.add(aboutcountry);

				aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
				budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
			}
			else
			{
				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
				for (int i = 1; i <= 12; i++)
				{
					JSONObject aboutcountrybudgetvalue = new JSONObject();
					// Double d=(Double)abb.get(i);
					aboutcountrybudgetvalue.put("value", 0);
					countryjsonArray.add(aboutcountrybudgetvalue);
				}

				aboutcountry.put("seriesname", "Budget " + countryName);
				aboutcountry.put("color", countryColor);
				aboutcountry.put("data", countryjsonArray);

				countryjsonbudgetArray.add(aboutcountry);

				aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
				budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
			}

			JSONObject aboutallcountriesbooking = new JSONObject();
			JSONArray countryjsonbookingArray = new JSONArray();

			JSONObject aboutcountry = new JSONObject();
			JSONArray countryjsonArray = new JSONArray();

			String countryname = (String) couArray[0];
			String color = (String) couArray[2];

			List<Object[]> boookingsofmonth = genericChartService.getBookingsOfMonthley(countryname, curYear);

			HashMap monthnumberss1 = new HashMap();
			for (Object[] bookingvalues : boookingsofmonth)
			{
				monthnumberss1.put((Integer) bookingvalues[1], (Double) bookingvalues[0]);

			}

			if (boookingsofmonth.size() != 0)
			{
				Set monthnumbers3 = monthnumberss1.keySet();
				for (int i = 1; i <= 12; i++)
				{
					JSONObject aboutcountrybookingvalue = new JSONObject();
					//Double d = migratesevice.getBookingvaluesforMonthleyYtd(i, countryname);
					if (monthnumbers3.contains(i))
					{
						// Double d=(Double)bookingvalues[0];
						aboutcountrybookingvalue.put("value", (Double) monthnumberss1.get(i));
						if (link.equalsIgnoreCase("product"))
							aboutcountrybookingvalue.put("link", "#/app/countryBookingYTD");
						countryjsonArray.add(aboutcountrybookingvalue);
					}
					else
					{
						aboutcountrybookingvalue.put("value", null);
						countryjsonArray.add(aboutcountrybookingvalue);
					}
				}
			}

			aboutcountry.put("seriesname", "Booking " + countryname);
			aboutcountry.put("color", color);
			aboutcountry.put("data", countryjsonArray);

			countryjsonbookingArray.add(aboutcountry);
			aboutallcountriesbooking.put("dataset", countryjsonbookingArray);

			budgetandbookingarrayforallcountries.add(aboutallcountriesbooking);

			String monthString = null;
			JSONObject tableObject = new JSONObject();
			JSONArray tabletArray = new JSONArray();

			for (int i = curYear - 2; i <= curYear; i++)
			{
				tableObject = new JSONObject();
				tableObject.put("year", i);

				for (int j = 1; j <= 4; j++)
				{
					if (j == 1)
					{
						monthString = "1,2,3";
						Double value = genericChartService.getQuarterlyBooking(i, monthString, countryname);
						tableObject.put("Q1", value);
					}
					if (j == 2)
					{
						monthString = "4,5,6";
						Double value = genericChartService.getQuarterlyBooking(i, monthString, countryname);
						tableObject.put("Q2", value);
					}
					if (j == 3)
					{
						monthString = "7,8,9";
						Double value = genericChartService.getQuarterlyBooking(i, monthString, countryname);
						tableObject.put("Q3", value);
					}
					if (j == 4)
					{
						monthString = "10,11,12";
						Double value = genericChartService.getQuarterlyBooking(i, monthString, countryname);
						tableObject.put("Q4", value);
					}
				}
				tabletArray.add(tableObject);
			}

			mainobject.put("data", budgetandbookingarrayforallcountries);
			mainobject.put("tabledata", tabletArray);

			return mainobject;

		}

		else
		{
			/*HashMap countrycurrenceyvalue = new HashMap();
			countrycurrenceyvalue.put("AU", 1.34);
			countrycurrenceyvalue.put("Hk", 6.68);
			countrycurrenceyvalue.put("IN", 67.20);
			countrycurrenceyvalue.put("ID", 13100.00);
			countrycurrenceyvalue.put("MY", 4.07);
			countrycurrenceyvalue.put("OT", 1.00);
			countrycurrenceyvalue.put("PH", 47.18);
			countrycurrenceyvalue.put("SG", 1.36);
			countrycurrenceyvalue.put("TH", 35.01);
			countrycurrenceyvalue.put("VN", 22301.52);*/

			HashMap monthname = new HashMap();
			monthname.put(1, "Jan");
			monthname.put(2, "Feb");
			monthname.put(3, "Mar");
			monthname.put(4, "Apr");
			monthname.put(5, "May");
			monthname.put(6, "Jun");
			monthname.put(7, "Jul");
			monthname.put(8, "Aug");
			monthname.put(9, "Sep");
			monthname.put(10, "Oct");
			monthname.put(11, "Nov");
			monthname.put(12, "Dec");

			JSONObject mainobject = new JSONObject();

			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			JSONObject months = null;
			JSONArray budgetandbookingarrayforallcountries = new JSONArray();

			List<Integer> monthnumbers = genericChartService.getDistinctMonthNumbers(curYear);

			for (int i = 1; i <= 12; i++)
			{
				months = new JSONObject();
				months.put("label", (String) monthname.get(i));
				jsonArray.add(months);
			}

			JSONObject subobject = new JSONObject();
			subobject.put("category", jsonArray);
			mainjsonArray.add(subobject);

			mainobject.put("xaxis", mainjsonArray);

			List<Object[]> countries = genericChartService.getCountriesNameColor(countryCode);
			Object[] couArray = countries.get(0);

			String countryName = (String) couArray[0];
			String countryColor = (String) couArray[1];

			ArrayList<String> countryarrayList = new ArrayList<String>();
			List<Object[]> budgetofallmonths = genericChartService.getBudgetofallmonths(curYear, countryName);
			HashMap budgetamountformonths = new HashMap();
			if (budgetofallmonths != null && !budgetofallmonths.isEmpty())
			{
				for (Object[] budmonth : budgetofallmonths)
				{
					ArrayList amd = new ArrayList();
					amd.add((String) budmonth[0]);
					amd.add((Double) budmonth[1]);
					amd.add((Double) budmonth[2]);
					amd.add((Double) budmonth[3]);
					amd.add((Double) budmonth[4]);
					amd.add((Double) budmonth[5]);
					amd.add((Double) budmonth[6]);
					amd.add((Double) budmonth[7]);
					amd.add((Double) budmonth[8]);
					amd.add((Double) budmonth[9]);
					amd.add((Double) budmonth[10]);
					amd.add((Double) budmonth[11]);
					amd.add((Double) budmonth[12]);
					budgetamountformonths.put((String) budmonth[0], amd);
				}

				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();

				String countryname = (String) couArray[0];
				String color = (String) couArray[1];

				ArrayList abb = (ArrayList) budgetamountformonths.get(countryname);
				for (int i = 1; i <= 12; i++)
				{
					JSONObject aboutcountrybudgetvalue = new JSONObject();
					Double d = (Double) abb.get(i);
					aboutcountrybudgetvalue.put("value", d);
					countryjsonArray.add(aboutcountrybudgetvalue);
				}

				aboutcountry.put("seriesname", "Budget " + countryname);
				aboutcountry.put("color", color);
				aboutcountry.put("data", countryjsonArray);

				countryjsonbudgetArray.add(aboutcountry);

				aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
				budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
			}
			else
			{
				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();
				for (int i = 1; i <= 12; i++)
				{
					JSONObject aboutcountrybudgetvalue = new JSONObject();
					// Double d=(Double)abb.get(i);
					aboutcountrybudgetvalue.put("value", 0);
					countryjsonArray.add(aboutcountrybudgetvalue);
				}

				aboutcountry.put("seriesname", "Budget " + countryName);
				aboutcountry.put("color", countryColor);
				aboutcountry.put("data", countryjsonArray);

				countryjsonbudgetArray.add(aboutcountry);

				aboutallcountriesbudget.put("dataset", countryjsonbudgetArray);
				budgetandbookingarrayforallcountries.add(aboutallcountriesbudget);
			}

			JSONObject aboutallcountriesbooking = new JSONObject();
			JSONArray countryjsonbookingArray = new JSONArray();

			JSONObject aboutcountry = new JSONObject();
			JSONArray countryjsonArray = new JSONArray();

			String countryname = (String) couArray[0];
			String color = (String) couArray[2];

			List<Object[]> boookingsofmonth = genericChartService.getBookingsOfMonthleyInUsd(countryname, curYear);

			HashMap monthnumberss1 = new HashMap();
			for (Object[] bookingvalues : boookingsofmonth)
			{
				monthnumberss1.put((Integer) bookingvalues[1], (Double) bookingvalues[0]);

			}

			if (boookingsofmonth.size() != 0)
			{
				Set monthnumbers3 = monthnumberss1.keySet();
				for (int i = 1; i <= 12; i++)
				{
					JSONObject aboutcountrybookingvalue = new JSONObject();
					/*
					 * Double d=migratesevice.getBookingvaluesforMonthleyYtd(i,
					 * countryname);
					 */
					if (monthnumbers3.contains(i))
					{
						// Double d=(Double)bookingvalues[0];
						aboutcountrybookingvalue.put("value", (Double) monthnumberss1.get(i));
						if (link.equalsIgnoreCase("product"))
							aboutcountrybookingvalue.put("link", "#/app/countryBookingYTD");
						countryjsonArray.add(aboutcountrybookingvalue);
					}
					else
					{
						aboutcountrybookingvalue.put("value", null);
						countryjsonArray.add(aboutcountrybookingvalue);
					}
				}
			}

			aboutcountry.put("seriesname", "Booking " + countryname);
			aboutcountry.put("color", color);
			aboutcountry.put("data", countryjsonArray);

			countryjsonbookingArray.add(aboutcountry);
			aboutallcountriesbooking.put("dataset", countryjsonbookingArray);

			budgetandbookingarrayforallcountries.add(aboutallcountriesbooking);

			String monthString = null;
			JSONObject tableObject = new JSONObject();
			JSONArray tabletArray = new JSONArray();

			for (int i = curYear - 2; i <= curYear; i++)
			{
				tableObject = new JSONObject();
				tableObject.put("year", i);

				for (int j = 1; j <= 4; j++)
				{
					if (j == 1)
					{
						monthString = "1,2,3";
						Double value = genericChartService.getQuarterlyBookingInUsd(i, monthString, countryname);
						tableObject.put("Q1", value);
					}
					if (j == 2)
					{
						monthString = "4,5,6";
						Double value = genericChartService.getQuarterlyBookingInUsd(i, monthString, countryname);
						tableObject.put("Q2", value);
					}
					if (j == 3)
					{
						monthString = "7,8,9";
						Double value = genericChartService.getQuarterlyBookingInUsd(i, monthString, countryname);
						tableObject.put("Q3", value);
					}
					if (j == 4)
					{
						monthString = "10,11,12";
						Double value = genericChartService.getQuarterlyBookingInUsd(i, monthString, countryname);
						tableObject.put("Q4", value);
					}
				}
				tabletArray.add(tableObject);
			}

			mainobject.put("data", budgetandbookingarrayforallcountries);
			mainobject.put("tabledata", tabletArray);

			return mainobject;
		}

	}

	/*
	@RequestMapping(value = "/BookingVsBudetYear", method = RequestMethod.GET)		
	 public @ResponseBody JSONObject BookingVsBudetYear(HttpServletResponse res )  
	 {  	
		JSONObject mainobject = new JSONObject();
		JSONArray jsonArray = new JSONArray();	
		JSONArray jsonArray1 = new JSONArray();	
		JSONArray jsonArray2 = new JSONArray();	
		JSONArray jsonArray3 = new JSONArray();	
		JSONObject subobject=null;
		DecimalFormat df = new DecimalFormat("#.##");
		
		JSONObject catObject = new JSONObject();
		JSONArray catArray = new JSONArray();	
		
		JSONObject series1 = new JSONObject();
		JSONObject series2 = new JSONObject();
		JSONObject series3 = new JSONObject();
		series1.put("seriesname","Budget");
		series2.put("seriesname","Booking");
		series3.put("seriesname","BookingLine");
		series3.put("renderas", "Line");
				
		Calendar now = Calendar.getInstance();
	    int currYear=now.get(Calendar.YEAR);
	    
		for(int i=2003;i<=currYear;i++)
		{			
			subobject=new JSONObject();		
			catObject = new JSONObject();
	    	
			catObject.put("label", String.valueOf(i));
			catArray.add(catObject);
	    	Double budgetValue=genericChartService.getBudgetValueByYear(i,"NA");
	    	subobject.put("value", budgetValue);
			subobject.put("color", "6495ED");
			
			//if(i==2016)
			//	subobject.put("color", "fdd400");
			
			jsonArray.add(subobject);
			
	    	Double bookingValue=genericChartService.getBookingValueByYear(i,"NA");
	    	//String booking_valueStr=df.format(bookingValue);
	    	 
			//subobject.put("label",String.valueOf(i));	    	
			subobject.put("value", bookingValue);
			subobject.put("color", "fdd400");
			
			//if(i==2016)
			//	subobject.put("color", "fdd400");
			
			jsonArray1.add(subobject);			
			jsonArray2.add(subobject);			
			
		}
		series1.put("data",jsonArray);
		series2.put("data",jsonArray1);		
		series3.put("data",jsonArray2);
		
		jsonArray3.add(series1);
		jsonArray3.add(series2);
		jsonArray3.add(series3);
		
		mainobject.put("dataset",jsonArray3);
		mainobject.put("category",catArray);
		
		return mainobject;
	}
	*/

	@RequestMapping(value = "/BookingVsBudetYear", method = RequestMethod.GET)
	public @ResponseBody JSONObject BookingVsBudetYear(HttpServletResponse res)
	{

		try
		{
			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			JSONArray jsonArray3 = new JSONArray();
			JSONObject subobjectbudget = null;
			JSONObject subobjectbooking = null;
			JSONObject subobjectline = null;

			DecimalFormat df = new DecimalFormat("#.##");

			JSONObject catObject = new JSONObject();
			JSONArray catArray = new JSONArray();

			JSONObject series1 = new JSONObject();
			JSONObject series2 = new JSONObject();
			JSONObject series3 = new JSONObject();
			series1.put("seriesname", "Budget");
			series2.put("seriesname", "Booking");
			series3.put("seriesname", "BookingLine");
			series3.put("renderas", "Line");

			Calendar now = Calendar.getInstance();
			int currYear = now.get(Calendar.YEAR);

			for (int i = 2003; i <= currYear; i++)
			{
				subobjectbudget = new JSONObject();
				subobjectbooking = new JSONObject();
				subobjectline = new JSONObject();
				catObject = new JSONObject();

				catObject.put("label", String.valueOf(i));
				catArray.add(catObject);
				Double budgetValue = genericChartService.getBudgetValueByYear(i, "NA");
				subobjectbudget.put("value", budgetValue);
				subobjectbudget.put("color", "6495ED");

				//if(i==2016)
				//	subobject.put("color", "fdd400");

				jsonArray.add(subobjectbudget);

				Double bookingValue = genericChartService.getBookingValueByYear(i, "NA");
				//String booking_valueStr=df.format(bookingValue);

				//subobject.put("label",String.valueOf(i));	    	
				subobjectbooking.put("value", bookingValue);
				subobjectbooking.put("color", "fdd400");

				//if(i==2016)
				//	subobject.put("color", "fdd400");

				jsonArray1.add(subobjectbooking);
				/*	subobjectline.put("color","fdd400");*/
				jsonArray2.add(subobjectbooking);

			}
			series1.put("data", jsonArray);
			series2.put("data", jsonArray1);
			series3.put("data", jsonArray2);

			jsonArray3.add(series1);
			jsonArray3.add(series2);
			jsonArray3.add(series3);

			mainobject.put("dataset", jsonArray3);
			mainobject.put("category", catArray);

			return mainobject;
		}
		catch (Exception e)
		{
			logger.info("=================CONTROLLER METHOD=BookingVsBudetYear=" + e.getCause());
			return null;
		}
	}

	@RequestMapping(value = "/bookingVsBudetYearCountryWise", method = RequestMethod.GET)
	public @ResponseBody JSONObject bookingVsBudetYearCowuntryWise(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			String countryCode = req.getParameter("countryCode");
			String id = req.getParameter("id");

			if (id.equalsIgnoreCase("local"))
			{
				/*HashMap countrycurrenceyvalue = new HashMap();
				countrycurrenceyvalue.put("AU", 1.371);
				countrycurrenceyvalue.put("Hk", 6.68);
				countrycurrenceyvalue.put("IN", 66.4);
				countrycurrenceyvalue.put("ID", 13731.75);
				countrycurrenceyvalue.put("MY", 4.29);
				countrycurrenceyvalue.put("OT", 1.00);
				countrycurrenceyvalue.put("PH", 47.06);
				countrycurrenceyvalue.put("SG", 1.45);
				countrycurrenceyvalue.put("TH", 35.99);
				countrycurrenceyvalue.put("VN", 22510.52);*/
				HashMap countrycurrenceyvalue = new HashMap();
				countrycurrenceyvalue.put("AU", 1.371);
				countrycurrenceyvalue.put("Hk", 6.68);
				countrycurrenceyvalue.put("IN", 66.4);
				countrycurrenceyvalue.put("ID", 13736.26);
				countrycurrenceyvalue.put("MY", 4.288);
				countrycurrenceyvalue.put("OT", 1.00);
				countrycurrenceyvalue.put("PH", 47.0);
				countrycurrenceyvalue.put("SG", 1.41);
				countrycurrenceyvalue.put("TH", 35.97);
				countrycurrenceyvalue.put("VN", 22522.52);

				JSONObject mainobject = new JSONObject();
				JSONArray jsonArray = new JSONArray();
				JSONArray jsonArray1 = new JSONArray();
				JSONArray jsonArray2 = new JSONArray();
				JSONArray jsonArray3 = new JSONArray();
				JSONObject subobject = null;
				//DecimalFormat df = new DecimalFormat("#.##");

				JSONObject catObject = new JSONObject();
				JSONArray catArray = new JSONArray();

				JSONObject series1 = new JSONObject();
				JSONObject series2 = new JSONObject();
				JSONObject series3 = new JSONObject();
				series1.put("seriesname", "Budget");
				series2.put("seriesname", "Booking");
				series3.put("seriesname", "BookingLine");
				series3.put("renderas", "Line");

				Calendar now = Calendar.getInstance();
				int currYear = now.get(Calendar.YEAR);

				for (int i = 2003; i <= currYear; i++)
				{
					subobject = new JSONObject();
					catObject = new JSONObject();

					catObject.put("label", String.valueOf(i));
					catArray.add(catObject);
					Double budgetValue = genericChartService.getBudgetValueByYear(i, countryCode);
					subobject.put("value", budgetValue * (Double) countrycurrenceyvalue.get(countryCode));
					subobject.put("color", "6495ED");

					//if(i==2016)
					//	subobject.put("color", "fdd400");

					jsonArray.add(subobject);

					Double bookingValue = genericChartService.getBookingValueByYear(i, countryCode);
					//String booking_valueStr=df.format(bookingValue);

					//subobject.put("label",String.valueOf(i));	    	
					subobject.put("value", bookingValue * (Double) countrycurrenceyvalue.get(countryCode));
					subobject.put("color", "fdd400");

					//if(i==2016)
					//	subobject.put("color", "fdd400");

					jsonArray1.add(subobject);
					jsonArray2.add(subobject);

				}
				series1.put("data", jsonArray);
				series2.put("data", jsonArray1);
				series3.put("data", jsonArray2);

				jsonArray3.add(series1);
				jsonArray3.add(series2);
				jsonArray3.add(series3);

				mainobject.put("dataset", jsonArray3);
				mainobject.put("category", catArray);

				return mainobject;

			}

			else
			{

				/*HashMap countrycurrenceyvalue = new HashMap();
				countrycurrenceyvalue.put("AU", 1.371);
				countrycurrenceyvalue.put("Hk", 6.68);
				countrycurrenceyvalue.put("IN", 66.4);
				countrycurrenceyvalue.put("ID", 13731.75);
				countrycurrenceyvalue.put("MY", 4.29);
				countrycurrenceyvalue.put("OT", 1.00);
				countrycurrenceyvalue.put("PH", 47.06);
				countrycurrenceyvalue.put("SG", 1.45);
				countrycurrenceyvalue.put("TH", 35.99);
				countrycurrenceyvalue.put("VN", 22510.52);*/
				JSONObject mainobject = new JSONObject();
				JSONArray jsonArray = new JSONArray();
				JSONArray jsonArray1 = new JSONArray();
				JSONArray jsonArray2 = new JSONArray();
				JSONArray jsonArray3 = new JSONArray();
				JSONObject subobject = null;
				//DecimalFormat df = new DecimalFormat("#.##");

				JSONObject catObject = new JSONObject();
				JSONArray catArray = new JSONArray();

				JSONObject series1 = new JSONObject();
				JSONObject series2 = new JSONObject();
				JSONObject series3 = new JSONObject();
				series1.put("seriesname", "Budget");
				series2.put("seriesname", "Booking");
				series3.put("seriesname", "BookingLine");
				series3.put("renderas", "Line");

				Calendar now = Calendar.getInstance();
				int currYear = now.get(Calendar.YEAR);

				for (int i = 2003; i <= currYear; i++)
				{
					subobject = new JSONObject();
					catObject = new JSONObject();

					catObject.put("label", String.valueOf(i));
					catArray.add(catObject);
					Double budgetValue = genericChartService.getBudgetValueByYear(i, countryCode);
					subobject.put("value", budgetValue);
					subobject.put("color", "6495ED");

					//if(i==2016)
					//	subobject.put("color", "fdd400");

					jsonArray.add(subobject);

					Double bookingValue = genericChartService.getBookingValueByYear(i, countryCode);
					//String booking_valueStr=df.format(bookingValue);

					//subobject.put("label",String.valueOf(i));	    	
					subobject.put("value", bookingValue);
					subobject.put("color", "fdd400");

					//if(i==2016)
					//	subobject.put("color", "fdd400");

					jsonArray1.add(subobject);
					jsonArray2.add(subobject);

				}
				series1.put("data", jsonArray);
				series2.put("data", jsonArray1);
				series3.put("data", jsonArray2);

				jsonArray3.add(series1);
				jsonArray3.add(series2);
				jsonArray3.add(series3);

				mainobject.put("dataset", jsonArray3);
				mainobject.put("category", catArray);

				return mainobject;

			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block

			logger.info("========CONTROLLER METHOD==bookingVsBudetYearCowuntryWise====" + e.getCause());
			return null;
		}
	}

	@RequestMapping(value = "/SCAgroupbyproduct", method = RequestMethod.GET)
	public @ResponseBody JSONObject groupByProductInner(HttpServletRequest req, HttpServletResponse resp)
	{
		try
		{
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);

			List<Integer> lastyears = genericChartService.getLastYears(year);

			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			ArrayList<Integer> yearsinorder = new ArrayList<Integer>();
			JSONObject months = null;
			for (Integer years : lastyears)
			{
				months = new JSONObject();
				yearsinorder.add(years);
				months.put("label", "" + years + "");
				jsonArray.add(months);
			}

			JSONObject subobject = new JSONObject();
			subobject.put("category", jsonArray);
			mainjsonArray.add(subobject);

			mainobject.put("xaxis", mainjsonArray);

			List<Object[]> aboutcatgories = migratesevice.getAllDetailsAboutCatageory();

			JSONObject aboutallcountriesbudget = new JSONObject();
			JSONArray countryjsonbudgetArray = new JSONArray();

			for (Object[] catageoreydetails : aboutcatgories)
			{
				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();

				String catageoryname = (String) catageoreydetails[0];
				String color = (String) catageoreydetails[1];

				List<Object[]> catagaoysbookingamountoveryears = genericChartService
				        .catagaoysbookingamountoveryears(catageoryname, year);
				HashMap<Integer, Double> data = new HashMap<Integer, Double>();
				for (Object[] yam : catagaoysbookingamountoveryears)
				{
					data.put((Integer) yam[0], (Double) yam[1]);
				}

				if (catagaoysbookingamountoveryears.size() != 0)

				{
					Set<Integer> years5 = data.keySet();

					for (Integer data1 : yearsinorder)

					{
						JSONObject data3 = new JSONObject();

						if (years5.contains(data1))
						{

							data3.put("value", (Double) data.get(data1));
							data3.put("link", " #/app/SCA_GroupByProduct_YTD");
							countryjsonArray.add(data3);

						}

						else
						{

							data3.put("value", null);
							countryjsonArray.add(data3);

						}

					}

				}

				aboutcountry.put("seriesname", "Booking " + catageoryname);
				aboutcountry.put("color", color);
				aboutcountry.put("data", countryjsonArray);
				countryjsonbudgetArray.add(aboutcountry);
			}

			mainobject.put("data", countryjsonbudgetArray);

			/*for table*/
			JSONArray catageroreyararray1 = new JSONArray();

			for (Integer tableyears : lastyears)

			{
				JSONArray catageroreyararray = new JSONArray();
				List<Object[]> catagoriesinpartocularyear = genericChartService.getTabledataforCatagories(tableyears,
				        aboutcatgories);
				for (Object[] tableproduct : catagoriesinpartocularyear)
				{
					JSONObject catageorydata = new JSONObject();

					catageorydata.put("year", tableyears);
					catageorydata.put("name", (String) tableproduct[0]);
					catageorydata.put("value", (Double) tableproduct[1]);

					catageroreyararray.add(catageorydata);

				}

				catageroreyararray1.add(catageroreyararray);

			}

			mainobject.put("tableData", catageroreyararray1);
			return mainobject;
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.info("=========CONTROLLER METHOD=groupByProductInner=======" + e.getCause());
			return null;
		}
	}

	@RequestMapping(value = "/SCAgroupbyproductdrilldown", method = RequestMethod.GET)
	public @ResponseBody JSONObject groupByProductInnerdrilldown(HttpServletRequest req, HttpServletResponse resp)
	{

		try
		{
			String cat = req.getParameter("catageory");
			String category = cat.substring(cat.lastIndexOf(" ") + 1);
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);

			List<Integer> lastyears = genericChartService.getLastYears(year);

			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			ArrayList<Integer> yearsinorder = new ArrayList<Integer>();
			JSONObject months = null;
			for (Integer years : lastyears)
			{
				months = new JSONObject();
				yearsinorder.add(years);
				months.put("label", "" + years + "");
				jsonArray.add(months);
			}

			JSONObject subobject = new JSONObject();
			subobject.put("category", jsonArray);
			mainjsonArray.add(subobject);

			mainobject.put("xaxis", mainjsonArray);

			List<Object[]> aboutcatgories = genericChartService.getAllDetailsAboutProducts(category);

			JSONObject aboutallcountriesbudget = new JSONObject();
			JSONArray countryjsonbudgetArray = new JSONArray();

			for (Object[] catageoreydetails : aboutcatgories)
			{
				JSONObject aboutcountry = new JSONObject();
				JSONArray countryjsonArray = new JSONArray();

				String catageoryname = (String) catageoreydetails[0];
				String color = (String) catageoreydetails[1];

				List<Object[]> catagaoysbookingamountoveryears = genericChartService
				        .catagaoysbookingamountforproductoveryears(catageoryname, year, category);
				HashMap<Integer, Double> data = new HashMap<Integer, Double>();
				for (Object[] yam : catagaoysbookingamountoveryears)
				{
					data.put((Integer) yam[0], (Double) yam[1]);
				}

				if (catagaoysbookingamountoveryears.size() != 0)

				{
					Set<Integer> years5 = data.keySet();

					for (Integer data1 : yearsinorder)

					{
						JSONObject data3 = new JSONObject();

						if (years5.contains(data1))
						{

							data3.put("value", (Double) data.get(data1));
							/*data3.put("link"," #/app/SCA_GroupByProduct_YTD");*/
							countryjsonArray.add(data3);

						}

						else
						{

							data3.put("value", null);
							countryjsonArray.add(data3);

						}

					}

				}

				aboutcountry.put("seriesname", "Booking " + catageoryname);
				aboutcountry.put("color", color);
				aboutcountry.put("data", countryjsonArray);
				countryjsonbudgetArray.add(aboutcountry);
			}

			mainobject.put("data", countryjsonbudgetArray);

			/*for table*/
			JSONArray catageroreyararray = new JSONArray();

			for (Integer tableyears : lastyears)

			{
				JSONArray catageroreyararray1 = new JSONArray();
				List<Object[]> catagoriesinpartocularyear = genericChartService.getTabledataforproducts(tableyears, category);
				for (Object[] tableproduct : catagoriesinpartocularyear)
				{
					JSONObject catageorydata = new JSONObject();

					catageorydata.put("year", tableyears);
					catageorydata.put("name", (String) tableproduct[0]);
					catageorydata.put("value", (Double) tableproduct[1]);

					catageroreyararray1.add(catageorydata);

				}
				catageroreyararray.add(catageroreyararray1);

			}

			mainobject.put("tabledataaaa", catageroreyararray);

			return mainobject;
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			logger.info("=====CONTROLLER METHOD==groupByProductInnerdrilldown=====" + e.getCause());

			return null;
		}
	}

	/*@RequestMapping(value = "/industryShareYtdforcountry", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   industryShareYtd(HttpServletRequest req,HttpServletResponse res) 
	{		
		HttpSession countryname = req.getSession();
		String group = (String) req.getParameter("group");
		String countrycode = (String) req.getParameter("countrycode");
	
		HashMap<String,String> CodeToCountryMap=new HashMap<String,String>();
		
		CodeToCountryMap.put("AU","Australia");
		CodeToCountryMap.put("ID","Indonesia");
		CodeToCountryMap.put("IN","India");
		CodeToCountryMap.put("MY","Malaysia");
		CodeToCountryMap.put("PH","Philippines");
		CodeToCountryMap.put("SG","Singapore");
		CodeToCountryMap.put("TH","Thailand");
		CodeToCountryMap.put("VN","Vietnam");
		CodeToCountryMap.put("OT","Other");
		
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1;
		int date = now.get(Calendar.DATE);
	
		JSONObject keyforyears = new JSONObject();
	
		try {
			
			int currentyearafter3yeras = year - 3;
	
			for (int i = currentyearafter3yeras; i <= year; i++)
			{
				List industrySectorData = genericChartService.getBookingValueForIndustrySectorShareOptBasedoncountry(i,CodeToCountryMap.get(countrycode),group);
	           
				
		
				if (i == year) {
					keyforyears.put("data", industrySectorData);
	
				}
				if (i == year - 3) {
					keyforyears.put("data1", industrySectorData);
				}
				if (i == year - 2) {
					keyforyears.put("data2", industrySectorData);
				}
				if (i == year - 1) {
					keyforyears.put("data3", industrySectorData);
				}
			}
			
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			JSONArray jsonArray3 = new JSONArray();
			JSONArray jsonArray4 = new JSONArray();
			JSONArray jsonArray = new JSONArray();
			
			JSONObject jsonObj = null;
			List industrySectorTableData=migratesevice.getBookingValueForIndustrySectorShareTableDataByCountry("All",countrycode);
			if(industrySectorTableData!=null)
			{
			for(int i=0;i<industrySectorTableData.size();i++)
			{
				jsonObj = new JSONObject();
				Map mp=(Map)industrySectorTableData.get(i);
				Integer yr=(Integer)mp.get("year");
				String name=(String)mp.get("name");
				Double val=(Double)mp.get("value");				
				
				if(yr==2016)
				{
					jsonObj.put("year", yr);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray1.add(jsonObj);
				}
				if(yr==2015)
				{
					jsonObj.put("year", yr);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray2.add(jsonObj);
				}
				if(yr==2014)
				{
					jsonObj.put("year", yr);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray3.add(jsonObj);
				}
				if(yr==2013)
				{
					jsonObj.put("year", yr);
					jsonObj.put("name", name);
					jsonObj.put("value", val);
					jsonArray4.add(jsonObj);
				}
			}
		}
	
			jsonArray.add(jsonArray1);
			jsonArray.add(jsonArray2);
			jsonArray.add(jsonArray3);
			jsonArray.add(jsonArray4);
			keyforyears.put("tableData",jsonArray);	
	
			return keyforyears;
		} catch (Exception e) {
			logger.info("this is the error message" + e.getMessage());
			return null;
		}
		 
	}
	*/

	@RequestMapping(value = "/industryShareYtdforcountry", method = RequestMethod.GET)
	public @ResponseBody JSONObject industryShareYtd(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			HttpSession countryname = req.getSession();
			String group = (String) req.getParameter("group");
			String countrycode = (String) req.getParameter("countrycode");
			String id = req.getParameter("id");

			if (id.equalsIgnoreCase("local"))
			{

				HashMap<String, String> CodeToCountryMap = new HashMap<String, String>();

				CodeToCountryMap.put("AU", "Australia");
				CodeToCountryMap.put("ID", "Indonesia");
				CodeToCountryMap.put("IN", "India");
				CodeToCountryMap.put("MY", "Malaysia");
				CodeToCountryMap.put("PH", "Philippines");
				CodeToCountryMap.put("SG", "Singapore");
				CodeToCountryMap.put("TH", "Thailand");
				CodeToCountryMap.put("VN", "Vietnam");
				CodeToCountryMap.put("OT", "Other");

				Calendar now = Calendar.getInstance();
				int year = now.get(Calendar.YEAR);
				int month = now.get(Calendar.MONTH) + 1;
				int date = now.get(Calendar.DATE);

				JSONObject keyforyears = new JSONObject();

				try
				{

					int currentyearafter3yeras = year - 4;

					for (int i = currentyearafter3yeras; i <= year; i++)
					{
						List industrySectorData = genericChartService.getBookingValueForIndustrySectorShareOptBasedoncountry(i,
						        CodeToCountryMap.get(countrycode), group);

						if (i == 2017)
						{
							keyforyears.put("data", industrySectorData);

						}
						if (i == 2014)
						{
							keyforyears.put("data1", industrySectorData);
						}
						if (i == 2015)
						{
							keyforyears.put("data2", industrySectorData);
						}
						if (i == 2016)
						{
							keyforyears.put("data3", industrySectorData);
						}
					}

					JSONArray jsonArray1 = new JSONArray();
					JSONArray jsonArray2 = new JSONArray();
					JSONArray jsonArray3 = new JSONArray();
					JSONArray jsonArray4 = new JSONArray();
					JSONArray jsonArray = new JSONArray();

					JSONObject jsonObj = null;
					List industrySectorTableData = migratesevice.getBookingValueForIndustrySectorShareTableDataByCountry("All",
					        countrycode);
					if (industrySectorTableData != null)
					{
						for (int i = 0; i < industrySectorTableData.size(); i++)
						{
							jsonObj = new JSONObject();
							Map mp = (Map) industrySectorTableData.get(i);
							Integer yr = (Integer) mp.get("year");
							String name = (String) mp.get("name");
							Double val = (Double) mp.get("value");

							if (yr == 2017)
							{
								jsonObj.put("year", yr);
								jsonObj.put("name", name);
								jsonObj.put("value", val);
								jsonArray1.add(jsonObj);
							}
							if (yr == 2016)
							{
								jsonObj.put("year", yr);
								jsonObj.put("name", name);
								jsonObj.put("value", val);
								jsonArray2.add(jsonObj);
							}
							if (yr == 2015)
							{
								jsonObj.put("year", yr);
								jsonObj.put("name", name);
								jsonObj.put("value", val);
								jsonArray3.add(jsonObj);
							}
							if (yr == 2014)
							{
								jsonObj.put("year", yr);
								jsonObj.put("name", name);
								jsonObj.put("value", val);
								jsonArray4.add(jsonObj);
							}
						}
					}

					jsonArray.add(jsonArray1);
					jsonArray.add(jsonArray2);
					jsonArray.add(jsonArray3);
					jsonArray.add(jsonArray4);
					keyforyears.put("tableData", jsonArray);

					return keyforyears;
				}
				catch (Exception e)
				{
					logger.info("this is the error message" + e.getMessage());
					return null;
				}

			}

			else
			{

				HashMap<String, String> CodeToCountryMap = new HashMap<String, String>();

				CodeToCountryMap.put("AU", "Australia");
				CodeToCountryMap.put("ID", "Indonesia");
				CodeToCountryMap.put("IN", "India");
				CodeToCountryMap.put("MY", "Malaysia");
				CodeToCountryMap.put("PH", "Philippines");
				CodeToCountryMap.put("SG", "Singapore");
				CodeToCountryMap.put("TH", "Thailand");
				CodeToCountryMap.put("VN", "Vietnam");
				CodeToCountryMap.put("OT", "Other");

				Calendar now = Calendar.getInstance();
				int year = now.get(Calendar.YEAR);
				int month = now.get(Calendar.MONTH) + 1;
				int date = now.get(Calendar.DATE);

				JSONObject keyforyears = new JSONObject();

				try
				{

					int currentyearafter3yeras = year - 4;

					for (int i = currentyearafter3yeras; i <= year; i++)
					{
						List industrySectorData = genericChartService
						        .getBookingValueForIndustrySectorShareOptBasedoncountryOnDollar(i,
						                CodeToCountryMap.get(countrycode), group);

						if (i == 2016)
						{
							keyforyears.put("data", industrySectorData);

						}
						if (i == 2013)
						{
							keyforyears.put("data1", industrySectorData);
						}
						if (i == 2014)
						{
							keyforyears.put("data2", industrySectorData);
						}
						if (i == 2015)
						{
							keyforyears.put("data3", industrySectorData);
						}
					}

					JSONArray jsonArray1 = new JSONArray();
					JSONArray jsonArray2 = new JSONArray();
					JSONArray jsonArray3 = new JSONArray();
					JSONArray jsonArray4 = new JSONArray();
					JSONArray jsonArray = new JSONArray();

					JSONObject jsonObj = null;
					List industrySectorTableData = migratesevice
					        .getBookingValueForIndustrySectorShareTableDataByCountryDollar("All", countrycode);
					if (industrySectorTableData != null)
					{
						for (int i = 0; i < industrySectorTableData.size(); i++)
						{
							jsonObj = new JSONObject();
							Map mp = (Map) industrySectorTableData.get(i);
							Integer yr = (Integer) mp.get("year");
							String name = (String) mp.get("name");
							Double val = (Double) mp.get("value");

							if (yr == 2016)
							{
								jsonObj.put("year", yr);
								jsonObj.put("name", name);
								jsonObj.put("value", val);
								jsonArray1.add(jsonObj);
							}
							if (yr == 2015)
							{
								jsonObj.put("year", yr);
								jsonObj.put("name", name);
								jsonObj.put("value", val);
								jsonArray2.add(jsonObj);
							}
							if (yr == 2014)
							{
								jsonObj.put("year", yr);
								jsonObj.put("name", name);
								jsonObj.put("value", val);
								jsonArray3.add(jsonObj);
							}
							if (yr == 2013)
							{
								jsonObj.put("year", yr);
								jsonObj.put("name", name);
								jsonObj.put("value", val);
								jsonArray4.add(jsonObj);
							}
						}
					}

					jsonArray.add(jsonArray1);
					jsonArray.add(jsonArray2);
					jsonArray.add(jsonArray3);
					jsonArray.add(jsonArray4);
					keyforyears.put("tableData", jsonArray);

					return keyforyears;
				}
				catch (Exception e)
				{
					logger.info("this is the error message" + e.getMessage());
					return null;
				}

			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.info("======CONTROLLER METHOD==industryShareYtd=============" + e.getCause());
			return null;
		}

	}

	@RequestMapping(value = "/productShareYtdforcountry", method = RequestMethod.GET)
	public @ResponseBody JSONObject productShareYtd(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			HttpSession countryname = req.getSession();
			//String countrycode = (String) countryname.getAttribute("countrycode");
			String countrycode = (String) req.getAttribute("countrycode");
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1;
			int date = now.get(Calendar.DATE);
			JSONObject keyforyears = new JSONObject();

			int currentyearafter3yeras = year - 3;

			for (int i = currentyearafter3yeras; i <= year; i++)
			{
				List productData = genericChartService.getBookingValueForProductShareOptBasingonCountry(i, countrycode);

				if (i == year)
					keyforyears.put("data", productData);
				if (i == year - 3)
					keyforyears.put("data1", productData);
				if (i == year - 2)
					keyforyears.put("data2", productData);
				if (i == year - 1)
					keyforyears.put("data3", productData);
			}

			return keyforyears;
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block

			logger.info("==============CONTROLLER METHOD=productShareYtd======================" + e.getCause());

			return null;

		}

	}

	@RequestMapping(value = "/industryShareYtdInnerdrilldown", method = RequestMethod.GET)
	public @ResponseBody JSONObject industryShareYtdInner(HttpServletRequest req, HttpServletResponse resp)
	{
		HttpSession countryname = req.getSession();
		String countrycode = (String) req.getParameter("countrycode");
		String id = req.getParameter("id");

		try
		{
			JSONObject keyforyears = new JSONObject();

			if (id.equalsIgnoreCase("local"))
			{
				JSONObject dataBooking = null;
				JSONArray jsonArrayofdataBooking = new JSONArray();

				List<Object[]> industrycodes = genericChartService
				        .getIndustriesByIndustrySectorbasedoncountry(req.getParameter("industrysectorcode"), countrycode);

				for (Object[] industrycode : industrycodes)
				{
					dataBooking = new JSONObject();
					String iname = (String) industrycode[0];
					String icode = (String) industrycode[1];
					Double budget_value_industry = genericChartService.getBookingValueForIndustrySharebasedoncountry(icode,
					        Integer.parseInt(req.getParameter("year")), countrycode);

					dataBooking.put("name", iname);
					dataBooking.put("value", budget_value_industry);

					jsonArrayofdataBooking.add(dataBooking);
				}
				keyforyears.put("data", jsonArrayofdataBooking);

				JSONArray jsonArray1 = new JSONArray();
				JSONArray jsonArray2 = new JSONArray();
				JSONArray jsonArray3 = new JSONArray();
				JSONArray jsonArray4 = new JSONArray();
				JSONArray jsonArray = new JSONArray();

				JSONObject jsonObj = null;
				List industrySectorTableData = migratesevice.getBookingValueForIndustrySectorShareTableDataByCountry(
				        req.getParameter("industrysectorcode"), countrycode);
				for (int i = 0; i < industrySectorTableData.size(); i++)
				{
					jsonObj = new JSONObject();
					Map mp = (Map) industrySectorTableData.get(i);
					Integer yr = (Integer) mp.get("year");
					String name = (String) mp.get("name");
					Double val = (Double) mp.get("value");

					if (yr == 2016)
					{
						jsonObj.put("year", yr);
						jsonObj.put("name", name);
						jsonObj.put("value", val);
						jsonArray1.add(jsonObj);
					}
					if (yr == 2015)
					{
						jsonObj.put("year", yr);
						jsonObj.put("name", name);
						jsonObj.put("value", val);
						jsonArray2.add(jsonObj);
					}
					if (yr == 2014)
					{
						jsonObj.put("year", yr);
						jsonObj.put("name", name);
						jsonObj.put("value", val);
						jsonArray3.add(jsonObj);
					}
					if (yr == 2013)
					{
						jsonObj.put("year", yr);
						jsonObj.put("name", name);
						jsonObj.put("value", val);
						jsonArray4.add(jsonObj);
					}
				}

				jsonArray.add(jsonArray1);
				jsonArray.add(jsonArray2);
				jsonArray.add(jsonArray3);
				jsonArray.add(jsonArray4);
				keyforyears.put("tableData", jsonArray);
				return keyforyears;

			}

			else
			{

				JSONObject dataBooking = null;
				JSONArray jsonArrayofdataBooking = new JSONArray();

				List<Object[]> industrycodes = genericChartService
				        .getIndustriesByIndustrySectorbasedoncountry(req.getParameter("industrysectorcode"), countrycode);

				for (Object[] industrycode : industrycodes)
				{
					dataBooking = new JSONObject();
					String iname = (String) industrycode[0];
					String icode = (String) industrycode[1];
					Double budget_value_industry = genericChartService.getBookingValueForIndustrySharebasedoncountrydollr(icode,
					        Integer.parseInt(req.getParameter("year")), countrycode);

					dataBooking.put("name", iname);
					dataBooking.put("value", budget_value_industry);

					jsonArrayofdataBooking.add(dataBooking);
				}
				keyforyears.put("data", jsonArrayofdataBooking);

				JSONArray jsonArray1 = new JSONArray();
				JSONArray jsonArray2 = new JSONArray();
				JSONArray jsonArray3 = new JSONArray();
				JSONArray jsonArray4 = new JSONArray();
				JSONArray jsonArray = new JSONArray();

				JSONObject jsonObj = null;
				List industrySectorTableData = migratesevice.getBookingValueForIndustrySectorShareTableDataByCountrydollar(
				        req.getParameter("industrysectorcode"), countrycode);
				for (int i = 0; i < industrySectorTableData.size(); i++)
				{
					jsonObj = new JSONObject();
					Map mp = (Map) industrySectorTableData.get(i);
					Integer yr = (Integer) mp.get("year");
					String name = (String) mp.get("name");
					Double val = (Double) mp.get("value");

					if (yr == 2016)
					{
						jsonObj.put("year", yr);
						jsonObj.put("name", name);
						jsonObj.put("value", val);
						jsonArray1.add(jsonObj);
					}
					if (yr == 2015)
					{
						jsonObj.put("year", yr);
						jsonObj.put("name", name);
						jsonObj.put("value", val);
						jsonArray2.add(jsonObj);
					}
					if (yr == 2014)
					{
						jsonObj.put("year", yr);
						jsonObj.put("name", name);
						jsonObj.put("value", val);
						jsonArray3.add(jsonObj);
					}
					if (yr == 2013)
					{
						jsonObj.put("year", yr);
						jsonObj.put("name", name);
						jsonObj.put("value", val);
						jsonArray4.add(jsonObj);
					}
				}

				jsonArray.add(jsonArray1);
				jsonArray.add(jsonArray2);
				jsonArray.add(jsonArray3);
				jsonArray.add(jsonArray4);
				keyforyears.put("tableData", jsonArray);
				return keyforyears;

			}
		}
		catch (Exception e)
		{
			logger.info("===========CONTROLLER METHOD=industryShareYtdInner===" + e.getMessage());
			return null;
		}
	}

	@RequestMapping(value = "/productShareYtdCountryWise", method = RequestMethod.GET)
	public @ResponseBody JSONObject productShareYtdCountryWise(HttpServletRequest req, HttpServletResponse resp)
	{
		try
		{
			String countrycode = (String) req.getParameter("countrycode");
			String id = (String) req.getParameter("id");
			logger.info("countrycode=============" + countrycode);
			JSONObject keyforyears = new JSONObject();

			HashMap<String, String> CodeToCountryMap = new HashMap<String, String>();

			CodeToCountryMap.put("AU", "Australia");
			CodeToCountryMap.put("ID", "Indonesia");
			CodeToCountryMap.put("IN", "India");
			CodeToCountryMap.put("MY", "Malaysia");
			CodeToCountryMap.put("PH", "Philippines");
			CodeToCountryMap.put("SG", "Singapore");
			CodeToCountryMap.put("TH", "Thailand");
			CodeToCountryMap.put("VN", "Vietnam");
			CodeToCountryMap.put("OT", "Other");

			if (id.equalsIgnoreCase("local"))
			{
				for (int i = 2013; i <= 2016; i++)
				{
					List productData = migratesevice.getBookingValueForProductShareOpt(i, CodeToCountryMap.get(countrycode));

					if (i == 2016)
						keyforyears.put("data", productData);
					if (i == 2013)
						keyforyears.put("data1", productData);
					if (i == 2014)
						keyforyears.put("data2", productData);
					if (i == 2015)
						keyforyears.put("data3", productData);
				}

				return keyforyears;

			}

			else
			{
				for (int i = 2013; i <= 2016; i++)
				{
					List productData = migratesevice.getBookingValueForProductShareOptindollars(i,
					        CodeToCountryMap.get(countrycode));

					if (i == 2016)
						keyforyears.put("data", productData);
					if (i == 2013)
						keyforyears.put("data1", productData);
					if (i == 2014)
						keyforyears.put("data2", productData);
					if (i == 2015)
						keyforyears.put("data3", productData);
				}

				return keyforyears;

			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("====CONTROLLER METHOD=productShareYtdCountryWise======" + e.getCause());

			return null;
		}
	}

	@RequestMapping(value = "/countrybyproduct", method = RequestMethod.GET)
	public @ResponseBody JSONObject countryByProductInner(HttpServletRequest req, HttpServletResponse resp)
	{

		try
		{
			String countrycode = req.getParameter("countryCode");
			String cggc = req.getParameter("group");
			String id = req.getParameter("id");
			if (id.equalsIgnoreCase("local"))
			{
				HashMap countryclinks = new HashMap();
				countryclinks.put("AU", "Australia_GroupByProduct_YTD");
				countryclinks.put("Hk", "");
				countryclinks.put("IN", "India_GroupByProduct_YTD");
				countryclinks.put("ID", "Indonesia_GroupByProduct_YTD");
				countryclinks.put("MY", "Malaysia_GroupByProduct_YTD");
				countryclinks.put("OT", "Other_GroupByProduct_YTD");
				countryclinks.put("PH", "Philippines_GroupByProduct_YTD");
				countryclinks.put("SG", "Singapore_GroupByProduct_YTD");
				countryclinks.put("TH", "Thailand_GroupByProduct_YTD");
				countryclinks.put("VN", "Vietnam_GroupByProduct_YTD");
				Calendar now = Calendar.getInstance();
				int year = now.get(Calendar.YEAR);

				//System.out.println("sivaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa "+k43+"sivaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa--------->");

				List<Integer> lastyears = genericChartService.getLastYears(year);

				JSONObject mainobject = new JSONObject();
				JSONArray jsonArray = new JSONArray();
				JSONArray mainjsonArray = new JSONArray();
				ArrayList<Integer> yearsinorder = new ArrayList<Integer>();
				JSONObject months = null;
				for (Integer years : lastyears)
				{
					months = new JSONObject();
					yearsinorder.add(years);
					months.put("label", "" + years + "");
					jsonArray.add(months);
				}

				JSONObject subobject = new JSONObject();
				subobject.put("category", jsonArray);
				mainjsonArray.add(subobject);

				mainobject.put("xaxis", mainjsonArray);

				List<Object[]> aboutcatgories = migratesevice.getAllDetailsAboutCatageory();

				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				for (Object[] catageoreydetails : aboutcatgories)
				{
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();

					String catageoryname = (String) catageoreydetails[0];
					String color = (String) catageoreydetails[1];

					List<Object[]> catagaoysbookingamountoveryears = genericChartService
					        .catagaoysbookingamountoveryearsforindividualcountry(catageoryname, year, countrycode);
					HashMap<Integer, Double> data = new HashMap<Integer, Double>();
					for (Object[] yam : catagaoysbookingamountoveryears)
					{
						data.put((Integer) yam[0], (Double) yam[1]);
					}

					if (catagaoysbookingamountoveryears.size() != 0)

					{
						Set<Integer> years5 = data.keySet();

						for (Integer data1 : yearsinorder)

						{
							JSONObject data3 = new JSONObject();

							if (years5.contains(data1))
							{

								data3.put("value", (Double) data.get(data1));
								if (cggc.equalsIgnoreCase("SCA"))
								{
									data3.put("link", "#/app/SCA_GroupByProduct_YTD");

								}

								else
								{

									data3.put("link", "#/app/" + (String) countryclinks.get(countrycode) + "");
								}
								countryjsonArray.add(data3);

							}

							else
							{

								data3.put("value", null);
								countryjsonArray.add(data3);

							}

						}

					}

					aboutcountry.put("seriesname", "Booking " + catageoryname);
					aboutcountry.put("color", color);
					aboutcountry.put("data", countryjsonArray);
					countryjsonbudgetArray.add(aboutcountry);
				}

				mainobject.put("data", countryjsonbudgetArray);

				/*for table*/
				JSONArray catageroreyararray = new JSONArray();

				for (Integer tableyears : lastyears)

				{
					JSONArray catageroreyararray1 = new JSONArray();
					List<Object[]> catagoriesinpartocularyear = genericChartService
					        .getTabledataforCatagoriesonparticularcountry(tableyears, countrycode);
					for (Object[] tableproduct : catagoriesinpartocularyear)
					{
						JSONObject catageorydata = new JSONObject();

						catageorydata.put("year", tableyears);
						catageorydata.put("name", (String) tableproduct[0]);
						catageorydata.put("value", (Double) tableproduct[1]);

						catageroreyararray1.add(catageorydata);

					}

					catageroreyararray.add(catageroreyararray1);

				}

				mainobject.put("tabledataaaa", catageroreyararray);

				logger.info("sivaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa " + cggc + "sivaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa--------->");

				return mainobject;

			}

			else

			{

				HashMap countryclinks = new HashMap();
				countryclinks.put("AU", "Australia_GroupByProduct_YTD");
				countryclinks.put("Hk", "");
				countryclinks.put("IN", "India_GroupByProduct_YTD");
				countryclinks.put("ID", "Indonesia_GroupByProduct_YTD");
				countryclinks.put("MY", "Malaysia_GroupByProduct_YTD");
				countryclinks.put("OT", "Other_GroupByProduct_YTD");
				countryclinks.put("PH", "Philippines_GroupByProduct_YTD");
				countryclinks.put("SG", "Singapore_GroupByProduct_YTD");
				countryclinks.put("TH", "Thailand_GroupByProduct_YTD");
				countryclinks.put("VN", "Vietnam_GroupByProduct_YTD");
				Calendar now = Calendar.getInstance();
				int year = now.get(Calendar.YEAR);

				//System.out.println("sivaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa "+k43+"sivaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa--------->");

				List<Integer> lastyears = genericChartService.getLastYears(year);

				JSONObject mainobject = new JSONObject();
				JSONArray jsonArray = new JSONArray();
				JSONArray mainjsonArray = new JSONArray();
				ArrayList<Integer> yearsinorder = new ArrayList<Integer>();
				JSONObject months = null;
				for (Integer years : lastyears)
				{
					months = new JSONObject();
					yearsinorder.add(years);
					months.put("label", "" + years + "");
					jsonArray.add(months);
				}

				JSONObject subobject = new JSONObject();
				subobject.put("category", jsonArray);
				mainjsonArray.add(subobject);

				mainobject.put("xaxis", mainjsonArray);

				List<Object[]> aboutcatgories = migratesevice.getAllDetailsAboutCatageory();

				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				for (Object[] catageoreydetails : aboutcatgories)
				{
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();

					String catageoryname = (String) catageoreydetails[0];
					String color = (String) catageoreydetails[1];

					List<Object[]> catagaoysbookingamountoveryears = genericChartService
					        .catagaoysbookingamountoveryearsforindividualcountrydollar(catageoryname, year, countrycode);
					HashMap<Integer, Double> data = new HashMap<Integer, Double>();
					for (Object[] yam : catagaoysbookingamountoveryears)
					{
						data.put((Integer) yam[0], (Double) yam[1]);
					}

					if (catagaoysbookingamountoveryears.size() != 0)

					{
						Set<Integer> years5 = data.keySet();

						for (Integer data1 : yearsinorder)

						{
							JSONObject data3 = new JSONObject();

							if (years5.contains(data1))
							{

								data3.put("value", (Double) data.get(data1));
								if (cggc.equalsIgnoreCase("SCA"))
								{
									data3.put("link", "#/app/SCA_GroupByProduct_YTD");

								}

								else
								{

									data3.put("link", "#/app/" + (String) countryclinks.get(countrycode) + "");
								}
								countryjsonArray.add(data3);

							}

							else
							{

								data3.put("value", null);
								countryjsonArray.add(data3);

							}

						}

					}

					aboutcountry.put("seriesname", "Booking " + catageoryname);
					aboutcountry.put("color", color);
					aboutcountry.put("data", countryjsonArray);
					countryjsonbudgetArray.add(aboutcountry);
				}

				mainobject.put("data", countryjsonbudgetArray);

				/*for table*/
				JSONArray catageroreyararray = new JSONArray();

				for (Integer tableyears : lastyears)

				{
					JSONArray catageroreyararray1 = new JSONArray();
					List<Object[]> catagoriesinpartocularyear = genericChartService
					        .getTabledataforCatagoriesonparticularcountrydollar(tableyears, countrycode);
					for (Object[] tableproduct : catagoriesinpartocularyear)
					{
						JSONObject catageorydata = new JSONObject();

						catageorydata.put("year", tableyears);
						catageorydata.put("name", (String) tableproduct[0]);
						catageorydata.put("value", (Double) tableproduct[1]);

						catageroreyararray1.add(catageorydata);

					}

					catageroreyararray.add(catageroreyararray1);

				}

				mainobject.put("tabledataaaa", catageroreyararray);

				logger.info("sivaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa " + cggc + "sivaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa--------->");

				return mainobject;

			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.info("==========CONTROLLER METHOD=countryByProductInner======" + e.getCause());
			return null;
		}

	}

	@RequestMapping(value = "/countrybyproductdrilldown", method = RequestMethod.GET)
	public @ResponseBody JSONObject countryByProductInnerdrilldown(HttpServletRequest req, HttpServletResponse resp)
	{

		try
		{
			String cat = req.getParameter("catageory");
			String category = cat.substring(cat.lastIndexOf(" ") + 1);
			String countrycode = req.getParameter("countryCode");
			String id = req.getParameter("id");
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			List<Integer> lastyears = genericChartService.getLastYears(year);
			JSONObject mainobject = new JSONObject();

			if (id.equalsIgnoreCase("local"))
			{

				JSONArray jsonArray = new JSONArray();
				JSONArray mainjsonArray = new JSONArray();
				ArrayList<Integer> yearsinorder = new ArrayList<Integer>();
				JSONObject months = null;
				for (Integer years : lastyears)
				{
					months = new JSONObject();
					yearsinorder.add(years);
					months.put("label", "" + years + "");
					jsonArray.add(months);
				}

				JSONObject subobject = new JSONObject();
				subobject.put("category", jsonArray);
				mainjsonArray.add(subobject);

				mainobject.put("xaxis", mainjsonArray);

				List<Object[]> aboutcatgories = genericChartService.getAllDetailsAboutProducts(category);

				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				for (Object[] catageoreydetails : aboutcatgories)
				{
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();

					String catageoryname = (String) catageoreydetails[0];
					String color = (String) catageoreydetails[1];

					List<Object[]> catagaoysbookingamountoveryears = genericChartService
					        .catagaoysbookingamountforproductoveryearsoverIndividualCountry(catageoryname, year, countrycode,
					                category);
					HashMap<Integer, Double> data = new HashMap<Integer, Double>();
					for (Object[] yam : catagaoysbookingamountoveryears)
					{
						data.put((Integer) yam[0], (Double) yam[1]);
					}

					if (catagaoysbookingamountoveryears.size() != 0)

					{
						Set<Integer> years5 = data.keySet();

						for (Integer data1 : yearsinorder)

						{
							JSONObject data3 = new JSONObject();

							if (years5.contains(data1))
							{

								data3.put("value", (Double) data.get(data1)); /*data3.put("link",
								                                              " #/app/SCA_GroupByProduct_YTD");*/
								countryjsonArray.add(data3);

							}

							else
							{

								data3.put("value", null);
								countryjsonArray.add(data3);

							}

						}

					}

					aboutcountry.put("seriesname", "Booking " + catageoryname);
					aboutcountry.put("color", color);
					aboutcountry.put("data", countryjsonArray);
					countryjsonbudgetArray.add(aboutcountry);
				}

				mainobject.put("data", countryjsonbudgetArray);

				JSONArray catageroreyararray = new JSONArray();

				for (Integer tableyears : lastyears)

				{
					JSONArray catageroreyararray1 = new JSONArray();
					List<Object[]> catagoriesinpartocularyear = genericChartService.getTabledataforproducts(tableyears, category,
					        countrycode);
					for (Object[] tableproduct : catagoriesinpartocularyear)
					{
						JSONObject catageorydata = new JSONObject();

						catageorydata.put("year", tableyears);
						catageorydata.put("name", (String) tableproduct[0]);
						catageorydata.put("value", (Double) tableproduct[1]);

						catageroreyararray1.add(catageorydata);

					}
					catageroreyararray.add(catageroreyararray1);

				}

				mainobject.put("tabledataaaa", catageroreyararray);

				return mainobject;

			}

			if (id.equalsIgnoreCase("dollar"))
			{

				JSONArray jsonArray = new JSONArray();
				JSONArray mainjsonArray = new JSONArray();
				ArrayList<Integer> yearsinorder = new ArrayList<Integer>();
				JSONObject months = null;
				for (Integer years : lastyears)
				{
					months = new JSONObject();
					yearsinorder.add(years);
					months.put("label", "" + years + "");
					jsonArray.add(months);
				}

				JSONObject subobject = new JSONObject();
				subobject.put("category", jsonArray);
				mainjsonArray.add(subobject);

				mainobject.put("xaxis", mainjsonArray);

				List<Object[]> aboutcatgories = genericChartService.getAllDetailsAboutProducts(category);

				JSONObject aboutallcountriesbudget = new JSONObject();
				JSONArray countryjsonbudgetArray = new JSONArray();

				for (Object[] catageoreydetails : aboutcatgories)
				{
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();

					String catageoryname = (String) catageoreydetails[0];
					String color = (String) catageoreydetails[1];

					List<Object[]> catagaoysbookingamountoveryears = genericChartService
					        .catagaoysbookingamountforproductoveryearsoverIndividualCountrydollar(catageoryname, year,
					                countrycode, category);
					HashMap<Integer, Double> data = new HashMap<Integer, Double>();
					for (Object[] yam : catagaoysbookingamountoveryears)
					{
						data.put((Integer) yam[0], (Double) yam[1]);
					}

					if (catagaoysbookingamountoveryears.size() != 0)

					{
						Set<Integer> years5 = data.keySet();

						for (Integer data1 : yearsinorder)

						{
							JSONObject data3 = new JSONObject();

							if (years5.contains(data1))
							{

								data3.put("value", (Double) data.get(data1));
								/*data3.put("link", " #/app/SCA_GroupByProduct_YTD");*/
								countryjsonArray.add(data3);

							}

							else
							{

								data3.put("value", null);
								countryjsonArray.add(data3);

							}

						}

					}

					aboutcountry.put("seriesname", "Booking " + catageoryname);
					aboutcountry.put("color", color);
					aboutcountry.put("data", countryjsonArray);
					countryjsonbudgetArray.add(aboutcountry);
				}

				mainobject.put("data", countryjsonbudgetArray);

				JSONArray catageroreyararray = new JSONArray();

				for (Integer tableyears : lastyears)

				{
					JSONArray catageroreyararray1 = new JSONArray();
					List<Object[]> catagoriesinpartocularyear = genericChartService.getTabledataforproductsdollar(tableyears,
					        category, countrycode);
					for (Object[] tableproduct : catagoriesinpartocularyear)
					{
						JSONObject catageorydata = new JSONObject();

						catageorydata.put("year", tableyears);
						catageorydata.put("name", (String) tableproduct[0]);
						catageorydata.put("value", (Double) tableproduct[1]);

						catageroreyararray1.add(catageorydata);

					}
					catageroreyararray.add(catageroreyararray1);

				}

				mainobject.put("tabledataaaa", catageroreyararray);

				return mainobject;

			}

			else
			{

				return mainobject;
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.info("=============CONTROLLER METHOD=countryByProductInnerdrilldown======" + e.getCause());

			return null;
		}
	}

	@RequestMapping(value = "/clustredforcountry", method = RequestMethod.GET)
	public @ResponseBody JSONObject clustredforcountry(HttpServletRequest request, HttpServletResponse res)
	{
		try
		{
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1;
			int date = now.get(Calendar.DATE);
			int last2years = year - 2;

			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			JSONObject months = null;
			ArrayList<Integer> yearsinorder = new ArrayList<Integer>();

			for (int i = last2years; i <= year; i++)
			{
				months = new JSONObject();
				months.put("label", "" + i + "");
				yearsinorder.add(i);
				jsonArray.add(months);

			}

			JSONObject subobject = new JSONObject();
			subobject.put("category", jsonArray);
			mainjsonArray.add(subobject);

			mainobject.put("xaxis", mainjsonArray);

			JSONArray chartdata = new JSONArray();

			String cgname1 = request.getParameter("catageory");;
			String cgname = cgname1.substring(cgname1.lastIndexOf(" ") + 1);
			String countrycode = request.getParameter("countryCode");
			String id = request.getParameter("id");

			List<Object[]> aboutcatgories = genericChartService.getAllDetailsAboutProducts(cgname.trim());
			String catageorey = cgname.trim();
			if (id.equalsIgnoreCase("local"))
			{

				for (Object[] catageoreydetails : aboutcatgories)
				{
					JSONObject backlog = new JSONObject();
					JSONArray backlogarray = new JSONArray();

					String catageoryname = (String) catageoreydetails[0];
					String color = (String) catageoreydetails[1];

					List<Object[]> catagaoysbookingamountoveryears = genericChartService
					        .productbookingamountoveryearsforCountry(catageoryname, year, countrycode, catageorey);

					HashMap<Integer, Double> data = new HashMap<Integer, Double>();
					for (Object[] yam : catagaoysbookingamountoveryears)
					{
						data.put((Integer) yam[0], (Double) yam[1]);
					}

					if (catagaoysbookingamountoveryears.size() != 0)

					{
						Set<Integer> years5 = data.keySet();

						for (Integer data1 : yearsinorder)

						{
							JSONObject data3 = new JSONObject();

							if (years5.contains(data1))
							{

								data3.put("value", (Double) data.get(data1));
								backlogarray.add(data3);
								/* data3.put("link"," #/app/SCA_GroupByProduct_YTD"); */

							}

							else
							{

								data3.put("value", null);
								backlogarray.add(data3);

							}

						}

					}

					backlog.put("seriesname", catageoryname);
					backlog.put("canvasPadding", 0);
					backlog.put("data", backlogarray);
					backlog.put("color", color);
					chartdata.add(backlog);

				}

				logger.info("--------------------------------------" + cgname + "-------------------------" + countrycode);

				mainobject.put("data", chartdata);

				return mainobject;

			}

			else
			{

				for (Object[] catageoreydetails : aboutcatgories)
				{
					JSONObject backlog = new JSONObject();
					JSONArray backlogarray = new JSONArray();

					String catageoryname = (String) catageoreydetails[0];
					String color = (String) catageoreydetails[1];

					List<Object[]> catagaoysbookingamountoveryears = genericChartService
					        .productbookingamountoveryearsforCountrydolllar(catageoryname, year, countrycode, catageorey);

					HashMap<Integer, Double> data = new HashMap<Integer, Double>();
					for (Object[] yam : catagaoysbookingamountoveryears)
					{
						data.put((Integer) yam[0], (Double) yam[1]);
					}

					if (catagaoysbookingamountoveryears.size() != 0)

					{
						Set<Integer> years5 = data.keySet();

						for (Integer data1 : yearsinorder)

						{
							JSONObject data3 = new JSONObject();

							if (years5.contains(data1))
							{

								data3.put("value", (Double) data.get(data1));
								backlogarray.add(data3);
								/* data3.put("link"," #/app/SCA_GroupByProduct_YTD"); */

							}

							else
							{

								data3.put("value", null);
								backlogarray.add(data3);

							}

						}

					}

					backlog.put("seriesname", catageoryname);
					backlog.put("canvasPadding", 0);
					backlog.put("data", backlogarray);
					backlog.put("color", color);
					chartdata.add(backlog);

				}

				logger.info("--------------------------------------" + cgname + "-------------------------" + countrycode);

				mainobject.put("data", chartdata);

				return mainobject;

			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.info("============CONTROLLER METHOD=clustredforcountry" + e.getCause());
			return null;
		}

	}

	@RequestMapping(value = "/clustredforgroup", method = RequestMethod.GET)
	public @ResponseBody JSONObject clustredforgroup(HttpServletRequest request, HttpServletResponse res)
	{
		try
		{
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1;
			int date = now.get(Calendar.DATE);
			int last2years = year - 2;

			JSONObject mainobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray mainjsonArray = new JSONArray();
			JSONObject months = null;
			ArrayList<Integer> yearsinorder = new ArrayList<Integer>();

			for (int i = last2years; i <= year; i++)
			{
				months = new JSONObject();
				months.put("label", "" + i + "");
				yearsinorder.add(i);
				jsonArray.add(months);

			}

			JSONObject subobject = new JSONObject();
			subobject.put("category", jsonArray);
			mainjsonArray.add(subobject);

			mainobject.put("xaxis", mainjsonArray);

			JSONArray chartdata = new JSONArray();

			String cgname1 = request.getParameter("catageory");
			String cgname = cgname1.substring(cgname1.lastIndexOf(" ") + 1);

			List<Object[]> aboutcatgories = genericChartService.getAllDetailsAboutProducts(cgname);

			for (Object[] catageoreydetails : aboutcatgories)
			{
				JSONObject backlog = new JSONObject();
				JSONArray backlogarray = new JSONArray();

				String catageoryname = (String) catageoreydetails[0];
				String color = (String) catageoreydetails[1];

				List<Object[]> catagaoysbookingamountoveryears = genericChartService.productbookingamountoveryears(catageoryname,
				        year);

				HashMap<Integer, Double> data = new HashMap<Integer, Double>();
				for (Object[] yam : catagaoysbookingamountoveryears)
				{
					data.put((Integer) yam[0], (Double) yam[1]);
				}

				if (catagaoysbookingamountoveryears.size() != 0)

				{
					Set<Integer> years5 = data.keySet();

					for (Integer data1 : yearsinorder)

					{
						JSONObject data3 = new JSONObject();

						if (years5.contains(data1))
						{

							data3.put("value", (Double) data.get(data1));
							backlogarray.add(data3);
							/* data3.put("link"," #/app/SCA_GroupByProduct_YTD"); */

						}

						else
						{

							data3.put("value", null);
							backlogarray.add(data3);

						}

					}

				}

				backlog.put("seriesname", catageoryname);
				backlog.put("canvasPadding", 0);
				backlog.put("data", backlogarray);
				backlog.put("color", color);
				chartdata.add(backlog);

			}

			mainobject.put("data", chartdata);

			return mainobject;
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.info("==========Controller METHOD=clustredforgroup===" + e.getCause());
			return null;
		}

	}

	/*@RequestMapping(value = "/ytdcodeOnCountryWise", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   ytdcode(HttpServletRequest request,HttpServletResponse response) 
	{
		String countrycode=request.getParameter("coutrycode");
		
		String id=request.getParameter("id");
		
		if(id.equals("local"))
		{
		
		ArrayList<String> al=new ArrayList<String>();
		ArrayList<String> al2=new ArrayList<String>();
		JSONObject keyforyears = null;
		JSONObject keyforyears1 = new JSONObject();
		JSONArray jsonArrayofvaluesbasedonyeras = null;
		int lastyear=0;
		JSONArray seriesobjArray = new JSONArray();
		HashMap<String,String> colorMap=new HashMap<String,String>();
		DecimalFormat df = new DecimalFormat("#.##");
		 //String budget_valueStr=df.format(budget_value2016);
		JSONObject categoryJson = null;
		JSONArray categoryArray = null;
		
		
		HashMap countrycurrenceyvalue=new HashMap();
		countrycurrenceyvalue.put("AU",1.34);
		countrycurrenceyvalue.put("Hk",6.68 );
		countrycurrenceyvalue.put("IN",67.20 );
		countrycurrenceyvalue.put("ID",13100.00 );
	   countrycurrenceyvalue.put("MY",4.07);
		countrycurrenceyvalue.put("OT",1.00);
		countrycurrenceyvalue.put("PH",47.18);
		countrycurrenceyvalue.put("SG",1.36);
		countrycurrenceyvalue.put("TH",35.01);
		countrycurrenceyvalue.put("VN",22301.52);
	
		HashMap monthname=new HashMap();
		monthname.put(1,"Jan");
		monthname.put(2,"Feb");
		monthname.put(3,"Mar");
		monthname.put(4,"Apr");
		monthname.put(5,"May");
		monthname.put(6,"Jun");
		monthname.put(7,"Jul");
		monthname.put(8,"Aug");
		monthname.put(9,"Sep");
		monthname.put(10,"Oct");
		monthname.put(11,"Nov");
		monthname.put(12,"Dec");
		
		Calendar now = Calendar.getInstance();
	    int currMonth=now.get(Calendar.MONTH) + 1;
		String currMonthname=(String)monthname.get(currMonth);
		List<Integer> years=migratesevice.gettingBookingBudgetYears();
	
		if (!years.isEmpty()) 
		{
			lastyear = years.get(years.size() - 1);
		}
		List<Object[]> coutries=genericChartService.getCountriesDtails(countrycode);
		
		for(Object[] countrydetails:coutries)
		{
			String countryname=(String)countrydetails[0];
			String color=(String)countrydetails[2];
			al.add(countryname);
			al2.add(color);
			colorMap.put(countryname, color);
		}	 
		
		for(Object[] countrydetails:coutries)
		{	
			categoryJson = new JSONObject();
			categoryArray = new JSONArray();
			String countryname=(String)countrydetails[0];
			
			keyforyears = new JSONObject();
			jsonArrayofvaluesbasedonyeras = new JSONArray();
			keyforyears.put("seriesname",countryname);
			keyforyears.put("color",colorMap.get(countryname));
			float budget_value=0;
			for(Integer year:years)		
			{
				JSONObject allvaluesinyears = new JSONObject();
				if(lastyear==year)
				{
					//categoryJson.put("label", year+"\n(Budget till "+currMonthname+")");	    	
					categoryJson.put("label", year+"\n(Budget )");
					categoryArray.add(categoryJson);
					
					allvaluesinyears.put("name","Budget of "+year);
					Double value=(Double)countrycurrenceyvalue.get(countrycode);
					budget_value=migratesevice.getBudgetValue(countryname,lastyear);
					 //String budget_valueStr=df.format(budget_value);
					 
	
					 allvaluesinyears.put("value",Math.round(budget_value*value*100.0)/100.0);
					 allvaluesinyears.put("color",colorMap.get(countryname));
					 allvaluesinyears.put("alpha","60");
					 jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);
				}	
				
				allvaluesinyears.remove("alpha");
				categoryJson.put("label", year.toString());	    	
				categoryArray.add(categoryJson);
				
				allvaluesinyears.put("name","Booking of "+year);
				
				Double booking_value=migratesevice.getBookingValue(countryname,year);
				allvaluesinyears.put("value",Math.round(booking_value*100.0)/100.0);
				allvaluesinyears.put("color",colorMap.get(countryname));
				if(lastyear==year)
				{
					allvaluesinyears.put("tooltext","$label ,$seriesname,$datavalue <br>"+(df.format((booking_value/budget_value)*100))+"% of Budget");
				}
				jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);
	
			}
			keyforyears.put("data",jsonArrayofvaluesbasedonyeras);
			seriesobjArray.add(keyforyears);
		}
		
		keyforyears1.put("category",categoryArray);		
		keyforyears1.put("dataset",seriesobjArray);	
	
		return keyforyears1;
	
	}
		
		else
		{
			
	
			
			ArrayList<String> al=new ArrayList<String>();
			ArrayList<String> al2=new ArrayList<String>();
			JSONObject keyforyears = null;
			JSONObject keyforyears1 = new JSONObject();
			JSONArray jsonArrayofvaluesbasedonyeras = null;
			int lastyear=0;
			JSONArray seriesobjArray = new JSONArray();
			HashMap<String,String> colorMap=new HashMap<String,String>();
			DecimalFormat df = new DecimalFormat("#.##");
			 //String budget_valueStr=df.format(budget_value2016);
			JSONObject categoryJson = null;
			JSONArray categoryArray = null;
			
			
			HashMap countrycurrenceyvalue=new HashMap();
			countrycurrenceyvalue.put("AU",1.34);
			countrycurrenceyvalue.put("Hk",6.68 );
			countrycurrenceyvalue.put("IN",67.20 );
			countrycurrenceyvalue.put("ID",13100.00 );
	       countrycurrenceyvalue.put("MY",4.07);
			countrycurrenceyvalue.put("OT",1.00);
			countrycurrenceyvalue.put("PH",47.18);
			countrycurrenceyvalue.put("SG",1.36);
			countrycurrenceyvalue.put("TH",35.01);
			countrycurrenceyvalue.put("VN",22301.52);
		
			HashMap monthname=new HashMap();
			monthname.put(1,"Jan");
			monthname.put(2,"Feb");
			monthname.put(3,"Mar");
			monthname.put(4,"Apr");
			monthname.put(5,"May");
			monthname.put(6,"Jun");
			monthname.put(7,"Jul");
			monthname.put(8,"Aug");
			monthname.put(9,"Sep");
			monthname.put(10,"Oct");
			monthname.put(11,"Nov");
			monthname.put(12,"Dec");
			
			Calendar now = Calendar.getInstance();
		    int currMonth=now.get(Calendar.MONTH) + 1;
			String currMonthname=(String)monthname.get(currMonth);
			List<Integer> years=migratesevice.gettingBookingBudgetYears();
	
			if (!years.isEmpty()) 
			{
				lastyear = years.get(years.size() - 1);
			}
			List<Object[]> coutries=genericChartService.getCountriesDtails(countrycode);
			
			for(Object[] countrydetails:coutries)
			{
				String countryname=(String)countrydetails[0];
				String color=(String)countrydetails[2];
				al.add(countryname);
				al2.add(color);
				colorMap.put(countryname, color);
			}	 
			
			for(Object[] countrydetails:coutries)
			{	
				categoryJson = new JSONObject();
				categoryArray = new JSONArray();
				String countryname=(String)countrydetails[0];
				
				keyforyears = new JSONObject();
				jsonArrayofvaluesbasedonyeras = new JSONArray();
				keyforyears.put("seriesname",countryname);
				keyforyears.put("color",colorMap.get(countryname));
				float budget_value=0;
				for(Integer year:years)		
				{
					JSONObject allvaluesinyears = new JSONObject();
					if(lastyear==year)
					{
						//categoryJson.put("label", year+"\n(Budget till "+currMonthname+")");	    	
						categoryJson.put("label", year+"\n(Budget )");
						categoryArray.add(categoryJson);
						
						allvaluesinyears.put("name","Budget of "+year);
						Double value=(Double)countrycurrenceyvalue.get(countrycode);
						budget_value=migratesevice.getBudgetValue(countryname,lastyear);
						 //String budget_valueStr=df.format(budget_value);
						 
	
						 allvaluesinyears.put("value",Math.round(budget_value*value*100.0)/100.0);
						 allvaluesinyears.put("color",colorMap.get(countryname));
						 allvaluesinyears.put("alpha","60");
						 jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);
					}	
					
					allvaluesinyears.remove("alpha");
					categoryJson.put("label", year.toString());	    	
					categoryArray.add(categoryJson);
					
					allvaluesinyears.put("name","Booking of "+year);
					
					Double booking_value=migratesevice.getBookingValue(countryname,year);
					allvaluesinyears.put("value",Math.round(booking_value*100.0)/100.0);
					allvaluesinyears.put("color",colorMap.get(countryname));
					if(lastyear==year)
					{
						allvaluesinyears.put("tooltext","$label ,$seriesname,$datavalue <br>"+(df.format((booking_value/budget_value)*100))+"% of Budget");
					}
					jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);
	
				}
				keyforyears.put("data",jsonArrayofvaluesbasedonyeras);
				seriesobjArray.add(keyforyears);
			}
			
			keyforyears1.put("category",categoryArray);		
			keyforyears1.put("dataset",seriesobjArray);	
	
			return keyforyears1;
		
		
			
		}
	}
	
	
	*/

	@RequestMapping(value = "/ytdcodeOnCountryWise", method = RequestMethod.GET)
	public @ResponseBody JSONObject ytdcodeincountrywisegeneric(HttpServletRequest request, HttpServletResponse response)
	{

		String countrycode = request.getParameter("countryCode");

		String id = request.getParameter("id");

		if (id.equals("local"))
		{

			ArrayList<String> al = new ArrayList<String>();
			ArrayList<String> al2 = new ArrayList<String>();
			JSONObject keyforyears = null;
			JSONObject keyforyears1 = new JSONObject();
			JSONArray jsonArrayofvaluesbasedonyeras = null;
			int lastyear = 0;
			JSONArray seriesobjArray = new JSONArray();
			HashMap<String, String> colorMap = new HashMap<String, String>();
			HashMap<String, String> budgetcolorMap = new HashMap<String, String>();
			DecimalFormat df = new DecimalFormat("#.##");
			//String budget_valueStr=df.format(budget_value2016);
			JSONObject categoryJson = null;
			JSONArray categoryArray = null;

			/*
			HashMap countrycurrenceyvalue=new HashMap();
			countrycurrenceyvalue.put("AU",1.34);
			countrycurrenceyvalue.put("Hk",6.68 );
			countrycurrenceyvalue.put("IN",67.20 );
			countrycurrenceyvalue.put("ID",13100.00 );
			countrycurrenceyvalue.put("MY",4.07);
			countrycurrenceyvalue.put("OT",1.00);
			countrycurrenceyvalue.put("PH",47.18);
			countrycurrenceyvalue.put("SG",1.36);
			countrycurrenceyvalue.put("TH",35.01);
			countrycurrenceyvalue.put("VN",22301.52);
			*/
			HashMap countrycurrenceyvalue = new HashMap();
			countrycurrenceyvalue.put("AU", 1.371);
			countrycurrenceyvalue.put("Hk", 6.68);
			countrycurrenceyvalue.put("IN", 66.4);
			countrycurrenceyvalue.put("ID", 13736.26);
			countrycurrenceyvalue.put("MY", 4.288);
			countrycurrenceyvalue.put("OT", 1.00);
			countrycurrenceyvalue.put("PH", 47.0);
			countrycurrenceyvalue.put("SG", 1.41);
			countrycurrenceyvalue.put("TH", 35.97);
			countrycurrenceyvalue.put("VN", 22522.52);
			countrycurrenceyvalue.put("EMA", 1.00);
			countrycurrenceyvalue.put("INT", 1.00);

			HashMap monthname = new HashMap();
			monthname.put(1, "Jan");
			monthname.put(2, "Feb");
			monthname.put(3, "Mar");
			monthname.put(4, "Apr");
			monthname.put(5, "May");
			monthname.put(6, "Jun");
			monthname.put(7, "Jul");
			monthname.put(8, "Aug");
			monthname.put(9, "Sep");
			monthname.put(10, "Oct");
			monthname.put(11, "Nov");
			monthname.put(12, "Dec");

			Calendar now = Calendar.getInstance();
			int currMonth = now.get(Calendar.MONTH) + 1;
			String currMonthname = (String) monthname.get(currMonth);
			List<Integer> years = migratesevice.gettingBookingBudgetYears();

			if (!years.isEmpty())
			{
				lastyear = years.get(years.size() - 1);
			}
			List<Object[]> coutries = genericChartService.getCountriesDtails(countrycode);

			for (Object[] countrydetails : coutries)
			{
				String countryname = (String) countrydetails[0];
				String color = (String) countrydetails[2];
				String budgetcolor = (String) countrydetails[1];
				al.add(countryname);
				al2.add(color);
				colorMap.put(countryname, color);
				budgetcolorMap.put(countryname, budgetcolor);
			}
			ArrayList<Double> budgetvalesinpercentage = new ArrayList<Double>();
			ArrayList<Double> bookingsinpercentage = new ArrayList<Double>();
			for (Object[] countrydetails : coutries)
			{
				categoryJson = new JSONObject();
				categoryArray = new JSONArray();
				String countryname = (String) countrydetails[0];

				keyforyears = new JSONObject();
				jsonArrayofvaluesbasedonyeras = new JSONArray();
				keyforyears.put("seriesname", countryname);
				keyforyears.put("color", colorMap.get(countryname));
				Double budget_value = 0.0;
				for (Integer year : years)
				{
					JSONObject allvaluesinyears = new JSONObject();
					if (lastyear == year)
					{
						//categoryJson.put("label", year+"\n(Budget till "+currMonthname+")");	    	
						categoryJson.put("label", year + "\n(Budget )");
						categoryArray.add(categoryJson);

						allvaluesinyears.put("name", "Budget of " + year);
						Double value = (Double) countrycurrenceyvalue.get(countrycode);
						budget_value = migratesevice.getBudgetValue(countryname, lastyear);
						//String budget_valueStr=df.format(budget_value);
						/* for calculating percentages*/
						Double percentbugdgetvalue = 0.0;
						if (budget_value != null)
						{
							percentbugdgetvalue = budget_value * value;
						}
						budgetvalesinpercentage.add(percentbugdgetvalue);

						/*****************************************/
						if (budget_value != null)
						{
							allvaluesinyears.put("value", budget_value * value);
						}
						else
						{
							allvaluesinyears.put("value", 0.0 * value);
						}
						allvaluesinyears.put("color", budgetcolorMap.get(countryname));
						//allvaluesinyears.put("alpha","60");
						jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);
					}

					allvaluesinyears.remove("alpha");
					categoryJson.put("label", year.toString());
					categoryArray.add(categoryJson);

					allvaluesinyears.put("name", "Booking of " + year);

					Double booking_value = migratesevice.getBookingValuelocal(countryname, year);
					allvaluesinyears.put("value", booking_value);
					allvaluesinyears.put("color", colorMap.get(countryname));
					if (lastyear == year)
					{
						bookingsinpercentage.add(booking_value);
						if (booking_value != null && budget_value != null)
						{
							allvaluesinyears.put("tooltext", "$label ,$seriesname,$datavalue <br>"
							        + (df.format((booking_value / budget_value) * 100)) + "% of Budget");
						}
					}
					jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);

				}
				keyforyears.put("data", jsonArrayofvaluesbasedonyeras);
				seriesobjArray.add(keyforyears);
			}

			keyforyears1.put("category", categoryArray);
			keyforyears1.put("dataset", seriesobjArray);

			ArrayList<Double> percentageforcountries = new ArrayList<Double>();
			Double d = 0.0;
			Double k = 0.0;
			if (lastyear > 0)
			{
				if (bookingsinpercentage.get(0) > 0.0 && budgetvalesinpercentage.get(0) > 0.0)
				{
					d = bookingsinpercentage.get(0) / budgetvalesinpercentage.get(0);
					k = d * 100;
				}

				if (k > 0)
				{
					percentageforcountries.add(k);
				}

				else
				{

					percentageforcountries.add(0.0);
				}

			}
			keyforyears1.put("per", percentageforcountries);

			return keyforyears1;

		}

		else
		{

			ArrayList<String> al = new ArrayList<String>();
			ArrayList<String> al2 = new ArrayList<String>();
			JSONObject keyforyears = null;
			JSONObject keyforyears1 = new JSONObject();
			JSONArray jsonArrayofvaluesbasedonyeras = null;
			int lastyear = 0;
			JSONArray seriesobjArray = new JSONArray();
			HashMap<String, String> colorMap = new HashMap<String, String>();
			HashMap<String, String> budgetcolorMap = new HashMap<String, String>();
			DecimalFormat df = new DecimalFormat("#.##");
			//String budget_valueStr=df.format(budget_value2016);
			JSONObject categoryJson = null;
			JSONArray categoryArray = null;

			/*HashMap countrycurrenceyvalue=new HashMap();
			countrycurrenceyvalue.put("AU",1.34);
			countrycurrenceyvalue.put("Hk",6.68 );
			countrycurrenceyvalue.put("IN",67.20 );
			countrycurrenceyvalue.put("ID",13100.00 );
			countrycurrenceyvalue.put("MY",4.07);
			countrycurrenceyvalue.put("OT",1.00);
			countrycurrenceyvalue.put("PH",47.18);
			countrycurrenceyvalue.put("SG",1.36);
			countrycurrenceyvalue.put("TH",35.01);
			countrycurrenceyvalue.put("VN",22301.52);*/

			HashMap countrycurrenceyvalue = new HashMap();
			countrycurrenceyvalue.put("AU", 1.371);
			countrycurrenceyvalue.put("Hk", 6.68);
			countrycurrenceyvalue.put("IN", 66.4);
			countrycurrenceyvalue.put("ID", 13736.26);
			countrycurrenceyvalue.put("MY", 4.288);
			countrycurrenceyvalue.put("OT", 1.00);
			countrycurrenceyvalue.put("PH", 47.0);
			countrycurrenceyvalue.put("SG", 1.41);
			countrycurrenceyvalue.put("TH", 35.97);
			countrycurrenceyvalue.put("VN", 22522.52);

			HashMap monthname = new HashMap();
			monthname.put(1, "Jan");
			monthname.put(2, "Feb");
			monthname.put(3, "Mar");
			monthname.put(4, "Apr");
			monthname.put(5, "May");
			monthname.put(6, "Jun");
			monthname.put(7, "Jul");
			monthname.put(8, "Aug");
			monthname.put(9, "Sep");
			monthname.put(10, "Oct");
			monthname.put(11, "Nov");
			monthname.put(12, "Dec");

			Calendar now = Calendar.getInstance();
			int currMonth = now.get(Calendar.MONTH) + 1;
			String currMonthname = (String) monthname.get(currMonth);
			List<Integer> years = migratesevice.gettingBookingBudgetYears();

			if (!years.isEmpty())
			{
				lastyear = years.get(years.size() - 1);
			}
			List<Object[]> coutries = genericChartService.getCountriesDtails(countrycode);
			ArrayList<Double> budgetvalesinpercentage = new ArrayList<Double>();
			ArrayList<Double> bookingsinpercentage = new ArrayList<Double>();

			for (Object[] countrydetails : coutries)
			{
				String countryname = (String) countrydetails[0];
				String color = (String) countrydetails[2];
				String budgetcolor = (String) countrydetails[1];
				al.add(countryname);
				al2.add(color);
				colorMap.put(countryname, color);
				budgetcolorMap.put(countryname, budgetcolor);
			}

			for (Object[] countrydetails : coutries)
			{
				categoryJson = new JSONObject();
				categoryArray = new JSONArray();
				String countryname = (String) countrydetails[0];

				keyforyears = new JSONObject();
				jsonArrayofvaluesbasedonyeras = new JSONArray();
				keyforyears.put("seriesname", countryname);
				keyforyears.put("color", colorMap.get(countryname));
				Double budget_value = 0.0;
				for (Integer year : years)
				{
					JSONObject allvaluesinyears = new JSONObject();
					if (lastyear == year)
					{
						//categoryJson.put("label", year+"\n(Budget till "+currMonthname+")");	    	
						categoryJson.put("label", year + "\n(Budget )");
						categoryArray.add(categoryJson);

						allvaluesinyears.put("name", "Budget of " + year);
						Double value = (Double) countrycurrenceyvalue.get(countrycode);
						budget_value = migratesevice.getBudgetValue(countryname, lastyear);
						//String budget_valueStr=df.format(budget_value);

						budgetvalesinpercentage.add(budget_value);
						allvaluesinyears.put("value", budget_value);
						allvaluesinyears.put("color", budgetcolorMap.get(countryname));
						//allvaluesinyears.put("alpha","60");
						jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);
					}

					allvaluesinyears.remove("alpha");
					categoryJson.put("label", year.toString());
					categoryArray.add(categoryJson);

					allvaluesinyears.put("name", "Booking of " + year);

					Double booking_value = migratesevice.getBookingValue(countryname, year);
					allvaluesinyears.put("value", booking_value);
					allvaluesinyears.put("color", colorMap.get(countryname));
					if (lastyear == year)
					{
						bookingsinpercentage.add(booking_value);
						if (booking_value != null && budget_value != null)
						{
							allvaluesinyears.put("tooltext", "$label ,$seriesname,$datavalue <br>"
							        + (df.format((booking_value / budget_value) * 100)) + "% of Budget");
						}
					}
					jsonArrayofvaluesbasedonyeras.add(allvaluesinyears);

				}
				keyforyears.put("data", jsonArrayofvaluesbasedonyeras);
				seriesobjArray.add(keyforyears);
			}

			keyforyears1.put("category", categoryArray);
			keyforyears1.put("dataset", seriesobjArray);
			ArrayList<Double> percentageforcountry = new ArrayList<Double>();
			Double d = 0.0;
			if (lastyear > 0)
			{
				if (bookingsinpercentage.get(0) > 0.0 && budgetvalesinpercentage.get(0) > 0.0)
				{
					d = bookingsinpercentage.get(0) / budgetvalesinpercentage.get(0);

				}
				Double k = d * 100;

				if (k > 0.0)
				{
					percentageforcountry.add(k);
				}

				else
				{

					percentageforcountry.add(0.0);
				}

				//percentageforcountry.add(k);
				keyforyears1.put("per", percentageforcountry);

			}

			return keyforyears1;

		}

	}

	@RequestMapping(value = "/cwQwarterlyBookingCountryWise", method = RequestMethod.GET)
	public @ResponseBody JSONObject cwQwarterlyBooking(HttpServletResponse res, HttpServletRequest req)
	{
		try
		{
			String countrycode = req.getParameter("countryCode");
			String id = req.getParameter("id");
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1;
			int date = now.get(Calendar.DATE);
			int last4years = year - 3;

			JSONObject mainobject = new JSONObject();

			if (id.equalsIgnoreCase("local"))
			{
				JSONArray jsonArray = new JSONArray();
				DecimalFormat df = new DecimalFormat("#.##");
				String quarterly = null;
				JSONObject series1 = new JSONObject();
				JSONObject series2 = new JSONObject();
				JSONObject series3 = new JSONObject();
				JSONObject series4 = new JSONObject();

				JSONObject sdata = null;
				JSONObject sdata1 = new JSONObject();
				JSONObject sdata2 = new JSONObject();
				JSONObject sdata3 = new JSONObject();
				JSONObject sdata4 = new JSONObject();

				JSONArray sArray1 = new JSONArray();
				JSONArray sArray2 = new JSONArray();
				JSONArray sArray3 = new JSONArray();
				JSONArray sArray4 = new JSONArray();
				ArrayList<Integer> yeras = new ArrayList<Integer>();
				for (int j = last4years; j <= year; j++)

				{
					yeras.add(j);

				}

				series1.put("seriesname", "" + (Integer) yeras.get(0) + "");
				series1.put("color", "008ee4");

				series2.put("seriesname", "" + (Integer) yeras.get(1) + "");
				series2.put("color", "f8bd19");

				series3.put("seriesname", "" + (Integer) yeras.get(2) + "");
				series3.put("color", "6baa01");

				series4.put("seriesname", "" + (Integer) yeras.get(3) + "");
				series4.put("color", "e44a00");

				for (int i = last4years; i <= year; i++)
				{
					sdata = new JSONObject();
					if (i == (Integer) yeras.get(0))
					{
						quarterly = "1,2,3";
						Double bookingValue1 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue1);
						sArray1.add(sdata);

						quarterly = "4,5,6";
						Double bookingValue2 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue2);
						sArray1.add(sdata);

						quarterly = "7,8,9";
						Double bookingValue3 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue3);
						sArray1.add(sdata);

						quarterly = "10,11,12";
						Double bookingValue4 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue4);
						sArray1.add(sdata);

					}
					else if (i == (Integer) yeras.get(1))
					{
						quarterly = "1,2,3";
						Double bookingValue1 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue1);
						sArray2.add(sdata);

						quarterly = "4,5,6";
						Double bookingValue2 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue2);
						sArray2.add(sdata);

						quarterly = "7,8,9";
						Double bookingValue3 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue3);
						sArray2.add(sdata);

						quarterly = "10,11,12";
						Double bookingValue4 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue4);
						sArray2.add(sdata);
					}
					else if (i == (Integer) yeras.get(2))
					{
						quarterly = "1,2,3";
						Double bookingValue1 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue1);
						sArray3.add(sdata);

						quarterly = "4,5,6";
						Double bookingValue2 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata3.put("value", bookingValue2);
						sArray3.add(sdata);

						quarterly = "7,8,9";
						Double bookingValue3 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue3);
						sArray3.add(sdata);

						quarterly = "10,11,12";
						Double bookingValue4 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue4);
						sArray3.add(sdata);
					}
					else
					{
						quarterly = "1,2,3";
						Double bookingValue1 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue1);
						sArray4.add(sdata);

						quarterly = "4,5,6";
						Double bookingValue2 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue2);
						sArray4.add(sdata);

						quarterly = "7,8,9";
						Double bookingValue3 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue3);
						sArray4.add(sdata);

						quarterly = "10,11,12";
						Double bookingValue4 = genericChartService.getCWQuarterlyBookingValue(quarterly, i, countrycode);
						sdata.put("value", bookingValue4);
						sArray4.add(sdata);
					}
				}

				series1.put("data", sArray1);
				series2.put("data", sArray2);
				series3.put("data", sArray3);
				series4.put("data", sArray4);
				jsonArray.add(series1);
				jsonArray.add(series2);
				jsonArray.add(series3);
				jsonArray.add(series4);

				mainobject.put("dataset", jsonArray);
				return mainobject;

			}

			else
			{

				JSONArray jsonArray = new JSONArray();
				DecimalFormat df = new DecimalFormat("#.##");
				String quarterly = null;
				JSONObject series1 = new JSONObject();
				JSONObject series2 = new JSONObject();
				JSONObject series3 = new JSONObject();
				JSONObject series4 = new JSONObject();

				JSONObject sdata = null;
				JSONObject sdata1 = new JSONObject();
				JSONObject sdata2 = new JSONObject();
				JSONObject sdata3 = new JSONObject();
				JSONObject sdata4 = new JSONObject();

				JSONArray sArray1 = new JSONArray();
				JSONArray sArray2 = new JSONArray();
				JSONArray sArray3 = new JSONArray();
				JSONArray sArray4 = new JSONArray();
				ArrayList<Integer> yeras = new ArrayList<Integer>();
				for (int j = last4years; j <= year; j++)

				{
					yeras.add(j);

				}

				series1.put("seriesname", "" + (Integer) yeras.get(0) + "");
				series1.put("color", "008ee4");

				series2.put("seriesname", "" + (Integer) yeras.get(1) + "");
				series2.put("color", "f8bd19");

				series3.put("seriesname", "" + (Integer) yeras.get(2) + "");
				series3.put("color", "6baa01");

				series4.put("seriesname", "" + (Integer) yeras.get(3) + "");
				series4.put("color", "e44a00");

				for (int i = last4years; i <= year; i++)
				{
					sdata = new JSONObject();
					if (i == (Integer) yeras.get(0))
					{
						quarterly = "1,2,3";
						Double bookingValue1 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue1);
						sArray1.add(sdata);

						quarterly = "4,5,6";
						Double bookingValue2 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue2);
						sArray1.add(sdata);

						quarterly = "7,8,9";
						Double bookingValue3 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue3);
						sArray1.add(sdata);

						quarterly = "10,11,12";
						Double bookingValue4 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue4);
						sArray1.add(sdata);

					}
					else if (i == (Integer) yeras.get(1))
					{
						quarterly = "1,2,3";
						Double bookingValue1 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue1);
						sArray2.add(sdata);

						quarterly = "4,5,6";
						Double bookingValue2 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue2);
						sArray2.add(sdata);

						quarterly = "7,8,9";
						Double bookingValue3 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue3);
						sArray2.add(sdata);

						quarterly = "10,11,12";
						Double bookingValue4 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue4);
						sArray2.add(sdata);
					}
					else if (i == (Integer) yeras.get(2))
					{
						quarterly = "1,2,3";
						Double bookingValue1 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue1);
						sArray3.add(sdata);

						quarterly = "4,5,6";
						Double bookingValue2 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata3.put("value", bookingValue2);
						sArray3.add(sdata);

						quarterly = "7,8,9";
						Double bookingValue3 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue3);
						sArray3.add(sdata);

						quarterly = "10,11,12";
						Double bookingValue4 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue4);
						sArray3.add(sdata);
					}
					else
					{
						quarterly = "1,2,3";
						Double bookingValue1 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue1);
						sArray4.add(sdata);

						quarterly = "4,5,6";
						Double bookingValue2 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue2);
						sArray4.add(sdata);

						quarterly = "7,8,9";
						Double bookingValue3 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue3);
						sArray4.add(sdata);

						quarterly = "10,11,12";
						Double bookingValue4 = genericChartService.getCWQuarterlyBookingValuedollar(quarterly, i, countrycode);
						sdata.put("value", bookingValue4);
						sArray4.add(sdata);
					}
				}

				series1.put("data", sArray1);
				series2.put("data", sArray2);
				series3.put("data", sArray3);
				series4.put("data", sArray4);
				jsonArray.add(series1);
				jsonArray.add(series2);
				jsonArray.add(series3);
				jsonArray.add(series4);

				mainobject.put("dataset", jsonArray);
				return mainobject;

			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("===CONTROLLER METHOD==cwQwarterlyBooking====" + e.getCause());
			return null;
		}
	}

	@RequestMapping(value = "/profitProductsforcountry", method = RequestMethod.GET)
	public @ResponseBody JSONObject profitProductsoncountry(HttpServletRequest request, HttpServletResponse res)
	{

		try
		{
			String cggc = request.getParameter("group");
			String id = request.getParameter("id");
			String countrycode = request.getParameter("countryCode");

			HashMap countryclinks = new HashMap();
			countryclinks.put("AU", "Australia_profitProducts");
			countryclinks.put("Hk", "");
			countryclinks.put("IN", "India_profitProducts");
			countryclinks.put("ID", "Indonesia_profitProducts");
			countryclinks.put("MY", "Malaysia_profitProducts");
			countryclinks.put("OT", "Other_profitProducts");
			countryclinks.put("PH", "Philippines_profitProducts");
			countryclinks.put("SG", "Singapore_profitProducts");
			countryclinks.put("TH", "Thailand_profitProducts");
			countryclinks.put("VN", "Vietnam_profitProducts");

			if (id.equalsIgnoreCase("local"))
			{

				JSONObject keyforyears = new JSONObject();

				List<Object[]> countries = genericChartService.getCountriesNameColor(countrycode);

				JSONArray vendorarray = new JSONArray();

				for (Object[] country : countries)
				{
					JSONObject vendorobject = new JSONObject();
					vendorobject.put("label", (String) country[0]);
					vendorarray.add(vendorobject);

				}
				JSONObject vendorcatgories = new JSONObject();
				vendorcatgories.put("category", vendorarray);
				JSONArray vendorvatageoreyarray = new JSONArray();
				vendorvatageoreyarray.add(vendorcatgories);
				keyforyears.put("countries", vendorvatageoreyarray);

				JSONArray countryjsonArraywithcatageroey = new JSONArray();
				List<Object[]> aboutcatgories = migratesevice.getAllDetailsAboutCatageory();
				for (Object[] catageoreydetails : aboutcatgories)
				{
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();

					String catageoryname = (String) catageoreydetails[0];
					String color = (String) catageoreydetails[1];

					List<Object[]> countriesbookingsoncatagorey = migratesevice
					        .getcountriesbookingsoncatagoreylocal(catageoryname);
					HashMap<String, Double> a = null;

					a = new HashMap<String, Double>();

					for (Object[] countriesbookings : countriesbookingsoncatagorey)
					{

						a.put((String) countriesbookings[0], (Double) countriesbookings[1]);
					}

					if (countriesbookingsoncatagorey.size() != 0)
					{
						Set sbookingcountries = a.keySet();

						logger.info("the values in the key set are" + sbookingcountries);

						for (Object[] country : countries)
						{

							logger.info("the country is follwing ------------------->" + (String) country[0]);

							if (sbookingcountries.contains(((String) country[0])))
							{

								JSONObject ebc = new JSONObject();
								Double d = (Double) a.get(((String) country[0]));
								logger.info(d + "-------------------------------------------->"
								        + ((String) country[0]).toUpperCase());
								ebc.put("value", d);
								if (cggc.equalsIgnoreCase("SCA"))
								{
									ebc.put("link", "#/app/SCA_profitProducts");
								}
								else
								{
									ebc.put("link", "#/app/" + (String) countryclinks.get(countrycode) + "");

								}
								countryjsonArray.add(ebc);
							}

							/*else {
							
								JSONObject ebc = new JSONObject();
								Double d = (Double) a.get(((String) country[0]).toUpperCase());
								logger.info(d + "-------------------------------------------->"
										+ ((String) country[0]).toUpperCase());
								ebc.put("value", null);
								ebc.put("link", "#/app/SCA_profitProducts");
								countryjsonArray.add(ebc);
							}*/
						}

					}

					aboutcountry.put("seriesname", catageoryname);
					aboutcountry.put("color", color);
					aboutcountry.put("data", countryjsonArray);
					countryjsonArraywithcatageroey.add(aboutcountry);
				}

				keyforyears.put("data", countryjsonArraywithcatageroey);

				return keyforyears;

			}

			else
			{

				JSONObject keyforyears = new JSONObject();

				List<Object[]> countries = genericChartService.getCountriesNameColor(countrycode);

				JSONArray vendorarray = new JSONArray();

				for (Object[] country : countries)
				{
					JSONObject vendorobject = new JSONObject();
					vendorobject.put("label", (String) country[0]);
					vendorarray.add(vendorobject);

				}
				JSONObject vendorcatgories = new JSONObject();
				vendorcatgories.put("category", vendorarray);
				JSONArray vendorvatageoreyarray = new JSONArray();
				vendorvatageoreyarray.add(vendorcatgories);
				keyforyears.put("countries", vendorvatageoreyarray);

				JSONArray countryjsonArraywithcatageroey = new JSONArray();
				List<Object[]> aboutcatgories = migratesevice.getAllDetailsAboutCatageory();
				for (Object[] catageoreydetails : aboutcatgories)
				{
					JSONObject aboutcountry = new JSONObject();
					JSONArray countryjsonArray = new JSONArray();

					String catageoryname = (String) catageoreydetails[0];
					String color = (String) catageoreydetails[1];

					List<Object[]> countriesbookingsoncatagorey = migratesevice.getcountriesbookingsoncatagorey(catageoryname);
					HashMap<String, Double> a = null;

					a = new HashMap<String, Double>();

					for (Object[] countriesbookings : countriesbookingsoncatagorey)
					{

						a.put((String) countriesbookings[0], (Double) countriesbookings[1]);
					}

					if (countriesbookingsoncatagorey.size() != 0)
					{
						Set sbookingcountries = a.keySet();

						logger.info("the values in the key set are" + sbookingcountries);

						for (Object[] country : countries)
						{

							logger.info("the country is follwing ------------------->" + (String) country[0]);

							if (sbookingcountries.contains(((String) country[0])))
							{

								JSONObject ebc = new JSONObject();
								Double d = (Double) a.get(((String) country[0]));

								ebc.put("value", d);
								if (cggc.equalsIgnoreCase("SCA"))
								{
									ebc.put("link", "#/app/SCA_profitProducts");
								}
								else
								{
									ebc.put("link", "#/app/" + (String) countryclinks.get(countrycode) + "");

								}
								countryjsonArray.add(ebc);
							}

							/*else {
							
								JSONObject ebc = new JSONObject();
								Double d = (Double) a.get(((String) country[0]).toUpperCase());
								logger.info(d + "-------------------------------------------->"
										+ ((String) country[0]).toUpperCase());
								ebc.put("value", null);
								ebc.put("link", "#/app/SCA_profitProducts");
								countryjsonArray.add(ebc);
							}*/
						}

					}

					aboutcountry.put("seriesname", catageoryname);
					aboutcountry.put("color", color);
					aboutcountry.put("data", countryjsonArray);
					countryjsonArraywithcatageroey.add(aboutcountry);
				}

				keyforyears.put("data", countryjsonArraywithcatageroey);

				return keyforyears;

			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.info("======CONTROLLER METHOD=====profitProductsoncountry========" + e.getCause());
			return null;

		}

	}

}
