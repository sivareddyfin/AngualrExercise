package com.finsol.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.finsol.bean.CustmerMasterBean;
import com.finsol.bean.CustmerMasterContactDetailsBean;
import com.finsol.bean.FrameSizeTagBean;
import com.finsol.bean.ItemAndScoreBean;
import com.finsol.bean.ScoreBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AppCode;
import com.finsol.model.ApplicationIndustryCode;
import com.finsol.model.Country;
import com.finsol.model.CreditStatus;
import com.finsol.model.CustmerContactDetails;
import com.finsol.model.CustmerDetails;
import com.finsol.model.CustmerType;
import com.finsol.model.Department;
import com.finsol.model.FrameSizeAndTag;
import com.finsol.model.ItemAndScore;
import com.finsol.model.Product;
import com.finsol.model.ProductCategory;
import com.finsol.model.ProductLine;
import com.finsol.model.ProspectCustmerContactDetails;
import com.finsol.model.ProspectCustmerDetails;
import com.finsol.model.Score;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.Bss_Migrate_ServiceImpl;
import com.finsol.service.MasterScreen_serviceImpl;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

/**
 * @author Rama Krishna
 * 
 */
@Controller
public class MasterScreenController {
	
	private static final Logger logger = Logger.getLogger(MasterScreenController.class);
	
	@Autowired
	private MasterScreen_serviceImpl masterservice;
	
	
	@Autowired
	private Bss_Migrate_ServiceImpl migratesevice;
	
	
	/*@Autowired
    private ServletContext servletContext;
	*/
	@Autowired
	private BSS_CommonService commonService;
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	
	
	//Generating Hi Chart for Area/Region YTD
	
	
	@RequestMapping(value = "/productCategoryForm", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public  @ResponseBody boolean  productCatageorySave(@RequestBody String jsonData) throws Exception
	{
		ProductCategory productageorey = new ObjectMapper().readValue(jsonData,ProductCategory.class);
		if(productageorey.getPcid()==null)
		{
		masterservice.saveCatageory(productageorey);
		}
		else
		{
			masterservice.updateProductCatgeorey(productageorey);
			
		}
		
		
		 
		return true;
	
	}
	
	
	
	@RequestMapping(value = "/viewproductcategory",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject viewproductcatageorey()
	{
		JSONObject productcatgories=new JSONObject();
		JSONArray productcatgoriesaraay = new JSONArray();
		
		List<ProductCategory> productageorey=masterservice.allValuesInProductCatageory();
		for(ProductCategory p:productageorey)
		{
			
		
			JSONObject subproductcatgories=new JSONObject();
			subproductcatgories.put("pcid",p.getPcid());
			subproductcatgories.put("productcategorycode",p.getProductcategorycode());
			subproductcatgories.put("bokclr",p.getBokclr());
			subproductcatgories.put("budgclr",p.getBudgclr());
			subproductcatgories.put("description",p.getDescription());
			subproductcatgories.put("productcategory",p.getProductcategory());
			subproductcatgories.put("status",p.getStatus());
			productcatgoriesaraay.add(subproductcatgories);
			
		}
		
		productcatgories.put( "data",productcatgoriesaraay);
		
		
	 
		
		 
		return productcatgories;
	
	}
	
	
	
	
	@RequestMapping(value = "/productsSubmit", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public  @ResponseBody boolean  productSave(@RequestBody   JSONObject jsonData) throws Exception
	{
		JSONObject json_data=null;
		String productcatgeoreycode=(String) jsonData.get("formData");
		JSONArray branchData=(JSONArray) jsonData.get("gridData");
		ArrayList<Product> product=new ArrayList<Product>();
		for(int i=0;i<branchData.size();i++)
		{
			Product p=new Product();
			  json_data = branchData.getJSONObject(i);
           String budgetcolor   =json_data.getString("bugColor");
           String bookingcolor  =json_data.getString("bookColor");
           String productname  =json_data.getString("productName");
           String productcode=json_data.getString("productCode");
         p.setBokclr(bookingcolor.trim() );
         p.setBudgclr(budgetcolor.trim());
         p.setPccode(productcatgeoreycode);
         p.setProduct(productname);
          p.setProductcode(productcode); 
         product.add(p);	
		}
		 
		masterservice.saveProducts(product);
		return true;
	
	}
	
	
	
	@RequestMapping(value = "/getAllAddedProducts",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllproductSave(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		String productcatgeoreycode=(String) req.getParameter("category");
		System.out.println(productcatgeoreycode+"---------------------------->");
		List<Product> produt=masterservice.getAllproductOnCatgeoreyCode(productcatgeoreycode);
		JSONObject product=new JSONObject();
		JSONArray productsaraay = new JSONArray();
		int i=1;
		for(Product p:produt)
		{
			JSONObject subproduct=new JSONObject();
			subproduct.put("id", i);
			subproduct.put("pcid",p.getProductid());
			subproduct.put("pccode",p.getPccode());
			subproduct.put("bookColor",p.getBokclr());
			subproduct.put("bugColor",p.getBudgclr());
			subproduct.put("productCode",p.getProductcode());
			subproduct.put("productName",p.getProduct());
		
			
			productsaraay.add(subproduct);
			
			i++;
		}
		product.put("data",productsaraay);
		
		return product;
	
	}
	
	
	
	
	
	
	@RequestMapping(value = "/DeparmentSaveForm", method = RequestMethod.POST)
   // public @ResponseBody boolean saveUsers(@RequestParam("userphoto") MultipartFile file,@RequestParam("usersign") MultipartFile usersign,@RequestParam("formData") String formData,HttpServletRequest req) throws Exception
	 public @ResponseBody boolean saveUsers(@RequestParam("formData") String formData,HttpServletRequest req) throws Exception
    {
	 	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		String ext=null;
		String ext1=null;		
		String photoName=null;
		String signName=null;		
		String photoPath=null;
		String signaturePath=null;
		
		
	 
/*			ServletContext servletContext=req.getServletContext();
			Integer maxId=commonService.getMaximumID("userid","Users");
			
		    UsersBean usersBean = new ObjectMapper().readValue(formData.toString(), UsersBean.class);
		    if(usersBean.getUserId()==null)
		    {
				ext=getFileExtension(file.getOriginalFilename());
				ext1=getFileExtension(usersign.getOriginalFilename());
				photoName=maxId+"_photo"+"."+ext;
				signName=maxId+"_sign"+"."+ext1;
				
				photoPath="images/"+photoName;
				signaturePath="images/"+signName;
				 
				usersBean.setPhotoPath(photoPath);
				usersBean.setSignaturePath(signaturePath);
				//usersBean.setUserId(maxId);
		    }
		    else
		    { 	
				if(file.getSize()!=0)
				{						
					ext=getFileExtension(file.getOriginalFilename());
					photoName=usersBean.getUserId()+"_photo"+"."+ext;
					photoPath="images/"+photoName;
					usersBean.setPhotoPath(photoPath);
				}
				if(usersign.getSize()!=0)
				{						
					ext1=getFileExtension(usersign.getOriginalFilename());						
					signName=usersBean.getUserId()+"_sign"+"."+ext1;						
					signaturePath="images/"+signName;
					usersBean.setSignaturePath(signaturePath);
				}

		    }*/
			
			Department department = new ObjectMapper().readValue(formData.toString(),Department.class);
			if(department.getDeptid()==null)
			{
			masterservice.saveDepatment(department);
			}
			else
			{
				masterservice.updateDepartMent(department);
				
			}
			
			
		 
			{/*
				isInsertSuccess =false;
				
				File dir1=new File(servletContext.getRealPath("/")+"images/");
				
				if (!dir1.exists())
					dir1.mkdirs();
				
				if(file.getSize()!=0)
				{					
					byte[] bytes1 = file.getBytes();
	
					File serverFile1 = new File(dir1.getAbsolutePath()+ File.separator + photoName);					
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}
				if(usersign.getSize()!=0)
				{
					byte[] bytes2 = usersign.getBytes();
					
					File serverFile2 = new File(dir1.getAbsolutePath()+ File.separator + signName);					
					BufferedOutputStream stream2 = new BufferedOutputStream(new FileOutputStream(serverFile2));
					stream2.write(bytes2);
					stream2.close();
				}
				isInsertSuccess =true;
			*/}
		   return true;
		 
     }
	
	
	
	/*
	@RequestMapping(value = "/DeparmentSaveForm", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public  @ResponseBody boolean  departmentSave(@RequestBody String jsonData) throws Exception
	{
		Department department = new ObjectMapper().readValue(jsonData,Department.class);
		if(department.getDeptid()==null)
		{
		masterservice.saveDepatment(department);
		}
		else
		{
			masterservice.updateDepartMent(department);
			
		}
		
		
		 
		return true;
	
	}*/
	
	
	
	@RequestMapping(value = "/departmentView",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject departmentView()
	{
		JSONObject productcatgories=new JSONObject();
		JSONArray productcatgoriesaraay = new JSONArray();
		
		List<Department> productageorey=masterservice.departmentView();
		for(Department p:productageorey)
		{
			
		
			JSONObject subproductcatgories=new JSONObject();
			subproductcatgories.put("deptid",p.getDeptid());
			subproductcatgories.put("departmentname",p.getDepartmentname());
			subproductcatgories.put("departmentnameshortcut",p.getDepartmentnameshortcut());
			subproductcatgories.put("contactperson",p.getContactperson());
			subproductcatgories.put("email",p.getEmail());
			subproductcatgories.put("extent",p.getExtent());
			subproductcatgories.put("phonenumber",p.getPhonenumber());
			subproductcatgories.put("status",p.getStatus());
			
			productcatgoriesaraay.add(subproductcatgories);
			
		}
		
		productcatgories.put( "data",productcatgoriesaraay);
		
		
	 
		
		 
		return productcatgories;
	
	}
	
	
	
	 
	
	/*@RequestMapping(value = "/saveCaneManager", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveCaneManager(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			CaneManagerBean caneManagerBean = new ObjectMapper().readValue(jsonData, CaneManagerBean.class);
			CaneManager caneManager = prepareModel(caneManagerBean);
			entityList.add(caneManager);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	*/
	
	
	
	
	
	
	@RequestMapping(value = "/getAllProdutsLine",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllProductLines(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		JSONObject mainproductline = new JSONObject();
		JSONArray productlinearaay = new JSONArray();
		HashMap<String, ArrayList<String>> productandproductcatgeory = new HashMap<String, ArrayList<String>>();

		List<ProductCategory> productageorey = masterservice.allValuesInProductCatageory();
		for (ProductCategory p : productageorey) {
			String s = p.getProductcategorycode();

			List<Product> produt = masterservice.getAllproductOnCatgeoreyCode(s);

			ArrayList<String> al = new ArrayList<String>();
			for (Product p1 : produt) {
				al.add(p1.getProduct());

			}

			productandproductcatgeory.put(s, al);

		}
		
		
		
		
		
		
		
		

		List<Object[]> productlines = masterservice.getAllProductLInes();
		int i=0;
		
		for (Object[] productline : productlines) {
			JSONObject subproductline = new JSONObject();
			subproductline.put("id", (Integer) productline[0]);
			subproductline.put("pccode", (String) productline[1]);
			subproductline.put("plcode", (String) productline[2]);
			subproductline.put("pldesc", (String) productline[3]);
			subproductline.put("product", (String) productline[4]);
			subproductline.put("productcategory", (String) productline[5]);
			subproductline.put("productcode", (String) productline[6]);
			
			
			
		ArrayList<String>  products	=(ArrayList<String>)productandproductcatgeory.get((String) productline[1]);
		JSONArray productsarray = new JSONArray();
		for(String s:products)
		{
			JSONObject subproducts = new JSONObject();
			
			subproducts.put("productName",s);
			productsarray.add(subproducts);
			
		}
			
		subproductline.put("productsarray"+i+"",productsarray);
		i++;
			
			productlinearaay.add(subproductline);

		}

		mainproductline.put("data", productlinearaay);

		return mainproductline;

	}
	
	
	
	
	@RequestMapping(value = "/getAllProdutsInProdutLine",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllProductInProductLines(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		String productcatgeoreycode=(String) req.getParameter("category");
		System.out.println(productcatgeoreycode+"---------------------------->");
		List<Product> produt=masterservice.getAllproductOnCatgeoreyCode(productcatgeoreycode);
		JSONObject product=new JSONObject();
		JSONArray productsaraay = new JSONArray();
		int i=1;
		for(Product p:produt)
		{
			JSONObject subproduct=new JSONObject();
			subproduct.put("id", i);
			subproduct.put("pcid",p.getProductid());
			subproduct.put("pccode",p.getPccode());
			subproduct.put("bookColor",p.getBokclr());
			subproduct.put("bugColor",p.getBudgclr());
			subproduct.put("productCode",p.getProductcode());
			subproduct.put("productName",p.getProduct());
		
			
			productsaraay.add(subproduct);
			
			i++;
		}
		product.put("data",productsaraay);
		
		return product;
	
	
	}
	
	
	
	/*@RequestMapping(value = "/getAllProdutsOnProductCatageory", method = RequestMethod.GET)
    public  @ResponseBody  JSONObject getAllProdutsOnProductCatageory(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		String productcatagerey =req.getParameter("productCatageory");
		JSONObject mainproductline=new JSONObject();
		JSONArray productlinearaay = new JSONArray();
		List<Object[]> productlines=masterservice.getAllProductsOnProductCatageory(productcatagerey);
		for(Object[] productline:productlines)
		{
			JSONObject subproductline=new JSONObject();
		 
			subproductline.put("product",(Integer)productline[0]);
			 
			subproductline.put("productcode",(Integer)productline[0]);
			productlinearaay.add(subproductline);
			
			
			
		}
	
		mainproductline.put("data",productlinearaay);
	 		
		return mainproductline;
	
	}
	*/
	
	
	@RequestMapping(value = "/productLineSubmit", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public  @ResponseBody boolean  productLineSave(@RequestBody   JSONObject jsonData) throws Exception
	{
		JSONObject json_data=null;
		
		
		JSONArray branchData=(JSONArray) jsonData.get("gridData");
		
		
		masterservice.deleteProductLine();
		ArrayList<ProductLine> al=new ArrayList<ProductLine>(); 
		
		for(int i=0;i<branchData.size();i++)
		{
			ProductLine p =new ProductLine();
			  json_data = branchData.getJSONObject(i);
          String pccode   =json_data.getString("pccode");
          String plcode  =json_data.getString("plcode");
          String pldesc  =json_data.getString("pldesc");
          String product=json_data.getString("product");
          String productcategory=json_data.getString("productcategory");
          String productcode=json_data.getString("productcode");
          p.setPccode(pccode);
          p.setPlcode(plcode);
          p.setPlcode(plcode);
          p.setPldesc(pldesc);
          p.setProduct(product);
          p.setProductcategory(productcategory);
          p.setProductcode(productcode);
          al.add(p);
		}
		 
		masterservice.saveProductLine(al);
		return true;
	
	}
	
	
	
	
	/*
	@RequestMapping(value = "/customerFormSubmit", method = RequestMethod.POST)
    public  @ResponseBody boolean  custmerSave(MultipartHttpServletRequest req) throws Exception
	{
	
		
	 
		String ss=req.getParameter("formData");
		//String description1=req.getParameter("fieldData");
		String ss1=req.getParameter("gridData");
		org.json.JSONArray gData=new org.json.JSONArray(ss1);
	    List<MultipartFile> mpf=req.getFiles("usersign");
		
	 String imapath=this.getImagepathOfCustmerTemp(mpf);
	 
	
	


	CustmerDetails productageorey = new ObjectMapper().readValue(ss,CustmerDetails.class);
	
	String k1	=DateUtils.formatDate(productageorey.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
	Date d=DateUtils.getSqlDateFromString(k1,Constants.GenericDateFormat.DATE_FORMAT);
	productageorey.setBddate(d);
	if(imapath!=null)
	{
	productageorey.setImagename(imapath);
	}
	
	 
	
	System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR------------------------->"+productageorey.getCustid());
	
	
	 
	
	
	ArrayList<CustmerContactDetails> plk=new ArrayList<CustmerContactDetails>(); 
	
	for(int i=0;i<gData.length();i++)
	{
		 
		CustmerContactDetails CustmerContactDetails = new ObjectMapper().readValue(gData.get(i).toString(), CustmerContactDetails.class);
		 
      plk.add(CustmerContactDetails);
      
	}
	
	
	if(productageorey.getCustid()!=null)
	{
		masterservice.deleteofCutmerContactDetails(productageorey.getCustid());
		masterservice.deleteCustmer(productageorey.getCustid());
		
		
		productageorey.setRolescreens(plk);
	    masterservice.saveCustmer(productageorey);
		
		masterservice.updateCustmer(productageorey,plk);
		masterservice.saveContactCustmerDetails(productageorey,plk);
		
 
 
	}
	 
 
	if(productageorey.getCustid()==null)
	{
	
	productageorey.setRolescreens(plk);
    masterservice.saveCustmer(productageorey);
 
	
	}
	 
	
		 
		return true;
		
		
 
	}


	
*/	
	
	
	

	 	 
	
	
	
	/*private com.finsol.model.CustmerDetails custmerdetails(CustmerBean custmer)
	{
		CustmerDetails c=new CustmerDetails();
		c.setCustid(custmer.getCustid());
		c.setAccount(custmer.getAccount());
		c.setAccountid(custmer.getAccountid());
		c.setApplication(custmer.getApplication());
		c.setBddate(custmer.getBddate());
		 
	
	return c;
}

*/
	
	

	@RequestMapping(value = "/customerFormSubmit", method = RequestMethod.POST)
    public  @ResponseBody boolean  custmerSave(MultipartHttpServletRequest req) throws Exception
	{
	
		HttpSession hs=req.getSession();
		/*hs.getS*/
		
		ServletContext servletContext	= req.getSession().getServletContext();
		String ss=req.getParameter("formData");
		//String description1=req.getParameter("fieldData");
		String ss1=req.getParameter("gridData");
		org.json.JSONArray gData=new org.json.JSONArray(ss1);
	    List<MultipartFile> mpf=req.getFiles("usersign");
		
	 String imapath=this.getImagepathOfCustmerTemp(mpf,servletContext);
	 
	
	


	 CustmerMasterBean productageorey = new ObjectMapper().readValue(ss,CustmerMasterBean.class);
	
	 
	if(imapath!=null)
	{
	productageorey.setImagename(imapath);
	}
	
	
	
	 
	
	System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR------------------------->"+productageorey.getCustid());
	
	
	 
	
	
	ArrayList<CustmerMasterContactDetailsBean> plk=new ArrayList<CustmerMasterContactDetailsBean>(); 
	
	for(int i=0;i<gData.length();i++)
	{
		 
		CustmerMasterContactDetailsBean CustmerContactDetails = new ObjectMapper().readValue(gData.get(i).toString(), CustmerMasterContactDetailsBean.class);
		 
      plk.add(CustmerContactDetails);
      
	}
	
	
	if(productageorey.getCustid()!=0)
	{
		masterservice.deleteofCutmerContactDetails(productageorey.getCustid());
		/*masterservice.deleteCustmer(productageorey.getCustid());*/
		
		CustmerDetails custmerdetailsdata=this.getCustmerMasterData(productageorey);
		custmerdetailsdata.setCustid(productageorey.getCustid());
		List<CustmerContactDetails> ccdl=this.getCustmerContactDetails(plk);
		custmerdetailsdata.setRolescreens(ccdl);
	    masterservice.saveCustmer(custmerdetailsdata);
		
		/*masterservice.updateCustmer(productageorey,plk);
		masterservice.saveContactCustmerDetails(productageorey,plk);*/
		
 
 
	}
	 
 
	if(productageorey.getCustid()==0)
	{
	
		CustmerDetails custmerdetailsdata=this.getCustmerMasterData(productageorey);
		List<CustmerContactDetails> ccdl=this.getCustmerContactDetails(plk);
		custmerdetailsdata.setRolescreens(ccdl);
	    masterservice.saveCustmer(custmerdetailsdata);
	
	}
	 
	
		 
		return true;
		
		
 
	}


	

	
	
	private List<CustmerContactDetails> getCustmerContactDetails(ArrayList<CustmerMasterContactDetailsBean> plk) {
		// TODO Auto-generated method stub
		ArrayList<CustmerContactDetails> ccd1=new ArrayList<CustmerContactDetails>();
		for(CustmerMasterContactDetailsBean custmercontact:plk)
		{
			CustmerContactDetails c=new CustmerContactDetails();
			 c.setContactName(custmercontact.getContactName()); 
		     c.setContactNumber(custmercontact.getContactNumber());
			 c.setContactAltNumber(custmercontact.getContactAltNumber()); 
			 c.setDepartment(custmercontact.getDepartment()); 
			 c.setEmailAddr(custmercontact.getEmailAddr());
		     c.setTitleName(custmercontact.getTitleName()); 
		     ccd1.add(c);
			
		}
		return ccd1;
	}



	private CustmerDetails getCustmerMasterData(CustmerMasterBean productageorey) 
	{
		CustmerDetails cdetails=new CustmerDetails();
		cdetails.setCustomerStatus(productageorey.getCustomerStatus());  
		cdetails.setAccount(productageorey.getAccount());
		cdetails.setAccountid(productageorey.getAccountid());
		cdetails.setClientwebsite(productageorey.getClientwebsite());
		cdetails.setMailingadress(productageorey.getMailingadress());
		cdetails.setShippingadress(productageorey.getShippingadress());
		cdetails.setBillingCountries(productageorey.getBillingCountries());
		cdetails.setLanguage(productageorey.getLanguage());
		cdetails.setCustomerType(productageorey.getCustomerGroup());
		cdetails.setIndustries(productageorey.getIndustries());
		cdetails.setApplication(productageorey.getApplication());
		cdetails.setTerritory(productageorey.getTerritory());
		cdetails.setCorporate(productageorey.getCorporate());
		cdetails.setContName(productageorey.getContName());
		cdetails.setTitle(productageorey.getTitle());
		cdetails.setPhnumber(productageorey.getPhnumber());
		cdetails.setMbnumber(productageorey.getMbnumber());
		cdetails.setOphnumber(productageorey.getOphnumber());
		cdetails.setEmail(productageorey.getEmail());
		cdetails.setFax(productageorey.getFax());
		cdetails.setDepartmentname(productageorey.getDepartmentname());
		cdetails.setReports(productageorey.getReports());
		cdetails.setSalesPerson(productageorey.getSalesPerson());
	    cdetails.setSalesPersonCode(productageorey.getSalesPersonCode());
		cdetails.setLeadsource(productageorey.getLeadsource()); 
		cdetails.setCurrency(productageorey.getCurrency());
		cdetails.setTaxName(productageorey.getTaxName());
		cdetails.setPaymentTerm(productageorey.getPaymentTerm());
	    cdetails.setOpportunityAmount(productageorey.getOpportunityAmount());
	    cdetails.setCreatedby(productageorey.getCreatedby());
	    cdetails.setLastmodifiedby(productageorey.getLastmodifiedby());
	    cdetails.setDiscountmultiplier(productageorey.getDiscountmultiplier());
	    cdetails.setCreditstatus(productageorey.getCreditStatus());
	    cdetails.setApplicanindustryname(productageorey.getApplicanIndustry());
	    
	    cdetails.setImagename(productageorey.getImagename());
		 
	    if (productageorey.getBddate() != null) {
			//String k1 = DateUtils.formatDate(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			Date bddadte = DateUtils.getSqlDateFromString(productageorey.getBddate(), Constants.GenericDateFormat.DATE_FORMAT);
			cdetails.setBddate(bddadte);
		}
		return cdetails;
	}



	private String getImagepathOfCustmerTemp(List<MultipartFile> mpf,ServletContext servletContext) throws Exception
	{
		String photoName=null;
		String photoPath=null;
	 int i=0;
		for(MultipartFile mf:mpf)
		{
			
			String fname=mf.getOriginalFilename();
			//logger.info("--------------"+mf.getOriginalFilename());
			
			
			if (mf.getSize() != 0) 
			{
				Random t = new Random();
				int num = t.nextInt(10000);
				
				String exts = getFileExtension(mf.getOriginalFilename());
				photoName = i + num + "_photo" + "." + exts;
				/*if(i==0){
				photoPath = "custmer/" + photoName;
				}*/
				 
					 photoPath="custmer/" + photoName;
					//photoPath = photoPath+","+photoPath1;
				 

				File dir1 = new File(servletContext.getRealPath("/") + "custmer/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (mf.getSize() != 0) 
				{
					byte[] bytes1 = mf.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}
			i++;
		}
	
		return photoPath;

	}


	

	@RequestMapping(value = "/custmerImage", method = RequestMethod.POST)
    public @ResponseBody boolean saveUsers(@RequestParam("usersign") MultipartFile file,ServletContext servletContext) throws Exception
    {
	 	 
		   
		   
			 try{
				 if(file.getSize()!=0)
				 {
				 Random t = new Random();
				int num= t.nextInt(10000);
				 
				 int maxid=masterservice.getMaxIdCustmer();
				String ext=getFileExtension(file.getOriginalFilename());
				String photoName=maxid+num+"_photo"+"."+ext;
				String photoPath="images/"+photoName;
				
				masterservice.updateimageonMaxId(maxid,photoPath);
				 
				 File dir1=new File(servletContext.getRealPath("/")+"images/");
					
					if (!dir1.exists())
						dir1.mkdirs();

		if(file.getSize()!=0)
					{					
						byte[] bytes1 = file.getBytes();
		
						File serverFile1 = new File(dir1.getAbsolutePath()+ File.separator + photoName);					
						BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
						stream1.write(bytes1);
						stream1.close();
					}
				 
				 }
				 
				 return true;
				 
		}
		catch (Exception e)
		{ 
			logger.info(e.getCause(), e);
			return false;
		}
     }
	

	

	
public  String getFileExtension(String fileName) 
{
    if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
    return fileName.substring(fileName.lastIndexOf(".")+1);
    else return "";
}
	 	


	@RequestMapping(value = "/getAllCountries",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllCountries(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		
		List<Country> coutries=migratesevice.getCountriesOnCountrycodes();
		JSONObject countryobject = new JSONObject();
		
		JSONArray vendorarray= new JSONArray();
		
		for(Country country:coutries)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("id",country.getCountry());
			vendorobject.put("billingCountries",country.getCountry());
			vendorarray.add(vendorobject);
			
		}
		
		countryobject.put("data",vendorarray);
		
		
		return countryobject;
	}
	
	
	

	@RequestMapping(value = "/getAllApplications",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllApplications(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		try
		{
		
		List<ApplicationIndustryCode> coutries=migratesevice.getAllApplicationCode();
		JSONObject countryobject = new JSONObject();
		
		JSONArray vendorarray= new JSONArray();
		
		for(ApplicationIndustryCode country:coutries)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("application",country.getMachinecode());
			vendorarray.add(vendorobject);
			
		}
		
		countryobject.put("data",vendorarray);
		
		
		return countryobject;
		
		}catch(Exception e)
		{
			 e.getStackTrace();
			return null;
		}
	}
	
	
	
	@RequestMapping(value = "/getApplicationIndustryName",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllApplicationName(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		
		
			String applicationcode=req.getParameter( "applicanIndusName");
			JSONArray j=new JSONArray();
			JSONObject mainobject = new JSONObject();
		
		List<ApplicationIndustryCode> coutries=migratesevice.getAllApplicationCodeName(applicationcode);
		String applicationname=null;
		for(ApplicationIndustryCode a:coutries)
		{
			
			applicationname=a.getTotalname();
			
		}
		JSONObject countryobject = new JSONObject();
		
		countryobject.put("applicanIndustry",applicationname);
		j.add(countryobject);
		
		mainobject.put("data",j);
		
		return mainobject;
		
		 
	}
	
	
	
	
		
	
	@RequestMapping(value = "/getAllIndustryNames",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllIndustryNames(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		
		List<String> coutries=migratesevice.getAllIndustryNames();
		JSONObject countryobject = new JSONObject();
		
		JSONArray vendorarray= new JSONArray();
		
		for(String country:coutries)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("industries",country);
			vendorarray.add(vendorobject);
			
		}
		
		countryobject.put("data",vendorarray);
		
		
		return countryobject;
	}
	
	
	
	@RequestMapping(value = "/getAllCutmerGroupNames",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllCutmerGroupNames(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		
		//List<String> coutries=migratesevice.getAllCutmerGroupNames();
		List<CustmerType> c=migratesevice.getAllCustmerTypes();
		
		
		JSONObject countryobject = new JSONObject();
		
		JSONArray vendorarray= new JSONArray();
		
		for(CustmerType country:c)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("id",country.getCustmertype());
			vendorobject.put("customerGroup",country.getCustmertypename());
			
			vendorarray.add(vendorobject);
			
		}
		
		countryobject.put("data",vendorarray);
		
		
		return countryobject;
	}
	 
	
	@RequestMapping(value = "/getAllTeriotreyNames",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllTeriotreyNames(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		
		List<String> coutries=migratesevice.getAllTeriotreyNames();
		JSONObject countryobject = new JSONObject();
		
		JSONArray vendorarray= new JSONArray();
		
		for(String country:coutries)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("territory",country);
			vendorarray.add(vendorobject);
			
		}
		
		countryobject.put("data",vendorarray);
		
		
		return countryobject;
	}
	
	
	@RequestMapping(value = "/getAllSalesMan",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllSalesManNames(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		
		List<String> coutries=migratesevice.getAllSalesManNames();
		JSONObject countryobject = new JSONObject();
		
		JSONArray vendorarray= new JSONArray();
		
		for(String country:coutries)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("salesPerson",country);
			vendorarray.add(vendorobject);
			
		}
		
		countryobject.put("data",vendorarray);
		
		
		return countryobject;
	}
	
	
	
	
	
		
		
		
	 
	
	
	
	
	
	
	@RequestMapping(value = "/getSalesManCode",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getSalesManCode(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		String s=req.getParameter("salesman");
		List<String> coutries=migratesevice.getAllSalesMancode(s);
		JSONObject countryobject = new JSONObject();
		
		JSONArray vendorarray= new JSONArray();
		
		for(String country:coutries)
		{
			 
			countryobject.put("data",country);
			  
			
		}
		
		 
		
		
		return countryobject;
	}
	
	

	@RequestMapping(value = "/getAllPayments",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllPaymentsNames(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		JSONObject  countryobject = null;;
		
		try
		{
			countryobject=new JSONObject();
		List<String> coutries=migratesevice.getAllPaymentsNames();
		 
		
		JSONArray vendorarray= new JSONArray();
		
		for(String country:coutries)
		{
			JSONObject vendorobject = new JSONObject();
			vendorobject.put("paymentTerm",country);
			vendorarray.add(vendorobject);
			
		}
		
		countryobject.put("data",vendorarray);
		
		
		}catch(Exception e)
		
		{
			
			
			System.out.println(e);
		}
		
		
		return countryobject;
	}
	
	
	
	
	@RequestMapping(value = "/getAllPaymentsAppcode",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllPaymentsNamesAppcode(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		JSONObject  countryobject = null;;
		
		try
		{
			countryobject=new JSONObject();
		List<AppCode> coutries=migratesevice.getAllPaymentsNamesAppcode();
		 
		
		JSONArray vendorarray= new JSONArray();
		
		for(AppCode country:coutries)
		{
			JSONObject vendorobject = new JSONObject();
			
			vendorobject.put("id",country.getMappingpterm());
			vendorobject.put("paymentTerm",country.getPaymenttewrmdescription());
			vendorarray.add(vendorobject);
			
		}
		
		countryobject.put("data",vendorarray);
		
		
		}catch(Exception e)
		
		{
			
			
			System.out.println(e);
		}
		
		
		return countryobject;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	@RequestMapping(value = "/saveFrameSizeAndTag", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveFramaSizeAndTag(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			FrameSizeTagBean frameSizeTagBean = new ObjectMapper().readValue(jsonData, FrameSizeTagBean.class);
			
			FrameSizeAndTag frameSizeAndTag = prepareModelForFrameSizeAndTag(frameSizeTagBean);
			
			entityList.add(frameSizeAndTag);

			isInsertSuccess = masterservice.saveMultipleEntities(entityList);
		
			
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private FrameSizeAndTag prepareModelForFrameSizeAndTag(FrameSizeTagBean frameSizeTagBean) 
	{
			FrameSizeAndTag frameSizeAndTag = new FrameSizeAndTag();
			frameSizeAndTag.setId(frameSizeTagBean.getId());
			frameSizeAndTag.setFramesize(frameSizeTagBean.getFrameSize());
			frameSizeAndTag.setProductcode(frameSizeTagBean.getProductCode());
			frameSizeAndTag.setTag(frameSizeTagBean.getTag());
		
		return frameSizeAndTag;
	}
	
	@RequestMapping(value = "/getAllFrameSizes", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<FrameSizeTagBean> getAllFrameSizes(@RequestBody String jsonData)throws Exception
	{
		
		List<FrameSizeTagBean> frameSizeTagBeans = prepareListofFrameSizeTagBeans(masterservice.listFrameSizeAndTag());
		return frameSizeTagBeans;
	}
	
	private List<FrameSizeTagBean> prepareListofFrameSizeTagBeans(List<FrameSizeAndTag> frameSizeAndTags)
	{
		List<FrameSizeTagBean> beans = null;
		if (frameSizeAndTags != null && !frameSizeAndTags.isEmpty())
		{
			beans = new ArrayList<FrameSizeTagBean>();
			FrameSizeTagBean bean = null;
			for (FrameSizeAndTag frameSizeAndTag : frameSizeAndTags)
			{
				bean = new FrameSizeTagBean();	
				
				bean.setId(frameSizeAndTag.getId());
				bean.setFrameSize(frameSizeAndTag.getFramesize());
				bean.setProductCode(frameSizeAndTag.getProductcode());
				bean.setTag(frameSizeAndTag.getTag());
				beans.add(bean);
			}
		}
		return beans;
	}	
	
	//---------------------------------End of Frame Size Master ----------------------------------//

	
	
	@RequestMapping(value = "/saveItemAndScore", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveItemAndScore(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			ItemAndScoreBean itemAndScoreBean = new ObjectMapper().readValue(jsonData, ItemAndScoreBean.class);
			
			ItemAndScore itemAndScore = prepareModelForItemAndScore(itemAndScoreBean);
			
			entityList.add(itemAndScore);

			isInsertSuccess = masterservice.saveMultipleEntities(entityList);
		
			
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private ItemAndScore prepareModelForItemAndScore(ItemAndScoreBean itemAndScoreBean) 
	{
			ItemAndScore itemAndScore = new ItemAndScore();
			itemAndScore.setId(itemAndScoreBean.getId());
			itemAndScore.setItemvalue(itemAndScoreBean.getItemValue());
			itemAndScore.setModel(itemAndScoreBean.getModel());
			itemAndScore.setNo(itemAndScoreBean.getStageNumber());
			itemAndScore.setScore(itemAndScoreBean.getScore());
			itemAndScore.setRemarks(itemAndScoreBean.getRemarks());
		
		return itemAndScore;
	}
	
	@RequestMapping(value = "/getAllItemAndScores", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<ItemAndScoreBean> getAllItemAndScores(@RequestBody String jsonData)throws Exception
	{
		
		List<ItemAndScoreBean> itemAndScoreBeans = prepareListofItemAndScoreBeans(masterservice.listItemAndScore());
		return itemAndScoreBeans;
	}
	
	private List<ItemAndScoreBean> prepareListofItemAndScoreBeans(List<ItemAndScore> itemAndScores)
	{
		List<ItemAndScoreBean> beans = null;
		if (itemAndScores != null && !itemAndScores.isEmpty())
		{
			beans = new ArrayList<ItemAndScoreBean>();
			ItemAndScoreBean bean = null;
			for (ItemAndScore itemAndScore : itemAndScores)
			{
				bean = new ItemAndScoreBean();	
				
				bean.setId(itemAndScore.getId());
				bean.setItemValue(itemAndScore.getItemvalue());
				bean.setModel(itemAndScore.getModel());
				bean.setScore(itemAndScore.getScore());
				bean.setStageNumber(itemAndScore.getNo());
				bean.setRemarks(itemAndScore.getRemarks());
				
				beans.add(bean);
			}
		}
		return beans;
	}	
	
	
	
	@RequestMapping(value = "/custmerdatabybasingoncustmercode",method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody JSONObject getAllCustmersDeatails(HttpServletRequest req,HttpServletResponse res) 
    {
	 	 
		   
		   
			 try{
				String custmername1=null;
				 
				 String custmername=req.getParameter("account");
				 if(custmername!=null)
				 {
				   custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
				 }
				 JSONObject mainobject=new JSONObject();
				 JSONArray mainarray=new  JSONArray();
				 JSONArray custmerarray=new  JSONArray();
				 
						 
			List<CustmerDetails>	cutmerdetails= masterservice.getAllCustmersDeatails(custmername1);
			
			mainobject.put("check",0);
			for(CustmerDetails a:cutmerdetails)
				
			{
				 
				 /*custmerobject.put("account",a);
				custmerarray.add(custmerobject);*/
			String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
			/*String k2 = k1.replace("-","/");*/
				 
			String[] k2=k1.split("-");
			 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
			
			
			/*DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");*/
			
		/*	Date frmDate = sdf.parse(k2);*/
			/*	mainobject.put("date",k2);*/
				mainobject.put("date",k1);
				mainobject.put("check",1);
				mainobject.put("custmer",a);
				mainobject.put("custid",a.getCustid());
				List<CustmerContactDetails> contactdetails=a.getRolescreens();
				int i=1;
				
				if(contactdetails.size()==0)
				{
					
					JSONObject subobject=new JSONObject();
					subobject.put("id",i);
					subobject.put("contactName",null);
					subobject.put("contactNumber",null);
					subobject.put("contactAltNumber",null);
					subobject.put("gdepatment",null);
					subobject.put("email",null);
					subobject.put("gtitle",null);
					mainarray.add(subobject);
					
					 
					
				}
				
				for(CustmerContactDetails b:contactdetails)
				{
					JSONObject subobject=new JSONObject();
					subobject.put("id",i);
					subobject.put("contactName",b.getContactName());
					subobject.put("contactNumber",b.getContactNumber());
					subobject.put("contactAltNumber",b.getContactAltNumber());
					subobject.put("department",b.getDepartment());
					subobject.put("emailAddr",b.getEmailAddr());
					subobject.put("titleName",b.getTitleName());
					mainarray.add(subobject);
					
					i++;
				}
				
			}
			/*mainobject.put("custmer",custmerarray);	 */
			mainobject.put("grid",mainarray );
				 
				 return mainobject;
				 
		}
		catch (Exception e)
		{ 
			logger.info("ERROR IN CUSTMER IS DUE TO THE FOLLOWING"+e.getCause(), e);
			System.out.println("ERROR IN CUSTMER IS DUE TO THE FOLLOWING"+e.getMessage());
			return null;
		}
     }
	

	
	
	
	@RequestMapping(value = "/getAllCustmerDetailsSavedOnId", method = RequestMethod.POST)
    public @ResponseBody JSONObject custmerdatabybasingoncustmercode(HttpServletRequest req,HttpServletResponse res) 
    {
	 	 
		   try{
		   HttpSession hs=req.getSession();
			 
				String custmername1=null;
				String applicationname=null;
				 
				 String custmername=req.getParameter("account");
				 if(custmername!=null)
				 {
				   custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
				 }
				 
				 if(custmername==null)
				 {
					 String  custmer =(String) hs.getAttribute("custmername"); 
					 custmername1 = custmer.substring(custmer.lastIndexOf('-') + 1);
				 }
				 JSONObject mainobject=new JSONObject();
				 JSONArray mainarray=new  JSONArray();
				// JSONArray custmerarray=new  JSONArray();
				 
						 
			List<CustmerDetails>	cutmerdetails= masterservice.getAllCustmersDeatails(custmername1);
			
			mainobject.put("check",0);
			for(CustmerDetails a:cutmerdetails)
				
			{
				JSONObject submainobject=new JSONObject();
				JSONArray custmerarray=new  JSONArray();
				submainobject.put("custid",a.getCustid());
				
				submainobject.put("customerStatus",a.getCustomerStatus());
				submainobject.put("account",a.getAccount());
				submainobject.put("accountid",a.getAccountid());
				submainobject.put("clientwebsite",a.getClientwebsite());
				submainobject.put("mailingadress",a.getMailingadress());
				submainobject.put("shippingadress",a.getShippingadress());
				submainobject.put("billingCountries",a.getBillingCountries());
				submainobject.put("language",a.getLanguage());
				submainobject.put("customerGroup",a.getCustomerType());
				submainobject.put("industries",a.getIndustries());
				submainobject.put("application",a.getApplication());
				submainobject.put("territory",a.getTerritory());
				submainobject.put("corporate",a.getCorporate());
				submainobject.put("contName", a.getContName());
				submainobject.put("title", a.getTitle());
				submainobject.put("phnumber",a.getPhnumber());
				submainobject.put("mbnumber",a.getMbnumber());
				submainobject.put("ophnumber",a.getOphnumber());
				submainobject.put("creditStatus",a.getCreditstatus());
				submainobject.put("email",a.getEmail());
				submainobject.put("fax", a.getFax());
				if(a.getApplicanindustryname()==null && a.getApplication()!=null)
				{
					//String applicationcode=req.getParameter( "applicanIndusName");
					
					List<ApplicationIndustryCode> coutries=migratesevice.getAllApplicationCodeName(a.getApplication());
					 
					for(ApplicationIndustryCode a2:coutries)
					{
						
						applicationname=a2.getTotalname();
						
					}
					 
					submainobject.put("applicanIndustry",applicationname);
					 
				}
				
				else
				{
					submainobject.put("applicanIndustry",a.getApplicanindustryname());
				}
				 
				submainobject.put("departmentname",a.getDepartmentname());
				submainobject.put("reports",a.getReports());
				if(a.getSalesPerson()!=null)
				{
					submainobject.put("salesPerson",a.getSalesPerson());
					
				}
				
				else
				{
					String salesman=masterservice.getSalesmanOnSalesCode(a.getSalesPersonCode());
					submainobject.put("salesPerson",salesman);
					
					
				}
				 
				submainobject.put("discountmultiplier",a.getDiscountmultiplier());
				submainobject.put("salesPersonCode",a.getSalesPersonCode());
				submainobject.put("leadsource",a.getLeadsource());
				submainobject.put("currency", a.getCurrency());
				submainobject.put("taxName",a.getTaxName());
				submainobject.put("paymentTerm",a.getPaymentTerm());
				submainobject.put("opportunityAmount", a.getOpportunityAmount());
				if(a.getBddate()!=null)
				{
					String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
					submainobject.put("bddate",k1 );
				}
				
				
				submainobject.put("createdby",a.getCreatedby());
				submainobject.put("lastmodifiedby",a.getLastmodifiedby());
				submainobject.put("imagename", a.getImagename());
				 
				custmerarray.add(submainobject);
				mainobject.put("custmer",custmerarray);
			    
				
				 
				 /*custmerobject.put("account",a);
				custmerarray.add(custmerobject);*/
		/*	String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
			String k2 = k1.replace("-","/");
				 
			String[] k2=k1.split("-");
			 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
			*/
			
			/*DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");*/
			
		/*	Date frmDate = sdf.parse(k2);*/
			/*	mainobject.put("date",k2);*/
				//mainobject.put("date",k1);
				mainobject.put("check",1);
				//mainobject.put("custmer",a);
				//mainobject.put("custid",a.getCustid());
				List<CustmerContactDetails> contactdetails=a.getRolescreens();
				int i=1;
				
				if(contactdetails.size()==0)
				{
					
					JSONObject subobject=new JSONObject();
					subobject.put("id",i);
					subobject.put("contactName",null);
					subobject.put("contactNumber",null);
					subobject.put("contactAltNumber",null);
					subobject.put("gdepatment",null);
					subobject.put("email",null);
					subobject.put("gtitle",null);
					mainarray.add(subobject);
					
					 
					
				}
				
				for(CustmerContactDetails b:contactdetails)
				{
					JSONObject subobject=new JSONObject();
					subobject.put("id",i);
					subobject.put("contactName",b.getContactName());
					subobject.put("contactNumber",b.getContactNumber());
					subobject.put("contactAltNumber",b.getContactAltNumber());
					subobject.put("department",b.getDepartment());
					subobject.put("emailAddr",b.getEmailAddr());
					subobject.put("titleName",b.getTitleName());
					mainarray.add(subobject);
					
					i++;
				}
				
			}
			/*mainobject.put("custmer",custmerarray);	 */
			mainobject.put("grid",mainarray );
			//mainobject.put("custmer",custmerarray);
				 
				 return mainobject;
				 
		   }catch(Exception e)
		   {
			   logger.info("=================controllerrrr================"+e.getMessage());
			   return null;
					   
		   }
				 
		}
		 
     
	

	

	
	
	
	
	
	
	/*@RequestMapping(value = "/getAllCustmerDetailsSavedOnId", method = RequestMethod.GET)
    public @ResponseBody JSONObject getAllCustmersDeatails(HttpServletRequest req,HttpServletResponse res) 
 {

		
			String custmername1 = null;

			String custmername = req.getParameter("account");
			if (custmername != null) {
				custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
			}
			JSONObject mainobject = new JSONObject();
			JSONArray mainarray = new JSONArray();
			JSONArray custmerarray = new JSONArray();

			List<CustmerDetails> cutmerdetails = masterservice.getAllCustmersDeatails(custmername1);

			mainobject.put("check", 0);
			for (CustmerDetails a : cutmerdetails)

			{

				String k1 = DateUtils.formatDate(a.getBddate(), Constants.GenericDateFormat.DATE_FORMAT);

				String[] k2 = k1.split("-");
				String k3 = k2[2] + "-" + k2[1] + "-" + k2[0];

				//mainobject.put("date", k2);
				mainobject.put("date", k3);
				mainobject.put("check", 1);
				mainobject.put("custmer", a);
				mainobject.put("custid", a.getCustid());
				List<CustmerContactDetails> contactdetails = a.getRolescreens();
				int i = 1;

				if (contactdetails.size() == 0) {

					JSONObject subobject = new JSONObject();
					subobject.put("id", i);
					subobject.put("contactName", null);
					subobject.put("contactNumber", null);
					subobject.put("contactAltNumber", null);
					subobject.put("gdepatment", null);
					subobject.put("email", null);
					subobject.put("gtitle", null);
					mainarray.add(subobject);

				}

				for (CustmerContactDetails b : contactdetails) {
					JSONObject subobject = new JSONObject();
					subobject.put("id", i);
					subobject.put("contactName", b.getContactName());
					subobject.put("contactNumber", b.getContactNumber());
					subobject.put("contactAltNumber", b.getContactAltNumber());
					subobject.put("gdepatment", b.getGdepatment());
					subobject.put("email", b.getEmail());
					subobject.put("gtitle", b.getGtitle());
					mainarray.add(subobject);

					i++;
				}

			}
			mainobject.put("custmer", custmerarray);
			mainobject.put("grid", mainarray);

			return mainobject;

		
	}
	
*/
	
	
	/*@RequestMapping(value = "/getAllCustmerDetailsChangedSavedOnId", method = RequestMethod.GET)
    public @ResponseBody JSONObject getAllCustmersDeatailsChanged(HttpServletRequest req,HttpServletResponse res) 
    {
	 	 
		   
		   
			 try{
				String custmername1=null;
				 
				 String custmername=req.getParameter("account");
				 if(custmername!=null)
				 {
				   custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
				 }
				 JSONObject mainobject=new JSONObject();
				 JSONArray mainarray=new  JSONArray();
				 JSONArray custmerarray=new  JSONArray();
				 
						 
			List<CustmerDetails>	cutmerdetails= masterservice.getAllCustmersDeatails(custmername1);
			
			mainobject.put("check",0);
			for(CustmerDetails a:cutmerdetails)
				
			{
				JSONObject mainsubobject=new JSONObject();
				
				mainsubobject.put("custid", value);
				mainsub
				
				
				 
				private Integer custid;


				private String account;
			
				private String phnumber;
			
				private String accountid;
		
				private String email;
		
				private String contactname;
		
				private String mbnumber;
		
				private String title;
			
				private String ophnumber;
			
				private String departmentname;
				
				private String fax;
				
		
				private Date bddate;
				
				private String Shippingadress;
		
				private String reports;
				
				private String clientwebsite;
			
				private String leadsource;
		
				private String salesPerson;
				private String mailingadress;
		
				private String salesPersonCode;
		
				private String billingCountries;
			
				private String discountmultiplier;
		
				private String language;
				
				private String paymentTerm;
		
				private String customerGroup;
			
				private String lastmodifiedby;
		
				private String industries;
			
				private String createdby;
				
				private String application;
				
			
				private String corporate;
		
				private String territory;
			
				private String opportunityAmount;

				private String customerStatus;
				
			
				private String imagename;

				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
	
			String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
			
				 
			String[] k2=k1.split("-");
			 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
			
			
	
			
	
				mainobject.put("date",k3);
				mainobject.put("check",1);
				mainobject.put("custmer",a);
				mainobject.put("custid",a.getCustid());
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				List<CustmerContactDetails> contactdetails=a.getRolescreens();
				int i=1;
				if(contactdetails.size()==0)
				{
					
					JSONObject subobject=new JSONObject();
					subobject.put("id",i);
					subobject.put("contactName",null);
					subobject.put("contactNumber",null);
					subobject.put("contactAltNumber",null);
					subobject.put("gdepatment",null);
					subobject.put("email",null);
					subobject.put("gtitle",null);
					mainarray.add(subobject);
					
					 
					
				}
				
				for(CustmerContactDetails b:contactdetails)
				{
					JSONObject subobject=new JSONObject();
					subobject.put("id",i);
					subobject.put("contactName",b.getContactName());
					subobject.put("contactNumber",b.getContactNumber());
					subobject.put("contactAltNumber",b.getContactAltNumber());
					subobject.put("gdepatment",b.getGdepatment());
					subobject.put("email",b.getEmail());
					subobject.put("gtitle",b.getGtitle());
					mainarray.add(subobject);
					
					i++;
				}
				
			}
			mainobject.put("custmer",custmerarray);	 
			mainobject.put("grid",mainarray );
				 
				 return mainobject;
				 
		}
		catch (Exception e)
		{ 
			logger.info("ERROR IN CUSTMER IS DUE TO THE FOLLOWING"+e.getCause(), e);
			System.out.println("ERROR IN CUSTMER IS DUE TO THE FOLLOWING"+e.getMessage());
			return null;
		}
     }
	

*/
	/*siva reddy custmer view*/
	
	
	/*siva reddy autocomplete for account names*/
	
	
	
	
	@RequestMapping(value = "/getAllAccountNames",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllAcccountNames(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		
		List<Object[]> c = masterservice.getAllAccountNames();
		JSONObject mainobject = new JSONObject();

		JSONArray accountarray = new JSONArray();
		ArrayList<String> al = new ArrayList();
		ArrayList<String> al2 = new ArrayList();

		for (Object[] c1 : c) {

			String account = (String) c1[0];
			String companey = (String) c1[1];

			al.add(companey + "-" + account);
			al2.add(account);

		}

	/*	for (String o : al2) {
			al.add(o);

		}*/

		for (String kl : al)

		{
			JSONObject k = new JSONObject();
			k.put("name", kl);
			accountarray.add(k);

		}

		mainobject.put("data", accountarray);

		return mainobject;
	}
	
	
	
	@RequestMapping(value = "/getstautsofcustmers",method = { RequestMethod.GET, RequestMethod.POST })
    public  @ResponseBody  JSONObject getAllAcccountNamesaaaaa(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		JSONObject maninobject=new JSONObject();
		
		String custmername1=null;
		 
		 String custmername=req.getParameter("account");
		 if(custmername!=null)
		 {
		   custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
		 }
		
	int i= masterservice.getCustmerStatus(custmername1); 
	int j=	masterservice. getCustmerStatusOnCustmerName(custmername1);
	
	if(i==0 && j==0)
	{
		maninobject.put("data",0);
	}
	
	else
	{
		maninobject.put("data",1);
	}
	
	
	return maninobject;

	
	}

	
	
	
	
	/*@RequestMapping(value = "/getstautsofcustmers", method = RequestMethod.GET)
    public  @ResponseBody  JSONObject getstautsofcustmers(HttpServletRequest req,HttpServletResponse res) throws Exception
    
	{
		JSONObject mainobject=new JSONObject();
		 

		mainobject.put("data",1);

		return mainobject;
	}
	*/
	
	
	

	

	 	
	
	
	
	//----------------------------------------------- Score Master  Start ------------------------------------------------------//
	
	@RequestMapping(value = "/saveScore", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveScore(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		Score score = null;
		try
		{
			JSONArray scoreData=(JSONArray) jsonData.get("gridData");
			JSONObject formData=(JSONObject) jsonData.get("formData");
			

			for(int i=0;i<scoreData.size();i++)
			{
				ScoreBean scoreBean = new ObjectMapper().readValue(scoreData.get(i).toString(), ScoreBean.class);
				score = prepareModelForScore(scoreBean,formData);
				entityList.add(score);
			}
			masterservice.deleteScore(score);
			isInsertSuccess = masterservice.saveMultipleEntities(entityList);
			
			return isInsertSuccess;
		}
		 catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
		
	}
	
	private Score prepareModelForScore(ScoreBean scoreBean,JSONObject totalData)
	{
		String productType=(String) totalData.get("productType");
		String subType=(String) totalData.get("subType");
		String productTypeCode=(String) totalData.get("productTypeCode");
		String subTypeCode=(String) totalData.get("subTypeCode");
		String frameType="NA";
		Score score=new Score();
		String framesizeFrom=scoreBean.getFrameSizeFrom();
		String framesizeTo=scoreBean.getFrameSizeTo();
		
		if(productType.equalsIgnoreCase("HBB") || productType.equalsIgnoreCase("BBB"))
		{
			if(productType.matches("[a-zA-z0-9]*"))
			{				
				String alphabet=framesizeFrom.replaceAll("[\\d]", "");			
				String[] stbs=framesizeFrom.split(alphabet);
				framesizeFrom=stbs[1];
				
				String alphabet1=framesizeTo.replaceAll("[\\d]", "");			
				String[] stbs1=framesizeTo.split(alphabet1);
				framesizeTo=stbs1[1];
				frameType=stbs[0]+alphabet;
			}
			
			
		}
		score.setId(scoreBean.getId());
		score.setProducttype(productType);
		score.setProducttypecode(Byte.parseByte(productTypeCode));
		score.setSubtype(subType);
		score.setSubtypecode(Byte.parseByte(subTypeCode));
		score.setGuide(scoreBean.getGuide());
		score.setFramesizefrom(Integer.parseInt(framesizeFrom));
		score.setFramesizeto(Integer.parseInt(framesizeTo));
		score.setStage(scoreBean.getStage());
		score.setScore(scoreBean.getScore());
		score.setMountingtype(scoreBean.getMountingType());
		score.setFrametype(frameType);
		return score;
	}
	
	
	@RequestMapping(value = "/getAllScores", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllShifts(@RequestBody JSONObject jsonData) throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		
		String ptcode=(String)jsonData.getString("ptcode");
		String stcode=(String)jsonData.getString("stcode");
		
		List<ScoreBean> scoreBeans =prepareListofScoreBeans(masterservice.loadScoreByProductType(Integer.parseInt(ptcode),Integer.parseInt(stcode)));

		org.json.JSONArray scoreArray = new org.json.JSONArray(scoreBeans);
		
		response.put("Scores",scoreArray);
		
		return response.toString();
	}

	private List<ScoreBean> prepareListofScoreBeans(List<Score> scores)
	{		
		List<ScoreBean> beans = null;
		ScoreBean bean = null;
		if (scores != null && !scores.isEmpty())
		{
			beans = new ArrayList<ScoreBean>();

			for (Score score : scores)
			{
				bean = new ScoreBean();	
				bean.setId(score.getId());
				bean.setProductType(score.getProducttype());
				bean.setProductTypeCode(score.getProducttypecode());
				bean.setSubType(score.getSubtype());
				bean.setSubTypeCode(score.getSubtypecode());
				bean.setGuide(score.getGuide());
				if(score.getProducttype().equalsIgnoreCase("HBB") || score.getProducttype().equalsIgnoreCase("BBB"))
				{
					if(!(score.getFrametype().equalsIgnoreCase("NA")))
					{
						String frameFrom=score.getFrametype()+score.getFramesizefrom();
						String frameTo=score.getFrametype()+score.getFramesizeto();
						
						bean.setFrameSizeFrom(frameFrom);
						bean.setFrameSizeTo(frameTo);
					}
					else
					{
						bean.setFrameSizeFrom(score.getFramesizefrom().toString());
						bean.setFrameSizeTo(score.getFramesizeto().toString());
					}
				}
				else
				{
					bean.setFrameSizeFrom(score.getFramesizefrom().toString());
					bean.setFrameSizeTo(score.getFramesizeto().toString());
				}
				bean.setStage(score.getStage());
				bean.setScore(score.getScore());
				bean.setMountingType(score.getMountingtype());
				beans.add(bean);
			}
		}


		return beans;
	}
	
	//----------------------------------------------- Score Master  End ------------------------------------------------------//
	 @RequestMapping(value = "/getSequenceNumber", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	 public @ResponseBody String getSequenceNumber(@RequestBody String jsonData,HttpServletRequest request) throws Exception 
	 {
			org.json.JSONObject response = new org.json.JSONObject();
			 HttpSession session=request.getSession();
			 String countryCode=(String)session.getAttribute("countrycode");
			 String usernasme=(String)session.getAttribute("user");

	
			String seqno=commonService.getSequenceNumber(jsonData,countryCode,usernasme);			
	
			response.put("sequence",seqno);		
			
			return response.toString();
	 }
	 
	 
	 
	 
	 
	 @RequestMapping(value = "/getToDaysDate", method = RequestMethod.GET)
	    public  @ResponseBody  JSONObject getToDaysDate(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
			JSONObject  todaysdateobject =  new JSONObject();
			
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			
			todaysdateobject.put("bddate",day+"-"+month+"-"+year);
			
			return  todaysdateobject;
		}
		
	 
	 
	 @RequestMapping(value = "/getCreditstatus",method = { RequestMethod.GET, RequestMethod.POST })
	    public  @ResponseBody  JSONObject getCeriditStatus(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 JSONObject j=new JSONObject();
		 JSONArray jarry=new JSONArray();
			List<CreditStatus> c = masterservice.getAllCreditStatus();
			for(CreditStatus cs:c)
			{
				 
				 JSONObject subobject=new JSONObject();
				 subobject.put("id",cs.getCreditstatus());
				 subobject.put("creditStatus",cs.getCreditstatusname());
				 jarry.add(subobject);
				
			}
			j.put("data",jarry);
			return j ;
		}
		
	 
	 
	 
	 
	 @RequestMapping(value = "/getOutPutSpeed",method = { RequestMethod.GET, RequestMethod.POST })
	    public  @ResponseBody  JSONObject getOytputSpeed(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 JSONObject j=new JSONObject();
		 JSONArray jarry=new JSONArray();
		 
		String rpm= req.getParameter("rpm");
		String ratio=req.getParameter("ratio");
		int outspeed=0;
		if(rpm!=null && ratio!=null )
		{
			int rpm1=Integer.parseInt(rpm);
			int ratio1=Integer.parseInt(ratio);
			outspeed=rpm1/ratio1;
			
		}
		
		
	 
	 
			j.put("data",outspeed);
			return j ;
		}
		
		
	 
	 @RequestMapping(value = "/validateCustomerCodeDuplicate",method = { RequestMethod.GET, RequestMethod.POST })
	    public  @ResponseBody  boolean  getCountryorderCheck(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 int p=0;
		 Session session=hibernateDao.getSessionFactory().openSession();
			String account= req.getParameter("account");
			String custid=req.getParameter("validateDuplicate");
			Integer custidcheck=0;
			if(custid!=null)
			{
			 custidcheck=Integer.parseInt(custid);
			}
			
			
			if(custidcheck==0)
			{
				
				String q="select count(account) from CustmerDetails where account='"+account+"'";
				
				
				Query a=session.createQuery(q);
				List<Long> l=a.list();
				if(l.size()!=0)
				{
				 for(Long k:l)
				 {
					p= k.intValue();
				 }
				
				}
			if(p==0)
			{
				return true;
			}
				
			else
			{
				return false;
			}
			
			
			

			
				
			}
			else
			{
				

				
				String q="select count(account) from CustmerDetails where account='"+account+"' and custid!="+custidcheck+"" ;
				
				
				Query a=session.createQuery(q);
				List<Long> l=a.list();
				if(l.size()!=0)
				{
				 for(Long k:l)
				 {
					p= k.intValue();
				 }
				
				}
			if(p==0)
			{
				return true;
			}
				
			else
			{
				return false;
			}
			
			}
			 
	 
		}
	 
	 
	 
	 
	 @RequestMapping(value = "/gettingcustmernamesandcustmercodes",method = { RequestMethod.GET, RequestMethod.POST })
	    public  @ResponseBody  JSONObject  gettingcustmernamesandcustmercodes(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 
		 Session session=hibernateDao.getSessionFactory().openSession();
		String account= req.getParameter("custmername");
		String firstLetter=null;
		JSONObject mainobject=new JSONObject();
		JSONArray mainarray=new JSONArray();
		if(account!=null)
		{
		    firstLetter =account.substring(0, 1);

		}
		
		if(account!=null)
		{
		
		String q1="select CONCAT(accountid,'-',account) From CustmerDetails where account like '"+firstLetter+"%' order by accountid";
		SQLQuery one=session.createSQLQuery(q1);
		List<String> values=one.list();
		//JSONObject mainobject=new JSONObject();
		//JSONArray mainarray=new JSONArray();
		if(values.size()!=0)
		{
			for(String k:values)
			{
				JSONObject subobject=new JSONObject();
				subobject.put("name",k);
				mainarray.add(subobject);
				
			}
		}
		
		if(values.size()==0)
		{
			
			String q2="select CONCAT(accountid,'-',account) From CustmerDetails where accountid like '"+firstLetter+"%' order by accountid";
			SQLQuery two=session.createSQLQuery(q2);
			List<String> values2=two.list();
			
			if(values2.size()!=0)
			{
				for(String p:values2)
				{
					JSONObject subobject=new JSONObject();
					subobject.put("name",p);
					mainarray.add(subobject);
					
				}
			}
			
		}
			 
			
			 
			 
		mainobject.put("data",mainarray);
			

		}	
				
			 
			 return mainobject;
	 
		}
	 
	 
	 @RequestMapping(value = "/getAllApplicationCodes",method = { RequestMethod.GET, RequestMethod.POST })
	    public  @ResponseBody  JSONObject  getApplicationCodes(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 
		 Session session=hibernateDao.getSessionFactory().openSession();
		String machinecode= req.getParameter("appCode");
		String firstLetter=null;
		JSONObject mainobject=new JSONObject();
		JSONArray mainarray=new JSONArray();
		
		if(machinecode!=null)
		{
		    firstLetter =machinecode.substring(0, 1);

		}
		
		if(machinecode!=null)
		{
		String q1="select distinct machinecode From ApplicationIndustryCode where machinecode like'"+firstLetter+"%' order by machinecode";
		SQLQuery one=session.createSQLQuery(q1);
		List<String> values=one.list();
		 
		if(values.size()!=0)
		{
			for(String k:values)
			{
				JSONObject subobject=new JSONObject();
				subobject.put("name",k);
				mainarray.add(subobject);
				
			}
		}
		
		 
			
			 
			 
		mainobject.put("data",mainarray);
			

		}
				
			 
			 return mainobject;
	 
		}

	 
	 

	 
	 @RequestMapping(value = "/getAllIndustrynames",method = { RequestMethod.GET, RequestMethod.POST })
	    public  @ResponseBody  JSONObject  getAllIndustrynames(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 
		 Session session=hibernateDao.getSessionFactory().openSession();
		 
		String firstLetter=null;
		JSONObject mainobject=new JSONObject();
		JSONArray mainarray=new JSONArray();
		
	 
	 
		String q1="select distinct totalname From ApplicationIndustryCode   order by totalname";
		SQLQuery one=session.createSQLQuery(q1);
		List<String> values=one.list();
		 
		if(values.size()!=0)
		{
			for(String k:values)
			{
				JSONObject subobject=new JSONObject();
				subobject.put("applicanIndustry",k);
				mainarray.add(subobject);
				
			}
		}
		
		 
			
			 
			 
		mainobject.put("data",mainarray);
			

	 
				
			 
			 return mainobject;
	 
		}

	 


	 
	 
	 @RequestMapping(value = "/getApplicationCodes",method = { RequestMethod.GET, RequestMethod.POST })
	    public  @ResponseBody  JSONObject  getApplicationCodesSingle(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
		 
		 Session session=hibernateDao.getSessionFactory().openSession();
		String industryname= req.getParameter("getAppCode");
		//String firstLetter=null;
		JSONObject mainobject=new JSONObject();
		JSONObject mainmainobject=new JSONObject();
		JSONArray mainarray=new JSONArray();
		
		 
		
		
		
		String q1="select machinecode From ApplicationIndustryCode where totalname= '"+industryname+"'";
		SQLQuery one=session.createSQLQuery(q1);
		List<String> values=one.list();
		 
		if(values.size()!=0)
		{
			for(String k:values)
			{
				 
				mainobject.put("application",k);
				 
				
			}
			
			
			mainarray.add(mainobject);
			
			
		}
		
		 
			
			 
			 
	 

		mainmainobject.put("data",mainarray);
				
			 
			 return mainmainobject;
	 
		}

	 /**********************************************************************************************/
	 /******************************************ProspectMaster**************************************/
	 /**********************************************************************************************/

	 
	 
	 @RequestMapping(value = "/prospectcustomerFormSubmit", method = RequestMethod.POST)
	    public  @ResponseBody boolean  custmerprospectsaveSave(MultipartHttpServletRequest req) throws Exception
		{
		
		 ServletContext servletContext	= req.getSession().getServletContext();
			//ServletContext servletContext	= req.getServletContext();
			String ss=req.getParameter("formData");
			//String description1=req.getParameter("fieldData");
			String ss1=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(ss1);
		    List<MultipartFile> mpf=req.getFiles("usersign");
			
		 String imapath=this.getprospectImagepathOfCustmerTemp(mpf,servletContext);
		 
		
		


		 CustmerMasterBean productageorey = new ObjectMapper().readValue(ss,CustmerMasterBean.class);
		
		 
		if(imapath!=null)
		{
		productageorey.setImagename(imapath);
		}
		
		
		
		 
		
		System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR------------------------->"+productageorey.getCustid());
		
		
		 
		
		
		ArrayList<CustmerMasterContactDetailsBean> plk=new ArrayList<CustmerMasterContactDetailsBean>(); 
		
		for(int i=0;i<gData.length();i++)
		{
			 
			CustmerMasterContactDetailsBean CustmerContactDetails = new ObjectMapper().readValue(gData.get(i).toString(), CustmerMasterContactDetailsBean.class);
			 
	      plk.add(CustmerContactDetails);
	      
		}
		
		
		if(productageorey.getCustid()!=0)
		{
			masterservice.deleteofProspectCutmerContactDetails(productageorey.getCustid());
			/*masterservice.deleteCustmer(productageorey.getCustid());*/
			
			ProspectCustmerDetails custmerdetailsdata=this.getprospectCustmerMasterData(productageorey);
			custmerdetailsdata.setCustid(productageorey.getCustid());
			List<ProspectCustmerContactDetails> ccdl=this.getprospectCustmerContactDetails(plk);
			custmerdetailsdata.setRolescreens(ccdl);
		   masterservice.saveProspectCustmer(custmerdetailsdata);
			
			/*masterservice.updateCustmer(productageorey,plk);
			masterservice.saveContactCustmerDetails(productageorey,plk);*/
			
	 
	 
		}
		 
	 
		if(productageorey.getCustid()==0)
		{
		
			ProspectCustmerDetails custmerdetailsdata=this.getprospectCustmerMasterData(productageorey);
			List<ProspectCustmerContactDetails> ccdl=this.getprospectCustmerContactDetails(plk);

			//List<CustmerContactDetails> ccdl=this.getCustmerContactDetails(plk);
			custmerdetailsdata.setRolescreens(ccdl);
		    //masterservice.saveCustmer(custmerdetailsdata);
			masterservice.saveProspectCustmer(custmerdetailsdata);
		
		}
		 
		
			 
			return true;
			
			
	 
		}
	 
	 
	 private List<ProspectCustmerContactDetails> getprospectCustmerContactDetails(ArrayList<CustmerMasterContactDetailsBean> plk) {
			// TODO Auto-generated method stub
			ArrayList<ProspectCustmerContactDetails> ccd1=new ArrayList<ProspectCustmerContactDetails>();
			for(CustmerMasterContactDetailsBean custmercontact:plk)
			{
				ProspectCustmerContactDetails c=new ProspectCustmerContactDetails();
				 c.setContactName(custmercontact.getContactName()); 
			     c.setContactNumber(custmercontact.getContactNumber());
				 c.setContactAltNumber(custmercontact.getContactAltNumber()); 
				 c.setDepartment(custmercontact.getDepartment()); 
				 c.setEmailAddr(custmercontact.getEmailAddr());
			     c.setTitleName(custmercontact.getTitleName()); 
			     ccd1.add(c);
				
			}
			return ccd1;
		}



		private ProspectCustmerDetails getprospectCustmerMasterData(CustmerMasterBean productageorey) 
		{
			ProspectCustmerDetails cdetails=new ProspectCustmerDetails();
			cdetails.setCustomerStatus(productageorey.getCustomerStatus());  
			cdetails.setAccount(productageorey.getAccount());
			cdetails.setAccountid(productageorey.getAccountid());
			cdetails.setClientwebsite(productageorey.getClientwebsite());
			cdetails.setMailingadress(productageorey.getMailingadress());
			cdetails.setShippingadress(productageorey.getShippingadress());
			cdetails.setBillingCountries(productageorey.getBillingCountries());
			cdetails.setLanguage(productageorey.getLanguage());
			cdetails.setCustomerType(productageorey.getCustomerGroup());
			cdetails.setIndustries(productageorey.getIndustries());
			cdetails.setApplication(productageorey.getApplication());
			cdetails.setTerritory(productageorey.getTerritory());
			cdetails.setCorporate(productageorey.getCorporate());
			cdetails.setContName(productageorey.getContName());
			cdetails.setTitle(productageorey.getTitle());
			cdetails.setPhnumber(productageorey.getPhnumber());
			cdetails.setMbnumber(productageorey.getMbnumber());
			cdetails.setOphnumber(productageorey.getOphnumber());
			cdetails.setEmail(productageorey.getEmail());
			cdetails.setFax(productageorey.getFax());
			cdetails.setDepartmentname(productageorey.getDepartmentname());
			cdetails.setReports(productageorey.getReports());
			cdetails.setSalesPerson(productageorey.getSalesPerson());
		    cdetails.setSalesPersonCode(productageorey.getSalesPersonCode());
			cdetails.setLeadsource(productageorey.getLeadsource()); 
			cdetails.setCurrency(productageorey.getCurrency());
			cdetails.setTaxName(productageorey.getTaxName());
			cdetails.setPaymentTerm(productageorey.getPaymentTerm());
		    cdetails.setOpportunityAmount(productageorey.getOpportunityAmount());
		    cdetails.setCreatedby(productageorey.getCreatedby());
		    cdetails.setLastmodifiedby(productageorey.getLastmodifiedby());
		    cdetails.setDiscountmultiplier(productageorey.getDiscountmultiplier());
		    cdetails.setCreditstatus(productageorey.getCreditStatus());
		    cdetails.setApplicanindustryname(productageorey.getApplicanIndustry());
		    
		    cdetails.setImagename(productageorey.getImagename());
			 
		    if (productageorey.getBddate() != null) {
				//String k1 = DateUtils.formatDate(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
				Date bddadte = DateUtils.getSqlDateFromString(productageorey.getBddate(), Constants.GenericDateFormat.DATE_FORMAT);
				cdetails.setBddate(bddadte);
			}
			return cdetails;
		}



		private String getprospectImagepathOfCustmerTemp(List<MultipartFile> mpf,ServletContext servletContext) throws Exception
		{
			String photoName=null;
			String photoPath=null;
		 int i=0;
			for(MultipartFile mf:mpf)
			{
				
				String fname=mf.getOriginalFilename();
				//logger.info("--------------"+mf.getOriginalFilename());
				
				
				if (mf.getSize() != 0) 
				{
					Random t = new Random();
					int num = t.nextInt(10000);
					
					String exts = getFileExtension(mf.getOriginalFilename());
					photoName = i + num + "_photo" + "." + exts;
					/*if(i==0){
					photoPath = "custmer/" + photoName;
					}*/
					 
						 photoPath="prospectcustmer/" + photoName;
						//photoPath = photoPath+","+photoPath1;
					 

					File dir1 = new File(servletContext.getRealPath("/") + "prospectcustmer/");

					if (!dir1.exists())
						dir1.mkdirs();

					if (mf.getSize() != 0) 
					{
						byte[] bytes1 = mf.getBytes();

						File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
						BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
						stream1.write(bytes1);
						stream1.close();
					}

				}
				i++;
			}
		
			return photoPath;

		}


		
		
		@RequestMapping(value = "/getAllProspectAccountNames",method = { RequestMethod.GET, RequestMethod.POST })
	    public  @ResponseBody  JSONObject getAllProspectCustmerAcccountNames(HttpServletRequest req,HttpServletResponse res) throws Exception
	    
		{
			
			List<Object[]> c = masterservice.getAllprospectAccountNames();
			JSONObject mainobject = new JSONObject();

			JSONArray accountarray = new JSONArray();
			ArrayList<String> al = new ArrayList();
			ArrayList<String> al2 = new ArrayList();

			for (Object[] c1 : c) {

				String account = (String) c1[0];
				String companey = (String) c1[1];

				al.add(companey + "-" + account);
				al2.add(account);

			}

		/*	for (String o : al2) {
				al.add(o);

			}*/

			for (String kl : al)

			{
				JSONObject k = new JSONObject();
				k.put("name", kl);
				accountarray.add(k);

			}

			mainobject.put("data", accountarray);

			return mainobject;
		}


		
		
		@RequestMapping(value = "/getAllprospectCustmerDetailsSavedOnId", method = RequestMethod.POST)
	    public @ResponseBody JSONObject prospectcustmerdatabybasingoncustmercode(HttpServletRequest req,HttpServletResponse res) 
	    {
		 	 
			   try{
			   
				 
					String custmername1=null;
					String applicationname=null;
					 
					 String custmername=req.getParameter("account");
					 if(custmername!=null)
					 {
					   custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
					 }
					 JSONObject mainobject=new JSONObject();
					 JSONArray mainarray=new  JSONArray();
					// JSONArray custmerarray=new  JSONArray();
					 
							 
				List<ProspectCustmerDetails>	cutmerdetails= masterservice.getAllprospectCustmersDeatails(custmername1);
				
				mainobject.put("check",0);
				for(ProspectCustmerDetails a:cutmerdetails)
					
				{
					JSONObject submainobject=new JSONObject();
					JSONArray custmerarray=new  JSONArray();
					submainobject.put("custid",a.getCustid());
					
					submainobject.put("customerStatus",a.getCustomerStatus());
					submainobject.put("account",a.getAccount());
					submainobject.put("accountid",a.getAccountid());
					submainobject.put("clientwebsite",a.getClientwebsite());
					submainobject.put("mailingadress",a.getMailingadress());
					submainobject.put("shippingadress",a.getShippingadress());
					submainobject.put("billingCountries",a.getBillingCountries());
					submainobject.put("language",a.getLanguage());
					submainobject.put("customerGroup",a.getCustomerType());
					submainobject.put("industries",a.getIndustries());
					submainobject.put("application",a.getApplication());
					submainobject.put("territory",a.getTerritory());
					submainobject.put("corporate",a.getCorporate());
					submainobject.put("contName", a.getContName());
					submainobject.put("title", a.getTitle());
					submainobject.put("phnumber",a.getPhnumber());
					submainobject.put("mbnumber",a.getMbnumber());
					submainobject.put("ophnumber",a.getOphnumber());
					submainobject.put("creditStatus",a.getCreditstatus());
					submainobject.put("email",a.getEmail());
					submainobject.put("fax", a.getFax());
					if(a.getApplicanindustryname()==null && a.getApplication()!=null)
					{
						//String applicationcode=req.getParameter( "applicanIndusName");
						
						List<ApplicationIndustryCode> coutries=migratesevice.getAllApplicationCodeName(a.getApplication());
						 
						for(ApplicationIndustryCode a2:coutries)
						{
							
							applicationname=a2.getTotalname();
							
						}
						 
						submainobject.put("applicanIndustry",applicationname);
						 
					}
					
					else
					{
						submainobject.put("applicanIndustry",a.getApplicanindustryname());
					}
					 
					submainobject.put("departmentname",a.getDepartmentname());
					submainobject.put("reports",a.getReports());
					if(a.getSalesPerson()!=null)
					{
						submainobject.put("salesPerson",a.getSalesPerson());
						
					}
					
					else
					{
						String salesman=masterservice.getSalesmanOnSalesCode(a.getSalesPersonCode());
						submainobject.put("salesPerson",salesman);
						
						
					}
					 
					submainobject.put("discountmultiplier",a.getDiscountmultiplier());
					submainobject.put("salesPersonCode",a.getSalesPersonCode());
					submainobject.put("leadsource",a.getLeadsource());
					submainobject.put("currency", a.getCurrency());
					submainobject.put("taxName",a.getTaxName());
					submainobject.put("paymentTerm",a.getPaymentTerm());
					submainobject.put("opportunityAmount", a.getOpportunityAmount());
					if(a.getBddate()!=null)
					{
						String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
						submainobject.put("bddate",k1 );
					}
					
					
					submainobject.put("createdby",a.getCreatedby());
					submainobject.put("lastmodifiedby",a.getLastmodifiedby());
					submainobject.put("imagename", a.getImagename());
					 
					custmerarray.add(submainobject);
					mainobject.put("custmer",custmerarray);
				    
					
					 
					 /*custmerobject.put("account",a);
					custmerarray.add(custmerobject);*/
			/*	String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
				String k2 = k1.replace("-","/");
					 
				String[] k2=k1.split("-");
				 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
				*/
				
				/*DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");*/
				
			/*	Date frmDate = sdf.parse(k2);*/
				/*	mainobject.put("date",k2);*/
					//mainobject.put("date",k1);
					mainobject.put("check",1);
					//mainobject.put("custmer",a);
					//mainobject.put("custid",a.getCustid());
					List<ProspectCustmerContactDetails> contactdetails=a.getRolescreens();
					int i=1;
					
					if(contactdetails.size()==0)
					{
						
						JSONObject subobject=new JSONObject();
						subobject.put("id",i);
						subobject.put("contactName",null);
						subobject.put("contactNumber",null);
						subobject.put("contactAltNumber",null);
						subobject.put("gdepatment",null);
						subobject.put("email",null);
						subobject.put("gtitle",null);
						mainarray.add(subobject);
						
						 
						
					}
					
					for(ProspectCustmerContactDetails b:contactdetails)
					{
						JSONObject subobject=new JSONObject();
						subobject.put("id",i);
						subobject.put("contactName",b.getContactName());
						subobject.put("contactNumber",b.getContactNumber());
						subobject.put("contactAltNumber",b.getContactAltNumber());
						subobject.put("department",b.getDepartment());
						subobject.put("emailAddr",b.getEmailAddr());
						subobject.put("titleName",b.getTitleName());
						mainarray.add(subobject);
						
						i++;
					}
					
				}
				/*mainobject.put("custmer",custmerarray);	 */
				mainobject.put("grid",mainarray );
				//mainobject.put("custmer",custmerarray);
					 
					 return mainobject;
					 
			   }catch(Exception e)
			   {
				   logger.info("=================controllerrrr================"+e.getMessage());
				   return null;
						   
			   }
			   
	    }
			 	 
	 
}