package com.finsol.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.finsol.bean.UsersBean;
import com.finsol.model.Users;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.UserManagementService;


/**
 * @author Rama Krishna
 *
 */
@Controller
public class UserManagementController {
	
	private static final Logger logger = Logger.getLogger(UserManagementController.class);
	
	@Autowired
	private UserManagementService userManagementService;
	
	@Autowired
	private BSS_CommonService commonService;
	
	/*@Autowired
    private ServletContext servletContext;*/
	
	//------------------------------ Users ----------------------------------------//
	
	@RequestMapping(value = "/saveUsers", method = RequestMethod.POST)
   // public @ResponseBody boolean saveUsers(@RequestParam("userphoto") MultipartFile file,@RequestParam("usersign") MultipartFile usersign,@RequestParam("formData") String formData,HttpServletRequest req) throws Exception
	 public @ResponseBody boolean saveUsers(@RequestParam("formData") String formData,HttpServletRequest req) throws Exception
    {
	 	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		String ext=null;
		String ext1=null;		
		String photoName=null;
		String signName=null;		
		String photoPath=null;
		String signaturePath=null;
		
		
		try 
		{
/*			ServletContext servletContext=req.getServletContext();
			Integer maxId=commonService.getMaximumID("userid","Users");
			
		    UsersBean usersBean = new ObjectMapper().readValue(formData.toString(), UsersBean.class);
		    if(usersBean.getUserId()==null)
		    {
				ext=getFileExtension(file.getOriginalFilename());
				ext1=getFileExtension(usersign.getOriginalFilename());
				photoName=maxId+"_photo"+"."+ext;
				signName=maxId+"_sign"+"."+ext1;
				
				photoPath="images/"+photoName;
				signaturePath="images/"+signName;
				 
				usersBean.setPhotoPath(photoPath);
				usersBean.setSignaturePath(signaturePath);
				//usersBean.setUserId(maxId);
		    }
		    else
		    { 	
				if(file.getSize()!=0)
				{						
					ext=getFileExtension(file.getOriginalFilename());
					photoName=usersBean.getUserId()+"_photo"+"."+ext;
					photoPath="images/"+photoName;
					usersBean.setPhotoPath(photoPath);
				}
				if(usersign.getSize()!=0)
				{						
					ext1=getFileExtension(usersign.getOriginalFilename());						
					signName=usersBean.getUserId()+"_sign"+"."+ext1;						
					signaturePath="images/"+signName;
					usersBean.setSignaturePath(signaturePath);
				}

		    }*/
			 UsersBean usersBean = new ObjectMapper().readValue(formData.toString(), UsersBean.class);
		    Users users = prepareModelForEmployees(usersBean);		    
		    entityList.add(users);		    
			isInsertSuccess = userManagementService.saveEntities(entityList);
			
			if(isInsertSuccess)
			{/*
				isInsertSuccess =false;
				
				File dir1=new File(servletContext.getRealPath("/")+"images/");
				
				if (!dir1.exists())
					dir1.mkdirs();
				
				if(file.getSize()!=0)
				{					
					byte[] bytes1 = file.getBytes();
	
					File serverFile1 = new File(dir1.getAbsolutePath()+ File.separator + photoName);					
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}
				if(usersign.getSize()!=0)
				{
					byte[] bytes2 = usersign.getBytes();
					
					File serverFile2 = new File(dir1.getAbsolutePath()+ File.separator + signName);					
					BufferedOutputStream stream2 = new BufferedOutputStream(new FileOutputStream(serverFile2));
					stream2.write(bytes2);
					stream2.close();
				}
				isInsertSuccess =true;
			*/}
		   return isInsertSuccess;
		}
		catch (Exception e)
		{ 
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
     }
	
		 
	public  String getFileExtension(String fileName) 
	{
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
	}
		 
	 private Users prepareModelForEmployees(UsersBean usersBean)
	 {
		 Users users = new Users();	
		 users.setEmailid(usersBean.getEmailId());
		 users.setCountrycode(usersBean.getCountryCode());
		 users.setLoginid(usersBean.getLoginId());
		// users.setSignaturelocation(usersBean.getSignaturePath());
		 //users.setPhotolocation(usersBean.getPhotoPath());
		 users.setUserfirstname(usersBean.getUserFirstName());
		 users.setUserlastname(usersBean.getUserLastName());
		 users.setRoleid(usersBean.getRoleId());
		 users.setStatus(usersBean.getStatus());
		 users.setIspwdchanged((byte)1);
		 users.setUserid(usersBean.getUserId());
		 users.setUtctimezone(usersBean.getUtctimeZone());
		 users.setSalesmancode(usersBean.getSalesManCode());
		 if(!(usersBean.getLoginId().equalsIgnoreCase("sca")))
		 {
			 users.setPassword("sca360");
		 }
		 else
			 users.setPassword(usersBean.getPassword());

			
		 return users;
	}
		 
		 
		 @RequestMapping(value = "/getAllUsers", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
		 public @ResponseBody List<UsersBean> getAllEmployees() throws Exception 
		 {
			List<UsersBean> usersBeans=prepareListofUsersBean(userManagementService.listUsers());
			return usersBeans;
		}
		 private List<UsersBean> prepareListofUsersBean(List<Users> users)
		 {
				List<UsersBean> beans = null;
				if(users != null && !users.isEmpty())
				{
					beans = new ArrayList<UsersBean>();
					UsersBean bean = null;
					for(Users user :  users)
					{
						bean = new UsersBean();					
						bean.setUserId(user.getUserid());
						bean.setPhotoPath(user.getPhotolocation());
						bean.setSignaturePath(user.getSignaturelocation());
						bean.setUserFirstName(user.getUserfirstname());
						bean.setUserLastName(user.getUserlastname());
						bean.setLoginId(user.getLoginid()); 
						bean.setEmailId(user.getEmailid());
						bean.setRoleId(user.getRoleid());
						bean.setCountryCode(user.getCountrycode());
						bean.setStatus(user.getStatus());
						bean.setUtctimeZone(user.getUtctimezone());
						bean.setPassword(user.getPassword());
						bean.setSalesManCode(user.getSalesmancode());
						beans.add(bean);
					}
				}
				return beans;
			}
/*
		 @RequestMapping(value = "/getAllEmployeesForUser", method = RequestMethod.POST, headers = { "Content-type=application/json" })
			public @ResponseBody
			String getAllEmployeesForUser(@RequestBody String jsonData) throws Exception
			{
			 	EmployeesBean employeesBean =null;				
			 	jsonResponse = new org.json.JSONObject();
				
				Employees employees=employeesService.getEmployees(Integer.parseInt(jsonData));
			
				if(employees!=null)
					employeesBean =prepareBeanForEmployees(employees);
					
				org.json.JSONObject advanceJson=new org.json.JSONObject(employeesBean);
				jsonResponse.put("employees",advanceJson);	
				
				
				return jsonResponse.toString();
			}
		 private EmployeesBean prepareBeanForEmployees(Employees employees) 
		{
				
			 	EmployeesBean bean = new EmployeesBean();
				bean.setEmployeeID(employees.getEmployeeid());
				bean.setEmployeeName(employees.getEmployeename().toUpperCase());
				bean.setDesignationCode(employees.getDesignationcode());
				bean.setDeptCode(employees.getDeptcode());
				bean.setLoginID(employees.getLoginid());
				bean.setDateOfBirth(DateUtils.formatDate(employees.getDateofbirth(),Constants.GenericDateFormat.DATE_FORMAT)); 
				//bean.setDateOfBirth(employees.getDateofbirth().toString());
				bean.setEmailID(employees.getEmailid());
				bean.setGender(employees.getGender());
				bean.setMaritalStatus(employees.getMaritalstatus());
				bean.setPhoneNo(employees.getPhoneno());
				bean.setMobileNo(employees.getMobileno());
				bean.setPhoneNo2(employees.getPhoneno2());
				bean.setRoleID(employees.getRoleid());
				bean.setStatus(employees.getStatus());
			return bean;
		}*/

}
