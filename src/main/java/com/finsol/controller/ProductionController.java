package com.finsol.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.bean.PackageDetailsBean;
import com.finsol.bean.ProductionSchedulerBean;
import com.finsol.model.PackageDetails;
import com.finsol.model.ProductionScheduler;
import com.finsol.model.ProductionSchedulerEvents;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.ProductionService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @author Rama Krishna
 *
 */

@Controller
public class ProductionController 
{	
	private static final Logger logger = Logger.getLogger(ProductionController.class);
	
	@Autowired
	private BSS_CommonService commonService;
	
	/*@Autowired
    private ServletContext servletContext;
	*/
	@Autowired
	private ProductionService productionService;
	
	
	
	
	 @RequestMapping(value = "/searchProductionScheduler", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	 public @ResponseBody String searchProductionScheduler(@RequestBody JSONObject jsonData) throws Exception 
	 {
			org.json.JSONObject response = new org.json.JSONObject();
	
			String name=null;
			String item=(String)jsonData.getString("item");			
			String value=(String)jsonData.getString("value");			
			int val=Integer.parseInt(value);
			
			if(val==0)			
				name="name";
			else if(val==1)
				name="salesorder";
			else
				name="modelno";
			
			
			List<ProductionSchedulerBean> productionSchedulerBeans =prepareBeanForProductionSchedulerSearch(productionService.loadProductionSchedulerSearch(name,item));
			
			org.json.JSONArray psArray = new org.json.JSONArray(productionSchedulerBeans);
	
			response.put("productionscheduler",psArray);
			
			return response.toString();			
	 }

	private List<ProductionSchedulerBean> prepareBeanForProductionSchedulerSearch(List<ProductionScheduler> productionSchedulers) 
	{
			List<ProductionSchedulerBean> beans = null;
			
			if (productionSchedulers != null && !productionSchedulers.isEmpty()) 
			{  
				beans = new ArrayList<ProductionSchedulerBean>();
				ProductionSchedulerBean bean = null;
				for (ProductionScheduler productionScheduler : productionSchedulers) 
				{					
					bean = new ProductionSchedulerBean();
					bean.setSalesOrder(productionScheduler.getSalesorder());
					bean.setContractno(productionScheduler.getContractno());
					bean.setModelno(productionScheduler.getModelno());
					bean.setPid(productionScheduler.getPid());
					bean.setQuantity(productionScheduler.getQuantity());
					bean.setScore(productionScheduler.getScore());;
					bean.setReqDeliveryDate(DateUtils.formatDate(productionScheduler.getReqdeliverydate(),Constants.GenericDateFormat.DATE_FORMAT));
					if(productionScheduler.getScore()!=null)
						bean.setTotalScore(productionScheduler.getScore()*productionScheduler.getQuantity());
					
					if(productionScheduler.getAssemblydate()!=null)
						bean.setAssemblyDate(DateUtils.formatDate(productionScheduler.getAssemblydate(),Constants.GenericDateFormat.DATE_FORMAT));
						
					bean.setProdStatus(productionScheduler.getProdstatus());
					
				if(productionScheduler.getDuedate()!=null)
						bean.setDueDate(DateUtils.formatDate(productionScheduler.getDuedate(),Constants.GenericDateFormat.DATE_FORMAT));
					
					beans.add(bean);						
				}
			}						
			return beans;
	}
	
	
	
	//---------------------------------------------------//
	 @RequestMapping(value = "/loadProductionScheduler", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	 public @ResponseBody String loadProductionScheduler(@RequestBody String jsonData) throws Exception 
	 {
			org.json.JSONObject response = new org.json.JSONObject();
	
			List<ProductionSchedulerBean> productionSchedulerBeans =prepareBeanForProductionScheduler(productionService.loadProductionSchedulerData(jsonData),jsonData);
			
			org.json.JSONArray psArray = new org.json.JSONArray(productionSchedulerBeans);
	
			response.put("productionscheduler",psArray);
			
			return response.toString();			
	 }

	private List<ProductionSchedulerBean> prepareBeanForProductionScheduler(List<ProductionScheduler> productionSchedulers,String jsonData) 
	{
			List<ProductionSchedulerBean> beans = null;
			
			if (productionSchedulers != null && !productionSchedulers.isEmpty()) 
			{  
				beans = new ArrayList<ProductionSchedulerBean>();
				ProductionSchedulerBean bean = null;
				for (ProductionScheduler productionScheduler : productionSchedulers) 
				{					
					bean = new ProductionSchedulerBean();
					bean.setSalesOrder(productionScheduler.getSalesorder());
					bean.setContractno(productionScheduler.getContractno());
					bean.setModelno(productionScheduler.getModelno());
					bean.setPid(productionScheduler.getPid());
					bean.setCountry(productionScheduler.getCountry());
					bean.setName(productionScheduler.getName());
					bean.setQuantity(productionScheduler.getQuantity());
					bean.setScore(productionScheduler.getScore());;
					bean.setReqDeliveryDate(DateUtils.formatDate(productionScheduler.getReqdeliverydate(),Constants.GenericDateFormat.DATE_FORMAT));
					if(productionScheduler.getScore()!=null)
						bean.setTotalScore(productionScheduler.getScore()*productionScheduler.getQuantity());
					
					bean.setId(productionScheduler.getId());
					if(jsonData.indexOf("-")!=-1)
					{
						if(productionScheduler.getCompleteddate()!=null)
						{
							bean.setCompletedDate(DateUtils.formatDate(productionScheduler.getCompleteddate(),Constants.GenericDateFormat.DATE_FORMAT));
							//bean.setCompletedDate(productionScheduler.getCompleteddate()+"T18:30");
						}
							if(productionScheduler.getStarttime()!=null)
						bean.setStartTime(productionScheduler.getStarttime().toString());
						if(productionScheduler.getEndtime()!=null)
						bean.setEndTime(productionScheduler.getEndtime().toString());
						
						if(productionScheduler.getAssemblydate()!=null)
						{
							bean.setAssemblyDate(DateUtils.formatDate(productionScheduler.getAssemblydate(),Constants.GenericDateFormat.DATE_FORMAT));
							//bean.setAssemblyDate(productionScheduler.getAssemblydate().toString()+"T18:30");
						}
						
						if(productionScheduler.getTestedby()!=null)
							bean.setTestedBy(productionScheduler.getTestedby());
						
						bean.setModifyFlag(true);
						
						if(productionScheduler.getCompletedquantity()!=null)
							bean.setCompletedQuantity(productionScheduler.getCompletedquantity());
						
					}
					else
					{
						if(productionScheduler.getAssemblydate()!=null)
							bean.setAssemblyDate(DateUtils.formatDate(productionScheduler.getAssemblydate(),Constants.GenericDateFormat.DATE_FORMAT));
					}
					bean.setProdStatus(productionScheduler.getProdstatus());
					
					bean.setChangedItem1(productionScheduler.getChangeditem1());
					bean.setChangedItem2(productionScheduler.getChangeditem2());
					bean.setChangedItem3(productionScheduler.getChangeditem3());
					bean.setChangedItem4(productionScheduler.getChangeditem4());
					
					
					//if(productionScheduler.getAssemblydate()!=null)
					//	bean.setAssemblyDate(DateUtils.formatDate(productionScheduler.getAssemblydate(),Constants.GenericDateFormat.DATE_FORMAT));
					  
					
					beans.add(bean);						
				}
			}						
			return beans;
	}
	
	@RequestMapping(value = "/loadAllScoreEvents", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	 public @ResponseBody String loadAllScoreEvents(@RequestBody String jsonData) throws Exception 
	 {
			org.json.JSONObject response = new org.json.JSONObject();
			List productionSchedulerBeansLst=new ArrayList();
			List productionSchedulerBeans =productionService.listEvents();
			
			for(int i=0;i<productionSchedulerBeans.size();i++)
			{
				ProductionSchedulerEvents pe=(ProductionSchedulerEvents)productionSchedulerBeans.get(i);
				if(pe.getScore()==0)
					continue;
				else
					productionSchedulerBeansLst.add(pe);
						
			}
			org.json.JSONArray psArray = new org.json.JSONArray(productionSchedulerBeansLst);
	
			response.put("scoreEvents",psArray);
			
			return response.toString();
	 }
	
		//----------------------------------------------- Update Production Scheduler Data ------------------------------------------------------//
		
		@RequestMapping(value = "/updateProductionScheduler", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateProductionScheduler(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			ProductionScheduler productionScheduler = null;
			try
			{
				JSONObject psgridData=(JSONObject) jsonData.get("gridData");
				ProductionSchedulerBean productionSchedulerBean = new ObjectMapper().readValue(psgridData.toString(), ProductionSchedulerBean.class);
				productionScheduler = prepareModelForProductionScheduler(productionSchedulerBean);

				entityList.add(productionScheduler);
				
				String newAssyDate=productionSchedulerBean.getAssemblyDate();
				//String newDueDate=productionSchedulerBean.getDueDate();
				if(productionSchedulerBean.getAssemblyDate()!=null)
				{
					/*if(productionSchedulerBean.getAssemblyDate().indexOf("T")!=-1)
					{
						String[] assyDate=productionSchedulerBean.getAssemblyDate().split("T");
						String[] assyDate1=assyDate[0].split("-");
						newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
					}*/
					
					
					
					ProductionSchedulerEvents pse=(ProductionSchedulerEvents)commonService.getEntityByDate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT), ProductionSchedulerEvents.class);
					if(pse!=null)
					{
						Integer score=productionScheduler.getScore();
						if("To be cancelled".equalsIgnoreCase(productionSchedulerBean.getProdStatus()))
						{
							pse.setScore(productionScheduler.getScore()-pse.getScore());
							//pse.setQuantity(productionScheduler.getQuantity()+pse.getQuantity());
						}
						else
						{
							pse.setScore(productionScheduler.getScore()+pse.getScore());
						}
						
						pse.setQuantity(productionScheduler.getQuantity()+pse.getQuantity());
						entityList.add(pse);
					}
					else
					{
						pse=new ProductionSchedulerEvents();
						pse.setEventdate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT));
						pse.setScore(productionScheduler.getScore());
						pse.setQuantity(productionScheduler.getQuantity());
						entityList.add(pse);
					}
				}
				


				isInsertSuccess = productionService.saveMultipleEntities(entityList);
				if("To be cancelled".equalsIgnoreCase(productionSchedulerBean.getProdStatus()))
				{
					String strQry = "UPDATE ProductionScheduler set prodstatus='Cancelled' WHERE salesorder ='"+productionSchedulerBean.getSalesOrder()+"'";
					Integer updatedRows=commonService.executeUpdate(strQry);
				}
				
				return isInsertSuccess;
			}
			 catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return isInsertSuccess;
	        }			
		}
		
		
		private ProductionScheduler prepareModelForProductionScheduler(ProductionSchedulerBean productionSchedulerBean)
		{
			ProductionScheduler productionScheduler=(ProductionScheduler)commonService.getEntityById(productionSchedulerBean.getId(), ProductionScheduler.class);
			String newAssyDate=productionSchedulerBean.getAssemblyDate();
			String newDueDate=productionSchedulerBean.getDueDate();
			
			if(productionSchedulerBean.getAssemblyDate()!=null)
			{
			/*	if(productionSchedulerBean.getAssemblyDate().indexOf("T")!=-1)
				{
					String[] assyDate=productionSchedulerBean.getAssemblyDate().split("T");
					String[] assyDate1=assyDate[0].split("-");
					newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
				}*/
				productionScheduler.setAssemblydate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT));
			}
			if(productionSchedulerBean.getDueDate()!=null)
			{
			/*	if(productionSchedulerBean.getDueDate().indexOf("T")!=-1)
				{
					String[] dueDate=productionSchedulerBean.getDueDate().split("T");
					String[] dueDate1=dueDate[0].split("-");
					newDueDate=(Integer.parseInt(dueDate1[2])+1)+"-"+dueDate1[1]+"-"+dueDate1[0];
				}*/
				productionScheduler.setDuedate(DateUtils.getSqlDateFromString(newDueDate,Constants.GenericDateFormat.DATE_FORMAT));
			}
		


			
			if(productionSchedulerBean.getProdStatus().equalsIgnoreCase("New") || productionSchedulerBean.getProdStatus().equalsIgnoreCase("Modified"))
				productionScheduler.setProdstatus("Scheduled");
			else
				productionScheduler.setProdstatus("Cancelled");
			
			return productionScheduler;
		}
		
		
		@RequestMapping(value = "/modifyProductionScheduler", method = RequestMethod.POST)
		public @ResponseBody
		boolean modifyProductionScheduler(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List finalEntityList = new ArrayList();
			ProductionScheduler productionScheduler = null;
			
			try
			{
				JSONArray psgridData=(JSONArray) jsonData.get("gridData");
				for(int i=0;i<psgridData.size();i++)
				{				
					ProductionSchedulerBean productionSchedulerBean = new ObjectMapper().readValue(psgridData.get(i).toString(), ProductionSchedulerBean.class);
					entityList = prepareModelForProductionSchedulerOnModify(productionSchedulerBean);
					//entityList.add(productionScheduler);
					finalEntityList.addAll(entityList);
				}

				isInsertSuccess = productionService.saveMultipleEntities(finalEntityList);
				
				return isInsertSuccess;
			}
			 catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return isInsertSuccess;
	        }			
		}
		
		
		private List prepareModelForProductionSchedulerOnModify(ProductionSchedulerBean productionSchedulerBean)
		{
			List entityList = new ArrayList();
			
			ProductionScheduler productionScheduler=null;
			
			if(productionSchedulerBean.getModifyFlag()==true)
			{
				
				productionScheduler=(ProductionScheduler)commonService.getEntityById(productionSchedulerBean.getId(), ProductionScheduler.class);
				
				String newAssyDate=productionSchedulerBean.getAssemblyDate();
				String newCompleteDate=productionSchedulerBean.getCompletedDate();
				
		/*		if(productionSchedulerBean.getAssemblyDate().indexOf("T")!=-1)
				{
					String[] assyDate=productionSchedulerBean.getAssemblyDate().split("T");
					String[] assyDate1=assyDate[0].split("-");
					newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
				}*/
				if(productionSchedulerBean.getCompletedDate()!=null)
				{
					/*if(productionSchedulerBean.getCompletedDate().indexOf("T")!=-1)
					{
						String[] cDate=productionSchedulerBean.getCompletedDate().split("T");
						String[] cDate1=cDate[0].split("-");
						newCompleteDate=(Integer.parseInt(cDate1[2])+1)+"-"+cDate1[1]+"-"+cDate1[0];
					}*/
				
				}
				if(productionSchedulerBean.getCompletedDate()!=null)
				productionScheduler.setCompleteddate(DateUtils.getSqlDateFromString(newCompleteDate,Constants.GenericDateFormat.DATE_FORMAT));
				if(productionSchedulerBean.getStartTime()!=null)
				productionScheduler.setStarttime(java.sql.Time.valueOf(productionSchedulerBean.getStartTime()));
				if(productionSchedulerBean.getEndTime()!=null)
				productionScheduler.setEndtime(java.sql.Time.valueOf(productionSchedulerBean.getEndTime()));
				productionScheduler.setId(productionSchedulerBean.getId());
				productionScheduler.setTestedby(productionSchedulerBean.getTestedBy());
				productionScheduler.setCompletedquantity(productionSchedulerBean.getCompletedQuantity());
				if(!(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT).toString().equals(productionScheduler.getAssemblydate().toString())))
				{
					ProductionSchedulerEvents pse=(ProductionSchedulerEvents)commonService.getEntityByDate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT), ProductionSchedulerEvents.class);
					if(pse!=null)
					{
						//Integer score=pse.getScore();
						pse.setScore(pse.getScore()+productionScheduler.getScore());
						pse.setQuantity(pse.getQuantity()+productionScheduler.getQuantity());
						entityList.add(pse);
					}
					else
					{
						ProductionSchedulerEvents pse1=new ProductionSchedulerEvents();
						pse1.setEventdate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT));
						pse1.setScore(productionScheduler.getScore());
						pse1.setQuantity(productionScheduler.getQuantity());
						entityList.add(pse1);
					}
					
					ProductionSchedulerEvents pse1=(ProductionSchedulerEvents)commonService.getEntityByDate(DateUtils.getSqlDateFromUtilDate(productionScheduler.getAssemblydate()), ProductionSchedulerEvents.class);
					
					if(pse1!=null)
					{
						//Integer score=pse1.getScore();
						pse1.setScore(pse1.getScore()-productionScheduler.getScore());
						pse1.setQuantity(pse1.getQuantity()-productionScheduler.getQuantity());
						entityList.add(pse1);
					}
					
				
				}
				productionScheduler.setAssemblydate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT));
				entityList.add(productionScheduler);
			}
			else
			{
				
				productionScheduler=(ProductionScheduler)commonService.getEntityById(productionSchedulerBean.getId(), ProductionScheduler.class);
				
				ProductionScheduler productionSchedulerSave=new ProductionScheduler();
				
				BeanUtils.copyProperties(productionScheduler, productionSchedulerSave);
				
				
				String newAssyDate=productionSchedulerBean.getAssemblyDate();
				String newCompleteDate=productionSchedulerBean.getCompletedDate();
				
			/*	if(productionSchedulerBean.getAssemblyDate().indexOf("T")!=-1)
				{
					String[] assyDate=productionSchedulerBean.getAssemblyDate().split("T");
					String[] assyDate1=assyDate[0].split("-");
					newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
				}*/
				if(productionSchedulerBean.getCompletedDate()!=null)
				{
				/*	if(productionSchedulerBean.getCompletedDate().indexOf("T")!=-1)
					{
						String[] cDate=productionSchedulerBean.getCompletedDate().split("T");
						String[] cDate1=cDate[0].split("-");
						newCompleteDate=(Integer.parseInt(cDate1[2])+1)+"-"+cDate1[1]+"-"+cDate1[0];
					}*/
				}
				if(productionSchedulerBean.getCompletedDate()!=null)
					productionSchedulerSave.setCompleteddate(DateUtils.getSqlDateFromString(newCompleteDate,Constants.GenericDateFormat.DATE_FORMAT));
				if(productionSchedulerBean.getStartTime()!=null)
					productionSchedulerSave.setStarttime(java.sql.Time.valueOf(productionSchedulerBean.getStartTime()));
				if(productionSchedulerBean.getEndTime()!=null)
					productionSchedulerSave.setEndtime(java.sql.Time.valueOf(productionSchedulerBean.getEndTime()));
			
				productionSchedulerSave.setId(productionSchedulerBean.getId());
				productionSchedulerSave.setTestedby(productionSchedulerBean.getTestedBy());
				productionSchedulerSave.setCompletedquantity(productionSchedulerBean.getCompletedQuantity());
								
				if(!(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT).toString().equals(productionScheduler.getAssemblydate().toString())))
				{
					ProductionSchedulerEvents pse=(ProductionSchedulerEvents)commonService.getEntityByDate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT), ProductionSchedulerEvents.class);
					if(pse!=null)
					{
						//Integer score=pse.getScore();
						pse.setScore(pse.getScore()+productionScheduler.getScore());
						pse.setQuantity(pse.getQuantity()+productionScheduler.getQuantity());
						entityList.add(pse);
					}
					else
					{
						ProductionSchedulerEvents pse1=new ProductionSchedulerEvents();
						pse1.setEventdate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT));
						pse1.setScore(productionScheduler.getScore());
						pse1.setQuantity(productionScheduler.getQuantity());
						entityList.add(pse1);
					}
					
					ProductionSchedulerEvents pse1=(ProductionSchedulerEvents)commonService.getEntityByDate(DateUtils.getSqlDateFromUtilDate(productionScheduler.getAssemblydate()), ProductionSchedulerEvents.class);
					
					if(pse1!=null)
					{
						//Integer score=pse1.getScore();
						pse1.setScore(pse1.getScore()-productionScheduler.getScore());
						pse1.setQuantity(pse1.getQuantity()-productionScheduler.getQuantity());
						entityList.add(pse1);
					}					
				
				}
				productionSchedulerSave.setAssemblydate(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT));
				
				productionSchedulerSave.setId(null);
				
				entityList.add(productionSchedulerSave);
				
			}
			//logger.info("Assembly Date 1--"+newCompleteDate);
			
			//logger.info("productionScheduler.getAssemblydate().toString()=== "+DateUtils.formatDate(productionScheduler.getAssemblydate(),Constants.GenericDateFormat.DATE_FORMAT));
			
			return entityList;
		}
		
		
		@RequestMapping(value = "/savePackingDetails", method = RequestMethod.POST)
		public @ResponseBody
		boolean savePackageDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			PackageDetails packageDetails=null;
			
			try
			{
				JSONArray packagegridData=(JSONArray) jsonData.get("gridData");
				JSONObject formData=(JSONObject) jsonData.get("formData");
				String modelno=(String)formData.getString("modelno");
				String salesOrder=(String)  formData.getString("salesOrder");
				String remarks=(String)  formData.getString("remarks");
				
				String[] modelNoArray=modelno.split(",");
				String[] salesOrderArray=salesOrder.split(",");

				for(int i=0;i<modelNoArray.length;i++)
				{
					for(int j=0;j<packagegridData.size();j++)
					{					
						PackageDetailsBean pdbean=new ObjectMapper().readValue(packagegridData.get(j).toString(), PackageDetailsBean.class);
						
						packageDetails=prepareModelForPackageDetails(pdbean,modelNoArray[i],salesOrderArray[i],remarks);
						
						entityList.add(packageDetails);
					}
				}
				
				
				productionService.deletePackageDtls(packageDetails);

				isInsertSuccess = productionService.saveMultipleEntities(entityList);
				
				return isInsertSuccess;  
			}
			 catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return isInsertSuccess;
	        }			
		}
		
		private PackageDetails prepareModelForPackageDetails(PackageDetailsBean packageDetailsBean,String mNum,String sOrder,String remarks)
		{			
			//java.sql.Timestamp stimestamp = java.sql.Timestamp.valueOf(packageDetailsBean.getPackingDateTime());
			
			String newStartDate=packageDetailsBean.getStartDate();

			/*if(packageDetailsBean.getStartDate().indexOf("T")!=-1)
			{
				String[] cDate=packageDetailsBean.getStartDate().split("T");
				String[] cDate1=cDate[0].split("-");
				newStartDate=(Integer.parseInt(cDate1[2])+1)+"-"+cDate1[1]+"-"+cDate1[0];
			}*/
			String newEndDate=packageDetailsBean.getEndDate();

		/*	if(packageDetailsBean.getEndDate().indexOf("T")!=-1)
			{
				String[] cDate=packageDetailsBean.getEndDate().split("T");
				String[] cDate1=cDate[0].split("-");
				newEndDate=(Integer.parseInt(cDate1[2])+1)+"-"+cDate1[1]+"-"+cDate1[0];
			}*/
			
			PackageDetails packageDetails=new PackageDetails();	
			
			packageDetails.setDimension(packageDetailsBean.getDimension());
			packageDetails.setGrossweight(packageDetailsBean.getGrossWeight());
			packageDetails.setNetweight(packageDetailsBean.getNetweight());
			packageDetails.setPackingtype(packageDetailsBean.getPackingType());
			packageDetails.setRemarks(packageDetailsBean.getRemarks());			
			packageDetails.setStartdate(DateUtils.getSqlDateFromString(newStartDate,Constants.GenericDateFormat.DATE_FORMAT));
			packageDetails.setStarttime(java.sql.Time.valueOf(packageDetailsBean.getStartTime()));
			packageDetails.setEnddate(DateUtils.getSqlDateFromString(newEndDate,Constants.GenericDateFormat.DATE_FORMAT));
			packageDetails.setEndtime(java.sql.Time.valueOf(packageDetailsBean.getEndTime()));
			packageDetails.setId(packageDetailsBean.getId());
			
			
			packageDetails.setSalesorder(sOrder);
			packageDetails.setModelno(mNum);
			packageDetails.setRemarks(remarks);

			return packageDetails;
		}
		
		
		@RequestMapping(value = "/loadPackingDetails", method = RequestMethod.POST)
		public @ResponseBody
		String loadPackingDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
		{
			List<String> salesOrderList = new ArrayList<String>();
			List<String> modelList = new ArrayList<String>();
			org.json.JSONObject response = new org.json.JSONObject();
			
			try
			{
				JSONObject formData=(JSONObject) jsonData.get("formData");
				String modelno=(String)formData.getString("modelno");
				String salesOrder=(String)  formData.getString("salesOrder");
				
				String[] modelNoArray=modelno.split(",");
				String[] salesOrderArray=salesOrder.split(",");

				for(int i=0;i<modelNoArray.length;i++)
				{
					salesOrderList.add(salesOrderArray[i]);
					modelList.add(modelNoArray[i]);
				}
				
				List<PackageDetailsBean> packageDetailsBeans =prepareBeanForPackingDetails(productionService.loadPackageDtls(salesOrderList, modelList));
				
				org.json.JSONArray psArray = new org.json.JSONArray(packageDetailsBeans);
		
				response.put("packingDetails",psArray);
				
				return response.toString();			
			}
			 catch (Exception e)
	        {
				 response.put("packingDetails","No Data Found");
	            logger.info(e.getCause(), e);
	            return response.toString();
	        }			
		}
		
		private List<PackageDetailsBean> prepareBeanForPackingDetails(List<PackageDetails> packageDetails) 
		{
				List<PackageDetailsBean> beans = null;
				
				if (packageDetails != null && !packageDetails.isEmpty()) 
				{  
					beans = new ArrayList<PackageDetailsBean>();
					PackageDetailsBean bean = null;
					for (PackageDetails packageDetail : packageDetails) 
					{					
						bean = new PackageDetailsBean();
						
						bean.setDimension(packageDetail.getDimension());
						bean.setGrossWeight(packageDetail.getGrossweight());
						bean.setNetweight(packageDetail.getNetweight());
						bean.setPackingType(packageDetail.getPackingtype());
						bean.setRemarks(packageDetail.getRemarks());	
						bean.setStartDate(DateUtils.formatDate(packageDetail.getStartdate(),Constants.GenericDateFormat.DATE_FORMAT));
						//bean.setStartDate(packageDetail.getStartdate()+"T18:30");
						//bean.setPackingDate(packageDetail.getPackingdate()+"T18:30");
						bean.setStartTime(packageDetail.getStarttime().toString());
						bean.setEndDate(DateUtils.formatDate(packageDetail.getEnddate(),Constants.GenericDateFormat.DATE_FORMAT));
						//bean.setEndDate(packageDetail.getEnddate()+"T18:30");
						bean.setEndTime(packageDetail.getEndtime().toString());
						bean.setModelno(packageDetail.getModelno());
						bean.setSalesOrder(packageDetail.getSalesorder());
						bean.setId(packageDetail.getId());
						beans.add(bean);						
					}
				}						
				return beans;
		}

	
}
