package com.finsol.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.finsol.bean.UserBean;
import com.finsol.model.Users;
import com.finsol.model.YtdProfit;
import com.finsol.model.YtdProfitDetailsByProduct;
import com.finsol.service.Bss_Migrate_ServiceImpl;
import com.finsol.service.UserSeviceImpl;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;
 
/*import com.finsol.service.CommonService;*/

/**
 * @author Rama Krishna
 *
 */
@Controller
public class LoginController 
{
	private static final Logger logger = Logger.getLogger(LoginController.class);
	
	/*@Autowired
	private CommonService bssService;*/
	@Autowired
	private UserSeviceImpl userService;	
	 
	@Autowired
	private Bss_Migrate_ServiceImpl migratesevice;
	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 public @ResponseBody String sca360Validate(@RequestBody String jsonData,HttpServletRequest request) throws Exception 
	 {
		Users user=null;
		//ServerSocketOn sso=new ServerSocketOn();
		HttpSession session=request.getSession();
		UserBean userBean = new ObjectMapper().readValue(jsonData, UserBean.class);
		
		String uName=userBean.getUserName();
		String uPassword=userBean.getPassword();
		//String uCountryCode=userBean.getCountryCode();
		
		user=userService.getUser(userBean.getUserName());
		
		if(user!=null)
		{		
			if(user.getLoginid().equalsIgnoreCase(uName) && user.getPassword().equals(uPassword))
			{				
				String countryCode=user.getCountrycode();
				session.setAttribute("countrycode",countryCode);
				session.setAttribute("user",user.getLoginid());
				session.setAttribute("userfname",user.getUserfirstname());;
			/*	session.setMaxInactiveInterval(600);*/
				 return "login.htm?"+countryCode+"?"+user.getUserfirstname();
								
			}
			else
			{
				return "/SCA-360";	
				//return "redirect:/welcome";
			}
		}
		else
		{
			return "/SCA-360";
		}
	}
	
	
	/*@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) 
	{
		HttpSession session=request.getSession(false);
		
		if(session!=null)
		{
			session.invalidate();
		}
		//return new ModelAndView("index.html");
		return new ModelAndView("redirect:/index.html");
	}*/
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public  String  welcome1() 
	{
		

		return "index";
	}
	
	/*@RequestMapping(value = "/testurl", method = RequestMethod.GET)
	public  @ResponseBody Integer  welcome5() 
	{
		Integer count=bssService.getTest();
		HashMap a =new HashMap();
		return count;
	}
	*/
	
	@RequestMapping(value = "/loginggg", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		String uName = request.getParameter("name");
		String uPass = request.getParameter("password");
		return new ModelAndView("redirect:/angular/login.jsp");
	}
	
	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String  logout(HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession hs=request.getSession();
		hs.removeAttribute("user");
		 return "index";
	}
	// Reference Code ---------------	
	/*	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String welcome() {
		System.out.println("==============Inside Welcome=====");
		logger.info("==============Inside welcome()=====");
		return "redirect:welcome.do";
	}*/
	
	//if(uName.equalsIgnoreCase(user.getName()) && uPassword.equalsIgnoreCase(user.getPassword()))
/*	if("makella".equalsIgnoreCase(uName) && "finsol".equalsIgnoreCase(uPassword))
	{
		return "angular/login.jsp";
	}
	else
	{
			return "/SugarERP";
		
	}*/
	
	
	
	@RequestMapping(value = "/scalogin", method = RequestMethod.GET)
	public @ResponseBody JSONObject  homepage(HttpServletRequest request) 
	{
		JSONObject j1=new JSONObject();
		j1.put("data",0);
		return j1;
		
		 
	}
	
	
	
	@RequestMapping(value = "/timeout", method = RequestMethod.GET)
	public  String  welcome2() 
	{

		return "index";
	}
	
	
	@RequestMapping(value = "/checksession", method = RequestMethod.GET)
	public @ResponseBody JSONObject  homepage1(HttpServletRequest request) 
	{
		JSONObject j1=new JSONObject();
		j1.put("data",1);
		return j1;
		
		 
	}
	
	
	@RequestMapping(value = "/YtdProfitMigration", method = RequestMethod.GET)
	public  @ResponseBody boolean  YtdProfitMigration(){
		System.out.println("hello bss <-------------------------------------------->");
		logger.info("<==============helloio0oooooooooooooooo welcome()==========================================================>");
		boolean success=false;
		String invdate=null;
		String ivdate[]=null;
		String year=null;
		Integer month=null;
		String day=null;
		String monthString=null;
		String product=null;
		String productcode=null;
		String pccode=null;
		//InvoiceDate,Customer,ProductGroup,InvoiceQty,SaleValue,TotalCost,Profit,ProfitPercent,id
				YtdProfit ytdprofit=null;
		ArrayList al=new ArrayList();
	List<Object[]> tempdata=migratesevice.getAllTheYtdProfitDetailsFromTempTable();
	
	for(Object[] data:tempdata)
		
	{
		ytdprofit=new YtdProfit();
		
		System.out.println("Record number in ytdprofit----------------------------------"+data[8]);
	
		invdate=(String)data[0];
		
		ivdate=invdate.split("/");
		year=ivdate[2];
		month=Integer.parseInt(ivdate[0]);
		day=ivdate[1];
		ytdprofit.setYear(Integer.parseInt(year));
		ytdprofit.setMonthcode(month);
		ytdprofit.setDate(Integer.parseInt(day));
		ytdprofit.setSalesinvoicedate(DateUtils.getSqlDateFromString(day+"-"+month+"-"+year,Constants.GenericDateFormat.DATE_FORMAT));
		
        switch (month)
        {
        case 1:  monthString = "January";       break;
        case 2:  monthString = "February";      break;
        case 3:  monthString = "March";         break;
        case 4:  monthString = "April";         break;
        case 5:  monthString = "May";           break;
        case 6:  monthString = "June";          break;
        case 7:  monthString = "July";          break;
        case 8:  monthString = "August";        break;
        case 9:  monthString = "September";     break;
        case 10: monthString = "October";       break;
        case 11: monthString = "November";      break;
        case 12: monthString = "December";      break;
        }
        ytdprofit.setMonth(monthString);
        product=(String)data[2];
        ytdprofit.setProduct(product);
        
    	List<Object[]> products=migratesevice.getYtdProfitProductDetails(product);
		
		
		if(products.size()>0)
		{
		for(Object productcodes[]:products)
		{
		
		productcode=(String)productcodes[0];
		if(productcode!=null)
		{
		pccode=(String)productcodes[1];
	
		List<String> productcategory=migratesevice.getProductCategoryCode(pccode);
		ytdprofit.setProduct(product);
		ytdprofit.setProductcode(productcode);
		ytdprofit.setProductcategory(productcategory.get(0));
		ytdprofit.setPccode(pccode);
		String country=migratesevice.getCountryDetails((String)data[1]);
		String countrycode=migratesevice.getCountryCode(country);
		
		ytdprofit.setCountry(country);
		ytdprofit.setCountrycode(countrycode);
		}
		}
		}
		ytdprofit.setTotalcost((Double)data[5]);
		ytdprofit.setProfit((Double)data[6]);
		ytdprofit.setProfitpercent((Double)data[7]);
		ytdprofit.setQty((Double)data[3]);
		ytdprofit.setInvoicedamount((Double)data[4]);
		
		al.add(ytdprofit);	
		
		}
		success=migratesevice.saveMigrateRegionData(al);
		return success;
		}
	
	
	
	@RequestMapping(value = "/migrateYtdProfitByProduct", method = RequestMethod.GET)
	public  @ResponseBody boolean  migrateYtdProfitByProduct() {
		System.out.println("hello bss <-------------------------------------------->");
		
		
	List<Integer> bookingYears=migratesevice.getYtdProfitYear();
	
	ArrayList al=new ArrayList();
	YtdProfitDetailsByProduct ytdproduct=null; 
	Boolean success=false;
	Double tcost=null;
	Double invcost=null;
	Double profit=null;
	Double profitpercent=null;
	for(Integer year:bookingYears)
	{
	
	List<Integer> bookingMonths=migratesevice.getYtdProfitMonth(year);
	if(bookingMonths.size()<1 || bookingMonths==null)
		continue;
	for(Integer month:bookingMonths)
	{
		
	List<String> bookingCountrys=migratesevice.getYtdProfitCountry(year,month);
	if(bookingCountrys.size()<1 || bookingCountrys==null)
		continue;
	for(String country:bookingCountrys)
	{
	List<String> bookingproductcategory=migratesevice.getYtdProfitProductCategory(year,month,country);
	if(bookingproductcategory.size()<1 || bookingproductcategory==null)
		continue;
	for(String productcategory:bookingproductcategory)
	{
	
	List<Object[]> bookingproduct=migratesevice.getYtdProfitProduct(year,month,country,productcategory);
	
	if(bookingproduct.size()>0 || bookingproduct!=null)
	{
		
	for(Object product[]:bookingproduct)
	{
	List<Object[]> profitAmount=migratesevice.getYtdProfitAmountForProduct(year,month,country,productcategory,(String)product[0]);
	if(profitAmount.size()<1 || profitAmount==null)
		continue;
	logger.info("<==============helloio0oooooooooooooooo welcome()==========================================================>"+(String)product[1]);
	Object[] pamount=profitAmount.get(0);
	
	ytdproduct=new YtdProfitDetailsByProduct();
	
	ytdproduct.setQty((Double)pamount[0]);
	tcost=(Double)pamount[1];
	invcost=(Double)pamount[2];
	ytdproduct.setTotalcost(tcost);
	ytdproduct.setInvoicedamount(invcost);
	profit=invcost-tcost;
	ytdproduct.setProfit(profit);
	if(invcost!=0)
	profitpercent=(profit*100)/invcost;
	else
	profitpercent=0.0;	
	ytdproduct.setProfitpercent(profitpercent);
	ytdproduct.setProduct((String)product[0]);
	ytdproduct.setPccode((String)product[2]);
	ytdproduct.setProductcode((String)product[3]);
	ytdproduct.setMonth((String)product[1]);
	ytdproduct.setProductcategory(productcategory);
	ytdproduct.setCountry(country);
	ytdproduct.setMonthcode(month);
	
	ytdproduct.setYear(year);
	al.add(ytdproduct);
		}
		
		}
				
		}
			
		}
		}
			
		}
	
	success=migratesevice.saveMigrateRegionData(al);
	
	return success;
		}

	
}
