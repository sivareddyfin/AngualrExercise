

package com.finsol.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.finsol.bean.QAIssueBean;
import com.finsol.bean.QAIssueDetailsBean;
import com.finsol.bean.ServiceInquiryBean;
import com.finsol.bean.ServiceInquiryDtlsBean;
import com.finsol.bean.TestReportBean;
import com.finsol.bean.TestReportDtlsBean1;
import com.finsol.bean.TestReportDtlsBean2;
import com.finsol.bean.TestReportDtlsBean3;
import com.finsol.bean.TestReportDtlsBean4;
import com.finsol.bean.TestReportDtlsBean5;
import com.finsol.dao.HibernateDao;
import com.finsol.model.CustmerDetails;
import com.finsol.model.HyphonicGreeseAmount;
import com.finsol.model.MotorHorsePower;
import com.finsol.model.MotorSpecifications;
import com.finsol.model.PrestNeoGreeseAmount;
import com.finsol.model.QAIssue;
import com.finsol.model.QAIssueDetails;
import com.finsol.model.SequenceNumber;
import com.finsol.model.ServiceInquiry;
import com.finsol.model.ServiceInquiryDtls;
import com.finsol.model.TestReport;
import com.finsol.model.TestReportDtls;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.ProductSelectionService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

/* @authour 
 * 
 * 
 * siva reddy*/
@Controller
public class ProductSelectionController 
{
	private static final Logger logger = Logger.getLogger(ProductSelectionController.class);

	@Autowired
	private ProductSelectionService productSelectionService;
	
	@Autowired
	private BSS_CommonService commonService;
	
	@Autowired
	private HibernateDao hibernateDao;
	
	/*@Autowired
	private  ServletContext servletContext;*/
	
	@RequestMapping(value = "/getVoltages", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllWeignBridge(HttpServletRequest req, HttpServletResponse res) {
		try
		{

		Integer frequencey = Integer.parseInt(req.getParameter("frequencey"));
		List<String> voltages = productSelectionService.getVoltagesOnfrequency(frequencey);

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (String voltage : voltages) {
			JSONObject mainobject1 = new JSONObject();
			mainobject1.put("volatge", voltage);
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", budgetandbookingarrayforallcountries);

		return mainobject;
	
		
	}catch(Exception e)
	{
		logger.error(e.getMessage());
		return null;
		
	}
	
}
	@RequestMapping(value = "/getCouplingTypes", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllCouplingTypes(HttpServletRequest req, HttpServletResponse res) {
		try
		{
 
		List<String> couplingTypes = productSelectionService.getAllCouplingTypes();
       JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (String couplingType : couplingTypes) {
			JSONObject mainobject1 = new JSONObject();
			mainobject1.put("ctype", couplingType);
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", budgetandbookingarrayforallcountries);

		return mainobject;
	}catch(Exception e)
		{
		logger.error(e.getMessage());
		return null;
		}
	}
	
	@RequestMapping(value = "/getLocationfactor", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllLocationfactor(HttpServletRequest req, HttpServletResponse res) {
		try{
 
		List<Object[]> couplingTypes = productSelectionService.getLocationFactor();

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (Object[] couplingType : couplingTypes) {
			JSONObject mainobject1 = new JSONObject();
			mainobject1.put("ctype", (String)couplingType[0]);
			mainobject1.put("cvalue", (Double)couplingType[1]);
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", budgetandbookingarrayforallcountries);

		return mainobject;
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return null;
		}
	}
	
	 
	
	@RequestMapping(value = "/getCouplingFactor", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllCouplingFactor(HttpServletRequest req, HttpServletResponse res) {
 
		try
		{
		List<Object[]> couplingTypes = productSelectionService.getCouplingFactor();

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (Object[] couplingType : couplingTypes) {
			JSONObject mainobject1 = new JSONObject();
			mainobject1.put("ctype",(String)couplingType[0]);
			mainobject1.put("cvalue",(Double)couplingType[1]);
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", budgetandbookingarrayforallcountries);

		return mainobject;
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value = "/getShockFactor", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllShockFactor(HttpServletRequest req, HttpServletResponse res) {
 try{
	 
 
		List<Object[]> couplingTypes = productSelectionService.getShockFactor();

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (Object[] couplingType : couplingTypes) {
			JSONObject mainobject1 = new JSONObject();
			mainobject1.put("ctype", (String)couplingType[0]);
			mainobject1.put("cvalue", (Double)couplingType[1]);
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", budgetandbookingarrayforallcountries);

		return mainobject;
 }catch(Exception e)
 {
	 logger.error(e.getMessage());
		return null;
	 
 }
	}

	
	@RequestMapping(value = "/getLoadTypes", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllLoadTypes(HttpServletRequest req, HttpServletResponse res) {
 
		try
		{
		List<String> couplingTypes = productSelectionService.getAllLoadTypes();

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (String couplingType : couplingTypes) {
			JSONObject mainobject1 = new JSONObject();
			mainobject1.put("ctype", couplingType);
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", budgetandbookingarrayforallcountries);

		return mainobject;
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return null;
		 
		}
	}
	
	
	@RequestMapping(value = "/getLoadFactor", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllLoadFactoer(HttpServletRequest req, HttpServletResponse res) {
		try
		{
  
		String loadtype = req.getParameter("loadtype");
		int workinghours = Integer.parseInt(req.getParameter("hours"));
		List<Object[]> couplingTypes = productSelectionService.getAllLoadfactor(loadtype, workinghours);

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();

		System.out.println(couplingTypes + "here the data ta coming");

		ArrayList al1 = null;
		ArrayList al2 = null;
		int i = 1;
		for (Object[] couplingType : couplingTypes) {

			if (i == 1) {
				al1 = new ArrayList();
				al1.add((Double) couplingType[0]);
				al1.add((Byte) couplingType[1]);
				i++;
			}

			else {
				al2 = new ArrayList();
				al2.add((Double) couplingType[0]);
				al2.add((Byte) couplingType[1]);
			}

		}

		Double loadfactor = 0.0;

		if (workinghours < (Byte) al1.get(1)) {
			loadfactor = (Double) al1.get(0);

		}

		else {
			loadfactor = (Double) al2.get(0);

		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", loadfactor);

		return mainobject;
		
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return null;
		 
		}
	}
	
	
	
	
	@RequestMapping(value = "/OutputSpeedR", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllOutputSpeedsRadialNodes(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
		  int frequency=Integer.parseInt(req.getParameter("frequency"));
 
		List<Object[]> couplingTypes = productSelectionService.getAllOutputSpeedsRadialNodes(frequency);

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (Object[] couplingType : couplingTypes) {
			JSONObject mainobject1 = new JSONObject();
			
			mainobject1.put("RadialLoadN",(Double)couplingType[0]);
			mainobject1.put("ReductionRatio",(Double)couplingType[1]);
			mainobject1.put("oPSpeed",(Double)couplingType[2]);
			mainobject1.put("oPTorqueKgFm",(Double)couplingType[3]);
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", budgetandbookingarrayforallcountries);

		return mainobject;
		
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return null;
		 
		}
	}
	
	
	
	
	@RequestMapping(value = "/getMomentofInertia", method = RequestMethod.GET)
	public @ResponseBody JSONObject getMomentOfInertia(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
		  String motortype=req.getParameter("motortype");
		  Double powerkw=Double.parseDouble(req.getParameter("wattage"));
 
		List<Object[]> couplingTypes = productSelectionService.gtmomentOfInertia(motortype,powerkw);

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (Object[] couplingType : couplingTypes) {
			JSONObject mainobject1 = new JSONObject();
			
			mainobject1.put("mi",(Double)couplingType[0]);
			mainobject1.put("gdsq",(Double)couplingType[1]);
			 
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		JSONObject mainobject = new JSONObject();
		mainobject.put("data", budgetandbookingarrayforallcountries);

		return mainobject;
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return null;
		 
		}
	}
	
	
	
	
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public @ResponseBody JSONObject getPower(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			
		
		  Double frequency=Double.parseDouble(req.getParameter("calpower"));
		  Double frequency1=0.0;
		  
		  if(frequency<=0.1)
			  
		  {
			  
			  frequency1=0.1;
		  }
		  
		  else
		  {
			  
			  List<Object[]> selectedpower=productSelectionService.getCalculatedPower();
			  
			  for(Object[] selectedpowers:selectedpower )
			  {
				   Double first=(Double)selectedpowers[0];
				   Double second=(Double)selectedpowers[1];
				   
				   if(first<frequency &&  second>frequency)
					   
				   {
					   frequency1=second;
					   
					   
				   }
				  
			  }
			  
		  }	  
		 
		  JSONObject mainobject = new JSONObject();  
		  
		  if(frequency1==0.0)
		  {
			  
			  mainobject.put("data",10.0);
			  
		  }
		  
		  else
		  {
			  mainobject.put("data",frequency1);

			  
		  }
		  
		 

		return mainobject;
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return null;
		 
		}
	}
	
	
	
	
	/*@RequestMapping(value = "/getReductionRatio", method = RequestMethod.GET)
	public @ResponseBody JSONObject getConstantReductionRatio(HttpServletRequest req, HttpServletResponse res)
	{ 
		try
	{
		
	
		 int frequency=Integer.parseInt(req.getParameter("frequency"));
		 Double rpm=Double.parseDouble(req.getParameter("rpm"));
		 
				 
				 
		 
		List<Integer> couplingTypes = productSelectionService.getReductionRation(frequency,rpm);

		JSONObject mainobject = new JSONObject();
		for (Integer couplingType : couplingTypes) {
			 
			mainobject.put("data",couplingType);
			 
			 
		}

		 
		 
		return mainobject;
	}catch(Exception e)
	{
		logger.error(e.getMessage());
		return null;
	 
		
	}
	}
	
*/	 
	
	@RequestMapping(value = "/getReductionRatio", method = RequestMethod.GET)
	public @ResponseBody JSONObject getConstantReductionRatio(HttpServletRequest req, HttpServletResponse res)
	{
		 int frequency=Integer.parseInt(req.getParameter("frequency"));
		 Double rpm=Double.parseDouble(req.getParameter("rpm"));
		 Double maxrpm=productSelectionService.getMaxRpm(frequency,rpm);
		 Double minrpm=productSelectionService.getMinRpm(frequency,rpm);
		 Double mean=(maxrpm+minrpm)/2;
		 
		 JSONObject mainobject = new JSONObject();
		 
		 if(rpm<=mean)
		 {
			 
				List<Integer> couplingTypes = productSelectionService.getReductionRation(frequency,minrpm);

				
				for (Integer couplingType : couplingTypes) 
				{
					 
					mainobject.put("data",couplingType);
					 
					 
				}

				 
				 
				
			 
			 
		 }

		 if(rpm>=mean)
			 
		 {
			 
			 
				List<Integer> couplingTypes = productSelectionService.getReductionRation(frequency,maxrpm);

				
				for (Integer couplingType : couplingTypes) 
				{
					 
					mainobject.put("data",couplingType);
					 
					 
				}

				 
				 
				
			 
		 }
		 
		 
		 return mainobject;
				 
		 
	 
	}
	
	
	
	
	

	
	
	@RequestMapping(value = "/getframesize", method = RequestMethod.GET)
	public @ResponseBody JSONObject getFrameSize(HttpServletRequest req, HttpServletResponse res)
 {
		try {
			
			

			List<Double> wattage = productSelectionService.getKws();
            String type=req.getParameter("wattage");
            String volatage=req.getParameter("volatage");
            
			int frequency = Integer.parseInt(req.getParameter("frequency"));
			Double rpm = Double.parseDouble(req.getParameter("outputRpm"));
			int reductionvalue = Integer.parseInt(req.getParameter("redRatio"));
			Double pr = Double.parseDouble(req.getParameter("shaftRad"));
			Double Te = Double.parseDouble(req.getParameter("eTorque"));
			String coupilingtype = req.getParameter("couplingType");
			String motortype = req.getParameter("motorRype");
			Double j1 = Double.parseDouble(req.getParameter("mmntRatio"));
			Double startupfrequency = Double.parseDouble(req.getParameter("strtFrq"));
			String mounttype = req.getParameter("mounttype");
			Integer allowablefrequency = Integer.parseInt(req.getParameter("allowablefrequency"));
			JSONObject mainobject = new JSONObject();

			Double framesize = 0.0;
			Double capacitysymbol = 0.0;
			Double reductionration = 0.0;
			Double shaftraidoloadn = 0.0;
			Double kws = 0.0;
			Double torque = 0.0;
			Double mi = 0.0;
			Double gd = 0.0;
			Double jlm = 0.0;
			Double ratio = 0.0;

			List<String> aboutmodelnumber = null;

			
			
			
			for (Double d : wattage) {

				List<Object[]> couplingTypes = productSelectionService.getFrameSize(frequency, reductionvalue, d);

				for (Object[] couplingType : couplingTypes) {

					framesize = (Double) couplingType[0];
					capacitysymbol = (Double) couplingType[1];
					reductionration = (Double) couplingType[2];
					shaftraidoloadn = (Double) couplingType[3];
					kws = (Double) couplingType[4];
					torque = (Double) couplingType[5];

				}

				if (torque >= Te && shaftraidoloadn >= pr) {
					List<Object[]> momentofInertia = productSelectionService.gtmomentOfInertia(motortype, d);

					for (Object[] couplingType : momentofInertia) {
						mi = (Double) couplingType[0];
						gd = (Double) couplingType[1];

					}

					Double reductionrationsqare = reductionration * reductionration;

					jlm = (j1 / reductionrationsqare);
					ratio = jlm / mi;

					if (allowablefrequency == 0) {

						if (coupilingtype.equals("Direct Shaft")) {
							allowablefrequency = productSelectionService.getAllowableFrequency(ratio, "direct");
							/* allowablefrequency = 10; */
							if (startupfrequency < allowablefrequency) {
								break;
							}

						}

						else {

							allowablefrequency = productSelectionService.getAllowableFrequency(ratio, "gear");

							if (startupfrequency < allowablefrequency) {
								break;
							}

						}

					}

				}

			}

			mainobject.put("frame",
					capacitysymbol.intValue() + "-" + framesize.intValue() + "-" + reductionration.intValue());
			mainobject.put("kws", kws);
			mainobject.put("radialnode", shaftraidoloadn);
			mainobject.put("jm", mi);
			mainobject.put("jlm", jlm);
			mainobject.put("ratio", ratio);
			mainobject.put("allowablefrequency", allowablefrequency);

			int capacity = capacitysymbol.intValue();
			int framesizevalue = framesize.intValue();
			int reduction = reductionration.intValue();
			int radialnode = shaftraidoloadn.intValue();

			if (mounttype != null && motortype != null) {
				if (mounttype.equalsIgnoreCase("Foot mount") && motortype.equalsIgnoreCase("w/o brake")) {
					aboutmodelnumber = productSelectionService.getModelNUmberOnFootMountWihoutBrake(frequency, kws,
							capacity, framesizevalue, reduction, radialnode, rpm);
				}

				if (mounttype.equalsIgnoreCase("Foot mount") && motortype.equalsIgnoreCase("w/ brake")) {
					aboutmodelnumber = productSelectionService.getModelNUmberOnFootMountBrake(frequency, kws, capacity,
							framesizevalue, reduction, shaftraidoloadn, rpm);
				}

				if (mounttype.equalsIgnoreCase("Flange mount") && motortype.equalsIgnoreCase("w/o brake")) {
					aboutmodelnumber = productSelectionService.getModelNUmberOnFlangeMountWihoutBrake(frequency, kws,
							capacity, framesizevalue, reduction, shaftraidoloadn, rpm);
				}

				if (mounttype.equalsIgnoreCase("Flange mount") && motortype.equalsIgnoreCase("w/ brake")) {
					aboutmodelnumber = productSelectionService.getModelNUmberOnFlangeMountWithBrake(frequency, kws,
							capacity, framesizevalue, reduction, shaftraidoloadn, rpm);
				}

			}

			if (aboutmodelnumber != null) {
				for (String modelnumber : aboutmodelnumber) {
					mainobject.put("modelnumber", modelnumber);
				}

			}

			
			
			
			
			
	return mainobject;


		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;

		}

	}
	
	
	
	@RequestMapping(value = "/getallowablefrequencies", method = RequestMethod.GET)
	public @ResponseBody JSONObject getallowablefrequencies(HttpServletRequest req, HttpServletResponse res) {
 
		try
		{
			String coupilingtype = req.getParameter("couplingType");
		List<Double> couplingTypes = productSelectionService.getAllowableFrequencies(coupilingtype);
		JSONObject mainobject = new JSONObject();
		

		JSONArray budgetandbookingarrayforallcountries = new JSONArray();
		for (Double voltage : couplingTypes) {
			JSONObject mainobject1 = new JSONObject();
			mainobject1.put("frequency", voltage.intValue());
			budgetandbookingarrayforallcountries.add(mainobject1);
		}

		
		mainobject.put("data", budgetandbookingarrayforallcountries);

		 

		 
		return mainobject;
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return null;
		}
	}
	
	
	@RequestMapping(value = "/saveTestReport", method = RequestMethod.POST)
    public @ResponseBody boolean saveTestReport(@RequestParam("formData") String formData,@RequestParam("gridData") String gridData) throws Exception
    {
	 	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		//byte[] bytes=file.getBytes();
		try 
		{
			Integer maxId=commonService.getMaximumID("id","TestReport");
			//logger.info("------------String-----------------"+formData.toString());
		    TestReportBean testReportBean = new ObjectMapper().readValue(formData.toString(), TestReportBean.class);
		    
		    TestReport testReport = prepareModelForTestReport(testReportBean);
		    if(testReportBean.getId()!=null)
		    {
		    	TestReport testReport1=(TestReport)productSelectionService.getEntityByIdVarchar(testReportBean.getCustomerAssembyNumber(), TestReport.class);
		    	
		    	/*if(file.getSize()!=0)
		    	{
		    		testReport.setBbsimage(bytes);	
		    	}
		    	else
		    	{
		    		testReport.setBbsimage(testReport1.getBbsimage());
		    		testReport.setId(testReport1.getId());
		    	}*/
		    	
		    	
			    
		    }
		    else
		    {
		    	testReport.setId(maxId);	
		    	//testReport.setBbsimage(bytes);
		    }

			org.json.JSONArray rr=new org.json.JSONArray(gridData);
			
			//productSelectionService.deleteTestReportDtls(testReport);
			
			for(int i=0;i<rr.length();i++)
			{
				org.json.JSONObject ob=(org.json.JSONObject)rr.get(i);
				if(i==0)
				{
					TestReportDtlsBean1 testReportDtlsBean1 = new ObjectMapper().readValue(rr.get(i).toString(), TestReportDtlsBean1.class);
					TestReportDtls testReportDtls=prepareModelForTestReportDtls1(testReportDtlsBean1,testReportBean);
					entityList.add(testReportDtls);	
					
				}
				else if(i==1)
				{
					TestReportDtlsBean2 testReportDtlsBean2 = new ObjectMapper().readValue(rr.get(i).toString(), TestReportDtlsBean2.class);
					TestReportDtls testReportDtls=prepareModelForTestReportDtls2(testReportDtlsBean2,testReportBean);
					entityList.add(testReportDtls);	 
				}
				else if(i==2)
				{
					TestReportDtlsBean3 testReportDtlsBean3 = new ObjectMapper().readValue(rr.get(i).toString(), TestReportDtlsBean3.class);
					TestReportDtls testReportDtls=prepareModelForTestReportDtls3(testReportDtlsBean3,testReportBean);
					entityList.add(testReportDtls);	 
				}
				else if(i==3)
				{
					TestReportDtlsBean4 testReportDtlsBean4 = new ObjectMapper().readValue(rr.get(i).toString(), TestReportDtlsBean4.class);
					TestReportDtls testReportDtls=prepareModelForTestReportDtls4(testReportDtlsBean4,testReportBean);
					entityList.add(testReportDtls);	 
				}
				else if(i==4)
				{
					TestReportDtlsBean5 testReportDtlsBean5 = new ObjectMapper().readValue(rr.get(i).toString(), TestReportDtlsBean5.class);
					TestReportDtls testReportDtls=prepareModelForTestReportDtls5(testReportDtlsBean5,testReportBean);
					entityList.add(testReportDtls);	 
				}
				
				//TestReportDtls testReportDtls=prepareModelForTestReportDtls(testReportDtlsBean,testReportBean);
				//TestReportDtls testReportDtls=prepareModelForTestReportDtls(jso,testReportBean);				
				// entityList.add(testReportDtls);	 	
				
			}
			
		    /*if(usersBean.getUserId()==null)
		    {
				ext=getFileExtension(file.getOriginalFilename());
				ext1=getFileExtension(usersign.getOriginalFilename());
				photoName=maxId+"_photo"+"."+ext;
				signName=maxId+"_sign"+"."+ext1;
				
				photoPath="images/"+photoName;
				signaturePath="images/"+signName;
				 
				usersBean.setPhotoPath(photoPath);
				usersBean.setSignaturePath(signaturePath);
				//usersBean.setUserId(maxId);
		    }
		    else
		    { 	
				if(file.getSize()!=0)
				{						
					ext=getFileExtension(file.getOriginalFilename());
					photoName=usersBean.getUserId()+"_photo"+"."+ext;
					photoPath="images/"+photoName;
					usersBean.setPhotoPath(photoPath);
				}
				if(usersign.getSize()!=0)
				{						
					ext1=getFileExtension(usersign.getOriginalFilename());						
					signName=usersBean.getUserId()+"_sign"+"."+ext1;						
					signaturePath="images/"+signName;
					usersBean.setSignaturePath(signaturePath);
				}

		    }*/
		    entityList.add(testReport);		    
		    
		    //logger.info("The Data ---------------------------"+testReport.toString());
		    
			isInsertSuccess = productSelectionService.saveEntitiesTestReport(entityList,testReport);
			
			
/*			if(isInsertSuccess)
			{
				isInsertSuccess =false;
				
				File dir1=new File(servletContext.getRealPath("/")+"images/");
				
				if (!dir1.exists())
					dir1.mkdirs();
				
				if(file.getSize()!=0)
				{					
					byte[] bytes1 = file.getBytes();
	
					File serverFile1 = new File(dir1.getAbsolutePath()+ File.separator + photoName);					
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}
				if(usersign.getSize()!=0)
				{
					byte[] bytes2 = usersign.getBytes();
					
					File serverFile2 = new File(dir1.getAbsolutePath()+ File.separator + signName);					
					BufferedOutputStream stream2 = new BufferedOutputStream(new FileOutputStream(serverFile2));
					stream2.write(bytes2);
					stream2.close();
				}
				isInsertSuccess =true;
			}*/
		   return isInsertSuccess;
		}
		catch (Exception e)
		{ 
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
     }
	
 
		 
	 private TestReport prepareModelForTestReport(TestReportBean testReportBean) throws Exception
	 {
		 
		//SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//java.util.Date dates =  simpleDateFormat.parse(testReportBean.getEndDateTime());

		//java.sql.Timestamp stimestamp = java.sql.Timestamp.valueOf(testReportBean.getStartDateTime());
		//java.sql.Timestamp etimestamp = java.sql.Timestamp.valueOf(testReportBean.getEndDateTime());
				
				String[] accArray=testReportBean.getAccessories();
				String acc="";
				for(int i=0;i<accArray.length;i++)
				{
					acc=acc+accArray[i]+",";
					
				}
				acc=acc.substring(0,acc.length()-1);
				
				String[] tbArray=testReportBean.getAccessories();
				String tbb="";
				for(int i=0;i<tbArray.length;i++)
				{
					tbb=tbb+tbArray[i]+",";
					
				}
				tbb=tbb.substring(0,tbb.length()-1);
				
				String[] cbArray=testReportBean.getCableEntry();
				String cbb="";
				for(int i=0;i<cbArray.length;i++)
				{
					cbb=cbb+cbArray[i]+",";
					
				}
				cbb=cbb.substring(0,cbb.length()-1);
				
				
		 TestReport testReport = new TestReport();	
		 testReport.setAccessories(acc);
		 testReport.setAssembledby(testReportBean.getAssembledBy());
		 testReport.setAssembleddate(DateUtils.getSqlDateFromString(testReportBean.getAssembledDate(),Constants.GenericDateFormat.DATE_FORMAT));
		 //testReport.setBbsimage(bbsimage);
		 testReport.setBrakevoltage(testReportBean.getBrakeVoltage());
		 testReport.setChangebrakekit(testReportBean.getChangeBrakeKit());
		 testReport.setCouplingtype(testReportBean.getCouplingType());
		 testReport.setCustomerpono(testReportBean.getCustomerAssembyNumber());
		 testReport.setDatereceived(DateUtils.getSqlDateFromString(testReportBean.getDateReceived(),Constants.GenericDateFormat.DATE_FORMAT));
		 testReport.setDaterequired(DateUtils.getSqlDateFromString(testReportBean.getDateRequired(),Constants.GenericDateFormat.DATE_FORMAT));
		 testReport.setEnddatetime(java.sql.Time.valueOf(testReportBean.getEndDateTime()));
		 testReport.setFrequency(testReportBean.getFrequency());
		 testReport.setInputshaft(testReportBean.getInputShaft());
		 testReport.setLubrication(testReportBean.getLubrication());//
		 testReport.setModel(testReportBean.getModel());
		 testReport.setMotorbrand(testReportBean.getMotorBrand());
		 testReport.setMotordirection(testReportBean.getMotorDirection());
		 testReport.setMotortype(testReportBean.getMotorType());
		 testReport.setOutputshaft(testReportBean.getOutputShaft());
		 testReport.setOutputrpm(testReportBean.getOutputrpm());
		 testReport.setPaintColor(testReportBean.getPaintColor());
		 testReport.setProtection(testReportBean.getProtection());
		 testReport.setSpaceheater(testReportBean.getSpaceHeater());		 
		 testReport.setStartdatetime(java.sql.Time.valueOf(testReportBean.getStartDateTime()));
		 testReport.setTerminalbox(tbb);
		 testReport.setTestperformedby(testReportBean.getTestpPerformedBy());
		 testReport.setThermister(testReportBean.getThermister());
		 testReport.setVoltage(testReportBean.getVoltage());
		 testReport.setCustomername(testReportBean.getCustomerName());
		testReport.setCableentry(cbb);
		 testReport.setPhasevoltage(testReportBean.getPhaseVoltage());
		 return testReport;
	}
	 
	 
	 private TestReportDtls prepareModelForTestReportDtls1(TestReportDtlsBean1 testReportDtlsBean,TestReportBean testReportBean) 
	 {

		 	TestReportDtls testReportDtls=new TestReportDtls();
		 	
		 	testReportDtls.setAirpressuretest(testReportDtlsBean.getAirPressureTest1());
		 	testReportDtls.setConcentricity(testReportDtlsBean.getConcentricity1());
		 	testReportDtls.setConfirmbbssize(testReportDtlsBean.getConfirmbbsSize1());
		 	testReportDtls.setMeasuredampere(testReportDtlsBean.getMeasuredAmpere1());
		 	testReportDtls.setMeasuredfinalwt(testReportDtlsBean.getMeasuredFinalWeight1());
		 	testReportDtls.setMeasuredgearheadwt(testReportDtlsBean.getMeasuredGearHeadWeight1());
		 	testReportDtls.setMeasuredgreasewt(testReportDtlsBean.getMeasuredGreaseWeight1());
		 	testReportDtls.setMeasuredippower(testReportDtlsBean.getMeasuredIpPower1());
		 	testReportDtls.setMeasuredoppower(testReportDtlsBean.getMeasuredOpPower1());
		 	testReportDtls.setMeasuredsoundlevelccw(testReportDtlsBean.getMeasuredSoundLevelccw1());
		 	testReportDtls.setMeasuredopspeed(testReportDtlsBean.getMeasuredOpSpeed1());
		 	testReportDtls.setMeasuredsoundlevelcw(testReportDtlsBean.getMeasuredSoundLevelcw1());
			testReportDtls.setMeasuredspaceheatercablepresent(testReportDtlsBean.getMeasuredSpaceHeaterCablePresent1());
			testReportDtls.setMeasuredtemperatureinitial(testReportDtlsBean.getMeasuredTemperatureInitial1());
			testReportDtls.setMeasuredtemperaturefinal(testReportDtlsBean.getMeasuredTemperatureFinal1());
			testReportDtls.setMeasuredthermistorcablepresent(testReportDtlsBean.getMeasuredThermistorCablePresent1());
			testReportDtls.setMotorslno(testReportDtlsBean.getMotorslno1());
		 	testReportDtls.setOverallvibration(testReportDtlsBean.getOverallVibration1());
		 	testReportDtls.setSlno(testReportDtlsBean.getSlno1());
		 	testReportDtls.setCustomerpono(testReportBean.getCustomerAssembyNumber());
		 	testReportDtls.setMeasuredipvoltage(testReportDtlsBean.getMeasuredIpVoltage1());
			return testReportDtls;
			
		}
	 
	 private TestReportDtls prepareModelForTestReportDtls2(TestReportDtlsBean2 testReportDtlsBean,TestReportBean testReportBean) 
	 {

		 	TestReportDtls testReportDtls=new TestReportDtls();
		 	
		 	testReportDtls.setAirpressuretest(testReportDtlsBean.getAirPressureTest2());
		 	testReportDtls.setConcentricity(testReportDtlsBean.getConcentricity2());
		 	testReportDtls.setConfirmbbssize(testReportDtlsBean.getConfirmbbsSize2());
		 	testReportDtls.setMeasuredampere(testReportDtlsBean.getMeasuredAmpere2());
		 	testReportDtls.setMeasuredfinalwt(testReportDtlsBean.getMeasuredFinalWeight2());
		 	testReportDtls.setMeasuredgearheadwt(testReportDtlsBean.getMeasuredGearHeadWeight2());
		 	testReportDtls.setMeasuredgreasewt(testReportDtlsBean.getMeasuredGreaseWeight2());
		 	testReportDtls.setMeasuredippower(testReportDtlsBean.getMeasuredIpPower2());
		 	testReportDtls.setMeasuredoppower(testReportDtlsBean.getMeasuredOpPower2());
		 	testReportDtls.setMeasuredsoundlevelccw(testReportDtlsBean.getMeasuredSoundLevelccw2());
		 	testReportDtls.setMeasuredopspeed(testReportDtlsBean.getMeasuredOpSpeed2());
		 	testReportDtls.setMeasuredsoundlevelcw(testReportDtlsBean.getMeasuredSoundLevelcw2());
			testReportDtls.setMeasuredspaceheatercablepresent(testReportDtlsBean.getMeasuredSpaceHeaterCablePresent2());
			testReportDtls.setMeasuredtemperatureinitial(testReportDtlsBean.getMeasuredTemperatureInitial2());
			testReportDtls.setMeasuredtemperaturefinal(testReportDtlsBean.getMeasuredTemperatureFinal2());
			testReportDtls.setMeasuredthermistorcablepresent(testReportDtlsBean.getMeasuredThermistorCablePresent2());
			testReportDtls.setMotorslno(testReportDtlsBean.getMotorslno2());
		 	testReportDtls.setOverallvibration(testReportDtlsBean.getOverallVibration2());
		 	testReportDtls.setSlno(testReportDtlsBean.getSlno2());
		 	testReportDtls.setCustomerpono(testReportBean.getCustomerAssembyNumber());
		 	testReportDtls.setMeasuredipvoltage(testReportDtlsBean.getMeasuredIpVoltage2());
			return testReportDtls;
			
		}
	 private TestReportDtls prepareModelForTestReportDtls3(TestReportDtlsBean3 testReportDtlsBean,TestReportBean testReportBean) 
	 {

		 	TestReportDtls testReportDtls=new TestReportDtls();
		 	
		 	testReportDtls.setAirpressuretest(testReportDtlsBean.getAirPressureTest3());
		 	testReportDtls.setConcentricity(testReportDtlsBean.getConcentricity3());
		 	testReportDtls.setConfirmbbssize(testReportDtlsBean.getConfirmbbsSize3());
		 	testReportDtls.setMeasuredampere(testReportDtlsBean.getMeasuredAmpere3());
		 	testReportDtls.setMeasuredfinalwt(testReportDtlsBean.getMeasuredFinalWeight3());
		 	testReportDtls.setMeasuredgearheadwt(testReportDtlsBean.getMeasuredGearHeadWeight3());
		 	testReportDtls.setMeasuredgreasewt(testReportDtlsBean.getMeasuredGreaseWeight3());
		 	testReportDtls.setMeasuredippower(testReportDtlsBean.getMeasuredIpPower3());
		 	testReportDtls.setMeasuredoppower(testReportDtlsBean.getMeasuredOpPower3());
		 	testReportDtls.setMeasuredsoundlevelccw(testReportDtlsBean.getMeasuredSoundLevelccw3());
		 	testReportDtls.setMeasuredopspeed(testReportDtlsBean.getMeasuredOpSpeed3());
		 	testReportDtls.setMeasuredsoundlevelcw(testReportDtlsBean.getMeasuredSoundLevelcw3());
			testReportDtls.setMeasuredspaceheatercablepresent(testReportDtlsBean.getMeasuredSpaceHeaterCablePresent3());
			testReportDtls.setMeasuredtemperatureinitial(testReportDtlsBean.getMeasuredTemperatureInitial3());
			testReportDtls.setMeasuredtemperaturefinal(testReportDtlsBean.getMeasuredTemperatureFinal3());
			testReportDtls.setMeasuredthermistorcablepresent(testReportDtlsBean.getMeasuredThermistorCablePresent3());
			testReportDtls.setMotorslno(testReportDtlsBean.getMotorslno3());
		 	testReportDtls.setOverallvibration(testReportDtlsBean.getOverallVibration3());
		 	testReportDtls.setSlno(testReportDtlsBean.getSlno3());
		 	testReportDtls.setCustomerpono(testReportBean.getCustomerAssembyNumber());
		 	testReportDtls.setMeasuredipvoltage(testReportDtlsBean.getMeasuredIpVoltage3());
			return testReportDtls;
			
		}
	 private TestReportDtls prepareModelForTestReportDtls4(TestReportDtlsBean4 testReportDtlsBean,TestReportBean testReportBean) 
	 {

		 	TestReportDtls testReportDtls=new TestReportDtls();
		 	
		 	testReportDtls.setAirpressuretest(testReportDtlsBean.getAirPressureTest4());
		 	testReportDtls.setConcentricity(testReportDtlsBean.getConcentricity4());
		 	testReportDtls.setConfirmbbssize(testReportDtlsBean.getConfirmbbsSize4());
		 	testReportDtls.setMeasuredampere(testReportDtlsBean.getMeasuredAmpere4());
		 	testReportDtls.setMeasuredfinalwt(testReportDtlsBean.getMeasuredFinalWeight4());
		 	testReportDtls.setMeasuredgearheadwt(testReportDtlsBean.getMeasuredGearHeadWeight4());
		 	testReportDtls.setMeasuredgreasewt(testReportDtlsBean.getMeasuredGreaseWeight4());
		 	testReportDtls.setMeasuredippower(testReportDtlsBean.getMeasuredIpPower4());
		 	testReportDtls.setMeasuredoppower(testReportDtlsBean.getMeasuredOpPower4());
		 	testReportDtls.setMeasuredsoundlevelccw(testReportDtlsBean.getMeasuredSoundLevelccw4());
		 	testReportDtls.setMeasuredopspeed(testReportDtlsBean.getMeasuredOpSpeed4());
		 	testReportDtls.setMeasuredsoundlevelcw(testReportDtlsBean.getMeasuredSoundLevelcw4());
			testReportDtls.setMeasuredspaceheatercablepresent(testReportDtlsBean.getMeasuredSpaceHeaterCablePresent4());
			testReportDtls.setMeasuredtemperatureinitial(testReportDtlsBean.getMeasuredTemperatureInitial4());
			testReportDtls.setMeasuredtemperaturefinal(testReportDtlsBean.getMeasuredTemperatureFinal4());
			testReportDtls.setMeasuredthermistorcablepresent(testReportDtlsBean.getMeasuredThermistorCablePresent4());
			testReportDtls.setMotorslno(testReportDtlsBean.getMotorslno4());
		 	testReportDtls.setOverallvibration(testReportDtlsBean.getOverallVibration4());
		 	testReportDtls.setSlno(testReportDtlsBean.getSlno4());
		 	testReportDtls.setCustomerpono(testReportBean.getCustomerAssembyNumber());
		 	testReportDtls.setMeasuredipvoltage(testReportDtlsBean.getMeasuredIpVoltage4());
			return testReportDtls;
			
		}
	 private TestReportDtls prepareModelForTestReportDtls5(TestReportDtlsBean5 testReportDtlsBean,TestReportBean testReportBean) 
	 {

		 	TestReportDtls testReportDtls=new TestReportDtls();
		 	
		 	testReportDtls.setAirpressuretest(testReportDtlsBean.getAirPressureTest5());
		 	testReportDtls.setConcentricity(testReportDtlsBean.getConcentricity5());
		 	testReportDtls.setConfirmbbssize(testReportDtlsBean.getConfirmbbsSize5());
		 	testReportDtls.setMeasuredampere(testReportDtlsBean.getMeasuredAmpere5());
		 	testReportDtls.setMeasuredfinalwt(testReportDtlsBean.getMeasuredFinalWeight5());
		 	testReportDtls.setMeasuredgearheadwt(testReportDtlsBean.getMeasuredGearHeadWeight5());
		 	testReportDtls.setMeasuredgreasewt(testReportDtlsBean.getMeasuredGreaseWeight5());
		 	testReportDtls.setMeasuredippower(testReportDtlsBean.getMeasuredIpPower5());
		 	testReportDtls.setMeasuredoppower(testReportDtlsBean.getMeasuredOpPower5());
		 	testReportDtls.setMeasuredsoundlevelccw(testReportDtlsBean.getMeasuredSoundLevelccw5());
		 	testReportDtls.setMeasuredopspeed(testReportDtlsBean.getMeasuredOpSpeed5());
		 	testReportDtls.setMeasuredsoundlevelcw(testReportDtlsBean.getMeasuredSoundLevelcw5());
			testReportDtls.setMeasuredspaceheatercablepresent(testReportDtlsBean.getMeasuredSpaceHeaterCablePresent5());
			testReportDtls.setMeasuredtemperatureinitial(testReportDtlsBean.getMeasuredTemperatureInitial5());
			testReportDtls.setMeasuredtemperaturefinal(testReportDtlsBean.getMeasuredTemperatureFinal5());
			testReportDtls.setMeasuredthermistorcablepresent(testReportDtlsBean.getMeasuredThermistorCablePresent5());
			testReportDtls.setMotorslno(testReportDtlsBean.getMotorslno5());
		 	testReportDtls.setOverallvibration(testReportDtlsBean.getOverallVibration5());
		 	testReportDtls.setSlno(testReportDtlsBean.getSlno5());
		 	testReportDtls.setCustomerpono(testReportBean.getCustomerAssembyNumber());
		 	testReportDtls.setMeasuredipvoltage(testReportDtlsBean.getMeasuredIpVoltage5());
			return testReportDtls;
			
		}
	 
	 @RequestMapping(value = "/getAllLTR", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getAllLTR(@RequestBody String jsonData) throws Exception {
			TestReportBean testReportBean =null;
			
			
			org.json.JSONObject response = new org.json.JSONObject();
			
			TestReport testReport=(TestReport)productSelectionService.getEntityByIdVarchar(jsonData, TestReport.class);
			
			if(testReport!=null)
				testReportBean =prepareBeanForTestReport(testReport);
			
			
			
			List<TestReportDtls> testReportDtls =productSelectionService.getTestReportDtlsByPONumber(testReport.getCustomerpono());
			
			List testReportDtlsBeans =prepareListofBeanForTestReportDtls(testReportDtls);
			
			org.json.JSONObject testReportJson=new org.json.JSONObject(testReportBean);
			org.json.JSONArray testReportDtlsArray = new org.json.JSONArray(testReportDtlsBeans);
			
			//org.json.JSONObject bankJson=new org.json.JSONObject(bankMasterBean);
			//org.json.JSONArray branchArray = new org.json.JSONArray(brancheBeans);
			
			
			//org.json.JSONObject bankJson=new org.json.JSONObject(bank);
			//org.json.JSONArray branchArray = new org.json.JSONArray(branches);
			
			//logger.info("----bankJson------"+bankJson);
			//logger.info("----branches Details------"+branchArray.toString());
			
			/* List<BankMasterBean> bankBeans = prepareListofBeanForBanks(bankService.listBanks());
			
			List<BranchBean> branchBeans = prepareListofBeanForBranches(bankService.listBranches());
			
			org.json.JSONArray bankArray = new org.json.JSONArray(bankBeans);
			
			org.json.JSONArray branchArray = new org.json.JSONArray(branchBeans); */
			
			
			//branchArray.put(bankJson);
			response.put("testReport",testReportJson);
			response.put("testReportDtls",testReportDtlsArray);		
			
			//logger.info("------  Load Test Report ---------"+response.toString());
			
			return response.toString();
		}

		
		
		private TestReportBean prepareBeanForTestReport(TestReport testReport) {
			
				TestReportBean bean = new TestReportBean();
				bean.setId(testReport.getId());
				String[] stacc=testReport.getAccessories().split(",");
				String[] sttbb=testReport.getTerminalbox().split(",");
				bean.setAccessories(stacc);
				bean.setAssembledBy(testReport.getAssembledby());
				bean.setAssembledDate(DateUtils.formatDate(testReport.getAssembleddate(),Constants.GenericDateFormat.DATE_FORMAT));
				bean.setBrakeVoltage(testReport.getBrakevoltage());
				bean.setChangeBrakeKit(testReport.getChangebrakekit());
				bean.setCouplingType(testReport.getCouplingtype());
				bean.setCustomerAssembyNumber(testReport.getCustomerpono());
				bean.setDateReceived(DateUtils.formatDate(testReport.getDatereceived(),Constants.GenericDateFormat.DATE_FORMAT));
				bean.setDateRequired(DateUtils.formatDate(testReport.getDaterequired(),Constants.GenericDateFormat.DATE_FORMAT));
				bean.setEndDateTime(testReport.getEnddatetime().toString());
				bean.setFrequency(testReport.getFrequency());
				bean.setInputShaft(testReport.getInputshaft());
				bean.setLubrication(testReport.getLubrication());
				bean.setModel(testReport.getModel());
				bean.setMotorBrand(testReport.getMotorbrand());
				bean.setMotorDirection(testReport.getMotordirection());
				bean.setMotorType(testReport.getMotortype());
				bean.setOutputrpm(testReport.getOutputrpm());
				bean.setOutputShaft(testReport.getOutputshaft());
				bean.setPaintColor(testReport.getPaintColor());
				bean.setProtection(testReport.getProtection());
				bean.setSpaceHeater(testReport.getSpaceheater());
				bean.setStartDateTime(testReport.getStartdatetime().toString());
				bean.setTerminalBox(sttbb);
				bean.setTestpPerformedBy(testReport.getTestperformedby());
				bean.setThermister(testReport.getThermister());
				bean.setVoltage(testReport.getVoltage());
				bean.setCustomerName(testReport.getCustomername());
				bean.setCableEntry(testReport.getCableentry().split(","));
				bean.setPhaseVoltage(testReport.getPhasevoltage());
		/*		if(testReport.getBbsimage()!=null)
				{
					byte[] encoded = Base64.encodeBase64(testReport.getBbsimage());
					String encodedString=new String(encoded);
					bean.setBbsimage(encodedString);
				}*/
				
				//logger.info("encodedString-----------------"+encodedString);

					
			return bean;
		}
		
		
		private List prepareListofBeanForTestReportDtls(
				List<TestReportDtls> testReportDtls) {
			List beans = null;
			
			TestReportDtlsBean1 testReportDtlsBean1=null;
			TestReportDtlsBean2 testReportDtlsBean2=null;
			TestReportDtlsBean3 testReportDtlsBean3=null;
			TestReportDtlsBean4 testReportDtlsBean4=null;
			TestReportDtlsBean5 testReportDtlsBean5=null;
			
			if (testReportDtls != null && !testReportDtls.isEmpty()) 
			{
				beans = new ArrayList();
				for(int i=0;i<testReportDtls.size();i++)
				{
					if(i==0)
					{
						testReportDtlsBean1 = new TestReportDtlsBean1();
						testReportDtlsBean1.setAirPressureTest1(testReportDtls.get(0).getAirpressuretest());
						testReportDtlsBean1.setConcentricity1(testReportDtls.get(0).getConcentricity());
						testReportDtlsBean1.setConfirmbbsSize1(testReportDtls.get(0).getConfirmbbssize());
						testReportDtlsBean1.setMeasuredAmpere1(testReportDtls.get(0).getMeasuredampere());
						testReportDtlsBean1.setMeasuredFinalWeight1(testReportDtls.get(0).getMeasuredfinalwt());
						testReportDtlsBean1.setMeasuredGearHeadWeight1(testReportDtls.get(0).getMeasuredgearheadwt());
						testReportDtlsBean1.setMeasuredIpPower1(testReportDtls.get(0).getMeasuredippower());
						testReportDtlsBean1.setMeasuredOpPower1(testReportDtls.get(0).getMeasuredoppower());
						testReportDtlsBean1.setMeasuredIpVoltage1(testReportDtls.get(0).getMeasuredipvoltage());
						testReportDtlsBean1.setMeasuredOpSpeed1(testReportDtls.get(0).getMeasuredopspeed());
						testReportDtlsBean1.setMeasuredSoundLevelccw1(testReportDtls.get(0).getMeasuredsoundlevelccw());
						testReportDtlsBean1.setMeasuredSoundLevelcw1(testReportDtls.get(0).getMeasuredsoundlevelcw());
						testReportDtlsBean1.setMeasuredTemperatureFinal1(testReportDtls.get(0).getMeasuredtemperaturefinal());
						testReportDtlsBean1.setMeasuredTemperatureInitial1(testReportDtls.get(0).getMeasuredtemperatureinitial());
						testReportDtlsBean1.setMeasuredSpaceHeaterCablePresent1(testReportDtls.get(0).getMeasuredspaceheatercablepresent());
						testReportDtlsBean1.setMeasuredThermistorCablePresent1(testReportDtls.get(0).getMeasuredthermistorcablepresent());
						testReportDtlsBean1.setSlno1(testReportDtls.get(0).getSlno());
						testReportDtlsBean1.setMotorslno1(testReportDtls.get(0).getMotorslno());
						testReportDtlsBean1.setOverallVibration1(testReportDtls.get(0).getOverallvibration());
						testReportDtlsBean1.setMeasuredGreaseWeight1(testReportDtls.get(0).getMeasuredgreasewt());
						beans.add(testReportDtlsBean1);
					}
					if(i==1)
					{

						testReportDtlsBean2 = new TestReportDtlsBean2();
						testReportDtlsBean2.setAirPressureTest2(testReportDtls.get(0).getAirpressuretest());
						testReportDtlsBean2.setConcentricity2(testReportDtls.get(0).getConcentricity());
						testReportDtlsBean2.setConfirmbbsSize2(testReportDtls.get(0).getConfirmbbssize());
						testReportDtlsBean2.setMeasuredAmpere2(testReportDtls.get(0).getMeasuredampere());
						testReportDtlsBean2.setMeasuredFinalWeight2(testReportDtls.get(0).getMeasuredfinalwt());
						testReportDtlsBean2.setMeasuredGearHeadWeight2(testReportDtls.get(0).getMeasuredgearheadwt());
						testReportDtlsBean2.setMeasuredIpPower2(testReportDtls.get(0).getMeasuredippower());
						testReportDtlsBean2.setMeasuredOpPower2(testReportDtls.get(0).getMeasuredoppower());
						testReportDtlsBean2.setMeasuredIpVoltage2(testReportDtls.get(0).getMeasuredipvoltage());
						testReportDtlsBean2.setMeasuredOpSpeed2(testReportDtls.get(0).getMeasuredopspeed());
						testReportDtlsBean2.setMeasuredSoundLevelccw2(testReportDtls.get(0).getMeasuredsoundlevelccw());
						testReportDtlsBean2.setMeasuredSoundLevelcw2(testReportDtls.get(0).getMeasuredsoundlevelcw());
						testReportDtlsBean2.setMeasuredTemperatureFinal2(testReportDtls.get(0).getMeasuredtemperaturefinal());
						testReportDtlsBean2.setMeasuredTemperatureInitial2(testReportDtls.get(0).getMeasuredtemperatureinitial());
						testReportDtlsBean2.setMeasuredSpaceHeaterCablePresent2(testReportDtls.get(0).getMeasuredspaceheatercablepresent());
						testReportDtlsBean2.setMeasuredThermistorCablePresent2(testReportDtls.get(0).getMeasuredthermistorcablepresent());
						testReportDtlsBean2.setSlno2(testReportDtls.get(0).getSlno());
						testReportDtlsBean2.setMotorslno2(testReportDtls.get(0).getMotorslno());
						testReportDtlsBean2.setOverallVibration2(testReportDtls.get(0).getOverallvibration());
						testReportDtlsBean2.setMeasuredGreaseWeight2(testReportDtls.get(0).getMeasuredgreasewt());
						beans.add(testReportDtlsBean2);
						
					}
					if(i==2)
					{
						testReportDtlsBean3 = new TestReportDtlsBean3();
						testReportDtlsBean3.setAirPressureTest3(testReportDtls.get(0).getAirpressuretest());
						testReportDtlsBean3.setConcentricity3(testReportDtls.get(0).getConcentricity());
						testReportDtlsBean3.setConfirmbbsSize3(testReportDtls.get(0).getConfirmbbssize());
						testReportDtlsBean3.setMeasuredAmpere3(testReportDtls.get(0).getMeasuredampere());
						testReportDtlsBean3.setMeasuredFinalWeight3(testReportDtls.get(0).getMeasuredfinalwt());
						testReportDtlsBean3.setMeasuredGearHeadWeight3(testReportDtls.get(0).getMeasuredgearheadwt());
						testReportDtlsBean3.setMeasuredIpPower3(testReportDtls.get(0).getMeasuredippower());
						testReportDtlsBean3.setMeasuredOpPower3(testReportDtls.get(0).getMeasuredoppower());
						testReportDtlsBean3.setMeasuredIpVoltage3(testReportDtls.get(0).getMeasuredipvoltage());
						testReportDtlsBean3.setMeasuredOpSpeed3(testReportDtls.get(0).getMeasuredopspeed());
						testReportDtlsBean3.setMeasuredSoundLevelccw3(testReportDtls.get(0).getMeasuredsoundlevelccw());
						testReportDtlsBean3.setMeasuredSoundLevelcw3(testReportDtls.get(0).getMeasuredsoundlevelcw());
						testReportDtlsBean3.setMeasuredTemperatureFinal3(testReportDtls.get(0).getMeasuredtemperaturefinal());
						testReportDtlsBean3.setMeasuredTemperatureInitial3(testReportDtls.get(0).getMeasuredtemperatureinitial());
						testReportDtlsBean3.setMeasuredSpaceHeaterCablePresent3(testReportDtls.get(0).getMeasuredspaceheatercablepresent());
						testReportDtlsBean3.setMeasuredThermistorCablePresent3(testReportDtls.get(0).getMeasuredthermistorcablepresent());
						testReportDtlsBean3.setSlno3(testReportDtls.get(0).getSlno());
						testReportDtlsBean3.setMotorslno3(testReportDtls.get(0).getMotorslno());
						testReportDtlsBean3.setOverallVibration3(testReportDtls.get(0).getOverallvibration());
						testReportDtlsBean3.setMeasuredGreaseWeight3(testReportDtls.get(0).getMeasuredgreasewt());
						beans.add(testReportDtlsBean3);
						
					}
					if(i==3)
					{
						testReportDtlsBean4 = new TestReportDtlsBean4();
						testReportDtlsBean4.setAirPressureTest4(testReportDtls.get(0).getAirpressuretest());
						testReportDtlsBean4.setConcentricity4(testReportDtls.get(0).getConcentricity());
						testReportDtlsBean4.setConfirmbbsSize4(testReportDtls.get(0).getConfirmbbssize());
						testReportDtlsBean4.setMeasuredAmpere4(testReportDtls.get(0).getMeasuredampere());
						testReportDtlsBean4.setMeasuredFinalWeight4(testReportDtls.get(0).getMeasuredfinalwt());
						testReportDtlsBean4.setMeasuredGearHeadWeight4(testReportDtls.get(0).getMeasuredgearheadwt());
						testReportDtlsBean4.setMeasuredIpPower4(testReportDtls.get(0).getMeasuredippower());
						testReportDtlsBean4.setMeasuredOpPower4(testReportDtls.get(0).getMeasuredoppower());
						testReportDtlsBean4.setMeasuredIpVoltage4(testReportDtls.get(0).getMeasuredipvoltage());
						testReportDtlsBean4.setMeasuredOpSpeed4(testReportDtls.get(0).getMeasuredopspeed());
						testReportDtlsBean4.setMeasuredSoundLevelccw4(testReportDtls.get(0).getMeasuredsoundlevelccw());
						testReportDtlsBean4.setMeasuredSoundLevelcw4(testReportDtls.get(0).getMeasuredsoundlevelcw());
						testReportDtlsBean4.setMeasuredTemperatureFinal4(testReportDtls.get(0).getMeasuredtemperaturefinal());
						testReportDtlsBean4.setMeasuredTemperatureInitial4(testReportDtls.get(0).getMeasuredtemperatureinitial());
						testReportDtlsBean4.setMeasuredSpaceHeaterCablePresent4(testReportDtls.get(0).getMeasuredspaceheatercablepresent());
						testReportDtlsBean4.setMeasuredThermistorCablePresent4(testReportDtls.get(0).getMeasuredthermistorcablepresent());
						testReportDtlsBean4.setSlno4(testReportDtls.get(0).getSlno());
						testReportDtlsBean4.setMotorslno4(testReportDtls.get(0).getMotorslno());
						testReportDtlsBean4.setOverallVibration4(testReportDtls.get(0).getOverallvibration());
						testReportDtlsBean4.setMeasuredGreaseWeight4(testReportDtls.get(0).getMeasuredgreasewt());
						
						beans.add(testReportDtlsBean4);
					}
					if(i==4)
					{
						testReportDtlsBean5 = new TestReportDtlsBean5();
						testReportDtlsBean5.setAirPressureTest5(testReportDtls.get(0).getAirpressuretest());
						testReportDtlsBean5.setConcentricity5(testReportDtls.get(0).getConcentricity());
						testReportDtlsBean5.setConfirmbbsSize5(testReportDtls.get(0).getConfirmbbssize());
						testReportDtlsBean5.setMeasuredAmpere5(testReportDtls.get(0).getMeasuredampere());
						testReportDtlsBean5.setMeasuredFinalWeight5(testReportDtls.get(0).getMeasuredfinalwt());
						testReportDtlsBean5.setMeasuredGearHeadWeight5(testReportDtls.get(0).getMeasuredgearheadwt());
						testReportDtlsBean5.setMeasuredIpPower5(testReportDtls.get(0).getMeasuredippower());
						testReportDtlsBean5.setMeasuredOpPower5(testReportDtls.get(0).getMeasuredoppower());
						testReportDtlsBean5.setMeasuredIpVoltage5(testReportDtls.get(0).getMeasuredipvoltage());
						testReportDtlsBean5.setMeasuredOpSpeed5(testReportDtls.get(0).getMeasuredopspeed());
						testReportDtlsBean5.setMeasuredSoundLevelccw5(testReportDtls.get(0).getMeasuredsoundlevelccw());
						testReportDtlsBean5.setMeasuredSoundLevelcw5(testReportDtls.get(0).getMeasuredsoundlevelcw());
						testReportDtlsBean5.setMeasuredTemperatureFinal5(testReportDtls.get(0).getMeasuredtemperaturefinal());
						testReportDtlsBean5.setMeasuredTemperatureInitial5(testReportDtls.get(0).getMeasuredtemperatureinitial());
						testReportDtlsBean5.setMeasuredSpaceHeaterCablePresent5(testReportDtls.get(0).getMeasuredspaceheatercablepresent());
						testReportDtlsBean5.setMeasuredThermistorCablePresent5(testReportDtls.get(0).getMeasuredthermistorcablepresent());
						testReportDtlsBean5.setSlno5(testReportDtls.get(0).getSlno());
						testReportDtlsBean5.setMotorslno5(testReportDtls.get(0).getMotorslno());
						testReportDtlsBean5.setOverallVibration5(testReportDtls.get(0).getOverallvibration());
						testReportDtlsBean5.setMeasuredGreaseWeight5(testReportDtls.get(0).getOverallvibration());
						beans.add(testReportDtlsBean5);
					}
					
				}
			}
			
			

			return beans;
		}
	 
		@RequestMapping(value = "/getServerDate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody	String getServerDate() throws Exception
		{
			org.json.JSONObject response = new org.json.JSONObject();
			Date sysdate = new Date();
			response.put("serverdate", DateUtils.formatDate(sysdate,Constants.GenericDateFormat.DATE_FORMAT));
			return response.toString();
		}
		
		@RequestMapping(value = "/getGreaseAmount", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody String getGreaseAmount(@RequestBody String jsonData) throws Exception 
		{
				org.json.JSONObject response = new org.json.JSONObject();
				
				Double greaseaAmt=0.00;
				
				String[] sp=jsonData.split("-");
				if(sp[0].indexOf('R')!=-1)
				{
					Integer framesize=Integer.parseInt(sp[1].replaceAll("[\\D]", ""));

					List hyphonicGreeseAmount =productSelectionService.loadHyphonicGreeseAmount(framesize);
					if(hyphonicGreeseAmount!=null && !(hyphonicGreeseAmount.isEmpty()))
					{
						
						HyphonicGreeseAmount hGreeseAmount=(HyphonicGreeseAmount)hyphonicGreeseAmount.get(0);
						greaseaAmt=hGreeseAmount.getGreaseamount();
					}
					else
					{
						greaseaAmt=0.00;
					}

					
				}
				else if(sp[0].indexOf('Z')!=-1)
				{
					Integer framesize=Integer.parseInt(sp[1]);
					
					String rationSt = jsonData.substring(jsonData.lastIndexOf("-")+1,jsonData.length());
					Integer ratio=Integer.parseInt(rationSt);
					String flangetype=sp[0].replaceAll("[\\d]", "");
					List prestNeoGreeseAmount =productSelectionService.loadPrestNeoGreeseAmount(framesize,ratio,flangetype);
					if(prestNeoGreeseAmount!=null && !(prestNeoGreeseAmount.isEmpty()))
					{
						PrestNeoGreeseAmount pNeoGreeseAmount=(PrestNeoGreeseAmount)prestNeoGreeseAmount.get(0);
						greaseaAmt=pNeoGreeseAmount.getGreaseamount();
					}
					else
					{
		
						
						greaseaAmt=0.00;
					}
					
				}
				else
				{
					greaseaAmt=0.00;
				}

				
				return greaseaAmt.toString();
				//return testReportBeans;
		}
		
		@RequestMapping(value = "/getHorsePower", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody String getHorsePower(@RequestBody JSONObject jsonData) throws Exception 
		{
			
				org.json.JSONObject response = new org.json.JSONObject();						
				String modelno=(String)jsonData.get("modelno");
				String voltage=(String)jsonData.get("voltage");
				String frequency=(String)jsonData.get("frequency");
				Integer vol=Integer.parseInt(voltage.replaceAll("[\\D]", ""));
				Integer freq=Integer.parseInt(frequency.replaceAll("[\\D]", ""));
				MotorSpecifications motorSpecifications = (MotorSpecifications) hibernateDao.getRowByColumn(MotorSpecifications.class, "modelno", modelno);
				
				List<MotorHorsePower> motorHorsePower =productSelectionService.getHorsePower(freq,vol,motorSpecifications.getPower());
				
				return motorHorsePower.get(0).getKilowatt().toString();
				//return testReportBeans;
		}
		
		@RequestMapping(value = "/getRatioFromOutputRPM", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody String getRatioFromOutputRPM(@RequestBody String jsonData) throws Exception 
		{
			
				org.json.JSONObject response = new org.json.JSONObject();						
				//String modelno=(String)jsonData.get("modelno");
				MotorSpecifications motorSpecifications = (MotorSpecifications) hibernateDao.getRowByColumn(MotorSpecifications.class, "modelno", jsonData);
				
				
				return motorSpecifications.getRatio().toString();
				//return testReportBeans;
		}
		
		@RequestMapping(value = "/loadTestReports", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody String loadTestReports(@RequestBody JSONObject jsonData) throws Exception 
		{
				org.json.JSONObject response = new org.json.JSONObject();
				
				//List<TestReportBean> testReportBeans =prepareBeanForTestReport(productSelectionService.listTestReports());

				//List<TestReportBean> testReportBeans =prepareBeanForTestReport(productSelectionService.loadTestReportsByCurrentDate());
				
				String fromdate=(String)jsonData.get("fromdate");
				String todate=(String)jsonData.get("todate");
				List<TestReportBean> testReportBeans =prepareBeanForTestReport(productSelectionService.loadTestReportsByCustomerName(fromdate,todate));
				
				
				org.json.JSONArray testReportArray = new org.json.JSONArray(testReportBeans);

				response.put("testReport",testReportArray);
				
				logger.info("------  Load Test Report ---------"+response.toString());
				
				return testReportArray.toString();
				//return testReportBeans;
		}

		private List<TestReportBean> prepareBeanForTestReport(List<TestReport> testReports) 
		{
				List<TestReportBean> beans = null;
				
				if (testReports != null && !testReports.isEmpty()) 
				{
					beans = new ArrayList<TestReportBean>();
					TestReportBean bean = null;
					for (TestReport testReport : testReports) 
					{
						bean = new TestReportBean();
						
						bean.setId(testReport.getId());
						bean.setAssembledBy(testReport.getAssembledby());
						bean.setAssembledDate(DateUtils.formatDate(testReport.getAssembleddate(),Constants.GenericDateFormat.DATE_FORMAT));
						bean.setCustomerAssembyNumber(testReport.getCustomerpono());
						bean.setModel(testReport.getModel());
						bean.setMotorBrand(testReport.getMotorbrand());
						bean.setStartDateTime(testReport.getStartdatetime().toString());
						bean.setTestpPerformedBy(testReport.getTestperformedby());

						beans.add(bean);						
					}
				}						
				return beans;
		}
		
		
/*		@RequestMapping(value = "/saveServiceInquiry", method = RequestMethod.POST)
	    public @ResponseBody boolean saveServiceInquiry(@RequestParam("userdrawing") MultipartFile file,@RequestParam("formData") String formData,HttpServletRequest req) throws Exception
	    {
		 	boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			String photoName=null;
			String photoPath=null;
			String ext=null;
			ServletContext servletContext=(ServletContext)req.getSession().getServletContext();
			Integer maxId=commonService.getMaximumID("ServiceInquiry","caseReferenceno");			
			
			ServiceInquiryBean serviceInquiryBean = new ObjectMapper().readValue(formData.toString(), ServiceInquiryBean.class);
					
			
		    if(serviceInquiryBean.getCaseReferenceno()==null)
		    {
		    	if(serviceInquiryBean.getAttachDrawing()!=0)
		    	{
					ext=getFileExtension(file.getOriginalFilename());
					photoName=maxId+"_drawing"+"."+ext;				
					photoPath="images/"+photoName;				 
					serviceInquiryBean.setPhotoLocation(photoPath);
		    	}
		    }
		    else
		    { 	
		    	if(serviceInquiryBean.getAttachDrawing()!=0)
		    	{
					if(file.getSize()!=0)
					{						
						ext=getFileExtension(file.getOriginalFilename());
						photoName=serviceInquiryBean.getCaseReferenceno()+"_drawing"+"."+ext;
						photoPath="images/"+photoName;
						serviceInquiryBean.setPhotoLocation(photoPath);
					}
		    	}

		    }
			ServiceInquiry serviceInquiry = prepareModelForServiceInquiry(serviceInquiryBean);	
			ServiceInquiryDtls serviceInquiryDtls = prepareModelForServiceInquiryDtls(serviceInquiryBean);	
			
			
			entityList.add(serviceInquiry);
			entityList.add(serviceInquiryDtls);
			 
			isInsertSuccess = productSelectionService.saveEntities(entityList);
			
			if(isInsertSuccess)
			{
				if(serviceInquiryBean.getAttachDrawing()!=0)
				{
					isInsertSuccess =false;
					
					File dir1=new File(servletContext.getRealPath("/")+"images/");
					
					if (!dir1.exists())
						dir1.mkdirs();
					
					if(file.getSize()!=0)
					{					
						byte[] bytes1 = file.getBytes();
		
						File serverFile1 = new File(dir1.getAbsolutePath()+ File.separator + photoName);					
						BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
						stream1.write(bytes1);
						stream1.close();
					}
		
					isInsertSuccess =true;
				}
			}
			
			return true;
	    }*/
		
		
		public  String getFileExtension(String fileName) 
		{
	        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	        return fileName.substring(fileName.lastIndexOf(".")+1);
	        else return "";
		}
		
		@RequestMapping(value = "/saveServiceInquiry", method = RequestMethod.POST)
		public @ResponseBody boolean saveServiceInquiry(MultipartHttpServletRequest request) throws Exception
	    {  			
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			String ss=request.getParameter("formData");
			String ss1=request.getParameter("gridData");
			org.json.JSONArray gridData=new org.json.JSONArray(ss1);
			
			String gik=request.getParameter("gridImage");
			String gi=gik.replaceAll("\"", "");
			
			ServiceInquiryBean serviceInquiryBean = new ObjectMapper().readValue(ss, ServiceInquiryBean.class);
			
			ServiceInquiry serviceInquiry = prepareModelForServiceInquiry(serviceInquiryBean);	
			serviceInquiry.setImageData(gi);
			entityList.add(serviceInquiry);
			
			for(int j=0;j<gridData.length();j++)
			{
				ServiceInquiryDtlsBean serviceInquiryDtlsBean=new ObjectMapper().readValue(gridData.get(j).toString(), ServiceInquiryDtlsBean.class);
				ServiceInquiryDtls serviceInquiryDtls = prepareModelForServiceInquiryDtls(serviceInquiryDtlsBean,serviceInquiryBean.getCaseReferenceno());
				entityList.add(serviceInquiryDtls);
			}
			 
			if(serviceInquiryBean.getModifyFlag()==false)
			{
				String caserefno=serviceInquiryBean.getCaseReferenceno();
				String[] spl=caserefno.split("-");					
				String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");
				
				
				SequenceNumber sequenceNumber=new SequenceNumber();
				
				sequenceNumber.setProcessname(spl[0]);
				sequenceNumber.setCountrycode(st[0]);
				sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
				sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
				sequenceNumber.setTotalsequenceno(caserefno);
				
				entityList.add(sequenceNumber);
				
			}
			isInsertSuccess = productSelectionService.saveEntities(entityList);
			
			return isInsertSuccess;			
	    }
		
		 private ServiceInquiry prepareModelForServiceInquiry(ServiceInquiryBean serviceInquiryBean)
		 {
			 ServiceInquiry serviceInquiry = new ServiceInquiry();	
				
			 serviceInquiry.setAddress(serviceInquiryBean.getAddress());
			 serviceInquiry.setAttchdrawing(serviceInquiryBean.getAttachDrawing());
			 serviceInquiry.setCasereferenceno(serviceInquiryBean.getCaseReferenceno());
			 serviceInquiry.setContactno(serviceInquiryBean.getContactno());
			 serviceInquiry.setCustomer(serviceInquiryBean.getCustomer());
			 serviceInquiry.setTelephoneno(serviceInquiryBean.getTelephoneno());
			 serviceInquiry.setCustomercode(serviceInquiryBean.getCustomerCode());
			 serviceInquiry.setCustomerstatus(serviceInquiryBean.getCustomerStatus());
			 serviceInquiry.setDescription(serviceInquiryBean.getDescription());
			 serviceInquiry.setPersoncontactno(serviceInquiryBean.getPersonContactno());
			 serviceInquiry.setRemarks(serviceInquiryBean.getCustomerRemarks());
			 serviceInquiry.setServicelocation(serviceInquiryBean.getServiceLocation());
			 serviceInquiry.setSiteaddress(serviceInquiryBean.getSiteaddress());
			 serviceInquiry.setTelephoneno(serviceInquiryBean.getTelephoneno());
			 serviceInquiry.setInquiryDate(DateUtils.getSqlDateFromString(serviceInquiryBean.getInquiryDate(),Constants.GenericDateFormat.DATE_FORMAT));
			 return serviceInquiry;
		}
		 
		 
		 private ServiceInquiryDtls prepareModelForServiceInquiryDtls(ServiceInquiryDtlsBean serviceInquiryDtlsBean,String caseRefNo)
		 {				
			 ServiceInquiryDtls serviceInquiryDtls = new ServiceInquiryDtls();			 
			 serviceInquiryDtls.setCasereferenceno(caseRefNo);
			 serviceInquiryDtls.setApplication(serviceInquiryDtlsBean.getApplication());
			 serviceInquiryDtls.setAccessories(serviceInquiryDtlsBean.getAccessories());
			 serviceInquiryDtls.setQuantity(serviceInquiryDtlsBean.getQuantity());
			 serviceInquiryDtls.setId(serviceInquiryDtlsBean.getId());
			 serviceInquiryDtls.setIsselectedmodel(serviceInquiryDtlsBean.getIsModelUnkonown());
			 serviceInquiryDtls.setModelunknown(serviceInquiryDtlsBean.getModelUnkonown());
			 serviceInquiryDtls.setMotorserialno(serviceInquiryDtlsBean.getMotorslno());
			 serviceInquiryDtls.setRotationdirection(serviceInquiryDtlsBean.getRotationDirection());
			 serviceInquiryDtls.setServiceinquiryfor(serviceInquiryDtlsBean.getServiceInquiryFor());
			 serviceInquiryDtls.setShimfgno(serviceInquiryDtlsBean.getShimfgno());
			 serviceInquiryDtls.setBrake(serviceInquiryDtlsBean.getBrake());
			 serviceInquiryDtls.setBrakevoltage(serviceInquiryDtlsBean.getBrakeVoltage());
			 serviceInquiryDtls.setGearheadserialno(serviceInquiryDtlsBean.getGearHeadslno());
			 serviceInquiryDtls.setIsmodelunknown(serviceInquiryDtlsBean.getIsModelUnkonown());
			 serviceInquiryDtls.setSelectedmodel(serviceInquiryDtlsBean.getSelectedModel());
			 serviceInquiryDtls.setSlno(serviceInquiryDtlsBean.getSlno());
			 return serviceInquiryDtls;
		}
		 
		 
		 @RequestMapping(value = "/getAllServiceInquiry", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
		 public @ResponseBody String getAllServiceInquiry(@RequestBody String jsonData) throws Exception 
		 {			 
			 
			 org.json.JSONObject response = new org.json.JSONObject();
			 
			//ServiceInquiryBean serviceInquiryBean=prepareListofServiceInquiryBean((ServiceInquiry)productSelectionService.getEntityByIdVarchar(jsonData, ServiceInquiry.class),(ServiceInquiryDtls)productSelectionService.getEntityByIdVarchar(jsonData, ServiceInquiryDtls.class));
			 
			ServiceInquiryBean serviceInquiryBean=prepareListofServiceInquiryBean((ServiceInquiry)productSelectionService.getEntityByIdVarchar(jsonData, ServiceInquiry.class));
			List<ServiceInquiryDtlsBean> serviceInquiryDtlsBean=prepareListofServiceInquiryDtlsBean(productSelectionService.listServiceInquiryDtls(serviceInquiryBean.getCaseReferenceno()));
			
			org.json.JSONObject serviceInquiryJson=new org.json.JSONObject(serviceInquiryBean);
			org.json.JSONArray serviceInquiryDtlsArray = new org.json.JSONArray(serviceInquiryDtlsBean);
			
			response.put("serviceInquiry",serviceInquiryJson);
			response.put("serviceInquiryDtls",serviceInquiryDtlsArray);		
			return response.toString();
		 }
		 
		 
		 private ServiceInquiryBean prepareListofServiceInquiryBean(ServiceInquiry serviceInquiry)
		 {
			ServiceInquiryBean bean = new ServiceInquiryBean();
			
			bean.setCaseReferenceno(serviceInquiry.getCasereferenceno());
			bean.setContactno(serviceInquiry.getContactno());
			bean.setCustomer(serviceInquiry.getCustomer());
			bean.setCustomerCode(serviceInquiry.getCustomercode());
			bean.setImageData(serviceInquiry.getImageData());
			bean.setAddress(serviceInquiry.getAddress());
			bean.setAttachDrawing(serviceInquiry.getAttchdrawing());
			bean.setCustomerRemarks(serviceInquiry.getRemarks());
			bean.setCustomerStatus(serviceInquiry.getCustomerstatus());
			bean.setDescription(serviceInquiry.getDescription());
			bean.setServiceLocation(serviceInquiry.getServicelocation());
			bean.setPersonContactno(serviceInquiry.getPersoncontactno());
			bean.setSiteaddress(serviceInquiry.getSiteaddress());
			bean.setTelephoneno(serviceInquiry.getTelephoneno());
			bean.setInquiryDate(DateUtils.formatDate(serviceInquiry.getInquiryDate(),Constants.GenericDateFormat.DATE_FORMAT));
			
			
			return bean;
		 }
		 
		 
		private List<ServiceInquiryDtlsBean> prepareListofServiceInquiryDtlsBean(List<ServiceInquiryDtls> serviceInquiryDtls) 
		{
			List<ServiceInquiryDtlsBean> beans = null;
				
			if (serviceInquiryDtls != null && !serviceInquiryDtls.isEmpty()) 
			{  
				beans = new ArrayList<ServiceInquiryDtlsBean>();
				ServiceInquiryDtlsBean bean = null;
				for (ServiceInquiryDtls serviceInquiryDtl : serviceInquiryDtls) 
				{
					bean = new ServiceInquiryDtlsBean();
					
					bean.setId(serviceInquiryDtl.getId());
					bean.setQuantity(serviceInquiryDtl.getQuantity());
					bean.setAccessories(serviceInquiryDtl.getAccessories());
					bean.setApplication(serviceInquiryDtl.getApplication());
					bean.setBrake(serviceInquiryDtl.getBrake());
					bean.setBrakeVoltage(serviceInquiryDtl.getBrakevoltage());
					bean.setIsModelUnkonown(serviceInquiryDtl.getIsmodelunknown());
					bean.setIsSelectedModel(serviceInquiryDtl.getIsselectedmodel());
					bean.setModelUnkonown(serviceInquiryDtl.getModelunknown());
					bean.setMotorslno(serviceInquiryDtl.getMotorserialno());
					bean.setShimfgno(serviceInquiryDtl.getShimfgno());
					bean.setServiceInquiryFor(serviceInquiryDtl.getServiceinquiryfor());
					bean.setRotationDirection(serviceInquiryDtl.getRotationdirection());
					bean.setGearHeadslno(serviceInquiryDtl.getGearheadserialno());
					bean.setSelectedModel(serviceInquiryDtl.getSelectedmodel());
					bean.setSlno(serviceInquiryDtl.getSlno());
					beans.add(bean);						
				}
			}						
			return beans;
		}
	 
		 
		@RequestMapping(value = "/loadServiceInquiry", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody String loadServiceInquiry(@RequestBody String jsonData) throws Exception 
		{
			org.json.JSONObject response = new org.json.JSONObject();
			org.json.JSONArray qaIssuesArray =new org.json.JSONArray("[]");;
			
			List<ServiceInquiryBean> serviceInquiryBeans =prepareBeanForQAIssue(productSelectionService.loadServiceInquiryByCustomerName(jsonData));

			qaIssuesArray = new org.json.JSONArray(serviceInquiryBeans);
			response.put("serviceInquiry",qaIssuesArray);		
						
			return response.toString();
		}
	 
	 

		private List<ServiceInquiryBean> prepareBeanForQAIssue(List<ServiceInquiry> sinqs) 
		{
				List<ServiceInquiryBean> beans = null;			 			
				if (sinqs != null && !sinqs.isEmpty()) 
				{  
					beans = new ArrayList<ServiceInquiryBean>();
					ServiceInquiryBean bean = null;
					for (ServiceInquiry serviceInquiry : sinqs) 
					{
						bean = new ServiceInquiryBean();				
						
						bean.setCaseReferenceno(serviceInquiry.getCasereferenceno());								
						bean.setCustomer(serviceInquiry.getCustomer());						
						bean.setInquiryDate(DateUtils.formatDate(serviceInquiry.getInquiryDate(),Constants.GenericDateFormat.DATE_FORMAT));
						
						beans.add(bean);
						
					}
				}						
				return beans;
		}
		
		@RequestMapping(value = "/getCustomerDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getCustomerDetails(@RequestBody String jsonData) throws Exception
		{
			org.json.JSONObject response = new org.json.JSONObject();
			
			CustmerDetails custmerDetails = (CustmerDetails) hibernateDao.getRowByColumn(CustmerDetails.class, "accountid", jsonData);
			
			response.put("customerCode", custmerDetails.getAccount());
			response.put("telephoneno", custmerDetails.getContName());
			response.put("contactno", custmerDetails.getPhnumber());
			response.put("address", custmerDetails.getShippingadress());
			
			return response.toString();
		}
		 
		//-------------------------------------------- Service Inquiry  End------ -----------------------------------------------------------//		 
		 
		 //---------------------------------- QA Issue (Internal )  Start---------------------------------------------------------------//
		 
		 @RequestMapping(value = "/saveQAIssue", method = RequestMethod.POST)
		 public @ResponseBody boolean saveQAIssue(MultipartHttpServletRequest request) throws Exception
		 {			 
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			String ss=request.getParameter("formData");
			String ss1=request.getParameter("gridData");
			org.json.JSONArray gridData=new org.json.JSONArray(ss1);
			String gik=request.getParameter("gridImage");
			String gi=gik.replaceAll("\"", "");
			
			QAIssueBean qaissueBean = new ObjectMapper().readValue(ss, QAIssueBean.class);
			
			QAIssue qaissue = prepareModelForQAIssue(qaissueBean);	
			qaissue.setImageData(gi);
			entityList.add(qaissue);
			
			for(int j=0;j<gridData.length();j++)
			{
				QAIssueDetailsBean qaissueDetailsBean=new ObjectMapper().readValue(gridData.get(j).toString(), QAIssueDetailsBean.class);
				QAIssueDetails qaIssueDetails = prepareModelForprepareModelForQAIssueDtls(qaissueDetailsBean,qaissueBean.getCaseReferenceNumber());
				entityList.add(qaIssueDetails);
			}
				
			if(qaissueBean.getModifyFlag()==false)
			{
				String caserefno=qaissueBean.getCaseReferenceNumber();
				String[] spl=caserefno.split("-");					
				String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");				
				
				SequenceNumber sequenceNumber=new SequenceNumber();
				
				sequenceNumber.setProcessname(spl[0]);
				sequenceNumber.setCountrycode(st[0]);
				sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
				sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
				sequenceNumber.setTotalsequenceno(caserefno);
				
				entityList.add(sequenceNumber);
				
			}
			
			isInsertSuccess = productSelectionService.saveEntities(entityList);

			return true;
		  }
			
			 private QAIssue prepareModelForQAIssue(QAIssueBean qaIssueBean)
			 {
				QAIssue qaissue = new QAIssue();	
				 
				String[] accArray=qaIssueBean.getActionRequired();
				String acc="";
				for(int i=0;i<accArray.length;i++)
				{
					acc=acc+accArray[i]+",";					
				}
					
				String[] qaccArray=qaIssueBean.getQaIssuesFor();
				String qacc="";
				for(int i=0;i<qaccArray.length;i++)
				{
					qacc=qacc+qaccArray[i]+",";					
				}
					
				 qaissue.setCasereferenceno(qaIssueBean.getCaseReferenceNumber());
				 qaissue.setAttachdrawing(qaIssueBean.getAttachDrawing());
				 qaissue.setDescription(qaIssueBean.getDescription());
				 qaissue.setPossiblereasonsremarks(qaIssueBean.getPossibleReasonsRemarks());
				 qaissue.setScaclaimno(qaIssueBean.getScaClaimno());
				 qaissue.setSerialno(qaIssueBean.getSerialno());
				 qaissue.setShimfgno(qaIssueBean.getShimfgno());
				 qaissue.setActionrequired(acc);
				 qaissue.setQaissuesfor(qacc);
				 qaissue.setScadate(DateUtils.getSqlDateFromString(qaIssueBean.getScaDate(),Constants.GenericDateFormat.DATE_FORMAT));
				 
				 return qaissue;
			}
			 
			 private QAIssueDetails prepareModelForprepareModelForQAIssueDtls(QAIssueDetailsBean qaIssueDetailsBean,String caseRefNum)
			 {
				QAIssueDetails qaissueDetails = new QAIssueDetails();
				 
				qaissueDetails.setId(qaIssueDetailsBean.getId());
				qaissueDetails.setInpackageno(qaIssueDetailsBean.getInPackageno());
				qaissueDetails.setItemdescription(qaIssueDetailsBean.getItemDescription());
				qaissueDetails.setPartno(qaIssueDetailsBean.getPartno());
				qaissueDetails.setQty(qaIssueDetailsBean.getQty());
				qaissueDetails.setSlno(qaIssueDetailsBean.getSlno());
				qaissueDetails.setOrderno(qaIssueDetailsBean.getOrderno());
				qaissueDetails.setToclaim(qaIssueDetailsBean.getToClaim());
				qaissueDetails.setCasereferenceno(caseRefNum);
				return qaissueDetails;
				 
			}
			 
			 @RequestMapping(value = "/getAllQAIssue", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
			 public @ResponseBody String getAllQAIssue(@RequestBody String jsonData) throws Exception 
			 {			 
				 org.json.JSONObject response = new org.json.JSONObject();
				// QAIssueBean qaissueBeanBean=prepareListofgetAllQAIssueBean((QAIssue)productSelectionService.getEntityByIdVarchar(jsonData, QAIssue.class),(QAIssueDetails)productSelectionService.getEntityByIdVarchar(jsonData, QAIssueDetails.class));
				 
			 	QAIssueBean qaissueBean=prepareListofgetAllQAIssueBean((QAIssue)productSelectionService.getEntityByIdVarchar(jsonData, QAIssue.class));
				
			 	List<QAIssueDetailsBean> qaissueDtlsBean=prepareListofQAIssueDtlsBean(productSelectionService.listQAIssueDetails(qaissueBean.getCaseReferenceNumber()));
				
				org.json.JSONObject qaissueJson=new org.json.JSONObject(qaissueBean);
				org.json.JSONArray qaissueDtlsArray = new org.json.JSONArray(qaissueDtlsBean);
				
				response.put("qaissue",qaissueJson);
				response.put("qaissuedtls",qaissueDtlsArray);		
				return response.toString();
				 
			 }
			 
			 private QAIssueBean prepareListofgetAllQAIssueBean(QAIssue qaissue)
			 {				 
				QAIssueBean bean = new QAIssueBean();
				
				String[] s1=qaissue.getActionrequired().split(",");
				String[] s2=qaissue.getQaissuesfor().split(",");
				
				bean.setActionRequired(s1);
				bean.setQaIssuesFor(s2);			 
				bean.setCaseReferenceNumber(qaissue.getCasereferenceno());
				bean.setScaDate(DateUtils.formatDate(qaissue.getScadate(),Constants.GenericDateFormat.DATE_FORMAT));
				bean.setScaClaimno(qaissue.getScaclaimno());
				bean.setImageData(qaissue.getImageData());
				bean.setSerialno(qaissue.getSerialno());
				bean.setShimfgno(qaissue.getShimfgno());
				bean.setDescription(qaissue.getDescription());
				bean.setPossibleReasonsRemarks(qaissue.getPossiblereasonsremarks());
				bean.setAttachDrawing(qaissue.getAttachdrawing());
				return bean;
			 }
			 
			private List<QAIssueDetailsBean> prepareListofQAIssueDtlsBean(List<QAIssueDetails> qaissueDetails) 
			{
				List<QAIssueDetailsBean> beans = null;
					
				if (qaissueDetails != null && !qaissueDetails.isEmpty()) 
				{  
					beans = new ArrayList<QAIssueDetailsBean>();
					QAIssueDetailsBean bean = null;
					for (QAIssueDetails qaissueDetail : qaissueDetails) 
					{
						bean = new QAIssueDetailsBean();
						
						bean.setId(qaissueDetail.getId());
						bean.setInPackageno(qaissueDetail.getInpackageno());
						bean.setItemDescription(qaissueDetail.getItemdescription());
						bean.setPartno(qaissueDetail.getPartno());
						bean.setQty(qaissueDetail.getQty());
						bean.setOrderno(qaissueDetail.getOrderno());
						bean.setToClaim(qaissueDetail.getToclaim());
						bean.setSlno(qaissueDetail.getSlno());
						beans.add(bean);						
					}
				}						
				return beans;
			}
			 
			@RequestMapping(value = "/loadQAIssues", method = RequestMethod.POST, headers = { "Content-type=application/json" })
			public @ResponseBody String loadQAIssues(@RequestBody JSONObject jsonData) throws Exception 
			{
				org.json.JSONObject response = new org.json.JSONObject();
				org.json.JSONArray qaIssuesArray =new org.json.JSONArray("[]");;
				String fromdate=(String)jsonData.get("fromdate");
				String todate=(String)jsonData.get("todate");
				
				List<QAIssueBean> qaIssueBeans =prepareBeanForQAIssues(productSelectionService.loadQAIssuesByCustomerName(fromdate,todate));
				qaIssuesArray = new org.json.JSONArray(qaIssueBeans);
				response.put("qaInt",qaIssuesArray);						
				
				return response.toString();
			}
			 
			 

			private List<QAIssueBean> prepareBeanForQAIssues(List<QAIssue> qaIssues) 
			{
					List<QAIssueBean> beans = null;			 			
					if (qaIssues != null && !qaIssues.isEmpty()) 
					{  
						beans = new ArrayList<QAIssueBean>();
						QAIssueBean bean = null;
						for (QAIssue qaIssue : qaIssues) 
						{
							bean = new QAIssueBean();							
							//QAIssue qaIssue=(QAIssue)productSelectionService.getEntityByIdVarchar(qaIssuedtl.getCasereferenceno(), QAIssue.class);
							bean.setCaseReferenceNumber(qaIssue.getCasereferenceno());								
							bean.setSerialno(qaIssue.getSerialno());
							bean.setDescription(qaIssue.getDescription());
							bean.setScaDate(DateUtils.formatDate(qaIssue.getScadate(),Constants.GenericDateFormat.DATE_FORMAT));
							beans.add(bean);
							
						}
					}						
					return beans;
			}
		 
	
}
