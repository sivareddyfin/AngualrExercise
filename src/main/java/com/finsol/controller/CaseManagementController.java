package com.finsol.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.finsol.bean.DescriptionCaseBean;
import com.finsol.bean.MianCaseFormBean;
import com.finsol.bean.NewSelectionGridFormBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.ApplicationIndustryCode;
import com.finsol.model.CaseSummary;
import com.finsol.model.CaseSummaryTemp;
import com.finsol.model.CompititorGrid;
import com.finsol.model.CompititorGridTemp;
import com.finsol.model.CustmerContactDetails;
import com.finsol.model.CustmerDetails;
import com.finsol.model.Department;
import com.finsol.model.Description;
import com.finsol.model.ExplositionClassfication;
import com.finsol.model.GearProductType;
import com.finsol.model.MTR_KW;
import com.finsol.model.NewSelectionGrid;
import com.finsol.model.NewSelectionGridTemp;
import com.finsol.model.RPM;
import com.finsol.model.SequenceNumber;
import com.finsol.model.SparePartGrid;
import com.finsol.model.SparePartGridTemp;
import com.finsol.model.SumitomoReplaceGrid;
import com.finsol.model.SumitomoReplaceTempGrid;
import com.finsol.model.TaxMaster;
import com.finsol.model.TechGrid;
import com.finsol.model.TechGridTemp;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.BSS_SalesManagementService;
import com.finsol.service.Bss_Migrate_ServiceImpl;
import com.finsol.service.MasterScreen_serviceImpl;
import com.finsol.service.ProductSelectionService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

@Controller
public class CaseManagementController {

	private static final Logger logger = Logger.getLogger(CaseManagementController.class);

	@Autowired
	private BSS_CommonService bsscommonService;

	@Autowired
	private MasterScreen_serviceImpl masterservice;
	
	@Autowired
	private Bss_Migrate_ServiceImpl migratesevice;

	@Autowired
	private BSS_SalesManagementService salesManagementService;

	@Autowired
	private HibernateDao hibernateDao;
	
	@Autowired
	private ProductSelectionService productSelectionService;
	
	
/*
	@Autowired
	private ServletContext servletContext;
*/
	/*-------------------------------------- Common Methods ----------------------------------------------*/

	@RequestMapping(value = "/newSelectionFormData", method = RequestMethod.POST)
	public @ResponseBody boolean NewselctionCase(MultipartHttpServletRequest req)
			throws Exception {
		
		HttpSession session=req.getSession(false);
		String countryCode=(String)session.getAttribute("countrycode");

		
		String ss=req.getParameter("formData");
		String description1=req.getParameter("fieldData");
		String ss1=req.getParameter("gridData");
		org.json.JSONArray gData=new org.json.JSONArray(ss1);
	    //List<MultipartFile> mpf=req.getFiles("motorFile");

		String imagepath1=null;
		  String gik=req.getParameter("motorFile");
		   if(gik!=null)
		   {
		    imagepath1=gik.replaceAll("\"", "");
		   	    
		   }
	    
	    //String imagepath1=this.getImagepathOfNewSelectionTemp(mpf);

		int status = 0;
	 
		int tab=1;
		TreeMap<Integer, String> al = null;
		DescriptionCaseBean description = null;
		HttpSession hs = req.getSession();
		if (hs.getAttribute("image") != null) {
			al = (TreeMap<Integer, String>) hs.getAttribute("image");
		}
		

		String user=(String)hs.getAttribute("user");

		/*JSONObject caseform = (JSONObject) jsonData.get("formData");
		JSONArray casegrid = (JSONArray) jsonData.get("gridData");
		JSONObject casedescription = (JSONObject) jsonData.get("fieldData");*/

		MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);
		
		
		
		
		
		
		
		
		
		if (!description1.equalsIgnoreCase("undefined")) {
			status = 1;
			description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
		}
		String k = null;
		ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
		for (int i = 0; i < gData.length(); i++) {

			NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
					NewSelectionGridFormBean.class);

			String a[] = newcasegrid.getSscCode();
			if (a != null) {
				for (int m = 0; m < a.length; m++) {
					if (m == 0) {
						k = a[m];
					}

					if (m > 0) {

						k = k + ',' + a[m];
					}

				}

			}

			if (al != null) {
				String imagepath = (String) al.get(i + 1);
				newcasegrid.setImagepath(imagepath);
			}
			newcasegrid.setSsCode(k);

			newselctiongrid.add(newcasegrid);
		}

 
		
	 
		
		if(maincase.getMainid()!=null)
		{
			
			CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
			newselection.setCasetype("NEW CASE SELECTION");
			newselection.setTab(1);
			newselection.setNewselectionid(maincase.getMainid());
            masterservice.saveNewSelectionCase(newselection);
             
            masterservice.deleteNecCaseSeletionGrid(newselection.getCaseRef());
			List<NewSelectionGrid> newselectiongrid = this.insertDataIntoNewSelectionGridModel(newselctiongrid,newselection.getCaseRef());
			masterservice.saveNewSelectionGrid(newselectiongrid);

			
		}
		
		
		if(maincase.getMainid()==null)
		{
			
			CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
			newselection.setCasetype("NEW CASE SELECTION");
			newselection.setTab(1);
			
			
			String caserefernce=newselection.getCaseRef();
			
			int caserefrencecount=masterservice.getCaseRefernce(caserefernce);
			
			
			ArrayList sequencenumberlist=new ArrayList();
			if(caserefrencecount==0)
			{
				
				String[] spl=caserefernce.split("-");	
				String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");


				SequenceNumber sequenceNumber=new SequenceNumber();

				sequenceNumber.setProcessname(spl[0]);
				sequenceNumber.setCountrycode(st[0]);
				sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
				sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
				sequenceNumber.setTotalsequenceno(caserefernce);
				
				masterservice.saveNewSelectionCase(newselection);
				List<NewSelectionGrid> newselectiongrid = this.insertDataIntoNewSelectionGridModel(newselctiongrid,newselection.getCaseRef());
				masterservice.saveNewSelectionGrid(newselectiongrid);
				
				sequencenumberlist.add(sequenceNumber);
				productSelectionService.saveEntities(sequencenumberlist);
				
			}
			
			
			
			
			if(caserefrencecount>0)
			
			{
				 HttpSession session2=req.getSession();
				 String countryCode1=(String)session2.getAttribute("countrycode");
				 String usernasme=(String)session.getAttribute("user");

				String caseref=bsscommonService.getSequenceNumber("E",countryCode1,usernasme);
				
				newselection.setCaseRef(caseref);
				masterservice.saveNewSelectionCase(newselection);
				List<NewSelectionGrid> newselectiongrid = this.insertDataIntoNewSelectionGridModel(newselctiongrid,newselection.getCaseRef());
				
				masterservice.saveNewSelectionGrid(newselectiongrid);
				
				
			
				String[] spl=caseref.split("-");	
				String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");


				SequenceNumber sequenceNumber=new SequenceNumber();

				sequenceNumber.setProcessname(spl[0]);
				sequenceNumber.setCountrycode(st[0]);
				sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
				sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
				sequenceNumber.setTotalsequenceno(caseref);

				
				sequencenumberlist.add(sequenceNumber);
				
				productSelectionService.saveEntities(sequencenumberlist);
				
			}
			
			
			if(maincase.getTempid()!=null)
			{
				
				masterservice.deleteFromTempTableTeable(maincase.getTempid(),tab);
			}
			
		}
		
		

	 
		hs.removeAttribute("image");

		return true;

	}

	private List<NewSelectionGrid> insertDataIntoNewSelectionGridModel(
			ArrayList<NewSelectionGridFormBean> newselctiongrid,String caseref) {

		String newselectionkey = masterservice.getNewSelectionKey();
		ArrayList<NewSelectionGrid> al = new ArrayList<NewSelectionGrid>();
		for (NewSelectionGridFormBean ndgb : newselctiongrid) {
			NewSelectionGrid ng = new NewSelectionGrid();
			ng.setCaseref(caseref);
			ng.setToken(newselectionkey);
			
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setImagepath(ndgb.getImagepath());
			ng.setConf(ndgb.getConf());
			ng.setGm(ndgb.getGm());
			ng.setGearMotor(ndgb.getGearMotor());
			ng.setQuantity(ndgb.getQuantity());
			ng.setApplication(ndgb.getApplication());
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setGearProduct(ndgb.getGearProduct());
			ng.setModel(ndgb.getModel());
			ng.setSelectModel(ndgb.getSelectModel());
			ng.setServFactor(ndgb.getServFactor());
			ng.setSscCode(ndgb.getSsCode());
			ng.setMotorType(ndgb.getMotorType());
			ng.setMotorKW(ndgb.getMotorKW());
			ng.setPole(ndgb.getPole());
			ng.setPhase(ndgb.getPhase());
			ng.setVoltage(ndgb.getVoltage());
			ng.setHertz(ndgb.getHertz());
			ng.setRpm(ndgb.getRpm());
			ng.setProtectionGrade(ndgb.getProtectionGrade());
			ng.setInternationalStd(ndgb.getInternationalStd());
			ng.setAccessMotor(ndgb.getAccessMotor());
			ng.setMotorBrake(ndgb.getMotorBrake());
			ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
			ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
			ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
			ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
			ng.setDirAxial(ndgb.getDirAxial());
			ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
			ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
			ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
			ng.setDirSssAxial(ndgb.getDirSssAxial());
			ng.setRatioRequired(ndgb.getRatioRequired());
			ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
			ng.setPresetTorque(ndgb.getPresetTorque());
			ng.setMount(ndgb.getMount());
			ng.setMount2(ndgb.getMount2());
			ng.setMount3(ndgb.getMount3());
			ng.setIshaft(ndgb.getIshaft());
			ng.setOshaft(ndgb.getOshaft());
			ng.setOshaftInc(ndgb.getOshaftInc());
			ng.setDeg(ndgb.getDeg());
			ng.setTowards(ndgb.getTowards());
			ng.setOshafty(ndgb.getOshafty());
			ng.setOshafty2(ndgb.getOshafty2());
			ng.setBackStop(ndgb.getBackStop());
			ng.setRot(ndgb.getRot());
			ng.setDirHssAxial(ndgb.getDirHssAxial());
			ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
			ng.setInverter(ndgb.getInverter());
			ng.setTab(1);
			ng.setOshaftInc2(ndgb.getOshaftInc2());
			ng.setOshaftInc1(ndgb.getOshaftInc1());
			ng.setOther(ndgb.getOther());
			ng.setOthersPlz(ndgb.getOthersPlz());
			
			al.add(ng);

		}
		// TODO Auto-generated method stub
		return al;
	}

	private CaseSummary insetDataIntoNewSElctionCaseMOdel(MianCaseFormBean maincase, DescriptionCaseBean description,
			int status,String username) {

		CaseSummary newselection = new CaseSummary();

		newselection.setContactPerson(maincase.getContactPerson());
		/* newselection.setDescription(description.getDescription()); */
		newselection.setAddress(maincase.getAddress());
		newselection.setCaseRef(maincase.getCaseRef());
		newselection.setCustomer(maincase.getCustomer());
		newselection.setCustomerCode(maincase.getCustomerCode());
		newselection.setCustomerRef(maincase.getCustomerRef());
		/* newselection.setRemarks(description.getRemarks()); */
		newselection.setPhone(maincase.getPhone());
		newselection.setUsername(username);
		/* newselection.setRemarksAdd(description.getRemarksAdd()); */
		String dateexp = maincase.getDateExp();
		String dtcase = maincase.getDateCase();
		/*
		 * String dateReport=maincase.getDateRepot(); String dtreport
		 * =maincase.getDtReport();
		 */

		if (dateexp != null) {
			//String k1 = DateUtils.formatDate(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			Date dateexp1 = DateUtils.getSqlDateFromString(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			newselection.setDateExp(dateexp1);
		}

		if (dtcase != null) {

			//String k2 = DateUtils.formatDate(dtcase, Constants.GenericDateFormat.DATE_FORMAT);
			Date datecase = DateUtils.getSqlDateFromString(dtcase, Constants.GenericDateFormat.DATE_FORMAT);
			newselection.setDtCase(datecase);

		}

		if (status != 0) {
			
			newselection.setDescription(description.getDescription());
			if (description.getDtReport() != null)

			{
				//String k1 = DateUtils.formatDate(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
				Date dateexp1 = DateUtils.getSqlDateFromString(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDtReport(dateexp1);

			}

			if (description.getDateRepot() != null)

			{

				//String k1 = DateUtils.formatDate(description.getDateRepot(), Constants.GenericDateFormat.DATE_FORMAT);
				Date dateexp1 = DateUtils.getSqlDateFromString(description.getDateRepot(), Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDtReportBy(dateexp1);

			}

		}

		if (maincase.getCase1Id() == null) {
			int newsectioncaseid = masterservice.getNewSelctionCaseMaxId();
			newselection.setNewcaseToken("TOKEN" + newsectioncaseid);
		} else {
			newselection.setNewcaseToken(maincase.getToken());

		}

		return newselection;
	}

	
	private CaseSummary insetDataIntoNewSElctionCaseMOdelChanged(MianCaseFormBean maincase, DescriptionCaseBean description,
			int status,String username,String imgesinbean,String imagepath1) {

		CaseSummary newselection = new CaseSummary();

		newselection.setContactPerson(maincase.getContactPerson());
		/* newselection.setDescription(description.getDescription()); */
		newselection.setAddress(maincase.getAddress());
		newselection.setCaseRef(maincase.getCaseRef());
		newselection.setCustomer(maincase.getCustomer());
		newselection.setCustomerCode(maincase.getCustomerCode());
		newselection.setCustomerRef(maincase.getCustomerRef());
		/* newselection.setRemarks(description.getRemarks()); */
		newselection.setPhone(maincase.getPhone());
		newselection.setUsername(username);
		newselection.setCustStatus(maincase.getCustStatus());
		newselection.setAssigned(maincase.getAssigned());
		newselection.setCasePriority(maincase.getCasePriority());
		newselection.setOrginReq(maincase.getOrginReq());
		
		
		/* newselection.setRemarksAdd(description.getRemarksAdd()); */
		String  dateexp = maincase.getDateExp();
		String dtcase = maincase.getDateCase();
		/*
		 * String dateReport=maincase.getDateRepot(); String dtreport
		 * =maincase.getDtReport();
		 */
		if(imagepath1!=null)
		{
			newselection.setImages(imagepath1);
			
		}
		
		if(imagepath1==null)
		{
			newselection.setImages(imgesinbean);
			
		}
		 
		if (dateexp != null) {
			
			

			//String newAssyDate=productionSchedulerBean.getAssemblyDate();
			//String newCompleteDate=productionSchedulerBean.getCompletedDate();
			
			if(dateexp.indexOf("T")!=-1)
			{
				String[] assyDate=dateexp.split("T");
				String[] assyDate1=assyDate[0].split("-");
			    String	newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
				Date dateexp1 = DateUtils.getSqlDateFromString(newAssyDate, Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDateExp(dateexp1);
			}
			
			else
			{
				
				Date dateexp1 = DateUtils.getSqlDateFromString(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDateExp(dateexp1);
				
			}
			
			
			//String k1 = DateUtils.formatDate(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			/*Date dateexp1 = DateUtils.getSqlDateFromString(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			newselection.setDateExp(dateexp1);*/
		}

		if (dtcase != null) {
			
			
			if(dtcase.indexOf("T")!=-1)
			{
				String[] assyDate=dtcase.split("T");
				String[] assyDate1=assyDate[0].split("-");
			    String	newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
				//Date dateexp1 = DateUtils.getSqlDateFromString(newAssyDate, Constants.GenericDateFormat.DATE_FORMAT);
				DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDtCase(DateUtils.getSqlDateFromString(newAssyDate,Constants.GenericDateFormat.DATE_FORMAT));
			}

		else
		{
		Date datecase = DateUtils.getSqlDateFromString(dtcase, Constants.GenericDateFormat.DATE_FORMAT);
		newselection.setDtCase(datecase);
		}	 

		}

		if (status != 0) {
			
			newselection.setDescription(description.getDescription());
			if (description.getDtReport() != null)

			{
				
				

				if(description.getDtReport().indexOf("T")!=-1)
				{
					String[] assyDate=description.getDtReport().split("T");
					String[] assyDate1=assyDate[0].split("-");
				    String	newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
					Date dateexp1 = DateUtils.getSqlDateFromString(newAssyDate, Constants.GenericDateFormat.DATE_FORMAT);
					
					newselection.setDtReport(dateexp1);
				}
				
				else
				{
					Date dateexp1 = DateUtils.getSqlDateFromString(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
					newselection.setDtReport(dateexp1);

					
				}
				//String k1 = DateUtils.formatDate(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
				//Date dateexp1 = DateUtils.getSqlDateFromString(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
				//newselection.setDtReport(dateexp1);

			}

			if (description.getDateRepot() != null)

			{
				
				if(description.getDateRepot().indexOf("T")!=-1)
					
				{
					String[] assyDate=description.getDateRepot().split("T");
					String[] assyDate1=assyDate[0].split("-");
				    String	newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
					Date dateexp1 = DateUtils.getSqlDateFromString(newAssyDate, Constants.GenericDateFormat.DATE_FORMAT);
					
					
					newselection.setDtReportBy(dateexp1);
					
					
				}
				
			else
			{

				//String k1 = DateUtils.formatDate(description.getDateRepot(), Constants.GenericDateFormat.DATE_FORMAT);
				Date dateexp1 = DateUtils.getSqlDateFromString(description.getDateRepot(), Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDtReportBy(dateexp1);
				
			}

			}

		}

		if (maincase.getCase1Id() == null) {
			int newsectioncaseid = masterservice.getNewSelctionCaseMaxId();
			newselection.setNewcaseToken("TOKEN" + newsectioncaseid);
		} else {
			newselection.setNewcaseToken(maincase.getToken());

		}

		return newselection;
	}


	
	
	
	/*@RequestMapping(value = "/newcaseselectiongridimage", method = RequestMethod.POST)
	public @ResponseBody boolean saveUsers(MultipartHttpServletRequest request,HttpServletRequest req) throws Exception
	{

		int gridid = Integer.parseInt("1");
	
		String photoPath = null;
		Iterator<String> itr =  request.getFileNames();
		while(itr.hasNext())
		{
			MultipartFile mpf=request.getFile(itr.next());

			if (mpf.getSize() != 0) {
				Random t = new Random();
				int num = t.nextInt(10000);

				int maxid = masterservice.getMaxIdCustmer();
				String ext = getFileExtension(mpf.getOriginalFilename());
				String photoName = maxid + num + "_photo" + "." + ext;
				photoPath = "imagesnewcase/" + photoName;

				 masterservice.updateimageonMaxId(maxid,photoPath); 

				File dir1 = new File(servletContext.getRealPath("/") + "imagesnewcase/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (mpf.getSize() != 0) {
					byte[] bytes1 = mpf.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}
		}

		return true;
		
		}
	
*/	
	
	
	/*@RequestMapping(value = "/newcaseselectiongridimage", method = RequestMethod.POST)
	public @ResponseBody String  saveUsers(MultipartHttpServletRequest request,HttpServletRequest req) throws Exception
	{

		int gridid = Integer.parseInt("1");
		String imgepath=null;
		
	
		String photoPath = null;
		Iterator<String> itr =  request.getFileNames();
		
		ArrayList<String> al=new ArrayList<String>();
		while(itr.hasNext())
		{
			MultipartFile mpf=request.getFile(itr.next());

			if (mpf.getSize() != 0) {
				Random t = new Random();
				int num = t.nextInt(10000);

				int maxid = masterservice.getMaxIdCustmer();
				String ext = getFileExtension(mpf.getOriginalFilename());
				String photoName = maxid + num + "_photo" + "." + ext;
				
			
				photoPath = "imagesnewcase/" + photoName;
				al.add(photoPath);
				
			 

				 masterservice.updateimageonMaxId(maxid,photoPath); 

				File dir1 = new File(servletContext.getRealPath("/") + "imagesnewcase/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (mpf.getSize() != 0) {
					byte[] bytes1 = mpf.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}
		}

		 
		HttpSession hs=req.getSession();
		
		
		if(hs.getAttribute("imagenewcase")==null)
		{
			
			  
			hs.setAttribute("imagenewcase",al);
			int z=0;
			for(String a:al)
			{
				if(z==0)
				{
				imgepath=a;
				}
				else
				{
					imgepath=imgepath+","+a;	
				}
				
				z++;
			}
			
			
		}
		
		
		if(hs.getAttribute("imagenewcase")!=null)
		{
			
			ArrayList<String> kl=(ArrayList<String>)hs.getAttribute("imagenewcase");
			for(String kh:kl)
			{
				al.add(kh);
				
			}
			
			int z=0;
			for(String a:al)
			{
				if(z==0)
				{
				imgepath=a;
				}
				else
				{
					imgepath=imgepath+","+a;	
				}
				
				z++;
				
			}
		}
		
		return imgepath;
		
		}
	
*/
	
	@RequestMapping(value = "/newcaseselectiongridimage", method = RequestMethod.POST)
	public @ResponseBody String  saveUsers(MultipartHttpServletRequest request,HttpServletRequest req,ServletContext servletContext ) throws Exception
	{

		int gridid = Integer.parseInt("1");
		String imgepath=null;
		
	
		String photoPath = null;
		Iterator<String> itr =  request.getFileNames();
		
		ArrayList<String> al=new ArrayList<String>();
		while(itr.hasNext())
		{
			MultipartFile mpf=request.getFile(itr.next());

			if (mpf.getSize() != 0) {
				Random t = new Random();
				int num = t.nextInt(10000);

				int maxid = masterservice.getMaxIdCustmer();
				String ext = getFileExtension(mpf.getOriginalFilename());
				String photoName = maxid + num + "_photo" + "." + ext;
				
			
				photoPath = "imagesnewcase/" + photoName;
				al.add(photoPath);
				
			 

				/* masterservice.updateimageonMaxId(maxid,photoPath); */

				File dir1 = new File(servletContext.getRealPath("/") + "imagesnewcase/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (mpf.getSize() != 0) {
					byte[] bytes1 = mpf.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}
		}

		 
		HttpSession hs=req.getSession();
		 
		
		
		if(hs.getAttribute("imagenewcase")==null)
		{
			
			  
			hs.setAttribute("imagenewcase",al);
			int z=0;
			for(String a:al)
			{
				if(z==0)
				{
				imgepath=a;
				}
				else
				{
					imgepath=imgepath+","+a;	
				}
				
				z++;
			}
			
			
		}
		
		
		if(hs.getAttribute("imagenewcase")!=null)
		{
			
			ArrayList<String> kl=(ArrayList<String>)hs.getAttribute("imagenewcase");
			for(String kh:kl)
			{
				al.add(kh);
				
			}
			
			int z=0;
			for(String a:al)
			{
				if(z==0)
				{
				imgepath=a;
				}
				else
				{
					imgepath=imgepath+","+a;	
				}
				
				z++;
				
			}
		}
		
		return imgepath;
		
		}
	

	
	
	/*@RequestMapping(value = "/newcaseselectiongridimage", method = RequestMethod.POST)
	public @ResponseBody boolean saveUsers() 
 throws Exception {

	 
		
   
		

		 

		return true;
	}


	*/
	
	/*@RequestMapping(value = "/newcaseselectiongridimage", method = RequestMethod.POST)
	public @ResponseBody boolean saveUsers(@RequestBody JSONObject jarray) throws Exception {

		
		for(int i=0;i<jarray.size();i++)
		{
			
			JSONObject jsonObject=(JSONObject)jarray.get(i);
			
			MultipartHttpServletRequest mr=(MultipartHttpServletRequest)jsonObject.get("fd");
			String id=(String)jsonObject.getString("id");
			
			
		}
		
		
		int gridid = Integer.parseInt("1");
		String ids=req.getParameter("dd");
		String photoPath = null;
		Iterator<String> itr =  request.getFileNames();
		while(itr.hasNext())
		{
			MultipartFile mpf=request.getFile(itr.next());

			if (mpf.getSize() != 0) {
				Random t = new Random();
				int num = t.nextInt(10000);

				int maxid = masterservice.getMaxIdCustmer();
				String ext = getFileExtension(mpf.getOriginalFilename());
				String photoName = maxid + num + "_photo" + "." + ext;
				photoPath = "images/" + photoName;

				 masterservice.updateimageonMaxId(maxid,photoPath); 

				File dir1 = new File(servletContext.getRealPath("/") + "imagesnewcase/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (mpf.getSize() != 0) {
					byte[] bytes1 = mpf.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}

		
		}
		

		HttpSession imagesesssion = req.getSession();

		if (imagesesssion.getAttribute("image") == null) {
			TreeMap<Integer, String> al = new TreeMap<Integer, String>();
			al.put(gridid, photoPath);

			imagesesssion.setAttribute("image", al);
		}

		if (imagesesssion.getAttribute("image") != null) {
			TreeMap<Integer, String> al = (TreeMap<Integer, String>) imagesesssion.getAttribute("image");
			al.put(gridid, photoPath);
			imagesesssion.setAttribute("image", al);

		}

		return true;
	}

*/	public String getFileExtension(String fileName) {
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	/******************************************
	 * storing in temp
	 ********************************************************************/

	/*@RequestMapping(value = "/newSelectionFormDataTemp", method = RequestMethod.POST, headers = {
			"Content-type=application/json" })
	public @ResponseBody boolean NewselctionCaseTemp(@RequestBody JSONObject jsonData, HttpServletRequest req)
			throws Exception {

		
			
			
			

			int status = 0;
			DescriptionCaseBean description = null;
			TreeMap<Integer, String> al = null;

			HttpSession hs = req.getSession();
			String user=(String)hs.getAttribute("user");
			int tab=1;
			
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			int hour = now.get(Calendar.HOUR_OF_DAY);
			int minute = now.get(Calendar.MINUTE);
			int second = now.get(Calendar.SECOND);
			int millis = now.get(Calendar.MILLISECOND);
		    String storedtime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
			
			if (hs.getAttribute("image") != null) {
				al = (TreeMap<Integer, String>) hs.getAttribute("image");
			}

			JSONObject caseform = (JSONObject) jsonData.get("formData");
			JSONArray casegrid = (JSONArray) jsonData.get("gridData");
			JSONObject casedescription = (JSONObject) jsonData.get("fieldData");

			MianCaseFormBean maincase = new ObjectMapper().readValue(caseform.toString(), MianCaseFormBean.class);
			if (casedescription != null) {
				status = 1;
				description = new ObjectMapper().readValue(casedescription.toString(), DescriptionCaseBean.class);
			}

			String k = null;
			ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
			for (int i = 0; i < casegrid.size(); i++) {

				NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(casegrid.get(i).toString(),
						NewSelectionGridFormBean.class);

				String a[] = newcasegrid.getSscCode();
				if (a != null) {
					for (int m = 0; m < a.length; m++) {
						if (m == 0) {
							k = a[m];
						}

						if (m > 0) {

							k = k + ',' + a[m];
						}

					}

				}
				if (al != null) {
					String imagepath = (String) al.get(i + 1);
					newcasegrid.setImagepath(imagepath);
				}
				newcasegrid.setSsCode(k);

				newselctiongrid.add(newcasegrid);
			}

			CaseSummaryTemp newselection = this.insetDataIntoNewSElctionCaseMOdelTemp(maincase, description, status);
			newselection.setStoredtime(storedtime);
			newselection.setTab(tab);
			newselection.setUsername(user);
			newselection.setCasetype("New SELECTION TYPE");
			
			if(maincase.getTempid()!=null)
			{
				
				masterservice.deleteFromTempTableTeable(maincase.getTempid(),tab);
			}

			masterservice.saveNewSelectionCaseTemp(newselection);
			List<NewSelectionGridTemp> newselectiongrid = this.insertDataIntoNewSelectionGridModelTemp(newselctiongrid,storedtime,tab,user,newselection.getCaseRef());
			masterservice.saveNewSelectionGridTemp(newselectiongrid);

			hs.removeAttribute("image");

			return true;

		 

	}

*/	
 
	
	@RequestMapping(value = "/newSelectionFormDataTemp", method = RequestMethod.POST )
public @ResponseBody boolean NewselctionCaseTemp(MultipartHttpServletRequest req)
	throws Exception {


	
	String ss=req.getParameter("formData");
	String description1=req.getParameter("fieldData");
	String ss1=req.getParameter("gridData");
	org.json.JSONArray gData=new org.json.JSONArray(ss1);
   // List<MultipartFile> mpf=req.getFiles("motorFile");
    
   String imagepath1=null;
	  String gik=req.getParameter("motorFile");
	   if(gik!=null)
	   {
	    imagepath1=gik.replaceAll("\"", "");
	   	    
	   }
	  
	

	int status = 0;
	DescriptionCaseBean description = null;
	TreeMap<Integer, String> al = null;

	HttpSession hs = req.getSession();
	String user=(String)hs.getAttribute("user");
	int tab=1;
	
	Calendar now = Calendar.getInstance();
	int year = now.get(Calendar.YEAR);
	int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
	int day = now.get(Calendar.DAY_OF_MONTH);
	int hour = now.get(Calendar.HOUR_OF_DAY);
	int minute = now.get(Calendar.MINUTE);
	int second = now.get(Calendar.SECOND);
	int millis = now.get(Calendar.MILLISECOND);
    String storedtime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
	
	if (hs.getAttribute("image") != null) {
		al = (TreeMap<Integer, String>) hs.getAttribute("image");
	}
/*
	JSONObject caseform = (JSONObject) jsonData.get("formData");
	JSONArray casegrid = (JSONArray) jsonData.get("gridData");
	JSONObject casedescription = (JSONObject) jsonData.get("fieldData");
*/
	MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);
	
	if (!description1.equalsIgnoreCase("undefined")) {
		status = 1;
		description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
	}

	String k = null;
	ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
	for (int i = 0; i < gData.length(); i++) {

		NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
				NewSelectionGridFormBean.class);

		String a[] = newcasegrid.getSscCode();
		if (a != null) {
			for (int m = 0; m < a.length; m++) {
				if (m == 0) {
					k = a[m];
				}

				if (m > 0) {

					k = k + ',' + a[m];
				}

			}

		}
		if (al != null) {
			String imagepath = (String) al.get(i + 1);
			newcasegrid.setImagepath(imagepath);
		}
		newcasegrid.setSsCode(k);

		newselctiongrid.add(newcasegrid);
	}

	CaseSummaryTemp newselection = this.insetDataIntoNewSElctionCaseMOdelTempModified(maincase, description, status,imagepath1,maincase.getImages());
	newselection.setStoredtime(storedtime);
	newselection.setTab(tab);
	newselection.setUsername(user);
	/*if(imagepath1!=null)
	{
	newselection.setImages(imagepath1);
	}
	
	if(imagepath1==null)
	{
	newselection.setImages(maincase.getImages());
	}*/
	
	newselection.setCasetype("New SELECTION TYPE");
	
	if(maincase.getTempid()!=null)
	{
		
		masterservice.deleteFromTempTableTeable(maincase.getTempid(),tab);
	}

	masterservice.saveNewSelectionCaseTemp(newselection);
	List<NewSelectionGridTemp> newselectiongrid = this.insertDataIntoNewSelectionGridModelTemp(newselctiongrid,storedtime,tab,user,newselection.getCaseRef());
	masterservice.saveNewSelectionGridTemp(newselectiongrid);

	hs.removeAttribute("image");

	return true;

 


		
		
	}
	
	
	private String getImagepathOfCompititorTemp(List<MultipartFile> mpf,ServletContext servletContext) throws Exception
	{
		String photoName=null;
		String photoPath=null;
	 int i=0;
		for(MultipartFile mf:mpf)
		{
			
			String fname=mf.getOriginalFilename();
			//logger.info("--------------"+mf.getOriginalFilename());
			
			
			if (mf.getSize() != 0) 
			{
				Random t = new Random();
				int num = t.nextInt(10000);
				
				String exts = getFileExtension(mf.getOriginalFilename());
				photoName = i + num + "_photo" + "." + exts;
				if(i==0){
				photoPath = "compititorcase/" + photoName;
				}
				if(i!=0)
				{
					String photoPath1="compititorcase/" + photoName;
					photoPath = photoPath+","+photoPath1;
				}

				File dir1 = new File(servletContext.getRealPath("/") + "compititorcase/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (mf.getSize() != 0) 
				{
					byte[] bytes1 = mf.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}
			i++;
		}
	
		return photoPath;

	}



	
	private String getImagepathOfNewSelectionTemp(List<MultipartFile> mpf,ServletContext servletContext) throws Exception
	{
		String photoName=null;
		String photoPath=null;
	 int i=0;
		for(MultipartFile mf:mpf)
		{
			
			String fname=mf.getOriginalFilename();
			//logger.info("--------------"+mf.getOriginalFilename());
			
			
			if (mf.getSize() != 0) 
			{
				Random t = new Random();
				int num = t.nextInt(10000);
				
				String exts = getFileExtension(mf.getOriginalFilename());
				photoName = i + num + "_photo" + "." + exts;
				if(i==0){
				photoPath = "imagesnewcase/" + photoName;
				}
				if(i!=0)
				{
					String photoPath1="imagesnewcase/" + photoName;
					photoPath = photoPath+","+photoPath1;
				}

				File dir1 = new File(servletContext.getRealPath("/") + "imagesnewcase/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (mf.getSize() != 0) 
				{
					byte[] bytes1 = mf.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}
			i++;
		}
	
		return photoPath;

	}

	private List<NewSelectionGridTemp> insertDataIntoNewSelectionGridModelTemp(
			ArrayList<NewSelectionGridFormBean> newselctiongrid,String storedtime,int tab,String user,String caseref) {

		String newselectionkey = masterservice.getNewSelectionTempKey();
		ArrayList<NewSelectionGridTemp> al = new ArrayList<NewSelectionGridTemp>();
		for (NewSelectionGridFormBean ndgb : newselctiongrid) {
			NewSelectionGridTemp ng = new NewSelectionGridTemp();
			ng.setCaseref(caseref);
			ng.setToken(newselectionkey);
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setImagepath(ndgb.getImagepath());
			ng.setConf(ndgb.getConf());
			ng.setGm(ndgb.getGm());
			ng.setGearMotor(ndgb.getGearMotor());
			ng.setQuantity(ndgb.getQuantity());
			ng.setApplication(ndgb.getApplication());
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setGearProduct(ndgb.getGearProduct());
			ng.setModel(ndgb.getModel());
			ng.setSelectModel(ndgb.getSelectModel());
			ng.setServFactor(ndgb.getServFactor());
			ng.setSscCode(ndgb.getSsCode());
			ng.setMotorType(ndgb.getMotorType());
			ng.setMotorKW(ndgb.getMotorKW());
			ng.setPole(ndgb.getPole());
			ng.setPhase(ndgb.getPhase());
			ng.setVoltage(ndgb.getVoltage());
			ng.setHertz(ndgb.getHertz());
			ng.setRpm(ndgb.getRpm());
			ng.setProtectionGrade(ndgb.getProtectionGrade());
			ng.setInternationalStd(ndgb.getInternationalStd());
			ng.setAccessMotor(ndgb.getAccessMotor());
			ng.setMotorBrake(ndgb.getMotorBrake());
			ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
			ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
			ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
			ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
			ng.setDirAxial(ndgb.getDirAxial());
			ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
			ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
			ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
			ng.setDirSssAxial(ndgb.getDirSssAxial());
			ng.setRatioRequired(ndgb.getRatioRequired());
			ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
			ng.setPresetTorque(ndgb.getPresetTorque());
			ng.setMount(ndgb.getMount());
			ng.setMount2(ndgb.getMount2());
			ng.setMount3(ndgb.getMount3());
			ng.setIshaft(ndgb.getIshaft());
			ng.setOshaft(ndgb.getOshaft());
			ng.setOshaftInc(ndgb.getOshaftInc());
			ng.setDeg(ndgb.getDeg());
			ng.setTowards(ndgb.getTowards());
			ng.setOshafty(ndgb.getOshafty());
			ng.setOshafty2(ndgb.getOshafty2());
			ng.setBackStop(ndgb.getBackStop());
			ng.setRot(ndgb.getRot());
			ng.setDirHssAxial(ndgb.getDirHssAxial());
			ng.setStoredtime(storedtime);
			ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
			ng.setInverter(ndgb.getInverter());
			ng.setTab(tab);
			ng.setUsername(user);
			ng.setOshaftInc2(ndgb.getOshaftInc2());
			ng.setOshaftInc1(ndgb.getOshaftInc1());
			ng.setOther(ndgb.getOther());
			ng.setOthersPlz(ndgb.getOthersPlz());
			al.add(ng);

		}
		// TODO Auto-generated method stub
		return al;
	}

	private CaseSummaryTemp insetDataIntoNewSElctionCaseMOdelTemp(MianCaseFormBean maincase,
			DescriptionCaseBean description, int status) {

		CaseSummaryTemp newselection = new CaseSummaryTemp();

		newselection.setContactPerson(maincase.getContactPerson());
		/* newselection.setDescription(description.getDescription()); */
		newselection.setAddress(maincase.getAddress());
		newselection.setCaseRef(maincase.getCaseRef());
		newselection.setCustomer(maincase.getCustomer());
		newselection.setCustomerCode(maincase.getCustomerCode());
		newselection.setCustomerRef(maincase.getCustomerRef());
		/* newselection.setRemarks(description.getRemarks()); */
		newselection.setPhone(maincase.getPhone());
		/* newselection.setRemarksAdd(description.getRemarksAdd()); */
		String dateexp = maincase.getDateExp();
		String dtcase = maincase.getDateCase();

		if (dateexp != null) {
			//String k1 = DateUtils.formatDate(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			Date dateexp1 = DateUtils.getSqlDateFromString(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			newselection.setDateExp(dateexp1);
		}

		if (dtcase != null) {
			//String k2 = DateUtils.formatDate(dtcase, Constants.GenericDateFormat.DATE_FORMAT);
			Date dateReport1 = DateUtils.getSqlDateFromString(dtcase, Constants.GenericDateFormat.DATE_FORMAT);
			newselection.setDtCase(dateReport1);
		}

		if (status != 0) {
			newselection.setDescription(description.getDescription());
			if (description.getDtReport() != null)

			{
				//String k1 = DateUtils.formatDate(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
				Date dateexp1 = DateUtils.getSqlDateFromString(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDtReport(dateexp1);

			}

			if (description.getDateRepot()!= null)

			{

				//String k1 = DateUtils.formatDate(description.getDateRepot(), Constants.GenericDateFormat.DATE_FORMAT);
				Date dateexp1 = DateUtils.getSqlDateFromString(description.getDateRepot(), Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDtReportBy(dateexp1);

			}

		}

		if (maincase.getCase1Id() == null) {
			int newsectioncaseid = masterservice.getNewSelctionCaseTempMaxId();
			newselection.setNewcaseToken("TEMPTOKEN" + newsectioncaseid);
		} else {
			newselection.setNewcaseToken(maincase.getToken());

		}

		return newselection;
	}

	
	private CaseSummaryTemp insetDataIntoNewSElctionCaseMOdelTempModified(MianCaseFormBean maincase,
			DescriptionCaseBean description, int status,String imagepath1,String imageinbean) {

		CaseSummaryTemp newselection = new CaseSummaryTemp();

		newselection.setContactPerson(maincase.getContactPerson());
		/* newselection.setDescription(description.getDescription()); */
		newselection.setAddress(maincase.getAddress());
		newselection.setCaseRef(maincase.getCaseRef());
		newselection.setCustomer(maincase.getCustomer());
		newselection.setCustomerCode(maincase.getCustomerCode());
		newselection.setCustomerRef(maincase.getCustomerRef());
		/* newselection.setRemarks(description.getRemarks()); */
		newselection.setPhone(maincase.getPhone());
		
		newselection.setCustStatus(maincase.getCustStatus());
		newselection.setAssigned(maincase.getAssigned());
		/* newselection.setRemarksAdd(description.getRemarksAdd()); */
		String dateexp = maincase.getDateExp();
		String dtcase = maincase.getDateCase();
		newselection.setCasePriority(maincase.getCasePriority());
		newselection.setOrginReq(maincase.getOrginReq());
	
		if(imagepath1!=null)
		{
			
			newselection.setImages(imagepath1);
		}

		
		if(imagepath1==null)
		{
			
			newselection.setImages(imageinbean);
		}

		if (dateexp != null) {
			
			
			
			
			if(dateexp.indexOf("T")!=-1)
				
			{
				String[] assyDate=dateexp.split("T");
				String[] assyDate1=assyDate[0].split("-");
			    String	newAssyDate=(Integer.parseInt(assyDate1[2])+1)+"-"+assyDate1[1]+"-"+assyDate1[0];
				Date dateexp1 = DateUtils.getSqlDateFromString(newAssyDate, Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDateExp(dateexp1);
				
				//newselection.setDtReportBy(dateexp1);
				
				
			}
			
			else
			{
			//String k1 = DateUtils.formatDate(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			Date dateexp1 = DateUtils.getSqlDateFromString(dateexp, Constants.GenericDateFormat.DATE_FORMAT);
			newselection.setDateExp(dateexp1);
			
			}
		}

		if (dtcase != null) {
			
			
        if(dtcase.indexOf("T")!=-1)
				
			{
				String[] assyDate = dtcase.split("T");
				String[] assyDate1 = assyDate[0].split("-");
				String newAssyDate = (Integer.parseInt(assyDate1[2]) + 1) + "-" + assyDate1[1] + "-" + assyDate1[0];
				Date dateexp1 = DateUtils.getSqlDateFromString(newAssyDate, Constants.GenericDateFormat.DATE_FORMAT);

				newselection.setDtCase(dateexp1);

			}
			
			
		else
		{
			Date dateReport1 = DateUtils.getSqlDateFromString(dtcase, Constants.GenericDateFormat.DATE_FORMAT);
			newselection.setDtCase(dateReport1);
			
		}
		}

		if (status != 0) {
			newselection.setDescription(description.getDescription());
			if (description.getDtReport() != null)

			{
				
				if(description.getDtReport().indexOf("T")!=-1)
					
				{
					String[] assyDate = description.getDtReport().split("T");
					String[] assyDate1 = assyDate[0].split("-");
					String newAssyDate = (Integer.parseInt(assyDate1[2]) + 1) + "-" + assyDate1[1] + "-" + assyDate1[0];
					Date dateexp1 = DateUtils.getSqlDateFromString(newAssyDate, Constants.GenericDateFormat.DATE_FORMAT);
					newselection.setDtReport(dateexp1);

					//newselection.setDtCase(dateexp1);
					
					
				}
				
				else
				{
				//String k1 = DateUtils.formatDate(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
				Date dateexp1 = DateUtils.getSqlDateFromString(description.getDtReport(), Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDtReport(dateexp1);
				
				}

			}

			if (description.getDateRepot()!= null)

			{
				if(description.getDateRepot().indexOf("T")!=-1)
				{
					String[] assyDate = description.getDateRepot().split("T");
					String[] assyDate1 = assyDate[0].split("-");
					String newAssyDate = (Integer.parseInt(assyDate1[2]) + 1) + "-" + assyDate1[1] + "-" + assyDate1[0];
					Date dateexp1 = DateUtils.getSqlDateFromString(newAssyDate, Constants.GenericDateFormat.DATE_FORMAT);
					newselection.setDtReport(dateexp1);

					newselection.setDtReportBy(dateexp1);
				}
				
				else
				{
					
	    Date dateexp1 = DateUtils.getSqlDateFromString(description.getDateRepot(), Constants.GenericDateFormat.DATE_FORMAT);
				newselection.setDtReportBy(dateexp1);
				}

			}

		}

		if (maincase.getCase1Id() == null) {
			int newsectioncaseid = masterservice.getNewSelctionCaseTempMaxId();
			newselection.setNewcaseToken("TEMPTOKEN" + newsectioncaseid);
		} else {
			newselection.setNewcaseToken(maincase.getToken());

		}

		return newselection;
	}

	
	/****************
	 * end of newSElection temp
	 *******************************************************************************************/

	@RequestMapping(value = "/savingDescriptionandRemarks", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllLoadFactoer(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Description d = new Description();
		/*String description = req.getParameter("description");*/
		String remarks = req.getParameter("remarks");
		Integer tab=Integer.parseInt(req.getParameter("tab"));
	/*	d.setDescription(description);*/
		d.setRemarks(remarks);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		String dd = dateFormat.format(date);
		Date dateexp1 = dateFormat.parse(dd);
		d.setCurrentDate(dateexp1);
		
		
		Calendar now = Calendar.getInstance();
		 
		int hour = now.get(Calendar.HOUR_OF_DAY);
		int minute = now.get(Calendar.MINUTE);
		 
		String hoursandminutes=hour+":"+minute;
		
		d.setHoursandminutes(hoursandminutes);
		d.setTab(tab);

		HttpSession hs=req.getSession();
		String username=(String)hs.getAttribute("user");
		String casref=(String)hs.getAttribute("caseref");
		d.setUserName(username);
		d.setCaserefernce(casref);
		Session session = hibernateDao.getSessionFactory().openSession();
		 session.beginTransaction();
			
		session.save(d);
		session.getTransaction().commit();
		session.close();
		
		
		Session session1 = hibernateDao.getSessionFactory().openSession();
		String query1="from Description where caserefernce='"+casref+"' order by did desc";
		Query query = session1.createQuery(query1);
		List<Description> description=query.list();
		
		JSONObject mainobject=new JSONObject();
		JSONArray mainarray=new JSONArray();
		int i=1;
		for(Description des:description)
		{
			JSONObject subobject=new JSONObject();
			subobject.put("id",i);
			subobject.put("remarks",des.getRemarks());
			String k2 = DateUtils.formatDate(des.getCurrentDate(), Constants.GenericDateFormat.DATE_FORMAT);
			subobject.put("date",k2+" at"+des.getHoursandminutes());
			
			subobject.put("username",des.getUserName());
			mainarray.add(subobject);
			i++;
		}
 
		mainobject.put("rerkadata", mainarray);
		return 	mainobject;
	
	}
	
	
	
	@RequestMapping(value = "/gettingRemarksandDiscription", method = RequestMethod.GET)
	public @ResponseBody JSONObject getRemarksAndDiscription(HttpServletRequest req, HttpServletResponse res) throws Exception {
	 

		 
	 Integer tab=Integer.parseInt(req.getParameter("tab"));
		 
		Session session = hibernateDao.getSessionFactory().openSession();
		String query1="from Description where tab="+tab+"";
		Query query = session.createQuery(query1);
		List<Description> description=query.list();
		
		JSONObject mainobject=new JSONObject();
		JSONArray mainarray=new JSONArray();
		int i=1;
		for(Description des:description)
		{
			JSONObject subobject=new JSONObject();
			subobject.put("id",i);
			subobject.put("remarks",des.getRemarks());
			String k2 = DateUtils.formatDate(des.getCurrentDate(), Constants.GenericDateFormat.DATE_FORMAT);
			subobject.put("date",k2+" at"+des.getHoursandminutes());
			
			subobject.put("username",des.getUserName());
			mainarray.add(subobject);
			i++;
		}
 
		mainobject.put("rerkadata", mainarray);
		return 	mainobject;
	}
	
	
	
	
	@RequestMapping(value = "/getAllSSccodes", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllSScodes(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		try
		{
		JSONObject sscodes=new JSONObject();
	    JSONArray  sscodeshjsonarray=new JSONArray();
		List<Object[]> sscodes1=masterservice.getAllSSCCodes();
		for(Object[] sscodes2:sscodes1)
		{
			JSONObject subsccode=new JSONObject();
			String sscoded=(String)sscodes2[0];
			String description=(String)sscodes2[1];
					
			subsccode.put("id", sscoded);
			subsccode.put("desp",description+"-"+sscoded);
			sscodeshjsonarray.add(subsccode);
			
		}
		sscodes.put("data",sscodeshjsonarray);

		return  sscodes;
		
		}catch(Exception  e)
		{
			logger.error(e.getMessage()+"  Error in  the line  ");
			return null;
		}
	}
	
	 
	 	
	
	
	@RequestMapping(value = "/getAllCustmerNamesInCasess", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAllCustmerNamesInCasess(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		try
		{
		JSONObject mainobject=new JSONObject();
	    JSONArray  custmernamesonarray=new JSONArray();
	    HttpSession hs=req.getSession();
	    String user=(String)hs.getAttribute("user");
		List<String> custmernames=masterservice.getAllCustmerNamesInCasesSummarey(user);
		for(String custmername:custmernames)
		{
			JSONObject custmer=new JSONObject();
			custmer.put("name", custmername);
			custmernamesonarray.add(custmer);
			
		}
		mainobject.put("data",custmernamesonarray);

		return  mainobject;
		
		}catch(Exception  e)
		{
			logger.error(e.getStackTrace()+"  Error in  the line  ");
			return null;
		}
	}
	

	@RequestMapping(value = "/getAlltheDetailsIntheCustmerInCasess", method = RequestMethod.GET)
	public @ResponseBody JSONObject getAlltheDetailsOfCustmerInCasess(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		try
		{
			String custmername=req.getParameter("custmername");
		JSONObject mainobject=new JSONObject();
	    JSONArray  custmernamesonarray=new JSONArray();
	    HttpSession hs=req.getSession();
	    String user=(String)hs.getAttribute("user");
		List<CaseSummary> custmernames=masterservice.getAllCustmerNamesInCasess(custmername,user);
	
		for(CaseSummary custmername1:custmernames)
		{
			JSONObject custmer=new JSONObject();
			custmer.put("caseRef",custmername1.getCaseRef());
			custmer.put("customer",custmername1.getCustomer());
			Date d2 = custmername1.getDtCase();
			if (d2 != null) {
				String k1 = DateUtils.formatDate(d2, Constants.GenericDateFormat.DATE_FORMAT);
				custmer.put("dateCase",k1);

			}
			 
			custmer.put("caseType",custmername1.getCasetype());
			custmer.put("description",custmername1.getDescription());
			custmer.put("tab",custmername1.getTab());
			 
			custmernamesonarray.add(custmer);
			
		}
		mainobject.put("data",custmernamesonarray);

		return  mainobject;
		
		}catch(Exception  e)
		{
			logger.error(e.getStackTrace()+"  Error in  the line  ");
			return null;
		}
	}
	

	
	
	
	
	
	@RequestMapping(value = "/loadingCasessTabFromMainTable", method = RequestMethod.POST)
	public @ResponseBody JSONObject loadingCasessTabFromMainTable(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
	

			JSONObject mainobject = new JSONObject();
			JSONArray gridarray = null;
			String token = null;

			HttpSession hs = req.getSession();
			String username = (String) hs.getAttribute("user");

			String custmername = req.getParameter("custmername");
			String caseref = req.getParameter("caseref");
			hs.setAttribute("caseref",caseref);
			Integer tab = Integer.parseInt(req.getParameter("tab"));

			
				List<CaseSummary> newselection = masterservice.getDeatilsFromNewSelection(custmername, caseref);
				JSONArray manincaseobjectarray = new JSONArray();
				JSONArray descriptionobjectarray = new JSONArray();
				for (CaseSummary data : newselection) {
					JSONObject maincaseobject = new JSONObject();
					JSONObject descriptioncaseobject = new JSONObject();
					maincaseobject.put("customer", data.getCustomer());
					maincaseobject.put("mainid", data.getNewselectionid());
					maincaseobject.put("customerCode", data.getCustomerCode());
					maincaseobject.put("caseRef", data.getCaseRef());
					maincaseobject.put("contactPerson", data.getContactPerson());
					maincaseobject.put("phone", data.getPhone());
					maincaseobject.put("address", data.getAddress());
					maincaseobject.put("customerRef", data.getCustomerRef());
					maincaseobject.put("images",data.getImages());
					maincaseobject.put("assigned",data.getAssigned());
					maincaseobject.put("custStatus",data.getCustStatus());
					maincaseobject.put("casePriority",data.getCasePriority());
					maincaseobject.put("orginReq",data.getOrginReq());
					
					token = data.getNewcaseToken();

					Date d1 = data.getDateExp();
					if (d1 != null) {
						d1 = data.getDateExp();
						String k1 = DateUtils.formatDate(data.getDateExp(), Constants.GenericDateFormat.DATE_FORMAT);
						String[] k2 = k1.split("-");
						String k3 = k2[2] + "-" + k2[1] + "-" + k2[0];

						maincaseobject.put("dateExp", k1);

					}
					Date d2 = data.getDtCase();
					if (d2 != null) {
						String k1 = DateUtils.formatDate(data.getDtCase(), Constants.GenericDateFormat.DATE_FORMAT);
						String[] k2 = k1.split("-");
						String k3 = k2[2] + "-" + k2[1] + "-" + k2[0];
						maincaseobject.put("dateCase", k1);

					}
					Date d3 = data.getDtReport();
					if (d3 != null) {
						String k1 = DateUtils.formatDate(d3, Constants.GenericDateFormat.DATE_FORMAT);
						String[] k2 = k1.split("-");
						String k3 = k2[2] + "-" + k2[1] + "-" + k2[0];
						descriptioncaseobject.put("dtReport", k1);

					}
					Date d4 = data.getDtReportBy();
					if (d4 != null) {
						String k1 = DateUtils.formatDate(d4, Constants.GenericDateFormat.DATE_FORMAT);
						String[] k2 = k1.split("-");
						String k3 = k2[2] + "-" + k2[1] + "-" + k2[0];
						descriptioncaseobject.put("dateRepot", k1);

					}

					descriptioncaseobject.put("description", data.getDescription());

					manincaseobjectarray.add(maincaseobject);
					descriptionobjectarray.add(descriptioncaseobject);

				}
				
				
				
				Session session = hibernateDao.getSessionFactory().openSession();
				String query1="from Description where caserefernce='"+caseref+"' order by did desc";
				Query query = session.createQuery(query1);
				List<Description> description=query.list();
				
				 
				JSONArray mainarray=new JSONArray();
				int p=1;
				for(Description des:description)
				{
					JSONObject subobject=new JSONObject();
					subobject.put("id",p);
					subobject.put("remarks",des.getRemarks());
					String k2 = DateUtils.formatDate(des.getCurrentDate(), Constants.GenericDateFormat.DATE_FORMAT);
					subobject.put("date",k2+" at"+des.getHoursandminutes());
					
					subobject.put("username",des.getUserName());
					mainarray.add(subobject);
					p++;
				}
		 
				mainobject.put("rerkadata", mainarray);
				
				

				if(tab==1)
				{
				List<NewSelectionGrid> newselectiongrid = masterservice.getDetailsFromNewSelectionGrid(caseref);
				gridarray = new JSONArray();
				int i = 1;
				for (NewSelectionGrid nst : newselectiongrid) {

					JSONObject gridobject = new JSONObject();
					gridobject.put("gm", nst.getGm());
					gridobject.put("id", i);
					gridobject.put("gearMotor", nst.getGearMotor());
					gridobject.put("quantity", nst.getQuantity());
					gridobject.put("application", nst.getApplication());
					gridobject.put("ambientTemp", nst.getAmbientTemp());
					gridobject.put("gearProduct", nst.getGearProduct());
					gridobject.put("model", nst.getModel());
					gridobject.put("selectModel", nst.getSelectModel());
					gridobject.put("servFactor", nst.getServFactor());
					gridobject.put("motorType", nst.getMotorType());
					gridobject.put("motorKW", nst.getMotorKW().toString());
					gridobject.put("pole", nst.getPole());
					gridobject.put("phase", nst.getPhase());
					gridobject.put("voltage", nst.getVoltage());
					gridobject.put("hertz", nst.getHertz());
					gridobject.put("rpm", nst.getRpm());
					gridobject.put("protectionGrade", nst.getProtectionGrade());
					gridobject.put("internationalStd", nst.getInternationalStd());
					gridobject.put("motorBrake", nst.getMotorBrake());
					gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
					gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
					gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
					gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
					gridobject.put("dirAxial", nst.getDirAxial());
					gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
					gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
					gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
					gridobject.put("dirSssAxial", nst.getDirSssAxial());
					gridobject.put("ratioRequired", nst.getRatioRequired());
					gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
					gridobject.put("presetTorque", nst.getPresetTorque());
					gridobject.put("conf", nst.getConf());
					gridobject.put("mount", nst.getMount());
					gridobject.put("accessMotor", nst.getAccessMotor());
					  
					
					 
					gridobject.put("mount2",nst.getMount2());
				 
					
					if(nst.getOshaftInc2().equalsIgnoreCase("true"))
					{
						gridobject.put("oshaftInc2",true);
						
					}
					
					else
					{
						gridobject.put("oshaftInc2",false);
						
					}
					
					if(nst.getOshaftInc1().equalsIgnoreCase("true"))
					{
						 
						 gridobject.put("oshaftInc1",true);
						 
						}
					
					else
					{
						
						 gridobject.put("oshaftInc1",false);
					}
					
					gridobject.put("ishaft", nst.getIshaft());
					gridobject.put("oshaft", nst.getOshaft());
					gridobject.put("oshaftInc", nst.getOshaftInc());
					gridobject.put("deg", nst.getDeg());
					gridobject.put("towards", nst.getTowards());
					gridobject.put("oshafty", nst.getOshafty());
					gridobject.put("oshafty2", nst.getOshafty2());
					gridobject.put("backStop", nst.getBackStop());
					gridobject.put("rot", nst.getRot());
					gridobject.put("dirHssAxial", nst.getDirHssAxial());
					gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
					gridobject.put("inverter",nst.getInverter());
					 
					gridobject.put("other",nst.getOther());
					gridobject.put("othersPlz",nst.getOthersPlz());
					
					
					String ssccodes = nst.getSscCode();
					if (ssccodes != null) {
						String sscode[] = ssccodes.split(",");
						gridobject.put("sscCode", sscode);
					}

					gridobject.put("docFile", nst.getImagepath());
					gridobject.put("gridid", i);
					
					gridobject.put("accessMotor",nst.getAccessMotor());

					gridarray.add(gridobject);

					i++;

				}

				if (gridarray.size() == 0) {
					JSONObject gridobject = new JSONObject();
					gridobject.put("id", 1);
					gridarray.add(gridobject);
				}

				mainobject.put("griddata", gridarray);
				mainobject.put("maincaseform", manincaseobjectarray);
				mainobject.put("description", descriptionobjectarray);
				mainobject.put("tab", 1);

				return mainobject;

			}

				else if (tab == 2) {
				

				List<CompititorGrid> newselectiongrid = masterservice.getDetailsFromCompititorGrid(caseref);
				gridarray = new JSONArray();
				int i = 1;
				for (CompititorGrid nst : newselectiongrid) {

					JSONObject gridobject = new JSONObject();
					gridobject.put("gm", nst.getGm());
					gridobject.put("id", i);
					gridobject.put("gearMotor", nst.getGearMotor());
					gridobject.put("quantity", nst.getQuantity());
					gridobject.put("application", nst.getApplication());
					gridobject.put("ambientTemp", nst.getAmbientTemp());
					gridobject.put("gearProduct", nst.getGearProduct());
					gridobject.put("model", nst.getModel());
					gridobject.put("selectModel", nst.getSelectModel());
					gridobject.put("servFactor", nst.getRequiredSreviceFactor());
					gridobject.put("motorType", nst.getMotorType());
					gridobject.put("motorKW", nst.getMotorKW().toString());
					gridobject.put("pole", nst.getPole());
					gridobject.put("phase", nst.getPhase());
					gridobject.put("voltage", nst.getVoltage());
					gridobject.put("hertz", nst.getHertz());
					gridobject.put("rpm", nst.getRpm());
					gridobject.put("protectionGrade", nst.getProtectionGrade());
					gridobject.put("internationalStd", nst.getInternationalStd());
					gridobject.put("motorBrake", nst.getMotorBrake());
					gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
					gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
					gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
					gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
					/*gridobject.put("dirAxial", nst.getDirAxial());*/
					gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
					gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
					gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
					gridobject.put("dirSssAxial", nst.getDirSssAxial());
					gridobject.put("ratioRequired", nst.getRatioRequired());
					gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
					gridobject.put("presetTorque", nst.getPresetTorque());
					gridobject.put("conf", nst.getConf());
					gridobject.put("mount", nst.getMount());
					//gridobject.put("mount2", nst.getMount2());
					//gridobject.put("mount3", nst.getMount3());
					gridobject.put("ishaft", nst.getIshaft());
					gridobject.put("oshaft", nst.getOshaft());
					gridobject.put("oshaftInc", nst.getOshaftInc());
					gridobject.put("deg", nst.getDeg());
					gridobject.put("towards", nst.getTowards());
					gridobject.put("oshafty", nst.getOshafty());
					gridobject.put("oshafty2", nst.getOshafty2());
					gridobject.put("backStop", nst.getBackStop());
					gridobject.put("rot", nst.getRot());
					gridobject.put("dirHssAxial", nst.getDirHssAxial());
					gridobject.put("requiredSreviceFactor",nst.getRequiredSreviceFactor());
					gridobject.put("competitorBrand", nst.getCompetitorBrand());
					gridobject.put("competitorModel",nst.getCompetitorModel());
					gridobject.put("targetPrice",nst.getTargetPrice());
					gridobject.put("dimension",nst.getDimension());
					
					gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
					gridobject.put("inverter",nst.getInverter());
					
					gridobject.put("accessMotor", nst.getAccessMotor());
				 
					gridobject.put("mount2",nst.getMount2());
					 
					
					if(nst.getOshaftInc2().equalsIgnoreCase("true"))
					{
						gridobject.put("oshaftInc2",true);
						
					}
					
					else
					{
						gridobject.put("oshaftInc2",false);
						
					}
					
					if(nst.getOshaftInc1().equalsIgnoreCase("true"))
					{
						 
						 gridobject.put("oshaftInc1",true);
						 
						}
					
					else
					{
						
						 gridobject.put("oshaftInc1",false);
					}
					
									
					//gridobject.put("oshaftInc2",nst.getOshaftInc2());
					//gridobject.put("oshaftInc1",nst.getOshaftInc1());
					gridobject.put("other",nst.getOther());
					gridobject.put("othersPlz",nst.getOthersPlz());
					
					
				
				
					
				 
					
					String ssccodes = nst.getCompititorcode();
					if (ssccodes != null) {
						String sscode[] = ssccodes.split(",");
						gridobject.put("compSSCCode", sscode);
					}
					

					gridobject.put("docFile", nst.getImagepath());
					gridobject.put("gridid", i);
					gridobject.put("accessMotor",nst.getAccessMotor());
					gridarray.add(gridobject);

					i++;

				}

				if (gridarray.size() == 0) {
					JSONObject gridobject = new JSONObject();
					gridobject.put("id", 1);
					gridarray.add(gridobject);
				}

				mainobject.put("griddata", gridarray);
				mainobject.put("maincaseform", manincaseobjectarray);
				mainobject.put("description", descriptionobjectarray);
				mainobject.put("tab", 2);

				 

			
				
				return mainobject;
			}
			
			else if(tab == 3)
			{
				

				List<SumitomoReplaceGrid> newselectiongrid = masterservice.getDetailsFromSumitimGrid(caseref);
				gridarray = new JSONArray();
				int i = 1;
				for (SumitomoReplaceGrid nst : newselectiongrid) {

					JSONObject gridobject = new JSONObject();
					gridobject.put("gm", nst.getGm());
					gridobject.put("id", i);
					gridobject.put("gearMotor", nst.getGearMotor());
					gridobject.put("accessMotor", nst.getAccessMotor());
					gridobject.put("quantity", nst.getQuantity());
					gridobject.put("application", nst.getApplication());
					gridobject.put("ambientTemp", nst.getAmbientTemp());
					gridobject.put("gearProduct", nst.getGearProduct());
					gridobject.put("model", nst.getModel());
					gridobject.put("selectModel", nst.getSelectModel());
					gridobject.put("servFactor", nst.getServFactor());
					gridobject.put("motorType", nst.getMotorType());
					gridobject.put("motorKW", nst.getMotorKW().toString());
					gridobject.put("pole", nst.getPole());
					gridobject.put("phase", nst.getPhase());
					gridobject.put("voltage", nst.getVoltage());
					gridobject.put("hertz", nst.getHertz());
					gridobject.put("rpm", nst.getRpm());
					gridobject.put("protectionGrade", nst.getProtectionGrade());
					gridobject.put("internationalStd", nst.getInternationalStd());
					gridobject.put("motorBrake", nst.getMotorBrake());
					gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
					gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
					gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
					gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
					gridobject.put("dirAxial", nst.getDirAxial());
					gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
					gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
					gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
					gridobject.put("dirSssAxial", nst.getDirSssAxial());
					gridobject.put("ratioRequired", nst.getRatioRequired());
					gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
					gridobject.put("presetTorque", nst.getPresetTorque());
					gridobject.put("conf", nst.getConf());
					gridobject.put("mount", nst.getMount());
					//gridobject.put("mount2", nst.getMount2());
					//gridobject.put("mount3", nst.getMount3());
					gridobject.put("ishaft", nst.getIshaft());
					gridobject.put("oshaft", nst.getOshaft());
					gridobject.put("oshaftInc", nst.getOshaftInc());
					gridobject.put("deg", nst.getDeg());
					gridobject.put("towards", nst.getTowards());
					gridobject.put("oshafty", nst.getOshafty());
					gridobject.put("oshafty2", nst.getOshafty2());
					gridobject.put("backStop", nst.getBackStop());
					gridobject.put("rot", nst.getRot());
					gridobject.put("dirHssAxial", nst.getDirHssAxial());
					
					gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
					gridobject.put("inverter",nst.getInverter());
					
 
					gridobject.put("mount2",nst.getMount2());
					 
					if(nst.getOshaftInc2().equalsIgnoreCase("true"))
					{
						gridobject.put("oshaftInc2",true);
						
					}
					
					else
					{
						gridobject.put("oshaftInc2",false);
						
					}
					
					if(nst.getOshaftInc1().equalsIgnoreCase("true"))
					{
						 
						 gridobject.put("oshaftInc1",true);
						 
						}
					
					else
					{
						
						 gridobject.put("oshaftInc1",false);
					}
					
					//gridobject.put("oshaftInc2",nst.getOshaftInc2());
					//gridobject.put("oshaftInc1",nst.getOshaftInc1());
					gridobject.put("other",nst.getOther());
					gridobject.put("othersPlz",nst.getOthersPlz());
					
					
					
					String ssccodes = nst.getSscCode();
					if (ssccodes != null) {
						String sscode[] = ssccodes.split(",");
						gridobject.put("sscCode", sscode);
					}

					gridobject.put("docFile", nst.getImagepath());
					gridobject.put("gridid", i);
					
					 
					
					gridobject.put("gearhead",nst.getGearhead());
					gridobject.put("motorSerial",nst.getMotorSerial());
					gridobject.put("shiMfg",nst.getShiMfg());
					
					gridobject.put("dimension",nst.getDimension());
					gridobject.put("accessMotor",nst.getAccessMotor());
					gridarray.add(gridobject);

					i++;

				}

				if (gridarray.size() == 0) {
					JSONObject gridobject = new JSONObject();
					gridobject.put("id", 1);
					gridarray.add(gridobject);
				}

				mainobject.put("griddata", gridarray);
				mainobject.put("maincaseform", manincaseobjectarray);
				mainobject.put("description", descriptionobjectarray);
				mainobject.put("tab", 3);

				return mainobject;

			
				
				
			}
			
			else if(tab==4)
			{

				List<SparePartGrid> newselectiongrid = masterservice.getDetailsFromSparePartGrid(caseref);
				gridarray = new JSONArray();
				int i = 1;
				for (SparePartGrid nst : newselectiongrid) {

					JSONObject gridobject = new JSONObject();
					gridobject.put("gm", nst.getGm());
					gridobject.put("id", i);
					gridobject.put("gearMotor", nst.getGearMotor());
					gridobject.put("accessMotor", nst.getAccessMotor());
					gridobject.put("quantity", nst.getQuantity());
					gridobject.put("application", nst.getApplication());
					gridobject.put("ambientTemp", nst.getAmbientTemp());
					gridobject.put("gearProduct", nst.getGearProduct());
					gridobject.put("model", nst.getModel());
					gridobject.put("selectModel", nst.getSelectModel());
					gridobject.put("servFactor", nst.getServFactor());
					gridobject.put("motorType", nst.getMotorType());
					gridobject.put("motorKW", nst.getMotorKW().toString());
					gridobject.put("pole", nst.getPole());
					gridobject.put("phase", nst.getPhase());
					gridobject.put("voltage", nst.getVoltage());
					gridobject.put("hertz", nst.getHertz());
					gridobject.put("rpm", nst.getRpm());
					gridobject.put("protectionGrade", nst.getProtectionGrade());
					gridobject.put("internationalStd", nst.getInternationalStd());
					gridobject.put("motorBrake", nst.getMotorBrake());
					gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
					gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
					gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
					gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
					gridobject.put("dirAxial", nst.getDirAxial());
					gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
					gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
					gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
					gridobject.put("dirSssAxial", nst.getDirSssAxial());
					gridobject.put("ratioRequired", nst.getRatioRequired());
					gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
					gridobject.put("presetTorque", nst.getPresetTorque());
					gridobject.put("conf", nst.getConf());
					gridobject.put("mount", nst.getMount());
					//gridobject.put("mount2", nst.getMount2());
					//gridobject.put("mount3", nst.getMount3());
					gridobject.put("ishaft", nst.getIshaft());
					gridobject.put("oshaft", nst.getOshaft());
					gridobject.put("oshaftInc", nst.getOshaftInc());
					gridobject.put("deg", nst.getDeg());
					gridobject.put("towards", nst.getTowards());
					gridobject.put("oshafty", nst.getOshafty());
					gridobject.put("oshafty2", nst.getOshafty2());
					gridobject.put("backStop", nst.getBackStop());
					gridobject.put("rot", nst.getRot());
					gridobject.put("dirHssAxial", nst.getDirHssAxial());
					String ssccodes = nst.getSscCode();
					if (ssccodes != null) {
						String sscode[] = ssccodes.split(",");
						gridobject.put("sscCode", sscode);
					}

					gridobject.put("docFile", nst.getImagepath());
					gridobject.put("gridid", i);
					
					gridobject.put("spare",nst.getSpare());
					gridobject.put("descript",nst.getDescript());
					gridobject.put("qty",nst.getQty());
					gridobject.put("spare1",nst.getSpare1());
					gridobject.put("descript1",nst.getDescript1());
					gridobject.put("qty1",nst.getQty1());
					gridobject.put("spare2",nst.getSpare2());
					gridobject.put("descript2",nst.getDescript2());
					gridobject.put("qty2",nst.getQty2());
					
					
					
					
					gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
					gridobject.put("inverter",nst.getInverter());
					

					/*if(nst.getMount2().equalsIgnoreCase("true"))
					{
					gridobject.put("mount2",1);
					}
					else
					{
					gridobject.put("mount2", 0);
					}
					if(nst.getMount3().equalsIgnoreCase("true"))
					{
						gridobject.put("mount3",1);
						
					}
					else
					{
						gridobject.put("mount3",0);
					}
					
					if(nst.getOshaftInc2().equalsIgnoreCase("true"))
					{
						gridobject.put("oshaftInc2",1);
						
					}
					
					else
					{
						gridobject.put("oshaftInc2",0);
						
					}
					
					if(nst.getOshaftInc1().equalsIgnoreCase("true"))
					{
						 
						 gridobject.put("oshaftInc1",1);
						 
						}
					
					else
					{
						
						 gridobject.put("oshaftInc1",0);
					}
					
				*/	//gridobject.put("oshaftInc2",nst.getOshaftInc2());
					//gridobject.put("oshaftInc1",nst.getOshaftInc1());
					gridobject.put("other",nst.getOther());
					gridobject.put("othersPlz",nst.getOthersPlz());
					
					
					gridobject.put("gearhead",nst.getGearhead());
					gridobject.put("motorSerial",nst.getMotorSerial());
					gridobject.put("shiMfg",nst.getShiMfg());
					gridobject.put("accessMotor",nst.getAccessMotor());
				
					gridarray.add(gridobject);

					i++;

				}

				if (gridarray.size() == 0) {
					JSONObject gridobject = new JSONObject();
					gridobject.put("id", 1);
					gridarray.add(gridobject);
				}

				mainobject.put("griddata", gridarray);
				mainobject.put("maincaseform", manincaseobjectarray);
				mainobject.put("description", descriptionobjectarray);
				
				
				
				
				mainobject.put("tab", 4);

				return mainobject;

			
			}
				
				if(tab==5)
					
				{
					

					List<TechGrid> newselectiongrid = masterservice.getDetailsFromTechGrid(caseref);
					gridarray = new JSONArray();
					int i = 1;
					for (TechGrid nst : newselectiongrid) {

						JSONObject gridobject = new JSONObject();
						gridobject.put("gm", nst.getGm());
						gridobject.put("id", i);
						gridobject.put("gearMotor", nst.getGearMotor());
						gridobject.put("quantity", nst.getQuantity());
						gridobject.put("application", nst.getApplication());
						gridobject.put("ambientTemp", nst.getAmbientTemp());
						gridobject.put("gearProduct", nst.getGearProduct());
						gridobject.put("model", nst.getModel());
						gridobject.put("selectModel", nst.getSelectModel());
						gridobject.put("servFactor", nst.getServFactor());
						gridobject.put("motorType", nst.getMotorType());
						gridobject.put("motorKW", nst.getMotorKW().toString());
						gridobject.put("pole", nst.getPole());
						gridobject.put("phase", nst.getPhase());
						gridobject.put("voltage", nst.getVoltage());
						gridobject.put("hertz", nst.getHertz());
						gridobject.put("accessMotor", nst.getAccessMotor());
						gridobject.put("rpm", nst.getRpm());
						gridobject.put("protectionGrade", nst.getProtectionGrade());
						gridobject.put("internationalStd", nst.getInternationalStd());
						gridobject.put("motorBrake", nst.getMotorBrake());
						gridobject.put("motorBrakeVolt", nst.getMotorBrakeVolt());
						gridobject.put("hssRadialLoadKn", nst.getHssRadialLoadKn());
						gridobject.put("hssRadialLoadMm", nst.getHssRadialLoadMm());
						gridobject.put("hssAxialLoadKn", nst.getHssAxialLoadKn());
						gridobject.put("dirAxial", nst.getDirAxial());
						gridobject.put("sssRadialLoadKn", nst.getSssRadialLoadKn());
						gridobject.put("sssRadialLoadMm", nst.getSssRadialLoadMm());
						gridobject.put("sssAxialLoadKn", nst.getSssAxialLoadKn());
						gridobject.put("dirSssAxial", nst.getDirSssAxial());
						gridobject.put("ratioRequired", nst.getRatioRequired());
						gridobject.put("requiredOutputSpeed", nst.getRequiredOutputSpeed());
						gridobject.put("presetTorque", nst.getPresetTorque());
						gridobject.put("conf", nst.getConf());
						gridobject.put("mount", nst.getMount());
						//gridobject.put("mount2", nst.getMount2());
						//gridobject.put("mount3", nst.getMount3());
						gridobject.put("ishaft", nst.getIshaft());
						gridobject.put("oshaft", nst.getOshaft());
						gridobject.put("oshaftInc", nst.getOshaftInc());
						gridobject.put("deg", nst.getDeg());
						gridobject.put("towards", nst.getTowards());
						gridobject.put("oshafty", nst.getOshafty());
						gridobject.put("oshafty2", nst.getOshafty2());
						gridobject.put("backStop", nst.getBackStop());
						gridobject.put("rot", nst.getRot());
						gridobject.put("dirHssAxial", nst.getDirHssAxial());
						gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
						gridobject.put("inverter",nst.getInverter());
						
						String ssccodes = nst.getSscCode();
						if (ssccodes != null) {
							String sscode[] = ssccodes.split(",");
							gridobject.put("sscCode", sscode);
						}

						gridobject.put("docFile", nst.getImagepath());
						gridobject.put("gridid", i);
						gridobject.put("requiredSreviceFactor",nst.getRequiredSreviceFactor());
						gridobject.put("gearhead",nst.getGearhead());
						gridobject.put("motorSerial",nst.getMotorSerial());
						gridobject.put("shiMfg",nst.getShiMfg());
 
						gridobject.put("mount2",nst.getMount2());
						 
						
						if(nst.getOshaftInc2().equalsIgnoreCase("true"))
						{
							gridobject.put("oshaftInc2",true);
							
						}
						
						else
						{
							gridobject.put("oshaftInc2",false);
							
						}
						
						if(nst.getOshaftInc1().equalsIgnoreCase("true"))
						{
							 
							 gridobject.put("oshaftInc1",true);
							 
							}
						
						else
						{
							
							 gridobject.put("oshaftInc1",false);
						}
						
						//gridobject.put("oshaftInc2",nst.getOshaftInc2());
						//gridobject.put("oshaftInc1",nst.getOshaftInc1());
						gridobject.put("other",nst.getOther());
						gridobject.put("othersPlz",nst.getOthersPlz());
						gridobject.put("accessMotor",nst.getAccessMotor());
						
						gridarray.add(gridobject);

						i++;

					}

					if (gridarray.size() == 0) {
						JSONObject gridobject = new JSONObject();
						gridobject.put("id", 1);
						gridarray.add(gridobject);
					}

					mainobject.put("griddata", gridarray);
					mainobject.put("maincaseform", manincaseobjectarray);
					mainobject.put("description", descriptionobjectarray);
					mainobject.put("tab", 5);

					return mainobject;

				
					
				}
			
			
				return mainobject;

			
		 
	}
	
	

	
	
	@RequestMapping(value = "/loadingsequenceNember", method = RequestMethod.GET)
	public @ResponseBody JSONObject loadingsequenceNember(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		try
		{
			HttpSession session=req.getSession(false);
			String countryCode=(String)session.getAttribute("countrycode");

			String usernasme=(String)session.getAttribute("user");
			JSONObject mainobject=new JSONObject();
			JSONArray manincaseobjectarray=new JSONArray();
			JSONArray descriptionobjectarray=new JSONArray();
			JSONObject maincaseobject = new JSONObject();
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			 
		     String   time=day+"-"+month+"-"+year;
			
			maincaseobject.put("caseRef",bsscommonService.getSequenceNumber("E",countryCode,usernasme));
			maincaseobject.put("assigned","Sales");
			maincaseobject.put("casePriority","Normal");
			maincaseobject.put("orginReq",0);
			maincaseobject.put("custStatus","Open");
			maincaseobject.put("dateCase",time);
			
			
			JSONObject descriptioncaseobject = new JSONObject();
			descriptioncaseobject.put("dtReport",time);
			descriptionobjectarray.add(descriptioncaseobject);
          mainobject.put("description",descriptionobjectarray);
			
			
			manincaseobjectarray.add(maincaseobject);
			mainobject.put("maincaseform",manincaseobjectarray);
			return  mainobject;
		
		}catch(Exception  e)
		{
			logger.error(e.getStackTrace()+"  Error in  the line  ");
			return null;
		}
	}
	

/*************************** Start Competior Replacement*****************************************************************************************************/
	
	
	@RequestMapping(value = "/competitorImageSave", method = RequestMethod.POST)
	public @ResponseBody boolean competitorImageSave(@RequestParam("docFile") List<MultipartFile> file1, HttpServletRequest req,
			@RequestParam("docFileId") String id,ServletContext servletContext) throws Exception {

		int gridid = Integer.parseInt(id);

		String photoPath = null;

		for (MultipartFile file : file1) {

			if (file.getSize() != 0) {
				Random t = new Random();
				int num = t.nextInt(10000);

				int maxid = masterservice.getMaxIdCustmer();
				String ext = getFileExtension(file.getOriginalFilename());
				String photoName = maxid + num + "_photo" + "." + ext;
				photoPath = "images/" + photoName;

				/* masterservice.updateimageonMaxId(maxid,photoPath); */

				File dir1 = new File(servletContext.getRealPath("/") + "competitorImges/");

				if (!dir1.exists())
					dir1.mkdirs();

				if (file.getSize() != 0) {
					byte[] bytes1 = file.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}

			}

		}

		HttpSession imagesesssion = req.getSession();

		if (imagesesssion.getAttribute("image") == null) {
			TreeMap<Integer, String> al = new TreeMap<Integer, String>();
			al.put(gridid, photoPath);

			imagesesssion.setAttribute("image", al);
		}

		if (imagesesssion.getAttribute("image") != null) {
			TreeMap<Integer, String> al = (TreeMap<Integer, String>) imagesesssion.getAttribute("image");
			al.put(gridid, photoPath);
			imagesesssion.setAttribute("image", al);

		}

		return true;
	}
	
	
	
	
	
	@RequestMapping(value = "/saveCompetitorReplaceData", method = RequestMethod.POST)
public @ResponseBody boolean Competitor(MultipartHttpServletRequest req)
 throws Exception {
		
		 HttpSession session=req.getSession(false);
		 String countryCode=(String)session.getAttribute("countrycode");
		 String usernasme=(String)session.getAttribute("user");
		 

		
		String ss=req.getParameter("formData");
		String description1=req.getParameter("fieldData");
		String ss1=req.getParameter("gridData");
		org.json.JSONArray gData=new org.json.JSONArray(ss1);
	  //  List<MultipartFile> mpf=req.getFiles("motorFile");
	    
	    //String imagepath1=this.getImagepathOfCompititorTemp(mpf);
		String imagepath1=null;
		  String gik=req.getParameter("motorFile");
		   if(gik!=null)
		   {
		    imagepath1=gik.replaceAll("\"", "");
		   	    
		   }  
		
		
		

		int status = 0;
		int tab=2;
		TreeMap<Integer, String> al = null;
		DescriptionCaseBean description = null;
		HttpSession hs = req.getSession();
		if (hs.getAttribute("image") != null) {
			al = (TreeMap<Integer, String>) hs.getAttribute("image");
		}
		String user=(String)hs.getAttribute("user");
		/*JSONObject caseform = (JSONObject) jsonData.get("formData");
		JSONArray casegrid = (JSONArray) jsonData.get("gridData");
		JSONObject casedescription = (JSONObject) jsonData.get("fieldData");*/

		MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);
		if(!description1.equalsIgnoreCase("undefined")) {
			status = 1;
			description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
		}
		String k = null;
		ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
		for (int i = 0; i < gData.length(); i++) {

			NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
					NewSelectionGridFormBean.class);

			String a[] = newcasegrid.getCompSSCCode();
			if (a != null) {
				for (int m = 0; m < a.length; m++) {
					if (m == 0) {
						k = a[m];
					}

					if (m > 0) {

						k = k + ',' + a[m];
					}

				}

			}

			if (al != null) {
				String imagepath = (String) al.get(i + 1);
				newcasegrid.setImagepath(imagepath);
			}
			newcasegrid.setSsCode(k);

			newselctiongrid.add(newcasegrid);

		}

		if (maincase.getMainid() != null) {

			CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
			newselection.setCasetype("Compititor Replace");
			newselection.setTab(2);
			newselection.setNewselectionid(maincase.getMainid());
			
			/*masterservice.deletefromNewCaseSElection(maincase.getMainid());*/
			/*masterservice.deleteCompitiotorGrid(newselection.getNewcaseToken());*/
			
			masterservice.saveNewSelectionCase(newselection);
			masterservice.deleteCompitiotorGrid(newselection.getCaseRef());
			List<CompititorGrid> newselectiongrid = this.insertDataIntoCompititorBeanModel(newselctiongrid,newselection.getCaseRef());
			masterservice.saveCompitiorRepalceGrid(newselectiongrid);

		}

		if (maincase.getMainid() == null) {

			CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
			newselection.setCasetype("Compititor Replace");
			newselection.setTab(2);
			String caserefernce = newselection.getCaseRef();
			int caserefrencecount = masterservice.getCaseRefernce(caserefernce);
			ArrayList sequencenumberlist = new ArrayList();

			if (caserefrencecount == 0) {

				String[] spl = caserefernce.split("-");
				String[] st = spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");
				SequenceNumber sequenceNumber = new SequenceNumber();
				sequenceNumber.setProcessname(spl[0]);
				sequenceNumber.setCountrycode(st[0]);
				sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
				sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
				sequenceNumber.setTotalsequenceno(caserefernce);
				masterservice.saveNewSelectionCase(newselection);
				List<CompititorGrid> newselectiongrid = this.insertDataIntoCompititorBeanModel(newselctiongrid,newselection.getCaseRef());
				masterservice.saveCompitiorRepalceGrid(newselectiongrid);

				sequencenumberlist.add(sequenceNumber);
				productSelectionService.saveEntities(sequencenumberlist);

			}

			if (caserefrencecount > 0) {
				String caseref = bsscommonService.getSequenceNumber("E",countryCode,usernasme);
				newselection.setCaseRef(caseref);
				masterservice.saveNewSelectionCase(newselection);
				List<CompititorGrid> newselectiongrid = this.insertDataIntoCompititorBeanModel(newselctiongrid,newselection.getCaseRef());
				masterservice.saveCompitiorRepalceGrid(newselectiongrid);

				String[] spl = caseref.split("-");
				String[] st = spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");

				SequenceNumber sequenceNumber = new SequenceNumber();

				sequenceNumber.setProcessname(spl[0]);
				sequenceNumber.setCountrycode(st[0]);
				sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
				sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
				sequenceNumber.setTotalsequenceno(caseref);

				sequencenumberlist.add(sequenceNumber);

				productSelectionService.saveEntities(sequencenumberlist);
			}

			if (maincase.getTempid() != null) {

				masterservice.deleteFromCompititorReplceTemp(maincase.getTempid(),tab);

			}

		}

		hs.removeAttribute("image");

		return true;

	}

	private List<CompititorGrid> insertDataIntoCompititorBeanModel(ArrayList<NewSelectionGridFormBean> newselctiongrid,String caseref) 
	{
		ArrayList<CompititorGrid> compitor=new ArrayList<CompititorGrid>();
		String newselectionkey = masterservice.getNewSelectionKey();
		 for(NewSelectionGridFormBean ndgb:newselctiongrid)
 {
			CompititorGrid ng = new CompititorGrid();

			ng.setToken(newselectionkey);
			ng.setCaseref(caseref);
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setImagepath(ndgb.getImagepath());
			ng.setConf(ndgb.getConf());
			ng.setGm(ndgb.getGm());
			ng.setGearMotor(ndgb.getGearMotor());
			ng.setQuantity(ndgb.getQuantity());
			ng.setApplication(ndgb.getApplication());
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setGearProduct(ndgb.getGearProduct());
			ng.setModel(ndgb.getModel());
			ng.setSelectModel(ndgb.getSelectModel());
            ng.setMotorType(ndgb.getMotorType());
			ng.setMotorKW(ndgb.getMotorKW());
			ng.setPole(ndgb.getPole());
			ng.setPhase(ndgb.getPhase());
			ng.setVoltage(ndgb.getVoltage());
			ng.setHertz(ndgb.getHertz());
			ng.setRpm(ndgb.getRpm());
			ng.setProtectionGrade(ndgb.getProtectionGrade());
			ng.setInternationalStd(ndgb.getInternationalStd());
			ng.setAccessMotor(ndgb.getAccessMotor());
			ng.setMotorBrake(ndgb.getMotorBrake());
			ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
			ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
			ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
			ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
            ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
			ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
			ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
			ng.setDirSssAxial(ndgb.getDirSssAxial());
			ng.setRatioRequired(ndgb.getRatioRequired());
			ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
			ng.setPresetTorque(ndgb.getPresetTorque());
			ng.setMount(ndgb.getMount());
			ng.setMount2(ndgb.getMount2());
			ng.setMount3(ndgb.getMount3());
			ng.setIshaft(ndgb.getIshaft());
			ng.setOshaft(ndgb.getOshaft());
			ng.setOshaftInc(ndgb.getOshaftInc());
			ng.setDeg(ndgb.getDeg());
			ng.setTowards(ndgb.getTowards());
			ng.setOshafty(ndgb.getOshafty());
			ng.setOshafty2(ndgb.getOshafty2());
			ng.setBackStop(ndgb.getBackStop());
			ng.setRot(ndgb.getRot());
			ng.setDirHssAxial(ndgb.getDirHssAxial());
			ng.setRequiredSreviceFactor(ndgb.getRequiredSreviceFactor());
			ng.setCompetitorBrand(ndgb.getCompetitorBrand());
			ng.setCompetitorModel(ndgb.getCompetitorModel());
			ng.setTargetPrice(ndgb.getTargetPrice());
			ng.setDimension(ndgb.getDimension());
			ng.setCompititorcode(ndgb.getSsCode());
			ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
			ng.setInverter(ndgb.getInverter());
			ng.setTab(2);
			ng.setRequiredSreviceFactor(ndgb.getServFactor());
			ng.setOshaftInc2(ndgb.getOshaftInc2());
			ng.setOshaftInc1(ndgb.getOshaftInc1());
			ng.setOther(ndgb.getOther());
			ng.setOthersPlz(ndgb.getOthersPlz());
            compitor.add(ng);

		}
		return compitor;
	}

	
	
	
	@RequestMapping(value = "/saveCompetitorReplaceDataTemp", method = RequestMethod.POST)
public @ResponseBody boolean CompitiorReplaceCaseTemp(MultipartHttpServletRequest req)
	throws Exception {

		String ss=req.getParameter("formData");
		String description1=req.getParameter("fieldData");
		String ss1=req.getParameter("gridData");
				org.json.JSONArray gData=new org.json.JSONArray(ss1);
			    //List<MultipartFile> mpf=req.getFiles("motorFile");
			    
			   // String imagepath1=this.getImagepathOfCompititorTemp(mpf);
				
				String imagepath1=null;
				  String gik=req.getParameter("motorFile");
				   if(gik!=null)
				   {
				    imagepath1=gik.replaceAll("\"", "");
				   	    
				   }
	

	int status = 0;
	DescriptionCaseBean description = null;
	TreeMap<Integer, String> al = null;

	HttpSession hs = req.getSession();
	String user=(String)hs.getAttribute("user");
	int tab=2;
	
	Calendar now = Calendar.getInstance();
	int year = now.get(Calendar.YEAR);
	int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
	int day = now.get(Calendar.DAY_OF_MONTH);
	int hour = now.get(Calendar.HOUR_OF_DAY);
	int minute = now.get(Calendar.MINUTE);
	int second = now.get(Calendar.SECOND);
	int millis = now.get(Calendar.MILLISECOND);
    String storedtime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
	
	if (hs.getAttribute("image") != null) {
		al = (TreeMap<Integer, String>) hs.getAttribute("image");
	}

	/*JSONObject caseform = (JSONObject) jsonData.get("formData");
	JSONArray casegrid = (JSONArray) jsonData.get("gridData");
	JSONObject casedescription = (JSONObject) jsonData.get("fieldData");
*/
	MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);
	if(!description1.equalsIgnoreCase("undefined")) {
		status = 1;
		description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
	}

	String k = null;
	ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
	for (int i = 0; i < gData.length(); i++) {

		NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
				NewSelectionGridFormBean.class);

		String a[] = newcasegrid.getCompSSCCode();
		if (a != null) {
			for (int m = 0; m < a.length; m++) {
				if (m == 0) {
					k = a[m];
				}

				if (m > 0) {

					k = k + ',' + a[m];
				}

			}

		}
		if (al != null) {
			String imagepath = (String) al.get(i + 1);
			newcasegrid.setImagepath(imagepath);
		}
		newcasegrid.setSsCode(k);

		newselctiongrid.add(newcasegrid);
	}

	CaseSummaryTemp newselection = this.insetDataIntoNewSElctionCaseMOdelTempModified(maincase, description, status, imagepath1,maincase.getImages());
	newselection.setStoredtime(storedtime);
	newselection.setTab(tab);
	newselection.setUsername(user);
	newselection.setCasetype("Compititor Replace Type");
	
	if(maincase.getTempid()!=null)
	{
		
		masterservice.deleteFromCompititorReplceTemp(maincase.getTempid(),tab);
	}

	masterservice.saveNewSelectionCaseTemp(newselection);
	List<CompititorGridTemp> newselectiongrid = this.insertDataIntoCompititorGridTemp(newselctiongrid,storedtime,tab,user,newselection.getCaseRef());
	masterservice.saveCompitorGridTemp(newselectiongrid);

	hs.removeAttribute("image");

	return true;

 

}


	
	
	private ArrayList<CompititorGridTemp> insertDataIntoCompititorGridTemp(
			ArrayList<NewSelectionGridFormBean> newselctiongrid,String storedtime,int tab,String user,String caseref) {

		String newselectionkey = masterservice.getNewSelectionTempKey();
		ArrayList<CompititorGridTemp> al = new ArrayList<CompititorGridTemp>();
		for (NewSelectionGridFormBean ndgb : newselctiongrid) {
			CompititorGridTemp ng = new CompititorGridTemp();
			ng.setToken(newselectionkey);
			ng.setCaseref(caseref);
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setImagepath(ndgb.getImagepath());
			ng.setConf(ndgb.getConf());
			ng.setGm(ndgb.getGm());
			ng.setGearMotor(ndgb.getGearMotor());
			ng.setQuantity(ndgb.getQuantity());
			ng.setApplication(ndgb.getApplication());
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setGearProduct(ndgb.getGearProduct());
			ng.setModel(ndgb.getModel());
			ng.setSelectModel(ndgb.getSelectModel());
		    ng.setMotorType(ndgb.getMotorType());
			ng.setMotorKW(ndgb.getMotorKW());
			ng.setPole(ndgb.getPole());
			ng.setPhase(ndgb.getPhase());
			ng.setVoltage(ndgb.getVoltage());
			ng.setHertz(ndgb.getHertz());
			ng.setRpm(ndgb.getRpm());
			ng.setProtectionGrade(ndgb.getProtectionGrade());
			ng.setInternationalStd(ndgb.getInternationalStd());
			ng.setAccessMotor(ndgb.getAccessMotor());
			ng.setMotorBrake(ndgb.getMotorBrake());
			ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
			ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
			ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
			ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
			ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
			ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
			ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
			ng.setDirSssAxial(ndgb.getDirSssAxial());
			ng.setRatioRequired(ndgb.getRatioRequired());
			ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
			ng.setPresetTorque(ndgb.getPresetTorque());
			ng.setMount(ndgb.getMount());
			ng.setMount2(ndgb.getMount2());
			ng.setMount3(ndgb.getMount3());
			ng.setIshaft(ndgb.getIshaft());
			ng.setOshaft(ndgb.getOshaft());
			ng.setOshaftInc(ndgb.getOshaftInc());
			ng.setDeg(ndgb.getDeg());
			ng.setTowards(ndgb.getTowards());
			ng.setOshafty(ndgb.getOshafty());
			ng.setOshafty2(ndgb.getOshafty2());
			ng.setBackStop(ndgb.getBackStop());
			ng.setRot(ndgb.getRot());
			ng.setDirHssAxial(ndgb.getDirHssAxial());
			ng.setStoredtime(storedtime);
			ng.setTab(tab);
			ng.setUsername(user);
			ng.setCompititorcode(ndgb.getSsCode());
			ng.setRequiredSreviceFactor(ndgb.getServFactor());
            ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
            ng.setInverter(ndgb.getInverter());
			ng.setRpm(ndgb.getRpm());
			ng.setOshaftInc2(ndgb.getOshaftInc2());
			ng.setOshaftInc1(ndgb.getOshaftInc1());
			ng.setOther(ndgb.getOther());
			ng.setOthersPlz(ndgb.getOthersPlz());
			ng.setCompetitorBrand(ndgb.getCompetitorBrand());
			ng.setCompetitorModel(ndgb.getCompetitorModel());
			ng.setTargetPrice(ndgb.getTargetPrice());
			ng.setDimension(ndgb.getDimension());
			 
			
			
			al.add(ng);

		}
		// TODO Auto-generated method stub
		return al;
	}

	
	
	
	@RequestMapping(value = "/loadingCasessTab5", method = RequestMethod.POST)
	public @ResponseBody JSONObject loadingCasessTab5(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
		int day = now.get(Calendar.DAY_OF_MONTH);
		int hour = now.get(Calendar.HOUR_OF_DAY);
		int minute = now.get(Calendar.MINUTE);
		int second = now.get(Calendar.SECOND);
		int millis = now.get(Calendar.MILLISECOND);
	     String   time=day+"-"+month+"-"+year;
		
		
		try
		{
		JSONObject mainobject=new JSONObject();
		JSONArray gridarray=new JSONArray();
		JSONArray manincaseobjectarray=new JSONArray();
		JSONArray descriptionobjectarray=new JSONArray();
		
		HttpSession hs=req.getSession();
		String username=(String)hs.getAttribute("user");
		int tab=5;
		
		String storedtime=masterservice.getStoredTime(username,tab);
		List<CaseSummaryTemp> newselectiontemp=masterservice.getDeatilsFromNewSelectionTemp(storedtime);
		 
		
		for(CaseSummaryTemp data:newselectiontemp)
 {
				JSONObject maincaseobject = new JSONObject();
				JSONObject descriptioncaseobject = new JSONObject();
				maincaseobject.put("tempid",data.getNewselectionid());
				maincaseobject.put("customer", data.getCustomer());
				maincaseobject.put("customerCode", data.getCustomerCode());
				maincaseobject.put("caseRef", data.getCaseRef());
				maincaseobject.put("contactPerson", data.getContactPerson());
				maincaseobject.put("phone", data.getPhone());
				maincaseobject.put("address", data.getAddress());
				maincaseobject.put("customerRef", data.getCustomerRef());
				maincaseobject.put("images", data.getImages());
				maincaseobject.put("assigned", data.getAssigned());
				maincaseobject.put("custStatus", data.getCustStatus());
				maincaseobject.put("casePriority",data.getCasePriority());
				maincaseobject.put("orginReq",data.getOrginReq());
				

				Date d1 = data.getDateExp();
				if (d1 != null) {
					d1 = data.getDateExp();
					String k1 = DateUtils.formatDate(data.getDateExp(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					
					maincaseobject.put("dateExp", k1);

				}
				Date d2 = data.getDtCase();
				if (d2 != null) {
					String k1 = DateUtils.formatDate(data.getDtCase(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					maincaseobject.put("dateCase", k1);

				}
				Date d3 = data.getDtReport();
				if (d3 != null) {
					String k1 = DateUtils.formatDate(d3, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dtReport", k1);

				}
				Date d4 = data.getDtReportBy();
				if (d4 != null) {
					String k1 = DateUtils.formatDate(d4, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dateRepot", k1);

				}
				
				descriptioncaseobject.put("description",data.getDescription());

				manincaseobjectarray.add(maincaseobject);
				descriptionobjectarray.add(descriptioncaseobject);
				tab = data.getTab();

			}
		
		
		
		if(manincaseobjectarray.size()==0)
		{
			JSONObject maincaseobject = new JSONObject();
			JSONObject descriptioncaseobject = new JSONObject();
			descriptioncaseobject.put("dtReport",time);
			descriptionobjectarray.add(descriptioncaseobject);
			
			
			 HttpSession session=req.getSession();
			 String countryCode=(String)session.getAttribute("countrycode");
			 String usernasme=(String)session.getAttribute("user");

			maincaseobject.put("caseRef",bsscommonService.getSequenceNumber("E",countryCode,usernasme));
			//maincaseobject.put("assigned","Sale");
			maincaseobject.put("custStatus","Open");
			maincaseobject.put("dateCase",time);
			maincaseobject.put("casePriority","Normal");
			maincaseobject.put("orginReq",0);
			manincaseobjectarray.add(maincaseobject);
		}
		
		mainobject.put("maincaseform",manincaseobjectarray);
		mainobject.put("description",descriptionobjectarray);
		
		
		
		
		
		
		
		
	
		List<TechGridTemp> newselectiongridtemp=masterservice.getDetailsFromTechGridTemp(storedtime);
		  
		int i=1;
		for(TechGridTemp nst:newselectiongridtemp)
		{
			 
			
			
			JSONObject gridobject = new JSONObject();
			gridobject.put("gm",nst.getGm());
			gridobject.put("id",i);
			gridobject.put("gearMotor",nst.getGearMotor());
			gridobject.put("quantity",nst.getQuantity());
			gridobject.put("application",nst.getApplication());
			gridobject.put("ambientTemp",nst.getAmbientTemp());
			gridobject.put("gearProduct",nst.getGearProduct());
			gridobject.put("model",nst.getModel());
			gridobject.put("selectModel",nst.getSelectModel());
			gridobject.put("servFactor",nst.getServFactor());
			gridobject.put("motorType",nst.getMotorType());
			gridobject.put("accessMotor",nst.getAccessMotor());
			gridobject.put("motorKW",nst.getMotorKW().toString());
			gridobject.put("pole",nst.getPole());
			gridobject.put("phase",nst.getPhase());
			gridobject.put("voltage",nst.getVoltage());
			gridobject.put("hertz",nst.getHertz());
			gridobject.put("rpm", nst.getRpm());
			gridobject.put("protectionGrade",nst.getProtectionGrade());
			gridobject.put("internationalStd",nst.getInternationalStd());
			gridobject.put("motorBrake",nst.getMotorBrake());
			gridobject.put("motorBrakeVolt",nst.getMotorBrakeVolt());
			gridobject.put("hssRadialLoadKn",nst.getHssRadialLoadKn());
			gridobject.put("hssRadialLoadMm",nst.getHssRadialLoadMm());
			gridobject.put("hssAxialLoadKn",nst.getHssAxialLoadKn());
			gridobject.put("dirAxial",nst.getDirAxial());
			gridobject.put("sssRadialLoadKn",nst.getSssRadialLoadKn());
			gridobject.put("sssRadialLoadMm",nst.getSssRadialLoadMm());
			gridobject.put("sssAxialLoadKn",nst.getSssAxialLoadKn());
			gridobject.put("dirSssAxial", nst.getDirSssAxial());
			gridobject.put("ratioRequired",nst.getRatioRequired());
			gridobject.put("requiredOutputSpeed",nst.getRequiredOutputSpeed());
			gridobject.put("presetTorque",nst.getPresetTorque());
			gridobject.put("conf",nst.getConf());
			gridobject.put("mount",nst.getMount());
			//gridobject.put("mount2",nst.getMount2());
			//gridobject.put("mount3",nst.getMount3());
			gridobject.put("ishaft",nst.getIshaft());
			gridobject.put("oshaft",nst.getOshaft());
			gridobject.put("oshaftInc",nst.getOshaftInc());
			gridobject.put("deg",nst.getDeg());
			gridobject.put("towards",nst.getTowards());
			gridobject.put("oshafty",nst.getOshafty());
			gridobject.put("oshafty2",nst.getOshafty2());
			gridobject.put("backStop",nst.getBackStop());
			gridobject.put("rot", nst.getRot());
			gridobject.put("dirHssAxial",nst.getDirHssAxial());
			String ssccodes=nst.getSscCode();
			if(ssccodes!=null)
			{
			String sscode[]=ssccodes.split(",");
			gridobject.put("sscCode",sscode);
			}
			
			Boolean b=true;
			Boolean b1=false;
			 
			 
			gridobject.put("mount2",nst.getMount2());
		 	
			if(nst.getOshaftInc2().equalsIgnoreCase("true"))
			{
				gridobject.put("oshaftInc2",b);
				
			}
			
			else
			{
				gridobject.put("oshaftInc2",b1);
				
			}
			
			if(nst.getOshaftInc1().equalsIgnoreCase("true"))
			{
				 
				 gridobject.put("oshaftInc1",b);
				 
				}
			
			else
			{
				
				 gridobject.put("oshaftInc1",b1);
			}
			
			//gridobject.put("oshaftInc2",nst.getOshaftInc2());
			//gridobject.put("oshaftInc1",nst.getOshaftInc1());
			gridobject.put("other",nst.getOther());
			gridobject.put("othersPlz",nst.getOthersPlz());
			
			
			
			gridobject.put("docFile",nst.getImagepath());
			gridobject.put("gridid",i);
			gridobject.put("requiredSreviceFactor",nst.getRequiredSreviceFactor());
			gridobject.put("gearhead",nst.getGearhead());
			gridobject.put("motorSerial",nst.getMotorSerial());
			gridobject.put("shiMfg",nst.getShiMfg());
			gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
			gridobject.put("inverter",nst.getInverter());
			
			gridobject.put("accessMotor",nst.getAccessMotor());
			
			
			
		 
			
			 	gridarray.add(gridobject);
			
			i++;
		
			
			 	
		}
		
	     
		
		if(gridarray.size()==0)
		{
			
			JSONObject gridobject = new JSONObject();
			gridobject.put("id",1);
			mainobject.put("value",0);
			gridarray.add(gridobject);
		}
		
		
	 
		
	 
		
		
		
		
		
		
		mainobject.put("griddata",gridarray);
		mainobject.put("tab",5);
		 
		
		
		return  mainobject;
		
		}catch(Exception  e)
		{
			logger.error(e.getMessage()+"  Error in  the line  ");
			return null;
		}
	}
	

	
	
	@RequestMapping(value = "/loadingCasessTab4", method = RequestMethod.POST)
	public @ResponseBody JSONObject loadingCasessTab4(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
	
		
		JSONObject mainobject=new JSONObject();
		JSONArray gridarray=new JSONArray();
		JSONArray manincaseobjectarray=new JSONArray();
		JSONArray descriptionobjectarray=new JSONArray();
		
		HttpSession hs=req.getSession();
		String username=(String)hs.getAttribute("user");
		int tab=4;
		
		String storedtime=masterservice.getStoredTime(username,tab);
		List<CaseSummaryTemp> newselectiontemp=masterservice.getDeatilsFromNewSelectionTemp(storedtime);
		 
		
		for(CaseSummaryTemp data:newselectiontemp)
 {
				JSONObject maincaseobject = new JSONObject();
				JSONObject descriptioncaseobject = new JSONObject();
				maincaseobject.put("tempid",data.getNewselectionid());
				maincaseobject.put("customer", data.getCustomer());
				maincaseobject.put("customerCode", data.getCustomerCode());
				maincaseobject.put("caseRef", data.getCaseRef());
				maincaseobject.put("contactPerson", data.getContactPerson());
				maincaseobject.put("phone", data.getPhone());
				maincaseobject.put("address", data.getAddress());
				maincaseobject.put("customerRef", data.getCustomerRef());
				maincaseobject.put("images", data.getImages());
				maincaseobject.put("assigned", data.getAssigned());
				maincaseobject.put("custStatus", data.getCustStatus());
				maincaseobject.put("casePriority",data.getCasePriority());
				maincaseobject.put("orginReq",data.getOrginReq());


				Date d1 = data.getDateExp();
				if (d1 != null) {
					d1 = data.getDateExp();
					String k1 = DateUtils.formatDate(data.getDateExp(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					
					maincaseobject.put("dateExp", k1);

				}
				Date d2 = data.getDtCase();
				if (d2 != null) {
					String k1 = DateUtils.formatDate(data.getDtCase(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					maincaseobject.put("dateCase", k1);

				}
				Date d3 = data.getDtReport();
				if (d3 != null) {
					String k1 = DateUtils.formatDate(d3, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dtReport", k1);

				}
				Date d4 = data.getDtReportBy();
				if (d4 != null) {
					String k1 = DateUtils.formatDate(d4, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dateRepot", k1);

				}
				
				descriptioncaseobject.put("description",data.getDescription());

				manincaseobjectarray.add(maincaseobject);
				descriptionobjectarray.add(descriptioncaseobject);
				tab = data.getTab();

			}
		
		
		
		if(manincaseobjectarray.size()==0)
		{
			JSONObject maincaseobject = new JSONObject();
			
			
			JSONObject descriptioncaseobject = new JSONObject();
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			String   time=day+"-"+month+"-"+year;
			descriptioncaseobject.put("dtReport",time);
			descriptionobjectarray.add(descriptioncaseobject);
			
			maincaseobject.put("assigned","Sales");
			maincaseobject.put("custStatus","Open");
			maincaseobject.put("casePriority","Normal");
			maincaseobject.put("orginReq",0);
	
			//maincaseobject.put("assigned","Open");
			//maincaseobject.put("custStatus","Sales");
			maincaseobject.put("dateCase",time);
			
			 HttpSession session=req.getSession();
			 String countryCode=(String)session.getAttribute("countrycode");

			 String usernasme=(String)session.getAttribute("user");
			maincaseobject.put("caseRef",bsscommonService.getSequenceNumber("E",countryCode,usernasme));
			manincaseobjectarray.add(maincaseobject);
		}
		
		mainobject.put("maincaseform",manincaseobjectarray);
		mainobject.put("description",descriptionobjectarray);
		
		
		
		
		
		
		
		
	
		List<SparePartGridTemp> newselectiongridtemp=masterservice.getDetailsFromSparePartGridTemp(storedtime);
		  
		int i=1;
		for(SparePartGridTemp nst:newselectiongridtemp)
		{
			 
			
			
			JSONObject gridobject = new JSONObject();
			gridobject.put("gm",nst.getGm());
			gridobject.put("id",i);
			gridobject.put("gearMotor",nst.getGearMotor());
			gridobject.put("quantity",nst.getQuantity());
			gridobject.put("application",nst.getApplication());
			gridobject.put("ambientTemp",nst.getAmbientTemp());
			gridobject.put("gearProduct",nst.getGearProduct());
			gridobject.put("model",nst.getModel());
			gridobject.put("selectModel",nst.getSelectModel());
			gridobject.put("servFactor",nst.getServFactor());
			gridobject.put("motorType",nst.getMotorType());
			gridobject.put("motorKW",nst.getMotorKW().toString());
			gridobject.put("pole",nst.getPole());
			gridobject.put("phase",nst.getPhase());
			gridobject.put("voltage",nst.getVoltage());
			gridobject.put("hertz",nst.getHertz());
			gridobject.put("accessMotor", nst.getAccessMotor());
			gridobject.put("rpm", nst.getRpm());
			gridobject.put("protectionGrade",nst.getProtectionGrade());
			gridobject.put("internationalStd",nst.getInternationalStd());
			gridobject.put("motorBrake",nst.getMotorBrake());
			gridobject.put("motorBrakeVolt",nst.getMotorBrakeVolt());
			gridobject.put("hssRadialLoadKn",nst.getHssRadialLoadKn());
			gridobject.put("hssRadialLoadMm",nst.getHssRadialLoadMm());
			gridobject.put("hssAxialLoadKn",nst.getHssAxialLoadKn());
			gridobject.put("dirAxial",nst.getDirAxial());
			gridobject.put("sssRadialLoadKn",nst.getSssRadialLoadKn());
			gridobject.put("sssRadialLoadMm",nst.getSssRadialLoadMm());
			gridobject.put("sssAxialLoadKn",nst.getSssAxialLoadKn());
			gridobject.put("dirSssAxial", nst.getDirSssAxial());
			gridobject.put("ratioRequired",nst.getRatioRequired());
			gridobject.put("requiredOutputSpeed",nst.getRequiredOutputSpeed());
			gridobject.put("presetTorque",nst.getPresetTorque());
			gridobject.put("conf",nst.getConf());
			gridobject.put("mount",nst.getMount());
			//gridobject.put("mount2",nst.getMount2());
			//gridobject.put("mount3",nst.getMount3());
			gridobject.put("ishaft",nst.getIshaft());
			gridobject.put("oshaft",nst.getOshaft());
			gridobject.put("oshaftInc",nst.getOshaftInc());
			gridobject.put("deg",nst.getDeg());
			gridobject.put("towards",nst.getTowards());
			gridobject.put("oshafty",nst.getOshafty());
			gridobject.put("oshafty2",nst.getOshafty2());
			gridobject.put("backStop",nst.getBackStop());
			gridobject.put("rot", nst.getRot());
			gridobject.put("dirHssAxial",nst.getDirHssAxial());
			String ssccodes=nst.getSscCode();
			if(ssccodes!=null)
			{
			String sscode[]=ssccodes.split(",");
			gridobject.put("sscCode",sscode);
			}
			
			gridobject.put("docFile",nst.getImagepath());
			gridobject.put("gridid",i);
			
			gridobject.put("spare",nst.getSpare());
			gridobject.put("descript",nst.getDescript());
			gridobject.put("qty",nst.getQty());
			gridobject.put("spare1",nst.getSpare1());
			gridobject.put("descript1",nst.getDescript1());
			gridobject.put("qty1",nst.getQty1());
			gridobject.put("spare2",nst.getSpare2());
			gridobject.put("descript2",nst.getDescript2());
			gridobject.put("qty2",nst.getQty2());
			gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
			gridobject.put("inverter",nst.getInverter());
			
			gridobject.put("gearhead",nst.getGearhead());
			gridobject.put("motorSerial",nst.getMotorSerial());
			gridobject.put("shiMfg",nst.getShiMfg());
			gridobject.put("accessMotor",nst.getAccessMotor());
			Boolean b=true;
			Boolean b1=false;
			

		/*	if(nst.getMount2().equalsIgnoreCase("true"))
			{
			gridobject.put("mount2",b);
			}
			else
			{
			gridobject.put("mount2", b1);
			}
			if(nst.getMount3().equalsIgnoreCase("true"))
			{
				gridobject.put("mount3",b);
				
			}
			else
			{
				gridobject.put("mount3",b1);
			}
			
			if(nst.getOshaftInc2().equalsIgnoreCase("true"))
			{
				gridobject.put("oshaftInc2",b);
				
			}
			
			else
			{
				gridobject.put("oshaftInc2",b1);
				
			}
			
			if(nst.getOshaftInc1().equalsIgnoreCase("true"))
			{
				 
				 gridobject.put("oshaftInc1",b);
				 
				}
			
			else
			{
				
				 gridobject.put("oshaftInc1",b1);
			}
			*/
						
			//gridobject.put("oshaftInc2",nst.getOshaftInc2());
			//gridobject.put("oshaftInc1",nst.getOshaftInc1());
			gridobject.put("other",nst.getOther());
			gridobject.put("othersPlz",nst.getOthersPlz());
			
			
		
			 
			
			
			
			
			 	gridarray.add(gridobject);
			
			i++;
		
			
			
			 	
		}
		
	     
		
		if(gridarray.size()==0)
		{
			JSONObject gridobject = new JSONObject();
			gridobject.put("id",1);
			mainobject.put("value",0);
			gridarray.add(gridobject);
		}
		
		
	 
		
	 
		
		
		
		
		
		
		mainobject.put("griddata",gridarray);
		mainobject.put("tab",4);
		 
		
		
		return  mainobject;
		
		
	 
	}
	

	
	
	
	@RequestMapping(value = "/loadingCasessTab3", method = RequestMethod.POST)
	public @ResponseBody JSONObject loadingCasessTab3(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		try
		{
		JSONObject mainobject=new JSONObject();
		JSONArray gridarray=new JSONArray();
		JSONArray manincaseobjectarray=new JSONArray();
		JSONArray descriptionobjectarray=new JSONArray();
		
		HttpSession hs=req.getSession();
		String username=(String)hs.getAttribute("user");
		int tab=3;
		
		String storedtime=masterservice.getStoredTime(username,tab);
		List<CaseSummaryTemp> newselectiontemp=masterservice.getDeatilsFromNewSelectionTemp(storedtime);
		 
		
		for(CaseSummaryTemp data:newselectiontemp)
 {
				JSONObject maincaseobject = new JSONObject();
				JSONObject descriptioncaseobject = new JSONObject();
				maincaseobject.put("tempid",data.getNewselectionid());
				maincaseobject.put("customer", data.getCustomer());
				maincaseobject.put("customerCode", data.getCustomerCode());
				maincaseobject.put("caseRef", data.getCaseRef());
				maincaseobject.put("contactPerson", data.getContactPerson());
				maincaseobject.put("phone", data.getPhone());
				maincaseobject.put("address", data.getAddress());
				maincaseobject.put("customerRef", data.getCustomerRef());
				maincaseobject.put("images", data.getImages());
				maincaseobject.put("assigned", data.getAssigned());
				maincaseobject.put("custStatus", data.getCustStatus());
				maincaseobject.put("casePriority",data.getCasePriority());
				maincaseobject.put("orginReq",data.getOrginReq());


				Date d1 = data.getDateExp();
				if (d1 != null) {
					d1 = data.getDateExp();
					String k1 = DateUtils.formatDate(data.getDateExp(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					
					maincaseobject.put("dateExp", k1);

				}
				Date d2 = data.getDtCase();
				if (d2 != null) {
					String k1 = DateUtils.formatDate(data.getDtCase(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					maincaseobject.put("dateCase", k1);

				}
				Date d3 = data.getDtReport();
				if (d3 != null) {
					String k1 = DateUtils.formatDate(d3, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dtReport", k1);

				}
				Date d4 = data.getDtReportBy();
				if (d4 != null) {
					String k1 = DateUtils.formatDate(d4, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dateRepot", k1);

				}
				
				descriptioncaseobject.put("description",data.getDescription());

				manincaseobjectarray.add(maincaseobject);
				descriptionobjectarray.add(descriptioncaseobject);
				tab = data.getTab();

			}
		
		
		
		if(manincaseobjectarray.size()==0)
		{
			JSONObject maincaseobject = new JSONObject();
			
			
			JSONObject descriptioncaseobject = new JSONObject();
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			String   time=day+"-"+month+"-"+year;
			descriptioncaseobject.put("dtReport",time);
			descriptionobjectarray.add(descriptioncaseobject);
			
			maincaseobject.put("assigned","Sales");
			maincaseobject.put("custStatus","Open");
			maincaseobject.put("casePriority","Normal");
			maincaseobject.put("orginReq",0);
			//maincaseobject.put("assigned","Open");
			//maincaseobject.put("custStatus","Sales");
			maincaseobject.put("dateCase",time);
			
			 HttpSession session=req.getSession();
			 String countryCode=(String)session.getAttribute("countrycode");

			 String usernasme=(String)session.getAttribute("user");
			maincaseobject.put("caseRef",bsscommonService.getSequenceNumber("E",countryCode,usernasme));
			manincaseobjectarray.add(maincaseobject);
		}
		
		mainobject.put("maincaseform",manincaseobjectarray);
		mainobject.put("description",descriptionobjectarray);
		
		
		
		
		
		
		
		
	
		List<SumitomoReplaceTempGrid> newselectiongridtemp=masterservice.getDetailsFromSumititoAndReplaceTemp(storedtime);
		  
		int i=1;
		for(SumitomoReplaceTempGrid nst:newselectiongridtemp)
		{
			 
			
			
			JSONObject gridobject = new JSONObject();
			gridobject.put("gm",nst.getGm());
			gridobject.put("id",i);
			gridobject.put("gearMotor",nst.getGearMotor());
			gridobject.put("quantity",nst.getQuantity());
			gridobject.put("application",nst.getApplication());
			gridobject.put("ambientTemp",nst.getAmbientTemp());
			gridobject.put("gearProduct",nst.getGearProduct());
			gridobject.put("model",nst.getModel());
			gridobject.put("accessMotor", nst.getAccessMotor());
			gridobject.put("selectModel",nst.getSelectModel());
		    gridobject.put("servFactor",nst.getServFactor());
			gridobject.put("motorType",nst.getMotorType().toString());
			gridobject.put("motorKW",nst.getMotorKW());
			gridobject.put("pole",nst.getPole());
			gridobject.put("phase",nst.getPhase());
			gridobject.put("voltage",nst.getVoltage());
			gridobject.put("hertz",nst.getHertz());
			gridobject.put("rpm", nst.getRpm());
			gridobject.put("protectionGrade",nst.getProtectionGrade());
			gridobject.put("internationalStd",nst.getInternationalStd());
			gridobject.put("motorBrake",nst.getMotorBrake());
			gridobject.put("motorBrakeVolt",nst.getMotorBrakeVolt());
			gridobject.put("hssRadialLoadKn",nst.getHssRadialLoadKn());
			gridobject.put("hssRadialLoadMm",nst.getHssRadialLoadMm());
			gridobject.put("hssAxialLoadKn",nst.getHssAxialLoadKn());
			//gridobject.put("dirAxial",nst.getDirAxial());
			gridobject.put("sssRadialLoadKn",nst.getSssRadialLoadKn());
			gridobject.put("sssRadialLoadMm",nst.getSssRadialLoadMm());
			gridobject.put("sssAxialLoadKn",nst.getSssAxialLoadKn());
			gridobject.put("dirSssAxial", nst.getDirSssAxial());
			gridobject.put("ratioRequired",nst.getRatioRequired());
			gridobject.put("requiredOutputSpeed",nst.getRequiredOutputSpeed());
			gridobject.put("presetTorque",nst.getPresetTorque());
			gridobject.put("conf",nst.getConf());
			gridobject.put("mount",nst.getMount());
			//gridobject.put("mount2",nst.getMount2());
			//gridobject.put("mount3",nst.getMount3());
			gridobject.put("ishaft",nst.getIshaft());
			gridobject.put("oshaft",nst.getOshaft());
			gridobject.put("oshaftInc",nst.getOshaftInc());
			gridobject.put("deg",nst.getDeg());
			gridobject.put("towards",nst.getTowards());
			gridobject.put("oshafty",nst.getOshafty());
			gridobject.put("oshafty2",nst.getOshafty2());
			gridobject.put("backStop",nst.getBackStop());
			gridobject.put("rot", nst.getRot());
			gridobject.put("dirHssAxial",nst.getDirHssAxial());
			String compititorcodes=nst.getSscCode();
			if(compititorcodes!=null)
			{
			String sscode[]=compititorcodes.split(",");
			gridobject.put("sscCode",sscode);
			}
			gridobject.put("gearhead",nst.getGearhead());
			gridobject.put("motorSerial",nst.getMotorSerial());
			gridobject.put("shiMfg",nst.getShiMfg());
		
			
			//gridobject.put(key, value);
			gridobject.put("docFile",nst.getImagepath());
			gridobject.put("gridid",i);
			
			gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
			gridobject.put("inverter",nst.getInverter());
			
Boolean b=true;
Boolean b1=false;
			 
			gridobject.put("mount2",nst.getMount2());
			 
			 
			
			if(nst.getOshaftInc2().equalsIgnoreCase("true"))
			{
				gridobject.put("oshaftInc2",b);
				
			}
			
			else
			{
				gridobject.put("oshaftInc2",b1);
				
			}
			
			if(nst.getOshaftInc1().equalsIgnoreCase("true"))
			{
				 
				 gridobject.put("oshaftInc1",b);
				 
				}
			
			else
			{
				
				 gridobject.put("oshaftInc1",b1);
			}
			
			//gridobject.put("oshaftInc2",nst.getOshaftInc2());
			//gridobject.put("oshaftInc1",nst.getOshaftInc1());
			gridobject.put("other",nst.getOther());
			gridobject.put("othersPlz",nst.getOthersPlz());
			
			gridobject.put("accessMotor",nst.getAccessMotor());
			gridobject.put("dimension",nst.getDimension());
			

			 gridarray.add(gridobject);
			 	
			 	
			 	
			 	
			 	
			 	
			    
			
			i++;
		
			
			
			 	
		}
		
		
		if(gridarray.size()==0)
		{
			JSONObject gridobject = new JSONObject();
			gridobject.put("id",1);
			mainobject.put("value",0);
			gridarray.add(gridobject);
		}
		
		
	 
		
	 
		
		
		
		
		
		
		mainobject.put("griddata",gridarray);
		mainobject.put("tab",3);
		 
		
		
		return  mainobject;
		
		}catch(Exception  e)
		{
			logger.error(e.getMessage()+"  Error in  the line  ");
			return null;
		}
	
		
		
	}
		

	
	
	@RequestMapping(value = "/loadingCasessTab2", method = RequestMethod.POST)
	public @ResponseBody JSONObject loadingCasessTab2(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		try
		{
		JSONObject mainobject=new JSONObject();
		JSONArray gridarray=new JSONArray();
		JSONArray manincaseobjectarray=new JSONArray();
		JSONArray descriptionobjectarray=new JSONArray();
		
		HttpSession hs=req.getSession();
		String username=(String)hs.getAttribute("user");
		int tab=2;
		
		String storedtime=masterservice.getStoredTime(username,tab);
		List<CaseSummaryTemp> newselectiontemp=masterservice.getDeatilsFromNewSelectionTemp(storedtime);
		 
		
		for(CaseSummaryTemp data:newselectiontemp)
 {
				JSONObject maincaseobject = new JSONObject();
				JSONObject descriptioncaseobject = new JSONObject();
				maincaseobject.put("tempid",data.getNewselectionid());
				maincaseobject.put("customer", data.getCustomer());
				maincaseobject.put("customerCode", data.getCustomerCode());
				maincaseobject.put("caseRef", data.getCaseRef());
				maincaseobject.put("contactPerson", data.getContactPerson());
				maincaseobject.put("phone", data.getPhone());
				maincaseobject.put("address", data.getAddress());
				maincaseobject.put("customerRef", data.getCustomerRef());
				maincaseobject.put("images", data.getImages());
				maincaseobject.put("assigned", data.getAssigned());
				maincaseobject.put("custStatus", data.getCustStatus());

				maincaseobject.put("casePriority",data.getCasePriority());
				maincaseobject.put("orginReq",data.getOrginReq());

				Date d1 = data.getDateExp();
				if (d1 != null) {
					d1 = data.getDateExp();
					String k1 = DateUtils.formatDate(data.getDateExp(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					
					maincaseobject.put("dateExp", k1);

				}
				Date d2 = data.getDtCase();
				if (d2 != null) {
					String k1 = DateUtils.formatDate(data.getDtCase(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					maincaseobject.put("dateCase", k1);

				}
				Date d3 = data.getDtReport();
				if (d3 != null) {
					String k1 = DateUtils.formatDate(d3, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dtReport", k1);

				}
				Date d4 = data.getDtReportBy();
				if (d4 != null) {
					String k1 = DateUtils.formatDate(d4, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dateRepot", k1);

				}
				
				descriptioncaseobject.put("description",data.getDescription());

				manincaseobjectarray.add(maincaseobject);
				descriptionobjectarray.add(descriptioncaseobject);
				tab = data.getTab();

			}
		
		
		
		if(manincaseobjectarray.size()==0)
		{
			JSONObject maincaseobject = new JSONObject();
			
			
			JSONObject descriptioncaseobject = new JSONObject();
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			String   time=day+"-"+month+"-"+year;
			descriptioncaseobject.put("dtReport",time);
			descriptionobjectarray.add(descriptioncaseobject);
			maincaseobject.put("assigned","Sales");
			maincaseobject.put("custStatus","Open");
			//maincaseobject.put("assigned","Open");
			//maincaseobject.put("custStatus","Sales");
			maincaseobject.put("dateCase",time);
			maincaseobject.put("casePriority","Normal");
			maincaseobject.put("orginReq",0);
			
			 HttpSession session=req.getSession();
			 String countryCode=(String)session.getAttribute("countrycode");

			 String usernasme=(String)session.getAttribute("user");
			
			maincaseobject.put("caseRef",bsscommonService.getSequenceNumber("E",countryCode,usernasme));
			manincaseobjectarray.add(maincaseobject);
		}
		
		mainobject.put("maincaseform",manincaseobjectarray);
		mainobject.put("description",descriptionobjectarray);
		
		
		
		
		
		
		
		
	
		List<CompititorGridTemp> newselectiongridtemp=masterservice.getDetailsFromCompititorAndReplaceGridTemp(storedtime);
		  
		int i=1;
		for(CompititorGridTemp nst:newselectiongridtemp)
		{
			 
			
			
			JSONObject gridobject = new JSONObject();
			gridobject.put("gm",nst.getGm());
			gridobject.put("id",i);
			gridobject.put("gearMotor",nst.getGearMotor());
			gridobject.put("quantity",nst.getQuantity());
			gridobject.put("application",nst.getApplication());
			
			gridobject.put("ambientTemp",nst.getAmbientTemp());
			gridobject.put("gearProduct",nst.getGearProduct());
			gridobject.put("model",nst.getModel());
			gridobject.put("selectModel",nst.getSelectModel());
			gridobject.put("servFactor",nst.getRequiredSreviceFactor());
			gridobject.put("motorType",nst.getMotorType());
			gridobject.put("motorKW",nst.getMotorKW().toString());
			gridobject.put("pole",nst.getPole());
			gridobject.put("phase",nst.getPhase());
			gridobject.put("voltage",nst.getVoltage());
			gridobject.put("hertz",nst.getHertz());
			gridobject.put("rpm", nst.getRpm());
			gridobject.put("accessMotor", nst.getAccessMotor());
			gridobject.put("protectionGrade",nst.getProtectionGrade());
			gridobject.put("internationalStd",nst.getInternationalStd());
			gridobject.put("motorBrake",nst.getMotorBrake());
			gridobject.put("motorBrakeVolt",nst.getMotorBrakeVolt());
			gridobject.put("hssRadialLoadKn",nst.getHssRadialLoadKn());
			gridobject.put("hssRadialLoadMm",nst.getHssRadialLoadMm());
			gridobject.put("hssAxialLoadKn",nst.getHssAxialLoadKn());
			//gridobject.put("dirAxial",nst.getDirAxial());
			gridobject.put("sssRadialLoadKn",nst.getSssRadialLoadKn());
			gridobject.put("sssRadialLoadMm",nst.getSssRadialLoadMm());
			gridobject.put("sssAxialLoadKn",nst.getSssAxialLoadKn());
			gridobject.put("dirSssAxial", nst.getDirSssAxial());
			gridobject.put("ratioRequired",nst.getRatioRequired());
			gridobject.put("requiredOutputSpeed",nst.getRequiredOutputSpeed());
			gridobject.put("presetTorque",nst.getPresetTorque());
			gridobject.put("conf",nst.getConf());
			//gridobject.put("mount",nst.getMount());
			//gridobject.put("mount2",nst.getMount2());
			gridobject.put("mount3",nst.getMount3());
			gridobject.put("ishaft",nst.getIshaft());
			gridobject.put("oshaft",nst.getOshaft());
			gridobject.put("oshaftInc",nst.getOshaftInc());
			gridobject.put("deg",nst.getDeg());
			gridobject.put("towards",nst.getTowards());
			gridobject.put("oshafty",nst.getOshafty());
			gridobject.put("oshafty2",nst.getOshafty2());
			gridobject.put("backStop",nst.getBackStop());
			gridobject.put("rot", nst.getRot());
			gridobject.put("dirHssAxial",nst.getDirHssAxial());
			String compititorcodes=nst.getCompititorcode();
			if(compititorcodes!=null)
			{
			String sscode[]=compititorcodes.split(",");
			gridobject.put("compSSCCode",sscode);
			}
			
			gridobject.put("requiredSreviceFactor",nst.getRequiredSreviceFactor());
			gridobject.put("competitorModel",nst.getCompetitorModel());
			gridobject.put("targetPrice",nst.getTargetPrice());
			gridobject.put("dimension",nst.getDimension());
			//gridobject.put(key, value);
			gridobject.put("docFile",nst.getImagepath());
			gridobject.put("gridid",i);
			gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
			gridobject.put("inverter",nst.getInverter());
			
			Boolean b=true;
			Boolean b1=false;
			
 
			gridobject.put("mount2",nst.getMount2());
			gridobject.put("mount",nst.getMount());
			gridobject.put("competitorBrand",nst.getCompetitorBrand());
			gridobject.put("accessMotor",nst.getAccessMotor());
			
			 
			if(nst.getOshaftInc2().equalsIgnoreCase("true"))
			{
				gridobject.put("oshaftInc2",b);
				
			}
			
			else
			{
				gridobject.put("oshaftInc2",b1);
				
			}
			
			if(nst.getOshaftInc1().equalsIgnoreCase("true"))
			{
				 
				 gridobject.put("oshaftInc1",b);
				 
				}
			
			else
			{
				
				 gridobject.put("oshaftInc1",b1);
			}
			
			
			//gridobject.put("oshaftInc2",nst.getOshaftInc2());
			//gridobject.put("oshaftInc1",nst.getOshaftInc1());
			gridobject.put("other",nst.getOther());
			gridobject.put("othersPlz",nst.getOthersPlz());
			
						

			 gridarray.add(gridobject);
			 	
			 	
			 	
			 	
			 	
			 	
			    
			
			i++;
		
			
			
			 	
		}
		
		
		
		if(gridarray.size()==0)
		{
			JSONObject gridobject = new JSONObject();
			gridobject.put("id",1);
			mainobject.put("value",0);
			gridarray.add(gridobject);
		}
		
		
	 
		
	 
		
		
		
		
		
		
		mainobject.put("griddata",gridarray);
		mainobject.put("tab",2);
		 
		
		
		return  mainobject;
		
		}catch(Exception  e)
		{
			logger.error(e.getMessage()+"  Error in  the line  ");
			return null;
		}
	
		
		
	}
		
		
		@RequestMapping(value = "/loadingCasessTab1", method = RequestMethod.POST)
		public @ResponseBody JSONObject loadingCasessTab1(HttpServletRequest req, HttpServletResponse res) throws Exception 
		{
			 
			JSONObject mainobject=new JSONObject();
			JSONArray gridarray=new JSONArray();
			JSONArray manincaseobjectarray=new JSONArray();
			JSONArray descriptionobjectarray=new JSONArray();
			
			HttpSession hs=req.getSession();
			String username=(String)hs.getAttribute("user");
			int tab=1;
			
			String storedtime=masterservice.getStoredTime(username,tab);
			List<CaseSummaryTemp> newselectiontemp=masterservice.getDeatilsFromNewSelectionTemp(storedtime);
			 
			
			for(CaseSummaryTemp data:newselectiontemp)
	 {
					JSONObject maincaseobject = new JSONObject();
					JSONObject descriptioncaseobject = new JSONObject();
					maincaseobject.put("tempid",data.getNewselectionid());
					maincaseobject.put("customer", data.getCustomer());
					maincaseobject.put("customerCode", data.getCustomerCode());
					maincaseobject.put("caseRef", data.getCaseRef());
					maincaseobject.put("contactPerson", data.getContactPerson());
					maincaseobject.put("phone", data.getPhone());
					maincaseobject.put("address", data.getAddress());
					maincaseobject.put("customerRef", data.getCustomerRef());
					maincaseobject.put("images", data.getImages());
					maincaseobject.put("assigned", data.getAssigned());
					maincaseobject.put("custStatus", data.getCustStatus());
					maincaseobject.put("casePriority",data.getCasePriority());
					maincaseobject.put("orginReq",data.getOrginReq());

					Date d1 = data.getDateExp();
					if (d1 != null) {
						d1 = data.getDateExp();
						String k1 = DateUtils.formatDate(data.getDateExp(), Constants.GenericDateFormat.DATE_FORMAT);
						String[] k2=k1.split("-");
						 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
						
						maincaseobject.put("dateExp", k1);

					}
					Date d2 = data.getDtCase();
					if (d2 != null) {
						String k1 = DateUtils.formatDate(data.getDtCase(), Constants.GenericDateFormat.DATE_FORMAT);
						String[] k2=k1.split("-");
						 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
						maincaseobject.put("dateCase", k1);

					}
					Date d3 = data.getDtReport();
					if (d3 != null) {
						String k1 = DateUtils.formatDate(d3, Constants.GenericDateFormat.DATE_FORMAT);
						String[] k2=k1.split("-");
						 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
						descriptioncaseobject.put("dtReport", k1);

					}
					Date d4 = data.getDtReportBy();
					if (d4 != null) {
						String k1 = DateUtils.formatDate(d4, Constants.GenericDateFormat.DATE_FORMAT);
						String[] k2=k1.split("-");
						 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
						descriptioncaseobject.put("dateRepot", k1);

					}
					
					descriptioncaseobject.put("description",data.getDescription());

					manincaseobjectarray.add(maincaseobject);
					descriptionobjectarray.add(descriptioncaseobject);
					tab = data.getTab();

				}
			
			
			
			if(manincaseobjectarray.size()==0)
			{
				JSONObject maincaseobject = new JSONObject();
				
				
				JSONObject descriptioncaseobject = new JSONObject();
				Calendar now = Calendar.getInstance();
				int year = now.get(Calendar.YEAR);
				int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
				int day = now.get(Calendar.DAY_OF_MONTH);
				String   time=day+"-"+month+"-"+year;
				descriptioncaseobject.put("dtReport",time);
				descriptionobjectarray.add(descriptioncaseobject);
				maincaseobject.put("assigned","Sales");
				maincaseobject.put("custStatus","Open");
				//maincaseobject.put("assigned","Open");
				//maincaseobject.put("custStatus","Sales");
				maincaseobject.put("dateCase",time);
				maincaseobject.put("casePriority","Normal");
				maincaseobject.put("orginReq",0);
				
				 HttpSession session=req.getSession();
				 String countryCode=(String)session.getAttribute("countrycode");
				 String usernasme=(String)session.getAttribute("user");

				maincaseobject.put("caseRef",bsscommonService.getSequenceNumber("E",countryCode,usernasme));
				manincaseobjectarray.add(maincaseobject);
			}
			
			mainobject.put("maincaseform",manincaseobjectarray);
			mainobject.put("description",descriptionobjectarray);
			
			
			
			
			
			
			
			
		
			List<NewSelectionGridTemp> newselectiongridtemp=masterservice.getDetailsFromNewSelectionGridTemp(storedtime);
			  
			int i=1;
			for(NewSelectionGridTemp nst:newselectiongridtemp)
			{
				 
				
				
				JSONObject gridobject = new JSONObject();
				gridobject.put("gm",nst.getGm());
				gridobject.put("id",i);
				gridobject.put("gearMotor",nst.getGearMotor());
				gridobject.put("quantity",nst.getQuantity());
				gridobject.put("application",nst.getApplication());
				gridobject.put("ambientTemp",nst.getAmbientTemp());
				gridobject.put("gearProduct",nst.getGearProduct());
				gridobject.put("model",nst.getModel());
				gridobject.put("selectModel",nst.getSelectModel());
				gridobject.put("servFactor",nst.getServFactor());
				gridobject.put("motorType",nst.getMotorType());
				gridobject.put("motorKW",nst.getMotorKW().toString());
				gridobject.put("pole",nst.getPole());
				gridobject.put("phase",nst.getPhase());
				gridobject.put("voltage",nst.getVoltage());
				gridobject.put("hertz",nst.getHertz());
				gridobject.put("rpm", nst.getRpm());
				gridobject.put("protectionGrade",nst.getProtectionGrade());
				gridobject.put("internationalStd",nst.getInternationalStd());
				gridobject.put("motorBrake",nst.getMotorBrake());
				gridobject.put("motorBrakeVolt",nst.getMotorBrakeVolt());
				gridobject.put("hssRadialLoadKn",nst.getHssRadialLoadKn());
				gridobject.put("hssRadialLoadMm",nst.getHssRadialLoadMm());
				gridobject.put("hssAxialLoadKn",nst.getHssAxialLoadKn());
				gridobject.put("dirAxial",nst.getDirAxial());
				gridobject.put("sssRadialLoadKn",nst.getSssRadialLoadKn());
				gridobject.put("sssRadialLoadMm",nst.getSssRadialLoadMm());
				gridobject.put("sssAxialLoadKn",nst.getSssAxialLoadKn());
				gridobject.put("dirSssAxial", nst.getDirSssAxial());
				gridobject.put("ratioRequired",nst.getRatioRequired());
				gridobject.put("requiredOutputSpeed",nst.getRequiredOutputSpeed());
				gridobject.put("presetTorque",nst.getPresetTorque());
				gridobject.put("conf",nst.getConf());
				gridobject.put("mount",nst.getMount());
				 
				//gridobject.put("mount2",nst.getMount2());
				//gridobject.put("mount3",nst.getMount3());
				gridobject.put("ishaft",nst.getIshaft());
				gridobject.put("oshaft",nst.getOshaft());
				gridobject.put("oshaftInc",nst.getOshaftInc());
				gridobject.put("deg",nst.getDeg());
				gridobject.put("towards",nst.getTowards());
				gridobject.put("oshafty",nst.getOshafty());
				gridobject.put("oshafty2",nst.getOshafty2());
				gridobject.put("backStop",nst.getBackStop());
				gridobject.put("rot", nst.getRot());
				gridobject.put("accessMotor", nst.getAccessMotor());
				
				
				gridobject.put("mtrEfficncyClass",nst.getMtrEfficncyClass());
				gridobject.put("inverter",nst.getInverter());

				gridobject.put("dirHssAxial",nst.getDirHssAxial());
				String ssccodes=nst.getSscCode();
				if(ssccodes!=null)
				{
				String sscode[]=ssccodes.split(",");
				gridobject.put("sscCode",sscode);
				}
				//gridobject.put("oshaftInc2",nst.getOshaftInc2());
				//gridobject.put("oshaftInc1",nst.getOshaftInc1());
				

				 Boolean b=true;
				 Boolean b1=false;
				

				 
				 
				gridobject.put("mount2",nst.getMount2());
				 
				 //gridobject.put("mount3",nst.getMount3());
					
				
				 
				
				if(nst.getOshaftInc2().equalsIgnoreCase("true"))
				{
					Boolean t=true;
					gridobject.put("oshaftInc2",t);
					
				}
				
				else
				{
					Boolean t=false;
					gridobject.put("oshaftInc2",t);
					
				}
				
				if(nst.getOshaftInc1().equalsIgnoreCase("true"))
				{
					 
					 gridobject.put("oshaftInc1",b);
					 
					}
				
				else
				{
					
					 gridobject.put("oshaftInc1",b1);
				}
				
				gridobject.put("other",nst.getOther());
				gridobject.put("othersPlz",nst.getOthersPlz());
				
				
				gridobject.put("docFile",nst.getImagepath());
				gridobject.put("gridid",i);
				 	gridarray.add(gridobject);
				
				i++;
			
				
				
				 	
			}
			
		     
			
			if(gridarray.size()==0)
			{
				JSONObject gridobject = new JSONObject();
				gridobject.put("id",1);
				mainobject.put("value",0);
				gridarray.add(gridobject);
			}
			
			
		 
			
		 
			
			
			
			
			
			
			mainobject.put("griddata",gridarray);
			mainobject.put("tab",1);
			 
			
			
			return  mainobject;
			
			 
		}
		
		
		


	
	
	
	@RequestMapping(value = "/loadingCasessTab", method = RequestMethod.POST)
	public @ResponseBody JSONObject loadingCasessTab(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		 
		 
		JSONObject mainobject=null;
		HttpSession hs=req.getSession();
		String username=(String)hs.getAttribute("user");
		int tab=0;
		
		String storedtime=masterservice.getStoredTime(username);
		List<CaseSummaryTemp> newselectiontemp=masterservice.getDeatilsFromNewSelectionTemp(storedtime);
		 
		
		for(CaseSummaryTemp data:newselectiontemp)
 {
				JSONObject maincaseobject = new JSONObject();
				JSONObject descriptioncaseobject = new JSONObject();
				maincaseobject.put("tempid",data.getNewselectionid());
				maincaseobject.put("customer", data.getCustomer());
				maincaseobject.put("customerCode", data.getCustomerCode());
				maincaseobject.put("caseRef", data.getCaseRef());
				maincaseobject.put("contactPerson", data.getContactPerson());
				maincaseobject.put("phone", data.getPhone());
				maincaseobject.put("address", data.getAddress());
				maincaseobject.put("customerRef", data.getCustomerRef());
				maincaseobject.put("assigned", data.getAssigned());
				maincaseobject.put("custStatus", data.getCustStatus());


				Date d1 = data.getDateExp();
				if (d1 != null) {
					d1 = data.getDateExp();
					String k1 = DateUtils.formatDate(data.getDateExp(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					
					maincaseobject.put("dateExp", k1);

				}
				Date d2 = data.getDtCase();
				if (d2 != null) {
					String k1 = DateUtils.formatDate(data.getDtCase(), Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					maincaseobject.put("dateCase", k1);

				}
				Date d3 = data.getDtReport();
				if (d3 != null) {
					String k1 = DateUtils.formatDate(d3, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dtReport", k1);

				}
				Date d4 = data.getDtReportBy();
				if (d4 != null) {
					String k1 = DateUtils.formatDate(d4, Constants.GenericDateFormat.DATE_FORMAT);
					String[] k2=k1.split("-");
					 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
					descriptioncaseobject.put("dateRepot;", k1);

				}
				
				descriptioncaseobject.put("description",data.getDescription());

				 
				tab = data.getTab();

			}
		
		
		
		if(tab==1)
		{
		 mainobject=this.loadingCasessTab1(req, res);
			
			
		}
		
		else if(tab==2)
		{
			
			 mainobject=this.loadingCasessTab2(req, res);
			
		}
	 	
		else if(tab==3)
		{
			
			mainobject=this.loadingCasessTab3(req, res);
			
		}
		
		
		else if(tab==4)
		{
			
			mainobject=this.loadingCasessTab4(req, res);
			
		}
		
		else if(tab==5)
		{
			
			mainobject=this.loadingCasessTab5(req, res);
			
		}
		
		
		else
		{
			mainobject=new JSONObject();
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			String   time=day+"-"+month+"-"+year;
			
			
				JSONArray manincaseobjectarray = new JSONArray();
				JSONArray gridarray = new JSONArray();

				JSONObject maincaseobject = new JSONObject();
				 HttpSession session=req.getSession(false);
				 String countryCode=(String)session.getAttribute("countrycode");
				 String usernasme=(String)session.getAttribute("user");
				maincaseobject.put("caseRef", bsscommonService.getSequenceNumber("E",countryCode,usernasme));
				maincaseobject.put("assigned","Sales");
				maincaseobject.put("custStatus","Open");
				maincaseobject.put("dateCase",time);
				maincaseobject.put("casePriority","Normal");
				maincaseobject.put("orginReq",0);
				
				manincaseobjectarray.add(maincaseobject);

				JSONObject gridobject = new JSONObject();
				gridobject.put("id", 1);
				mainobject.put("value",0);
				gridarray.add(gridobject);

				JSONArray descriptionobjectarray = new JSONArray();
				JSONObject descriptioncaseobject = new JSONObject();
				
				
				
				 
				descriptioncaseobject.put("dtReport",time);
				
				
				
				

				descriptionobjectarray.add(descriptioncaseobject);

				mainobject.put("griddata", gridarray);
				mainobject.put("maincaseform", manincaseobjectarray);
				mainobject.put("description", descriptionobjectarray);
				mainobject.put("value",0);
				mainobject.put("tab", 0);
				return  mainobject;
			
		}
		
		
		return  mainobject;
		
		 
	}
	
	
	


	

/********************************************************End of Compitoir************************************************************************************/	

	
	
	@RequestMapping(value = "/sumitomoReplaceDataSaveTemp", method = RequestMethod.POST)
public @ResponseBody boolean sumitomoReplaceDataSaveTemp(MultipartHttpServletRequest req)
	throws Exception {


		String ss=req.getParameter("formData");
		String description1=req.getParameter("fieldData");
		String ss1=req.getParameter("gridData");
		org.json.JSONArray gData=new org.json.JSONArray(ss1);
	   // List<MultipartFile> mpf=req.getFiles("motorFile");
	   String imagepath1=null;
	   String gik=req.getParameter("motorFile");
	   if(gik!=null)
	   {
	    imagepath1=gik.replaceAll("\"", "");
	   	    
	   }
	

	int status = 0;
	DescriptionCaseBean description = null;
	TreeMap<Integer, String> al = null;

	HttpSession hs = req.getSession();
	String user=(String)hs.getAttribute("user");
	int tab=3;
	
	Calendar now = Calendar.getInstance();
	int year = now.get(Calendar.YEAR);
	int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
	int day = now.get(Calendar.DAY_OF_MONTH);
	int hour = now.get(Calendar.HOUR_OF_DAY);
	int minute = now.get(Calendar.MINUTE);
	int second = now.get(Calendar.SECOND);
	int millis = now.get(Calendar.MILLISECOND);
    String storedtime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
	
	if (hs.getAttribute("image") != null) {
		al = (TreeMap<Integer, String>) hs.getAttribute("image");
	}

	/*JSONObject caseform = (JSONObject) jsonData.get("formData");
	JSONArray casegrid = (JSONArray) jsonData.get("gridData");
	JSONObject casedescription = (JSONObject) jsonData.get("fieldData");*/

	MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);
	if (!description1.equalsIgnoreCase("undefined")) {
		status = 1;
		description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
	}

	String k = null;
	ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
	for (int i = 0; i < gData.length(); i++) {

		NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
				NewSelectionGridFormBean.class);

		String a[] = newcasegrid.getSscCode();
		if (a != null) {
			for (int m = 0; m < a.length; m++) {
				if (m == 0) {
					k = a[m];
				}

				if (m > 0) {

					k = k + ',' + a[m];
				}

			}

		}
		if (al != null) {
			String imagepath = (String) al.get(i + 1);
			newcasegrid.setImagepath(imagepath);
		}
		newcasegrid.setSsCode(k);

		newselctiongrid.add(newcasegrid);
	}

	CaseSummaryTemp newselection = this.insetDataIntoNewSElctionCaseMOdelTempModified(maincase, description, status, imagepath1,maincase.getImages());
	newselection.setStoredtime(storedtime);
	newselection.setTab(tab);
	newselection.setUsername(user);
	newselection.setCasetype("SumitomoReplace TYpe");
	
	if(maincase.getTempid()!=null)
	{
		
		/*masterservice.deleteFromTempTableTeable(maincase.getTempid());*/
		masterservice.deleteSumtimogridTemp(maincase.getTempid(),tab);
	}

	masterservice.saveNewSelectionCaseTemp(newselection);
	List<SumitomoReplaceTempGrid> newselectiongrid = this.insertDataIntoSumitomoReplaceTempGrid(newselctiongrid,storedtime,tab,user,newselection.getCaseRef());
	masterservice.saveSumitomoReplaceTempGrid(newselectiongrid);

	hs.removeAttribute("image");

	return true;

 

}

private List<SumitomoReplaceTempGrid> insertDataIntoSumitomoReplaceTempGrid(
		ArrayList<NewSelectionGridFormBean> newselctiongrid, String storedtime, int tab, String user, String caseRef) 
{
	try {
		String newselectionkey = masterservice.getNewSelectionTempKey();
		ArrayList<SumitomoReplaceTempGrid> al = new ArrayList<SumitomoReplaceTempGrid>();
		for (NewSelectionGridFormBean ndgb : newselctiongrid) {
			SumitomoReplaceTempGrid ng = new SumitomoReplaceTempGrid();
			ng.setCaseref(caseRef);
			ng.setToken(newselectionkey);
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setImagepath(ndgb.getImagepath());
			ng.setConf(ndgb.getConf());
			ng.setGm(ndgb.getGm());
			ng.setGearMotor(ndgb.getGearMotor());
			ng.setQuantity(ndgb.getQuantity());
			ng.setApplication(ndgb.getApplication());
			ng.setAmbientTemp(ndgb.getAmbientTemp());
			ng.setGearProduct(ndgb.getGearProduct());
			ng.setModel(ndgb.getModel());
			ng.setSelectModel(ndgb.getSelectModel());
			ng.setServFactor(ndgb.getServFactor());
			ng.setSscCode(ndgb.getSsCode());
			ng.setMotorType(ndgb.getMotorType());
			ng.setMotorKW(ndgb.getMotorKW());
			ng.setPole(ndgb.getPole());
			ng.setPhase(ndgb.getPhase());
			ng.setVoltage(ndgb.getVoltage());
			ng.setHertz(ndgb.getHertz());
			ng.setRpm(ndgb.getRpm());
			ng.setDimension(ndgb.getDimension());
			ng.setProtectionGrade(ndgb.getProtectionGrade());
			ng.setInternationalStd(ndgb.getInternationalStd());
			ng.setAccessMotor(ndgb.getAccessMotor());
			ng.setMotorBrake(ndgb.getMotorBrake());
			ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
			ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
			ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
			ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
			ng.setDirAxial(ndgb.getDirAxial());
			ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
			ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
			ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
			ng.setDirSssAxial(ndgb.getDirSssAxial());
			ng.setRatioRequired(ndgb.getRatioRequired());
			ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
			ng.setPresetTorque(ndgb.getPresetTorque());
			ng.setMount(ndgb.getMount());
			ng.setMount2(ndgb.getMount2());
			ng.setMount3(ndgb.getMount3());
			ng.setIshaft(ndgb.getIshaft());
			ng.setOshaft(ndgb.getOshaft());
			ng.setOshaftInc(ndgb.getOshaftInc());
			ng.setDeg(ndgb.getDeg());
			ng.setTowards(ndgb.getTowards());
			ng.setOshafty(ndgb.getOshafty());
			ng.setOshafty2(ndgb.getOshafty2());
			ng.setBackStop(ndgb.getBackStop());
			ng.setRot(ndgb.getRot());
			ng.setDirHssAxial(ndgb.getDirHssAxial());
			ng.setStoredtime(storedtime);
			ng.setTab(tab);
			ng.setUsername(user);
			ng.setGearhead(ndgb.getGearhead());
			ng.setMotorSerial(ndgb.getMotorSerial());
			ng.setShiMfg(ndgb.getShiMfg());
			ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
            ng.setInverter(ndgb.getInverter());
            ng.setOshaftInc2(ndgb.getOshaftInc2());
			ng.setOshaftInc1(ndgb.getOshaftInc1());
			ng.setOther(ndgb.getOther());
			ng.setOthersPlz(ndgb.getOthersPlz());


			al.add(ng);

		}
		// TODO Auto-generated method stub
		return al;

		// TODO Auto-generated method stub
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
	
}




@RequestMapping(value = "/sumitomoReplaceDataSave", method = RequestMethod.POST)
public @ResponseBody boolean sumitomoReplaceDataSave(MultipartHttpServletRequest req)
throws Exception {
	
	
	String ss=req.getParameter("formData");
	String description1=req.getParameter("fieldData");
	String ss1=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(ss1);
		   // List<MultipartFile> mpf=req.getFiles("motorFile");
		    
		    String imagepath1=null;
		    String gik=req.getParameter("motorFile");
			   if(gik!=null)
			   {
			    imagepath1=gik.replaceAll("\"", "");
			   	    
			   }
	
	
		    
	
	
	
	

int status = 0;
int tab=3;
TreeMap<Integer, String> al = null;
DescriptionCaseBean description = null;
HttpSession hs = req.getSession();
if (hs.getAttribute("image") != null) {
al = (TreeMap<Integer, String>) hs.getAttribute("image");
}

String user=(String)hs.getAttribute("user");

/*JSONObject caseform = (JSONObject) jsonData.get("formData");
JSONArray casegrid = (JSONArray) jsonData.get("gridData");
JSONObject casedescription = (JSONObject) jsonData.get("fieldData");*/

MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);









if (!description1.equalsIgnoreCase("undefined")) {
status = 1;
description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
}
String k = null;
ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
for (int i = 0; i < gData.length(); i++) {

NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
		NewSelectionGridFormBean.class);

String a[] = newcasegrid.getSscCode();
if (a != null) {
	for (int m = 0; m < a.length; m++) {
		if (m == 0) {
			k = a[m];
		}

		if (m > 0) {

			k = k + ',' + a[m];
		}

	}

}

if (al != null) {
	String imagepath = (String) al.get(i + 1);
	newcasegrid.setImagepath(imagepath);
}
newcasegrid.setSsCode(k);

newselctiongrid.add(newcasegrid);
}





if(maincase.getMainid()!=null)
{

CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
newselection.setCasetype("Sumitomo Replace");
newselection.setTab(3);
newselection.setNewselectionid(maincase.getMainid());
masterservice.saveNewSelectionCase(newselection);
 
masterservice.deleteSumitomoReplaceGrid(newselection.getCaseRef());
List<SumitomoReplaceGrid> newselectiongrid = this.insertDataIntoSumitomoReplaceGrid(newselctiongrid,newselection.getCaseRef());
//masterservice.saveNewSelectionGrid(newselectiongrid);
masterservice.saveSumitimoGrid(newselectiongrid);


}


if(maincase.getMainid()==null)
{

CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
newselection.setCasetype("Sumitomo Replace");
newselection.setTab(3);


String caserefernce=newselection.getCaseRef();

int caserefrencecount=masterservice.getCaseRefernce(caserefernce);


ArrayList sequencenumberlist=new ArrayList();
if(caserefrencecount==0)
{
	
	String[] spl=caserefernce.split("-");	
	String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");


	SequenceNumber sequenceNumber=new SequenceNumber();

	sequenceNumber.setProcessname(spl[0]);
	sequenceNumber.setCountrycode(st[0]);
	sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
	sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
	sequenceNumber.setTotalsequenceno(caserefernce);
	
	masterservice.saveNewSelectionCase(newselection);
	List<SumitomoReplaceGrid> newselectiongrid = this.insertDataIntoSumitomoReplaceGrid(newselctiongrid,newselection.getCaseRef());
	masterservice.saveSumitimoGrid(newselectiongrid);
	
	sequencenumberlist.add(sequenceNumber);
	productSelectionService.saveEntities(sequencenumberlist);
	
}




if(caserefrencecount>0)

{
	 HttpSession session=req.getSession();
	 String countryCode=(String)session.getAttribute("countrycode");
	 String usernasme=(String)session.getAttribute("user");
	String caseref=bsscommonService.getSequenceNumber("E",countryCode,usernasme);
	 
	
	newselection.setCaseRef(caseref);
	masterservice.saveNewSelectionCase(newselection);
	List<SumitomoReplaceGrid> newselectiongrid = this.insertDataIntoSumitomoReplaceGrid(newselctiongrid,newselection.getCaseRef());
	
	masterservice.saveSumitimoGrid(newselectiongrid);
	
	

	String[] spl=caseref.split("-");	
	String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");


	SequenceNumber sequenceNumber=new SequenceNumber();

	sequenceNumber.setProcessname(spl[0]);
	sequenceNumber.setCountrycode(st[0]);
	sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
	sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
	sequenceNumber.setTotalsequenceno(caseref);

	
	sequencenumberlist.add(sequenceNumber);
	
	productSelectionService.saveEntities(sequencenumberlist);
	
}


if(maincase.getTempid()!=null)
{
	
	masterservice.deleteSumtimogridTemp(maincase.getTempid(),tab);
}

}




hs.removeAttribute("image");

return true;

}



private List<SumitomoReplaceGrid> insertDataIntoSumitomoReplaceGrid(
		ArrayList<NewSelectionGridFormBean> newselctiongrid,String caseref) {

	String newselectionkey = masterservice.getNewSelectionKey();
	ArrayList<SumitomoReplaceGrid> al = new ArrayList<SumitomoReplaceGrid>();
	for (NewSelectionGridFormBean ndgb : newselctiongrid) {
		SumitomoReplaceGrid ng = new SumitomoReplaceGrid();
		ng.setCaseref(caseref);
		ng.setToken(newselectionkey);
		ng.setAmbientTemp(ndgb.getAmbientTemp());
		ng.setImagepath(ndgb.getImagepath());
		ng.setConf(ndgb.getConf());
		ng.setGm(ndgb.getGm());
		ng.setGearMotor(ndgb.getGearMotor());
		ng.setDimension(ndgb.getDimension());
		ng.setQuantity(ndgb.getQuantity());
		ng.setApplication(ndgb.getApplication());
		ng.setAmbientTemp(ndgb.getAmbientTemp());
		ng.setGearProduct(ndgb.getGearProduct());
		ng.setModel(ndgb.getModel());
		ng.setSelectModel(ndgb.getSelectModel());
		ng.setServFactor(ndgb.getServFactor());
		ng.setSscCode(ndgb.getSsCode());
		ng.setMotorType(ndgb.getMotorType());
		ng.setMotorKW(ndgb.getMotorKW());
		ng.setPole(ndgb.getPole());
		ng.setPhase(ndgb.getPhase());
		ng.setVoltage(ndgb.getVoltage());
		ng.setHertz(ndgb.getHertz());
		ng.setRpm(ndgb.getRpm());
		ng.setProtectionGrade(ndgb.getProtectionGrade());
		ng.setInternationalStd(ndgb.getInternationalStd());
		ng.setAccessMotor(ndgb.getAccessMotor());
		ng.setMotorBrake(ndgb.getMotorBrake());
		ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
		ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
		ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
		ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
		ng.setDirAxial(ndgb.getDirAxial());
		ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
		ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
		ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
		ng.setDirSssAxial(ndgb.getDirSssAxial());
		ng.setRatioRequired(ndgb.getRatioRequired());
		ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
		ng.setPresetTorque(ndgb.getPresetTorque());
		ng.setMount(ndgb.getMount());
		ng.setMount2(ndgb.getMount2());
		ng.setMount3(ndgb.getMount3());
		ng.setIshaft(ndgb.getIshaft());
		ng.setOshaft(ndgb.getOshaft());
		ng.setOshaftInc(ndgb.getOshaftInc());
		ng.setDeg(ndgb.getDeg());
		ng.setTowards(ndgb.getTowards());
		ng.setOshafty(ndgb.getOshafty());
		ng.setOshafty2(ndgb.getOshafty2());
		ng.setBackStop(ndgb.getBackStop());
		ng.setRot(ndgb.getRot());
		ng.setDirHssAxial(ndgb.getDirHssAxial());
		ng.setTab(3);
		ng.setGearhead(ndgb.getGearhead());
		ng.setMotorSerial(ndgb.getMotorSerial());
		ng.setShiMfg(ndgb.getShiMfg());
		ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
        ng.setInverter(ndgb.getInverter());
        ng.setOshaftInc2(ndgb.getOshaftInc2());
		ng.setOshaftInc1(ndgb.getOshaftInc1());
		ng.setOther(ndgb.getOther());
		ng.setOthersPlz(ndgb.getOthersPlz());
		al.add(ng);

	}
	// TODO Auto-generated method stub
	return al;
}


/**************************************************starting spare parts******************************************************************************/

@RequestMapping(value = "/SparePartCaseTemp", method = RequestMethod.POST)
public @ResponseBody boolean SparePartCaseTemp(MultipartHttpServletRequest req )
throws Exception {


	String ss=req.getParameter("formData");
	String description1=req.getParameter("fieldData");
	String ss1=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(ss1);
		   // List<MultipartFile> mpf=req.getFiles("motorFile");
		    
		    String imagepath1=null;
		    
		    String gik=req.getParameter("motorFile");
			   if(gik!=null)
			   {
			    imagepath1=gik.replaceAll("\"", "");
			   	    
			   }



int status = 0;
DescriptionCaseBean description = null;
TreeMap<Integer, String> al = null;

HttpSession hs = req.getSession();
String user=(String)hs.getAttribute("user");
int tab=4;

Calendar now = Calendar.getInstance();
int year = now.get(Calendar.YEAR);
int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
int day = now.get(Calendar.DAY_OF_MONTH);
int hour = now.get(Calendar.HOUR_OF_DAY);
int minute = now.get(Calendar.MINUTE);
int second = now.get(Calendar.SECOND);
int millis = now.get(Calendar.MILLISECOND);
String storedtime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;

if (hs.getAttribute("image") != null) {
	al = (TreeMap<Integer, String>) hs.getAttribute("image");
}

/*JSONObject caseform = (JSONObject) jsonData.get("formData");
JSONArray casegrid = (JSONArray) jsonData.get("gridData");
JSONObject casedescription = (JSONObject) jsonData.get("fieldData");*/

MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);
if(!description1.equalsIgnoreCase("undefined")) {
	status = 1;
	description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
}

String k = null;
ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
for (int i = 0; i < gData.length(); i++) {

	NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
			NewSelectionGridFormBean.class);

	String a[] = newcasegrid.getSscCode();
	if (a != null) {
		for (int m = 0; m < a.length; m++) {
			if (m == 0) {
				k = a[m];
			}

			if (m > 0) {

				k = k + ',' + a[m];
			}

		}

	}
	if (al != null) {
		String imagepath = (String) al.get(i + 1);
		newcasegrid.setImagepath(imagepath);
	}
	newcasegrid.setSsCode(k);

	newselctiongrid.add(newcasegrid);
}

CaseSummaryTemp newselection = this.insetDataIntoNewSElctionCaseMOdelTempModified(maincase, description, status, imagepath1,maincase.getImages());
newselection.setStoredtime(storedtime);
newselection.setTab(tab);
newselection.setUsername(user);
newselection.setCasetype("SPARE Part Temp");

if(maincase.getTempid()!=null)
{
	
	masterservice.deleteFromSparePartTempTable(maincase.getTempid(),tab);
}

masterservice.saveNewSelectionCaseTemp(newselection);
List<SparePartGridTemp> newselectiongrid = this.insertDataIntoSparePartTempGrid(newselctiongrid,storedtime,tab,user,newselection.getCaseRef());
masterservice.saveSparePartGridTemp(newselectiongrid);

hs.removeAttribute("image");

return true;



}

private List<SparePartGridTemp> insertDataIntoSparePartTempGrid(
ArrayList<NewSelectionGridFormBean> newselctiongrid,String storedtime,int tab,String user,String caseref) {

String newselectionkey = masterservice.getNewSelectionTempKey();
ArrayList<SparePartGridTemp> al = new ArrayList<SparePartGridTemp>();
for (NewSelectionGridFormBean ndgb : newselctiongrid) {
	SparePartGridTemp ng = new SparePartGridTemp();
ng.setCaseref(caseref);
ng.setToken(newselectionkey);
ng.setAmbientTemp(ndgb.getAmbientTemp());
ng.setImagepath(ndgb.getImagepath());
ng.setConf(ndgb.getConf());
ng.setGm(ndgb.getGm());
ng.setGearMotor(ndgb.getGearMotor());
ng.setQuantity(ndgb.getQuantity());
ng.setApplication(ndgb.getApplication());
ng.setAmbientTemp(ndgb.getAmbientTemp());
ng.setGearProduct(ndgb.getGearProduct());
ng.setModel(ndgb.getModel());
ng.setSelectModel(ndgb.getSelectModel());
ng.setServFactor(ndgb.getServFactor());
ng.setSscCode(ndgb.getSsCode());
ng.setMotorType(ndgb.getMotorType());
ng.setMotorKW(ndgb.getMotorKW());
ng.setPole(ndgb.getPole());
ng.setPhase(ndgb.getPhase());
ng.setVoltage(ndgb.getVoltage());
ng.setHertz(ndgb.getHertz());
ng.setRpm(ndgb.getRpm());
ng.setProtectionGrade(ndgb.getProtectionGrade());
ng.setInternationalStd(ndgb.getInternationalStd());
ng.setAccessMotor(ndgb.getAccessMotor());
ng.setMotorBrake(ndgb.getMotorBrake());
ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
ng.setDirAxial(ndgb.getDirAxial());
ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
ng.setDirSssAxial(ndgb.getDirSssAxial());
ng.setRatioRequired(ndgb.getRatioRequired());
ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
ng.setPresetTorque(ndgb.getPresetTorque());
ng.setMount(ndgb.getMount());
ng.setMount2(ndgb.getMount2());
ng.setMount3(ndgb.getMount3());
ng.setIshaft(ndgb.getIshaft());
ng.setOshaft(ndgb.getOshaft());
ng.setOshaftInc(ndgb.getOshaftInc());
ng.setDeg(ndgb.getDeg());
ng.setTowards(ndgb.getTowards());
ng.setOshafty(ndgb.getOshafty());
ng.setOshafty2(ndgb.getOshafty2());
ng.setBackStop(ndgb.getBackStop());
ng.setRot(ndgb.getRot());
ng.setDirHssAxial(ndgb.getDirHssAxial());
ng.setStoredtime(storedtime);
ng.setTab(tab);
ng.setUsername(user);
ng.setDescript(ndgb.getDescript());
ng.setQty(ndgb.getQty());
ng.setQty1(ndgb.getQty1());
ng.setQty2(ndgb.getQty2());
ng.setSpare1(ndgb.getSpare1());
ng.setSpare2(ndgb.getSpare2());
ng.setDescript1(ndgb.getDescript1());
ng.setDescript2(ndgb.getDescript2());
ng.setSpare(ndgb.getSpare());
ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
ng.setInverter(ndgb.getInverter());
ng.setOshaftInc2(ndgb.getOshaftInc2());
ng.setOshaftInc1(ndgb.getOshaftInc1());
ng.setOther(ndgb.getOther());
ng.setOthersPlz(ndgb.getOthersPlz());
ng.setGearhead(ndgb.getGearhead());
ng.setMotorSerial(ndgb.getMotorSerial());
ng.setShiMfg(ndgb.getShiMfg());
ng.setAccessMotor(ndgb.getAccessMotor());


al.add(ng);

}
// TODO Auto-generated method stub
return al;
}



@RequestMapping(value = "/SparePartCase", method = RequestMethod.POST)
public @ResponseBody boolean SparePartCase(MultipartHttpServletRequest req)
throws Exception {

	
	String ss=req.getParameter("formData");
	String description1=req.getParameter("fieldData");
	String ss1=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(ss1);
		   // List<MultipartFile> mpf=req.getFiles("motorFile");
		    
		    String imagepath1=null;
		    String gik=req.getParameter("motorFile");
			   if(gik!=null)
			   {
			    imagepath1=gik.replaceAll("\"", "");
			   	    
			   }
int status = 0;
int tab=4;
TreeMap<Integer, String> al = null;
DescriptionCaseBean description = null;
HttpSession hs = req.getSession();
if (hs.getAttribute("image") != null) {
al = (TreeMap<Integer, String>) hs.getAttribute("image");
}
String user=(String)hs.getAttribute("user");

/*JSONObject caseform = (JSONObject) jsonData.get("formData");
JSONArray casegrid = (JSONArray) jsonData.get("gridData");
JSONObject casedescription = (JSONObject) jsonData.get("fieldData");*/

MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);









if (!description1.equalsIgnoreCase("undefined")) {
status = 1;
description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
}
String k = null;
ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
for (int i = 0; i < gData.length(); i++) {

NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
		NewSelectionGridFormBean.class);

String a[] = newcasegrid.getSscCode();
if (a != null) {
	for (int m = 0; m < a.length; m++) {
		if (m == 0) {
			k = a[m];
		}

		if (m > 0) {

			k = k + ',' + a[m];
		}

	}

}

if (al != null) {
	String imagepath = (String) al.get(i + 1);
	newcasegrid.setImagepath(imagepath);
}
newcasegrid.setSsCode(k);

newselctiongrid.add(newcasegrid);
}





if(maincase.getMainid()!=null)
{

CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
newselection.setCasetype("Spare Parts");
newselection.setTab(4);
newselection.setNewselectionid(maincase.getMainid());
masterservice.saveNewSelectionCase(newselection);
 
masterservice.deletefromsparePartsGrid(newselection.getCaseRef());
List<SparePartGrid> newselectiongrid = this.insertDataIntoSparePartGrid(newselctiongrid,newselection.getCaseRef());
masterservice.saveSparePartGrid(newselectiongrid);

//List<SparePartGrid> insertDataIntoSparePartGrid
}


if(maincase.getMainid()==null)
{

CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
newselection.setCasetype("Spare Parts");
newselection.setTab(4);


String caserefernce=newselection.getCaseRef();

int caserefrencecount=masterservice.getCaseRefernce(caserefernce);


ArrayList sequencenumberlist=new ArrayList();
if(caserefrencecount==0)
{
	
	String[] spl=caserefernce.split("-");	
	String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");


	SequenceNumber sequenceNumber=new SequenceNumber();

	sequenceNumber.setProcessname(spl[0]);
	sequenceNumber.setCountrycode(st[0]);
	sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
	sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
	sequenceNumber.setTotalsequenceno(caserefernce);
	
	masterservice.saveNewSelectionCase(newselection);
	List<SparePartGrid> newselectiongrid = this.insertDataIntoSparePartGrid(newselctiongrid,newselection.getCaseRef());
	masterservice.saveSparePartGrid(newselectiongrid);
	
	
	//List<SparePartGrid> insertDataIntoSparePartGrid
	sequencenumberlist.add(sequenceNumber);
	productSelectionService.saveEntities(sequencenumberlist);
	
}




if(caserefrencecount>0)

{
	HttpSession session=req.getSession();
	 String countryCode=(String)session.getAttribute("countrycode");
	 String usernasme=(String)session.getAttribute("user");

	String caseref=bsscommonService.getSequenceNumber("E",countryCode,usernasme);
	
	newselection.setCaseRef(caseref);
	masterservice.saveNewSelectionCase(newselection);
	List<SparePartGrid> newselectiongrid = this.insertDataIntoSparePartGrid(newselctiongrid,newselection.getCaseRef());
	
	masterservice.saveSparePartGrid(newselectiongrid);
	
	

	String[] spl=caseref.split("-");	
	String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");


	SequenceNumber sequenceNumber=new SequenceNumber();

	sequenceNumber.setProcessname(spl[0]);
	sequenceNumber.setCountrycode(st[0]);
	sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
	sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
	sequenceNumber.setTotalsequenceno(caseref);

	
	sequencenumberlist.add(sequenceNumber);
	
	productSelectionService.saveEntities(sequencenumberlist);
	
}


if(maincase.getTempid()!=null)
{
	
	masterservice.deleteFromSparePartTempTable(maincase.getTempid(),tab);
}

}




hs.removeAttribute("image");

return true;

}

private List<SparePartGrid> insertDataIntoSparePartGrid(
ArrayList<NewSelectionGridFormBean> newselctiongrid,String caseref) {

String newselectionkey = masterservice.getNewSelectionKey();
ArrayList<SparePartGrid> al = new ArrayList<SparePartGrid>();
for (NewSelectionGridFormBean ndgb : newselctiongrid) {
SparePartGrid ng = new SparePartGrid();
ng.setCaseref(caseref);
ng.setToken(newselectionkey);

ng.setAmbientTemp(ndgb.getAmbientTemp());
ng.setImagepath(ndgb.getImagepath());
ng.setConf(ndgb.getConf());
ng.setGm(ndgb.getGm());
ng.setGearMotor(ndgb.getGearMotor());
ng.setQuantity(ndgb.getQuantity());
ng.setApplication(ndgb.getApplication());
ng.setAmbientTemp(ndgb.getAmbientTemp());
ng.setGearProduct(ndgb.getGearProduct());
ng.setModel(ndgb.getModel());
ng.setSelectModel(ndgb.getSelectModel());
ng.setServFactor(ndgb.getServFactor());
ng.setSscCode(ndgb.getSsCode());
ng.setMotorType(ndgb.getMotorType());
ng.setMotorKW(ndgb.getMotorKW());
ng.setPole(ndgb.getPole());
ng.setPhase(ndgb.getPhase());
ng.setVoltage(ndgb.getVoltage());
ng.setHertz(ndgb.getHertz());
ng.setRpm(ndgb.getRpm());
ng.setProtectionGrade(ndgb.getProtectionGrade());
ng.setInternationalStd(ndgb.getInternationalStd());
ng.setAccessMotor(ndgb.getAccessMotor());
ng.setMotorBrake(ndgb.getMotorBrake());
ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
ng.setDirAxial(ndgb.getDirAxial());
ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
ng.setDirSssAxial(ndgb.getDirSssAxial());
ng.setRatioRequired(ndgb.getRatioRequired());
ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
ng.setPresetTorque(ndgb.getPresetTorque());
ng.setMount(ndgb.getMount());
ng.setMount2(ndgb.getMount2());
ng.setMount3(ndgb.getMount3());
ng.setIshaft(ndgb.getIshaft());
ng.setOshaft(ndgb.getOshaft());
ng.setOshaftInc(ndgb.getOshaftInc());
ng.setDeg(ndgb.getDeg());
ng.setTowards(ndgb.getTowards());
ng.setOshafty(ndgb.getOshafty());
ng.setOshafty2(ndgb.getOshafty2());
ng.setBackStop(ndgb.getBackStop());
ng.setRot(ndgb.getRot());
ng.setDirHssAxial(ndgb.getDirHssAxial());
ng.setDescript(ndgb.getDescript());
ng.setQty(ndgb.getQty());
ng.setQty1(ndgb.getQty1());
ng.setQty2(ndgb.getQty2());
ng.setSpare1(ndgb.getSpare1());
ng.setSpare2(ndgb.getSpare2());
ng.setDescript1(ndgb.getDescript1());
ng.setDescript2(ndgb.getDescript2());
ng.setSpare(ndgb.getSpare());
ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
ng.setInverter(ndgb.getInverter());
ng.setOshaftInc2(ndgb.getOshaftInc2());
ng.setOshaftInc1(ndgb.getOshaftInc1());
ng.setOther(ndgb.getOther());
ng.setOthersPlz(ndgb.getOthersPlz());
ng.setGearhead(ndgb.getGearhead());
ng.setMotorSerial(ndgb.getMotorSerial());
ng.setShiMfg(ndgb.getShiMfg());
ng.setAccessMotor(ndgb.getAccessMotor());


ng.setTab(4);
al.add(ng);

}
// TODO Auto-generated method stub
return al;
}

@RequestMapping(value = "/techquerycasetempsave", method = RequestMethod.POST)
public @ResponseBody boolean techquerycasetempsave(MultipartHttpServletRequest req)
throws Exception {


	String ss=req.getParameter("formData");
	String description1=req.getParameter("fieldData");
	String ss1=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(ss1);
		   // List<MultipartFile> mpf=req.getFiles("motorFile");
		    
		    String imagepath1=null;

		    String gik=req.getParameter("motorFile");
			   if(gik!=null)
			   {
			    imagepath1=gik.replaceAll("\"", "");
			   	    
			   }



int status = 0;
DescriptionCaseBean description = null;
TreeMap<Integer, String> al = null;

HttpSession hs = req.getSession();
String user=(String)hs.getAttribute("user");
int tab=5;

Calendar now = Calendar.getInstance();
int year = now.get(Calendar.YEAR);
int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
int day = now.get(Calendar.DAY_OF_MONTH);
int hour = now.get(Calendar.HOUR_OF_DAY);
int minute = now.get(Calendar.MINUTE);
int second = now.get(Calendar.SECOND);
int millis = now.get(Calendar.MILLISECOND);
String storedtime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;

if (hs.getAttribute("image") != null) {
	al = (TreeMap<Integer, String>) hs.getAttribute("image");
}

/*JSONObject caseform = (JSONObject) jsonData.get("formData");
JSONArray casegrid = (JSONArray) jsonData.get("gridData");
JSONObject casedescription = (JSONObject) jsonData.get("fieldData");
*/
MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);
if (!description1.equalsIgnoreCase("undefined")) {
	status = 1;
	description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
}

String k = null;
ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
for (int i = 0; i < gData.length(); i++) {

	NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
			NewSelectionGridFormBean.class);

	String a[] = newcasegrid.getSscCode();
	if (a != null) {
		for (int m = 0; m < a.length; m++) {
			if (m == 0) {
				k = a[m];
			}

			if (m > 0) {

				k = k + ',' + a[m];
			}

		}

	}
	if (al != null) {
		String imagepath = (String) al.get(i + 1);
		newcasegrid.setImagepath(imagepath);
	}
	newcasegrid.setSsCode(k);

	newselctiongrid.add(newcasegrid);
}

CaseSummaryTemp newselection = this.insetDataIntoNewSElctionCaseMOdelTempModified(maincase, description, status,imagepath1,maincase.getImages());
newselection.setStoredtime(storedtime);
newselection.setTab(tab);
newselection.setUsername(user);
newselection.setCasetype("Tech Query Case Type");

if(maincase.getTempid()!=null)
{
	
	masterservice.deleteFromTechQueryTempTable(maincase.getTempid(),tab);
}

masterservice.saveNewSelectionCaseTemp(newselection);
List<TechGridTemp> newselectiongrid = this.insertDataIntoTechQueryGridModelTemp(newselctiongrid,storedtime,tab,user,newselection.getCaseRef());
masterservice.saveTechGridTemp(newselectiongrid);

hs.removeAttribute("image");

return true;



}

private List<TechGridTemp> insertDataIntoTechQueryGridModelTemp(
ArrayList<NewSelectionGridFormBean> newselctiongrid,String storedtime,int tab,String user,String caseref) {

String newselectionkey = masterservice.getNewSelectionTempKey();
ArrayList<TechGridTemp> al = new ArrayList<TechGridTemp>();
for (NewSelectionGridFormBean ndgb : newselctiongrid) {
	TechGridTemp ng = new TechGridTemp();
ng.setCaseref(caseref);
ng.setToken(newselectionkey);
ng.setAmbientTemp(ndgb.getAmbientTemp());
ng.setImagepath(ndgb.getImagepath());
ng.setConf(ndgb.getConf());
ng.setGm(ndgb.getGm());
ng.setGearMotor(ndgb.getGearMotor());
ng.setQuantity(ndgb.getQuantity());
ng.setApplication(ndgb.getApplication());
ng.setAmbientTemp(ndgb.getAmbientTemp());
ng.setGearProduct(ndgb.getGearProduct());
ng.setModel(ndgb.getModel());
ng.setSelectModel(ndgb.getSelectModel());
ng.setServFactor(ndgb.getServFactor());
ng.setSscCode(ndgb.getSsCode());
ng.setMotorType(ndgb.getMotorType());
ng.setMotorKW(ndgb.getMotorKW());
ng.setPole(ndgb.getPole());
ng.setPhase(ndgb.getPhase());
ng.setVoltage(ndgb.getVoltage());
ng.setHertz(ndgb.getHertz());
ng.setRpm(ndgb.getRpm());
ng.setProtectionGrade(ndgb.getProtectionGrade());
ng.setInternationalStd(ndgb.getInternationalStd());
ng.setAccessMotor(ndgb.getAccessMotor());
ng.setMotorBrake(ndgb.getMotorBrake());
ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
ng.setDirAxial(ndgb.getDirAxial());
ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
ng.setDirSssAxial(ndgb.getDirSssAxial());
ng.setRatioRequired(ndgb.getRatioRequired());
ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
ng.setPresetTorque(ndgb.getPresetTorque());
ng.setMount(ndgb.getMount());
ng.setMount2(ndgb.getMount2());
ng.setMount3(ndgb.getMount3());
ng.setIshaft(ndgb.getIshaft());
ng.setOshaft(ndgb.getOshaft());
ng.setOshaftInc(ndgb.getOshaftInc());
ng.setDeg(ndgb.getDeg());
ng.setTowards(ndgb.getTowards());
ng.setOshafty(ndgb.getOshafty());
ng.setOshafty2(ndgb.getOshafty2());
ng.setBackStop(ndgb.getBackStop());
ng.setRot(ndgb.getRot());
ng.setDirHssAxial(ndgb.getDirHssAxial());
ng.setStoredtime(storedtime);
ng.setTab(tab);
ng.setUsername(user);
ng.setRequiredSreviceFactor(ndgb.getRequiredSreviceFactor());
ng.setGearhead(ndgb.getGearhead());
ng.setMotorSerial(ndgb.getMotorSerial());
ng.setShiMfg(ndgb.getShiMfg());
ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
ng.setInverter(ndgb.getInverter());
ng.setOshaftInc2(ndgb.getOshaftInc2());
ng.setOshaftInc1(ndgb.getOshaftInc1());
ng.setOther(ndgb.getOther());
ng.setOthersPlz(ndgb.getOthersPlz());
al.add(ng);

}
// TODO Auto-generated method stub
return al;
}


@RequestMapping(value = "/techquerygridsave", method = RequestMethod.POST)
public @ResponseBody boolean techquerygridsave(MultipartHttpServletRequest req)
throws Exception {
	
	
	String ss=req.getParameter("formData");
	String description1=req.getParameter("fieldData");
	String ss1=req.getParameter("gridData");
			org.json.JSONArray gData=new org.json.JSONArray(ss1);
		   // List<MultipartFile> mpf=req.getFiles("motorFile");
		    
		    String imagepath1=null;
		    String gik=req.getParameter("motorFile");
			   if(gik!=null)
			   {
			    imagepath1=gik.replaceAll("\"", "");
			   	    
			   }



int status = 0;
int tab=5;
TreeMap<Integer, String> al = null;
DescriptionCaseBean description = null;
HttpSession hs = req.getSession();
if (hs.getAttribute("image") != null) {
al = (TreeMap<Integer, String>) hs.getAttribute("image");
}
String user=(String)hs.getAttribute("user");

/*JSONObject caseform = (JSONObject) jsonData.get("formData");
JSONArray casegrid = (JSONArray) jsonData.get("gridData");
JSONObject casedescription = (JSONObject) jsonData.get("fieldData");*/

MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);









if (!description1.equalsIgnoreCase("undefined")) {
status = 1;
description = new ObjectMapper().readValue(description1, DescriptionCaseBean.class);
}
String k = null;
ArrayList<NewSelectionGridFormBean> newselctiongrid = new ArrayList<NewSelectionGridFormBean>();
for (int i = 0; i < gData.length(); i++) {

NewSelectionGridFormBean newcasegrid = new ObjectMapper().readValue(gData.get(i).toString(),
		NewSelectionGridFormBean.class);

String a[] = newcasegrid.getSscCode();
if (a != null) {
	for (int m = 0; m < a.length; m++) {
		if (m == 0) {
			k = a[m];
		}

		if (m > 0) {

			k = k + ',' + a[m];
		}

	}

}

if (al != null) {
	String imagepath = (String) al.get(i + 1);
	newcasegrid.setImagepath(imagepath);
}
newcasegrid.setSsCode(k);

newselctiongrid.add(newcasegrid);
}





if(maincase.getMainid()!=null)
{

CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);
newselection.setCasetype("Tech Query Grid");
newselection.setTab(5);
newselection.setNewselectionid(maincase.getMainid());
masterservice.saveNewSelectionCase(newselection);
 
masterservice.deleteTechQueryGrid(newselection.getCaseRef());
List<TechGrid> newselectiongrid = this.insertDataIntoTechQueryGridModel(newselctiongrid,newselection.getCaseRef());
masterservice.saveTechQueryGrid(newselectiongrid);


}


if(maincase.getMainid()==null)
{

CaseSummary newselection = this.insetDataIntoNewSElctionCaseMOdelChanged(maincase, description, status,user,maincase.getImages(),imagepath1);;
newselection.setCasetype("Tech Query Grid");
newselection.setTab(5);


String caserefernce=newselection.getCaseRef();

int caserefrencecount=masterservice.getCaseRefernce(caserefernce);


ArrayList sequencenumberlist=new ArrayList();
if(caserefrencecount==0)
{
	
	String[] spl=caserefernce.split("-");	
	String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");


	SequenceNumber sequenceNumber=new SequenceNumber();

	sequenceNumber.setProcessname(spl[0]);
	sequenceNumber.setCountrycode(st[0]);
	sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
	sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
	sequenceNumber.setTotalsequenceno(caserefernce);
	
	masterservice.saveNewSelectionCase(newselection);
	List<TechGrid> newselectiongrid = this.insertDataIntoTechQueryGridModel(newselctiongrid,newselection.getCaseRef());
	masterservice.saveTechQueryGrid(newselectiongrid);

	//List<NewSelectionGrid> newselectiongrid = this.insertDataIntoNewSelectionGridModel(newselctiongrid,newselection.getCaseRef());
	//masterservice.saveNewSelectionGrid(newselectiongrid);
	
	sequencenumberlist.add(sequenceNumber);
	productSelectionService.saveEntities(sequencenumberlist);
	
}




if(caserefrencecount>0)

{
	 HttpSession session=req.getSession();
	 String countryCode=(String)session.getAttribute("countrycode");
	 String usernasme=(String)session.getAttribute("user");

	
	String caseref=bsscommonService.getSequenceNumber("E",countryCode,usernasme);
	
	newselection.setCaseRef(caseref);
	masterservice.saveNewSelectionCase(newselection);
	List<TechGrid> newselectiongrid = this.insertDataIntoTechQueryGridModel(newselctiongrid,newselection.getCaseRef());
	masterservice.saveTechQueryGrid(newselectiongrid);

	//List<NewSelectionGrid> newselectiongrid = this.insertDataIntoNewSelectionGridModel(newselctiongrid,newselection.getCaseRef());
	
	//masterservice.saveNewSelectionGrid(newselectiongrid);
	
	

	String[] spl=caseref.split("-");	
	String[] st=spl[1].split("(?i)((?<=[A-Z])(?=\\d))|((?<=\\d)(?=[A-Z]))");


	SequenceNumber sequenceNumber=new SequenceNumber();

	sequenceNumber.setProcessname(spl[0]);
	sequenceNumber.setCountrycode(st[0]);
	sequenceNumber.setSeqyear(Integer.parseInt(st[1]));
	sequenceNumber.setSerialno(Integer.parseInt(spl[2]));
	sequenceNumber.setTotalsequenceno(caseref);

	
	sequencenumberlist.add(sequenceNumber);
	
	productSelectionService.saveEntities(sequencenumberlist);
	
}


if(maincase.getTempid()!=null)
{
	
	//masterservice.deleteFromTempTableTeable(maincase.getTempid(),tab);
	masterservice.deleteFromTechQueryTempTable(maincase.getTempid(),tab);
	
}

}




hs.removeAttribute("image");

return true;

}

private List<TechGrid> insertDataIntoTechQueryGridModel(
ArrayList<NewSelectionGridFormBean> newselctiongrid,String caseref) {

String newselectionkey = masterservice.getNewSelectionKey();
ArrayList<TechGrid> al = new ArrayList<TechGrid>();
for (NewSelectionGridFormBean ndgb : newselctiongrid) {
	TechGrid ng = new TechGrid();
ng.setCaseref(caseref);
ng.setToken(newselectionkey);

ng.setAmbientTemp(ndgb.getAmbientTemp());
ng.setImagepath(ndgb.getImagepath());
ng.setConf(ndgb.getConf());
ng.setGm(ndgb.getGm());
ng.setGearMotor(ndgb.getGearMotor());
ng.setQuantity(ndgb.getQuantity());
ng.setApplication(ndgb.getApplication());
ng.setAmbientTemp(ndgb.getAmbientTemp());
ng.setGearProduct(ndgb.getGearProduct());
ng.setModel(ndgb.getModel());
ng.setSelectModel(ndgb.getSelectModel());
ng.setServFactor(ndgb.getServFactor());
ng.setSscCode(ndgb.getSsCode());
ng.setMotorType(ndgb.getMotorType());
ng.setMotorKW(ndgb.getMotorKW());
ng.setPole(ndgb.getPole());
ng.setPhase(ndgb.getPhase());
ng.setVoltage(ndgb.getVoltage());
ng.setHertz(ndgb.getHertz());
ng.setRpm(ndgb.getRpm());
ng.setProtectionGrade(ndgb.getProtectionGrade());
ng.setInternationalStd(ndgb.getInternationalStd());
ng.setAccessMotor(ndgb.getAccessMotor());
ng.setMotorBrake(ndgb.getMotorBrake());
ng.setMotorBrakeVolt(ndgb.getMotorBrakeVolt());
ng.setHssAxialLoadKn(ndgb.getHssAxialLoadKn());
ng.setHssRadialLoadMm(ndgb.getHssRadialLoadMm());
ng.setHssRadialLoadKn(ndgb.getHssRadialLoadKn());
ng.setDirAxial(ndgb.getDirAxial());
ng.setSssRadialLoadKn(ndgb.getSssRadialLoadKn());
ng.setSssRadialLoadMm(ndgb.getSssRadialLoadMm());
ng.setSssAxialLoadKn(ndgb.getSssAxialLoadKn());
ng.setDirSssAxial(ndgb.getDirSssAxial());
ng.setRatioRequired(ndgb.getRatioRequired());
ng.setRequiredOutputSpeed(ndgb.getRequiredOutputSpeed());
ng.setPresetTorque(ndgb.getPresetTorque());
ng.setMount(ndgb.getMount());
ng.setMount2(ndgb.getMount2());
ng.setMount3(ndgb.getMount3());
ng.setIshaft(ndgb.getIshaft());
ng.setOshaft(ndgb.getOshaft());
ng.setOshaftInc(ndgb.getOshaftInc());
ng.setDeg(ndgb.getDeg());
ng.setTowards(ndgb.getTowards());
ng.setOshafty(ndgb.getOshafty());
ng.setOshafty2(ndgb.getOshafty2());
ng.setBackStop(ndgb.getBackStop());
ng.setRot(ndgb.getRot());
ng.setDirHssAxial(ndgb.getDirHssAxial());
ng.setTab(5);
ng.setRequiredSreviceFactor(ndgb.getRequiredSreviceFactor());
ng.setGearhead(ndgb.getGearhead());
ng.setMotorSerial(ndgb.getMotorSerial());
ng.setShiMfg(ndgb.getShiMfg());
ng.setMtrEfficncyClass(ndgb.getMtrEfficncyClass());
ng.setInverter(ndgb.getInverter());
ng.setOshaftInc2(ndgb.getOshaftInc2());
ng.setOshaftInc1(ndgb.getOshaftInc1());
ng.setOther(ndgb.getOther());
ng.setOthersPlz(ndgb.getOthersPlz());
al.add(ng);

}
// TODO Auto-generated method stub
return al;
}




/*************************************************************Discarding Drafts *************************************************************/

	 

@RequestMapping(value = "/newSelectionFormDiscardData", method = RequestMethod.POST )
public @ResponseBody boolean discaredNewselctionCaseTemp(MultipartHttpServletRequest req)
	throws Exception {


	
		String ss = req.getParameter("formData");
		String description1 = req.getParameter("fieldData");
		String ss1 = req.getParameter("gridData");
		org.json.JSONArray gData = new org.json.JSONArray(ss1);
		int tab = 1;

		MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);

		if (maincase.getTempid() != null) {

			masterservice.deleteFromTempTableTeable(maincase.getTempid(), tab);
		}

		return true;


		
		
	}

@RequestMapping(value = "/saveCompetitorReplaceDataDiscaredTemp", method = RequestMethod.POST)
public @ResponseBody boolean CompitiorReplaceCaseDiscaredTemp(MultipartHttpServletRequest req)
 throws Exception {

		String ss = req.getParameter("formData");
		String description1 = req.getParameter("fieldData");
		String ss1 = req.getParameter("gridData");
		org.json.JSONArray gData = new org.json.JSONArray(ss1);
		// List<MultipartFile> mpf=req.getFiles("motorFile");

		// String imagepath1=this.getImagepathOfCompititorTemp(mpf);

		int tab = 2;

		MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);

		if (maincase.getTempid() != null) {

			masterservice.deleteFromCompititorReplceTemp(maincase.getTempid(), tab);
		}

		return true;

	}




@RequestMapping(value = "/techquerycaseDiscardtempsave", method = RequestMethod.POST)
public @ResponseBody boolean techquerycaseDiscardtempsave(MultipartHttpServletRequest req)
 throws Exception {

		String ss = req.getParameter("formData");
		String description1 = req.getParameter("fieldData");
		String ss1 = req.getParameter("gridData");
		org.json.JSONArray gData = new org.json.JSONArray(ss1);
		// List<MultipartFile> mpf=req.getFiles("motorFile");

		String imagepath1 = null;

		String gik = req.getParameter("motorFile");
		if (gik != null) {
			imagepath1 = gik.replaceAll("\"", "");

		}

		int tab = 5;

		MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);

		if (maincase.getTempid() != null) {

			masterservice.deleteFromTechQueryTempTable(maincase.getTempid(), tab);
		}

		return true;

	}


@RequestMapping(value = "/SparePartCaseDiscardTemp", method = RequestMethod.POST)
public @ResponseBody boolean SparePartCaseDiscardTemp(MultipartHttpServletRequest req )
 throws Exception {

		String ss = req.getParameter("formData");
		String description1 = req.getParameter("fieldData");
		String ss1 = req.getParameter("gridData");
		org.json.JSONArray gData = new org.json.JSONArray(ss1);
		// List<MultipartFile> mpf=req.getFiles("motorFile");

		String imagepath1 = null;

		String gik = req.getParameter("motorFile");
		if (gik != null) {
			imagepath1 = gik.replaceAll("\"", "");

		}

		int status = 0;

		int tab = 4;

		MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);

		if (maincase.getTempid() != null) {

			masterservice.deleteFromSparePartTempTable(maincase.getTempid(), tab);
		}

		return true;

	}



@RequestMapping(value = "/sumitomoReplaceDataDiscardSaveTemp", method = RequestMethod.POST)
public @ResponseBody boolean sumitomoReplaceDataDiscardSaveTemp(MultipartHttpServletRequest req)
 throws Exception {

		String ss = req.getParameter("formData");
		String description1 = req.getParameter("fieldData");
		String ss1 = req.getParameter("gridData");
		org.json.JSONArray gData = new org.json.JSONArray(ss1);
		// List<MultipartFile> mpf=req.getFiles("motorFile");

		int tab = 3;

		MianCaseFormBean maincase = new ObjectMapper().readValue(ss, MianCaseFormBean.class);

		if (maincase.getTempid() != null) {

			/* masterservice.deleteFromTempTableTeable(maincase.getTempid()); */
			masterservice.deleteSumtimogridTemp(maincase.getTempid(), tab);
		}

		return true;

	}



@RequestMapping(value = "/getAllCustmerNames", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getAllCustmerNames(HttpServletRequest req,HttpServletResponse res) throws Exception

{
	
		List<Object[]> c = masterservice.getAllAccountNames();
		JSONObject mainobject = new JSONObject();

		JSONArray accountarray = new JSONArray();

		for (Object[] c1 : c) {

			String custmernames = (String) c1[1];

			JSONObject k = new JSONObject();
			k.put("name", custmernames);
			accountarray.add(k);

		}

		mainobject.put("data", accountarray);

		return mainobject;
}




@RequestMapping(value = "/getAllCustmerCopdes", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getAllCustmerCodes(HttpServletRequest req,HttpServletResponse res) throws Exception

{
	
		List<Object[]> c = masterservice.getAllAccountNames();
		JSONObject mainobject = new JSONObject();

		JSONArray accountarray = new JSONArray();
		ArrayList<String> al = new ArrayList();
		ArrayList<String> al2 = new ArrayList();

		for (Object[] c1 : c) {

			String cutmecodes = (String) c1[0];

			JSONObject k = new JSONObject();
			k.put("name", cutmecodes);
			accountarray.add(k);

		}
 
 
	mainobject.put("data", accountarray);

	return mainobject;
}



@RequestMapping(value = "/getCustmerNamequatation", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getCustmerNameforquation(HttpServletRequest req,HttpServletResponse res) throws Exception

{
	String custmername=req.getParameter("custmername");
String 	cutmercode=masterservice.getAccountCodefromCutmerMaster(custmername);
JSONObject mainobject=this.custmerdatabybasingoncustmercode(cutmercode);
			 
	  

		 
 return mainobject;
	 

	
}






@RequestMapping(value = "/getCustmerNameFromNewCase", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getCustmerNameFromNewCase(HttpServletRequest req,HttpServletResponse res) throws Exception

{
	String custmername=req.getParameter("custmername");
	/*String custmername2 = String.format(l, format, args)Replace(custmername,"'","''");*/
	String custmername2 = custmername.replace("'","''");
	String cutmercode=null;
	
	
	 cutmercode=masterservice.getAccountCodefromCutmerMaster(custmername2);
	
	 
		JSONObject mainobject = new JSONObject();
		 JSONArray mainarray=new  JSONArray();
		
		List<CustmerDetails>	cutmerdetails= masterservice.getAllCustmersDeatails(cutmercode);
		
		mainobject.put("check",0);
		for(CustmerDetails a:cutmerdetails)
			
		{
			 
			 
		String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
	    String[] k2=k1.split("-");
		String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
		mainobject.put("date",k3);
		mainobject.put("check",1);
		mainobject.put("custmer",a);
		mainobject.put("custid",a.getCustid());
			List<CustmerContactDetails> contactdetails=a.getRolescreens();
			int i=1;
			
			if(contactdetails.size()==0)
			{
				
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("contactName",null);
				subobject.put("contactNumber",null);
				subobject.put("contactAltNumber",null);
				subobject.put("department",null);
				subobject.put("emailAddr",null);
				subobject.put("titleName",null);
				mainarray.add(subobject);
				
				 
				
			}
			
			for(CustmerContactDetails b:contactdetails)
			{
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("contactName",b.getContactName());
				subobject.put("contactNumber",b.getContactNumber());
				subobject.put("contactAltNumber",b.getContactAltNumber());
				subobject.put("department",b.getDepartment());
				subobject.put("emailAddr",b.getEmailAddr());
				subobject.put("titleName",b.getTitleName());
				mainarray.add(subobject);
				
				i++;
			}
			
		}
		
		mainobject.put("grid",mainarray );
			 
			 return mainobject;
			 


		 
 
	 

	
}






public @ResponseBody JSONObject custmerdatabybasingoncustmercode(String custmername1 ) 
{
 	 
	   try{
	   
		 
			//String custmername1=null;
			String applicationname=null;
			 
			/* String custmername=req.getParameter("account");
			 if(custmername!=null)
			 {
			   custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
			 }*/
			 JSONObject mainobject=new JSONObject();
			 JSONArray mainarray=new  JSONArray();
			// JSONArray custmerarray=new  JSONArray();
			 
					 
		List<CustmerDetails>	cutmerdetails= masterservice.getAllCustmersDeatails(custmername1);
		
		mainobject.put("check",0);
		for(CustmerDetails a:cutmerdetails)
			
		{
			JSONObject submainobject=new JSONObject();
			JSONArray custmerarray=new  JSONArray();
			submainobject.put("custid",a.getCustid());
			
			submainobject.put("customerStatus",a.getCustomerStatus());
			submainobject.put("account",a.getAccount());
			submainobject.put("accountid",a.getAccountid());
			submainobject.put("clientwebsite",a.getClientwebsite());
			submainobject.put("mailingadress",a.getMailingadress());
			submainobject.put("shippingadress",a.getShippingadress());
			submainobject.put("billingCountries",a.getBillingCountries());
			submainobject.put("language",a.getLanguage());
			submainobject.put("customerGroup",a.getCustomerType());
			submainobject.put("industries",a.getIndustries());
			submainobject.put("application",a.getApplication());
			submainobject.put("territory",a.getTerritory());
			submainobject.put("corporate",a.getCorporate());
			submainobject.put("contName", a.getContName());
			submainobject.put("title", a.getTitle());
			submainobject.put("phnumber",a.getPhnumber());
			submainobject.put("mbnumber",a.getMbnumber());
			submainobject.put("ophnumber",a.getOphnumber());
			submainobject.put("creditStatus",a.getCreditstatus());
			submainobject.put("email",a.getEmail());
			submainobject.put("fax", a.getFax());
			if(a.getApplicanindustryname()==null && a.getApplication()!=null)
			{
				//String applicationcode=req.getParameter( "applicanIndusName");
				
				List<ApplicationIndustryCode> coutries=migratesevice.getAllApplicationCodeName(a.getApplication());
				 
				for(ApplicationIndustryCode a2:coutries)
				{
					
					applicationname=a2.getTotalname();
					
				}
				 
				submainobject.put("applicanIndustry",applicationname);
				 
			}
			
			else
			{
				submainobject.put("applicanIndustry",a.getApplicanindustryname());
			}
			 
			submainobject.put("departmentname",a.getDepartmentname());
			submainobject.put("reports",a.getReports());
			if(a.getSalesPerson()!=null)
			{
				submainobject.put("salesPerson",a.getSalesPerson());
				
			}
			
			else
			{
				String salesman=masterservice.getSalesmanOnSalesCode(a.getSalesPersonCode());
				submainobject.put("salesPerson",salesman);
				
				
			}
			 
			submainobject.put("discountmultiplier",a.getDiscountmultiplier());
			submainobject.put("salesPersonCode",a.getSalesPersonCode());
			submainobject.put("leadsource",a.getLeadsource());
			submainobject.put("currency", a.getCurrency());
			submainobject.put("taxName",a.getTaxName());
			submainobject.put("paymentTerm",a.getPaymentTerm());
			submainobject.put("opportunityAmount", a.getOpportunityAmount());
			if(a.getBddate()!=null)
			{
				String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
				submainobject.put("bddate",k1 );
			}
			
			
			submainobject.put("createdby",a.getCreatedby());
			submainobject.put("lastmodifiedby",a.getLastmodifiedby());
			submainobject.put("imagename", a.getImagename());
			 
			custmerarray.add(submainobject);
			mainobject.put("custmer",custmerarray);
		    
			
			 
			 /*custmerobject.put("account",a);
			custmerarray.add(custmerobject);*/
	/*	String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
		String k2 = k1.replace("-","/");
			 
		String[] k2=k1.split("-");
		 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
		*/
		
		/*DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");*/
		
	/*	Date frmDate = sdf.parse(k2);*/
		/*	mainobject.put("date",k2);*/
			//mainobject.put("date",k1);
			mainobject.put("check",1);
			//mainobject.put("custmer",a);
			//mainobject.put("custid",a.getCustid());
			List<CustmerContactDetails> contactdetails=a.getRolescreens();
			int i=1;
			
			if(contactdetails.size()==0)
			{
				
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("contactName",null);
				subobject.put("contactNumber",null);
				subobject.put("contactAltNumber",null);
				subobject.put("gdepatment",null);
				subobject.put("email",null);
				subobject.put("gtitle",null);
				mainarray.add(subobject);
				
				 
				
			}
			
			for(CustmerContactDetails b:contactdetails)
			{
				JSONObject subobject=new JSONObject();
				subobject.put("id",i);
				subobject.put("contactName",b.getContactName());
				subobject.put("contactNumber",b.getContactNumber());
				subobject.put("contactAltNumber",b.getContactAltNumber());
				subobject.put("department",b.getDepartment());
				subobject.put("emailAddr",b.getEmailAddr());
				subobject.put("titleName",b.getTitleName());
				mainarray.add(subobject);
				
				i++;
			}
			
		}
		/*mainobject.put("custmer",custmerarray);	 */
		mainobject.put("grid",mainarray );
		//mainobject.put("custmer",custmerarray);
			 
			 return mainobject;
			 
	   }catch(Exception e)
	   {
		   logger.info("=================controllerrrr================"+e.getMessage());
		   return null;
				   
	   }
			 
	}








































@RequestMapping(value = "/getTaxNamesForCustmerMaster", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getTaxNamesForCustmerMaster(HttpServletRequest req,HttpServletResponse res) throws Exception

{
	
		String country = req.getParameter("countrycode");
		
		String countrycode=masterservice.getCountryOnCountreycode(country);

		List<TaxMaster> c = masterservice.getAllTaxNmaesOnCountrycode(countrycode);
		JSONObject mainobject = new JSONObject();

		JSONArray accountarray = new JSONArray();

		for (TaxMaster c1 : c) {

			JSONObject k = new JSONObject();
			k.put("id", c1.getTaxname());
			k.put("taxName", c1.getTaxname());
			
			accountarray.add(k);

		}
		
		if(c.size()==0)
		{
			JSONObject k1 = new JSONObject();
			JSONObject k2 = new JSONObject();
			k1.put("id","VAT");
			k1.put("taxName","VAT");
			k2.put("id","GST");
			k2.put("taxName","GST");
			accountarray.add(k1);
			accountarray.add(k2);
			
		}

		mainobject.put("tname", accountarray);

		return mainobject;
}

@RequestMapping(value = "/getLocalCureenceyOfCountryCode", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getLocalCureenceyOfCountryCode(HttpServletRequest req,HttpServletResponse res) throws Exception

{
		HttpSession hs = req.getSession();
		String countrycode = (String) hs.getAttribute("countrycode");
		JSONObject mainobject=new JSONObject();
		String c=null;
		
		if(countrycode.equalsIgnoreCase("all"))
		
		{
			 c="USD";
			
		}

		else
		{
		 c = masterservice.getLocalCurrenceyOfCountryCode(countrycode);
	 }
		mainobject.put("data",c);

		return mainobject;
}


@RequestMapping(value = "/getpro_grp", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getpro_grp(HttpServletRequest req,HttpServletResponse res) throws Exception

{
	 

		List<GearProductType> c = masterservice.getpro_grp();

		JSONObject mainobject = new JSONObject();
		JSONArray mainarray = new JSONArray();

		for (GearProductType a : c) {
			JSONObject submainobject = new JSONObject();
			submainobject.put("id", a.getGearproducttype());
			submainobject.put("gearProduct", a.getGearproducttype());

			mainarray.add(submainobject);

		}

		mainobject.put("data", mainarray);

		return mainobject;
}


@RequestMapping(value = "/getmtr_kw", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getmtr_kw(HttpServletRequest req,HttpServletResponse res) throws Exception

{
	 
		List<MTR_KW> c = masterservice.getmtr_kw();
	 
		JSONObject mainobject=new JSONObject();
		JSONArray mainarray = new JSONArray();
		 

		for (MTR_KW a : c) {
			JSONObject submainobject = new JSONObject();
			submainobject.put("id", a.getMtr_kw().toString());
           submainobject.put("motorKW", a.getMtr_kw().toString());

			mainarray.add(submainobject);

		}

		JSONObject submainobject1 = new JSONObject();
		submainobject1.put("motorKW","OTHER");
		submainobject1.put("id","OTHER");
		mainarray.add(submainobject1);
		mainobject.put("data", mainarray);
		return mainobject;
		

}


@RequestMapping(value = "/getexlositionclassfication", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getexlositionclassfication(HttpServletRequest req,HttpServletResponse res) throws Exception

{
		 

		List<ExplositionClassfication> c = masterservice.getexlositionclassfication();
		JSONObject mainobject=new JSONObject();
		JSONArray mainarray = new JSONArray();
		 

		for (ExplositionClassfication a : c) {
			JSONObject submainobject = new JSONObject();
			submainobject.put("id", a.getExplositiontype());
			submainobject.put("motorType", a.getExplositiontype());

			mainarray.add(submainobject);

		}

		mainobject.put("data", mainarray);
		return mainobject;
	 
		 
}



@RequestMapping(value = "/getVoltagesforcase", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getVoltagesforCase(HttpServletRequest req,HttpServletResponse res) throws Exception

{
		 

	List<String> c = masterservice.getVolatagesforCase();
	JSONObject mainobject=new JSONObject();
	JSONArray mainarray = new JSONArray();
	 

	for (String a : c) {
		JSONObject submainobject = new JSONObject();
		submainobject.put("id", a);
		submainobject.put("voltage", a);

		mainarray.add(submainobject);

	}

	mainobject.put("data", mainarray);
	return mainobject;
 
		 
}




@RequestMapping(value = "/getRpmforCase", method = RequestMethod.GET)
public  @ResponseBody  JSONObject getRpmForCase(HttpServletRequest req,HttpServletResponse res) throws Exception

{
		String frequency=req.getParameter("frequency");
		String poles=req.getParameter("pole");
		JSONObject mainobject=new JSONObject();
		if(frequency!=null && poles!=null)
		{
			int frequency1=Integer.parseInt(frequency);
			int pole=Integer.parseInt(poles);
			
			List<RPM> rpm=masterservice.getRpm(frequency1,pole);
			for(RPM r:rpm)
			{
				mainobject.put("rpm",r.getRpm());
			}
			
			
		}

		else
		{
			mainobject.put("rpm",0);
			
		}

		 
		return mainobject;
}



@RequestMapping(value = "/departmentViewForCase", method = RequestMethod.GET)
public  @ResponseBody  JSONObject departmentView()
{
	JSONObject productcatgories=new JSONObject();
	JSONArray productcatgoriesaraay = new JSONArray();
	
	List<Department> productageorey=masterservice.departmentView();
	for(Department p:productageorey)
	{
		
	
		JSONObject subproductcatgories=new JSONObject();
		subproductcatgories.put("deptid",p.getDeptid());
		subproductcatgories.put("assigned",p.getDepartmentname());
		subproductcatgories.put("departmentnameshortcut",p.getDepartmentnameshortcut());
		subproductcatgories.put("contactperson",p.getContactperson());
		subproductcatgories.put("email",p.getEmail());
		subproductcatgories.put("extent",p.getExtent());
		subproductcatgories.put("phonenumber",p.getPhonenumber());
		subproductcatgories.put("status",p.getStatus());
		
		productcatgoriesaraay.add(subproductcatgories);
		
	}
	
	productcatgories.put( "data",productcatgoriesaraay);
	
	
	
 
	
	 
	return productcatgories;

}





	


}