package com.finsol.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.finsol.dao.HibernateDao;
import com.finsol.model.ApplicationIndustryCode;
import com.finsol.model.CaseSummary;
import com.finsol.model.CustmerContactDetails;
import com.finsol.model.CustmerDetails;
import com.finsol.model.Quotation;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.Bss_Migrate_ServiceImpl;
import com.finsol.service.MasterScreen_serviceImpl;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

/**
 * @author Rama Krishna
 * 
 */
@Controller
public class CustmerDbController {
	
	private static final Logger logger = Logger.getLogger(CustmerDbController.class);
	
/*	@Autowired
	private Bss_Migrate_ServiceImpl migratesevice;*/
	@Autowired
	private MasterScreen_serviceImpl masterservice;
	
	
	@Autowired
	private Bss_Migrate_ServiceImpl migratesevice;
	
	
	/*@Autowired
    private ServletContext servletContext;
	*/
	@Autowired
	private BSS_CommonService commonService;
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	@Autowired	
	private CaseManagementController casemangementcontrol;
	
	
	
	
	
	
	//Generating Hi Chart for Area/Region YTD
	 
	
	@RequestMapping(value = "/getAllCustmerDetailsforcustmerDB", method = RequestMethod.POST)
    public @ResponseBody JSONObject custmerdatabybasingoncustmercode(HttpServletRequest req,HttpServletResponse res) 
    {
		HttpSession hs=req.getSession();
		
			 
				String custmername1=null;
				String applicationname=null;
				String accountname=null;
				//HttpSession hs=req.getSession();
				 
				 String custmername=req.getParameter("account");
				 hs.setAttribute("custmername",custmername);
				 if(custmername!=null)
				 {
				   custmername1 = custmername.substring(custmername.lastIndexOf('-') + 1);
				 }
				 JSONObject mainobject=new JSONObject();
				 JSONArray mainarray=new  JSONArray();
				// JSONArray custmerarray=new  JSONArray();
				 
						 
			List<CustmerDetails>	cutmerdetails= masterservice.getAllCustmersDeatails(custmername1);
			
			mainobject.put("check",0);
			for(CustmerDetails a:cutmerdetails)
				
			{
				JSONObject submainobject=new JSONObject();
				JSONArray custmerarray=new  JSONArray();
				submainobject.put("custid",a.getCustid());
				
				submainobject.put("customerStatus",a.getCustomerStatus());
				submainobject.put("account",a.getAccount());
				submainobject.put("accountid",a.getAccountid());
				accountname=a.getAccountid();
				submainobject.put("clientwebsite",a.getClientwebsite());
				submainobject.put("mailingadress",a.getMailingadress());
				submainobject.put("shippingadress",a.getShippingadress());
				submainobject.put("billingCountries",a.getBillingCountries());
				submainobject.put("language",a.getLanguage());
				submainobject.put("customerGroup",a.getCustomerType());
				submainobject.put("industries",a.getIndustries());
				submainobject.put("application",a.getApplication());
				submainobject.put("territory",a.getTerritory());
				submainobject.put("corporate",a.getCorporate());
				submainobject.put("contName", a.getContName());
				submainobject.put("title", a.getTitle());
				submainobject.put("phnumber",a.getPhnumber());
				submainobject.put("mbnumber",a.getMbnumber());
				submainobject.put("ophnumber",a.getOphnumber());
				submainobject.put("creditStatus",a.getCreditstatus());
				submainobject.put("email",a.getEmail());
				submainobject.put("fax", a.getFax());
				if(a.getApplicanindustryname()==null && a.getApplication()!=null)
				{
					//String applicationcode=req.getParameter( "applicanIndusName");
					
					List<ApplicationIndustryCode> coutries=migratesevice.getAllApplicationCodeName(a.getApplication());
					 
					for(ApplicationIndustryCode a2:coutries)
					{
						
						applicationname=a2.getTotalname();
						
					}
					 
					submainobject.put("applicanIndustry",applicationname);
					 
				}
				
				else
				{
					submainobject.put("applicanIndustry",a.getApplicanindustryname());
				}
				 
				submainobject.put("departmentname",a.getDepartmentname());
				submainobject.put("reports",a.getReports());
				if(a.getSalesPerson()!=null)
				{
					submainobject.put("salesPerson",a.getSalesPerson());
					
				}
				
				else
				{
					String salesman=masterservice.getSalesmanOnSalesCode(a.getSalesPersonCode());
					submainobject.put("salesPerson",salesman);
					
					
				}
				 
				submainobject.put("discountmultiplier",a.getDiscountmultiplier());
				submainobject.put("salesPersonCode",a.getSalesPersonCode());
				submainobject.put("leadsource",a.getLeadsource());
				submainobject.put("currency", a.getCurrency());
				submainobject.put("taxName",a.getTaxName());
				submainobject.put("paymentTerm",a.getPaymentTerm());
				submainobject.put("opportunityAmount", a.getOpportunityAmount());
				if(a.getBddate()!=null)
				{
					String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
					submainobject.put("bddate",k1 );
				}
				
				
				submainobject.put("createdby",a.getCreatedby());
				submainobject.put("lastmodifiedby",a.getLastmodifiedby());
				submainobject.put("imagename", a.getImagename());
				 
				custmerarray.add(submainobject);
				mainobject.put("custmer",custmerarray);
			    
				
				 
				 /*custmerobject.put("account",a);
				custmerarray.add(custmerobject);*/
		/*	String k1	=DateUtils.formatDate(a.getBddate(),Constants.GenericDateFormat.DATE_FORMAT);
			String k2 = k1.replace("-","/");
				 
			String[] k2=k1.split("-");
			 String k3=k2[2]+"-"+k2[1]+"-"+k2[0];
			*/
			
			/*DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");*/
			
		/*	Date frmDate = sdf.parse(k2);*/
			/*	mainobject.put("date",k2);*/
				//mainobject.put("date",k1);
				mainobject.put("check",1);
				//mainobject.put("custmer",a);
				//mainobject.put("custid",a.getCustid());
				List<CustmerContactDetails> contactdetails=a.getRolescreens();
				int i=1;
				
				if(contactdetails.size()==0)
				{
					
					JSONObject subobject=new JSONObject();
					subobject.put("id",i);
					subobject.put("contactName",null);
					subobject.put("contactNumber",null);
					subobject.put("contactAltNumber",null);
					subobject.put("gdepatment",null);
					subobject.put("email",null);
					subobject.put("gtitle",null);
					mainarray.add(subobject);
					
					 
					
				}
				
				for(CustmerContactDetails b:contactdetails)
				{
					JSONObject subobject=new JSONObject();
					subobject.put("id",i);
					subobject.put("contactName",b.getContactName());
					subobject.put("contactNumber",b.getContactNumber());
					subobject.put("contactAltNumber",b.getContactAltNumber());
					subobject.put("department",b.getDepartment());
					subobject.put("emailAddr",b.getEmailAddr());
					subobject.put("titleName",b.getTitleName());
					mainarray.add(subobject);
					
					i++;
				}
				
			}
			/*mainobject.put("custmer",custmerarray);	 */
			mainobject.put("grid",mainarray );
			//mainobject.put("custmer",custmerarray);
			
	/*************************************************for getting number of open casess on this custmer****************************************************/
			
			//String custmername=req.getParameter("custmername");
			//JSONObject mainobject=new JSONObject();
		    JSONArray  custmernamesonarray=new JSONArray();
		   // HttpSession hs=req.getSession();
		    String user=(String)hs.getAttribute("user");
			List<CaseSummary> custmernames=masterservice.getAllCustmerNamesInCasess(accountname.replace("'","''"),user);
		int j=1;
			for(CaseSummary custmernamecase:custmernames)
			{
				JSONObject custmer=new JSONObject();
				custmer.put("id",j);
				custmer.put("caseRef",custmernamecase.getCaseRef());
				custmer.put("customer",custmernamecase.getCustomer());
				custmer.put("assignto",custmernamecase.getAssigned());
				
				Date d2 = custmernamecase.getDtCase();
				if (d2 != null) {
					String k1 = DateUtils.formatDate(d2, Constants.GenericDateFormat.DATE_FORMAT);
					custmer.put("dateCase",k1);

				}
				 
				Date d3 = custmernamecase.getDateExp();
				if (d3 != null) {
					String k1 = DateUtils.formatDate(d3, Constants.GenericDateFormat.DATE_FORMAT);
					custmer.put("dateexp",k1);

				}
				
				
				
				custmer.put("caseType",custmernamecase.getCasetype());
				custmer.put("description",custmernamecase.getDescription());
				custmer.put("tab",custmernamecase.getTab());
				 
				custmernamesonarray.add(custmer);
				j++;
				
			}
			mainobject.put("casedata",custmernamesonarray);


			
			
			
	/***********************************************end of geeting of open casess  of this custmer**************************/
			
			
			
	/*********************START OF QUATATION OBJECT**********************/
			JSONArray 	jarray=this.getSavedQuatationDataOnCustmerName(accountname.replace("'","''"));
			
			mainobject.put("quatationgrid",jarray);
			
			
	/******************END OF QUATATION OBJECT***********************************************/
			
			
			
	/***************OPen ordrs from ytdbooking*************************************/
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			Session session=hibernateDao.getSessionFactory().openSession();
			String openorders="select count(*) from ytdbooking where  bookingyear="+year+"  and customercode='"+custmername1+"'" ;
			SQLQuery query=session.createSQLQuery(openorders);
			List<Integer> v=query.list();
			for(Integer k:v)
			{
				mainobject.put("open",k);
			}
	/*************************************End Open Orders from Ytdbooking*********/		
				 
				 return mainobject;
	 
		   
		   
		   
		   
		   
		   
				 
		}
	
	
	
	
	
	
	 
	public JSONArray getSavedQuatationDataOnCustmerName(String custmername)
	{
		

		/*HttpSession hs=req.getSession();*/
		//String caserefrence=(String)hs.getAttribute("caseref");
		//String custmername=req.getParameter("custmernmae");
		int qid=0;
		
		
		Session session=hibernateDao.getSessionFactory().openSession();
		String hql = "from Quotation where  custName='"+custmername+"'";
		Query query = session.createQuery(hql);
		List<Quotation> results = query.list();
		JSONObject quatation=new JSONObject();
		JSONArray quatatinformarray=new JSONArray();
		int k=1;
		for(Quotation f:results)
		{
			
			 
			qid=f.getQid();
			JSONObject quatatinsubobject=new JSONObject();
			quatatinsubobject.put("id",k);
			quatatinsubobject.put("qtnType",f.getQtnType());
			quatatinsubobject.put("qtnNo",f.getQtnNo());
			quatatinsubobject.put("custName",f.getCustName());
			quatatinsubobject.put("custCode",f.getCustCode());
			quatatinsubobject.put("paymentTerm",f.getPaymentTerm());
			quatatinsubobject.put("qtnProgress",f.getQtnProgress());
			quatatinsubobject.put("profit",f.getProfit());
			
			quatatinsubobject.put("salesmanPhNo",f.getSalesmanPhNo());
			quatatinsubobject.put("createdBy",f.getCreatedBy());
			 
			
			
			
			if (f.getDateQuote() != null) {
				//d1 = data.getDateExp();
				String k1 = DateUtils.formatDate(f.getDateQuote(),Constants.GenericDateFormat.DATE_FORMAT);
				quatatinsubobject.put("dateQuote", k1);
	                                   }
			quatatinsubobject.put("currency",f.getCurrency());
			if (f.getDateValid() != null) {
				//d1 = data.getDateExp();
				String k1 = DateUtils.formatDate(f.getDateValid(),Constants.GenericDateFormat.DATE_FORMAT);
				quatatinsubobject.put("dateValid", k1);
	}
			
			quatatinsubobject.put("tax",f.getTax());
			quatatinsubobject.put("copy",f.getCopy());
			quatatinsubobject.put("country",f.getCountry());
			quatatinsubobject.put("salesMan",f.getSalesMan());
			quatatinsubobject.put("taxPercent",f.getTaxPercent());
			quatatinsubobject.put("telephone",f.getTelephone());
			quatatinsubobject.put("custRef",f.getCustRef());
			quatatinsubobject.put("salesManCode",f.getSalesManCode());
			quatatinsubobject.put("taxPercent",f.getTaxPercent());
			quatatinsubobject.put("telephone",f.getTelephone());
			quatatinsubobject.put("custRef",f.getCustRef());
	        quatatinsubobject.put("discount",f.getDiscount());
			quatatinsubobject.put("email",f.getEmail());
			quatatinsubobject.put("payment",f.getPayment());
			quatatinsubobject.put("salesTerm",f.getSalesTerm());
			quatatinsubobject.put("extPercent",f.getExtPercent());
			quatatinsubobject.put("item",f.getItem());
			quatatinsubobject.put("showCost",f.getShowCost());
			quatatinsubobject.put("subject",f.getSubject());
			quatatinsubobject.put("application",f.getApplication());
			quatatinsubobject.put("industry",f.getIndustry());
			quatatinsubobject.put("freight", f.getFreight());
			quatatinsubobject.put("freightTotal",f.getFreightTotal());
			quatatinsubobject.put("courier",f.getCourier());
			quatatinsubobject.put("courierTotal",f.getCourierTotal());
			quatatinsubobject.put("qid",f.getQid());
			quatatinsubobject.put("salesPerson",f.getSalesPerson());
			quatatinsubobject.put("emailAddr",f.getEmailAddr());
			quatatinsubobject.put("assigned",f.getAssigned());
			quatatinsubobject.put("caseRef", f.getCaseRef());
			quatatinsubobject.put("remarks",f.getRemarks());
			quatatinsubobject.put("modelTerm",f.getModelTerm());
			quatatinsubobject.put("attention",f.getAttention());
			quatatinsubobject.put("subTotal",f.getSubTotal());
			quatatinsubobject.put("grandTotal",f.getGrandTotal());
			quatatinsubobject.put("fax",f.getFax());
			quatatinsubobject.put("addedTax",f.getAddedTax());
			if(f.getFlag()==1)
			{
				quatatinsubobject.put("action",1);
				
			}
			
			else
			{
				quatatinsubobject.put("action",0);
				
			}
			
			
			
			
			quatatinformarray.add(quatatinsubobject);
			   		
			 
			  k++;
			
			
			 
			
		}
		
		   		return quatatinformarray;

	}

		 
     
	

}