package com.finsol.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.finsol.bean.HistoricalDataYearBean;
import com.finsol.bean.HistoricalDataYearlyHistoryBean;
import com.finsol.bean.RegionBean;
import com.finsol.bean.YearlyBudgetBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.Country;
import com.finsol.model.ItemAndScore;
import com.finsol.model.ProductCategory;
import com.finsol.model.Region;
import com.finsol.model.Stage;
import com.finsol.model.YearlyBudget;
import com.finsol.model.YearlyHistory;
import com.finsol.service.BSS_CommonService;
import com.finsol.service.BSS_SalesManagementService;

/**
 * @author Naidu
 * 
 */
@Controller
public class BSS_SalesManagementController {
	
	private static final Logger logger = Logger.getLogger(BSS_SalesManagementController.class);
	
	@Autowired
	private BSS_CommonService bsscommonService;
	
	@Autowired
	private BSS_SalesManagementService salesManagementService;
	
	@Autowired
	private HibernateDao hibernateDao;
	

	/*-------------------------------------- Common Methods ----------------------------------------------*/

	@RequestMapping(value = "/getMxId", method = RequestMethod.POST)
	public @ResponseBody
	String getMxId(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tableName=(String)jsonData.get("tablename");
		Integer maxId = bsscommonService.getMaxID(tableName);
		
		JSONObject json = new JSONObject();
		json.put("id", maxId);
		return json.toString();
	}		
	
	/* ---------To load the DropDown values into corresponding controls --------- */
	@RequestMapping(value = "/loadDropDown", method = RequestMethod.POST)
	public @ResponseBody
	List loadDropDown(@RequestBody JSONObject jsonData) throws Exception 
	{
		String dropdownName=(String)jsonData.get("dropdownname");
		String columnName=(String)jsonData.get("columnname");	
		String columnCode=(String)jsonData.get("columncode");
		List dropDownList = bsscommonService.getDropdownNames(columnName,columnCode,dropdownName);
		return dropDownList;
	}
	
	/* ---------------To load the DropDown values into corresponding controls based on previous dropdown values --------- */
	@RequestMapping(value = "/loadDropDownOnChange", method = RequestMethod.POST)
	public @ResponseBody
	List onChangeDropDownValues(@RequestBody JSONObject jsonData) throws Exception 
	{
		List dropDownList =new ArrayList();
		String dropdownName=(String)jsonData.get("targetdropdownname");
		String columnName=(String)jsonData.get("targetcolumnname");
		String columnCode=(String)jsonData.get("targetcolumncode");		
		String whereColumnname=(String)jsonData.get("columnname");			
		String value=jsonData.get("value").toString();
		
		dropDownList = bsscommonService.onChangeDropDownValues(dropdownName,columnName,columnCode,whereColumnname,value);
		if(dropDownList==null)
			dropDownList =new ArrayList();
		return dropDownList;
	}
	
	/* ---------------------- To check the entered value in the field exists or not ------------------- */
	@RequestMapping(value = "/checkValue", method = RequestMethod.POST)
	public @ResponseBody
	Boolean checkValue(@RequestBody JSONObject jsonData) throws Exception 
	{
		Boolean isExist = false;
		String tablename=(String)jsonData.get("tablename");
		String columnname=(String)jsonData.get("columnname");
		String value=(String)jsonData.get("value");
		
		isExist = bsscommonService.checkValueExistOrNot(tablename,columnname,value);
		
		return isExist;			
	}
	@RequestMapping(value = "/checkValueExist", method = RequestMethod.POST)
	public @ResponseBody
	Boolean checkValueExist(@RequestBody String value) throws Exception 
	{
		Boolean isExist = false;
		
		isExist = bsscommonService.checkValueExist(value);
		
		return isExist;			
	}
	
	/*------------------------------------- End Of Common Methods ------------------------------------------*/
	
	/*------------------------------------- Region Master -------------------------------------------------*/
	
	@RequestMapping(value = "/onLoadRegionValidation", method = RequestMethod.POST)
	public @ResponseBody
	Boolean onLoadRegionValidation(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tablename=(String)jsonData.get("tablename");
		String columnname=(String)jsonData.get("columnname");
		String whereColumnname=(String)jsonData.get("wherecolumnname");		
		String value=jsonData.get("value").toString();
		
		Boolean valid = salesManagementService.onLoadRegionValidation(tablename,columnname,whereColumnname,value);
		
		return valid;		
	}
	
	@RequestMapping(value = "/saveRegionMaster", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveRegionMaster(@RequestBody JSONObject jsonData) throws Exception 
	{
		boolean res  = false;
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			JSONObject formdata=(JSONObject)jsonData.get("formData");
			
			String countrycode=(String)formdata.getString("countrycode");

			//JSONObject formData=(JSONObject) jsonData.get("formData");
			JSONArray gridData=(JSONArray) jsonData.get("gridData");			
			Session session=hibernateDao.getSessionFactory().openSession();
			
			for(int i=0;i<gridData.size();i++)
			{
				RegionBean regionbean = new ObjectMapper().readValue(gridData.get(i).toString(), RegionBean.class);
				Region region = prepareModel(regionbean,countrycode);
				entityList.add(region);
				
				String updateregioninytdbookingregion="update Salesman set regstatus="+region.getStatus()+" where regioncode='"+region.getRegionid()+"'";
				
				SQLQuery updateregion  =session.createSQLQuery(updateregioninytdbookingregion);
				updateregion.executeUpdate();
				
				String updateregioninytdbookingregion1="update YtdBookingRegion set regstatus="+region.getStatus()+" where region='"+region.getRegionid()+"'";
				SQLQuery updateregion1  =session.createSQLQuery(updateregioninytdbookingregion1);
				updateregion1.executeUpdate();
				
				String updateregioninytdbookingregion2="update YtdBooking set regstatus="+region.getStatus()+" where regioncode='"+region.getRegionid()+"'";
				SQLQuery updateregion2  =session.createSQLQuery(updateregioninytdbookingregion2);
				updateregion2.executeUpdate();
				
			}
						
			//String salesmanCode=(String)jsonData.get("salesmanCode");		
			//if(regionbean.getModifyFlag().equalsIgnoreCase("Yes"))
			
				isInsertSuccess = salesManagementService.saveMultipleEntities(entityList);
				
			//else
			//	isInsertSuccess = salesManagementService.saveEntities(entityList);
			//
			//res= prepareModelForSales(regionbean,salesmanCode);
			//Boolean fr=false;
			//if(res && isInsertSuccess)
			//{
			//	fr=true;
			//	return fr;						
			//}
			//else
			//{					
				//return fr;
			//}		
				
				return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private Region prepareModel(RegionBean regionbean,String countrycode)
	{	
		Region region = new Region();
		
		region.setCountrycode(countrycode);
		region.setRegion(regionbean.getRegion());
		region.setDescription("NA");
		region.setRegioncode(regionbean.getRegionCode());
		region.setStatus(regionbean.getStatus());
		region.setId(regionbean.getId());
		region.setRegionid(regionbean.getRegionid());
		return region;
	}
	
/*	private Boolean prepareModelForSales(RegionBean regionbean,String salesmanCode)
	{
		salesManagementService.deleteRegionSales(regionbean.getRegionCode());
		
		List entityList = new ArrayList();
		Boolean isInsertSuccess=false;
		Regionsalesmen regionsalesman=null;
		
		String modifyFlag = regionbean.getModifyFlag();
	 
		String slmanCode[]=salesmanCode.split(",");
		
		for(int i=0;i<slmanCode.length;i++)
		{
			regionsalesman = new Regionsalesmen();
	    	
			regionsalesman.setCountrycode(regionbean.getCountryCode());				
			regionsalesman.setSalesmancode(slmanCode[i]);
			regionsalesman.setDescription("NA");
			regionsalesman.setRegioncode(regionbean.getRegionCode());
			regionsalesman.setStatus(regionbean.getStatus());
			entityList.add(regionsalesman);
		}
	    	
    
	    isInsertSuccess = salesManagementService.saveEntities(entityList);
	    return isInsertSuccess;
	}*/
	
	@RequestMapping(value = "/getRegion", method = RequestMethod.POST)
	public @ResponseBody
	List<RegionBean> getRegion(@RequestBody String countrycode) throws Exception 
	{
		//String regioncode=jsonData.getString("regioncode");
		org.json.JSONObject response = new org.json.JSONObject();
		List salesmenList = null;
		Boolean isMaster=false;
		String salesmen="";
		
		
		List<RegionBean> regionBean=prepareBeanForRegion(salesManagementService.getRegionByCountryCode(countrycode));
		
		/*Region region = (Region)bsscommonService.getEntityByIdVarchar(regioncode, Region.class);
		
		RegionBean regionBean=new RegionBean();
		
		
		salesmenList = salesManagementService.getRegionSalesMenByRegionCode(regioncode);
		
		
		for(int i=0;i<salesmenList.size();i++)
		{
			Map rlMap=(Map)salesmenList.get(i);
			String salesmancode=(String)rlMap.get("salesmancode");
			if(i==0)
				salesmen=salesmancode;
			else
				salesmen=salesmen+","+salesmancode;

		}
		org.json.JSONObject regionJson=new org.json.JSONObject(regionBean);
		response.put("region",regionJson);
		response.put("salesmanCode",salesmen);	*/
		
		//org.json.JSONObject regionJson=new org.json.JSONObject(regionBean);
		//response.put("region",regionJson);
		
		return regionBean;
	}
	
	
	private List<RegionBean> prepareBeanForRegion(List<Region> regions) 
	{
		List<RegionBean> beans = new ArrayList<RegionBean>();;
		if (regions != null && !regions.isEmpty()) 
		{
			//beans = new ArrayList<RegionBean>();
			RegionBean bean = null;
			for (Region region : regions)
			{
				bean = new RegionBean();
				bean.setRegionCode(region.getRegioncode());
				bean.setRegion(region.getRegion());
				bean.setStatus(region.getStatus());
				bean.setRegionid(region.getRegionid());
				bean.setId(region.getId());
				
				beans.add(bean);
			}
		}
		return beans;
	}
	
	

	
	
	/*-------------------------------------- End Region Master -----------------------------------------------------*/
	
/*------------------------------------- Historical Data Master ----------------------------------------------------*/
	
	@RequestMapping(value = "/saveHistoricalData", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveHistoricalData(@RequestBody JSONObject jsonData) throws Exception 
	{		
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{
			JSONObject formData=(JSONObject) jsonData.get("formData");
			JSONArray gridData=(JSONArray) jsonData.get("gridData");			
			
			HistoricalDataYearBean historicalDataYearBean = new ObjectMapper().readValue(formData.toString(), HistoricalDataYearBean.class);

			for(int i=0;i<gridData.size();i++)
			{
				HistoricalDataYearlyHistoryBean historicalDataYearlyHistoryBean = new ObjectMapper().readValue(gridData.get(i).toString(), HistoricalDataYearlyHistoryBean.class);
				YearlyHistory yearlyHistory = prepareModelForYearlyHistory(historicalDataYearlyHistoryBean,historicalDataYearBean);
				entityList.add(yearlyHistory);
			}				
			isInsertSuccess = salesManagementService.saveMultipleEntities(entityList);		
			
			return isInsertSuccess;
		}
		 catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
	}

	
	private YearlyHistory prepareModelForYearlyHistory(HistoricalDataYearlyHistoryBean historicalDataYearlyHistoryBean,HistoricalDataYearBean historicalDataYearBean) 
	{
		YearlyHistory yearlyHistory=new YearlyHistory();
		
		yearlyHistory.setBookingvalue(historicalDataYearlyHistoryBean.getBookingValue());
		
		yearlyHistory.setProductname(historicalDataYearlyHistoryBean.getProductName());
		yearlyHistory.setCountrycode(historicalDataYearBean.getCountryCode());
		yearlyHistory.setSlno(historicalDataYearlyHistoryBean.getSlno());
		
		yearlyHistory.setPccode(historicalDataYearBean.getPcCode());
		yearlyHistory.setYear(historicalDataYearBean.getYear());
		yearlyHistory.setCurrencycode("NA");
		yearlyHistory.setPcname("NA");
		yearlyHistory.setProductcode("NA");
		
		return yearlyHistory;
	}
	
	@RequestMapping(value = "/getProductsByCountry", method = RequestMethod.POST)
	public @ResponseBody
	List getProductsByCountry(@RequestBody JSONObject jsonData) throws Exception 
	{
		String country=jsonData.getString("countryCode");
		Integer year=jsonData.getInt("year");
		String isMaster="false";
		
		List productsList = null;
		productsList=prepareBeanForHistoryByCountry(salesManagementService.getProductsByCategory(country,year,isMaster));
		
		return productsList;
	}
	
	@RequestMapping(value = "/getProductsByCategory", method = RequestMethod.POST)
	public @ResponseBody
	List getProductsByCategory(@RequestBody JSONObject jsonData) throws Exception 
	{
		String country=jsonData.getString("countryCode");
		Integer year=jsonData.getInt("year");
		String pccode=jsonData.getString("pcCode");
		
		List productsList = null;
		productsList=prepareBeanForHistoryByProduct(salesManagementService.getProductsByCategory(country,year,pccode));
		return productsList;
	}
	

	private List<HistoricalDataYearlyHistoryBean> prepareBeanForHistoryByCountry(List yearlyhistory) 
	{
		List<HistoricalDataYearlyHistoryBean> beans = null;
		if (yearlyhistory != null && !yearlyhistory.isEmpty()) 
		{
			beans = new ArrayList<HistoricalDataYearlyHistoryBean>();
			HistoricalDataYearlyHistoryBean bean = null;
			for(int i=0;i<yearlyhistory.size();i++)
			{
				Map history1=(Map)yearlyhistory.get(i);
			
				bean = new HistoricalDataYearlyHistoryBean();
				bean.setProductName((String)history1.get("productname"));
				bean.setBookingValue((Double)history1.get("bookingvalue"));
				bean.setSlno((Integer)history1.get("slno"));
				beans.add(bean);
			}
		}
		return beans;
	}

	private List<HistoricalDataYearlyHistoryBean> prepareBeanForHistoryByProduct(List yearlyhistory) 
	{
		List<HistoricalDataYearlyHistoryBean> beans = null;
		if (yearlyhistory != null && !yearlyhistory.isEmpty()) 
		{
			beans = new ArrayList<HistoricalDataYearlyHistoryBean>();
			HistoricalDataYearlyHistoryBean bean = null;
			for(int i=0;i<yearlyhistory.size();i++)
			{
				Map history1=(Map)yearlyhistory.get(i);
				bean = new HistoricalDataYearlyHistoryBean();
				bean.setProductName((String)history1.get("productname"));
				bean.setBookingValue((Double)history1.get("bookingvalue"));
				bean.setSlno((Integer)history1.get("slno"));
				beans.add(bean);
			}
		}
		return beans;
	}

	/*------------------------------------End Historical Data Master----------------------------------------------------*/

	/*------------------------------------ Yearly Budger Master----------------------------------------------------*/
	
	
	
	
	
	
	@RequestMapping(value = "/saveYearlyBudget", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveYearlyBudget(@RequestBody JSONObject jsonData) throws Exception 
	{		
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{
			JSONObject formData=(JSONObject) jsonData.get("formData");
			JSONArray gridData=(JSONArray) jsonData.get("gridData");	

			for(int i=0;i<gridData.size();i++)
			{
				YearlyBudgetBean yearlyBudgerBean = new ObjectMapper().readValue(gridData.get(i).toString(), YearlyBudgetBean.class);
				YearlyBudget yearlyBudget = prepareModelForYearlyBudget(yearlyBudgerBean,formData);
				entityList.add(yearlyBudget);
			}				
			isInsertSuccess = salesManagementService.saveMultipleEntities(entityList);		
			
			return isInsertSuccess;
		}
		 catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
	}

	
	private YearlyBudget prepareModelForYearlyBudget(YearlyBudgetBean yearlyBudgerBean,JSONObject formData) throws Exception
	{		
		//String country=formData.getString("country");
		String countrycode=formData.getString("countryCode");
		//String productcategory=formData.getString("category");
		String pccode=formData.getString("pcCode");
		Integer year=formData.getInt("year");

		
		Country country = (Country)bsscommonService.getEntityByIdVarchar(countrycode, Country.class);
	//ProductCategory productCategory = (ProductCategory)bsscommonService.getEntityByIdVarchar(pccode, ProductCategory.class);
	List<ProductCategory> pr=	bsscommonService.getProductCategory(pccode);
	String productcatgeory=null;
	for(ProductCategory pr1:pr)
	{
		productcatgeory=pr1.getProductcategory();
		
	}
	
	List<com.finsol.model.Currency> currency=salesManagementService.getCurrencyByCountryCode1(countrycode);
	String currencey1=null;
	String currencycode1=null;
	
	for(com.finsol.model.Currency cur:currency)
	{
		currencey1=cur.getCurrency();
		currencycode1=cur.getCurrencycode();
				
		
	}
		//List currencyList=salesManagementService.getCurrencyByCountryCode(countrycode);
		// currency=(Currency)currencyList.get(0);
		
		YearlyBudget yearlyBudget=new YearlyBudget();
		
		yearlyBudget.setMonth1(yearlyBudgerBean.getMonth1());
		yearlyBudget.setMonth2(yearlyBudgerBean.getMonth2());
		yearlyBudget.setMonth3(yearlyBudgerBean.getMonth3());
		yearlyBudget.setMonth4(yearlyBudgerBean.getMonth4());
		yearlyBudget.setMonth5(yearlyBudgerBean.getMonth5());
		yearlyBudget.setMonth6(yearlyBudgerBean.getMonth6());
		yearlyBudget.setMonth7(yearlyBudgerBean.getMonth7());
		yearlyBudget.setMonth8(yearlyBudgerBean.getMonth8());
		yearlyBudget.setMonth9(yearlyBudgerBean.getMonth9());
		yearlyBudget.setMonth10(yearlyBudgerBean.getMonth10());
		yearlyBudget.setMonth11(yearlyBudgerBean.getMonth11());
		yearlyBudget.setMonth12(yearlyBudgerBean.getMonth12());
		yearlyBudget.setQuart1(yearlyBudgerBean.getQuarter1());
		yearlyBudget.setQuart2(yearlyBudgerBean.getQuarter2());
		yearlyBudget.setQuart3(yearlyBudgerBean.getQuarter3());
		yearlyBudget.setQuart4(yearlyBudgerBean.getQuarter4());
		yearlyBudget.setYeartotal(yearlyBudgerBean.getYearTotal());
		yearlyBudget.setProduct(yearlyBudgerBean.getProduct());
		yearlyBudget.setProductcode(yearlyBudgerBean.getProductCode());
		
		yearlyBudget.setSlno(yearlyBudgerBean.getSlno());
		yearlyBudget.setProductcategory(productcatgeory);
		yearlyBudget.setPccode(pccode);
		yearlyBudget.setCountry(country.getCountry());
		yearlyBudget.setCountrycode(countrycode);
		yearlyBudget.setYear(year);
		yearlyBudget.setCurrency(currencey1);
		yearlyBudget.setCurrencycode(currencycode1);
		return yearlyBudget;
	}
	
	
@RequestMapping(value = "/getYearlyBudget", method = RequestMethod.POST)
	public @ResponseBody
	List getYearlyBudget(@RequestBody JSONObject jsonData) throws Exception 
	{
		String countrycode=jsonData.getString("countrycode");
		String year1=jsonData.getString("year");
		int year=Integer.parseInt(year1);
		
		String categorycode=jsonData.getString("categorycode");	
		List yearBudgetList = null;
		Boolean isMaster=false;
		List budgetBeans = null;
		
		yearBudgetList = salesManagementService.getYearlyBudget(countrycode, year,categorycode,isMaster);
		if(yearBudgetList == null)
		{
			isMaster=true;
			budgetBeans = salesManagementService.getYearlyBudget(countrycode, year,categorycode,isMaster);
		}
		else
		{
			budgetBeans =prepareListofBeanForYearlyBudget(yearBudgetList);
		}
		
		return budgetBeans;
	}
	
	
	private List prepareListofBeanForYearlyBudget(List<YearlyBudget> yearBudgets) 
	{
		List<YearlyBudgetBean> beans = null;
		if (yearBudgets != null && !yearBudgets.isEmpty()) 
		{
			beans = new ArrayList<YearlyBudgetBean>();
			YearlyBudgetBean bean = null;
			for (YearlyBudget yearlyBudget : yearBudgets)
			{
				bean = new YearlyBudgetBean();
				bean.setProduct(yearlyBudget.getProduct());
				bean.setProductCode(yearlyBudget.getProductcode());
				bean.setMonth1(yearlyBudget.getMonth1());
				bean.setMonth2(yearlyBudget.getMonth2());
				bean.setMonth3(yearlyBudget.getMonth3());
				bean.setMonth4(yearlyBudget.getMonth4());
				bean.setMonth5(yearlyBudget.getMonth5());
				bean.setMonth6(yearlyBudget.getMonth6());
				bean.setMonth7(yearlyBudget.getMonth7());
				bean.setMonth8(yearlyBudget.getMonth8());
				bean.setMonth9(yearlyBudget.getMonth9());
				bean.setMonth10(yearlyBudget.getMonth10());
				bean.setMonth11(yearlyBudget.getMonth11());
				bean.setMonth12(yearlyBudget.getMonth12());
				bean.setQuarter1(yearlyBudget.getQuart1());
				bean.setQuarter2(yearlyBudget.getQuart2());
				bean.setQuarter3(yearlyBudget.getQuart3());
				bean.setQuarter4(yearlyBudget.getQuart4());
				bean.setYearTotal(yearlyBudget.getYeartotal());
				bean.setSlno(yearlyBudget.getSlno());
				beans.add(bean);
			}
		}
		return beans;
	}
	
	/*------------------------------------End Yearly Budger Master----------------------------------------------------*/
	@RequestMapping(value = "/getScore", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	JSONObject getScore(@RequestBody JSONObject jsonData) throws Exception 
	{			
			//ReadFile rf=new  ReadFile();
			//JobSchedulers jobSchedulers=new JobSchedulers();
			//jobSchedulers.startScheduler();
		
			//readFile.Insert();
			JSONObject respJson=new JSONObject();
			String mNum=(String)jsonData.get("modelno");
			String productCode = Character.toString(mNum.charAt(0));
			String motorType=null;
			String tag=null;
			String stage=null;	
			Stage stages=null;
			String frameSize=null;
			ItemAndScore itemAndScore=null;
			String score=null;
			String itemValue=null;
			String[] spString=mNum.split("-");
			
/*			logger.info(productCode);
			logger.info(spString[0]+"----"+spString[1]+"---"+spString[2]);			
			logger.info(mNum.indexOf("-")+1);
			logger.info(mNum.lastIndexOf("-")+1);
			logger.info(spString[0].contains("M"));	
			logger.info("***************"+mNum.substring(mNum.indexOf("-")+5,mNum.indexOf("-")+7));*/
			
			try 
			{			
				if(spString[0].contains("M"))
				{
					motorType="M";
				}
				else
					motorType="R";
	
				if(productCode.equalsIgnoreCase("C"))
				{				
					int fOccur=mNum.indexOf("-")+1;
					frameSize=mNum.substring(mNum.indexOf("-")+1,mNum.indexOf("-")+5);
					
					//find out stage
					if(spString[1].length()<6)
					{
						stage=mNum.substring(mNum.lastIndexOf("-")+1,mNum.lastIndexOf("-")+2);
						//stage="1";
					}
					else
					{
						String stagecode=mNum.substring(mNum.indexOf("-")+5,mNum.indexOf("-")+7);
						//get stage	 from db stage table(stageCode)			
						stages=(Stage)salesManagementService.getRowByColumn(Stage.class,"stagecode",stagecode);
						if(stages.getStage()!=null)
							stage=stages.getStage().toString();
					}				
				}
				else
				{				
					frameSize=spString[1];					
					// out stage is null
					stage="1";									
				}
				
				if(productCode.equalsIgnoreCase("C") || productCode.equalsIgnoreCase("R"))
				{		
					List frameSizeAndTagList=salesManagementService.getTag(productCode,frameSize);
					if(frameSizeAndTagList!=null)
					{
						Map frameSizeAndTag=(Map)frameSizeAndTagList.get(0);
						if(frameSizeAndTag.get("tag")!=null)
							tag=frameSizeAndTag.get("tag").toString();
					}
					
				}
				else
				{
					tag=productCode;				
				}
				

				
				itemValue=productCode+motorType+tag+"-"+stage;
				
				itemAndScore=(ItemAndScore)salesManagementService.getRowByColumn(ItemAndScore.class,"itemvalue",itemValue);
				
				if(itemAndScore==null)
				{
					score="Not Available";
				}
				else
				{					
					logger.info(itemAndScore.getScore());
					score=itemAndScore.getScore().toString();
				}

				if(tag==null||productCode==null||motorType==null||stage==null)
					itemValue="Not Available";
				
				if(tag==null)
					tag="Not Available";
				if(stage==null)
					stage="Not Available";
				
				respJson.put("modelno", mNum);
				respJson.put("pcode", productCode);
				respJson.put("mtype", motorType);
				respJson.put("tag", tag);
				respJson.put("stage", stage);					
				respJson.put("itemVal", itemValue);					
				respJson.put("score", score);
					
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return respJson;
		}
		
		return respJson;
	}
	
	
	
}


