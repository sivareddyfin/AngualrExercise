package com.finsol.service;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.bean.ApplicationBean;
import com.finsol.bean.IndustryBean;
import com.finsol.bean.TaxMasterBean;
import com.finsol.dao.MasterScreen_TwoDao;
import com.finsol.model.Country;
import com.finsol.model.Salesman;

@Service("MasterScreen_ServiceTwo")
public class MasterScreen_ServiceTwo 
{
	private static final Logger logger = Logger.getLogger( MasterScreen_ServiceTwo.class);
	
	@Autowired
	private MasterScreen_TwoDao masterscreendao;

	public List<Country> getCountriesandCountryCodes() {
		// TODO Auto-generated method stub
		return masterscreendao.getCountriesandCountryCodes();
	}

	public void saveCountry(ArrayList<Country> al) {
		// TODO Auto-generated method stub
		masterscreendao.saveCountry(al);
		
	}

	public List<Salesman> getAllSalesman(String countrycode) {
		// TODO Auto-generated method stub
		return masterscreendao.getAllSalesman(countrycode);
	}

	public void saveSalesman(List<Salesman> m, String countrycode) {
		// TODO Auto-generated method stub
		masterscreendao.saveSalesman(m,countrycode);
		
	}

	public void saveTaxMaster(String countrycode, ArrayList<TaxMasterBean> al)
	{
		// TODO Auto-generated method stub
		masterscreendao.saveTaxMaster(countrycode,al);
	}

	 

	public void saveIndustryMaster(ArrayList<IndustryBean> al)
	{
		// TODO Auto-generated method stub
		masterscreendao.saveIndustryMaster(al);
		
	}

	public void saveApplication(ArrayList<ApplicationBean> al)
	{
		// TODO Auto-generated method stub
		masterscreendao.saveApplication(al);
	}
	
	

	
}