package com.finsol.service;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.dao.BSS_SalesManagementDao;
import com.finsol.dao.HibernateDao;


/**
 * @author Rama Krishna
 * 
 */
@Service("BSS_SalesManagementService")
public class BSS_SalesManagementService {

	private static final Logger logger = Logger.getLogger(BSS_SalesManagementService.class);
	
	@Autowired
	private BSS_SalesManagementDao salesManagementDao;
	@Autowired
	private  HibernateDao hibernateDao;

	public Integer getMaxID()
    {
		Integer maxVal=(Integer) salesManagementDao.getMaxValue("select max(id) from BSS_Region");
		if(maxVal==null)
			maxVal=0;
        return maxVal;
    }
	public Boolean onLoadRegionValidation(String tableName,String columnname,String whereColumnname,String value)
    {	
		String strQuery = "Select * from "+ tableName+" where "+whereColumnname+"='"+value+"'";
		
		List dropdownList=hibernateDao.findBySqlCriteria(strQuery);
		
		if(dropdownList.size()>0 && dropdownList!=null)
			return true;
		else
			return false;
				
	}
	
	public boolean saveMultipleEntities(List entitiesList)
	{
		return salesManagementDao.saveMultipleEntities(entitiesList);    
	}
	public boolean saveEntities(List entitiesList)
	{
		return salesManagementDao.saveEntities(entitiesList);    
	}
	
	public List getProductCategory(String countrycode,Integer year,Boolean isMaster)
	{
		return salesManagementDao.getProductCategory(countrycode,year,isMaster);
	}
	
	public List getRegionSalesMenByRegionCode(String regionCode)
	{
		return salesManagementDao.getRegionSalesMenByRegionCode(regionCode);
	}
	
	public List getProductsByCategory(String categorycode,Boolean isMaster)
	{
		return salesManagementDao.getProductsByCategory(categorycode,isMaster);
	}
	
	public List getYearlyBudget(String countrycode,Integer year,String pccode,Boolean isMaster)
	{
		return salesManagementDao.getYearlyBudget(countrycode,year,pccode,isMaster);
	}
	
	public List getCurrencyByCountryCode(String countrycode)
	{
		return salesManagementDao.getCurrencyByCountryCode(countrycode);
	} 
	
	public List getRegionByCountryCode(String countrycode)
	{
		return salesManagementDao.getRegionByCountryCode(countrycode);
	} 
	
	
	public void deleteRegionSales(String regionCode) {
		salesManagementDao.deleteRegionSales(regionCode);
	}
	
	public List getProductsByCategory(String country,Integer year,String isMaster)
	{
		return salesManagementDao.getProductsByCategory(country,year,isMaster);
	}
	public Object getRowByColumn(Class className,String fName,String value)
	{
		return salesManagementDao.getRowByColumn(className,fName,value);
	}
	public List getTag(String pcode,String framesize)
	{
		return salesManagementDao.getTag(pcode,framesize);
	}
	public List<com.finsol.model.Currency> getCurrencyByCountryCode1(String countrycode) {
		// TODO Auto-generated method stub
		return salesManagementDao.getCurrencyByCountryCode1(countrycode);
	}
	
}
