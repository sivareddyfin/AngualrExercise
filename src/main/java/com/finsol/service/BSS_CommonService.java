package com.finsol.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.dao.BSS_CommonDao;
import com.finsol.dao.HibernateDao;
import com.finsol.model.ProductCategory;

/**
 * @author naidu
 *
 */
@Service("BSS_CommonService")
public class BSS_CommonService {

	private static final Logger logger = Logger.getLogger(BSS_CommonService.class);

	@Autowired
	private BSS_CommonDao bsscommonDao;
	
	@Autowired
	private HibernateDao hibernateDao;
	
/*	@Autowired
	private HttpServletRequest request;

	*/
	
	public Integer getMaxID(String tableName)
    {
		Integer maxVal=(Integer) bsscommonDao.getMaxValue("select max(regioncode) from "+tableName);
		// if(maxVal==null)
			//maxVal=0;

		//maxVal = maxVal + 1;
		
        return maxVal;
    }
	

	public List getDropdownNames(String columnName,String columnCode,String tableName)
    {	
		try 
		{
			String sqlQuery = "Select "+ columnName+","+ columnCode+" from "+ tableName+"";
			
			if(columnCode.equalsIgnoreCase("budgetflag"))
			{
				sqlQuery = "Select "+ columnName+","+ columnCode+" from "+ tableName+" where budgetflag = 0";
			}
			else if(columnCode.equalsIgnoreCase("historyflag"))
			{
				sqlQuery = "Select "+ columnName+","+ columnCode+" from "+ tableName+" where historyflag = 0";
			}
			else if(tableName.equalsIgnoreCase("Country"))				
				sqlQuery = "Select "+ columnName+","+ columnCode+" from "+ tableName+" where countrycode <> 'HK'";
			else if(tableName.equalsIgnoreCase("SalesProgressIndicators"))	
					sqlQuery = "Select "+ columnName+","+ columnCode+" from "+ tableName+" where status=0";
			
			else
				sqlQuery = "Select "+ columnName+","+ columnCode+" from "+ tableName+"";
			
			List dropdownList=bsscommonDao.getDropdownNames(sqlQuery);
			
			return dropdownList;
			
		} catch (Exception e) 
		{
            logger.info(e.getCause(), e);
            return null;
		}
		
	}
	
	public List onChangeDropDownValues(String tableName,String columnName,String columnCode,String whereColumnname,String value)
    {	
		try 
		{
			String strQuery="Select "+ columnName+","+ columnCode+" from "+ tableName+" where  "+ whereColumnname +"='"+value+"'";
			
			if("Region".equalsIgnoreCase(tableName))
			{
				strQuery="Select "+ columnName+","+ columnCode+" from "+ tableName+" where  "+ whereColumnname +"='"+value+"' and status=0";
			}
			List dropdownList=bsscommonDao.getDropdownNames(strQuery);		
			
			return dropdownList;
			
		} catch (Exception e) 
		{
            logger.info(e.getCause(), e);
            return null;
		}		
	}
	
	 public Object getEntityByIdVarchar(String id,Class className) 
	 {
		 return bsscommonDao.getEntityByIdVarchar(id,className);
	 }
	
		public Boolean checkValueExistOrNot(String tableName,String columnname,String value) throws Exception
	    {	
			try
			{		
				Object obj=hibernateDao.findUniqueResult(Class.forName(""+tableName+"").newInstance(),columnname,value);
				
				if(obj !=null)
					return true;
				else
					return false;
		    }catch(Exception e)
		    {
				logger.info(e.getCause(), e);
				return null;
		    }
					
		}
		
		
		public Boolean checkValueExist(String value) throws Exception
	    {	
			Integer count=0;
			try
			{		
				Session session = (Session)hibernateDao.getSessionFactory().openSession();
				String q="select count(regioncode) from Region where regioncode='"+value+"'";
				Query a=session.createQuery(q);
				List<Long> l=a.list();
				if(l.size()!=0)
				{
				 for(Long k:l)
				 {
					 count= k.intValue();
				 }

				}
				if(count >0)
					return true;
				else
					return false;
		    }catch(Exception e)
		    {
				logger.info(e.getCause(), e);
				return null;
		    }					
		}
		public Integer getMaximumID(String columnName,String tableName)
	    {
			Integer maxVal=(Integer) bsscommonDao.getMaxValue("select max("+columnName+") from "+tableName);
			 if(maxVal==null)
				maxVal=0;

			maxVal = maxVal + 1;
			
	        return maxVal;
	    }
		
		public String getSequenceNumber(String processName, String countryCode2,String usernasme)
		{
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			//int month = now.get(Calendar.MONTH) + 1;
			//int date = now.get(Calendar.DATE);
			String sequence=null;
			Integer slno=null;
			
			/*HttpSession session=request.getSession(false);
			String countryCode="kk";*/
			/*HttpSession session=request.getSession(false);
			String countryCode=(String)session.getAttribute("countrycode");
			
			
*/
			
			String countryCode=countryCode2;
			HashMap<String, String> hm=new HashMap<String, String>();
			
			hm.put("TH", "T");
			hm.put("ID", "D");
			hm.put("MY", "M");
			hm.put("PH", "P");
			hm.put("VN", "V");
			hm.put("SG", "S");
			hm.put("all", "S");
			
			String countryInitial=hm.get(countryCode);

			if("Q".equalsIgnoreCase(processName))
			{	
				
				
			//	String usernasme=(String)session.getAttribute("user");
				//String usernasme="gh";
				String firsttwocharcters=usernasme.substring(0,2);
				slno=(Integer)hibernateDao.getMaxValue("select max(serialno) from SequenceNumber where processname='"+processName+"' and countrycode='"+countryInitial+"' and seqyear="+year%1000+"");
				if(slno==null)
				{
					slno=0;
					sequence=processName+"-"+countryInitial+firsttwocharcters+(year%1000)+"-"+"0000"+(slno+1)+"-"+"A";
				}
				
				if(slno!=null)
				{
					int length = (int)(Math.log10(slno)+1);
				if(length==1)
				{
				sequence=processName+"-"+countryInitial+firsttwocharcters+(year%1000)+"-"+"0000"+(slno+1)+"-"+"A";
				}
					
				if(length==2)
				{
				sequence=processName+"-"+countryInitial+firsttwocharcters+(year%1000)+"-"+"000"+(slno+1)+"-"+"A";
				}
				
				if(length==3)
				{
				sequence=processName+"-"+countryInitial+firsttwocharcters+(year%1000)+"-"+"00"+(slno+1)+"-"+"A";
				}
				
				if(length==4)
				{
				sequence=processName+"-"+countryInitial+firsttwocharcters+(year%1000)+"-"+"0"+(slno+1)+"-"+"A";
				}


				if(length>=5)
				{
				sequence=processName+"-"+countryInitial+firsttwocharcters+(year%1000)+"-"+(slno+1)+"-"+"A";
				}


					
					
					
				}//To Do Code
			}
			else
			{				
				slno=(Integer)hibernateDao.getMaxValue("select max(serialno) from SequenceNumber where processname='"+processName+"' and countrycode='"+countryInitial+"' and seqyear="+year%1000+"");
	
				if(slno==null)
				{
					slno=0;
					sequence=processName+"-"+countryInitial+(year%1000)+"-"+"0000"+(slno+1);
				}
				
				if(slno!=null)
				{
					int length = (int)(Math.log10(slno)+1);
					if(length==1)
					{
						sequence=processName+"-"+countryInitial+(year%1000)+"-"+"0000"+(slno+1);
						
					}
					
					if(length==2)
					{
						sequence=processName+"-"+countryInitial+(year%1000)+"-"+"000"+(slno+1);
						
					}
					if(length==3)
					{
						sequence=processName+"-"+countryInitial+(year%1000)+"-"+"00"+(slno+1);
					}
					if(length==4)
					{
						sequence=processName+"-"+countryInitial+(year%1000)+"-"+"0"+(slno+1);
						
					}
					if(length>=5)
					{
						sequence=processName+"-"+countryInitial+(year%1000)+"-"+(slno+1);
					}
				
				
				}
			}
			return sequence;
		}
		
		public Object getEntityByDate(Date id,Class className) 
		{
			//logger.info("----------class Name---------"+className);
			return bsscommonDao.getEntityByDate(id,className);
		}
		
		public Object getEntityById(Integer id,Class className) 
		{
			//logger.info("----------class Name---------"+className);
			return bsscommonDao.getEntityById(id,className);
		}
		  

		public Integer executeUpdate(String strQry)
		{
		Session session = (Session)hibernateDao.getSessionFactory().openSession();	
		SQLQuery sqlQuery=session.createSQLQuery(strQry);
		Integer updatedRows=sqlQuery.executeUpdate();
		return updatedRows;
		}


		public List<ProductCategory> getProductCategory(String pccode) {
			Session session = (Session)hibernateDao.getSessionFactory().openSession();	
			String strQry="From ProductCategory where productcategorycode='"+pccode+"'";
			Query sqlQuery=session.createQuery(strQry);
			List<ProductCategory> productcatageory=sqlQuery.list();
			return productcatageory;
			
			 
		}


		public List<String> getAddedQuatationCustmerNames() {
			// TODO Auto-generated method stub
			return null;
		}

}
