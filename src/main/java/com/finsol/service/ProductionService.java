package com.finsol.service;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.dao.ProductionDAO;
import com.finsol.model.PackageDetails;
import com.finsol.model.ProductionScheduler;
import com.finsol.model.ProductionSchedulerEvents;

/* @authour 
 * 
 * Rama Krishna */
@Service("ProductionService")
public class ProductionService 
{
	private static final Logger logger = Logger.getLogger(ProductionService.class);
	
	@Autowired
	private ProductionDAO productionDAO;
	
	public List<ProductionScheduler> loadProductionSchedulerData(String changeItem)
    {
        return productionDAO.loadProductionSchedulerData(changeItem);
    }
	
	public List<ProductionScheduler> loadProductionSchedulerSearch(String name,String value)
    {
        return productionDAO.loadProductionSchedulerSearch(name,value);
    }
	
	public boolean saveMultipleEntities(List entitiesList)
	{
		return productionDAO.saveMultipleEntities(entitiesList);
	}
	
	public List<ProductionSchedulerEvents> listEvents() 
	{
		return productionDAO.listEvents();
	}
	
	public void deletePackageDtls(PackageDetails packageDetails) 
	{
		productionDAO.deletePackageDtls(packageDetails);
	}
	
	public List<PackageDetails> loadPackageDtls(List one,List two)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return productionDAO.loadPackageDtls(one,two);
    }
}
