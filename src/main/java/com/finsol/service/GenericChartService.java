package com.finsol.service;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.dao.GenericChartDAO;

/**
 * @author Rama Krishna
 * 
 */
@Service("GenericChartService")
public class GenericChartService {
	
	private static final Logger logger = Logger.getLogger( GenericChartService.class);
	
	@Autowired
	private GenericChartDAO genericChartDAO ;
	public List<Integer> getLastYears(int year) {
		// TODO Auto-generated method stub
		return genericChartDAO.getLastYears(year);
	}


	public List<Object[]> catagaoysbookingamountoveryears(String catageoryname, int year) {
		// TODO Auto-generated method stub
		return genericChartDAO.getcatagaoysbookingamountoveryears(catageoryname, year);
	}


	public List<Object[]> getProducts(int year, String category) {
		// TODO Auto-generated method stub
		return genericChartDAO.getProducts(year,category);
	}


	public List getBookingValueForIndustrySectorShareOptBasedoncountry(int i, String country,String group) {
		// TODO Auto-generated method stub
		return genericChartDAO.getBookingValueForIndustrySectorShareOptBasedoncountry(i,country,group);
	}


	public List<Object[]> getIndustriesByIndustrySectorbasedoncountry(String parameter, String countrycode) {
		// TODO Auto-generated method stub
		return genericChartDAO.getIndustriesByIndustrySectorbasedoncountry(parameter,countrycode);
	}


 

	public Double getBookingValueForIndustrySharebasedoncountry(String icode, int parseInt, String countrycode) {
		// TODO Auto-generated method stub
		return genericChartDAO.getBookingValueForIndustrySharebasedoncountry(icode,parseInt,countrycode);
	}


	public List<Object[]> getAllDetailsAboutProducts(String category) {
		// TODO Auto-generated method stub
		return genericChartDAO.getAllDetailsAboutProducts(category);
	}


	public List<Object[]> catagaoysbookingamountforproductoveryears(String catageoryname, int year) {
		// TODO Auto-generated method stub
		return genericChartDAO.catagaoysbookingamountforproductoveryears(catageoryname,year);
	}
	
	
	public List<Integer> getDistinctMonthNumbers(Integer curYear) {
		// TODO Auto-generated method stub
		return genericChartDAO.getDistinctMonthNumbers(curYear);
	}
	
	public List<Object[]> getCountries() {
		return genericChartDAO.getCountries();
	}
	public List<Object[]> getCountriesNameColor(String countryCode) {
		return genericChartDAO.getCountriesNameColor(countryCode);
	}
	
	
	public List<Object[]> getBudgetofallmonths(Integer curYear,String countryname) {
		// TODO Auto-generated method stub
		return genericChartDAO.getBudgetofallmonths(curYear,countryname);
	}
	public List<Object[]> getBookingsOfMonthley(String countryname,Integer curYear) {
		// TODO Auto-generated method stub
		return genericChartDAO.getBookingsOfMonthley(countryname,curYear);
	}
	
	public Double getQuarterlyBooking(Integer year,String mString,String countryName)
	{
		return genericChartDAO.getQuarterlyBooking(year,mString,countryName);
	}

	public Double getBookingValueByYear(Integer year,String countryCode) {
		return genericChartDAO.getBookingValueByYear(year,countryCode);
	}
	
	public Double getBudgetValueByYear(Integer year,String countryCode) {
		return genericChartDAO.getBudgetValueByYear(year,countryCode);
	}
	
	public List getBookingValueForProductShareOptBasingonCountry(int i, String countrycode) {
		// TODO Auto-generated method stub
		return genericChartDAO.getBookingValueForProductShareOptBasingonCountry(i,countrycode);
		}
	 public List<Object[]> getTabledataforCatagories(Integer tableyears, List<Object[]> aboutcatgories) {
		// TODO Auto-generated method stub
		return genericChartDAO.getTabledataforCatagories(tableyears,aboutcatgories);
		}
	 
	 public List<Object[]> getTabledataforproducts(Integer tableyears, String category) {
			// TODO Auto-generated method stub
			return genericChartDAO.getTabledataforproducts(tableyears,category);
		}
	 
	 public List<Object[]> catagaoysbookingamountoveryearsforindividualcountry(String catageoryname, int year,
				String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getcatagaoysbookingamountoveryearsforindividualcountry(catageoryname,year,countrycode);
		}
		public List<Object[]> catagaoysbookingamountforproductoveryearsoverIndividualCountry(String catageoryname, int year,
				String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getcatagaoysbookingamountforproductoveryearsoverIndividualCountry(catageoryname,year,countrycode);
		}

		public List<Object[]> productbookingamountoveryearsforCountry(String catageoryname, int year, String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.productbookingamountoveryearsforCountry(catageoryname,year,countrycode);
		}


		public List<Object[]> getTabledataforCatagoriesonparticularcountry(Integer tableyears, String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getTabledataforCatagoriesonparticularcountry(tableyears,countrycode);
		}


		public List<Object[]> getTabledataforproducts(Integer tableyears, String category, String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getTabledataforproducts(tableyears,category,countrycode);
		}
		
		public List<Object[]> productbookingamountoveryears(String catageoryname, int year) {
			// TODO Auto-generated method stub
			return genericChartDAO.getproductbookingamountoveryears(catageoryname,year);
		}
		
		public List<Object[]> getCountriesDtails(String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getCountriesDtails(countrycode);
		}


		public List<Object[]> getBookingsOfMonthleyInUsd(String countryname, Integer curYear) {
			// TODO Auto-generated method stub
			return genericChartDAO.getBookingsOfMonthleyInUsd(countryname,curYear);
		}


		public Double getQuarterlyBookingInUsd(int i, String monthString, String countryname) {
			// TODO Auto-generated method stub
			return genericChartDAO.getQuarterlyBookingInUsd(i,monthString,countryname);
		}


		public Double getCWQuarterlyBookingValue(String quarterly, int i, String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getCWQuarterlyBookingValue(quarterly,i,countrycode);
		}


		public List getBookingValueForIndustrySectorShareOptBasedoncountryOnDollar(int i, String country, String group) {
			// TODO Auto-generated method stub
			return genericChartDAO.getBookingValueForIndustrySectorShareOptBasedoncountryOnDollar(i,country,group);
		}


		public List<Object[]> catagaoysbookingamountoveryearsforindividualcountrydollar(String catageoryname, int year,
				String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.catagaoysbookingamountoveryearsforindividualcountrydollar(catageoryname,year,countrycode);
		}


		public List<Object[]> getTabledataforCatagoriesonparticularcountrydollar(Integer tableyears,
				String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getTabledataforCatagoriesonparticularcountrydollar(tableyears,countrycode);
		}


		public List<Object[]> getTabledataforproductsdollar(Integer tableyears, String category, String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getTabledataforproductsdollar(tableyears, category, countrycode);
		}


		public List<Object[]> catagaoysbookingamountforproductoveryearsoverIndividualCountrydollar(String catageoryname,
				int year, String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.catagaoysbookingamountforproductoveryearsoverIndividualCountrydollar(catageoryname,
					 year, countrycode);
		}


		public List<Object[]> productbookingamountoveryearsforCountrydolllar(String catageoryname, int year,
				String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.productbookingamountoveryearsforCountrydolllar(catageoryname,year,countrycode);
		}


		public Double getCWQuarterlyBookingValuedollar(String quarterly, int i, String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getCWQuarterlyBookingValuedollar(quarterly,i,countrycode);
		}


		public List<Object[]> catagaoysbookingamountforproductoveryears(String catageoryname, int year,
				String category) {
			// TODO Auto-generated method stub
			return genericChartDAO.catagaoysbookingamountforproductoveryears(catageoryname,year,category);
		}


		public List<Object[]> catagaoysbookingamountforproductoveryearsoverIndividualCountry(String catageoryname,
				int year, String countrycode, String category) {
			// TODO Auto-generated method stub
			return genericChartDAO.catagaoysbookingamountforproductoveryearsoverIndividualCountry(catageoryname,year,countrycode,category);
		}


		public List<Object[]> catagaoysbookingamountforproductoveryearsoverIndividualCountrydollar(String catageoryname,
				int year, String countrycode, String category) {
			// TODO Auto-generated method stub
			return genericChartDAO.catagaoysbookingamountforproductoveryearsoverIndividualCountrydollar(catageoryname,year,countrycode,category);
		}


		public List<Object[]> productbookingamountoveryearsforCountry(String catageoryname, int year,
				String countrycode, String catageorey) {
			// TODO Auto-generated method stub
			return genericChartDAO.productbookingamountoveryearsforCountry(catageoryname,year,countrycode,catageorey);
		}


		public List<Object[]> productbookingamountoveryearsforCountrydolllar(String catageoryname, int year,
				String countrycode, String catageorey) {
			// TODO Auto-generated method stub
			return genericChartDAO.productbookingamountoveryearsforCountrydolllar(catageoryname, year,countrycode,catageorey);
		}


		public Double getBookingValueForIndustrySharebasedoncountrydollr(String icode, int parseInt,
				String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getBookingValueForIndustrySharebasedoncountrydollr(icode,parseInt,countrycode);
		}


		public Double getBookingValueForIndustrySharebasedoncountrydollar(String icode, int parseInt,
				String countrycode) {
			// TODO Auto-generated method stub
			return genericChartDAO.getBookingValueForIndustrySharebasedoncountrydollar(icode,parseInt,countrycode);
		}


	 

		 
	
}
