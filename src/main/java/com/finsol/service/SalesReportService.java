package com.finsol.service;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.dao.SalesReportDAO;
import com.finsol.model.DeliveryUpdateTemp;
import com.finsol.model.ProbabilityMaster;
import com.finsol.model.ProgressUpdateDtlsTemp;
import com.finsol.model.ProgressUpdateSmryTemp;
import com.finsol.model.Quotation;
import com.finsol.model.SalesProgressIndicators;


/* @authour 
 * 
 * Rama Krishna */
@Service("SalesReportService")
public class SalesReportService {

	private static final Logger logger = Logger.getLogger(SalesReportService.class);
	
	@Autowired
	private SalesReportDAO salesReportDAO;
	
	public boolean saveMultipleEntities(List entitiesList)
	{
		return salesReportDAO.saveMultipleEntities(entitiesList);
	}
	
	public void deleteSalesProgressIndicators() {
		salesReportDAO.deleteSalesProgressIndicators();
	}
	
	
	public List<SalesProgressIndicators> listSalesProgressIndicators()
	{
		return salesReportDAO.listSalesProgressIndicators();
	}
	
	public List<ProbabilityMaster> listProbabilityMaster()
	{
		return salesReportDAO.listProbabilityMaster();
	}
	
	
	public void deleteProbabilityMaster(ProbabilityMaster probabilityMaster) {
		salesReportDAO.deleteProbabilityMaster(probabilityMaster);
	}
	
	public List<ProbabilityMaster> loadProbabilityMasterByCustomerGroup(String customerGroup)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadProbabilityMasterByCustomerGroup(customerGroup);
    }
	
	public List loadDeliveryUpdate(String custName)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadDeliveryUpdate(custName);
    }
	
	
	public List<Quotation> loadQutationUpdate(String custName)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadQutationUpdate(custName);
    }
	
	public List<DeliveryUpdateTemp> loadDeliveryUpdateTemp(String custName)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadDeliveryUpdateTemp(custName);
    }
	
	public List<ProgressUpdateSmryTemp> loadProgressUpdateSmryTemp(String custName)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadProgressUpdateSmryTemp(custName);
    }
	
	public List<ProgressUpdateDtlsTemp> loadProgressUpdateDtlsTemp(String custName)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadProgressUpdateDtlsTemp(custName);
    }
	
	public List loadProgressUpdateDtls(String qtno)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadProgressUpdateDtls(qtno);
    }
	
	public List loadViewNotes(String quotno,String customer,String salesman,String srdate)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadViewNotes(quotno,customer,salesman,srdate);
    }
	
	public List loadViewNotesByDate(String fromdate,String todate)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return salesReportDAO.loadViewNotesByDate(fromdate,todate);
    }
	public void deleteDeliverUpdates(String qtnno) 
	{
		salesReportDAO.deleteDeliverUpdates(qtnno);
	}
	
	public void deleteDeliverUpdatesTemp(String qtnno) 
	{
		salesReportDAO.deleteDeliverUpdatesTemp(qtnno);
	}
	
	public void deleteProgressUpdateSmry(String qtnno) {
		salesReportDAO.deleteProgressUpdateSmry(qtnno);
	}
	public void deleteProgressUpdateSmryTemp(String qtnno) {
		salesReportDAO.deleteProgressUpdateSmryTemp(qtnno);
	}
	
	
	public void deleteProgressUpdateDtls(String qtnno) {
		salesReportDAO.deleteProgressUpdateDtls(qtnno);
	}
}
