package com.finsol.service;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.dao.UserManagementDao;
import com.finsol.model.Users;

/**
 * @author Rama Krishna
 *
 */
@Service("UserManagementService")
public class UserManagementService {

	private static final Logger logger = Logger.getLogger(UserManagementService.class);
	
	@Autowired
	private UserManagementDao userManagementDao;
	
	public boolean saveEntities(List entitiesList)
	{
		return userManagementDao.saveEntities(entitiesList);    
	}
	
	public List<Users> listUsers() {
		return userManagementDao.listUsers();
	}
	
}
