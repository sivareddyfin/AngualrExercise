package com.finsol.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.dao.ProductSelectionDao;
import com.finsol.model.HyphonicGreeseAmount;
import com.finsol.model.MotorHorsePower;
import com.finsol.model.PrestNeoGreeseAmount;
import com.finsol.model.QAIssue;
import com.finsol.model.QAIssueDetails;
import com.finsol.model.ServiceInquiry;
import com.finsol.model.ServiceInquiryDtls;
import com.finsol.model.TestReport;
import com.finsol.model.TestReportDtls;


/* @authour 
 * 
 * siva reddy*/
@Service("ProductSelectionService")
public class ProductSelectionService 
{

	
	@Autowired
	private ProductSelectionDao productSelectionDao;

	public List<String> getVoltagesOnfrequency(Integer frequencey) {
		// TODO Auto-generated method stub
		return productSelectionDao.getVoltagesOnfrequency(frequencey);
	}

	public List<String> getAllCouplingTypes() {
		// TODO Auto-generated method stub
		return productSelectionDao.getAllCouplingTypes();
	}

	public List<Object[]> getLocationFactor() {
		// TODO Auto-generated method stub
		return productSelectionDao.getLocationFactor();
	}

	public List<Object[]> getCouplingFactor() {
		// TODO Auto-generated method stub
		return productSelectionDao.getCouplingFactor();
	}

	public List<Object[]> getShockFactor() {
		// TODO Auto-generated method stub
		return productSelectionDao.getShockFactor();
	}

	public List<String> getAllLoadTypes() {
		// TODO Auto-generated method stub
		return productSelectionDao.getAllLoadTypes();
	}

	public List<Object[]> getAllLoadfactor(String loadtype, int workinghours) 
	{
		// TODO Auto-generated method stub
		return productSelectionDao.getAllLoadfactor(loadtype, workinghours);
	}

	public List<Object[]> getAllOutputSpeedsRadialNodes(int frequency) {
		// TODO Auto-generated method stub
		return productSelectionDao.getAllOutputSpeedsRadialNodes(frequency);
	}

	public List<Object[]> gtmomentOfInertia(String motortype, Double powerkw) {
		// TODO Auto-generated method stub
		return productSelectionDao.gtmomentOfInertia(motortype,powerkw);
	}

	public List<Object[]> getCalculatedPower() {
		// TODO Auto-generated method stub
		return productSelectionDao.getCalculatedPower();
	}

	public List<Integer> getReductionRation(int frequency, Double rpm) {
		// TODO Auto-generated method stub
		return productSelectionDao.getReductionRation(frequency,rpm);
	}

	public List<Double> getKws() {
		// TODO Auto-generated method stub
		return productSelectionDao.getKws();
	}

	public List<Object[]> getFrameSize(int frequency, int reductionvalue, Double kws) {
		// TODO Auto-generated method stub
		return productSelectionDao.getFrameSize(frequency,reductionvalue,kws);
	}

	public Double getMaxRpm(int frequency, Double rpm) {
		// TODO Auto-generated method stub
		return productSelectionDao.getMaxRpm(frequency,rpm);
	}

	public Double getMinRpm(int frequency, Double rpm) {
		// TODO Auto-generated method stub
		return productSelectionDao.getMinRpm(frequency,rpm);
	}

	public List<String> getModelNUmberOnFootMountWihoutBrake(int frequency, Double kws, int capacity,
			int framesizevalue, int reduction, int shaftraidoloadn, Double rpm) {
		// TODO Auto-generated method stub
		return productSelectionDao.getModelNUmberOnFootMountWihoutBrake(frequency,kws, capacity,
				framesizevalue, reduction, shaftraidoloadn, rpm);
	}

	public List<String> getModelNUmberOnFootMountBrake(int frequency, Double kws, int capacity, int framesizevalue,
			int reduction, Double shaftraidoloadn, Double rpm) {
		// TODO Auto-generated method stub
		return productSelectionDao.getModelNUmberOnFootMountBrake(frequency,kws, capacity,
				framesizevalue, reduction, shaftraidoloadn, rpm);
	}

	public List<String> getModelNUmberOnFlangeMountWihoutBrake(int frequency, Double kws, int capacity,
			int framesizevalue, int reduction, Double shaftraidoloadn, Double rpm) {
		// TODO Auto-generated method stub
		return productSelectionDao.getModelNUmberOnFlangeMountWihoutBrake(frequency,kws, capacity,
				framesizevalue, reduction, shaftraidoloadn, rpm);
	}

	public List<String> getModelNUmberOnFlangeMountWithBrake(int frequency, Double kws, int capacity,
			int framesizevalue, int reduction, Double shaftraidoloadn, Double rpm) {
		// TODO Auto-generated method stub
		return productSelectionDao.getModelNUmberOnFlangeMountWithBrake(frequency,kws, capacity,
				framesizevalue, reduction, shaftraidoloadn, rpm);
	}

	public Integer getAllowableFrequency(Double ratio, String string) {
		// TODO Auto-generated method stub
		return productSelectionDao.getAllowableFrequency(ratio,string);
	}

	public List<Double> getAllowableFrequencies(String coupilingtype) {
		// TODO Auto-generated method stub
		return productSelectionDao.getAllowableFrequencies(coupilingtype) ;
	}

	 
	public boolean saveEntities(List entitiesList)
	{
		return productSelectionDao.saveEntities(entitiesList);    
	}
	public boolean saveEntitiesTestReport(List entitiesList,TestReport testReport)
	{
		return productSelectionDao.saveEntitiesTestReport(entitiesList,testReport);    
	}

	public Object getEntityByIdVarchar(String id,Class className) 
	{
		return productSelectionDao.getEntityByIdVarchar(id,className);
	}
	
	public List<TestReportDtls> getTestReportDtlsByPONumber(String custPoNum)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return productSelectionDao.getTestReportDtlsByPONumber(custPoNum);
    }

	public void deleteTestReportDtls(TestReport testReport) {
		productSelectionDao.deleteTestReportDtls(testReport);
	}
	 
	public List<TestReport> listTestReports() {
		return productSelectionDao.listTestReports();
	}

	public List<TestReport> loadTestReportsByCurrentDate()
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return productSelectionDao.loadTestReportsByCurrentDate();
    }
	public List<TestReport> loadTestReportsByCustomerName(String fromdate,String todate)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return productSelectionDao.loadTestReportsByCustomerName(fromdate,todate);
    }
	
	
	public List<QAIssue> loadQAIssuesByCustomerName(String fromdate,String todate)
    {
        return productSelectionDao.loadQAIssuesByCustomerName(fromdate,todate);
        
    }
	public List<ServiceInquiry> loadServiceInquiryByCustomerName(String cusName)
    {
        return productSelectionDao.loadServiceInquiryByCustomerName(cusName);
        
    }
	
	public List loadPrestNeoGreeseAmount(Integer framesize,Integer ratio,String flangetype)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return productSelectionDao.loadPrestNeoGreeseAmount(framesize,ratio,flangetype);
    }

	public List loadHyphonicGreeseAmount(Integer framesize)
    {       // String sql = "from Branch where bankcode =" + bankcode;
        return productSelectionDao.loadHyphonicGreeseAmount(framesize);
    }
	
	public List<MotorHorsePower> getHorsePower(Integer frequency,Integer voltage,Double power)
    {       // String sql = "from Branch where bankcode =" + bankcode;
        return productSelectionDao.getHorsePower(frequency,voltage,power);
    }
	
	public List<ServiceInquiryDtls> listServiceInquiryDtls(String crefno) {
		return productSelectionDao.listServiceInquiryDtls(crefno);
	}
	
	public List<QAIssueDetails> listQAIssueDetails(String crefno) {
		return productSelectionDao.listQAIssueDetails(crefno);
	}

}
