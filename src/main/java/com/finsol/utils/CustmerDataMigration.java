package com.finsol.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.finsol.dao.HibernateDao;
import com.finsol.dao.MasterScreen_Dao;
import com.finsol.model.CustmerDetails;
import com.finsol.model.CustmerDetailsTemp;
import com.finsol.service.Bss_Migrate_ServiceImpl;

public class CustmerDataMigration
{
	
	@Autowired	
	private HibernateDao hibernateDao;
	@Autowired	
	private Bss_Migrate_ServiceImpl migratesevice;
	
	@Autowired	
	private MasterScreen_Dao masterScreenDao;

	private static final Logger logger = Logger.getLogger(CustmerDataMigration.class);
	
	public String migartiontime=null;
	
	
	
	public void checkingdrive1() throws Exception {
		File [] a=File.listRoots();
		System.out.println(a[0]);
		ArrayList<String> foraccessiondrives=new ArrayList<String>();
		for(int i=0;i<a.length;i++)
		{
		String drive=a[i].toString();
		foraccessiondrives.add(drive.substring(0, 1));
		
		}
		
	if(foraccessiondrives.contains("E"))
	{
	
		this.Insertcustmer("E");
	}
		 
	
	else if(foraccessiondrives.contains("D"))
	{
		
		this.Insertcustmer("D");
		
	}

	}
	


	public  void Insertcustmer(String drive) throws Exception
	{
		String slaesorderrecord=null;
		String currentfile=null;
		String migartiontimefolder=null;
		
		
		
		 
		
		StringTokenizer st=null;
		//String slaesorderrecord=null;
		//String currentfile=null;
		CustmerDetailsTemp dbt=null;
		int which=5;
		
		List<String> list=new ArrayList<String>();
		ArrayList<String> foldername=new  ArrayList<String>();
		foldername.add("SCA");
		foldername.add("SMAU");
		foldername.add("SMID");
		foldername.add("SMMA");
		foldername.add("SMPH");
		foldername.add("SMTH");
		foldername.add("SMVN");
		foldername.add("sca");
		foldername.add("smau");
		foldername.add("smid");
		foldername.add("smma");
		foldername.add("smph");
		foldername.add("smth");
		foldername.add("smvn");
		
		
		
		HashMap<String,String> countrycodes=  new HashMap<String,String>();
		
		countrycodes.put("BK","TH");
		countrycodes.put("SG","SG");
		countrycodes.put("HC","VN");
		countrycodes.put("KP","MY");
		countrycodes.put("MN","PH");
		countrycodes.put("JK","ID");
		countrycodes.put("SY","AU");
		/*countrycodes.put("ID","ID");*/
		
		
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
		int day = now.get(Calendar.DAY_OF_MONTH);
		int hour = now.get(Calendar.HOUR_OF_DAY);
		int minute = now.get(Calendar.MINUTE);
		int second = now.get(Calendar.SECOND);
		int millis = now.get(Calendar.MILLISECOND);
	   migartiontime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
	    migartiontimefolder=month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
		
		/*migartiontime="aaa";*/
		
		
	   // File dir = new File("D://CRMSCADATA//PGN Daily Book//"+year);
	    File dir = new File(""+drive+":\\crmscadata\\CUSTOMER");
	    //FileFilter fileFilter = new WildcardFilter("*." + ext);
	    File[] files = dir.listFiles();
	    for(int i=0;i<files.length;i++)
	    {
	    	
	    	
	   	String absloutefolderpathinthesysytem=files[i].getAbsolutePath();
		String	 f1= absloutefolderpathinthesysytem.substring(absloutefolderpathinthesysytem.lastIndexOf('\\') + 1);
				 
	    	
	    	
	    	
		 if(foldername.contains(f1))
		 {
	    File absloutefolderpathinthesysytemobject = new File(absloutefolderpathinthesysytem);
		File[] absloutefolderpathinthesysytemfiles = absloutefolderpathinthesysytemobject.listFiles();
	    	
	    	
	    	
		 for(int absf=0;absf<absloutefolderpathinthesysytemfiles.length;absf++)
		 {
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	int count=0;
	    	String FileName=absloutefolderpathinthesysytemfiles[absf].getAbsolutePath();
	    	currentfile=FileName;
	    	logger.info("----------------------------FileName---------------------"+FileName.substring(Math.max(FileName.length() - 2, 0)));
		Scanner in = new Scanner(new File(absloutefolderpathinthesysytemfiles[absf].getAbsolutePath()));
		while (in.hasNext()) 
		{
			//String filenme1=FileName.substring(0, FileName.length() - 4);
			
			//String filename2=filenme1.substring(Math.max(filenme1.length() - 2, 0));
			
			logger.debug("startrdddddddddddddddddddddddddd----------------------------------->");
			System.out.println("startred------------------------------------------------------->"+FileName );
		    String line = in.nextLine();
		    if(count==0)
		    {
		    	count++;
		    	continue;
		    }		    
		    else
		    {
		    	String[] str=line.split("\\|");//Need to use Escape sequence \\ for Pipe(|)
				for(int k=0;k<str.length;k++)
				{
					list.add(str[k]);
				}
			
				
				
								dbt = new CustmerDetailsTemp();
								String company = (String) list.get(0);
								String account = (String) list.get(1);
								String vendororcustmer = (String) list.get(2);
								String name1 = (String) list.get(3);
								String name2 = (String) list.get(4);
								String address1 = (String) list.get(5);
								String address2 = (String) list.get(6);
								String address3 = (String) list.get(7);
								String city = (String) list.get(8);
								String country = (String) list.get(9);
								
								
								
								String title = (String) list.get(10);
								String contact = (String) list.get(11);
								String tel = (String) list.get(12);
								String fax = (String) list.get(13);
								
								
								String email=(String) list.get(14);
								String post = (String) list.get(15);
								String pbox = (String) list.get(16);
								String salesman = (String) list.get(17);

								String territory = (String) list.get(18);
								String custmertype = (String) list.get(19);
								//String type = (String) list.get(20);
								String paymentterm = (String) list.get(20);
								String applicationcode = (String) list.get(21);
								String creditstatus = (String) list.get(22);
								String entrydate = (String) list.get(23);
								
								
								
                                Date d=this.converttoDate(entrydate);
                                dbt.setAccount(account);
                                dbt.setAccountid(name1+" "+name2);
                                dbt.setBillingCountries(country);
                                dbt.setTitle(title);
                                dbt.setContName(contact);
                                dbt.setPhnumber(tel);
                                dbt.setFax(fax);
                                dbt.setEmail(email);
                                dbt.setSalesPersonCode(salesman);
                                dbt.setTerritory(territory);
                                dbt.setCustomerType(custmertype);
                                dbt.setPaymentTerm(paymentterm);
                                dbt.setApplication(applicationcode);
                                dbt.setCreditstatus(creditstatus);
                                dbt.setBddate(d);
                                
                                
								Session session = (Session) hibernateDao.getSessionFactory().openSession();
								session.save(dbt);
								session.flush();
								list.clear();
			 
		    }		    	
		    count++;
		}

		in.close(); 
		 }
		
		 }
		
	}
	    
	    
	    this.movingfolders(drive);
	    
	    
	    
	    Session s1=(Session)hibernateDao.getSessionFactory().openSession();
	    String query="select distinct  account from CustmerDetailsTemp";
	    
	    
	    Query q= s1.createQuery(query);
	    List<String> account=q.list();
	    
	    
	    this.updatingcustmertemptablewithcustmerData(account);
	    this.insertingintocustmerdataTemp(account);
	    
	 
	    
	     
      
      
      
      
	
	}


	
	
	
	
	
	private void updatingcustmertemptablewithcustmerData(List<String> account)
    {
		 Session s1=(Session)hibernateDao.getSessionFactory().openSession();
		 String query="from CustmerDetails where account  in (:account) and  custmermodifeyStatus IS NULL ";
		 
	     Query q=s1.createQuery(query);
	     q.setParameterList("account",account);
	     
	     List<CustmerDetails> cutmerdetails=q.list();
	     int sizeofcustmerdetails=cutmerdetails.size();
	     
	     for(CustmerDetails p:cutmerdetails)
	     {
	    	 
	    	 
	    	
	    	 
	    	 
	     }
	    
    }



	private void insertingintocustmerdataTemp(List<String> account)
    {
		 Session s1=(Session)hibernateDao.getSessionFactory().openSession();
		 String query="from CustmerDetails where account not in (:account) and  custmermodifeyStatus IS NULL ";
		 
	     Query q=s1.createQuery(query);
	     q.setParameterList("account",account);
	     
	     List<CustmerDetails> cutmerdetails=q.list();
	     int sizeofcustmerdetails=cutmerdetails.size();
	     
	     for(CustmerDetails productageorey:cutmerdetails)
	     {
	    	 
	    	 
	    	 
	    	 CustmerDetailsTemp cdetails=new CustmerDetailsTemp();
	 		cdetails.setCustomerStatus(productageorey.getCustomerStatus());  
	 		cdetails.setAccount(productageorey.getAccount());
	 		cdetails.setAccountid(productageorey.getAccountid());
	 		cdetails.setClientwebsite(productageorey.getClientwebsite());
	 		cdetails.setMailingadress(productageorey.getMailingadress());
	 		cdetails.setShippingadress(productageorey.getShippingadress());
	 		cdetails.setBillingCountries(productageorey.getBillingCountries());
	 		cdetails.setLanguage(productageorey.getLanguage());
	 		cdetails.setCustomerType(productageorey.getCustomerType());
	 		cdetails.setIndustries(productageorey.getIndustries());
	 		cdetails.setApplication(productageorey.getApplication());
	 		cdetails.setTerritory(productageorey.getTerritory());
	 		cdetails.setCorporate(productageorey.getCorporate());
	 		cdetails.setContName(productageorey.getContName());
	 		cdetails.setTitle(productageorey.getTitle());
	 		cdetails.setPhnumber(productageorey.getPhnumber());
	 		cdetails.setMbnumber(productageorey.getMbnumber());
	 		cdetails.setOphnumber(productageorey.getOphnumber());
	 		cdetails.setEmail(productageorey.getEmail());
	 		cdetails.setFax(productageorey.getFax());
	 		cdetails.setDepartmentname(productageorey.getDepartmentname());
	 		cdetails.setReports(productageorey.getReports());
	 		cdetails.setSalesPerson(productageorey.getSalesPerson());
	 	    cdetails.setSalesPersonCode(productageorey.getSalesPersonCode());
	 		cdetails.setLeadsource(productageorey.getLeadsource()); 
	 		cdetails.setCurrency(productageorey.getCurrency());
	 		cdetails.setTaxName(productageorey.getTaxName());
	 		cdetails.setPaymentTerm(productageorey.getPaymentTerm());
	 	    cdetails.setOpportunityAmount(productageorey.getOpportunityAmount());
	 	    cdetails.setCreatedby(productageorey.getCreatedby());
	 	    cdetails.setLastmodifiedby(productageorey.getLastmodifiedby());
	 	    cdetails.setDiscountmultiplier(productageorey.getDiscountmultiplier());
	 	    cdetails.setCreditstatus(productageorey.getCreditstatus());
	 	    cdetails.setApplicanindustryname(productageorey.getApplicanindustryname());
	 	    cdetails.setImagename(productageorey.getImagename());
	 	    cdetails.setBddate(productageorey.getBddate());
	 		 s1.save(cdetails);
	 	    
	}
	     
	     
	     
	      System.out.println("<------------------------------done sucessfully---------------------------->");
	    
    }



	public  void movingfolders(String drive) throws IOException 
    {
    	 
    		
    		Calendar now = Calendar.getInstance();
    		int year = now.get(Calendar.YEAR);
    		int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
    		int day = now.get(Calendar.DAY_OF_MONTH);
    		int hour = now.get(Calendar.HOUR_OF_DAY);
    		int minute = now.get(Calendar.MINUTE);
    		int second = now.get(Calendar.SECOND);
    		int millis = now.get(Calendar.MILLISECOND);
    		
    		
    		
    		ArrayList<String> foldername=new  ArrayList<String>();
    		foldername.add("SMSG");
    		foldername.add("SMAU");
    		foldername.add("SMID");
    		foldername.add("SMMA");
    		foldername.add("SMPH");
    		foldername.add("SMTH");
    		foldername.add("SMVN");
    		foldername.add("sca");
    		foldername.add("smau");
    		foldername.add("smid");
    		foldername.add("smma");
    		foldername.add("smph");
    		foldername.add("smth");
    		foldername.add("smvn");
    		
    		
    		//File sourceFile = new File("directory-source/test1.txt");
    		//File destinationDir = new File("directory-destination");

    		//FileUtils.moveFileToDirectory(sourceFile, destinationDir, true);

    		
    	int numberoffolders=foldername.size();
    	
    	//E:\Booking
    	
    	for(int i=0;i<numberoffolders;i++)
    	{
    		 File dir1 = new File(""+drive+"://crmscadata//CUSTOMER//"+foldername.get(i));
    		 
    		 File dir2 = new File(""+drive+"://crmscadata//CUSTOMER//YEARS//"+year+"//"+foldername.get(i));
    		// File dir2 = new File("E://Booking//YEARS//"+foldername.get(i));
    		 File[] files = dir1.listFiles();
    		 
    		 if(files!=null)
    		 {
    		 for(int j=0;j<files.length;j++)
    		 {
    			 String fimename=files[j].getName();
    			 File sourceFile = new File(files[j].getAbsolutePath());
    			 File destinationDir = new File(dir2.toString());
    			 File destinationDir2 = new File(destinationDir+"//"+files[j].getName());
    			 boolean condition=destinationDir2.exists();
    			if( destinationDir2.exists())
    			{
    				destinationDir2.delete();
    			}
    			 
    			org.apache.commons.io.FileUtils.moveFileToDirectory(sourceFile, destinationDir, true);
    			 
    		
    		 }
    		 
    		 }
    	}
    	
    	
    		
    	
    	
    	
    	     	
    	
    	  
    }
	

	
	
	public Date  converttoDate(String s)
	
	{
		Date entrydate=null;
		
		if(s.length()==8)
		{
			 String date=String.valueOf(s.charAt(6))+String.valueOf(s.charAt(7))+"-"+String.valueOf(s.charAt(4))+String.valueOf(s.charAt(5))+"-"+String.valueOf(s.charAt(0))+String.valueOf(s.charAt(1))+String.valueOf(s.charAt(2))+String.valueOf(s.charAt(3));
			  entrydate = DateUtils.getSqlDateFromString(date, Constants.GenericDateFormat.DATE_FORMAT);

		}
		
		return entrydate;
	}
	
	


}
