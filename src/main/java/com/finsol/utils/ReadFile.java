 package com.finsol.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import com.finsol.dao.HibernateDao;
import com.finsol.dao.MasterScreen_Dao;
import com.finsol.model.Application;
import com.finsol.model.DailyBookingTemp;
import com.finsol.model.Industry;
import com.finsol.model.IndustrySector;
import com.finsol.model.MigrationExceptionCheck;
import com.finsol.model.MotorSpecifications;
import com.finsol.model.ProductionScheduler;
import com.finsol.model.Score;
import com.finsol.model.YtdBooking;
import com.finsol.model.YtdBookingIndustry;
import com.finsol.model.YtdBookingProduct;
import com.finsol.model.YtdBookingRegion;
import com.finsol.model.YtdProfit;
import com.finsol.service.Bss_Migrate_ServiceImpl;
/*import com.finsol.model.IndustryMigrate;
*/
/**
 * @author Rama Krishna
 * 
 */
public class ReadFile {
	
	@Autowired	
	private HibernateDao hibernateDao;
	@Autowired	
	private Bss_Migrate_ServiceImpl migratesevice;
	
	@Autowired	
	private MasterScreen_Dao masterScreenDao;

	private static final Logger logger = Logger.getLogger(ReadFile.class);
	
	public String migartiontime=null;
	
	/*public  void Insert() throws Exception
	{	
		StringTokenizer st=null;
		DailyBookingTemp dbt=null;
		
		List<String> list=new ArrayList<String>();
		
		
		HashMap<String,String> countrycodes=  new HashMap<String,String>();
		
		countrycodes.put("BK","TH");
		countrycodes.put("SG","SG");
		countrycodes.put("HC","VN");
		countrycodes.put("KP","MY");
		countrycodes.put("MN","PH");
		countrycodes.put("JK","ID");
		countrycodes.put("SY","AU");
		countrycodes.put("ID","ID");
		
		
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
		int day = now.get(Calendar.DAY_OF_MONTH);
		int hour = now.get(Calendar.HOUR_OF_DAY);
		int minute = now.get(Calendar.MINUTE);
		int second = now.get(Calendar.SECOND);
		int millis = now.get(Calendar.MILLISECOND);
	   migartiontime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
		
		migartiontime="aaa";
		
		
	    File dir = new File("D://Feeds//");
	    //FileFilter fileFilter = new WildcardFilter("*." + ext);
	    File[] files = dir.listFiles();
	    for(int i=0;i<files.length;i++)
	    {
	    	int count=0;
	    	String FileName=files[i].getAbsolutePath();
	    	logger.info("----------------------------FileName---------------------"+FileName.substring(Math.max(FileName.length() - 2, 0)));
		Scanner in = new Scanner(new File(files[i].getAbsolutePath()));
		while (in.hasNext()) 
		{
			String filenme1=FileName.substring(0, FileName.length() - 4);
			
			String filename2=filenme1.substring(Math.max(filenme1.length() - 2, 0));
			
			logger.debug("startrdddddddddddddddddddddddddd----------------------------------->");
			System.out.println("startred------------------------------------------------------->"+FileName );
		    String line = in.nextLine();
		    if(count==0)
		    {
		    	count++;
		    	continue;
		    }		    
		    else
		    {
		    	String[] str=line.split("\\|");//Need to use Escape sequence \\ for Pipe(|)
				for(int k=0;k<str.length;k++)
				{
					list.add(str[k]);
				}
				dbt=new DailyBookingTemp();				
				dbt.setEdate(list.get(0));
				dbt.setSalesorder(list.get(1));
				dbt.setContractno(list.get(2));
				dbt.setCustomercode(list.get(3));		 
				dbt.setName1(list.get(4));
				dbt.setName2(list.get(5));
				dbt.setShiptoname1(list.get(6));
				dbt.setShiptoname2(list.get(7));
				dbt.setEnduser(list.get(8));
				dbt.setFlag("true");
				if(list.get(9)==null || list.get(9).trim().equals(""))
					dbt.setPayment("NA");
				else
				{
					Integer iVal=Integer.parseInt(list.get(9));
					dbt.setPayment(list.get(9));
				}
				dbt.setPaymentdescript(list.get(10));				
				dbt.setShipvia(list.get(11));
				dbt.setSalesoffice1(list.get(12));
				dbt.setCode1(list.get(13));
				
				if(list.get(14)==null || list.get(14).trim().equals(""))
					dbt.setM1p("NA");
				else
					dbt.setM1p(list.get(14));				
				
				dbt.setSalesman1(list.get(15));
				if(list.get(16)==null || list.get(16).trim().equals(""))
					dbt.setPercent1(0.00);
				else
				dbt.setPercent1(Double.parseDouble(list.get(16)));
				
				if(list.get(17)==null || list.get(17).trim().equals(""))
					dbt.setAmount1(0.00);
				else
				dbt.setAmount1(Double.parseDouble(list.get(17)));
				dbt.setSalesoffice2(list.get(18));
				dbt.setCode2(list.get(19));
				if(list.get(20)==null || list.get(20).trim().equals(""))
					dbt.setM2p("NA");
				else
					dbt.setM2p(list.get(20));
				
				dbt.setSalesman2(list.get(21));
				if(list.get(22)==null || list.get(22).trim().equals(""))
					dbt.setPercent2(0.00);
				else
				dbt.setPercent2(Double.parseDouble(list.get(22)));
				
				if(list.get(23)==null || list.get(23).trim().equals(""))
					dbt.setAmount2(0.00);
				else
				dbt.setAmount2(Double.parseDouble(list.get(23)));	
				
				dbt.setSalesoffice3(list.get(24));
				dbt.setCode3(list.get(25));
				if(list.get(26)==null || list.get(26).trim().equals(""))
					dbt.setM3p("NA");
				else
					dbt.setM3p(list.get(26));
				dbt.setSalesman3(list.get(27));
				if(list.get(28)==null || list.get(28).trim().equals(""))
					dbt.setPercent3(0.00);
				else
				dbt.setPercent3(Double.parseDouble(list.get(28)));
				
				if(list.get(29)==null || list.get(29).trim().equals(""))
					dbt.setAmount3(0.00);
				else
				dbt.setAmount3(Double.parseDouble(list.get(29)));				
				dbt.setSalesoffice4(list.get(30));
				dbt.setCode4(list.get(31));
				if(list.get(32)==null || list.get(32).trim().equals(""))
					dbt.setM4p("NA");
				else
					dbt.setM4p(list.get(32));
				dbt.setSalesman4(list.get(33));
				if(list.get(34)==null || list.get(34).trim().equals(""))
					dbt.setPercent4(0.00);
				else
				dbt.setPercent4(Double.parseDouble(list.get(34)));	
				
				if(list.get(35)==null || list.get(35).trim().equals(""))
					dbt.setAmount4(0.00);
				else
				dbt.setAmount4(Double.parseDouble(list.get(35)));				
				dbt.setCustomerclass(list.get(36));
				dbt.setDelivterm(list.get(37));
				dbt.setDelivdescript(list.get(38));				
				if(list.get(39)==null || list.get(39).trim().equals(""))
					dbt.setQty(0);
				else
				dbt.setQty(Double.valueOf(list.get(39)).intValue());				
				dbt.setUnit(list.get(40));
				dbt.setPid(list.get(41));
				
				if(list.get(42)==null || list.get(42).trim().equals(""))	
					dbt.setPline("NA");
				else	
					dbt.setPline(list.get(42));
				
				dbt.setDescription(list.get(43));
				dbt.setCurrency(list.get(44));
				if(list.get(45)==null || list.get(45).trim().equals(""))
					dbt.setListprice(0.00);
				else
					dbt.setListprice(Double.parseDouble(list.get(45)));
						 
				if(list.get(46)==null || list.get(46).trim().equals(""))
					dbt.setUnitprice(0.00);
				else
				dbt.setUnitprice(Double.parseDouble(list.get(46)));						 
				dbt.setContractamount(list.get(47));
				dbt.setShippingpoint(list.get(48));
				dbt.setCertraw(list.get(49));
				dbt.setNumberofdoc(list.get(50));
				dbt.setTestreport(list.get(51));
				dbt.setUpr(list.get(52));
				
				if(list.get(53)==null || list.get(53).trim().equals(""))
					dbt.setApplicationcode("NA");
				else
					dbt.setApplicationcode(list.get(53));
				
				dbt.setSpecialdrawing(list.get(54));
				dbt.setRequireddeliv(list.get(55));
				dbt.setReplieddeliv(list.get(56));
				dbt.setBookingdate(list.get(57));
				dbt.setNewchange(list.get(58));
				dbt.setCountrycode(countrycodes.get(filename2));
				dbt.setMigrationtime(migartiontime);
				 
				dbt.setChangemark(list.get(59));
				dbt.setFirstdate(list.get(60));				
				if(list.get(61)==null || list.get(61).trim().equals(""))
					dbt.setOrdertype(0);
				else
					dbt.setOrdertype(Integer.parseInt(list.get(61)));
				
				Session session=(Session)hibernateDao.getSessionFactory().openSession();
				session.save(dbt);
				session.flush();
				list.clear();
		    }		    	
		    count++;
		}

		in.close(); 
		
	}
		//migrateYtdBooking("SG");//YTDBooking Table
		
		// don't forget to close resource leaks
		
		ReadFile rf=new ReadFile();
		File theNewestFile =rf.getTheNewestFile("D://Feeds//", "txt");
		if(!(theNewestFile==null))
		System.out.println(theNewestFile.getName());
	    
	    
	    
	    Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	    
	    
	   String thsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'TH' and  customercode in ('SMMA','SMID','SMPH','SMVN','SMAU','SCA','SMIN','SMSG')";
       String sgsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'SG' and  customercode in ('SMMA','SMID','SMPH','SMVN','SMAU','SCA','SMTH')";	    
       String vnsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'VN' and  customercode in ('SMMA','SMID','SMPH','SMSG','SMAU','SCA','SMIN','SMTH')";
       String mysql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'MY' and  customercode in ('SMVN','SMID','SMPH','SMSG','SMAU','SCA','SMIN','SMTH')";
       String phsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'PH' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMAU','SCA','SMIN','SMTH')";
       //String insql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'IN' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMAU','SCA','SMPH','SMTH')";
       String ausql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'AU' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMIN','SCA','SMPH','SMTH')";
       String idsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'ID' and  customercode in ('SMVN','SMAU','SMMA','SMSG','SMIN','SCA','SMPH','SMTH')";
        
       
      Query thquery= session1.createQuery(thsql);
      Query sgquery= session1.createQuery(sgsql);
      Query vnquery= session1.createQuery(vnsql);
      Query myquery= session1.createQuery(mysql);
      Query phquery= session1.createQuery(phsql);
      
      Query auquery= session1.createQuery(ausql);
      Query idquery= session1.createQuery(idsql);
      
      thquery.executeUpdate();
      sgquery.executeUpdate();
      vnquery.executeUpdate();
      myquery.executeUpdate();
      phquery.executeUpdate();
      
      auquery.executeUpdate();  
      idquery.executeUpdate();  
      
       this.migrateYtdBooking();
	}

	
*/	/* Get the newest file for a specific extension */
/*	public File getTheNewestFile(String filePath, String ext) 
	{
	    File theNewestFile = null;
	    File dir = new File(filePath);
	    FileFilter fileFilter = new WildcardFilter("*." + ext);
	    File[] files = dir.listFiles();

	    if (files.length > 0) 
	    {
	        // ---The newest file comes first --- //
	       // Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
	        theNewestFile = (File)files[0];
	    }
	    //System.out.println(files[0].getName());
	    return theNewestFile;
	}*/
	
	
	public void checkingdrive() throws Exception {
		File [] a=File.listRoots();
		System.out.println(a[0]);
		ArrayList<String> foraccessiondrives=new ArrayList<String>();
		for(int i=0;i<a.length;i++)
		{
		String drive=a[i].toString();
		foraccessiondrives.add(drive.substring(0, 1));
		
		}
		
	if(foraccessiondrives.contains("E"))
	{
	
		this.Insert("E");
	}
		 
	
	else if(foraccessiondrives.contains("D"))
	{
		
		this.Insert("D");
		
	}

	}
	
	
	public  void Insert(String drive) throws Exception
	{
		String slaesorderrecord=null;
		String currentfile=null;
		String migartiontimefolder=null;
		
		
		
		try{
		
		StringTokenizer st=null;
		//String slaesorderrecord=null;
		//String currentfile=null;
		DailyBookingTemp dbt=null;
		int which=5;
		
		List<String> list=new ArrayList<String>();
		ArrayList<String> foldername=new  ArrayList<String>();
		foldername.add("SCA");
		foldername.add("SMAU");
		foldername.add("SMID");
		foldername.add("SMMA");
		foldername.add("SMPH");
		foldername.add("SMTH");
		foldername.add("SMVN");
		foldername.add("sca");
		foldername.add("smau");
		foldername.add("smid");
		foldername.add("smma");
		foldername.add("smph");
		foldername.add("smth");
		foldername.add("smvn");
		
		
		
		HashMap<String,String> countrycodes=  new HashMap<String,String>();
		
		countrycodes.put("BK","TH");
		countrycodes.put("SG","SG");
		countrycodes.put("HC","VN");
		countrycodes.put("KP","MY");
		countrycodes.put("MN","PH");
		countrycodes.put("JK","ID");
		countrycodes.put("SY","AU");
		/*countrycodes.put("ID","ID");*/
		
		
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
		int day = now.get(Calendar.DAY_OF_MONTH);
		int hour = now.get(Calendar.HOUR_OF_DAY);
		int minute = now.get(Calendar.MINUTE);
		int second = now.get(Calendar.SECOND);
		int millis = now.get(Calendar.MILLISECOND);
	   migartiontime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
	    migartiontimefolder=month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
		
		/*migartiontime="aaa";*/
		
		
	    File dir = new File(""+drive+":\\crmscadata\\Booking");
	    //FileFilter fileFilter = new WildcardFilter("*." + ext);
	    File[] files = dir.listFiles();
	    for(int i=0;i<files.length;i++)
	    {
	    	
	    	
	   	String absloutefolderpathinthesysytem=files[i].getAbsolutePath();
		String	 f1= absloutefolderpathinthesysytem.substring(absloutefolderpathinthesysytem.lastIndexOf('\\') + 1);
				 
	    	
	    	
	    	
		 if(foldername.contains(f1))
		 {
	    File absloutefolderpathinthesysytemobject = new File(absloutefolderpathinthesysytem);
		File[] absloutefolderpathinthesysytemfiles = absloutefolderpathinthesysytemobject.listFiles();
	    	
	    	
	    	
		 for(int absf=0;absf<absloutefolderpathinthesysytemfiles.length;absf++)
		 {
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	int count=0;
	    	String FileName=absloutefolderpathinthesysytemfiles[absf].getAbsolutePath();
	    	currentfile=FileName;
	    	logger.info("----------------------------FileName---------------------"+FileName.substring(Math.max(FileName.length() - 2, 0)));
		Scanner in = new Scanner(new File(absloutefolderpathinthesysytemfiles[absf].getAbsolutePath()));
		while (in.hasNext()) 
		{
			String filenme1=FileName.substring(0, FileName.length() - 4);
			
			String filename2=filenme1.substring(Math.max(filenme1.length() - 2, 0));
			
			logger.debug("startrdddddddddddddddddddddddddd----------------------------------->");
			System.out.println("startred------------------------------------------------------->"+FileName );
		    String line = in.nextLine();
		    if(count==0)
		    {
		    	count++;
		    	continue;
		    }		    
		    else
		    {
		    	String[] str=line.split("\\|");//Need to use Escape sequence \\ for Pipe(|)
				for(int k=0;k<str.length;k++)
				{
					list.add(str[k]);
				}
				dbt=new DailyBookingTemp();	
				slaesorderrecord=list.get(1);
			String date=(String)list.get(0);
			int datetocompare=Integer.parseInt(list.get(0));
			 
			if(datetocompare<20161114)
			{
				which=0;
				dbt.setEdate(list.get(0));
				dbt.setSalesorder(list.get(1));
				dbt.setContractno(list.get(2));
				dbt.setCustomercode(list.get(3));		 
				dbt.setName1(list.get(4));
				dbt.setName2(list.get(5));
				dbt.setShiptoname1(list.get(6));
				dbt.setShiptoname2(list.get(7));
				dbt.setEnduser(list.get(8));
				dbt.setFlag("true");
				if(list.get(9)==null || list.get(9).trim().equals(""))
					dbt.setPayment("NA");
				else
				{
					/*Integer iVal=Integer.parseInt(list.get(9));*/
					dbt.setPayment(list.get(9));
				}
				dbt.setPaymentdescript(list.get(10));				
				dbt.setShipvia(list.get(11));
				dbt.setSalesoffice1(list.get(12));
				dbt.setCode1(list.get(13));
				
				if(list.get(14)==null || list.get(14).trim().equals(""))
					dbt.setM1p("NA");
				else
					dbt.setM1p(list.get(14));				
				
				dbt.setSalesman1(list.get(15));
				if(list.get(16)==null || list.get(16).trim().equals(""))
					dbt.setPercent1(0.00);
				else
				dbt.setPercent1(Double.parseDouble(list.get(16)));
				
				if(list.get(17)==null || list.get(17).trim().equals(""))
					dbt.setAmount1(0.00);
				else
				dbt.setAmount1(Double.parseDouble(list.get(17)));
				dbt.setSalesoffice2(list.get(18));
				dbt.setCode2(list.get(19));
				if(list.get(20)==null || list.get(20).trim().equals(""))
					dbt.setM2p("NA");
				else
					dbt.setM2p(list.get(20));
				
				dbt.setSalesman2(list.get(21));
				if(list.get(22)==null || list.get(22).trim().equals(""))
					dbt.setPercent2(0.00);
				else
				dbt.setPercent2(Double.parseDouble(list.get(22)));
				
				if(list.get(23)==null || list.get(23).trim().equals(""))
					dbt.setAmount2(0.00);
				else
				dbt.setAmount2(Double.parseDouble(list.get(23)));	
				
				dbt.setSalesoffice3(list.get(24));
				dbt.setCode3(list.get(25));
				if(list.get(26)==null || list.get(26).trim().equals(""))
					dbt.setM3p("NA");
				else
					dbt.setM3p(list.get(26));
				dbt.setSalesman3(list.get(27));
				if(list.get(28)==null || list.get(28).trim().equals(""))
					dbt.setPercent3(0.00);
				else
				dbt.setPercent3(Double.parseDouble(list.get(28)));
				
				if(list.get(29)==null || list.get(29).trim().equals(""))
					dbt.setAmount3(0.00);
				else
				dbt.setAmount3(Double.parseDouble(list.get(29)));				
				dbt.setSalesoffice4(list.get(30));
				dbt.setCode4(list.get(31));
				if(list.get(32)==null || list.get(32).trim().equals(""))
					dbt.setM4p("NA");
				else
					dbt.setM4p(list.get(32));
				dbt.setSalesman4(list.get(33));
				if(list.get(34)==null || list.get(34).trim().equals(""))
					dbt.setPercent4(0.00);
				else
				dbt.setPercent4(Double.parseDouble(list.get(34)));	
				
				if(list.get(35)==null || list.get(35).trim().equals(""))
					dbt.setAmount4(0.00);
				else
				dbt.setAmount4(Double.parseDouble(list.get(35)));				
				dbt.setCustomerclass(list.get(36));
				dbt.setDelivterm(list.get(37));
				dbt.setDelivdescript(list.get(38));				
				if(list.get(39)==null || list.get(39).trim().equals(""))
					dbt.setQty(0);
				else
				dbt.setQty(Double.valueOf(list.get(39)).intValue());				
				dbt.setUnit(list.get(40));
				dbt.setPid(list.get(41));
				
				if(list.get(42)==null || list.get(42).trim().equals(""))	
					dbt.setPline("NA");
				else	
					dbt.setPline(list.get(42));
				
				dbt.setDescription(list.get(43));
				
				dbt.setCurrency(list.get(44));
				if(list.get(45)==null || list.get(45).trim().equals(""))
					dbt.setListprice(0.00);
				else
					dbt.setListprice(Double.parseDouble(list.get(45)));
						 
				if(list.get(46)==null || list.get(46).trim().equals(""))
					dbt.setUnitprice(0.00);
				else
				dbt.setUnitprice(Double.parseDouble(list.get(46)));						 
				dbt.setContractamount(list.get(47));
				dbt.setShippingpoint(list.get(48));
				dbt.setCertraw(list.get(49));
				dbt.setNumberofdoc(list.get(50));
				dbt.setTestreport(list.get(51));
				dbt.setUpr(list.get(52));
				
				if(list.get(53)==null || list.get(53).trim().equals(""))
					dbt.setApplicationcode("NA");
				else
					dbt.setApplicationcode(list.get(53));
				
				dbt.setSpecialdrawing(list.get(54));
				dbt.setRequireddeliv(list.get(55));
				dbt.setReplieddeliv(list.get(56));
				dbt.setBookingdate(list.get(57));
				dbt.setNewchange(list.get(58));
				dbt.setCountrycode(countrycodes.get(filename2));
				dbt.setMigrationtime(migartiontime);
				 
				dbt.setChangemark(list.get(59));
				dbt.setFirstdate(list.get(60));	
				dbt.setModelno("NA");
				dbt.setProduct("NA");
				if(list.get(61)==null || list.get(61).trim().equals(""))
					dbt.setOrdertype(0);
				else
					dbt.setOrdertype(Integer.parseInt(list.get(61)));
				
				Session session=(Session)hibernateDao.getSessionFactory().openSession();
				session.save(dbt);
				session.flush();
				list.clear();
				
			}
			
			else
			{
				which=1;
				dbt.setEdate(list.get(0));
				dbt.setSalesorder(list.get(1));
				dbt.setContractno(list.get(2));
				dbt.setCustomercode(list.get(3));		 
				dbt.setName1(list.get(4));
				dbt.setName2(list.get(5));
				dbt.setShiptoname1(list.get(6));
				dbt.setShiptoname2(list.get(7));
				dbt.setEnduser(list.get(8));
				dbt.setFlag("true");
				if(list.get(9)==null || list.get(9).trim().equals(""))
					dbt.setPayment("NA");
				else
				{
					/*Integer iVal=Integer.parseInt(list.get(9));*/
					dbt.setPayment(list.get(9));
				}
				dbt.setPaymentdescript(list.get(10));				
				dbt.setShipvia(list.get(11));
				dbt.setSalesoffice1(list.get(12));
				dbt.setCode1(list.get(13));
				
				if(list.get(14)==null || list.get(14).trim().equals(""))
					dbt.setM1p("NA");
				else
					dbt.setM1p(list.get(14));				
				
				dbt.setSalesman1(list.get(15));
				if(list.get(16)==null || list.get(16).trim().equals(""))
					dbt.setPercent1(0.00);
				else
				dbt.setPercent1(Double.parseDouble(list.get(16)));
				
				if(list.get(17)==null || list.get(17).trim().equals(""))
					dbt.setAmount1(0.00);
				else
				dbt.setAmount1(Double.parseDouble(list.get(17)));
				dbt.setSalesoffice2(list.get(18));
				dbt.setCode2(list.get(19));
				if(list.get(20)==null || list.get(20).trim().equals(""))
					dbt.setM2p("NA");
				else
					dbt.setM2p(list.get(20));
				
				dbt.setSalesman2(list.get(21));
				if(list.get(22)==null || list.get(22).trim().equals(""))
					dbt.setPercent2(0.00);
				else
				dbt.setPercent2(Double.parseDouble(list.get(22)));
				
				if(list.get(23)==null || list.get(23).trim().equals(""))
					dbt.setAmount2(0.00);
				else
				dbt.setAmount2(Double.parseDouble(list.get(23)));	
				
				dbt.setSalesoffice3(list.get(24));
				dbt.setCode3(list.get(25));
				if(list.get(26)==null || list.get(26).trim().equals(""))
					dbt.setM3p("NA");
				else
					dbt.setM3p(list.get(26));
				dbt.setSalesman3(list.get(27));
				if(list.get(28)==null || list.get(28).trim().equals(""))
					dbt.setPercent3(0.00);
				else
				dbt.setPercent3(Double.parseDouble(list.get(28)));
				
				if(list.get(29)==null || list.get(29).trim().equals(""))
					dbt.setAmount3(0.00);
				else
				dbt.setAmount3(Double.parseDouble(list.get(29)));				
				dbt.setSalesoffice4(list.get(30));
				dbt.setCode4(list.get(31));
				if(list.get(32)==null || list.get(32).trim().equals(""))
					dbt.setM4p("NA");
				else
					dbt.setM4p(list.get(32));
				dbt.setSalesman4(list.get(33));
				if(list.get(34)==null || list.get(34).trim().equals(""))
					dbt.setPercent4(0.00);
				else
				dbt.setPercent4(Double.parseDouble(list.get(34)));	
				
				if(list.get(35)==null || list.get(35).trim().equals(""))
					dbt.setAmount4(0.00);
				else
				dbt.setAmount4(Double.parseDouble(list.get(35)));				
				dbt.setCustomerclass(list.get(36));
				dbt.setDelivterm(list.get(37));
				dbt.setDelivdescript(list.get(38));				
				if(list.get(39)==null || list.get(39).trim().equals(""))
					dbt.setQty(0);
				else
				dbt.setQty(Double.valueOf(list.get(39)).intValue());				
				dbt.setUnit(list.get(40));
				dbt.setPid(list.get(41));
				
				if(list.get(42)==null || list.get(42).trim().equals(""))	
					dbt.setPline("NA");
				else	
					dbt.setPline(list.get(42));
				
				dbt.setDescription(list.get(43));
				
					if(list.get(44)==null|| list.get(44).trim().equals(""))
					{
						dbt.setModelno("NA");
					}
					else
					{
						dbt.setModelno(list.get(44));
					}
						
				dbt.setCurrency(list.get(45));
				if(list.get(46)==null || list.get(46).trim().equals(""))
					dbt.setListprice(0.00);
				else
					dbt.setListprice(Double.parseDouble(list.get(46)));
						 
				if(list.get(47)==null || list.get(47).trim().equals(""))
					dbt.setUnitprice(0.00);
				else
				dbt.setUnitprice(Double.parseDouble(list.get(47)));						 
				dbt.setContractamount(list.get(48));
				dbt.setShippingpoint(list.get(49));
				dbt.setCertraw(list.get(50));
				dbt.setNumberofdoc(list.get(51));
				dbt.setTestreport(list.get(52));
				dbt.setUpr(list.get(53));
				
				if(list.get(54)==null || list.get(54).trim().equals(""))
					dbt.setApplicationcode("NA");
				else
					dbt.setApplicationcode(list.get(54));
				
				dbt.setSpecialdrawing(list.get(55));
				dbt.setRequireddeliv(list.get(56));
				dbt.setReplieddeliv(list.get(57));
				dbt.setBookingdate(list.get(58));
				dbt.setNewchange(list.get(59));
				dbt.setCountrycode(countrycodes.get(filename2));
				dbt.setMigrationtime(migartiontime);
				 
				dbt.setChangemark(list.get(60));
				dbt.setFirstdate(list.get(61));				
				if(list.get(62)==null || list.get(62).trim().equals(""))
					dbt.setOrdertype(0);
				else
					dbt.setOrdertype(Integer.parseInt(list.get(62)));
				if(list.get(63)==null  || list.get(63).trim().equals(""))
				{
				dbt.setProduct("NA");
				}
				if(list.get(63)!=null)
				{
				dbt.setProduct(list.get(63));
				}
				
				Session session=(Session)hibernateDao.getSessionFactory().openSession();
				session.save(dbt);
				session.flush();
				list.clear();

				
				
			}
		    }		    	
		    count++;
		}

		in.close(); 
		 }
		
		 }
		
	}
	    
	    
	    this.movingfolders(drive);
	    
	    
		/* File dir1 = new File("E://Data//PGN Daily Book//"+year);
	     File newDir = new File("E://Data//PGN Daily Book//"+year+"-"+migartiontimefolder);
	     dir1.renameTo(newDir);
	     dir1.mkdir();*/
		//migrateYtdBooking("SG");//YTDBooking Table
		
		// don't forget to close resource leaks
		
		/*ReadFile rf=new ReadFile();
		File theNewestFile =rf.getTheNewestFile("D://Feeds//", "txt");
		if(!(theNewestFile==null))
		System.out.println(theNewestFile.getName());*/
	    
	    
	    
	    Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	    
	    
	   String thsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'TH' and  customercode in ('SMMA','SMID','SMPH','SMVN','SMAU','SCA','SMIN','SMSG')";
       String sgsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'SG' and  customercode in ('SMMA','SMID','SMPH','SMVN','SMAU','SCA','SMTH')";	    
       String vnsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'VN' and  customercode in ('SMMA','SMID','SMPH','SMSG','SMAU','SCA','SMIN','SMTH')";
       String mysql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'MY' and  customercode in ('SMVN','SMID','SMPH','SMSG','SMAU','SCA','SMIN','SMTH')";
       String phsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'PH' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMAU','SCA','SMIN','SMTH')";
       //String insql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'IN' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMAU','SCA','SMPH','SMTH')";
       String ausql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'AU' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMIN','SCA','SMPH','SMTH')";
       String idsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'ID' and  customercode in ('SMVN','SMAU','SMMA','SMSG','SMIN','SCA','SMPH','SMTH')";
        
       
      Query thquery= session1.createQuery(thsql);
      Query sgquery= session1.createQuery(sgsql);
      Query vnquery= session1.createQuery(vnsql);
      Query myquery= session1.createQuery(mysql);
      Query phquery= session1.createQuery(phsql);
      
      Query auquery= session1.createQuery(ausql);
      Query idquery= session1.createQuery(idsql);
      
      thquery.executeUpdate();
      sgquery.executeUpdate();
      vnquery.executeUpdate();
      myquery.executeUpdate();
      phquery.executeUpdate();
      
      auquery.executeUpdate();  
      idquery.executeUpdate();  
      
      
      logger.info("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+which);
      System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+which);
      
     this.migrateProductionSchedularData();
      this.migrateYtdBooking();
      
		}catch(NumberFormatException e)  
		{
			Session session1=(Session)hibernateDao.getSessionFactory().openSession();
			Query q=session1.createQuery("delete from MigrationExceptionCheck");
			q.executeUpdate();
			
			MigrationExceptionCheck mec=new MigrationExceptionCheck();
			 
			mec.setSalesorder(slaesorderrecord);
			mec.setCause(e.getLocalizedMessage()+" - String Type Convert To Integer Type");
			mec.setFeedname(currentfile);
			mec.setMigrationtime(migartiontime);
			session1.save(mec);
			 logger.info("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+e.getStackTrace());
			
		}catch(IndexOutOfBoundsException e)
		{
			Session session1=(Session)hibernateDao.getSessionFactory().openSession();
			
			Query q=session1.createQuery("delete from MigrationExceptionCheck");
			q.executeUpdate();
			
			MigrationExceptionCheck mec=new MigrationExceptionCheck();
			 
			mec.setSalesorder(slaesorderrecord);
			mec.setCause(e.getLocalizedMessage()+" - Index Out Of Bounds Exception");
			mec.setFeedname(currentfile);
			mec.setMigrationtime(migartiontime);
			session1.save(mec);
			 logger.info("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+e.getStackTrace());
			
		}
		catch(Exception e)
		{
			Session session1=(Session)hibernateDao.getSessionFactory().openSession();
			Query q=session1.createQuery("delete from MigrationExceptionCheck");
			q.executeUpdate();

			MigrationExceptionCheck mec=new MigrationExceptionCheck();
			 
			mec.setSalesorder(slaesorderrecord);
			mec.setCause(e.getLocalizedMessage()+" - check in logs for  the exception");
			mec.setFeedname(currentfile);
			mec.setMigrationtime(migartiontime);
			session1.save(mec);
			 logger.info("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+e.getStackTrace());
			
		}
     // this.migrateProductionSchedularData();
     // migrateProductionSchedularData_From_14_Nov_2016();
      
	
	}

	
	
	
	public  void InsertD() throws Exception
	{
		String slaesorderrecord=null;
		String currentfile=null;
		String migartiontimefolder=null;
		
		
		
		try{
		
		StringTokenizer st=null;
		//String slaesorderrecord=null;
		//String currentfile=null;
		DailyBookingTemp dbt=null;
		int which=5;
		
		List<String> list=new ArrayList<String>();
		ArrayList<String> foldername=new  ArrayList<String>();
		foldername.add("SCA");
		foldername.add("SMAU");
		foldername.add("SMID");
		foldername.add("SMMA");
		foldername.add("SMPH");
		foldername.add("SMTH");
		foldername.add("SMVN");
		foldername.add("sca");
		foldername.add("smau");
		foldername.add("smid");
		foldername.add("smma");
		foldername.add("smph");
		foldername.add("smth");
		foldername.add("smvn");
		
		
		
		HashMap<String,String> countrycodes=  new HashMap<String,String>();
		
		countrycodes.put("BK","TH");
		countrycodes.put("SG","SG");
		countrycodes.put("HC","VN");
		countrycodes.put("KP","MY");
		countrycodes.put("MN","PH");
		countrycodes.put("JK","ID");
		countrycodes.put("SY","AU");
		/*countrycodes.put("ID","ID");*/
		
		
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
		int day = now.get(Calendar.DAY_OF_MONTH);
		int hour = now.get(Calendar.HOUR_OF_DAY);
		int minute = now.get(Calendar.MINUTE);
		int second = now.get(Calendar.SECOND);
		int millis = now.get(Calendar.MILLISECOND);
	   migartiontime=year+"-"+month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
	    migartiontimefolder=month+"-"+day+"-"+hour+"-"+minute+"-"+second+"-"+millis;
		
		/*migartiontime="aaa";*/
		
		
	   // File dir = new File("D://CRMSCADATA//PGN Daily Book//"+year);
	    File dir = new File("D:\\crmscadata\\Booking");
	    //FileFilter fileFilter = new WildcardFilter("*." + ext);
	    File[] files = dir.listFiles();
	    for(int i=0;i<files.length;i++)
	    {
	    	
	    	
	   	String absloutefolderpathinthesysytem=files[i].getAbsolutePath();
		String	 f1= absloutefolderpathinthesysytem.substring(absloutefolderpathinthesysytem.lastIndexOf('\\') + 1);
				 
	    	
	    	
	    	
		 if(foldername.contains(f1))
		 {
	    File absloutefolderpathinthesysytemobject = new File(absloutefolderpathinthesysytem);
		File[] absloutefolderpathinthesysytemfiles = absloutefolderpathinthesysytemobject.listFiles();
	    	
	    	
	    	
		 for(int absf=0;absf<absloutefolderpathinthesysytemfiles.length;absf++)
		 {
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	int count=0;
	    	String FileName=absloutefolderpathinthesysytemfiles[absf].getAbsolutePath();
	    	currentfile=FileName;
	    	logger.info("----------------------------FileName---------------------"+FileName.substring(Math.max(FileName.length() - 2, 0)));
		Scanner in = new Scanner(new File(absloutefolderpathinthesysytemfiles[absf].getAbsolutePath()));
		while (in.hasNext()) 
		{
			String filenme1=FileName.substring(0, FileName.length() - 4);
			
			String filename2=filenme1.substring(Math.max(filenme1.length() - 2, 0));
			
			logger.debug("startrdddddddddddddddddddddddddd----------------------------------->");
			System.out.println("startred------------------------------------------------------->"+FileName );
		    String line = in.nextLine();
		    if(count==0)
		    {
		    	count++;
		    	continue;
		    }		    
		    else
		    {
		    	String[] str=line.split("\\|");//Need to use Escape sequence \\ for Pipe(|)
				for(int k=0;k<str.length;k++)
				{
					list.add(str[k]);
				}
				dbt=new DailyBookingTemp();	
				slaesorderrecord=list.get(1);
			String date=(String)list.get(0);
			int datetocompare=Integer.parseInt(list.get(0));
			 
			if(datetocompare<20161114)
			{
				which=0;
				dbt.setEdate(list.get(0));
				dbt.setSalesorder(list.get(1));
				dbt.setContractno(list.get(2));
				dbt.setCustomercode(list.get(3));		 
				dbt.setName1(list.get(4));
				dbt.setName2(list.get(5));
				dbt.setShiptoname1(list.get(6));
				dbt.setShiptoname2(list.get(7));
				dbt.setEnduser(list.get(8));
				dbt.setFlag("true");
				if(list.get(9)==null || list.get(9).trim().equals(""))
					dbt.setPayment("NA");
				else
				{
					/*Integer iVal=Integer.parseInt(list.get(9));*/
					dbt.setPayment(list.get(9));
				}
				dbt.setPaymentdescript(list.get(10));				
				dbt.setShipvia(list.get(11));
				dbt.setSalesoffice1(list.get(12));
				dbt.setCode1(list.get(13));
				
				if(list.get(14)==null || list.get(14).trim().equals(""))
					dbt.setM1p("NA");
				else
					dbt.setM1p(list.get(14));				
				
				dbt.setSalesman1(list.get(15));
				if(list.get(16)==null || list.get(16).trim().equals(""))
					dbt.setPercent1(0.00);
				else
				dbt.setPercent1(Double.parseDouble(list.get(16)));
				
				if(list.get(17)==null || list.get(17).trim().equals(""))
					dbt.setAmount1(0.00);
				else
				dbt.setAmount1(Double.parseDouble(list.get(17)));
				dbt.setSalesoffice2(list.get(18));
				dbt.setCode2(list.get(19));
				if(list.get(20)==null || list.get(20).trim().equals(""))
					dbt.setM2p("NA");
				else
					dbt.setM2p(list.get(20));
				
				dbt.setSalesman2(list.get(21));
				if(list.get(22)==null || list.get(22).trim().equals(""))
					dbt.setPercent2(0.00);
				else
				dbt.setPercent2(Double.parseDouble(list.get(22)));
				
				if(list.get(23)==null || list.get(23).trim().equals(""))
					dbt.setAmount2(0.00);
				else
				dbt.setAmount2(Double.parseDouble(list.get(23)));	
				
				dbt.setSalesoffice3(list.get(24));
				dbt.setCode3(list.get(25));
				if(list.get(26)==null || list.get(26).trim().equals(""))
					dbt.setM3p("NA");
				else
					dbt.setM3p(list.get(26));
				dbt.setSalesman3(list.get(27));
				if(list.get(28)==null || list.get(28).trim().equals(""))
					dbt.setPercent3(0.00);
				else
				dbt.setPercent3(Double.parseDouble(list.get(28)));
				
				if(list.get(29)==null || list.get(29).trim().equals(""))
					dbt.setAmount3(0.00);
				else
				dbt.setAmount3(Double.parseDouble(list.get(29)));				
				dbt.setSalesoffice4(list.get(30));
				dbt.setCode4(list.get(31));
				if(list.get(32)==null || list.get(32).trim().equals(""))
					dbt.setM4p("NA");
				else
					dbt.setM4p(list.get(32));
				dbt.setSalesman4(list.get(33));
				if(list.get(34)==null || list.get(34).trim().equals(""))
					dbt.setPercent4(0.00);
				else
				dbt.setPercent4(Double.parseDouble(list.get(34)));	
				
				if(list.get(35)==null || list.get(35).trim().equals(""))
					dbt.setAmount4(0.00);
				else
				dbt.setAmount4(Double.parseDouble(list.get(35)));				
				dbt.setCustomerclass(list.get(36));
				dbt.setDelivterm(list.get(37));
				dbt.setDelivdescript(list.get(38));				
				if(list.get(39)==null || list.get(39).trim().equals(""))
					dbt.setQty(0);
				else
				dbt.setQty(Double.valueOf(list.get(39)).intValue());				
				dbt.setUnit(list.get(40));
				dbt.setPid(list.get(41));
				
				if(list.get(42)==null || list.get(42).trim().equals(""))	
					dbt.setPline("NA");
				else	
					dbt.setPline(list.get(42));
				
				dbt.setDescription(list.get(43));
				
				dbt.setCurrency(list.get(44));
				if(list.get(45)==null || list.get(45).trim().equals(""))
					dbt.setListprice(0.00);
				else
					dbt.setListprice(Double.parseDouble(list.get(45)));
						 
				if(list.get(46)==null || list.get(46).trim().equals(""))
					dbt.setUnitprice(0.00);
				else
				dbt.setUnitprice(Double.parseDouble(list.get(46)));						 
				dbt.setContractamount(list.get(47));
				dbt.setShippingpoint(list.get(48));
				dbt.setCertraw(list.get(49));
				dbt.setNumberofdoc(list.get(50));
				dbt.setTestreport(list.get(51));
				dbt.setUpr(list.get(52));
				
				if(list.get(53)==null || list.get(53).trim().equals(""))
					dbt.setApplicationcode("NA");
				else
					dbt.setApplicationcode(list.get(53));
				
				dbt.setSpecialdrawing(list.get(54));
				dbt.setRequireddeliv(list.get(55));
				dbt.setReplieddeliv(list.get(56));
				dbt.setBookingdate(list.get(57));
				dbt.setNewchange(list.get(58));
				dbt.setCountrycode(countrycodes.get(filename2));
				dbt.setMigrationtime(migartiontime);
				 
				dbt.setChangemark(list.get(59));
				dbt.setFirstdate(list.get(60));	
				dbt.setModelno("NA");
				dbt.setProduct("NA");
				if(list.get(61)==null || list.get(61).trim().equals(""))
					dbt.setOrdertype(0);
				else
					dbt.setOrdertype(Integer.parseInt(list.get(61)));
				
				Session session=(Session)hibernateDao.getSessionFactory().openSession();
				session.save(dbt);
				session.flush();
				list.clear();
				
			}
			
			else
			{
				which=1;
				dbt.setEdate(list.get(0));
				dbt.setSalesorder(list.get(1));
				dbt.setContractno(list.get(2));
				dbt.setCustomercode(list.get(3));		 
				dbt.setName1(list.get(4));
				dbt.setName2(list.get(5));
				dbt.setShiptoname1(list.get(6));
				dbt.setShiptoname2(list.get(7));
				dbt.setEnduser(list.get(8));
				dbt.setFlag("true");
				if(list.get(9)==null || list.get(9).trim().equals(""))
					dbt.setPayment("NA");
				else
				{
					/*Integer iVal=Integer.parseInt(list.get(9));*/
					dbt.setPayment(list.get(9));
				}
				dbt.setPaymentdescript(list.get(10));				
				dbt.setShipvia(list.get(11));
				dbt.setSalesoffice1(list.get(12));
				dbt.setCode1(list.get(13));
				
				if(list.get(14)==null || list.get(14).trim().equals(""))
					dbt.setM1p("NA");
				else
					dbt.setM1p(list.get(14));				
				
				dbt.setSalesman1(list.get(15));
				if(list.get(16)==null || list.get(16).trim().equals(""))
					dbt.setPercent1(0.00);
				else
				dbt.setPercent1(Double.parseDouble(list.get(16)));
				
				if(list.get(17)==null || list.get(17).trim().equals(""))
					dbt.setAmount1(0.00);
				else
				dbt.setAmount1(Double.parseDouble(list.get(17)));
				dbt.setSalesoffice2(list.get(18));
				dbt.setCode2(list.get(19));
				if(list.get(20)==null || list.get(20).trim().equals(""))
					dbt.setM2p("NA");
				else
					dbt.setM2p(list.get(20));
				
				dbt.setSalesman2(list.get(21));
				if(list.get(22)==null || list.get(22).trim().equals(""))
					dbt.setPercent2(0.00);
				else
				dbt.setPercent2(Double.parseDouble(list.get(22)));
				
				if(list.get(23)==null || list.get(23).trim().equals(""))
					dbt.setAmount2(0.00);
				else
				dbt.setAmount2(Double.parseDouble(list.get(23)));	
				
				dbt.setSalesoffice3(list.get(24));
				dbt.setCode3(list.get(25));
				if(list.get(26)==null || list.get(26).trim().equals(""))
					dbt.setM3p("NA");
				else
					dbt.setM3p(list.get(26));
				dbt.setSalesman3(list.get(27));
				if(list.get(28)==null || list.get(28).trim().equals(""))
					dbt.setPercent3(0.00);
				else
				dbt.setPercent3(Double.parseDouble(list.get(28)));
				
				if(list.get(29)==null || list.get(29).trim().equals(""))
					dbt.setAmount3(0.00);
				else
				dbt.setAmount3(Double.parseDouble(list.get(29)));				
				dbt.setSalesoffice4(list.get(30));
				dbt.setCode4(list.get(31));
				if(list.get(32)==null || list.get(32).trim().equals(""))
					dbt.setM4p("NA");
				else
					dbt.setM4p(list.get(32));
				dbt.setSalesman4(list.get(33));
				if(list.get(34)==null || list.get(34).trim().equals(""))
					dbt.setPercent4(0.00);
				else
				dbt.setPercent4(Double.parseDouble(list.get(34)));	
				
				if(list.get(35)==null || list.get(35).trim().equals(""))
					dbt.setAmount4(0.00);
				else
				dbt.setAmount4(Double.parseDouble(list.get(35)));				
				dbt.setCustomerclass(list.get(36));
				dbt.setDelivterm(list.get(37));
				dbt.setDelivdescript(list.get(38));				
				if(list.get(39)==null || list.get(39).trim().equals(""))
					dbt.setQty(0);
				else
				dbt.setQty(Double.valueOf(list.get(39)).intValue());				
				dbt.setUnit(list.get(40));
				dbt.setPid(list.get(41));
				
				if(list.get(42)==null || list.get(42).trim().equals(""))	
					dbt.setPline("NA");
				else	
					dbt.setPline(list.get(42));
				
				dbt.setDescription(list.get(43));
				
					if(list.get(44)==null|| list.get(44).trim().equals(""))
					{
						dbt.setModelno("NA");
					}
					else
					{
						dbt.setModelno(list.get(44));
					}
						
				dbt.setCurrency(list.get(45));
				if(list.get(46)==null || list.get(46).trim().equals(""))
					dbt.setListprice(0.00);
				else
					dbt.setListprice(Double.parseDouble(list.get(46)));
						 
				if(list.get(47)==null || list.get(47).trim().equals(""))
					dbt.setUnitprice(0.00);
				else
				dbt.setUnitprice(Double.parseDouble(list.get(47)));						 
				dbt.setContractamount(list.get(48));
				dbt.setShippingpoint(list.get(49));
				dbt.setCertraw(list.get(50));
				dbt.setNumberofdoc(list.get(51));
				dbt.setTestreport(list.get(52));
				dbt.setUpr(list.get(53));
				
				if(list.get(54)==null || list.get(54).trim().equals(""))
					dbt.setApplicationcode("NA");
				else
					dbt.setApplicationcode(list.get(54));
				
				dbt.setSpecialdrawing(list.get(55));
				dbt.setRequireddeliv(list.get(56));
				dbt.setReplieddeliv(list.get(57));
				dbt.setBookingdate(list.get(58));
				dbt.setNewchange(list.get(59));
				dbt.setCountrycode(countrycodes.get(filename2));
				dbt.setMigrationtime(migartiontime);
				 
				dbt.setChangemark(list.get(60));
				dbt.setFirstdate(list.get(61));				
				if(list.get(62)==null || list.get(62).trim().equals(""))
					dbt.setOrdertype(0);
				else
					dbt.setOrdertype(Integer.parseInt(list.get(62)));
				if(list.get(63)==null  || list.get(63).trim().equals(""))
				{
				dbt.setProduct("NA");
				}
				if(list.get(63)!=null)
				{
				dbt.setProduct(list.get(63));
				}
				
				Session session=(Session)hibernateDao.getSessionFactory().openSession();
				session.save(dbt);
				session.flush();
				list.clear();

				
				
			}
		    }		    	
		    count++;
		}

		in.close(); 
		 }
		
		 }
		
	}
	    
	    
	    this.movingfolders("D");
	    
	    
	/*	 File dir1 = new File("D://Data//PGN Daily Book//"+year);
	     File newDir = new File("D://Data//PGN Daily Book//"+year+"-"+migartiontimefolder);
	     dir1.renameTo(newDir);
	     dir1.mkdir();*/
		//migrateYtdBooking("SG");//YTDBooking Table
		
		// don't forget to close resource leaks
		
		/*ReadFile rf=new ReadFile();
		File theNewestFile =rf.getTheNewestFile("D://Feeds//", "txt");
		if(!(theNewestFile==null))
		System.out.println(theNewestFile.getName());*/
	    
	    
	    
	    Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	    
	    
	   String thsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'TH' and  customercode in ('SMMA','SMID','SMPH','SMVN','SMAU','SCA','SMIN','SMSG')";
       String sgsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'SG' and  customercode in ('SMMA','SMID','SMPH','SMVN','SMAU','SCA','SMTH')";	    
       String vnsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'VN' and  customercode in ('SMMA','SMID','SMPH','SMSG','SMAU','SCA','SMIN','SMTH')";
       String mysql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'MY' and  customercode in ('SMVN','SMID','SMPH','SMSG','SMAU','SCA','SMIN','SMTH')";
       String phsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'PH' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMAU','SCA','SMIN','SMTH')";
       //String insql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'IN' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMAU','SCA','SMPH','SMTH')";
       String ausql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'AU' and  customercode in ('SMVN','SMID','SMMA','SMSG','SMIN','SCA','SMPH','SMTH')";
       String idsql= "Update DailyBookingTemp set flag  = 'false' where countrycode = 'ID' and  customercode in ('SMVN','SMAU','SMMA','SMSG','SMIN','SCA','SMPH','SMTH')";
        
       
      Query thquery= session1.createQuery(thsql);
      Query sgquery= session1.createQuery(sgsql);
      Query vnquery= session1.createQuery(vnsql);
      Query myquery= session1.createQuery(mysql);
      Query phquery= session1.createQuery(phsql);
      
      Query auquery= session1.createQuery(ausql);
      Query idquery= session1.createQuery(idsql);
      
      thquery.executeUpdate();
      sgquery.executeUpdate();
      vnquery.executeUpdate();
      myquery.executeUpdate();
      phquery.executeUpdate();
      
      auquery.executeUpdate();  
      idquery.executeUpdate();  
      
      
      logger.info("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+which);
      System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+which);
      
        this.migrateProductionSchedularData();
         this.migrateYtdBooking();
      
		}catch(NumberFormatException e)  
		{
			Session session1=(Session)hibernateDao.getSessionFactory().openSession();
			Query q=session1.createQuery("delete from MigrationExceptionCheck");
			q.executeUpdate();
			
			MigrationExceptionCheck mec=new MigrationExceptionCheck();
			 
			mec.setSalesorder(slaesorderrecord);
			mec.setCause(e.getLocalizedMessage()+" - String Type Convert To Integer Type");
			mec.setFeedname(currentfile);
			mec.setMigrationtime(migartiontime);
			session1.save(mec);
			 logger.info("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+e.getStackTrace());
			
		}catch(IndexOutOfBoundsException e)
		{
			Session session1=(Session)hibernateDao.getSessionFactory().openSession();
			
			Query q=session1.createQuery("delete from MigrationExceptionCheck");
			q.executeUpdate();
			
			MigrationExceptionCheck mec=new MigrationExceptionCheck();
			 
			mec.setSalesorder(slaesorderrecord);
			mec.setCause(e.getLocalizedMessage()+" - Index Out Of Bounds Exception");
			mec.setFeedname(currentfile);
			mec.setMigrationtime(migartiontime);
			session1.save(mec);
			 logger.info("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+e.getStackTrace());
			
		}
		catch(Exception e)
		{
			Session session1=(Session)hibernateDao.getSessionFactory().openSession();
			Query q=session1.createQuery("delete from MigrationExceptionCheck");
			q.executeUpdate();

			MigrationExceptionCheck mec=new MigrationExceptionCheck();
			 
			mec.setSalesorder(slaesorderrecord);
			mec.setCause(e.getLocalizedMessage()+" - check in logs for  the exception");
			mec.setFeedname(currentfile);
			mec.setMigrationtime(migartiontime);
			session1.save(mec);
			 logger.info("-------------------------------------------------------------------------------------------------------------------------------------------------------------------->"+e.getStackTrace());
			
		}
     // this.migrateProductionSchedularData();
     // migrateProductionSchedularData_From_14_Nov_2016();
      
	
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void migrateProductionSchedularData()
	{	
		Map hm=null;
		Map psMap=null;
		ProductionScheduler ps=null;
		ProductionScheduler ps1=null;
		List entityList=null;
		try
		{			
			List tempdata=migratesevice.getAllTheBookingDetailsFromTempTableForProductionScheduler(migartiontime);
			Session session = (Session)hibernateDao.getSessionFactory().openSession();
			Date sysdate = new Date();
		 
			List<ProductionScheduler> scheduledData=migratesevice.getAllTheScheduledFromProductionScheduler();
			boolean isPresent=false;
			for(int i=0;i<tempdata.size();i++)
			{
				logger.info("------- Production Scheduler  Record Number --------> "+(i+1));
				
				hm=(HashMap)tempdata.get(i);
				String newChange=(String)hm.get("newchange");
				
				//if("SG".equalsIgnoreCase((String)hm.get("shippingpoint")))
				//{
				String requireddt1=(String)hm.get("requireddeliv");
				String newReqDelDate1=requireddt1.substring(6,8)+"-"+requireddt1.substring(4,6)+"-"+requireddt1.substring(0,4);
				java.sql.Date d1=DateUtils.getSqlDateFromString(newReqDelDate1,Constants.GenericDateFormat.DATE_FORMAT);
				java.sql.Date d2 = new java.sql.Date(sysdate.getTime());
				Calendar calendar1 = Calendar.getInstance();
		        calendar1.setTime(d1);
		        Calendar calendar2 = Calendar.getInstance();
		        calendar2.setTime(d2);
		        String str = "";
		        long timeinmillis1 = calendar1.getTimeInMillis();
		        long timeinmillis2 = calendar2.getTimeInMillis();
		        if (timeinmillis1 < timeinmillis2)
		        {
		          continue;
		        } 
		        
				if("P".equalsIgnoreCase((String)hm.get("upr")))
				{					
					scheduledData=migratesevice.getAllTheScheduledFromProductionScheduler();
					for(int k=0;k<scheduledData.size();k++)
					{
						isPresent=false;
						ProductionScheduler productionSchedulers=(ProductionScheduler)scheduledData.get(k);
						if(productionSchedulers.getUpr().equalsIgnoreCase("P"))
						{
							String[] spSalesOrderOld=productionSchedulers.getSalesorder().split("-");
							String[] spSalesOrderNew=hm.get("salesorder").toString().split("-");
							
							if(spSalesOrderOld[0].equalsIgnoreCase(spSalesOrderNew[0]))
							{
								String requireddt=(String)hm.get("requireddeliv");
								String newReqDelDate=requireddt.substring(6,8)+"-"+requireddt.substring(4,6)+"-"+requireddt.substring(0,4);
								
								String one1=DateUtils.getSqlDateFromString(newReqDelDate,Constants.GenericDateFormat.DATE_FORMAT).toString();
								String two2=productionSchedulers.getReqdeliverydate().toString();
								if(one1.equals(two2))
								{
									isPresent=true;
									break;									
								}
								else
								{
									Session session1 = (Session)hibernateDao.getSessionFactory().openSession();
									productionSchedulers.setLineitemstatus("invalid");
									productionSchedulers.setId(productionSchedulers.getId());
									session1.saveOrUpdate(productionSchedulers);
									session1.flush();
									break;
								}
							}							
						}						
					}			
					if(isPresent)
						continue;
				}
				
				ps=new ProductionScheduler();					
				ps.setApplicationcode((String)hm.get("applicationcode"));					
				ps.setChangeitem((String)hm.get("newchange"));
				ps.setContractno((String)hm.get("contractno"));
				ps.setCustomercode((String)hm.get("customercode"));				
				String description=(String)hm.get("description");	
				ps.setModelno("NA");
					
				if(description.indexOf("HBB")!=-1 || description.indexOf("HYPONIC")!=-1 || description.indexOf("PREST NEO")!=-1 || description.indexOf("CYCLO")!=-1)
				{						
					if(description.indexOf("CYCLO")!=-1 && description.indexOf("-6")!=-1)
					{
							Integer i1=description.indexOf("-6");
							String s=description.substring(0, i1);
							Integer i2=s.lastIndexOf(" ");
							String modelNo=description.substring(i2, description.lastIndexOf("")).replace("*", "").trim();
							ps.setModelno(modelNo);
						
					}
					else if(description.indexOf("HYPONIC")!=-1 && description.indexOf("RN")!=-1)
					{
						String modelNo=description.substring(description.indexOf("RN"), description.lastIndexOf("")).replace("*", "").trim();
						ps.setModelno(modelNo);
					}
					else if(description.indexOf("PREST NEO")!=-1 && description.indexOf("ZN")!=-1)
					{
						String modelNo=description.substring(description.indexOf("ZN"), description.lastIndexOf("")).replace("*", "").trim();
						ps.setModelno(modelNo);
					}
					/*else if(description.indexOf("BBB")!=-1)
					{
						Integer i1=0;
						if(description.indexOf("-3")!=-1)
							i1=description.indexOf("-3");
							
						if(description.indexOf("-4")!=-1)
							i1=description.indexOf("-4");
						
						String s=description.substring(0, i1);
						Integer i2=s.lastIndexOf(" ");	
						String modelNo=description.substring(i2, description.lastIndexOf("")).replace("*", "").trim();
						ps.setModelno(modelNo);
					}*/
					else if(description.indexOf("HBB")!=-1 && description.indexOf("-")!=-1)
					{
						
						Integer i1=description.indexOf("-");
						String s=description.substring(0, i1);
						Integer i2=s.lastIndexOf(" ");
						
						String modelNo=description.substring(i2, description.lastIndexOf("")).replace("*", "").trim();
						ps.setModelno(modelNo);
						
					}
					else
						ps.setModelno("NA");
					

				}						
				else
				{
					ps.setModelno("NA");
				}	

					ps.setDescription((String)hm.get("description"));				
					ps.setLineitemstatus("valid");
					ps.setName((String)hm.get("name1")+" "+(String)hm.get("name2"));
					ps.setPline((String)hm.get("pline"));
					ps.setPid((String)hm.get("pid"));
					
					ps.setQuantity((Integer)hm.get("qty"));
					String requireddeliv=(String)hm.get("requireddeliv");
					String newDate=requireddeliv.substring(6,8)+"-"+requireddeliv.substring(4,6)+"-"+requireddeliv.substring(0,4);
					ps.setReqdeliverydate(DateUtils.getSqlDateFromString(newDate,Constants.GenericDateFormat.DATE_FORMAT));
					ps.setSalesorder((String)hm.get("salesorder"));
					ps.setTestedby("NA");
					//ps.setTotalscore(totalscore)					
					ps.setTransdate(DateUtils.getCurrentDate(Constants.GenericDateFormat.DATE_FORMAT));
					ps.setUpr((String)hm.get("upr"));
					ps.setShippingpoint((String)hm.get("shippingpoint"));
					if("NA".equalsIgnoreCase(ps.getModelno()))
					{
						ps.setScore(1);
					}
					else
					{
						Double score=getScore(ps.getModelno());
						if(score==null)
						{
							ps.setScore(0);
						}
						else
							ps.setScore(score.intValue());
					}
					
					if("NEW".equalsIgnoreCase(newChange))
					{
						ps.setProdstatus("New");
						session.save(ps);
						session.flush();
					}
					else if("CANCEL".equalsIgnoreCase(newChange))
					{
						if("P".equalsIgnoreCase((String)hm.get("upr")))
						{
							ps.setProdstatus("To be cancelled");
							session.save(ps);
							session.flush();
						}
						else
						{
							scheduledData=migratesevice.getAllTheScheduledFromProductionScheduler();
							if(scheduledData.size()>0 && scheduledData!=null)
							{
								for(int k=0;k<scheduledData.size();k++)
								{
									ProductionScheduler productionSchedulerCan=(ProductionScheduler)scheduledData.get(k);
									
									if(productionSchedulerCan.getSalesorder().equalsIgnoreCase((String)hm.get("salesorder")))
									{
										ps.setAssemblydate(productionSchedulerCan.getAssemblydate());
										ps.setDuedate(productionSchedulerCan.getDuedate());
										break;
									}
								}
							}
							
							ps.setProdstatus("To be cancelled");
							session.save(ps);
							session.flush();
						}
					}
					else if("CHANGE".equalsIgnoreCase(newChange))
					{
						boolean occurance=false;
						if("P".equalsIgnoreCase((String)hm.get("upr")))
						{
							ps.setProdstatus("Modified");
							session.save(ps);
							session.flush();
						}
						else
						{							
							entityList=new ArrayList();
							scheduledData=migratesevice.getAllTheScheduledFromProductionScheduler();
							if(scheduledData.size()>0 && scheduledData!=null)
							{
								String requireddt2=(String)hm.get("requireddeliv");
								String newReqDelDate2=requireddt1.substring(6,8)+"-"+requireddt1.substring(4,6)+"-"+requireddt1.substring(0,4);

								for(int j=0;j<scheduledData.size();j++)
								{
									ProductionScheduler productionScheduler=(ProductionScheduler)scheduledData.get(j);
									
									if(productionScheduler.getSalesorder().equalsIgnoreCase((String)hm.get("salesorder")))
									{
										if((Integer)hm.get("qty")!=0)
										{
											ps.setLineitemstatus("invalid");
											ps.setProdstatus("Modified");
											entityList.add(ps);
											ps.setChangeditem1("qty");											
											//save new Record withe old data..
											ps1=new ProductionScheduler();
											ps1.setApplicationcode((String)hm.get("applicationcode"));										
											ps1.setChangeitem((String)hm.get("newchange"));
											ps1.setContractno((String)hm.get("contractno"));
											ps1.setCustomercode((String)hm.get("customercode"));				
											ps1.setModelno("NA");
											ps1.setDescription((String)hm.get("description"));				
											ps1.setName((String)hm.get("name1")+" "+(String)hm.get("name2"));
											ps1.setPline((String)hm.get("pline"));
											//ps1.setReqdeliverydate(DateUtils.getSqlDateFromString(newDate,Constants.GenericDateFormat.DATE_FORMAT));
											ps1.setReqdeliverydate(productionScheduler.getReqdeliverydate());										
											ps1.setSalesorder((String)hm.get("salesorder"));
											ps1.setShippingpoint((String)hm.get("shippingpoint"));
											if((String)hm.get("pid")!=(String)productionScheduler.getPid())
											{
												ps1.setChangeditem2("pid");
											}
											if((String)hm.get("description")!=(String)productionScheduler.getDescription())
											{
												ps1.setChangeditem3("description");
											}									
											if(!DateUtils.getSqlDateFromString(newReqDelDate2,Constants.GenericDateFormat.DATE_FORMAT).toString().equals(productionScheduler.getReqdeliverydate().toString()))
											{
												ps1.setChangeditem4("reqdeliverydate");
											}
											ps1.setQuantity((Integer)hm.get("qty")+productionScheduler.getQuantity());
											ps1.setDescription((String)hm.get("description"));
											ps1.setPid((String)hm.get("pid"));
											//ps1.setReqdeliverydate(DateUtils.getSqlDateFromString((String)hm.get("reqdeliverydate")));
											ps1.setAssemblydate(productionScheduler.getAssemblydate());
											
											//Double score1=getScore(ps.getModelno());
											ps1.setScore(productionScheduler.getScore());
											ps1.setLineitemstatus("valid");
											ps1.setProdstatus("Modified");	
											ps1.setUpr((String)hm.get("upr"));
											ps1.setCompleteddate(productionScheduler.getCompleteddate());
											ps1.setShippingpoint((String)hm.get("shippingpoint"));
											ps1.setTotalscore(ps1.getQuantity()*productionScheduler.getScore());
											ps1.setModelno(productionScheduler.getModelno());
											ps1.setDuedate(productionScheduler.getDuedate());
											ps1.setTransdate(DateUtils.getCurrentDate(Constants.GenericDateFormat.DATE_FORMAT));
											ps1.setTestedby("NA");
											entityList.add(ps1);										
											//Update Old Record
											productionScheduler.setLineitemstatus("invalid");
											entityList.add(productionScheduler);
											
											masterScreenDao.saveMultipleEntities(entityList);	

											session.flush();
											
											occurance=true;
										}
										else
										{
											ps.setLineitemstatus("invalid");
											ps.setProdstatus("Modified");
											entityList.add(ps);
											
											//save new Record withe old data..
											ps1=new ProductionScheduler();
											
											ps1.setApplicationcode((String)hm.get("applicationcode"));										
											ps1.setChangeitem((String)hm.get("newchange"));
											ps1.setContractno((String)hm.get("contractno"));
											ps1.setCustomercode((String)hm.get("customercode"));				
											ps1.setModelno("NA");
											ps1.setDescription((String)hm.get("description"));				
											ps1.setName((String)hm.get("name1")+" "+(String)hm.get("name2"));
											ps1.setPline((String)hm.get("pline"));
											//ps1.setReqdeliverydate(DateUtils.getSqlDateFromString(newDate,Constants.GenericDateFormat.DATE_FORMAT));
											ps1.setReqdeliverydate(productionScheduler.getReqdeliverydate());
											
											ps1.setSalesorder((String)hm.get("salesorder"));
											ps1.setChangeditem1("qty");
											ps1.setShippingpoint((String)hm.get("shippingpoint"));
											if((String)hm.get("pid")!=(String)productionScheduler.getPid())
											{
												ps1.setChangeditem2("pid");							
											}
											if((String)hm.get("description")!=(String)productionScheduler.getDescription())
											{
												ps1.setChangeditem3("description");
											}		
											String one=DateUtils.getSqlDateFromString(newReqDelDate2,Constants.GenericDateFormat.DATE_FORMAT).toString();
											String two=productionScheduler.getReqdeliverydate().toString();
											if(!DateUtils.getSqlDateFromString(newReqDelDate2,Constants.GenericDateFormat.DATE_FORMAT).toString().equals(productionScheduler.getReqdeliverydate().toString()))
											{
												ps1.setChangeditem4("reqdeliverydate");
											}
											ps1.setQuantity((Integer)hm.get("qty")+productionScheduler.getQuantity());
											ps1.setDescription((String)hm.get("description"));
											ps1.setPid((String)hm.get("pid"));
											//ps1.setReqdeliverydate(DateUtils.getSqlDateFromString((String)hm.get("reqdeliverydate")));
											ps1.setAssemblydate(productionScheduler.getAssemblydate());
											//Double score2=getScore(ps.getModelno());
											ps1.setScore(productionScheduler.getScore());
											ps1.setProdstatus("Modified");
											ps1.setLineitemstatus("valid");
											ps1.setUpr((String)hm.get("upr"));
											ps1.setTotalscore(ps1.getQuantity()*productionScheduler.getScore());
											ps1.setCompleteddate(productionScheduler.getCompleteddate());
											ps1.setShippingpoint((String)hm.get("shippingpoint"));
											ps1.setModelno(productionScheduler.getModelno());
											ps1.setDuedate(productionScheduler.getDuedate());
											ps1.setTransdate(DateUtils.getCurrentDate(Constants.GenericDateFormat.DATE_FORMAT));
											ps1.setTestedby("NA");
											entityList.add(ps1);
											//Update Old Record
											productionScheduler.setLineitemstatus("invalid");
											entityList.add(productionScheduler);
											
											masterScreenDao.saveMultipleEntities(entityList);
										
											occurance=true;
										}
									}	
								}
							}
						}
						if(!occurance)
						{
							ps.setProdstatus("Modified");
							session.save(ps);
							session.flush();
						}
					}
					
				//}
			}			
			
		}catch(Exception e)
		{
			logger.info("The Exception Trace---"+e.getMessage());
		}		
		
	}	
	
	public  Double getScore(String mNum) throws Exception 
	{			
			//logger.info("=============Model Number============="+mNum);		
			String productCode = Character.toString(mNum.trim().charAt(0));
			String stage=null;	
			String frameSize=null;
			Double score=null;
			String productSubType=null;
			String productType=null;
			String frameType="NA";
			String[] spString=mNum.trim().split("-");
			try 
			{			
				if(productCode.equalsIgnoreCase("C"))
				{					
					if(spString[0].contains("M"))
					{
						productSubType="Gear Motor";
					}
					else
						productSubType="Gear Reducer";
					
					productType="CYCLO";
					
					int fOccur=mNum.indexOf("-")+1;
					frameSize=mNum.substring(mNum.indexOf("-")+1,mNum.indexOf("-")+5);
					
					//find out stage
					if(spString[1].length()<6)
					{
						//stage=mNum.substring(mNum.lastIndexOf("-")+1,mNum.lastIndexOf("-")+2);
						List<Score> scoreList=masterScreenDao.getScoreByFrameSize(Integer.parseInt(frameSize), "NA",productType,productSubType,frameType);
						if(scoreList.size()>0)
						{
							Score scoreObj=(Score)scoreList.get(0);
							score=scoreObj.getScore();
						}
					}
					else
					{
						String stagecode=mNum.substring(mNum.indexOf("-")+5,mNum.indexOf("-")+7);
						//get stage	 from db stage table(stageCode)			
						//stages=(Stage)salesManagementService.getRowByColumn(Stage.class,"stagecode",stagecode);
						List<Score> scoreList=masterScreenDao.getScoreByFrameSize(Integer.parseInt(frameSize), stagecode,productType,productSubType,frameType);
						if(scoreList.size()>0)
						{
							Score scoreObj=(Score)scoreList.get(0);
							score=scoreObj.getScore();
						}
						else
							score=0.0;
					}				
				}
				else
				{		
					//frameSize=mNum.substring(mNum.indexOf("-")+1,mNum.indexOf("-")+5);
					frameSize=spString[1];
					//logger.info("============Frame Size=========="+frameSize);
					if(productCode.equalsIgnoreCase("R"))
					{  
						productSubType="NA";
						productType="Hyponic";		
						if(frameSize.matches("[a-zA-z0-9]*"))
						{
							frameSize=frameSize.replaceAll("[\\D]", "");
						}
					}
					else if(productCode.equalsIgnoreCase("Z"))
					{
						productSubType="NA";
						productType="PrestNeo";
					}
					else if(productCode.equalsIgnoreCase("L"))
					{
						productSubType="NA";
						productType="BBB";
						if(frameSize.matches("[a-zA-z0-9]*"))
						{
							String alphabet=frameSize.replaceAll("[\\d]", "");			
							String[] stbs=frameSize.split(alphabet);
							frameSize=stbs[1];
							frameType=stbs[0]+alphabet;
						}
						else
						{
							frameType="NA";
						}
					}
					else if(productCode.equalsIgnoreCase("E"))
					{
						productSubType="NA";
						productType="HBB";
						if(frameSize.matches("[a-zA-z0-9]*"))
						{
							String alphabet=frameSize.replaceAll("[\\d]", "");			
							String[] stbs=frameSize.split(alphabet);
							frameSize=stbs[1];
							frameType=stbs[0]+alphabet;
						}
						else
						{
							frameType="NA";
						}
					}					
					//frameSize=spString[1];					
					List<Score> scoreList=masterScreenDao.getScoreByFrameSize(Integer.parseInt(frameSize), "NA",productType,productSubType,frameType);
					if(scoreList.size()>0)
					{
						Score scoreObj=(Score)scoreList.get(0);
						score=scoreObj.getScore();
					}
					else
						score=0.0;					
				}				
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
		}
		
		return score;
	}
	
	
	
	public  void  getPowerCapacityFromModelNo() throws Exception 
	{
		Session session=null;
		//Transaction tx=null;
		List entityList=new ArrayList();
		try
		{
		session=hibernateDao.getSessionFactory().openSession();
			//tx=session.beginTransaction();
		String sql="select modelno,description,product  from DailyBookingTemp";
		
		List  tempdata=hibernateDao.findBySqlCriteria(sql);
		
		HashMap<String,Double> powerCapacityMap=new HashMap();
		
		powerCapacityMap.put("01", 0.1);
		powerCapacityMap.put("02", 0.2);
		powerCapacityMap.put("03", 0.25);
		powerCapacityMap.put("05", 0.4);
		powerCapacityMap.put("08", 0.55);
		powerCapacityMap.put("1", 0.75);
		powerCapacityMap.put("1H", 1.1);
		powerCapacityMap.put("2", 1.5);
		powerCapacityMap.put("3", 2.2);
		powerCapacityMap.put("4", 3.0);
		powerCapacityMap.put("5", 3.7);
		powerCapacityMap.put("8", 5.5);
		powerCapacityMap.put("10", 7.5);
		powerCapacityMap.put("15", 11.0);
		powerCapacityMap.put("20", 15.0);
		powerCapacityMap.put("25", 18.5);
		powerCapacityMap.put("30", 22.0);
		powerCapacityMap.put("40", 30.0);
		powerCapacityMap.put("50", 37.0);
		powerCapacityMap.put("60", 45.0);
		powerCapacityMap.put("75", 55.0);
		powerCapacityMap.put("100", 75.0);
		
		powerCapacityMap.put("016", 0.1);
		powerCapacityMap.put("026", 0.2);
		powerCapacityMap.put("036", 0.25);
		powerCapacityMap.put("056", 0.4);
		powerCapacityMap.put("086", 0.55);
		powerCapacityMap.put("16", 0.75);
		powerCapacityMap.put("1H6", 1.1);
		powerCapacityMap.put("26", 1.5);
		powerCapacityMap.put("36", 2.2);
		powerCapacityMap.put("46", 3.0);
		powerCapacityMap.put("56", 3.7);
		powerCapacityMap.put("86", 5.5);
		powerCapacityMap.put("106", 7.5);
		powerCapacityMap.put("156", 11.0);
		powerCapacityMap.put("206", 15.0);
		powerCapacityMap.put("256", 18.5);
		powerCapacityMap.put("306", 22.0);
		powerCapacityMap.put("406", 30.0);
		powerCapacityMap.put("506", 37.0);
		powerCapacityMap.put("606", 45.0);
		powerCapacityMap.put("756", 55.0);
		powerCapacityMap.put("1006", 75.0);		
		powerCapacityMap.put("1256", 90.0);
		powerCapacityMap.put("1506", 110.0);
		powerCapacityMap.put("1756", 132.0);
		
		
		for(int i=0;i<tempdata.size();i++)
		{
			logger.info("-----------------RECORD NO---- "+i);
			HashMap hm=(HashMap)tempdata.get(i);
			
			String modelno=(String)hm.get("modelno");
			String description=(String)hm.get("description");
			String product=(String)hm.get("product");
			
			logger.info("-----------------model NO---- "+modelno);
			if(description.indexOf("HBB")!=-1 || description.indexOf("HYPONIC")!=-1 || description.indexOf("PREST NEO")!=-1 || description.indexOf("CYCLO")!=-1)
			{
				if(modelno.indexOf("-")!=-1  && !(modelno.equalsIgnoreCase("BVFM-N10A")) && !(modelno.equalsIgnoreCase("INVERTER MODEL HF3212-3A7")) && !(modelno.equalsIgnoreCase("RNFM05-270R-7.5"))  && !(modelno.equalsIgnoreCase("M4-FHF4024")) && !(modelno.equalsIgnoreCase("BVFM-N5A")) && !(modelno.equalsIgnoreCase("BHHM-N8B")) &&   !(modelno.equalsIgnoreCase("SERVICE CHARGE BHHM-30B")) && !(modelno.equalsIgnoreCase("BHHM-N05A"))) 
				{				
					String[] newSp=modelno.split("-");
					if( newSp[0].indexOf("M")!=-1)
					{
						String capacity=null;
					if(newSp[0].indexOf("MS")!=-1)
						capacity=newSp[0].substring(newSp[0].indexOf("MS")+2,newSp[0].length());
					else
						capacity=newSp[0].substring(newSp[0].indexOf("M")+1,newSp[0].length());
					Double power=powerCapacityMap.get(capacity);
					
					Integer ratio=Integer.parseInt(modelno.substring(modelno.lastIndexOf("-")+1,modelno.length()));				
					
					String frameSize=newSp[1];
					
					MotorSpecifications ms=new MotorSpecifications();
					ms.setCapacity(capacity);
					ms.setRatio(ratio);
					ms.setModelno(modelno);
					ms.setPower(power);
					ms.setFramesize(frameSize);
					ms.setProducttype(product);
					session.merge(ms);
					session.flush();
			        //tx.commit();
					}
					
				}
			}
		}	
		
        session.close();
		}
		catch (Exception e)
		{
			
			logger.info(e.getCause(), e);
		}
	
	}
	
	
	public  void  migrateYtdBooking() throws Exception 
	{
		
		long cal1=Calendar.getInstance().getTimeInMillis();
		
		int changeflag=0; 
		String bdate=null;
		Double amount1=null;
		Double amount2=null;
		Double amount3=null;
		Double amount4=null;
		Double orgamount=0.0;
		String currency=null;
		Double cfactor=0.0;
		String day=null;
		String month=null;
		String year=null;
		String slcode="";
		String slcode1=null;
		String slcode2=null;
		String slcode3=null;
		String slcode4=null;
		String slname="";
		String slname1=null;
		String slname2=null;
		String slname3=null;
		String slname4=null;
		String custname1=null;
		String custname2=null;
		String plineCode=null;
		String appcode=null;
		String industrysector=null;
		String industry=null;
		String countrycode=null;
		String country=null;
		Boolean success=false;
		YtdBooking ytdb=null;
		String cntry=null;
		String tocode=null;
		Double exchangerate1=0.0;
		Double exchangerate3=0.0;
		Double exchangerate4=0.0;
		Double exchangerate5=0.0;
		Double percent1=0.0;
		Double percent2=0.0;
		Double percent3=0.0;
		Double percent4=0.0;
		Double BookingValue=0.0;
		Integer count=0;
		Double amount=0.0;
		Double amountusd=0.0;
		String customercode=null;
		String upr=null;
		List othercustcode=null;
		int ascCount=0;
		String checkingcountrycode=null;
		String regioncode="NA";
		String regionaname="NA";
		
		try
		{			
			List tempdata=migratesevice.getAllTheBookingDetailsFromTempTable(migartiontime);
			Map hm=null;
			Map hm1=null;
			Map hm5=null;
			
			count=tempdata.size();
			for(int i=0;i<tempdata.size();i++)
			{	
				/*if(ascCount==555)
				break;*/
				hm=(HashMap)tempdata.get(i);
				
				
				logger.info("<----------------------------RECORD NUMBER WE ARE INSERTING INTO DB--------------------------->"+(ascCount++));
				
				slcode1=(String)hm.get("m1p");
				slcode2=(String)hm.get("m2p");
				slcode3=(String)hm.get("m3p");
				slcode4=(String)hm.get("m4p");
				slname1=(String)hm.get("salesman1");
				slname2=(String)hm.get("salesman2");
				slname3=(String)hm.get("salesman3");
				slname4=(String)hm.get("salesman4");
				percent1=(Double)hm.get("percent1");
				percent2=(Double)hm.get("percent2");
				percent3=(Double)hm.get("percent3");
				percent4=(Double)hm.get("percent4");
				amount1=(Double)hm.get("amount1");
				amount2=(Double)hm.get("amount2");
				amount3=(Double)hm.get("amount3");
				amount4=(Double)hm.get("amount4");
				orgamount=amount1+amount2+amount3+amount4;
				Double percent[]={percent1,percent2,percent3,percent4};
				String salesmans[]={slcode1,slcode2,slcode3,slcode4};
				customercode=(String)hm.get("customercode");
				String cust_code[]=customercode.split("/");
				String onlysalesman[]={slname1,slname2,slname3,slname4};
				
				//write FOR loop for 4 salesmen
				//for each salesman, get the percent 
				//if percent > 0 , get the salesman code otherwise continue the loop
				//for the salesman code, get the country and thus the currency
				//this currency would be to currency
				//also consider the % while calculating the booking value
				//BookingValue = original amount * percent / 100; - consider the booking value for calculations
				
				for(int j=0;j<4;j++)
				{
					 if(percent[j]>0)
					 {
						 //a,i,m,n,p,s,t,v,
						 if(!salesmans[j].equalsIgnoreCase("NA"))
						 {				 		
						 	countrycode=migratesevice.getIndustrySector1("countrycode",salesmans[j].substring(0,1),"salesmancode","salesman");
						 	//for getting countrycode from salesman table depending up on saleamancode
						 	slname1=onlysalesman[j];
						 	slcode1=salesmans[j];
						 	
						 	 regioncode=migratesevice.getRegioncodeFromSalesmanTable(countrycode,slcode1);
						 	 if(regioncode!=null)
						 	 {
						 	if(!regioncode.equalsIgnoreCase("NA"))
						 	{
						 		Integer regionid=Integer.parseInt(regioncode);
						 		regionaname=migratesevice.getRegionNamefromRegion(countrycode,regionid);
						 		
						 	}
						  
						 	 }
						 	
						 }
						 else
						 {
							 countrycode="NA";
						 }
						 
					 	logger.info("<----------------------------firts cell data--------------------------->"+(Integer)hm.get("slno"));
					 	ArrayList ytd=new ArrayList();
					 	ytdb=new YtdBooking();
					
						
						bdate=(String)hm.get("bookingdate");
								
						year=bdate.substring(0, 4);
						month=bdate.substring(4, 6);
						day=bdate.substring(6, 8);
						int comprisionyear=Integer.parseInt(year);
						ytdb.setBookingyear(Integer.parseInt(year));
						ytdb.setBookingmonth(Integer.parseInt(month));
						ytdb.setBookingday(Integer.parseInt(day));
						ytdb.setMigrationtime(migartiontime);
						ytdb.setBookingdate(DateUtils.getSqlDateFromString(day+"-"+month+"-"+year,Constants.GenericDateFormat.DATE_FORMAT));
						ytdb.setSalesorder((String)hm.get("salesorder"));
						  
						String salesorder=(String)hm.get("salesorder");
						String changestatus=(String)hm.get("newchange");
						
						
						/***************************************Logic for change and Cancel*******************************************************************/
						
						
						
						if(changestatus.equalsIgnoreCase("CHANGE")|| changestatus.equalsIgnoreCase("CANCEL"))		
						{
							int bookingyearcompare=0;
							int bookingmonthcompare=0;
							String countrycodeforold=null;
							Double usd1=0.0;
							Double updatedusd=0.0;
							String testcurrency=migratesevice.getCurrency(salesorder);
							if(testcurrency!=null)
							{
							if(!testcurrency.equalsIgnoreCase((String)hm.get("currency")))
							{
							    Session session1 = (Session) hibernateDao.getSessionFactory().openSession();
								/*String ytdbookingProducttruncate = "delete  from  YtdBookingProduct";
								String ytdbookingregionTruncate = "delete   from   YtdBookingRegion";
								String ytdbookingIndustrytruncate = "delete from YtdBookingIndustry";
	                            Query pt = session1.createQuery(ytdbookingProducttruncate);
								Query rt = session1.createQuery(ytdbookingregionTruncate);
								Query it = session1.createQuery(ytdbookingIndustrytruncate);
								pt.executeUpdate();
								rt.executeUpdate();
								it.executeUpdate();*/								
								changeflag=1;
								
								List<Object[]> amounts1=migratesevice.getAmountandAmountusd(salesorder);
								for(Object[] a:amounts1)
								{
									 amount=(Double)a[0];
									 amountusd=(Double)a[1];
									 bookingyearcompare=(Integer)a[2];
									 bookingmonthcompare=(Integer)a[3];
									 countrycodeforold=(String)a[4];
									 }
						      
								
								
								if(testcurrency.equalsIgnoreCase("USD"))
 {
									//checkhashmapusd.put(k+testcurrency,(String)hm.get("currency")+"kkk "+salesorder);
									//k++;
										Double localcurrencey = amountusd;

                    List<Double> exchrate2 = migratesevice.getExchangeRate(bookingyearcompare,bookingmonthcompare,"USD",(String) hm.get("currency"));

										for (Double usd : exchrate2) 
										{
											usd1 = usd;
										}

										updatedusd = usd1 * localcurrencey;

			migratesevice.updateCurrenceyto(salesorder,(String) hm.get("currency"),updatedusd, localcurrencey,migartiontime);

									}
								
								else
								{
									//checkhashmapelse.put(l+testcurrency,(String)hm.get("currency")+"lll "+salesorder);
									//l++;
								 updatedusd=amountusd;
								 List<Double> exchrate2=migratesevice.getExchangeRate(bookingyearcompare,bookingmonthcompare,"USD",testcurrency);
								 
								 for(Double usd:exchrate2)
								 {
									 usd1=usd;
								 }
								 
								 updatedusd=updatedusd/usd1;
								 
								 HashMap<String,String> countrycodesformap=new HashMap<String,String>();
								 countrycodesformap.put("VN","VND");
								 countrycodesformap.put("MY","MYR");
								 countrycodesformap.put("AU","AUD");
								 countrycodesformap.put("SG","SGD");
								 countrycodesformap.put("PH","PHP");
								 countrycodesformap.put("ID","IDR");
								 countrycodesformap.put("TH","THB");
								 countrycodesformap.put("EMA","USD");
								 countrycodesformap.put("INT","USD");
								 countrycodesformap.put("OT","USD");
								 countrycodesformap.put("IN","INR");
								 countrycodesformap.put("BG","BGR");
								 countrycodesformap.put("HK","HKD");
								 

								
								 
								 
								 List<Double> exchrate3=migratesevice.getExchangeRate(bookingyearcompare,bookingmonthcompare,"USD",(String)countrycodesformap.get(countrycodeforold));
								 
								 Double usd4=0.0;
								 for(Double usd3:exchrate3)
								 {
									 usd4=usd3;
								 }
								 
								 migratesevice.updateCurrenceyto(salesorder, (String) hm.get("currency"),updatedusd,(updatedusd/usd4),migartiontime);

								 
								}
							
							
							
							
							}
							}
						 
							
							
							 
							
						}
		


						
						
						
						
						 						
						/*************************************End of Logic for Change and Cancel**************************************************************/
				ytdb.setRegioncode(regioncode);
						ytdb.setRegionname(regionaname);					
						ytdb.setCustomercode(customercode);						
						custname1=(String)hm.get("name1");
						custname2=(String)hm.get("name2");
						
						if(custname2.equalsIgnoreCase("NA"))
							ytdb.setCustormer(custname1);
						else
							ytdb.setCustormer(custname1+" "+custname2);
						
						upr=(String)hm.get("upr");
						plineCode=(String)hm.get("pline");
						
						if(plineCode.equalsIgnoreCase("NA"))
						{
							ytdb.setProduct("NA");
							ytdb.setProductcategory("NA");
							ytdb.setProductcategorycode("NA");
							ytdb.setProductcode("NA");							
							ytdb.setApc("NA");							
						}
						else
						{
							List products=migratesevice.getProductDetails(plineCode);
							if(products!=null && products.size()>0)
							{
								hm1=(HashMap)products.get(0);
								ytdb.setProduct((String)hm1.get("product"));
								ytdb.setProductcategory((String)hm1.get("productcategory"));
								ytdb.setProductcategorycode((String)hm1.get("pccode"));
								ytdb.setProductcode((String)hm1.get("productcode"));
								
								if(!upr.equalsIgnoreCase("U"))
									ytdb.setApc("PS");
								else
									ytdb.setApc((String)hm1.get("productcategory"));								
							}	
						}
						
						ytdb.setU_P_R(upr);
						
						appcode=(String)hm.get("applicationcode");
						ytdb.setAppcode(appcode);
						if(!appcode.equalsIgnoreCase("NA"))
						{
							ytdb.setIndustrysectorcode(appcode.substring(0, 1));
							industrysector=migratesevice.getIndustrySector("industrysector",appcode.substring(0, 1),"industrysectorcode","industrysector");
							ytdb.setIndustrysector(industrysector);
							ytdb.setIndustrycode(appcode.substring(0, 2));
							industry=migratesevice.getIndustrySector("industry",appcode.substring(0, 2),"industrycode","industry");
							ytdb.setIndustry(industry);
						}
						else
						{
							ytdb.setIndustry("NA");
							ytdb.setIndustrycode("NA");
							ytdb.setIndustrysector("NA");
							ytdb.setIndustrysectorcode("NA");					
						}
											
						/*if(!slcode2.equalsIgnoreCase("NA"))
						{
							slcode=slcode1+","+slcode2;
							slname=slname1+","+slname2;
						}
						if(!slcode3.equalsIgnoreCase("NA"))
						{
							if(slcode.equals(""))
							{
								slcode=slcode1+","+slcode3;
								slname=slname1+","+slname3;
							}
							else
							{
								slcode=slcode+","+slcode3;
								slname=slname+","+slname3;
							}
						}
						if(!slcode4.equalsIgnoreCase("NA"))
						{
							if(slcode.equals(""))
							{
								slcode=slcode1+","+slcode4;
								slname=slname1+","+slname4;
							}
							else
							{
								slcode=slcode+","+slcode4;
								slname=slname+","+slname4;
							}
						}
						
						if(slcode2.equalsIgnoreCase("NA") && slcode3.equalsIgnoreCase("NA") && slcode4.equalsIgnoreCase("NA"))
						{
							ytdb.setSalesmancode(slcode1);
							ytdb.setSalesmanname(slname1);
						}
						else
						{
							ytdb.setSalesmancode(slcode);
							ytdb.setSalesmanname(slname);
						}
						*/
						
						ytdb.setSalesmancode(slcode1);
						ytdb.setSalesmanname(slname1);
						
						
						country=migratesevice.getIndustrySector("country",countrycode,"countrycode","country");
						tocode=migratesevice.getIndustrySector("currencycode",country,"country","country");
						
						if(salesmans[j].equalsIgnoreCase("A004") || salesmans[j].equalsIgnoreCase("I001") || salesmans[j].equalsIgnoreCase("I002") || salesmans[j].equalsIgnoreCase("I003") || salesmans[j].substring(0, 1) .equalsIgnoreCase("J") || salesmans[j].substring(0, 1) .equalsIgnoreCase("H"))
						{
						
							List otcustcode=null;
							if(Integer.parseInt(year)>=2016 && Integer.parseInt(month)>3)
							{
								otcustcode=migratesevice.getCustCodeAndCurrencyForNew(cust_code[0]);
							}
							else
								otcustcode=migratesevice.getCustCodeAndCurrency(cust_code[0]);
							
							if(otcustcode.size()>0 && otcustcode!=null)
							{
							hm5=(HashMap)otcustcode.get(0);
							countrycode=(String)hm5.get("countrycode");
							country=(String)hm5.get("country");
							tocode=(String)hm5.get("currencycode");
							}
						}
						//by siva for new feed
						if(customercode.equalsIgnoreCase("SMIN"))
						{
							
							countrycode="EMA";
							country="EMARKET";
							tocode="USD";	
							
						}

						ytdb.setCountrycode(countrycode);
						
						if(!country.equalsIgnoreCase("NA"))
							ytdb.setCountry(country);
						else
							ytdb.setCountry("NA");
						
						currency=(String)hm.get("currency");
						BookingValue = orgamount * (percent[j] / 100);
						//exchangerate1 - for local 2, 3 and 4 used for calculation of 1
						//exchangerate5 - for USD
						List<Double> exchrate=migratesevice.getExchangeRate(Integer.parseInt(year),Integer.parseInt(month),currency,tocode);
						
						if(exchrate.size()<1 || exchrate.get(0)==null)
						{
							List<Double> exchrate1=migratesevice.getExchangeRate(Integer.parseInt(year),Integer.parseInt(month),"USD",currency);
							if(exchrate1.size()<1 || exchrate1.get(0)==null)
							exchangerate1=1.0;
							else
							{
								 exchangerate3=(Double)exchrate1.get(0);
								 List<Double> exchrate2=migratesevice.getExchangeRate(Integer.parseInt(year),Integer.parseInt(month),"USD",tocode);
								 if(exchrate2.size()<1 || exchrate2.get(0)==null)
									 exchangerate1=1.0;
								 else
								 {
									 exchangerate4=(Double)exchrate2.get(0);
									 exchangerate1= exchangerate3/exchangerate4;
								 }
							 }
						 }
						 else
						 {
							 exchangerate1=(Double)exchrate.get(0);
							 exchangerate1 = 1/exchangerate1;
						 }
						
						 ytdb.setOriginalamount(Double.parseDouble(new DecimalFormat("0.0000").format(BookingValue)));
						 
						 ytdb.setAmount(Double.parseDouble(new DecimalFormat("0.0000").format(BookingValue*exchangerate1)));
						 List<Double> exchrate3=migratesevice.getExchangeRate(Integer.parseInt(year),Integer.parseInt(month),"USD",currency);
						 if(exchrate3.size()<1 || exchrate3.get(0)==null)
							 exchangerate5=0.0;
						 else
							 exchangerate5=(Double)exchrate3.get(0);
						 
						 ytdb.setAmountusd(Double.parseDouble(new DecimalFormat("0.0000").format(BookingValue* exchangerate5)));
						 ytdb.setExchrate(exchangerate1);
						 ytdb.setCurrency(currency);
						 
						 // to currency
						 
						 if(comprisionyear>=2016)
						 {
						 ytd.add(ytdb);
						 }
						 
						 
						 if(comprisionyear < 2016 && !changestatus.equalsIgnoreCase("CANCEL"))
						 {
							 ytd.add(ytdb); 
						 }
						 
						 success=migratesevice.saveMigrateRegionData(ytd);
					 }
					 else 
						 continue;
				 }
				count--;
			}
			
			logger.info("-------------------------DATA MIGRATED SUCCESSFULLY --------------------------");
			
			 
			  this.testregion();
			  this.testproduct();
			  this.updatebooking();
			  this.saveApplication();
			  this.saveIndustry();
			  this.updatebooking();
			  this.testindustry();
			  
			 
			
			//this.migrateYtdBookingIndustry(changeflag);
			//this.migrateYtdBookingProduct(changeflag);
			//this.migrateYtdBookingRegion(changeflag);
			//this.migrateProductionSchedularData(); 
			 
			 
			
			/* Session session2=(Session)hibernateDao.getSessionFactory().openSession();
			 String ytdindustry="truncate table YtdBookingIndustry ";
			 String ytdregion="truncate table  YtdBookingRegion";
			 String ytdproduct="truncate table  YtdBookingProduct";
			 SQLQuery industryt	= session2.createSQLQuery(ytdindustry);
			 SQLQuery regiont	= session2.createSQLQuery(ytdregion);
			 SQLQuery product	= session2.createSQLQuery(ytdproduct);
			 
			 industryt.executeUpdate();
			 regiont.executeUpdate();
			 product.executeUpdate();
		*/
			
			/*migrateYtdBookingProduct();
			migrateYtdBookingIndustry();
			migrateYtdBookingRegion();*/
			/*
			long cal2=Calendar.getInstance().getTimeInMillis();
			long diff = cal2 - cal1;
			
			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
			
			logger.info("DIFFERENCE IN SECS---"+diffSeconds);
			logger.info("DIFFERENCE IN MINUTES---"+diffMinutes);
			logger.info("DIFFERENCE IN HOURS---"+diffHours);
			logger.info("DIFFERENCE IN DAYS---"+diffDays);
			 */
	}
	catch(Exception e)
	{
			logger.info(e.getCause()+"-------------------------EXCEPTION HAS OCCURED DUE TO THIS TYPE-------------"+countrycode+"-------------", e);
	}		
	//return success;
	}
	
	
public  void testindustry() {
	
	Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	String sql="Truncate table ytdbookingIndustry";
	 SQLQuery q =session1.createSQLQuery(sql);
	 q.executeUpdate();
	 
	 Session session2=(Session)hibernateDao.getSessionFactory().openSession();
	 String sql2="INSERT INTO [dbo].[YtdBookingIndustry]([amountusd],[bookingamount],[country],[industry],[industrycode],[industrysector],[industrysectorcode],[month],[year],[migrationtime],[applicationcode],[application]) select [amountusd],[amount],[country],[industry],[industrycode],[industrysector],[industrysectorcode],[bookingmonth],[bookingyear],[migrationtime],[applicationcode],[application] from ytdbooking";
	
	
	 SQLQuery q2 =session1.createSQLQuery(sql2);
	 q2.executeUpdate();
	 
	 
	 String sql3="update YtdBookingIndustry set industrysectorcode='NA' where industrysector='NA'";
	 SQLQuery q3 =session1.createSQLQuery(sql3);
	 q3.executeUpdate();
	 
	 
	 
	 System.out.println("updtaeddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd industry");
		// TODO Auto-generated m
		// TODO Auto-generated method stub
		
	}


public  void testproduct() {
	
	Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	String sql="Truncate table ytdbookingproduct";
	 SQLQuery q =session1.createSQLQuery(sql);
	 q.executeUpdate();
	 
	 Session session2=(Session)hibernateDao.getSessionFactory().openSession();
	 String sql2=" INSERT INTO [dbo].[YtdBookingProduct]([amountusd] ,[bookingamount],[country],[month],[product],[productcategory],[year],[apc],[migationtime]) select [amountusd] ,[amount],[country],[bookingmonth],[product],[productcategory],[bookingyear],[apc],[migrationtime] from YtdBooking";
	
	
	 SQLQuery q2 =session1.createSQLQuery(sql2);
	 q2.executeUpdate();
	 String sql3="update ytdbookingproduct set apc='NA',product='NA',productcategory='NA' where apc is null and product is null and productcategory is null";
	 SQLQuery q3 =session1.createSQLQuery(sql3);
	 q3.executeUpdate();
	 System.out.println("updtaeddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd product");
		// TODO Auto-generated method stub
		
		// TODO Auto-generated method stub
		
	}


private void testregion() {
	Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	String sql="Truncate table ytdbookingregion";
	 SQLQuery q =session1.createSQLQuery(sql);
	 q.executeUpdate();
	 
	 Session session2=(Session)hibernateDao.getSessionFactory().openSession();
	 String sql2="INSERT INTO [dbo].[YtdBookingRegion](amountusd,bookingamount,country,month,region,salesman,year,migrationtime,salesmancode,regstatus)  select amountusd ,amount,country,bookingmonth,regioncode,salesmanname,bookingyear,migrationtime,salesmancode,regstatus from ytdbooking";
	
	
	 SQLQuery q2 =session1.createSQLQuery(sql2);
	 q2.executeUpdate();
	 
	 
	 System.out.println("updtaeddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd region");
		// TODO Auto-generated method stub
		
	}




public void saveIndustry()
{
	Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	String industryquery="SELECT  industrycode  FROM [ytdbooking] where industrycode not in ('NoAppcode','No Industry Code')   EXCEPT SELECT industrycode FROM [industry]";
	SQLQuery results=session1.createSQLQuery(industryquery);
	List<String> industrycodes=results.list();
	session1.beginTransaction();
	for(String c:industrycodes)
	{
	Industry v1=new 	Industry();
	 v1.setTotalIndustryname("unspecified");
	 v1.setIndustrycode(c);
	 session1.save(v1);
}

session1.getTransaction().commit();

session1.close();
	
	
}




public void saveApplication()
{
	Session session1=(Session)hibernateDao.getSessionFactory().openSession();
	String applicationquery="SELECT  applicationcode  FROM [ytdbooking] where applicationcode not in ('NoAppcode','No Application Code')  EXCEPT SELECT    applicationcode FROM [Application]";
	SQLQuery results=session1.createSQLQuery(applicationquery);
	List<String> applicationcodes=results.list();
	session1.beginTransaction();
	for(String c:applicationcodes)
	{
	Application v1=new 	Application();
	 v1.setApplication("unspecified");
	 v1.setApplicationcode(c);
	 session1.save(v1);
}

session1.getTransaction().commit();

session1.close();
	
	
}




public void  updatebooking()
{
	
	Session session2=hibernateDao.getSessionFactory().openSession();
	
	//for Application------------------------------------>
	
	String  applicationquery="from Application";
	Query applicationquery1 =session2.createQuery(applicationquery);
	List<Application> applicationdata=applicationquery1.list();	
	TreeMap<String,String> applicationcodeandapplicationname= new TreeMap<String,String>();
	for(Application a:applicationdata)
	{
		applicationcodeandapplicationname.put(a.getApplicationcode(),a.getApplication());
		
	}
	
	
	//for Industry------------------------------------------------>
	String IndustryQuery="from Industry";
	Query industry1=session2.createQuery(IndustryQuery);
	List<Industry> industry2=industry1.list();
	TreeMap<String,String> industrycodeindustryname=new TreeMap<String,String>();
	for(Industry i1:industry2)
	{
		industrycodeindustryname.put(i1.getIndustrycode(),i1.getTotalIndustryname());
		
	}
	
	
	
	//for Industry Sector-------------------------------------------------->
	
	String Industrysector="from IndustrySector";
	Query industrysectorquery=session2.createQuery(Industrysector);
	List<IndustrySector> industrysector1=industrysectorquery.list();
	TreeMap<String,String> industrysectornameandindustrysectorcode=new TreeMap<String,String>();
	
	
	for(IndustrySector is1:industrysector1)
	{
		industrysectornameandindustrysectorcode.put(is1.getIndustrysectorcode(),is1.getIndustrysector());
	}
	
	
	
	
	
	
	
	String appcodequery="select distinct appcode from YtdBooking";
	Query appcodequerydata =session2.createQuery(appcodequery);
	
	List<String> onlyappcodequerydata=appcodequerydata.list();
	
	for(String a:onlyappcodequerydata)
	{
		
		if(a.equalsIgnoreCase("NA"))
		{
			String  industrycode="NoAppcode";
			String industryname="NoAppcode";
			String appcode="NoAppcode";
			String applicationname ="NoAppcode";
			String industrysector="NoAppcode";
			String industrysectorcode="NoAppcode";
			
			String updatingindustryapplicationinytdbooking="update YtdBooking  set applicationcode='"+appcode+"',application='"+applicationname+"',industry='"+industryname+"',industrycode='"+industrycode+"',industrysectorcode='"+industrysectorcode+"',industrysector ='"+industrysector+"' where  appcode='"+a+"'";
			Query queryupdatingindustryapplicationinytdbooking =session2.createQuery(updatingindustryapplicationinytdbooking);
			queryupdatingindustryapplicationinytdbooking.executeUpdate();
			
		}
		
		else
		{
			
	 
		String appcode="No Application Code";
		if(a.length()>=3)
		{
			appcode=a.substring(a.length() - 1);
		}
		String industrycode="No Industry Code";
		if(a.length()>=2)
		{
	   industrycode=a.substring (0,2);
	    }
	  String industrysectorcode=a.substring (0,1);
	  
		 
		 
			String applicationname1=(String)applicationcodeandapplicationname.get(appcode);
			String applicationname =null;
            String industryname1=(String)industrycodeindustryname.get(industrycode);
            String industryname=null;
            String industrysectorname1=(String)industrysectornameandindustrysectorcode.get(industrysectorcode);
            String industrysectorname=null;
            if(applicationname1==null)
            {
            	applicationname="No Application Name";
            }
            
            else
            {
             applicationname = applicationname1.replace("'","''");
            	
            }
            
            if(industryname1==null)
            {
            	industryname="NO Industry Name";
            }
            else
            {
            	industryname = industryname1.replace("'","''");
            	
            }
            
            if(industrysectorname1==null)
            {
            	industrysectorname="No Industry Sector Name";
            }
            else
            {
            	industrysectorname = industrysectorname1.replace("'","''");
            }
          
		
			 String updatingindustryapplicationinytdbooking="update YtdBooking  set applicationcode='"+appcode+"',application='"+applicationname+"',industry='"+industryname+"',industrycode='"+industrycode+"',industrysectorcode='"+industrysectorcode+"',industrysector ='"+industrysectorname+"' where  appcode='"+a+"'";
             Query queryupdatingindustryapplicationinytdbooking =session2.createQuery(updatingindustryapplicationinytdbooking);
			  queryupdatingindustryapplicationinytdbooking.executeUpdate();
			
			
	 
		
		
	}
	
	

	}
	 
	
	

	
}






	public  void  migrateYtdBookingRegion(int changeflag) 
	{
		
		changeflag=0;
		List<Integer> bookingYears=migratesevice.getYtdBookingYear();
		
		 
		YtdBookingRegion ytdregion=null; 
		Integer year=null;
		Integer month=null;
		String country=null;
		String region=null;
		String salesman=null;
		String salesmancode=null;
		List bookingAmounts=null;
		Map hm=null;
		Boolean success=false;
		try
		{
			for(Integer years:bookingYears)
			{
				year=years;
				List<Integer> bookingMonths=migratesevice.getYtdBookingMonth(year,migartiontime,changeflag);
				
				for(Integer months:bookingMonths)
				{
					month=months;
						
					List<String> bookingCountrys=migratesevice.getYtdBookingCountry(year,months,migartiontime,changeflag);
					for(String countrys:bookingCountrys)
					{							
						country=countrys;
						List<String> bookingRegions=migratesevice.getYtdBookingRegion(year,month,country,migartiontime,changeflag);
						for(String regions:bookingRegions)
						{
							region=regions;				
							List<Object[]> bookingSalesmans=migratesevice.getYtdBookingSalesman(year,month,country,region,migartiontime,changeflag);
							
							if(bookingSalesmans.size()>0)
							{
								for(Object[] salesmans:bookingSalesmans)
								{											
									if((String)salesmans[1]==null)
										continue;
									salesman=(String)salesmans[0];
									salesmancode=(String)salesmans[1];
									bookingAmounts=migratesevice.getYtdBookingAmount(year,month,country,region,salesmancode,migartiontime,changeflag);
									
									List<YtdBookingRegion> dataofytdbookingregion=migratesevice.getDatafromytdBookingRegion(year,month,country,region,salesmancode);
									
									if(dataofytdbookingregion.size()==0)
									{
									if(bookingAmounts.size()>0 && bookingAmounts != null)
									{
									    hm=(HashMap)bookingAmounts.get(0);
									    ArrayList al=new ArrayList();
										ytdregion=new YtdBookingRegion();
										ytdregion.setMigrationtime(migartiontime);
										ytdregion.setSalesman(salesman);
										ytdregion.setSalesmancode(salesmancode);
										ytdregion.setBookingamount((Double)hm.get("amount"));
										ytdregion.setAmountusd((Double)hm.get("amountusd"));
										ytdregion.setRegion(region);
										ytdregion.setCountry(country);
										ytdregion.setMonth(month);
										ytdregion.setYear(year);
										al.add(ytdregion);
										success=migratesevice.saveMigrateRegionData(al);
									}
									
									}
									
									else
									{
										for(YtdBookingRegion dattar:dataofytdbookingregion)
										{
											Session session1=(Session)hibernateDao.getSessionFactory().openSession();
											 hm=(HashMap)bookingAmounts.get(0);
											 Double bookingamount1=(Double)hm.get("amount");
											 Double amountusd1=(Double)hm.get("amountusd");
											 String regionname1=dattar.getRegion();
											 String country1=dattar.getCountry();
											 int month1=dattar.getMonth();
											 int year1=dattar.getYear();
											 String Salesman1=dattar.getSalesman();
											 String salesmancode1=dattar.getSalesmancode();
											 int slno1=dattar.getSlno();
											 YtdBookingRegion k=new YtdBookingRegion();
											 
											 k.setSlno(slno1);
											 k.setAmountusd(amountusd1);
											 k.setBookingamount(bookingamount1);
											 k.setCountry(country1);
											 k.setRegion(regionname1);
											 k.setSalesmancode(salesmancode1);
											 k.setSalesman(Salesman1);
											 k.setMonth(month1);
											 k.setYear(year1);
											 
											 session1.beginTransaction();
											 session1.saveOrUpdate(k);
											session1.getTransaction().commit();
											session1.close();
										
											 
											//String query="update YtdBookingRegion set bookingamount="+bookingamount+", amountusd="+amountusd+" where 
											
											
											
										}
										
										
										
										
									}
								}
							}			
						}	
					}		
				}	
			}
			 
			logger.info("------------------Records Updated--");
		}
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
		}
	}
	

	public  void  migrateYtdBookingProduct(int changeflag) 
	{		
		changeflag=0;
		List<Integer> bookingYears=migratesevice.getYtdBookingYear();
		//ArrayList al=new ArrayList();
		YtdBookingProduct ytdproduct=null; 
		List bookingAmounts=null;
		Boolean success=false;
		Map hm=null;
		Map tempMap=null;
		String apc=null;
		String productcategory=null;
		
		try
		{
			for(Integer year:bookingYears)
			{
				logger.info("<==============booking year is::==========================================================>"+year);
				
				List<Integer> bookingMonths=migratesevice.getYtdBookingMonth(year,migartiontime,changeflag);
				for(Integer month:bookingMonths)
				{					
						List<String> bookingCountrys=migratesevice.getYtdBookingCountry(year,month,migartiontime,changeflag);
						for(String country:bookingCountrys)
						{
							List bookingproductcategory=migratesevice.getYtdBookingProductCategory(year,month,country,migartiontime,changeflag);
							
							if(bookingproductcategory != null && bookingproductcategory.size()>0)
							{
								for (int i = 0; i < bookingproductcategory.size(); i++)
								{
									tempMap = new HashMap();
									tempMap = (Map) bookingproductcategory.get(i);
									productcategory=(String)tempMap.get("productcategory");
									apc=(String)tempMap.get("apc");
									List<String> bookingproduct=migratesevice.getYtdBookingProduct(year,month,country,productcategory,apc,migartiontime,changeflag);
									if(bookingproduct.size()>0)
									{
										for(String product:bookingproduct)
										{													
											if(product==null)
												continue;													
											
											bookingAmounts=migratesevice.getYtdBookingAmountForProduct(year,month,country,productcategory,product,apc,migartiontime,changeflag);
											List<YtdBookingProduct> dataofytdbookingproduct=migratesevice.getDatafromytdBookingRegion(year,month,country,productcategory,product,apc);
											if(dataofytdbookingproduct.size()==0)
											{
											
											if(bookingAmounts.size()>0 && bookingAmounts != null)
											{
												hm=(HashMap)bookingAmounts.get(0);	
												ArrayList al=new ArrayList();
												ytdproduct=new YtdBookingProduct();	
												ytdproduct.setMigationtime(migartiontime);
												ytdproduct.setBookingamount((Double)hm.get("amount"));
												ytdproduct.setAmountusd((Double)hm.get("amountusd"));
												ytdproduct.setProduct(product);
												ytdproduct.setProductcategory(productcategory);
												ytdproduct.setCountry(country);
												ytdproduct.setMonth(month);
												ytdproduct.setYear(year);
												ytdproduct.setApc(apc);
												al.add(ytdproduct);
												success=migratesevice.saveMigrateRegionData(al);
											}
											
											}
											
											else
											{
												
												
												for(YtdBookingProduct product1:dataofytdbookingproduct)
												{
													Session session5=(Session)hibernateDao.getSessionFactory().openSession();
													hm=(HashMap)bookingAmounts.get(0);
													YtdBookingProduct p=new YtdBookingProduct();
													p.setSlno(product1.getSlno());
													p.setApc(product1.getApc());
													p.setAmountusd((Double)hm.get("amountusd"));
													p.setBookingamount((Double)hm.get("amount"));
													p.setCountry(product1.getCountry());
													p.setMonth(product1.getMonth());
													p.setYear(product1.getYear());
													p.setProduct(product1.getProduct());
													p.setProductcategory(product1.getProductcategory());
													 session5.beginTransaction();
													 session5.saveOrUpdate(p);
													session5.getTransaction().commit();
													session5.close();
													
												
													
												}
												
												
												
												
												
												
											}
											}
										}
									}
								}
							}
						
						}
					}
				
				 
		}
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
		}
	}
	
	 
	public  void  migrateYtdBookingIndustry(int changeflag) 
	{
		System.out.println("hello bss <-------------------------------------------->");
		changeflag=0;
		List<Integer> bookingYears=migratesevice.getYtdBookingYear();
		//ArrayList al=new ArrayList();
		YtdBookingIndustry ytdindustry=null; 
		String indsector=null;
		String indsectorcode=null;
		String industry1=null;
		String industrycode1=null;
		List bookingAmounts=null;
		Map hm=null;
		Boolean success=false;
		try
		{
			for(Integer year:bookingYears)
			{
				logger.info("<==============booking year is::==========================================================>"+year);
				List<Integer> bookingMonths=migratesevice.getYtdBookingMonth(year,migartiontime,changeflag);				
				for(Integer month:bookingMonths)
				{					
					List<String> bookingCountrys=migratesevice.getYtdBookingCountry(year,month,migartiontime,changeflag);
					for(String country:bookingCountrys)
					{
							List bookingindustrysector=migratesevice.getYtdBookingIndustrySector(year,month,country,migartiontime,changeflag);
								
							if(bookingindustrysector != null && bookingindustrysector.size()>0)
							{
								for (int i = 0; i < bookingindustrysector.size(); i++)
								{
									Map tempMap = new HashMap();
									tempMap = (Map) bookingindustrysector.get(i);
									indsector=(String)tempMap.get("industrysector");
										
									indsectorcode=(String)tempMap.get("industrysectorcode");
									
									List<Object[]> bookingindustry=migratesevice.getYtdBookingIndustry(year,month,country,indsector,indsectorcode,migartiontime,changeflag);
											
											
									if(bookingindustry.size()>0)
									{
										for(Object industry[]:bookingindustry)
										{
											industry1=(String)industry[0];
											if(industry1==null)
												continue;
											industrycode1=(String)industry[1];
											
											bookingAmounts=migratesevice.getYtdBookingAmountForIndustry(year,month,country,indsector,industry1,indsectorcode,industrycode1,migartiontime,changeflag);
											List<YtdBookingIndustry> dataofytdbookingindustry=migratesevice.getDatafromytdBookingIndustry(year,month,country,indsector,industry1,indsectorcode,industrycode1);
											
											if(dataofytdbookingindustry.size()==0)
											{
											if(bookingAmounts.size()>0 &&bookingAmounts != null)
											{
												hm=(HashMap)bookingAmounts.get(0);
												ArrayList al=new ArrayList();
												ytdindustry=new YtdBookingIndustry();
												ytdindustry.setMigrationtime(migartiontime);
												ytdindustry.setBookingamount((Double)hm.get("amount"));
												ytdindustry.setAmountusd((Double)hm.get("amountusd"));
												ytdindustry.setIndustry(industry1);
												ytdindustry.setIndustrysector(indsector);
												ytdindustry.setIndustrysectorcode(indsectorcode);
												ytdindustry.setIndustrycode(industrycode1);															
												ytdindustry.setCountry(country);
												ytdindustry.setMonth(month);
												ytdindustry.setYear(year);
												al.add(ytdindustry);
												success=migratesevice.saveMigrateRegionData(al);
											}
											
											}
											
											else
											{
												for(YtdBookingIndustry indu:dataofytdbookingindustry)
												{
													hm=(HashMap)bookingAmounts.get(0);
													YtdBookingIndustry y=new YtdBookingIndustry();
													
													y.setSlno(indu.getSlno());
													y.setAmountusd((Double)hm.get("amountusd"));
													y.setBookingamount((Double)hm.get("amount"));
													y.setCountry(indu.getCountry());
													y.setIndustry(indu.getIndustry());
													y.setIndustrycode(indu.getIndustrycode());
													y.setIndustrysector(indu.getIndustrysector());
													y.setIndustrysectorcode(indu.getIndustrysectorcode());
													y.setYear(indu.getYear());
													y.setMonth(indu.getMonth());
													
													Session session2=(Session)hibernateDao.getSessionFactory().openSession();
													 session2.beginTransaction();
													 session2.saveOrUpdate(y);
													session2.getTransaction().commit();
													session2.close();
												}
												 
												
												
												
												
											}
										}
								}
							}
						}
					}
				}
			}
			 
		}
		catch(Exception e)
		{
			
		logger.info(e.getCause(), e);
		}		
	}

	
	
	
	public void migrateProductionSchedularData_From_14_Nov_2016()
	{	
		Map hm=null;
		Map psMap=null;
		ProductionScheduler ps=null;
		ProductionScheduler ps1=null;
		List entityList=null;
		try
		{			
			List tempdata=migratesevice.getAllTheBookingDetailsFromTempTableForProductionScheduler14nov(migartiontime);
			Session session = (Session)hibernateDao.getSessionFactory().openSession();
			Date sysdate = new Date();
		 
			List<ProductionScheduler> scheduledData=migratesevice.getAllTheScheduledFromProductionScheduler();
			boolean isPresent=false;
			for(int i=0;i<tempdata.size();i++)
			{
			
				logger.info("------- Production Scheduler  Record Number --------> "+(i+1));
				
				hm=(HashMap)tempdata.get(i);
				String newChange=(String)hm.get("newchange");
				
				//if("SG".equalsIgnoreCase((String)hm.get("shippingpoint")))
				//{
				String requireddt1=(String)hm.get("requireddeliv");
				String newReqDelDate1=requireddt1.substring(6,8)+"-"+requireddt1.substring(4,6)+"-"+requireddt1.substring(0,4);
				java.sql.Date d1=DateUtils.getSqlDateFromString(newReqDelDate1,Constants.GenericDateFormat.DATE_FORMAT);
				java.sql.Date d2 = new java.sql.Date(sysdate.getTime());
				Calendar calendar1 = Calendar.getInstance();
		        calendar1.setTime(d1);
		        Calendar calendar2 = Calendar.getInstance();
		        calendar2.setTime(d2);
		        String str = "";
		        long timeinmillis1 = calendar1.getTimeInMillis();
		        long timeinmillis2 = calendar2.getTimeInMillis();
		        if (timeinmillis1 < timeinmillis2)
		        {
		          continue;
		        } 
		        
				if("P".equalsIgnoreCase((String)hm.get("upr")))
				{					
					scheduledData=migratesevice.getAllTheScheduledFromProductionScheduler();
					for(int k=0;k<scheduledData.size();k++)
					{
						isPresent=false;
						ProductionScheduler productionSchedulers=(ProductionScheduler)scheduledData.get(k);
						if(productionSchedulers.getUpr().equalsIgnoreCase("P"))
						{
							String[] spSalesOrderOld=productionSchedulers.getSalesorder().split("-");
							String[] spSalesOrderNew=hm.get("salesorder").toString().split("-");
							
							if(spSalesOrderOld[0].equalsIgnoreCase(spSalesOrderNew[0]))
							{
								String requireddt=(String)hm.get("requireddeliv");
								String newReqDelDate=requireddt.substring(6,8)+"-"+requireddt.substring(4,6)+"-"+requireddt.substring(0,4);
								
								String one1=DateUtils.getSqlDateFromString(newReqDelDate,Constants.GenericDateFormat.DATE_FORMAT).toString();
								String two2=productionSchedulers.getReqdeliverydate().toString();
								if(one1.equals(two2))
								{
									isPresent=true;
									break;									
								}
								else
								{
									Session session1 = (Session)hibernateDao.getSessionFactory().openSession();
									productionSchedulers.setLineitemstatus("invalid");
									productionSchedulers.setId(productionSchedulers.getId());
									session1.saveOrUpdate(productionSchedulers);
									session1.flush();
									break;
								}
							}							
						}						
					}			
					if(isPresent)
						continue;
				}
				
				ps=new ProductionScheduler();					
				ps.setApplicationcode((String)hm.get("applicationcode"));					
				ps.setChangeitem((String)hm.get("newchange"));
				ps.setContractno((String)hm.get("contractno"));
				ps.setCustomercode((String)hm.get("customercode"));				
			
					ps.setModelno((String)hm.get("modelno"));
					
					ps.setDescription((String)hm.get("description"));				
					ps.setLineitemstatus("valid");
					ps.setName((String)hm.get("name1")+" "+(String)hm.get("name2"));
					ps.setPline((String)hm.get("pline"));
					ps.setPid((String)hm.get("pid"));
					ps.setQuantity((Integer)hm.get("qty"));
					String requireddeliv=(String)hm.get("requireddeliv");
					String newDate=requireddeliv.substring(6,8)+"-"+requireddeliv.substring(4,6)+"-"+requireddeliv.substring(0,4);
					ps.setReqdeliverydate(DateUtils.getSqlDateFromString(newDate,Constants.GenericDateFormat.DATE_FORMAT));
					ps.setSalesorder((String)hm.get("salesorder"));
					ps.setTestedby("NA");
					//ps.setTotalscore(totalscore)					
					ps.setTransdate(DateUtils.getCurrentDate(Constants.GenericDateFormat.DATE_FORMAT));
					ps.setUpr((String)hm.get("upr"));
					ps.setShippingpoint((String)hm.get("shippingpoint"));
					if("NA".equalsIgnoreCase(ps.getModelno()))
					{
						ps.setScore(0);
					}
					else
					{
						Double score=getScore(ps.getModelno());
						if(score==null)
						{
							ps.setScore(0);
						}
						else
							ps.setScore(score.intValue());
					}
					
					if("NEW".equalsIgnoreCase(newChange))
					{
						ps.setProdstatus("New");
						session.save(ps);
						session.flush();
					}
					else if("CANCEL".equalsIgnoreCase(newChange))
					{
						if("P".equalsIgnoreCase((String)hm.get("upr")))
						{
							ps.setProdstatus("To be cancelled");
							session.save(ps);
							session.flush();
						}
						else
						{
							scheduledData=migratesevice.getAllTheScheduledFromProductionScheduler();
							if(scheduledData.size()>0 && scheduledData!=null)
							{
								for(int k=0;k<scheduledData.size();k++)
								{
									ProductionScheduler productionSchedulerCan=(ProductionScheduler)scheduledData.get(k);
									
									if(productionSchedulerCan.getSalesorder().equalsIgnoreCase((String)hm.get("salesorder")))
									{
										ps.setAssemblydate(productionSchedulerCan.getAssemblydate());
										ps.setDuedate(productionSchedulerCan.getDuedate());
										break;
									}
								}
							}
							
							ps.setProdstatus("To be cancelled");
							session.save(ps);
							session.flush();
						}
					}
					else if("CHANGE".equalsIgnoreCase(newChange))
					{
						boolean occurance=false;
						if("P".equalsIgnoreCase((String)hm.get("upr")))
						{
							ps.setProdstatus("Modified");
							session.save(ps);
							session.flush();
						}
						else
						{							
							entityList=new ArrayList();
							scheduledData=migratesevice.getAllTheScheduledFromProductionScheduler();
							if(scheduledData.size()>0 && scheduledData!=null)
							{
								String requireddt2=(String)hm.get("requireddeliv");
								String newReqDelDate2=requireddt1.substring(6,8)+"-"+requireddt1.substring(4,6)+"-"+requireddt1.substring(0,4);

								for(int j=0;j<scheduledData.size();j++)
								{
									ProductionScheduler productionScheduler=(ProductionScheduler)scheduledData.get(j);
									
									if(productionScheduler.getSalesorder().equalsIgnoreCase((String)hm.get("salesorder")) && productionScheduler.getLineitemstatus().equalsIgnoreCase("valid"))
									{										
										if((Integer)hm.get("qty")!=0)
										{
											ps.setLineitemstatus("invalid");
											ps.setProdstatus("Modified");
											entityList.add(ps);
											ps.setChangeditem1("qty");											
											//save new Record withe old data..
											ps1=new ProductionScheduler();
											ps1.setApplicationcode((String)hm.get("applicationcode"));										
											ps1.setChangeitem((String)hm.get("newchange"));
											ps1.setContractno((String)hm.get("contractno"));
											ps1.setCustomercode((String)hm.get("customercode"));				
											ps1.setModelno("NA");
											ps1.setDescription((String)hm.get("description"));				
											ps1.setName((String)hm.get("name1")+" "+(String)hm.get("name2"));
											ps1.setPline((String)hm.get("pline"));
											//ps1.setReqdeliverydate(DateUtils.getSqlDateFromString(newDate,Constants.GenericDateFormat.DATE_FORMAT));
											ps1.setReqdeliverydate(productionScheduler.getReqdeliverydate());										
											ps1.setSalesorder((String)hm.get("salesorder"));
											ps1.setShippingpoint((String)hm.get("shippingpoint"));
											if((String)hm.get("pid")!=(String)productionScheduler.getPid())
											{
												ps1.setChangeditem2("pid");
											}
											if((String)hm.get("description")!=(String)productionScheduler.getDescription())
											{
												ps1.setChangeditem3("description");
											}									
											if(!DateUtils.getSqlDateFromString(newReqDelDate2,Constants.GenericDateFormat.DATE_FORMAT).toString().equals(productionScheduler.getReqdeliverydate().toString()))
											{
												ps1.setChangeditem4("reqdeliverydate");
											}
											ps1.setQuantity((Integer)hm.get("qty")+productionScheduler.getQuantity());
											ps1.setDescription((String)hm.get("description"));
											ps1.setPid((String)hm.get("pid"));
											ps1.setAssemblydate(productionScheduler.getAssemblydate());
											
											//Double score1=getScore(ps.getModelno());
											ps1.setScore(productionScheduler.getScore());
											ps1.setLineitemstatus("valid");
											ps1.setProdstatus("Modified");	
											ps1.setUpr((String)hm.get("upr"));
											ps1.setCompleteddate(productionScheduler.getCompleteddate());
											ps1.setShippingpoint((String)hm.get("shippingpoint"));
											ps1.setTotalscore(ps1.getQuantity()*productionScheduler.getScore());
											ps1.setModelno(productionScheduler.getModelno());
											ps1.setDuedate(productionScheduler.getDuedate());
											ps1.setTransdate(DateUtils.getCurrentDate(Constants.GenericDateFormat.DATE_FORMAT));
											ps1.setTestedby("NA");
											entityList.add(ps1);										
											//Update Old Record
											productionScheduler.setLineitemstatus("invalid");
											entityList.add(productionScheduler);
											
											masterScreenDao.saveMultipleEntities(entityList);	

											session.flush();
											
											occurance=true;
										}
										else
										{
											ps.setLineitemstatus("invalid");
											ps.setProdstatus("Modified");
											entityList.add(ps);
											
											//save new Record withe old data..
											ps1=new ProductionScheduler();
											
											ps1.setApplicationcode((String)hm.get("applicationcode"));										
											ps1.setChangeitem((String)hm.get("newchange"));
											ps1.setContractno((String)hm.get("contractno"));
											ps1.setCustomercode((String)hm.get("customercode"));				
											ps1.setModelno("NA");
											ps1.setDescription((String)hm.get("description"));				
											ps1.setName((String)hm.get("name1")+" "+(String)hm.get("name2"));
											ps1.setPline((String)hm.get("pline"));
											//ps1.setReqdeliverydate(DateUtils.getSqlDateFromString(newDate,Constants.GenericDateFormat.DATE_FORMAT));
											ps1.setReqdeliverydate(productionScheduler.getReqdeliverydate());
											
											ps1.setSalesorder((String)hm.get("salesorder"));
											ps1.setChangeditem1("qty");
											ps1.setShippingpoint((String)hm.get("shippingpoint"));
											if((String)hm.get("pid")!=(String)productionScheduler.getPid())
											{
												ps1.setChangeditem2("pid");							
											}
											if((String)hm.get("description")!=(String)productionScheduler.getDescription())
											{
												ps1.setChangeditem3("description");
											}		
											String one=DateUtils.getSqlDateFromString(newReqDelDate2,Constants.GenericDateFormat.DATE_FORMAT).toString();
											String two=productionScheduler.getReqdeliverydate().toString();
											if(!DateUtils.getSqlDateFromString(newReqDelDate2,Constants.GenericDateFormat.DATE_FORMAT).toString().equals(productionScheduler.getReqdeliverydate().toString()))
											{
												ps1.setChangeditem4("reqdeliverydate");
											}
											ps1.setQuantity((Integer)hm.get("qty")+productionScheduler.getQuantity());
											ps1.setDescription((String)hm.get("description"));
											ps1.setPid((String)hm.get("pid"));
											//ps1.setReqdeliverydate(DateUtils.getSqlDateFromString((String)hm.get("reqdeliverydate")));
											ps1.setAssemblydate(productionScheduler.getAssemblydate());
											//Double score2=getScore(ps.getModelno());
											ps1.setScore(productionScheduler.getScore());
											ps1.setProdstatus("Modified");
											ps1.setLineitemstatus("valid");
											ps1.setUpr((String)hm.get("upr"));
											ps1.setTotalscore(ps1.getQuantity()*productionScheduler.getScore());
											ps1.setCompleteddate(productionScheduler.getCompleteddate());
											ps1.setShippingpoint((String)hm.get("shippingpoint"));
											ps1.setModelno(productionScheduler.getModelno());
											ps1.setDuedate(productionScheduler.getDuedate());
											ps1.setTransdate(DateUtils.getCurrentDate(Constants.GenericDateFormat.DATE_FORMAT));
											ps1.setTestedby("NA");
											entityList.add(ps1);
											//Update Old Record
											productionScheduler.setLineitemstatus("invalid");
											entityList.add(productionScheduler);
											
											masterScreenDao.saveMultipleEntities(entityList);
										
											occurance=true;
										}
									}	
								}
							}
						}
	
						if(!occurance)
						{
							ps.setProdstatus("Modified");
							session.save(ps);
							session.flush();
						}
					}
					
				//}
					
					
			}			
			
		}catch(Exception e)
		{
			logger.info("The Exception Trace---"+e.getMessage());
		}		
		
	}	

	
	/*public void machinecode()
	{
		  Session session1=(Session)hibernateDao.getSessionFactory().openSession();
		  
		  String maincode=null;
		  String mainindustry=null;
		  String getsubcode=null;
		  String subindustry=null;
		  String industrycode=null;
		  String totalIndustryname=null;
		  
		  ArrayList<ApplicationIndustryCode> applicationIndustrymigration=new ArrayList<ApplicationIndustryCode>();
		  
		  String sql="From ApplicationMigrate ";
		Query f=  session1.createQuery(sql);
		
		List<ApplicationMigrate> a=f.list();
		for(ApplicationMigrate a1:a)
		{
			
			maincode=a1.getMaincode();
			mainindustry=a1.getMainindustry();
			getsubcode=a1.getSubcode();
			subindustry=a1.getSubindustry();
			
			  Session session2=(Session)hibernateDao.getSessionFactory().openSession();
			  String sql1="From IndustryMigrate";
				Query f1=  session2.createQuery(sql1);
				List<IndustryMigrate> idr=f1.list();
				for(IndustryMigrate idm:idr)
				{
					
					ApplicationIndustryCode app=new ApplicationIndustryCode();
					industrycode=idm.getIndustrycode();
					totalIndustryname=idm.getTotalIndustryname();
					app.setMachinecode(maincode+""+getsubcode+""+industrycode);
					app.setTotalname(mainindustry+"("+subindustry+","+totalIndustryname+")");
					applicationIndustrymigration.add(app);
					
					
				}
			
			
			
			System.out.println(a1+"------------------------------>");
		}
	
	
	
	 Session session3=(Session)hibernateDao.getSessionFactory().openSession();
	 
	 for(ApplicationIndustryCode l:applicationIndustrymigration)
	 {
		 ApplicationIndustryCode app=new ApplicationIndustryCode();
		 app.setMachinecode(l.getMachinecode());
		 app.setTotalname(l.getTotalname());
		 session3.save(app);
		 
		 
	 }
	 
	}
	
	*/
	
	
	public void regioncodeupdate()
	{
		
		 Session session1=(Session)hibernateDao.getSessionFactory().openSession();
		 String sql="Select distinct salesmancode from YtdBookingRegion";
		 SQLQuery q =session1.createSQLQuery(sql);
		  List<String> salesmacodes=q.list();
		
		  for(String k:salesmacodes)
		  {
			  String slamencode[]=k.split(",");
			  //getting region based on salesmancode from salesman table
			  String region="Select regioncode from  Salesman where salesmancode='"+slamencode[0]+"'";  
			  SQLQuery q2 =session1.createSQLQuery(region);
			  List<String> regioncodes=q2.list();
			  for(String r:regioncodes)
			  {
				 
				  String updateregioninytdbookingregion="update YtdBookingRegion set region='"+r+"' where salesmancode='"+k+"'";
				  SQLQuery updateregion  =session1.createSQLQuery(updateregioninytdbookingregion);
				updateregion.executeUpdate();
			  }
		  }
	}
	
	
	
	
	
	//@RequestMapping(value = "/YtdProfitMigration", method = RequestMethod.GET)
	public  void  YtdProfitMigration(){
		System.out.println("hello bss <-------------------------------------------->");
		logger.info("<==============helloio0oooooooooooooooo welcome()==========================================================>");
		boolean success=false;
		String invdate=null;
		String ivdate[]=null;
		String year=null;
		Integer month=null;
		String day=null;
		String monthString=null;
		String product=null;
		String productcode=null;
		String pccode=null;
		//InvoiceDate,Customer,ProductGroup,InvoiceQty,SaleValue,TotalCost,Profit,ProfitPercent,id
				YtdProfit ytdprofit=null;
		ArrayList al=new ArrayList();
	List<Object[]> tempdata=migratesevice.getAllTheYtdProfitDetailsFromTempTable();
	
	for(Object[] data:tempdata)
		
	{
		ytdprofit=new YtdProfit();
		
		System.out.println("Record number in ytdprofit----------------------------------"+data[8]);
	
		invdate=(String)data[0];
		
		ivdate=invdate.split("/");
		year=ivdate[2];
		month=Integer.parseInt(ivdate[0]);
		day=ivdate[1];
		ytdprofit.setYear(Integer.parseInt(year));
		ytdprofit.setMonthcode(month);
		ytdprofit.setDate(Integer.parseInt(day));
		ytdprofit.setSalesinvoicedate(DateUtils.getSqlDateFromString(day+"-"+month+"-"+year,Constants.GenericDateFormat.DATE_FORMAT));
		
        switch (month)
        {
        case 1:  monthString = "January";       break;
        case 2:  monthString = "February";      break;
        case 3:  monthString = "March";         break;
        case 4:  monthString = "April";         break;
        case 5:  monthString = "May";           break;
        case 6:  monthString = "June";          break;
        case 7:  monthString = "July";          break;
        case 8:  monthString = "August";        break;
        case 9:  monthString = "September";     break;
        case 10: monthString = "October";       break;
        case 11: monthString = "November";      break;
        case 12: monthString = "December";      break;
        }
        ytdprofit.setMonth(monthString);
        product=(String)data[2];
        ytdprofit.setProduct(product);
        
    	List<Object[]> products=migratesevice.getYtdProfitProductDetails(product);
		
		
		if(products.size()>0)
		{
		for(Object productcodes[]:products)
		{
		
		productcode=(String)productcodes[0];
		if(productcode!=null)
		{
		pccode=(String)productcodes[1];
	
		List<String> productcategory=migratesevice.getProductCategoryCode(pccode);
		ytdprofit.setProduct(product);
		ytdprofit.setProductcode(productcode);
		ytdprofit.setProductcategory(productcategory.get(0));
		ytdprofit.setPccode(pccode);
		String country=migratesevice.getCountryDetails((String)data[1]);
		String countrycode=migratesevice.getCountryCode(country);
		
		ytdprofit.setCountry(country);
		ytdprofit.setCountrycode(countrycode);
		}
		}
		}
		ytdprofit.setTotalcost((Double)data[5]);
		ytdprofit.setProfit((Double)data[6]);
		ytdprofit.setProfitpercent((Double)data[7]);
		ytdprofit.setQty((Double)data[3]);
		ytdprofit.setInvoicedamount((Double)data[4]);
		
		al.add(ytdprofit);	
		
		}
		success=migratesevice.saveMigrateRegionData(al);
		
		//return success;
		}
	
	
	
	public  void movingfolders(String drive) throws IOException 
    {
    	 
    		
    		Calendar now = Calendar.getInstance();
    		int year = now.get(Calendar.YEAR);
    		int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
    		int day = now.get(Calendar.DAY_OF_MONTH);
    		int hour = now.get(Calendar.HOUR_OF_DAY);
    		int minute = now.get(Calendar.MINUTE);
    		int second = now.get(Calendar.SECOND);
    		int millis = now.get(Calendar.MILLISECOND);
    		
    		
    		
    		ArrayList<String> foldername=new  ArrayList<String>();
    		foldername.add("SMSG");
    		foldername.add("SMAU");
    		foldername.add("SMID");
    		foldername.add("SMMA");
    		foldername.add("SMPH");
    		foldername.add("SMTH");
    		foldername.add("SMVN");
    		foldername.add("sca");
    		foldername.add("smau");
    		foldername.add("smid");
    		foldername.add("smma");
    		foldername.add("smph");
    		foldername.add("smth");
    		foldername.add("smvn");
    		
    		
    		//File sourceFile = new File("directory-source/test1.txt");
    		//File destinationDir = new File("directory-destination");

    		//FileUtils.moveFileToDirectory(sourceFile, destinationDir, true);

    		
    	int numberoffolders=foldername.size();
    	
    	//E:\Booking
    	
    	for(int i=0;i<numberoffolders;i++)
    	{
    		 File dir1 = new File(""+drive+"://crmscadata//Booking//"+foldername.get(i));
    		 
    		 File dir2 = new File(""+drive+"://crmscadata//Booking//YEARS//"+year+"//"+foldername.get(i));
    		// File dir2 = new File("E://Booking//YEARS//"+foldername.get(i));
    		 File[] files = dir1.listFiles();
    		 
    		 if(files!=null)
    		 {
    		 for(int j=0;j<files.length;j++)
    		 {
    			 String fimename=files[j].getName();
    			 File sourceFile = new File(files[j].getAbsolutePath());
    			 File destinationDir = new File(dir2.toString());
    			 File destinationDir2 = new File(destinationDir+"//"+files[j].getName());
    			 boolean condition=destinationDir2.exists();
    			if( destinationDir2.exists())
    			{
    				destinationDir2.delete();
    			}
    			 
    			org.apache.commons.io.FileUtils.moveFileToDirectory(sourceFile, destinationDir, true);
    			 
    		
    		 }
    		 
    		 }
    	}
    	
    	
    		
    	
    	
    	
    	     	
    	
    	  
    }
	
	
	
	public static void readXLSFileWithBlankCells() {
		try {
			HashMap a=new HashMap();
			File f=new File("E:\\feb2\\b.xls");
			InputStream ExcelFileToRead = new FileInputStream(f);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;
			HSSFCell cell;

			Iterator rows = sheet.rowIterator();
			rows.next();
int ki=1;
			while (rows.hasNext()) {
				row = (HSSFRow) rows.next();
				ArrayList al2=new ArrayList();
				//ki++;
				for(int i=0; i<row.getLastCellNum(); i++) {
					cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
					if(cell.toString().equals(""))
					{
						al2.add("0");
						
					}
					else
					{
						al2.add(cell.toString());
					}
					//System.out.print(cell.toString()+" ");
				}
				a.put(ki,al2);
				ki++;
				 
				 
			}
			
			System.out.println(a);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	
	public static void readXLSXFileWithBlankCell() {
		try {
			HashMap a=new HashMap();
			InputStream ExcelFileToRead = new FileInputStream("some file");
			/*HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

			HSSFSheet sheet = wb.getSheetAt(0);*/
			
			
			XSSFWorkbook workbook = new XSSFWorkbook (ExcelFileToRead);

			//Get first sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			XSSFRow row;
			XSSFCell cell;

			Iterator rows = sheet.rowIterator();
			rows.next();
int ki=1;
			while (rows.hasNext()) {
				row = (XSSFRow) rows.next();
				ArrayList al2=new ArrayList();
				//ki++;
				for(int i=0; i<row.getLastCellNum(); i++) {
					cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
					if(cell.toString().equals(""))
					{
						al2.add("0");
						
					}
					else
					{
						al2.add(cell.toString());
					}
					//System.out.print(cell.toString()+" ");
				}
				a.put(ki,al2);
				ki++;
				 
				 
			}
			
			System.out.println(a);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	 	 

	
	 	
}
