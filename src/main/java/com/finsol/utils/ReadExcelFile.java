package com.finsol.utils;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelFile {

	public static final String EXCEL_FILE = "E:\\feb2\\custmeron29.xls";

	public static void readXLSFile() {
		try {
			InputStream ExcelFileToRead = new FileInputStream(EXCEL_FILE);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;
			HSSFCell cell;

			Iterator rows = sheet.rowIterator();

			while (rows.hasNext()) {
				row = (HSSFRow) rows.next();
				Iterator cells = row.cellIterator();

				while (cells.hasNext()) {
					cell = (HSSFCell) cells.next();
					System.out.print(cell.toString()+" ");
				}
				System.out.println();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static void readXLSFileWithBlankCells() {
		try {
			HashMap a=new HashMap();
			InputStream ExcelFileToRead = new FileInputStream(EXCEL_FILE);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;
			HSSFCell cell;

			Iterator rows = sheet.rowIterator();
			rows.next();
int ki=1;
			while (rows.hasNext()) {
				row = (HSSFRow) rows.next();
				ArrayList al2=new ArrayList();
				//ki++;
				for(int i=0; i<row.getLastCellNum(); i++) {
					cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
					if(cell.toString().equals(""))
					{
						al2.add("0");
						
					}
					else
					{
						al2.add(cell.toString());
					}
					//System.out.print(cell.toString()+" ");
				}
				a.put(ki,al2);
				ki++;
				 
				 
			}
			
			System.out.println(a);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	
	public static void readXLSFileWithBlankCellsxlxa() {
		try {
			HashMap a=new HashMap();
			InputStream ExcelFileToRead = new FileInputStream(EXCEL_FILE);
			/*HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

			HSSFSheet sheet = wb.getSheetAt(0);*/
			
			
			XSSFWorkbook workbook = new XSSFWorkbook (ExcelFileToRead);

			//Get first sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			XSSFRow row;
			XSSFCell cell;

			Iterator rows = sheet.rowIterator();
			rows.next();
int ki=1;
			while (rows.hasNext()) {
				row = (XSSFRow) rows.next();
				ArrayList al2=new ArrayList();
				//ki++;
				for(int i=0; i<row.getLastCellNum(); i++) {
					cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
					if(cell.toString().equals(""))
					{
						al2.add("0");
						
					}
					else
					{
						al2.add(cell.toString());
					}
					//System.out.print(cell.toString()+" ");
				}
				a.put(ki,al2);
				ki++;
				 
				 
			}
			
			System.out.println(a);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public static void main(String[] args) {
		//readXLSFile();
		readXLSFileWithBlankCells();
		//readXLSFileWithBlankCellsxlxa();
	}

}