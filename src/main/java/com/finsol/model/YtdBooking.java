package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 *
 */
@Entity
public class YtdBooking {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private Integer bookingyear;
	@Column
	private Integer bookingday;
	@Column
	private Integer bookingmonth;
	@Column
	private Date bookingdate;
	@Column
	private String countrycode;
	@Column
	private String country;
	@Column
	private String salesorder;
	@Column
	private String regioncode;
	@Column
	private String regionname;
	@Column
	private String salesmancode;
	@Column
	private String salesmanname;
	@Column
	private String customercode;
	@Column
	private String custormer;
	@Column
	private String productcode;
	@Column
	private String product;
	@Column
	private String productcategorycode;
	@Column
	private String productcategory;
	@Column
	private String U_P_R;
	@Column
	private Double amount;
	@Column
	private String appcode;
	@Column
	private String industrysectorcode ;
	@Column
	private String industrysector;
	@Column
	private String industrycode ;
	@Column
	private String industry ;
	@Column
	private Double originalamount;	
	@Column
	private Double exchrate;
	@Column
	private String currency;
	@Column
	private Double amountusd;
	@Column
	private String apc;
	
	@Column
	private String migrationtime;
	
	@Column
    private Byte regstatus;
	
	
	public String getMigrationtime() {
		return migrationtime;
	}
	public Byte getRegstatus()
	{
		return regstatus;
	}
	public void setRegstatus(Byte regstatus)
	{
		this.regstatus = regstatus;
	}
	public void setMigrationtime(String migrationtime) {
		this.migrationtime = migrationtime;
	}
	public String getApc() {
		return apc;
	}
	public void setApc(String apc) {
		this.apc = apc;
	}
	public Double getAmountusd() {
		return amountusd;
	}
	public void setAmountusd(Double amountusd) {
		this.amountusd = amountusd;
	}
	public Double getOriginalamount() {
		return originalamount;
	}
	public void setOriginalamount(Double originalamount) {
		this.originalamount = originalamount;
	}
	
	public Double getExchrate() {
		return exchrate;
	}
	public void setExchrate(Double exchrate) {
		this.exchrate = exchrate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public Integer getBookingyear() {
		return bookingyear;
	}
	public void setBookingyear(Integer bookingyear) {
		this.bookingyear = bookingyear;
	}
	public Integer getBookingday() {
		return bookingday;
	}
	public void setBookingday(Integer bookingday) {
		this.bookingday = bookingday;
	}
	public Integer getBookingmonth() {
		return bookingmonth;
	}
	public void setBookingmonth(Integer bookingmonth) {
		this.bookingmonth = bookingmonth;
	}
	public Date getBookingdate() {
		return bookingdate;
	}
	public void setBookingdate(Date bookingdate) {
		this.bookingdate = bookingdate;
	}
	public String getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getSalesorder() {
		return salesorder;
	}
	public void setSalesorder(String salesorder) {
		this.salesorder = salesorder;
	}
	public String getRegioncode() {
		return regioncode;
	}
	public void setRegioncode(String regioncode) {
		this.regioncode = regioncode;
	}
	public String getRegionname() {
		return regionname;
	}
	public void setRegionname(String regionname) {
		this.regionname = regionname;
	}
	public String getSalesmancode() {
		return salesmancode;
	}
	public void setSalesmancode(String salesmancode) {
		this.salesmancode = salesmancode;
	}
	public String getSalesmanname() {
		return salesmanname;
	}
	public void setSalesmanname(String salesmanname) {
		this.salesmanname = salesmanname;
	}
	public String getCustomercode() {
		return customercode;
	}
	public void setCustomercode(String customercode) {
		this.customercode = customercode;
	}
	public String getCustormer() {
		return custormer;
	}
	public void setCustormer(String custormer) {
		this.custormer = custormer;
	}
	public String getProductcode() {
		return productcode;
	}
	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getProductcategorycode() {
		return productcategorycode;
	}
	public void setProductcategorycode(String productcategorycode) {
		this.productcategorycode = productcategorycode;
	}
	public String getProductcategory() {
		return productcategory;
	}
	public void setProductcategory(String productcategory) {
		this.productcategory = productcategory;
	}
	public String getU_P_R() {
		return U_P_R;
	}
	public void setU_P_R(String u_P_R) {
		U_P_R = u_P_R;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getAppcode() {
		return appcode;
	}
	public void setAppcode(String appcode) {
		this.appcode = appcode;
	}
	public String getIndustrysectorcode() {
		return industrysectorcode;
	}
	public void setIndustrysectorcode(String industrysectorcode) {
		this.industrysectorcode = industrysectorcode;
	}
	public String getIndustrysector() {
		return industrysector;
	}
	public void setIndustrysector(String industrysector) {
		this.industrysector = industrysector;
	}
	public String getIndustrycode() {
		return industrycode;
	}
	public void setIndustrycode(String industrycode) {
		this.industrycode = industrycode;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	
	@Column
	private String applicationcode ;
	@Column
	private String application;


	public String getApplicationcode()
	{
		return applicationcode;
	}
	public void setApplicationcode(String applicationcode)
	{
		this.applicationcode = applicationcode;
	}
	public String getApplication()
	{
		return application;
	}
	public void setApplication(String application)
	{
		this.application = application;
	}
	 
	

}
