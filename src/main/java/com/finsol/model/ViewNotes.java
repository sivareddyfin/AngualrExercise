package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class ViewNotes
{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	@Type(type="date")
	private Date notesdate;
	@Column
	private String viewnotes;	
	@Column
	private String salesman;
	@Column
	private String customer;
	@Column
	private String quotationno;
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}

	
	public Date getNotesdate()
	{
		return notesdate;
	}
	public void setNotesdate(Date notesdate)
	{
		this.notesdate = notesdate;
	}
	public String getViewnotes()
	{
		return viewnotes;
	}
	public void setViewnotes(String viewnotes)
	{
		this.viewnotes = viewnotes;
	}
	public String getSalesman()
	{
		return salesman;
	}
	public void setSalesman(String salesman)
	{
		this.salesman = salesman;
	}
	public String getCustomer()
	{
		return customer;
	}
	public void setCustomer(String customer)
	{
		this.customer = customer;
	}
	public String getQuotationno()
	{
		return quotationno;
	}
	public void setQuotationno(String quotationno)
	{
		this.quotationno = quotationno;
	}
	
	
	
}
