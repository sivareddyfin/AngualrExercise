package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class QAIssueDetails {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private Integer id;
	@Column
	private String casereferenceno; 	
	@Column
	private  String itemdescription;	
	@Column
	private  Integer qty;	
	@Column
	private  String partno;	
	@Column
	private  String orderno;
	@Column
	private  Boolean toclaim;	
	@Column
	private  Integer inpackageno;
	
	public Integer getSlno()
	{
		return slno;
	}
	public void setSlno(Integer slno)
	{
		this.slno = slno;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getCasereferenceno()
	{
		return casereferenceno;
	}
	public void setCasereferenceno(String casereferenceno)
	{
		this.casereferenceno = casereferenceno;
	}
	public String getItemdescription()
	{
		return itemdescription;
	}
	public void setItemdescription(String itemdescription)
	{
		this.itemdescription = itemdescription;
	}
	public Integer getQty()
	{
		return qty;
	}
	public void setQty(Integer qty)
	{
		this.qty = qty;
	}
	public String getPartno()
	{
		return partno;
	}
	public void setPartno(String partno)
	{
		this.partno = partno;
	}
	public Integer getInpackageno()
	{
		return inpackageno;
	}
	public void setInpackageno(Integer inpackageno)
	{
		this.inpackageno = inpackageno;
	}
	public String getOrderno()
	{
		return orderno;
	}
	public void setOrderno(String orderno)
	{
		this.orderno = orderno;
	}
	public Boolean getToclaim()
	{
		return toclaim;
	}
	public void setToclaim(Boolean toclaim)
	{
		this.toclaim = toclaim;
	}
	
	
	
}
