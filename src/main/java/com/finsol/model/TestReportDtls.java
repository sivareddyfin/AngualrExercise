package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama krishna
 */
@Entity
public class TestReportDtls {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private  String slno;
	@Column
	private  String motorslno;
	@Column
	private  Double	measuredgearheadwt;
	@Column
	private  Double	measuredgreasewt;
	@Column
	private  Double	measuredfinalwt;
	@Column
	private Byte frequency;
	@Column
	private Byte airpressuretest;	
	@Column
	private  Double measuredipvoltage; 	

	@Column
	private  Double measuredampere; 
	
	@Column
	private  Double measuredippower;
	
	@Column
	private  Double measuredoppower;
	
	@Column
	private  Double measuredtemperatureinitial;
	
	@Column
	private  Double measuredtemperaturefinal;
	
	@Column
	private  Double measuredsoundlevelcw;
	
	@Column
	private  Double measuredsoundlevelccw;
	
	@Column
	private Byte confirmbbssize;
	 
	@Column
	private Byte measuredthermistorcablepresent;
	@Column
	private Byte measuredspaceheatercablepresent;
	
	@Column
	private  Double overallvibration;
			
	@Column
	private Byte  concentricity;
	
	@Column
	private Double  measuredopspeed;
	
	@Column
	private  String customerpono;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSlno() {
		return slno;
	}

	public void setSlno(String slno) {
		this.slno = slno;
	}

	public String getMotorslno() {
		return motorslno;
	}

	public void setMotorslno(String motorslno) {
		this.motorslno = motorslno;
	}



	public Double getMeasuredgearheadwt() {
		return measuredgearheadwt;
	}

	public void setMeasuredgearheadwt(Double measuredgearheadwt) {
		this.measuredgearheadwt = measuredgearheadwt;
	}

	public Double getMeasuredgreasewt() {
		return measuredgreasewt;
	}

	public void setMeasuredgreasewt(Double measuredgreasewt) {
		this.measuredgreasewt = measuredgreasewt;
	}

	public Double getMeasuredfinalwt() {
		return measuredfinalwt;
	}

	public void setMeasuredfinalwt(Double measuredfinalwt) {
		this.measuredfinalwt = measuredfinalwt;
	}

	public Byte getFrequency() {
		return frequency;
	}

	public void setFrequency(Byte frequency) {
		this.frequency = frequency;
	}

	public Byte getAirpressuretest() {
		return airpressuretest;
	}

	public void setAirpressuretest(Byte airpressuretest) {
		this.airpressuretest = airpressuretest;
	}

	public Double getMeasuredipvoltage() {
		return measuredipvoltage;
	}

	public void setMeasuredipvoltage(Double measuredipvoltage) {
		this.measuredipvoltage = measuredipvoltage;
	}

	public Double getMeasuredampere() {
		return measuredampere;
	}

	public void setMeasuredampere(Double measuredampere) {
		this.measuredampere = measuredampere;
	}

	public Double getMeasuredippower() {
		return measuredippower;
	}

	public void setMeasuredippower(Double measuredippower) {
		this.measuredippower = measuredippower;
	}

	public Double getMeasuredoppower() {
		return measuredoppower;
	}

	public void setMeasuredoppower(Double measuredoppower) {
		this.measuredoppower = measuredoppower;
	}

	public Double getMeasuredtemperatureinitial() {
		return measuredtemperatureinitial;
	}

	public void setMeasuredtemperatureinitial(Double measuredtemperatureinitial) {
		this.measuredtemperatureinitial = measuredtemperatureinitial;
	}

	public Double getMeasuredtemperaturefinal() {
		return measuredtemperaturefinal;
	}

	public void setMeasuredtemperaturefinal(Double measuredtemperaturefinal) {
		this.measuredtemperaturefinal = measuredtemperaturefinal;
	}

	public Double getMeasuredsoundlevelcw() {
		return measuredsoundlevelcw;
	}

	public void setMeasuredsoundlevelcw(Double measuredsoundlevelcw) {
		this.measuredsoundlevelcw = measuredsoundlevelcw;
	}

	public Double getMeasuredsoundlevelccw() {
		return measuredsoundlevelccw;
	}

	public void setMeasuredsoundlevelccw(Double measuredsoundlevelccw) {
		this.measuredsoundlevelccw = measuredsoundlevelccw;
	}

	public Byte getConfirmbbssize() {
		return confirmbbssize;
	}

	public void setConfirmbbssize(Byte confirmbbssize) {
		this.confirmbbssize = confirmbbssize;
	}

	public Byte getMeasuredthermistorcablepresent() {
		return measuredthermistorcablepresent;
	}

	public void setMeasuredthermistorcablepresent(
			Byte measuredthermistorcablepresent) {
		this.measuredthermistorcablepresent = measuredthermistorcablepresent;
	}

	public Byte getMeasuredspaceheatercablepresent() {
		return measuredspaceheatercablepresent;
	}

	public void setMeasuredspaceheatercablepresent(
			Byte measuredspaceheatercablepresent) {
		this.measuredspaceheatercablepresent = measuredspaceheatercablepresent;
	}



	public Double getOverallvibration() {
		return overallvibration;
	}

	public void setOverallvibration(Double overallvibration) {
		this.overallvibration = overallvibration;
	}

	public Byte getConcentricity() {
		return concentricity;
	}

	public void setConcentricity(Byte concentricity) {
		this.concentricity = concentricity;
	}


	public String getCustomerpono() {
		return customerpono;
	}

	public void setCustomerpono(String customerpono) {
		this.customerpono = customerpono;
	}

	public Double getMeasuredopspeed() {
		return measuredopspeed;
	}

	public void setMeasuredopspeed(Double measuredopspeed) {
		this.measuredopspeed = measuredopspeed;
	}
	
	

	
	
	
	
}
