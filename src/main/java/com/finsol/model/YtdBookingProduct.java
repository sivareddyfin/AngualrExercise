package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 *
 */
@Entity
public class YtdBookingProduct {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private String product;
	@Column
	private String country;
	@Column 
	private String productcategory;
	@Column
	private Double  bookingamount;
	@Column
	private Double  amountusd;
	@Column
	private int year;
	@Column
	private int month;
	@Column
	private String apc;
	
	@Column
	private String migationtime;
	
	
	public String getMigationtime() {
		return migationtime;
	}
	public void setMigationtime(String migationtime) {
		this.migationtime = migationtime;
	}
	public String getApc() {
		return apc;
	}
	public void setApc(String apc) {
		this.apc = apc;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProductcategory() {
		return productcategory;
	}
	public void setProductcategory(String productcategory) {
		this.productcategory = productcategory;
	}
	public Double getBookingamount() {
		return bookingamount;
	}
	public void setBookingamount(Double bookingamount) {
		this.bookingamount = bookingamount;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Double getAmountusd() {
		return amountusd;
	}
	public void setAmountusd(Double amountusd) {
		this.amountusd = amountusd;
	}
	

}
