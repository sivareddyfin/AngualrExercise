package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 *
 */
@Entity
public class YtdBookingIndustry {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private String industry;
	@Column
	private String industrycode;
	@Column
	private String country;
	@Column 
	private String industrysectorcode;
	@Column 
	private String industrysector;
	@Column
	private Double  bookingamount;
	@Column
	private Double  amountusd;
	@Column
	private int year;
	@Column
	private int month;
	
	
	@Column
	private String  migrationtime;
	
	
	public String getMigrationtime() {
		return migrationtime;
	}
	public void setMigrationtime(String migrationtime) {
		this.migrationtime = migrationtime;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getIndustrysector() {
		return industrysector;
	}
	public void setIndustrysector(String industrysector) {
		this.industrysector = industrysector;
	}
	public Double getBookingamount() {
		return bookingamount;
	}
	public void setBookingamount(Double bookingamount) {
		this.bookingamount = bookingamount;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public Double getAmountusd() {
		return amountusd;
	}
	public void setAmountusd(Double amountusd) {
		this.amountusd = amountusd;
	}
	public String getIndustrycode() {
		return industrycode;
	}
	public void setIndustrycode(String industrycode) {
		this.industrycode = industrycode;
	}
	public String getIndustrysectorcode() {
		return industrysectorcode;
	}
	public void setIndustrysectorcode(String industrysectorcode) {
		this.industrysectorcode = industrysectorcode;
	}
	
	@Column
	private String applicationcode ;
	@Column
	private String application;


	public String getApplicationcode()
	{
		return applicationcode;
	}
	public void setApplicationcode(String applicationcode)
	{
		this.applicationcode = applicationcode;
	}
	public String getApplication()
	{
		return application;
	}
	public void setApplication(String application)
	{
		this.application = application;
	}
	

	
}
