package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 *
 */
@Entity
public class YTDSales {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private String invoiceno;
	@Column
	private Integer invoiceyear;
	@Column
	private Integer invoicedday;
	@Column
	private Integer invoicemonth;
	@Column
	private Date invoicedate;
	@Column
	private Double invamountlc;
	@Column
	private String currency;
	@Column
	private Double invamountusd;
	@Column
	private Date reqdeliverydate;
	@Column
	private Integer reqdeliveryyear;
	@Column
	private Integer reqdeliverymonth;
	@Column
	private Integer reqdeliveryday;
	@Column
	private Double invqty;
	@Column
	private String vendorname;
	@Column
	private String productline;
	@Column
	private String submodel;
	@Column
	private Date dateofjobclose;
	@Column
	private Integer yearofjobclose;
	@Column
	private Integer monthofjobclose;
	@Column
	private Integer dayofjobclose;
	@Column
	private Date dateofentry;
	@Column
	private Integer yearofdateofentry;
	@Column
	private Integer monthofdateofentry;
	@Column
	private Integer dayofdateofentry;
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getInvoiceno() {
		return invoiceno;
	}
	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}
	public Integer getInvoiceyear() {
		return invoiceyear;
	}
	public void setInvoiceyear(Integer invoiceyear) {
		this.invoiceyear = invoiceyear;
	}
	public Integer getInvoicedday() {
		return invoicedday;
	}
	public void setInvoicedday(Integer invoicedday) {
		this.invoicedday = invoicedday;
	}
	public Integer getInvoicemonth() {
		return invoicemonth;
	}
	public void setInvoicemonth(Integer invoicemonth) {
		this.invoicemonth = invoicemonth;
	}
	public Date getInvoicedate() {
		return invoicedate;
	}
	public void setInvoicedate(Date invoicedate) {
		this.invoicedate = invoicedate;
	}
	public Double getInvamountlc() {
		return invamountlc;
	}
	public void setInvamountlc(Double invamountlc) {
		this.invamountlc = invamountlc;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Double getInvamountusd() {
		return invamountusd;
	}
	public void setInvamountusd(Double invamountusd) {
		this.invamountusd = invamountusd;
	}
	public Date getReqdeliverydate() {
		return reqdeliverydate;
	}
	public void setReqdeliverydate(Date reqdeliverydate) {
		this.reqdeliverydate = reqdeliverydate;
	}
	public Integer getReqdeliveryyear() {
		return reqdeliveryyear;
	}
	public void setReqdeliveryyear(Integer reqdeliveryyear) {
		this.reqdeliveryyear = reqdeliveryyear;
	}
	public Integer getReqdeliverymonth() {
		return reqdeliverymonth;
	}
	public void setReqdeliverymonth(Integer reqdeliverymonth) {
		this.reqdeliverymonth = reqdeliverymonth;
	}
	public Integer getReqdeliveryday() {
		return reqdeliveryday;
	}
	public void setReqdeliveryday(Integer reqdeliveryday) {
		this.reqdeliveryday = reqdeliveryday;
	}
	public Double getInvqty() {
		return invqty;
	}
	public void setInvqty(Double invqty) {
		this.invqty = invqty;
	}
	public String getVendorname() {
		return vendorname;
	}
	public void setVendorname(String vendorname) {
		this.vendorname = vendorname;
	}
	public String getProductline() {
		return productline;
	}
	public void setProductline(String productline) {
		this.productline = productline;
	}
	public String getSubmodel() {
		return submodel;
	}
	public void setSubmodel(String submodel) {
		this.submodel = submodel;
	}
	public Date getDateofjobclose() {
		return dateofjobclose;
	}
	public void setDateofjobclose(Date dateofjobclose) {
		this.dateofjobclose = dateofjobclose;
	}
	public Integer getYearofjobclose() {
		return yearofjobclose;
	}
	public void setYearofjobclose(Integer yearofjobclose) {
		this.yearofjobclose = yearofjobclose;
	}
	public Integer getMonthofjobclose() {
		return monthofjobclose;
	}
	public void setMonthofjobclose(Integer monthofjobclose) {
		this.monthofjobclose = monthofjobclose;
	}
	public Integer getDayofjobclose() {
		return dayofjobclose;
	}
	public void setDayofjobclose(Integer dayofjobclose) {
		this.dayofjobclose = dayofjobclose;
	}
	public Date getDateofentry() {
		return dateofentry;
	}
	public void setDateofentry(Date dateofentry) {
		this.dateofentry = dateofentry;
	}
	public Integer getYearofdateofentry() {
		return yearofdateofentry;
	}
	public void setYearofdateofentry(Integer yearofdateofentry) {
		this.yearofdateofentry = yearofdateofentry;
	}
	public Integer getMonthofdateofentry() {
		return monthofdateofentry;
	}
	public void setMonthofdateofentry(Integer monthofdateofentry) {
		this.monthofdateofentry = monthofdateofentry;
	}
	public Integer getDayofdateofentry() {
		return dayofdateofentry;
	}
	public void setDayofdateofentry(Integer dayofdateofentry) {
		this.dayofdateofentry = dayofdateofentry;
	}
	
	
	
	
	
}
