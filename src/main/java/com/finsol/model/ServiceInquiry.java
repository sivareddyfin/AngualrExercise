package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class ServiceInquiry {

	@Id
	private String casereferenceno;   	
	@Column
	private  String customer;	
	@Column
	private  String customercode;	
	@Column
	private  String customerstatus;
	@Column
	private  String contactno;	
	@Column
	private  String telephoneno;	
	@Column
	private  String address;	
	@Column
	private  String description;
	@Column
	private  Byte servicelocation;	
	@Column
	private  String siteaddress;	
	@Column
	private  String personcontactno;	
	@Column
	private Byte attchdrawing;
	@Column	
	private String  photolocation;
	@Column	
	private String remarks;
	
	@Column(columnDefinition="TEXT")
	private String imageData;
	@Column
	@Type(type="date")
	private Date inquiryDate;
	
	
	public String getCasereferenceno()
	{
		return casereferenceno;
	}
	public void setCasereferenceno(String casereferenceno)
	{
		this.casereferenceno = casereferenceno;
	}
	public String getCustomer()
	{
		return customer;
	}
	public void setCustomer(String customer)
	{
		this.customer = customer;
	}
	public String getCustomercode()
	{
		return customercode;
	}
	public void setCustomercode(String customercode)
	{
		this.customercode = customercode;
	}
	public String getCustomerstatus()
	{
		return customerstatus;
	}
	public void setCustomerstatus(String customerstatus)
	{
		this.customerstatus = customerstatus;
	}
	public String getContactno()
	{
		return contactno;
	}
	public void setContactno(String contactno)
	{
		this.contactno = contactno;
	}
	public String getTelephoneno()
	{
		return telephoneno;
	}
	public void setTelephoneno(String telephoneno)
	{
		this.telephoneno = telephoneno;
	}
	public String getAddress()
	{
		return address;
	}
	public void setAddress(String address)
	{
		this.address = address;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Byte getServicelocation()
	{
		return servicelocation;
	}
	public void setServicelocation(Byte servicelocation)
	{
		this.servicelocation = servicelocation;
	}
	public String getSiteaddress()
	{
		return siteaddress;
	}
	public void setSiteaddress(String siteaddress)
	{
		this.siteaddress = siteaddress;
	}
	public String getPersoncontactno()
	{
		return personcontactno;
	}
	public void setPersoncontactno(String personcontactno)
	{
		this.personcontactno = personcontactno;
	}
	public Byte getAttchdrawing()
	{
		return attchdrawing;
	}
	public void setAttchdrawing(Byte attchdrawing)
	{
		this.attchdrawing = attchdrawing;
	}
	public String getPhotolocation()
	{
		return photolocation;
	}
	public void setPhotolocation(String photolocation)
	{
		this.photolocation = photolocation;
	}
	public String getRemarks()
	{
		return remarks;
	}
	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}
	public String getImageData()
	{
		return imageData;
	}
	public void setImageData(String imageData)
	{
		this.imageData = imageData;
	}
	public Date getInquiryDate()
	{
		return inquiryDate;
	}
	public void setInquiryDate(Date inquiryDate)
	{
		this.inquiryDate = inquiryDate;
	}
		
	
}
