package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author naidu
 *
 */
@Entity
public class BSS_Details_booking_order {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String territory;
	@Column
	private String salesmancode;
	@Column
	private String salesman;
	@Column
	private String customercode;
	@Column
	private String customer;
	@Column
	private String startdate;
	@Column
	private String product;
	@Column
	private String entrydate;
	@Column
	private	String salesorder;
	@Column
	private	String u_p_r;
	@Column
	private String amount_usd;
	@Column
	private String app_code;
	@Column
	private Integer year;
	@Column
	private Byte status;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTerritory() {
		return territory;
	}
	public void setTerritory(String territory) {
		this.territory = territory;
	}
	public String getSalesmancode() {
		return salesmancode;
	}
	public void setSalesmancode(String salesmancode) {
		this.salesmancode = salesmancode;
	}
	public String getSalesman() {
		return salesman;
	}
	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}
	public String getCustomercode() {
		return customercode;
	}
	public void setCustomercode(String customercode) {
		this.customercode = customercode;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getEntrydate() {
		return entrydate;
	}
	public void setEntrydate(String entrydate) {
		this.entrydate = entrydate;
	}
	public String getSalesorder() {
		return salesorder;
	}
	public void setSalesorder(String salesorder) {
		this.salesorder = salesorder;
	}
	public String getU_p_r() {
		return u_p_r;
	}
	public void setU_p_r(String u_p_r) {
		this.u_p_r = u_p_r;
	}
	public String getAmount_usd() {
		return amount_usd;
	}
	public void setAmount_usd(String amount_usd) {
		this.amount_usd = amount_usd;
	}
	public String getApp_code() {
		return app_code;
	}
	public void setApp_code(String app_code) {
		this.app_code = app_code;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}

	
	
	
}
