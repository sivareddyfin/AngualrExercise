package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class QuotationGrid 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column (nullable = false)
	private Integer qgid;
	@Column
	private String  model;
	@Column
	private String  productDesp;
	@Column
	private String  motorPower;
	@Column
	private String  inputRPM;
	@Column
	private String  ratio;
	@Column
	private String  outPutRPM;
	public Integer getQgid() {
		return qgid;
	}
	public void setQgid(Integer qgid) {
		this.qgid = qgid;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getProductDesp() {
		return productDesp;
	}
	public void setProductDesp(String productDesp) {
		this.productDesp = productDesp;
	}
	public String getMotorPower() {
		return motorPower;
	}
	public void setMotorPower(String motorPower) {
		this.motorPower = motorPower;
	}
	public String getInputRPM() {
		return inputRPM;
	}
	public void setInputRPM(String inputRPM) {
		this.inputRPM = inputRPM;
	}
	public String getRatio() {
		return ratio;
	}
	public void setRatio(String ratio) {
		this.ratio = ratio;
	}
	public String getOutPutRPM() {
		return outPutRPM;
	}
	public void setOutPutRPM(String outPutRPM) {
		this.outPutRPM = outPutRPM;
	}
	public String getAccessories() {
		return accessories;
	}
	public void setAccessories(String accessories) {
		this.accessories = accessories;
	}
	public String getQnty() {
		return qnty;
	}
	public void setQnty(String qnty) {
		this.qnty = qnty;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartDesp() {
		return partDesp;
	}
	public void setPartDesp(String partDesp) {
		this.partDesp = partDesp;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getQid() {
		return qid;
	}
	public void setQid(Integer qid) {
		this.qid = qid;
	}
	@Column
	private String  accessories;
	@Column
	private String  qnty;
	@Column
	private String  deliveryTime;
	@Column
	private String  unitPrice;
	@Column
	private String  subTotal;
	@Column
	private String  partNumber;
	@Column
	private String  partDesp;
	@Column
	private String  serialNumber;
	@Column
	private Integer qid;
	
	
	
	@Column
	private String sfactor;
	@Column
	private String costPrice;
	@Column
	private String motorDetails;
	@Column
	private String specialFeaturs;
	@Column
	private String perSN;
	@Column
	private String quantity;
	@Column
	private String totalPrice;
	public String getSfactor() {
		return sfactor;
	}
	public void setSfactor(String sfactor) {
		this.sfactor = sfactor;
	}
	public String getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}
	public String getMotorDetails() {
		return motorDetails;
	}
	public void setMotorDetails(String motorDetails) {
		this.motorDetails = motorDetails;
	}
	public String getSpecialFeaturs() {
		return specialFeaturs;
	}
	public void setSpecialFeaturs(String specialFeaturs) {
		this.specialFeaturs = specialFeaturs;
	}
	public String getPerSN() {
		return perSN;
	}
	public void setPerSN(String perSN) {
		this.perSN = perSN;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Column
	private String region;
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	
	 
		public String getPole()
	{
		return pole;
	}
	public void setPole(String pole)
	{
		this.pole = pole;
	}
	public String getPhase()
	{
		return phase;
	}
	public void setPhase(String phase)
	{
		this.phase = phase;
	}
	public String getVoltage()
	{
		return voltage;
	}
	public void setVoltage(String voltage)
	{
		this.voltage = voltage;
	}
	public String getFreq()
	{
		return freq;
	}
	public void setFreq(String freq)
	{
		this.freq = freq;
	}
		@Column
		private String pole;
		
		@Column
		private String phase;
	 
		
		public String getAppcode()
		{
			return appcode;
		}
		public void setAppcode(String appcode)
		{
			this.appcode = appcode;
		}
		@Column
		private String voltage;
	 
		@Column
		private String freq;
		
		 
	 @Column
	 private String appcode;
	 
	 @Column
	 private String equipNo;
	public String getEquipNo()
	{
		return equipNo;
	}
	public void setEquipNo(String equipNo)
	{
		this.equipNo = equipNo;
	}
			
		 
			
		 
	

}
