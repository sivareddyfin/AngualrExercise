package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class  CaseSummaryTemp 
{

	
	public int getTab() {
		return tab;
	}

	public void setTab(int tab) {
		this.tab = tab;
	}

	 

	public String getStoredtime() {
		return storedtime;
	}

	public void setStoredtime(String storedtime) {
		this.storedtime = storedtime;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}



	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer newselectionid;
 
	public Integer getNewselectionid() {
		return newselectionid;
	}

	public void setNewselectionid(Integer newselectionid) {
		this.newselectionid = newselectionid;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCaseRef() {
		return caseRef;
	}

	public void setCaseRef(String caseRef) {
		this.caseRef = caseRef;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCustomerRef() {
		return customerRef;
	}

	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}

	public Date getDtCase() {
		return dtCase;
	}

	public void setDtCase(Date dtCase) {
		this.dtCase = dtCase;
	}

	public Date getDateExp() {
		return dateExp;
	}

	public void setDateExp(Date dateExp) {
		this.dateExp = dateExp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDtReport() {
		return dtReport;
	}

	public void setDtReport(Date dtReport) {
		this.dtReport = dtReport;
	}

	public Date getDtReportBy() {
		return dtReportBy;
	}

	public void setDtReportBy(Date dtReportBy) {
		this.dtReportBy = dtReportBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemarksAdd() {
		return remarksAdd;
	}

	public void setRemarksAdd(String remarksAdd) {
		this.remarksAdd = remarksAdd;
	}

	 
	 
	@Column
	private String customer;
	@Column
	private String customerCode;
	@Column
	private String caseRef;
	@Column
	private String contactPerson;
	@Column
	private String phone;
	@Column
	private String address;
	@Column
	private String customerRef;
	@Column
	private Date dtCase;
	@Column
	private Date dateExp;
	@Column
	private String description;
	@Column
	private Date dtReport;
	@Column
	private Date dtReportBy;
	@Column
	private String remarks;
	@Column
	private String remarksAdd;
	@Column
	private int tab;
	@Column
	private String storedtime;
	@Column
	private String username;
	
	
	
	@Column
	private String  assigned;
	@Column
	private String  custStatus;

	
	public String getAssigned() {
		return assigned;
	}

	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}

	public String getCustStatus() {
		return custStatus;
	}

	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}



	private String casetype;
	
	@Column(columnDefinition="TEXT")
	private String images;
	
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getCasetype() {
		return casetype;
	}

	public void setCasetype(String casetype) {
		this.casetype = casetype;
	}



	@Column(nullable = false)
	private String newcaseToken;
	
	
	public String getNewcaseToken() {
		return newcaseToken;
	}

	public void setNewcaseToken(String newcaseToken) {
		this.newcaseToken = newcaseToken;
	}

	@Column
	private String casePriority;
	
	@Column
	private int  orginReq;

	public String getCasePriority()
	{
		return casePriority;
	}

	public void setCasePriority(String casePriority)
	{
		this.casePriority = casePriority;
	}

	public int getOrginReq()
	{
		return orginReq;
	}

	public void setOrginReq(int orginReq)
	{
		this.orginReq = orginReq;
	}
	
	
	
	
	
 
}
