package com.finsol.model;

import java.sql.Time;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Rama krishna
 */
@Entity
public class TestReport {
	
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Id
	@Column
	private  String customerpono;	
	@Column
	private Time startdatetime;

	@Column
	private Time enddatetime;	
	@Column
	@Type(type="date")
	private Date daterequired;
	@Column
	@Type(type="date")
	private Date datereceived;	
	@Column
	private  String model;
	@Column
	private  String couplingtype;
	@Column
	private  String accessories;
	@Column
	private  Double inputshaft;
	@Column
	private  Double outputshaft;
	@Column
	private  String motorbrand;
	@Column
	private  String outputrpm;
	@Column
	private  String motortype;
	@Column
	private  String terminalbox;
	@Column
	private  String paintColor;
	@Column
	private Byte thermister;
	@Column
	private Byte spaceheater;
	@Column
	private Byte frequency;
	@Column
	private  String protection;
	@Column
	private  String voltage;
	@Column
	private  Double brakevoltage;
	@Column
	private  String changebrakekit;
	@Column
	private  String lubrication;	
	@Column
	private Byte motordirection;	
	@Column
	private  String assembledby;	
	@Column
	private  String testperformedby;	
	@Column
	@Type(type="date")
	private Date assembleddate;	
	@Column
	private  Byte phasevoltage;
	
	/*@Column(length = 10000)
	private byte[] bbsimage;*/
	
	@Column
	private  String customername;
	
	@Column
	private  String cableentry;
	 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCustomerpono() {
		return customerpono;
	}
	public void setCustomerpono(String customerpono) {
		this.customerpono = customerpono;
	}

	
	public Time getStartdatetime()
	{
		return startdatetime;
	}
	public void setStartdatetime(Time startdatetime)
	{
		this.startdatetime = startdatetime;
	}
	public Time getEnddatetime()
	{
		return enddatetime;
	}
	public void setEnddatetime(Time enddatetime)
	{
		this.enddatetime = enddatetime;
	}
	public Date getDaterequired() {
		return daterequired;
	}
	public void setDaterequired(Date daterequired) {
		this.daterequired = daterequired;
	}
	public Date getDatereceived() {
		return datereceived;
	}
	public void setDatereceived(Date datereceived) {
		this.datereceived = datereceived;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getCouplingtype() {
		return couplingtype;
	}
	public void setCouplingtype(String couplingtype) {
		this.couplingtype = couplingtype;
	}
	public String getAccessories() {
		return accessories;
	}
	public void setAccessories(String accessories) {
		this.accessories = accessories;
	}
	public Double getInputshaft() {
		return inputshaft;
	}
	public void setInputshaft(Double inputshaft) {
		this.inputshaft = inputshaft;
	}
	public Double getOutputshaft() {
		return outputshaft;
	}
	public void setOutputshaft(Double outputshaft) {
		this.outputshaft = outputshaft;
	}
	public String getMotorbrand() {
		return motorbrand;
	}
	public void setMotorbrand(String motorbrand) {
		this.motorbrand = motorbrand;
	}
	public String getOutputrpm() {
		return outputrpm;
	}
	public void setOutputrpm(String outputrpm) {
		this.outputrpm = outputrpm;
	}
	public String getMotortype() {
		return motortype;
	}
	public void setMotortype(String motortype) {
		this.motortype = motortype;
	}
	public String getTerminalbox() {
		return terminalbox;
	}
	public void setTerminalbox(String terminalbox) {
		this.terminalbox = terminalbox;
	}
	public String getPaintColor() {
		return paintColor;
	}
	public void setPaintColor(String paintColor) {
		this.paintColor = paintColor;
	}
	public Byte getThermister() {
		return thermister;
	}
	public void setThermister(Byte thermister) {
		this.thermister = thermister;
	}
	public Byte getSpaceheater() {
		return spaceheater;
	}
	public void setSpaceheater(Byte spaceheater) {
		this.spaceheater = spaceheater;
	}
	public Byte getFrequency() {
		return frequency;
	}
	public void setFrequency(Byte frequency) {
		this.frequency = frequency;
	}
	public String getProtection() {
		return protection;
	}
	public void setProtection(String protection) {
		this.protection = protection;
	}
	public String getVoltage() {
		return voltage;
	}
	public void setVoltage(String voltage) {
		this.voltage = voltage;
	}
	public Double getBrakevoltage() {
		return brakevoltage;
	}
	public void setBrakevoltage(Double brakevoltage) {
		this.brakevoltage = brakevoltage;
	}
	public String getChangebrakekit() {
		return changebrakekit;
	}
	public void setChangebrakekit(String changebrakekit) {
		this.changebrakekit = changebrakekit;
	}
	public String getLubrication() {
		return lubrication;
	}
	public void setLubrication(String lubrication) {
		this.lubrication = lubrication;
	}
	public Byte getMotordirection() {
		return motordirection;
	}
	public void setMotordirection(Byte motordirection) {
		this.motordirection = motordirection;
	}
	public String getAssembledby() {
		return assembledby;
	}
	public void setAssembledby(String assembledby) {
		this.assembledby = assembledby;
	}
	public String getTestperformedby() {
		return testperformedby;
	}
	public void setTestperformedby(String testperformedby) {
		this.testperformedby = testperformedby;
	}
	public Date getAssembleddate() {
		return assembleddate;
	}
	public void setAssembleddate(Date assembleddate) {
		this.assembleddate = assembleddate;
	}
/*	public byte[] getBbsimage() {
		return bbsimage;
	}
	public void setBbsimage(byte[] bbsimage) {
		this.bbsimage = bbsimage;
	}*/
	
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String toString() {
	    return "accessories: " + this.accessories + ", assembledby: " + this.assembledby + ", changebrakekit: " + this.changebrakekit + ", couplingtype: " + this.couplingtype + ", customerpono: " + this.customerpono + ", lubrication: " + this.lubrication + ", model: " + this.model + ", motorbrand: " + this.motorbrand + ", motortype: " + this.motortype + ", outputrpm: " + this.outputrpm + ", paintColor: " + this.paintColor + ", protection: " + this.protection + ", terminalbox: " + this.terminalbox+ ", testperformedby: " + this.testperformedby+ ", voltage: " + this.voltage+ ", testperformedby: " +  this.testperformedby+ ", id: " + this.id+ ", assembleddate: " + this.assembleddate+ ", startdatetime: " + this.startdatetime+ ", enddatetime: " + this.enddatetime+ ", datereceived: " + this.datereceived+ ", daterequired: " + this.daterequired;
	}
	public Byte getPhasevoltage()
	{
		return phasevoltage;
	}
	public void setPhasevoltage(Byte phasevoltage)
	{
		this.phasevoltage = phasevoltage;
	}
	public String getCableentry()
	{
		return cableentry;
	}
	public void setCableentry(String cableentry)
	{
		this.cableentry = cableentry;
	}

	
	
}
