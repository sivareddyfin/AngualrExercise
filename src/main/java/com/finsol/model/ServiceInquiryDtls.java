package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Rama Krishna
 *
 */
@Entity
public class ServiceInquiryDtls {

	@Column
	private String casereferenceno; 
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer slno;
	@Column
	private Integer id;	
	@Column
	private  String gearheadserialno;	
	@Column
	private  String motorserialno;	
	@Column
	private  String shimfgno;
	@Column
	private Byte serviceinquiryfor;
	@Column
	private Double quantity;
	@Column
	private  Byte isselectedmodel;	
	@Column
	private  String selectedmodel;	
	@Column
	private  Byte brake;	
	@Column
	private  Byte  ismodelunknown;	
	@Column
	private  String modelunknown;	
	@Column
	private  Byte brakevoltage;
	@Column
	private  String application;	
	@Column
	private Byte rotationdirection;		
	@Column
	private  String accessories;
	
	
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getCasereferenceno()
	{
		return casereferenceno;
	}
	public void setCasereferenceno(String casereferenceno)
	{
		this.casereferenceno = casereferenceno;
	}
	public String getGearheadserialno()
	{
		return gearheadserialno;
	}
	public void setGearheadserialno(String gearheadserialno)
	{
		this.gearheadserialno = gearheadserialno;
	}
	public String getMotorserialno()
	{
		return motorserialno;
	}
	public void setMotorserialno(String motorserialno)
	{
		this.motorserialno = motorserialno;
	}
	public String getShimfgno()
	{
		return shimfgno;
	}
	public void setShimfgno(String shimfgno)
	{
		this.shimfgno = shimfgno;
	}
	public Byte getServiceinquiryfor()
	{
		return serviceinquiryfor;
	}
	public void setServiceinquiryfor(Byte serviceinquiryfor)
	{
		this.serviceinquiryfor = serviceinquiryfor;
	}
	public Double getQuantity()
	{
		return quantity;
	}
	public void setQuantity(Double quantity)
	{
		this.quantity = quantity;
	}
	public Byte getIsselectedmodel()
	{
		return isselectedmodel;
	}
	public void setIsselectedmodel(Byte isselectedmodel)
	{
		this.isselectedmodel = isselectedmodel;
	}
	public String getSelectedmodel()
	{
		return selectedmodel;
	}
	public void setSelectedmodel(String selectedmodel)
	{
		this.selectedmodel = selectedmodel;
	}
	public Byte getBrake()
	{
		return brake;
	}
	public void setBrake(Byte brake)
	{
		this.brake = brake;
	}
	public Byte getIsmodelunknown()
	{
		return ismodelunknown;
	}
	public void setIsmodelunknown(Byte ismodelunknown)
	{
		this.ismodelunknown = ismodelunknown;
	}
	public String getModelunknown()
	{
		return modelunknown;
	}
	public void setModelunknown(String modelunknown)
	{
		this.modelunknown = modelunknown;
	}
	public Byte getBrakevoltage()
	{
		return brakevoltage;
	}
	public void setBrakevoltage(Byte brakevoltage)
	{
		this.brakevoltage = brakevoltage;
	}
	public String getApplication()
	{
		return application;
	}
	public void setApplication(String application)
	{
		this.application = application;
	}
	public Byte getRotationdirection()
	{
		return rotationdirection;
	}
	public void setRotationdirection(Byte rotationdirection)
	{
		this.rotationdirection = rotationdirection;
	}
	public String getAccessories()
	{
		return accessories;
	}
	public void setAccessories(String accessories)
	{
		this.accessories = accessories;
	}
	public Integer getSlno()
	{
		return slno;
	}
	public void setSlno(Integer slno)
	{
		this.slno = slno;
	}	
	
	
	
	
	
	
}
