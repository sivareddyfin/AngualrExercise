package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class DeliveryUpdateTemp {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column
	private String customername; 
	
	@Column
	private String quotationno;
	
	@Column
	@Type(type="date")
	private Date  quotationdate;
	
	@Column
	@Type(type="date")
	private Date  exporderdate;	

	@Column
	@Type(type="date")
	private Date  expdeldate;
	
	@Column
	@Type(type="date")
	private Date  projdeldate;
	
	@Column
	private String salesrprtrefno;
	
	@Column
	private String salesmancode;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public String getQuotationno() {
		return quotationno;
	}

	public void setQuotationno(String quotationno) {
		this.quotationno = quotationno;
	}

	public Date getQuotationdate() {
		return quotationdate;
	}

	public void setQuotationdate(Date quotationdate) {
		this.quotationdate = quotationdate;
	}

	public Date getExporderdate() {
		return exporderdate;
	}

	public void setExporderdate(Date exporderdate) {
		this.exporderdate = exporderdate;
	}

	public Date getExpdeldate() {
		return expdeldate;
	}

	public void setExpdeldate(Date expdeldate) {
		this.expdeldate = expdeldate;
	}

	public Date getProjdeldate() {
		return projdeldate;
	}

	public void setProjdeldate(Date projdeldate) {
		this.projdeldate = projdeldate;
	}

	public String getSalesrprtrefno() {
		return salesrprtrefno;
	}

	public void setSalesrprtrefno(String salesrprtrefno) {
		this.salesrprtrefno = salesrprtrefno;
	}

	public String getSalesmancode()
	{
		return salesmancode;
	}

	public void setSalesmancode(String salesmancode)
	{
		this.salesmancode = salesmancode;
	}

	
	
}
