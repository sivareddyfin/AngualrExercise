package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class DedDetails_temp {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer caneacslno;
	@Column
	private String ryotcode;
	@Column
	private Integer DedCode;
	@Column
	private String Deduction;
	@Column
	private Integer harvestcontcode;
	@Column
	private Double suppliedcaneWt;
	@Column
	private Double extentsize;
	@Column
	private Double oldpendingamount;
	@Column
	private Double newpayable;
	@Column
	private Double netpayable;
	@Column
	private Double deductionamt;
	@Column
	private Double pendingamt;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getCaneacslno() {
		return caneacslno;
	}
	public void setCaneacslno(Integer caneacslno) {
		this.caneacslno = caneacslno;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Integer getDedCode() {
		return DedCode;
	}
	public void setDedCode(Integer dedCode) {
		DedCode = dedCode;
	}
	public String getDeduction() {
		return Deduction;
	}
	public void setDeduction(String deduction) {
		Deduction = deduction;
	}
	public Integer getHarvestcontcode() {
		return harvestcontcode;
	}
	public void setHarvestcontcode(Integer harvestcontcode) {
		this.harvestcontcode = harvestcontcode;
	}
	public Double getSuppliedcaneWt() {
		return suppliedcaneWt;
	}
	public void setSuppliedcaneWt(Double suppliedcaneWt) {
		this.suppliedcaneWt = suppliedcaneWt;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public Double getOldpendingamount() {
		return oldpendingamount;
	}
	public void setOldpendingamount(Double oldpendingamount) {
		this.oldpendingamount = oldpendingamount;
	}
	public Double getNewpayable() {
		return newpayable;
	}
	public void setNewpayable(Double newpayable) {
		this.newpayable = newpayable;
	}
	public Double getNetpayable() {
		return netpayable;
	}
	public void setNetpayable(Double netpayable) {
		this.netpayable = netpayable;
	}
	public Double getDeductionamt() {
		return deductionamt;
	}
	public void setDeductionamt(Double deductionamt) {
		this.deductionamt = deductionamt;
	}
	public Double getPendingamt() {
		return pendingamt;
	}
	public void setPendingamt(Double pendingamt) {
		this.pendingamt = pendingamt;
	}
	
	


}
