package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 *
 */
@Entity
public class YtdBookingRegion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	
	@Column
	private String region;
	@Column
	private String country;
	
	@Column 
	private String salesman;
	
	@Column 
	private String salesmancode;
	
	@Column
	private Double  bookingamount;
	@Column
	private Double  amountusd;
	@Column
	private int year;
	@Column
	private int month;
	@Column
    private Byte regstatus;
	
	@Column
	private String migrationtime;

	public String getMigrationtime() {
		return migrationtime;
	}
	public void setMigrationtime(String migrationtime) {
		this.migrationtime = migrationtime;
	}
	public Double getAmountusd() {
		return amountusd;
	}
	public void setAmountusd(Double amountusd) {
		this.amountusd = amountusd;
	}
	
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getSalesman() {
		return salesman;
	}
	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}
	
	
	public String getSalesmancode() {
		return salesmancode;
	}
	public void setSalesmancode(String salesmancode) {
		this.salesmancode = salesmancode;
	}
	public Double getBookingamount() {
		return bookingamount;
	}
	public void setBookingamount(Double bookingamount) {
		this.bookingamount = bookingamount;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public Byte getRegstatus()
	{
		return regstatus;
	}
	public void setRegstatus(Byte regstatus)
	{
		this.regstatus = regstatus;
	}

}
