package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 */
@Entity
public class DailyBookingTemp {
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer slno;
	 @Column
	 private String edate;
	 @Column
	 private String salesorder;
	 @Column
	 private String contractno;
	 @Column
	 private String customercode;
	 @Column
	 private String name1;
	 @Column
	 private String name2;
	 @Column
	 private String shiptoname1;
	 @Column
	 private String shiptoname2;
	 @Column
	 private String enduser;
	 @Column
	 private String payment;
	 @Column
	 private String paymentdescript;
	 @Column
	 private String shipvia;
	 @Column
	 private String salesoffice1;
	 @Column
	 private String code1;
	 @Column
	 private String m1p;
	 @Column
	 private String salesman1;
	 @Column
	 private Double percent1;
	 @Column
	 private Double amount1;
	 @Column
	 private String salesoffice2;
	 @Column
	 private String code2;
	 @Column
	 private String m2p;
	 @Column
	 private String salesman2;
	 @Column
	 private Double percent2;
	 @Column
	 private Double amount2;
	 @Column
	 private String salesoffice3;
	 @Column
	 private String code3;
	 @Column
	 private String m3p;
	 @Column
	 private String salesman3;
	 @Column
	 private Double percent3;
	 @Column
	 private Double amount3;
	 @Column
	 private String salesoffice4;
	 @Column
	 private String code4;
	 @Column
	 private String m4p;
	 @Column
	 private String salesman4;
	 @Column
	 private Double percent4;
	 @Column
	 private Double amount4;
	 @Column
	 private String delivterm;
	 @Column
	 private String delivdescript;
	 @Column
	 private String customerclass;
	 @Column
	 private String unit;
	 @Column
	 private String pid;
	 @Column
	 private String pline;
	 @Column
	 private String description;
	 @Column
	 private String currency;
	 @Column
	 private Double listprice;
	 @Column
	 private Double unitprice;
	 @Column
	 private String contractamount;
	 @Column
	 private String shippingpoint;
	 @Column
	 private String certraw;
	 @Column
	 private String numberofdoc;
	 @Column
	 private String testreport;
	 @Column
	 private String upr;
	 @Column
	 private String applicationcode;
	 @Column
	 private String specialdrawing;
	 public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	@Column
	 private String requireddeliv;
	 @Column
	 private String replieddeliv;
	 @Column
	 private String bookingdate;
	 @Column
	 private String newchange;
	 @Column
	 private String changemark;
	 @Column
	 private String firstdate;
	 @Column
	 private Integer ordertype;
	 @Column
	 private Integer qty;
	 
	 @Column
	 private String flag;
	 
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getSalesorder() {
		return salesorder;
	}
	public void setSalesorder(String salesorder) {
		this.salesorder = salesorder;
	}
	public String getContractno() {
		return contractno;
	}
	public void setContractno(String contractno) {
		this.contractno = contractno;
	}
	public String getCustomercode() {
		return customercode;
	}
	public void setCustomercode(String customercode) {
		this.customercode = customercode;
	}
	public String getName1() {
		return name1;
	}
	public void setName1(String name1) {
		this.name1 = name1;
	}
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
		this.name2 = name2;
	}
	public String getShiptoname1() {
		return shiptoname1;
	}
	public void setShiptoname1(String shiptoname1) {
		this.shiptoname1 = shiptoname1;
	}
	public String getShiptoname2() {
		return shiptoname2;
	}
	public void setShiptoname2(String shiptoname2) {
		this.shiptoname2 = shiptoname2;
	}
	public String getEnduser() {
		return enduser;
	}
	public void setEnduser(String enduser) {
		this.enduser = enduser;
	}
	 
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getPaymentdescript() {
		return paymentdescript;
	}
	public void setPaymentdescript(String paymentdescript) {
		this.paymentdescript = paymentdescript;
	}
	public String getShipvia() {
		return shipvia;
	}
	public void setShipvia(String shipvia) {
		this.shipvia = shipvia;
	}
	public String getSalesoffice1() {
		return salesoffice1;
	}
	public void setSalesoffice1(String salesoffice1) {
		this.salesoffice1 = salesoffice1;
	}
	public String getCode1() {
		return code1;
	}
	public void setCode1(String code1) {
		this.code1 = code1;
	}
	public String getM1p() {
		return m1p;
	}
	public void setM1p(String m1p) {
		this.m1p = m1p;
	}
	public String getSalesman1() {
		return salesman1;
	}
	public void setSalesman1(String salesman1) {
		this.salesman1 = salesman1;
	}
	public Double getPercent1() {
		return percent1;
	}
	public void setPercent1(Double percent1) {
		this.percent1 = percent1;
	}
	public Double getAmount1() {
		return amount1;
	}
	public void setAmount1(Double amount1) {
		this.amount1 = amount1;
	}
	public String getSalesoffice2() {
		return salesoffice2;
	}
	public void setSalesoffice2(String salesoffice2) {
		this.salesoffice2 = salesoffice2;
	}
	public String getCode2() {
		return code2;
	}
	public void setCode2(String code2) {
		this.code2 = code2;
	}
	public String getM2p() {
		return m2p;
	}
	public void setM2p(String m2p) {
		this.m2p = m2p;
	}
	public String getSalesman2() {
		return salesman2;
	}
	public void setSalesman2(String salesman2) {
		this.salesman2 = salesman2;
	}
	public Double getPercent2() {
		return percent2;
	}
	public void setPercent2(Double percent2) {
		this.percent2 = percent2;
	}
	public Double getAmount2() {
		return amount2;
	}
	public void setAmount2(Double amount2) {
		this.amount2 = amount2;
	}
	public String getSalesoffice3() {
		return salesoffice3;
	}
	public void setSalesoffice3(String salesoffice3) {
		this.salesoffice3 = salesoffice3;
	}
	public String getCode3() {
		return code3;
	}
	public void setCode3(String code3) {
		this.code3 = code3;
	}
	public String getM3p() {
		return m3p;
	}
	public void setM3p(String m3p) {
		this.m3p = m3p;
	}
	public String getSalesman3() {
		return salesman3;
	}
	public void setSalesman3(String salesman3) {
		this.salesman3 = salesman3;
	}
	public Double getPercent3() {
		return percent3;
	}
	public void setPercent3(Double percent3) {
		this.percent3 = percent3;
	}
	public Double getAmount3() {
		return amount3;
	}
	public void setAmount3(Double amount3) {
		this.amount3 = amount3;
	}
	public String getSalesoffice4() {
		return salesoffice4;
	}
	public void setSalesoffice4(String salesoffice4) {
		this.salesoffice4 = salesoffice4;
	}
	public String getCode4() {
		return code4;
	}
	public void setCode4(String code4) {
		this.code4 = code4;
	}
	public String getM4p() {
		return m4p;
	}
	public void setM4p(String m4p) {
		this.m4p = m4p;
	}
	public String getSalesman4() {
		return salesman4;
	}
	public void setSalesman4(String salesman4) {
		this.salesman4 = salesman4;
	}
	public Double getPercent4() {
		return percent4;
	}
	public void setPercent4(Double percent4) {
		this.percent4 = percent4;
	}
	public Double getAmount4() {
		return amount4;
	}
	public void setAmount4(Double amount4) {
		this.amount4 = amount4;
	}
	public String getDelivterm() {
		return delivterm;
	}
	public void setDelivterm(String delivterm) {
		this.delivterm = delivterm;
	}
	public String getDelivdescript() {
		return delivdescript;
	}
	public void setDelivdescript(String delivdescript) {
		this.delivdescript = delivdescript;
	}
	public String getCustomerclass() {
		return customerclass;
	}
	public void setCustomerclass(String customerclass) {
		this.customerclass = customerclass;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getPline() {
		return pline;
	}
	public void setPline(String pline) {
		this.pline = pline;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Double getListprice() {
		return listprice;
	}
	public void setListprice(Double listprice) {
		this.listprice = listprice;
	}
	public Double getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(Double unitprice) {
		this.unitprice = unitprice;
	}
	public String getContractamount() {
		return contractamount;
	}
	public void setContractamount(String contractamount) {
		this.contractamount = contractamount;
	}
	public String getShippingpoint() {
		return shippingpoint;
	}
	public void setShippingpoint(String shippingpoint) {
		this.shippingpoint = shippingpoint;
	}
	public String getCertraw() {
		return certraw;
	}
	public void setCertraw(String certraw) {
		this.certraw = certraw;
	}
	public String getNumberofdoc() {
		return numberofdoc;
	}
	public void setNumberofdoc(String numberofdoc) {
		this.numberofdoc = numberofdoc;
	}
	public String getTestreport() {
		return testreport;
	}
	public void setTestreport(String testreport) {
		this.testreport = testreport;
	}
	public String getUpr() {
		return upr;
	}
	public void setUpr(String upr) {
		this.upr = upr;
	}
	public String getApplicationcode() {
		return applicationcode;
	}
	public void setApplicationcode(String applicationcode) {
		this.applicationcode = applicationcode;
	}
	public String getSpecialdrawing() {
		return specialdrawing;
	}
	public void setSpecialdrawing(String specialdrawing) {
		this.specialdrawing = specialdrawing;
	}
	public String getRequireddeliv() {
		return requireddeliv;
	}
	public void setRequireddeliv(String requireddeliv) {
		this.requireddeliv = requireddeliv;
	}
	public String getReplieddeliv() {
		return replieddeliv;
	}
	public void setReplieddeliv(String replieddeliv) {
		this.replieddeliv = replieddeliv;
	}
	public String getBookingdate() {
		return bookingdate;
	}
	public void setBookingdate(String bookingdate) {
		this.bookingdate = bookingdate;
	}
	public String getNewchange() {
		return newchange;
	}
	public void setNewchange(String newchange) {
		this.newchange = newchange;
	}
	public String getChangemark() {
		return changemark;
	}
	public void setChangemark(String changemark) {
		this.changemark = changemark;
	}
	public String getFirstdate() {
		return firstdate;
	}
	public void setFirstdate(String firstdate) {
		this.firstdate = firstdate;
	}
	public Integer getOrdertype() {
		return ordertype;
	}
	public void setOrdertype(Integer ordertype) {
		this.ordertype = ordertype;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	 
	 
	
	 @Column
	 private String countrycode;
	 
	 
	 @Column
	 private String migrationtime;
	 
	public String getModelno() {
		return modelno;
	}
	public void setModelno(String modelno) {
		this.modelno = modelno;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getMigrationtime() {
		return migrationtime;
	}
	public void setMigrationtime(String migrationtime) {
		this.migrationtime = migrationtime;
	}
	public String getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	
							

	 @Column
	 private String modelno;
	 
	 @Column
	 private String  product;
	 
	 

}
