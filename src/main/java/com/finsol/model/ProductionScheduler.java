package com.finsol.model;

import java.sql.Time;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class ProductionScheduler {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;	
	
	@Column
	@Type(type="date")
	private Date transdate;
	
	@Column
	@Type(type="date")
	private Date assemblydate;
	
	@Column
	@Type(type="date")
	private Date duedate;
	
	@Column
	@Type(type="date")
	private Date completeddate;	
	
	@Column
	@Type(type="date")
	private Date bookingdate;
	
	@Column
	@Type(type="date")
	private Date reqdeliverydate;
	
	@Column
	private String applicationcode;
	
	@Column
	private String pid;
	
	@Column
	private String pline;
	
	@Column
	private String upr;
	
	@Column
	private String salesorder;
	
	@Column
	private String name;
	
	@Column
	private String description;
	
	@Column
	private String contractno;
	
	@Column
	private String customercode;
	
	@Column
	private Byte country;	
	
	@Column
	private String modelno;
	
	@Column
	private String serialno;
	
	@Column
	private Integer score;	
	@Column
	private Integer totalscore;	
	
	@Column
	private Integer quantity;	
	
	@Column
	private Integer completedquantity;	
	
	@Column
	private String testedby;
	 
	@Column
	private String prodstatus; //  New/scheduled / Modified / closed / cancelled
	
	@Column
	private String lineitemstatus; //  valid/invalid
	
	@Column
	private String changeitem; //  New/Change/Cancel

	@Column
	private String changeditem1;
	
	@Column
	private String changeditem2;
	
	@Column
	private String changeditem3;
	
	@Column
	private String changeditem4;
	
	@Column
	private String shippingpoint;

	@Column
	private Time starttime;
	
	@Column
	private Time endtime;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTransdate() {
		return transdate;
	}

	public void setTransdate(Date transdate) {
		this.transdate = transdate;
	}

	public Date getAssemblydate() {
		return assemblydate;
	}

	public void setAssemblydate(Date assemblydate) {
		this.assemblydate = assemblydate;
	}

	public Date getDuedate() {
		return duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public Date getCompleteddate() {
		return completeddate;
	}

	public void setCompleteddate(Date completeddate) {
		this.completeddate = completeddate;
	}

	public Date getBookingdate() {
		return bookingdate;
	}

	public void setBookingdate(Date bookingdate) {
		this.bookingdate = bookingdate;
	}

	public Date getReqdeliverydate() {
		return reqdeliverydate;
	}

	public void setReqdeliverydate(Date reqdeliverydate) {
		this.reqdeliverydate = reqdeliverydate;
	}

	public String getApplicationcode() {
		return applicationcode;
	}

	public void setApplicationcode(String applicationcode) {
		this.applicationcode = applicationcode;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPline() {
		return pline;
	}

	public void setPline(String pline) {
		this.pline = pline;
	}

	public String getUpr() {
		return upr;
	}

	public void setUpr(String upr) {
		this.upr = upr;
	}

	public String getSalesorder() {
		return salesorder;
	}

	public void setSalesorder(String salesorder) {
		this.salesorder = salesorder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContractno() {
		return contractno;
	}

	public void setContractno(String contractno) {
		this.contractno = contractno;
	}

	public String getCustomercode() {
		return customercode;
	}

	public void setCustomercode(String customercode) {
		this.customercode = customercode;
	}

	public Byte getCountry() {
		return country;
	}

	public void setCountry(Byte country) {
		this.country = country;
	}

	public String getModelno() {
		return modelno;
	}

	public void setModelno(String modelno) {
		this.modelno = modelno;
	}

	public String getSerialno() {
		return serialno;
	}

	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getTotalscore() {
		return totalscore;
	}

	public void setTotalscore(Integer totalscore) {
		this.totalscore = totalscore;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	public Integer getCompletedquantity() {
		return completedquantity;
	}

	public void setCompletedquantity(Integer completedquantity) {
		this.completedquantity = completedquantity;
	}

	public String getTestedby() {
		return testedby;
	}

	public void setTestedby(String testedby) {
		this.testedby = testedby;
	}

	public String getProdstatus() {
		return prodstatus;
	}

	public void setProdstatus(String prodstatus) {
		this.prodstatus = prodstatus;
	}

	public String getLineitemstatus() {
		return lineitemstatus;
	}

	public void setLineitemstatus(String lineitemstatus) {
		this.lineitemstatus = lineitemstatus;
	}

	public String getChangeitem() {
		return changeitem;
	}

	public void setChangeitem(String changeitem) {
		this.changeitem = changeitem;
	}

	public String getChangeditem1() {
		return changeditem1;
	}

	public void setChangeditem1(String changeditem1) {
		this.changeditem1 = changeditem1;
	}

	public String getChangeditem2() {
		return changeditem2;
	}

	public void setChangeditem2(String changeditem2) {
		this.changeditem2 = changeditem2;
	}

	public String getChangeditem3() {
		return changeditem3;
	}

	public void setChangeditem3(String changeditem3) {
		this.changeditem3 = changeditem3;
	}

	public String getChangeditem4() {
		return changeditem4;
	}

	public void setChangeditem4(String changeditem4) {
		this.changeditem4 = changeditem4;
	}

	public String getShippingpoint() {
		return shippingpoint;
	}

	public void setShippingpoint(String shippingpoint) {
		this.shippingpoint = shippingpoint;
	}

	public Time getStarttime() {
		return starttime;
	}

	public void setStarttime(Time starttime) {
		this.starttime = starttime;
	}

	public Time getEndtime() {
		return endtime;
	}

	public void setEndtime(Time endtime) {
		this.endtime = endtime;
	}
	
	
	
	
}
