package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 *
 */
@Entity
public class YtdProfitDetailsByProduct {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private Integer year;
	@Column
	private Integer monthcode;
	@Column
	private String month;
	@Column
	private String country;
	@Column
	private String pccode;
	@Column
	private String productcategory;
	@Column
	private String productcode;
	@Column
	private String product;
	@Column
	private Double qty;
	@Column
	private Double totalcost;
	@Column
	private Double invoicedamount;
	@Column
	private Double profit;
	@Column
	private Double profitpercent;
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonthcode() {
		return monthcode;
	}
	public void setMonthcode(Integer monthcode) {
		this.monthcode = monthcode;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPccode() {
		return pccode;
	}
	public void setPccode(String pccode) {
		this.pccode = pccode;
	}
	public String getProductcategory() {
		return productcategory;
	}
	public void setProductcategory(String productcategory) {
		this.productcategory = productcategory;
	}
	public String getProductcode() {
		return productcode;
	}
	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getTotalcost() {
		return totalcost;
	}
	public void setTotalcost(Double totalcost) {
		this.totalcost = totalcost;
	}
	public Double getInvoicedamount() {
		return invoicedamount;
	}
	public void setInvoicedamount(Double invoicedamount) {
		this.invoicedamount = invoicedamount;
	}
	public Double getProfit() {
		return profit;
	}
	public void setProfit(Double profit) {
		this.profit = profit;
	}
	public Double getProfitpercent() {
		return profitpercent;
	}
	public void setProfitpercent(Double profitpercent) {
		this.profitpercent = profitpercent;
	}
	
	
	
	
}
