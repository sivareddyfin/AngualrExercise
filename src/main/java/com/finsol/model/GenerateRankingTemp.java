package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author DMurty
 */
@Entity
public class GenerateRankingTemp 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(nullable = false)
	private String zone;
	
	@Column(nullable = false)
	private String circle;
	
	@Column(nullable = false)
	private String ryotcode;
	
	@Column(nullable = false)
	private String ryotname;
	
	@Column(nullable = false)
	private String agreementno;
	
	@Column(nullable = false)
	private String plotno;
	
	@Column(nullable = false)
	private String plantorratoon;
	
	@Column(nullable = false)
	private double extentsize;
	
	@Column(nullable = false)
	private double familygroupccsrating;
	
	@Column(nullable = false)
	private int rank;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getCircle() {
		return circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getRyotname() {
		return ryotname;
	}

	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}

	public String getAgreementno() {
		return agreementno;
	}

	public void setAgreementno(String agreementno) {
		this.agreementno = agreementno;
	}

	public String getPlotno() {
		return plotno;
	}

	public void setPlotno(String plotno) {
		this.plotno = plotno;
	}

	public String getPlantorratoon() {
		return plantorratoon;
	}

	public void setPlantorratoon(String plantorratoon) {
		this.plantorratoon = plantorratoon;
	}

	public double getExtentsize() {
		return extentsize;
	}

	public void setExtentsize(double extentsize) {
		this.extentsize = extentsize;
	}

	public double getFamilygroupccsrating() {
		return familygroupccsrating;
	}

	public void setFamilygroupccsrating(double familygroupccsrating) {
		this.familygroupccsrating = familygroupccsrating;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
}
