package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class QAIssue {

	@Id
	private String casereferenceno;   	
	@Column
	private  String description;
	@Column
	private  String qaissuesfor;
	@Column
	private  String actionrequired;
	@Column
	private  String shimfgno;	
	@Column
	private  String scaclaimno;
	@Column
	@Type(type="date")
	private  Date scadate;
	@Column
	private  String serialno;
	@Column
	private  String possiblereasonsremarks;
	@Column
	private  Byte attachdrawing;
	
	@Column(columnDefinition="TEXT")
	private String imageData;

	public String getCasereferenceno()
	{
		return casereferenceno;
	}

	public void setCasereferenceno(String casereferenceno)
	{
		this.casereferenceno = casereferenceno;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getQaissuesfor()
	{
		return qaissuesfor;
	}

	public void setQaissuesfor(String qaissuesfor)
	{
		this.qaissuesfor = qaissuesfor;
	}

	public String getActionrequired()
	{
		return actionrequired;
	}

	public void setActionrequired(String actionrequired)
	{
		this.actionrequired = actionrequired;
	}

	public String getShimfgno()
	{
		return shimfgno;
	}

	public void setShimfgno(String shimfgno)
	{
		this.shimfgno = shimfgno;
	}

	public String getScaclaimno()
	{
		return scaclaimno;
	}

	public void setScaclaimno(String scaclaimno)
	{
		this.scaclaimno = scaclaimno;
	}



	public Date getScadate()
	{
		return scadate;
	}

	public void setScadate(Date scadate)
	{
		this.scadate = scadate;
	}


	public String getSerialno()
	{
		return serialno;
	}

	public void setSerialno(String serialno)
	{
		this.serialno = serialno;
	}

	public String getPossiblereasonsremarks()
	{
		return possiblereasonsremarks;
	}

	public void setPossiblereasonsremarks(String possiblereasonsremarks)
	{
		this.possiblereasonsremarks = possiblereasonsremarks;
	}

	public Byte getAttachdrawing()
	{
		return attachdrawing;
	}

	public void setAttachdrawing(Byte attachdrawing)
	{
		this.attachdrawing = attachdrawing;
	}

	public String getImageData()
	{
		return imageData;
	}

	public void setImageData(String imageData)
	{
		this.imageData = imageData;
	}
	  
	
	

}
