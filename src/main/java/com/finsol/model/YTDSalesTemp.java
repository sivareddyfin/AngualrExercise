package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author makella - 22-06-2016
 */

@Entity
public class YTDSalesTemp {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer Slno;
	
	@Column
	private String InvNo;
	
	@Column
	private String InvDt;
	
	@Column
	private Double LCInvAmt;
	
	@Column
	private String Currency;
	
	@Column
	private Double USDInvAmt;
	
	@Column
	private String ReqDelDate;
	
	@Column 
	private Double InvQty;
	
	@Column
	private String VendName;
	
	@Column
	private String ProductLine;
	
	@Column
	private String ItemDesc;
	
	@Column
	private String DtOfJobclose;
	
	@Column
	private String DtOfEntry;

	/**
	 * @return the slno
	 */
	public Integer getSlno() {
		return Slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(Integer slno) {
		Slno = slno;
	}

	/**
	 * @return the invNo
	 */
	public String getInvNo() {
		return InvNo;
	}

	/**
	 * @param invNo the invNo to set
	 */
	public void setInvNo(String invNo) {
		InvNo = invNo;
	}

	/**
	 * @return the invDt
	 */
	public String getInvDt() {
		return InvDt;
	}

	/**
	 * @param invDt the invDt to set
	 */
	public void setInvDt(String invDt) {
		InvDt = invDt;
	}

	/**
	 * @return the lCInvAmt
	 */
	public Double getLCInvAmt() {
		return LCInvAmt;
	}

	/**
	 * @param lCInvAmt the lCInvAmt to set
	 */
	public void setLCInvAmt(Double lCInvAmt) {
		LCInvAmt = lCInvAmt;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return Currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		Currency = currency;
	}

	/**
	 * @return the uSDInvAmt
	 */
	public Double getUSDInvAmt() {
		return USDInvAmt;
	}

	/**
	 * @param uSDInvAmt the uSDInvAmt to set
	 */
	public void setUSDInvAmt(Double uSDInvAmt) {
		USDInvAmt = uSDInvAmt;
	}

	/**
	 * @return the reqDelDate
	 */
	public String getReqDelDate() {
		return ReqDelDate;
	}

	/**
	 * @param reqDelDate the reqDelDate to set
	 */
	public void setReqDelDate(String reqDelDate) {
		ReqDelDate = reqDelDate;
	}

	/**
	 * @return the invQty
	 */
	public Double getInvQty() {
		return InvQty;
	}

	/**
	 * @param invQty the invQty to set
	 */
	public void setInvQty(Double invQty) {
		InvQty = invQty;
	}

	/**
	 * @return the vendName
	 */
	public String getVendName() {
		return VendName;
	}

	/**
	 * @param vendName the vendName to set
	 */
	public void setVendName(String vendName) {
		VendName = vendName;
	}

	/**
	 * @return the productLine
	 */
	public String getProductLine() {
		return ProductLine;
	}

	/**
	 * @param productLine the productLine to set
	 */
	public void setProductLine(String productLine) {
		ProductLine = productLine;
	}

	/**
	 * @return the itemDesc
	 */
	public String getItemDesc() {
		return ItemDesc;
	}

	/**
	 * @param itemDesc the itemDesc to set
	 */
	public void setItemDesc(String itemDesc) {
		ItemDesc = itemDesc;
	}

	/**
	 * @return the dtOfJobclose
	 */
	public String getDtOfJobclose() {
		return DtOfJobclose;
	}

	/**
	 * @param dtOfJobclose the dtOfJobclose to set
	 */
	public void setDtOfJobclose(String dtOfJobclose) {
		DtOfJobclose = dtOfJobclose;
	}

	/**
	 * @return the dtOfEntry
	 */
	public String getDtOfEntry() {
		return DtOfEntry;
	}

	/**
	 * @param dtOfEntry the dtOfEntry to set
	 */
	public void setDtOfEntry(String dtOfEntry) {
		DtOfEntry = dtOfEntry;
	}	
}

