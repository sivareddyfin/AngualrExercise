package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class ProgressUpdateSmry 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;	
	@Column
	private String salesrprtrefno;
	@Column
	private String customername;	
	@Column
	private String quotationno;	
	@Column
	private String probabilities;
	@Column
	private Double  totalprobability;
	
	@Column
	private String salesmancode;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSalesrprtrefno() {
		return salesrprtrefno;
	}
	public void setSalesrprtrefno(String salesrprtrefno) {
		this.salesrprtrefno = salesrprtrefno;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getQuotationno() {
		return quotationno;
	}
	public void setQuotationno(String quotationno) {
		this.quotationno = quotationno;
	}
	public String getProbabilities() {
		return probabilities;
	}
	public void setProbabilities(String probabilities) {
		this.probabilities = probabilities;
	}
	public Double getTotalprobability() {
		return totalprobability;
	}
	public void setTotalprobability(Double totalprobability) {
		this.totalprobability = totalprobability;
	}
	public String getSalesmancode()
	{
		return salesmancode;
	}
	public void setSalesmancode(String salesmancode)
	{
		this.salesmancode = salesmancode;
	}
	
	
	
	
}
