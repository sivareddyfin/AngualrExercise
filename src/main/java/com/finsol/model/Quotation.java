package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Quotation {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column (nullable = false)
	private Integer qid;
	
	public Integer getQid() {
		return qid;
	}
	public void setQid(Integer qid) {
		this.qid = qid;
	}
	public String getQtnType() {
		return qtnType;
	}
	public void setQtnType(String qtnType) {
		this.qtnType = qtnType;
	}
	public String getQtnNo() {
		return qtnNo;
	}
	public void setQtnNo(String qtnNo) {
		this.qtnNo = qtnNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	 
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAttention() {
		return attention;
	}
	public void setAttention(String attention) {
		this.attention = attention;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	 
	public Date getDateQuote() {
		return dateQuote;
	}
	public void setDateQuote(Date dateQuote) {
		this.dateQuote = dateQuote;
	}
	public Date getDateValid() {
		return dateValid;
	}
	public void setDateValid(Date dateValid) {
		this.dateValid = dateValid;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getCopy() {
		return copy;
	}
	public void setCopy(String copy) {
		this.copy = copy;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	public String getTaxPercent() {
		return taxPercent;
	}
	public void setTaxPercent(String taxPercent) {
		this.taxPercent = taxPercent;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getCustRef() {
		return custRef;
	}
	public void setCustRef(String custRef) {
		this.custRef = custRef;
	}
	public String getSalesManCode() {
		return salesManCode;
	}
	public void setSalesManCode(String salesManCode) {
		this.salesManCode = salesManCode;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getSalesTerm() {
		return salesTerm;
	}
	public void setSalesTerm(String salesTerm) {
		this.salesTerm = salesTerm;
	}
	public String getExtPercent() {
		return extPercent;
	}
	public void setExtPercent(String extPercent) {
		this.extPercent = extPercent;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getShowCost() {
		return showCost;
	}
	public void setShowCost(String showCost) {
		this.showCost = showCost;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getFreight() {
		return freight;
	}
	public void setFreight(String freight) {
		this.freight = freight;
	}
	public String getFreightTotal() {
		return freightTotal;
	}
	public void setFreightTotal(String freightTotal) {
		this.freightTotal = freightTotal;
	}
	public String getCourier() {
		return courier;
	}
	public void setCourier(String courier) {
		this.courier = courier;
	}
	public String getCourierTotal() {
		return courierTotal;
	}
	public void setCourierTotal(String courierTotal) {
		this.courierTotal = courierTotal;
	}
	 
	@Column
	private String  qtnType;
	@Column
	private String  qtnNo;
	@Column
	private String  custName;
	@Column
	private Date  dateQuote;
	@Column
	private String  currency;
	@Column
	private String  attention;
	@Column
	private String  custCode;
	@Column
	private Date  dateValid;
	@Column
	private String  tax;
	@Column
	private String  copy;
	@Column
	private String  country;
	@Column
	private String  salesMan;
	@Column
	private String  taxPercent;
	@Column
	private String  telephone;
	@Column
	private String  custRef;
	@Column
	private String  salesManCode;
	@Column
	private String  discount;
	@Column
	private String  email;
	@Column
	private String  payment;
	@Column
	private String  salesTerm;
	@Column
	private String  extPercent;
	@Column
	private String  item;
	@Column
	private String  showCost;
	@Column
	private String  subject;
	@Column
	private String  application;
	@Column
	private String  industry;
	@Column
	private String  freight;
	@Column
	private String  freightTotal;
	@Column
	private String  courier;
	@Column
	private String  courierTotal;
	 
	 
	@Column
	private String salesPerson;
	@Column
	private String emailAddr;
	@Column
	private String paymentTerm;
	@Column
	private String estPercent;
	@Column
	private String assigned;
	@Column
	private String caseRef;
	@Column(columnDefinition = "TEXT")
	private String remarks;
	@Column
	private String profit;
	@Column
	private String modelTerm;
	public String getSalesPerson() {
		return salesPerson;
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	public String getEmailAddr() {
		return emailAddr;
	}
	public void setEmailAddr(String emailAddr) {
		this.emailAddr = emailAddr;
	}
	public String getPaymentTerm() {
		return paymentTerm;
	}
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	public String getEstPercent() {
		return estPercent;
	}
	public void setEstPercent(String estPercent) {
		this.estPercent = estPercent;
	}
	public String getAssigned() {
		return assigned;
	}
	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}
	public String getCaseRef() {
		return caseRef;
	}
	public void setCaseRef(String caseRef) {
		this.caseRef = caseRef;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getProfit() {
		return profit;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	public String getModelTerm() {
		return modelTerm;
	}
	public void setModelTerm(String modelTerm) {
		this.modelTerm = modelTerm;
	}
	
	@Column
	private String quser;

	public String getQuser() {
		return quser;
	}
	public void setQuser(String quser) {
		this.quser = quser;
	}
	
	
	@Column
	private String fax;

	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	
	

	@Column
	private String subTotal;
	@Column
	private String grandTotal;

	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
@Column
private String addedTax;

public String getAddedTax() {
	return addedTax;
}
public void setAddedTax(String addedTax) {
	this.addedTax = addedTax;
}



@Column
private String salesmanPhNo;

@Column
private String createdBy;

public String getSalesmanPhNo()
{
	return salesmanPhNo;
}
public void setSalesmanPhNo(String salesmanPhNo)
{
	this.salesmanPhNo = salesmanPhNo;
}
public String getCreatedBy()
{
	return createdBy;
}
public void setCreatedBy(String createdBy)
{
	this.createdBy = createdBy;
}

@Column
private String qtnProgress;

public String getQtnProgress()
{
	return qtnProgress;
}
public void setQtnProgress(String qtnProgress)
{
	this.qtnProgress = qtnProgress;
}

@Column
private int flag;

public int getFlag()
{
	return flag;
}
public void setFlag(int flag)
{
	this.flag = flag;
}

 
	
 
}
