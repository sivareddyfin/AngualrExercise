package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class SparePartGridTemp 
{
	
	
	
	
	public int getTab() {
		return tab;
	}
	public void setTab(int tab) {
		this.tab = tab;
	}
	 
	public String getStoredtime() {
		return storedtime;
	}
	public void setStoredtime(String storedtime) {
		this.storedtime = storedtime;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer newselectiongridid;
	@Column
	private String  gm;
	@Column
	private String  gearMotor;
	@Column
	private String  quantity;
	@Column
	private String  application;
	@Column
	private String  ambientTemp;
	@Column
	private String  gearProduct;
	@Column
	private String  model;
	@Column
	private String  selectModel;
	@Column
	private String  servFactor;
	@Column
	private String  sscCode;
	@Column
	private String  motorType;
	@Column
	private String  motorKW;
	@Column
	private String  pole;
	@Column
	private String  phase;
	@Column
	private String  voltage;
	@Column
	private String  hertz;
	@Column
	private String  rpm;
	@Column
	private String  protectionGrade;
	@Column
	private String  internationalStd;
	@Column
	private String  accessMotor;
	@Column
	private String  motorBrake;
	@Column
	private String  motorBrakeVolt;
	@Column
	private String  hssRadialLoadKn;
	@Column
	private String  hssRadialLoadMm;
	@Column
	private String  hssAxialLoadKn;
	@Column
	private String  dirAxial;
	@Column
	private String  sssRadialLoadKn;
	@Column
	private String  sssRadialLoadMm;
	@Column
	private String  sssAxialLoadKn;
	@Column
	private String  dirSssAxial;
	@Column
	private String  ratioRequired;
	@Column
	private String  requiredOutputSpeed;
	@Column
	private String  presetTorque;
	@Column
	private String  conf;
	@Column
	private String  mount;
	@Column
	private String  mount2;
	@Column
	private String  mount3;
	@Column
	private String  ishaft;
	@Column
	private String  oshaft;
	@Column
	private String  oshaftInc;
	@Column
	private String  deg;
	@Column
	private String  towards;
	@Column
	private String  oshafty;
	@Column
	private String  oshafty2;
	@Column
	private String  backStop;
	@Column
	private String  rot;
	@Column
    private String dirHssAxial;
	@Column
	private Integer newcaseid;
	
	@Column
	private String token;
	@Column
	private int tab;
	@Column
	private String storedtime;
	@Column
	private String username;
	
	@Column
	private String imagepath;
	
	@Column
	private String  mtrEfficncyClass;
	@Column
	private String  inverter;
	

	public String getMtrEfficncyClass() {
		return mtrEfficncyClass;
	}
	public void setMtrEfficncyClass(String mtrEfficncyClass) {
		this.mtrEfficncyClass = mtrEfficncyClass;
	}
	public String getInverter() {
		return inverter;
	}
	public void setInverter(String inverter) {
		this.inverter = inverter;
	}
	@Column
	private String caseref;
	public String getCaseref() {
		return caseref;
	}
	public void setCaseref(String caseref) {
		this.caseref = caseref;
	}
	public String getImagepath() {
		return imagepath;
	}
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Integer getNewselectiongridid() {
		return newselectiongridid;
	}
	public void setNewselectiongridid(Integer newselectiongridid) {
		this.newselectiongridid = newselectiongridid;
	}
	public String getGm() {
		return gm;
	}
	public void setGm(String gm) {
		this.gm = gm;
	}
	public String getGearMotor() {
		return gearMotor;
	}
	public void setGearMotor(String gearMotor) {
		this.gearMotor = gearMotor;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getAmbientTemp() {
		return ambientTemp;
	}
	public void setAmbientTemp(String ambientTemp) {
		this.ambientTemp = ambientTemp;
	}
	public String getGearProduct() {
		return gearProduct;
	}
	public void setGearProduct(String gearProduct) {
		this.gearProduct = gearProduct;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getSelectModel() {
		return selectModel;
	}
	public void setSelectModel(String selectModel) {
		this.selectModel = selectModel;
	}
	public String getServFactor() {
		return servFactor;
	}
	public void setServFactor(String servFactor) {
		this.servFactor = servFactor;
	}
	public String getSscCode() {
		return sscCode;
	}
	public void setSscCode(String sscCode) {
		this.sscCode = sscCode;
	}
	public String getMotorType() {
		return motorType;
	}
	public void setMotorType(String motorType) {
		this.motorType = motorType;
	}
	public String getMotorKW() {
		return motorKW;
	}
	public void setMotorKW(String motorKW) {
		this.motorKW = motorKW;
	}
	public String getPole() {
		return pole;
	}
	public void setPole(String pole) {
		this.pole = pole;
	}
	public String getPhase() {
		return phase;
	}
	public void setPhase(String phase) {
		this.phase = phase;
	}
	public String getVoltage() {
		return voltage;
	}
	public void setVoltage(String voltage) {
		this.voltage = voltage;
	}
	public String getHertz() {
		return hertz;
	}
	public void setHertz(String hertz) {
		this.hertz = hertz;
	}
	public String getRpm() {
		return rpm;
	}
	public void setRpm(String rpm) {
		this.rpm = rpm;
	}
	public String getProtectionGrade() {
		return protectionGrade;
	}
	public void setProtectionGrade(String protectionGrade) {
		this.protectionGrade = protectionGrade;
	}
	public String getInternationalStd() {
		return internationalStd;
	}
	public void setInternationalStd(String internationalStd) {
		this.internationalStd = internationalStd;
	}
	public String getAccessMotor() {
		return accessMotor;
	}
	public void setAccessMotor(String accessMotor) {
		this.accessMotor = accessMotor;
	}
	public String getMotorBrake() {
		return motorBrake;
	}
	public void setMotorBrake(String motorBrake) {
		this.motorBrake = motorBrake;
	}
	public String getMotorBrakeVolt() {
		return motorBrakeVolt;
	}
	public void setMotorBrakeVolt(String motorBrakeVolt) {
		this.motorBrakeVolt = motorBrakeVolt;
	}
	public String getHssRadialLoadKn() {
		return hssRadialLoadKn;
	}
	public void setHssRadialLoadKn(String hssRadialLoadKn) {
		this.hssRadialLoadKn = hssRadialLoadKn;
	}
	public String getHssRadialLoadMm() {
		return hssRadialLoadMm;
	}
	public void setHssRadialLoadMm(String hssRadialLoadMm) {
		this.hssRadialLoadMm = hssRadialLoadMm;
	}
	public String getHssAxialLoadKn() {
		return hssAxialLoadKn;
	}
	public void setHssAxialLoadKn(String hssAxialLoadKn) {
		this.hssAxialLoadKn = hssAxialLoadKn;
	}
	public String getDirAxial() {
		return dirAxial;
	}
	public void setDirAxial(String dirAxial) {
		this.dirAxial = dirAxial;
	}
	public String getSssRadialLoadKn() {
		return sssRadialLoadKn;
	}
	public void setSssRadialLoadKn(String sssRadialLoadKn) {
		this.sssRadialLoadKn = sssRadialLoadKn;
	}
	public String getSssRadialLoadMm() {
		return sssRadialLoadMm;
	}
	public void setSssRadialLoadMm(String sssRadialLoadMm) {
		this.sssRadialLoadMm = sssRadialLoadMm;
	}
	public String getSssAxialLoadKn() {
		return sssAxialLoadKn;
	}
	public void setSssAxialLoadKn(String sssAxialLoadKn) {
		this.sssAxialLoadKn = sssAxialLoadKn;
	}
	public String getDirSssAxial() {
		return dirSssAxial;
	}
	public void setDirSssAxial(String dirSssAxial) {
		this.dirSssAxial = dirSssAxial;
	}
	public String getRatioRequired() {
		return ratioRequired;
	}
	public void setRatioRequired(String ratioRequired) {
		this.ratioRequired = ratioRequired;
	}
	public String getRequiredOutputSpeed() {
		return requiredOutputSpeed;
	}
	public void setRequiredOutputSpeed(String requiredOutputSpeed) {
		this.requiredOutputSpeed = requiredOutputSpeed;
	}
	public String getPresetTorque() {
		return presetTorque;
	}
	public void setPresetTorque(String presetTorque) {
		this.presetTorque = presetTorque;
	}
	public String getConf() {
		return conf;
	}
	public void setConf(String conf) {
		this.conf = conf;
	}
	public String getMount() {
		return mount;
	}
	public void setMount(String mount) {
		this.mount = mount;
	}
	public String getMount2() {
		return mount2;
	}
	public void setMount2(String mount2) {
		this.mount2 = mount2;
	}
	public String getMount3() {
		return mount3;
	}
	public void setMount3(String mount3) {
		this.mount3 = mount3;
	}
	public String getIshaft() {
		return ishaft;
	}
	public void setIshaft(String ishaft) {
		this.ishaft = ishaft;
	}
	public String getOshaft() {
		return oshaft;
	}
	public void setOshaft(String oshaft) {
		this.oshaft = oshaft;
	}
	public String getOshaftInc() {
		return oshaftInc;
	}
	public void setOshaftInc(String oshaftInc) {
		this.oshaftInc = oshaftInc;
	}
	public String getDeg() {
		return deg;
	}
	public void setDeg(String deg) {
		this.deg = deg;
	}
	public String getTowards() {
		return towards;
	}
	public void setTowards(String towards) {
		this.towards = towards;
	}
	public String getOshafty() {
		return oshafty;
	}
	public void setOshafty(String oshafty) {
		this.oshafty = oshafty;
	}
	public String getOshafty2() {
		return oshafty2;
	}
	public void setOshafty2(String oshafty2) {
		this.oshafty2 = oshafty2;
	}
	public String getBackStop() {
		return backStop;
	}
	public void setBackStop(String backStop) {
		this.backStop = backStop;
	}
	public String getRot() {
		return rot;
	}
	public void setRot(String rot) {
		this.rot = rot;
	}
	public String getDirHssAxial() {
		return dirHssAxial;
	}
	public void setDirHssAxial(String dirHssAxial) {
		this.dirHssAxial = dirHssAxial;
	}
	public Integer getNewcaseid() {
		return newcaseid;
	}
	public void setNewcaseid(Integer newcaseid) {
		this.newcaseid = newcaseid;
	}
	@Column
	private String  spare;
	@Column
	private String  descript;
	@Column
	private String  qty;
	@Column
	private String  spare1;
	@Column
	private String  descript1;
	@Column
	private String  qty1;
	@Column
	private String  spare2;
	@Column
	private String  descript2;
	@Column
	private String  qty2;
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getSpare1() {
		return spare1;
	}
	public void setSpare1(String spare1) {
		this.spare1 = spare1;
	}
	public String getDescript1() {
		return descript1;
	}
	public void setDescript1(String descript1) {
		this.descript1 = descript1;
	}
	public String getQty1() {
		return qty1;
	}
	public void setQty1(String qty1) {
		this.qty1 = qty1;
	}
	public String getSpare2() {
		return spare2;
	}
	public void setSpare2(String spare2) {
		this.spare2 = spare2;
	}
	public String getDescript2() {
		return descript2;
	}
	public void setDescript2(String descript2) {
		this.descript2 = descript2;
	}
	public String getQty2() {
		return qty2;
	}
	public void setQty2(String qty2) {
		this.qty2 = qty2;
	}
	public String getSpare() {
		return spare;
	}
	public void setSpare(String spare) {
		this.spare = spare;
	}
	
	
	@Column
	private String oshaftInc2;
	@Column
	private String oshaftInc1;
	@Column
	private String other;
	@Column
	private String othersPlz;


	public String getOshaftInc2() {
		return oshaftInc2;
	}
	public void setOshaftInc2(String oshaftInc2) {
		this.oshaftInc2 = oshaftInc2;
	}
	public String getOshaftInc1() {
		return oshaftInc1;
	}
	public void setOshaftInc1(String oshaftInc1) {
		this.oshaftInc1 = oshaftInc1;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	public String getOthersPlz() {
		return othersPlz;
	}
	public void setOthersPlz(String othersPlz) {
		this.othersPlz = othersPlz;
	}
	
	@Column
	private String gearhead;
	@Column
	private String  motorSerial;
	@Column
	private String shiMfg;


	public String getGearhead() {
		return gearhead;
	}
	public void setGearhead(String gearhead) {
		this.gearhead = gearhead;
	}
	public String getMotorSerial() {
		return motorSerial;
	}
	public void setMotorSerial(String motorSerial) {
		this.motorSerial = motorSerial;
	}
	public String getShiMfg() {
		return shiMfg;
	}
	public void setShiMfg(String shiMfg) {
		this.shiMfg = shiMfg;
	}
	
	
	 

 	 
}
