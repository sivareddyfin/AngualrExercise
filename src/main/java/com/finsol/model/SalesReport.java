package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class SalesReport {
	
	@Id
	private Integer seqno;		
	@Column
	private String salesrptrefno;	//Salesmancode+Sequence/Year
	@Column
	private String salesmancode;	
	@Column
	private String customer;	
	@Column
	@Type(type="date")
	private Date  srdatetime;	
	@Column
	private Byte activity;	
	@Column
	private String callnotes;	
	@Column
	private String remarks;	
	@Column
	private String assignedto;	
	@Column
	private String quotationno;	
	@Column
	private String casereferenceno;
	@Column
	private String customergroup;
	@Column
	private String customerstatus;
	@Column
	private String contactname;
	

	
	public String getSalesrptrefno() {
		return salesrptrefno;
	}
	public void setSalesrptrefno(String salesrptrefno) {
		this.salesrptrefno = salesrptrefno;
	}
	public String getSalesmancode() {
		return salesmancode;
	}
	public void setSalesmancode(String salesmancode) {
		this.salesmancode = salesmancode;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public Date getSrdatetime() {
		return srdatetime;
	}
	public void setSrdatetime(Date srdatetime) {
		this.srdatetime = srdatetime;
	}
	public Byte getActivity() {
		return activity;
	}
	public void setActivity(Byte activity) {
		this.activity = activity;
	}
	public String getCallnotes() {
		return callnotes;
	}
	public void setCallnotes(String callnotes) {
		this.callnotes = callnotes;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getAssignedto() {
		return assignedto;
	}
	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}
	public String getQuotationno() {
		return quotationno;
	}
	public void setQuotationno(String quotationno) {
		this.quotationno = quotationno;
	}
	public String getCasereferenceno() {
		return casereferenceno;
	}
	public void setCasereferenceno(String casereferenceno) {
		this.casereferenceno = casereferenceno;
	}
	
	public Integer getSeqno() {
		return seqno;
	}
	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}
	public String getCustomergroup() {
		return customergroup;
	}
	public void setCustomergroup(String customergroup) {
		this.customergroup = customergroup;
	}
	public String getContactname()
	{
		return contactname;
	}
	public void setContactname(String contactname)
	{
		this.contactname = contactname;
	}
	public String getCustomerstatus()
	{
		return customerstatus;
	}
	public void setCustomerstatus(String customerstatus)
	{
		this.customerstatus = customerstatus;
	}
		
	
}
