var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "main.htm",
        resolve: {
            deps: [
                '$ocLazyLoad',
                function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        serie: true,
                        files: [
                            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',   
                            'app/directives/realtimechart.js',
                            'lib/jquery/charts/flot/jquery.flot.js',
                            'lib/jquery/charts/flot/jquery.flot.orderBars.js',
                            'lib/jquery/charts/flot/jquery.flot.tooltip.js',
                            'lib/jquery/charts/flot/jquery.flot.resize.js',
                            'lib/jquery/charts/flot/jquery.flot.selection.js',
                            'lib/jquery/charts/flot/jquery.flot.crosshair.js',
                            'lib/jquery/charts/flot/jquery.flot.stack.js',
                            'lib/jquery/charts/flot/jquery.flot.time.js',
                            'lib/jquery/charts/flot/jquery.flot.pie.js',
                            'lib/jquery/charts/morris/raphael-2.0.2.min.js',
                            'lib/jquery/charts/chartjs/chart.js',
                            'lib/jquery/charts/morris/morris.js',
                            'lib/jquery/charts/sparkline/jquery.sparkline.js',
                            'lib/jquery/charts/easypiechart/jquery.easypiechart.js',
                            //'app/controllers/databoxes.js'
                        ]
                    });
                }
            ]
        }
        	
    })
    .when("/red", {
        templateUrl : "red.htm"
    })
    .when("/green", {
        templateUrl : "green.htm"
    })
    .when("/blue", {
        templateUrl : "blue.htm"
    });
});